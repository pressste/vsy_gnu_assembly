//*******************************************************************************
//* File: 	stm32f303xe.s
//* Date: 	11. 10. 2018
//* Author:	Jan Svetlik
//* Course: 	A3B38VSY - Department of Measurement
//* Brief:	Header file with STM32F303xe peripheral definitions transferred into
//* 			assembler for uVision Keil v4/v5
//*******************************************************************************
//Bug fixes:
//			11.10.2018 Vaclav Grim - changed adress offset of GPIOx_BSRR registers (0x1a->0x18)
//
//*
//  ******************************************************************************
//  * @file    stm32f303xe.h
//  * @author  MCD Application Team
//  * @brief   CMSIS STM32F303xE Devices Peripheral Access Layer Header File.
//  *
//  *          This file contains:
//  *           - Data structures and the address mapping for all peripherals
//  *           - Peripheral's registers declarations and bits definition
//  *           - Macros to access peripheral�s registers hardware
//  *
//  ******************************************************************************
//  * @attention
//  *
//  * <h2><center>&copy// COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
//  *
//  * Redistribution and use in source and binary forms, with or without modification,
//  * are permitted provided that the following conditions are met:
//  *   1. Redistributions of source code must retain the above copyright notice,
//  *      this list of conditions and the following disclaimer.
//  *   2. Redistributions in binary form must reproduce the above copyright notice,
//  *      this list of conditions and the following disclaimer in the documentation
//  *      and/or other materials provided with the distribution.
//  *   3. Neither the name of STMicroelectronics nor the names of its contributors
//  *      may be used to endorse or promote products derived from this software
//  *      without specific prior written permission.
//  *
//  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
//  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
//  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONS.equENTIAL
//  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
//  * SERVICES// LOSS OF USE, DATA, OR PROFITS// OR BUSINESS INTERRUPTION) HOWEVER
//  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
//  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//  *
//  ******************************************************************************
// Configuration of the Cortex-M4 Processor and Core Peripherals
.equ __CM4_REV, 0x0001 //!< Core revision r0p1 
.equ __MPU_PRESENT, 1 //!< STM32F303xE devices provide an MPU 
.equ __NVIC_PRIO_BITS, 4 //!< STM32F303xE devices use 4 Bits for the Priority Levels 
.equ __Vendor_SysTickConfig, 0 //!< Set to 1 if different SysTick Config is used 
.equ __FPU_PRESENT, 1 //!< STM32F303xE devices provide an FPU 
//* Peripheral_memory_map
.equ FLASH_BASE, (0x08000000) //!< FLASH base address in the alias region 
.equ CCMDATARAM_BASE, (0x10000000) //!< CCM(core coupled memory) data RAM base address in the alias region 
.equ SRAM_BASE, (0x20000000) //!< SRAM base address in the alias region 
.equ PERIPH_BASE, (0x40000000) //!< Peripheral base address in the alias region 
.equ FMC_BASE, (0x60000000) //!< FMC base address 
.equ FMC_R_BASE, (0xA0000000) //!< FMC registers base address 
.equ SRAM_BB_BASE, (0x22000000) //!< SRAM base address in the bit-band region 
.equ PERIPH_BB_BASE, (0x42000000) //!< Peripheral base address in the bit-band region 
//!< Peripheral memory map
.equ APB1PERIPH_BASE, PERIPH_BASE 
.equ APB2PERIPH_BASE, (PERIPH_BASE + 0x00010000) 
.equ AHB1PERIPH_BASE, (PERIPH_BASE + 0x00020000) 
.equ AHB2PERIPH_BASE, (PERIPH_BASE + 0x08000000) 
.equ AHB3PERIPH_BASE, (PERIPH_BASE + 0x10000000) 
//!< APB1 peripherals
.equ TIM2_BASE, (APB1PERIPH_BASE + 0x00000000) 
.equ TIM3_BASE, (APB1PERIPH_BASE + 0x00000400) 
.equ TIM4_BASE, (APB1PERIPH_BASE + 0x00000800) 
.equ TIM6_BASE, (APB1PERIPH_BASE + 0x00001000) 
.equ TIM7_BASE, (APB1PERIPH_BASE + 0x00001400) 
.equ RTC_BASE, (APB1PERIPH_BASE + 0x00002800) 
.equ WWDG_BASE, (APB1PERIPH_BASE + 0x00002C00) 
.equ IWDG_BASE, (APB1PERIPH_BASE + 0x00003000) 
.equ I2S2ext_BASE, (APB1PERIPH_BASE + 0x00003400) 
.equ SPI2_BASE, (APB1PERIPH_BASE + 0x00003800) 
.equ SPI3_BASE, (APB1PERIPH_BASE + 0x00003C00) 
.equ I2S3ext_BASE, (APB1PERIPH_BASE + 0x00004000) 
.equ USART2_BASE, (APB1PERIPH_BASE + 0x00004400) 
.equ USART3_BASE, (APB1PERIPH_BASE + 0x00004800) 
.equ UART4_BASE, (APB1PERIPH_BASE + 0x00004C00) 
.equ UART5_BASE, (APB1PERIPH_BASE + 0x00005000) 
.equ I2C1_BASE, (APB1PERIPH_BASE + 0x00005400) 
.equ I2C2_BASE, (APB1PERIPH_BASE + 0x00005800) 
.equ USB_BASE, (APB1PERIPH_BASE + 0x00005C00) //!< USB_IP Peripheral Registers base address 
.equ USB_PMAADDR, (APB1PERIPH_BASE + 0x00006000) //!< USB_IP Packet Memory Area base address 
.equ CAN_BASE, (APB1PERIPH_BASE + 0x00006400) 
.equ PWR_BASE, (APB1PERIPH_BASE + 0x00007000) 
.equ DAC1_BASE, (APB1PERIPH_BASE + 0x00007400) 
.equ DAC_BASE, DAC1_BASE 
.equ I2C3_BASE, (APB1PERIPH_BASE + 0x00007800) 
//!< APB2 peripherals
.equ SYSCFG_BASE, (APB2PERIPH_BASE + 0x00000000) 
.equ COMP1_BASE, (APB2PERIPH_BASE + 0x0000001C) 
.equ COMP2_BASE, (APB2PERIPH_BASE + 0x00000020) 
.equ COMP3_BASE, (APB2PERIPH_BASE + 0x00000024) 
.equ COMP4_BASE, (APB2PERIPH_BASE + 0x00000028) 
.equ COMP5_BASE, (APB2PERIPH_BASE + 0x0000002C) 
.equ COMP6_BASE, (APB2PERIPH_BASE + 0x00000030) 
.equ COMP7_BASE, (APB2PERIPH_BASE + 0x00000034) 
.equ COMP_BASE, COMP1_BASE 
.equ OPAMP1_BASE, (APB2PERIPH_BASE + 0x00000038) 
.equ OPAMP2_BASE, (APB2PERIPH_BASE + 0x0000003C) 
.equ OPAMP3_BASE, (APB2PERIPH_BASE + 0x00000040) 
.equ OPAMP4_BASE, (APB2PERIPH_BASE + 0x00000044) 
.equ OPAMP_BASE, OPAMP1_BASE 
.equ EXTI_BASE, (APB2PERIPH_BASE + 0x00000400) 
.equ TIM1_BASE, (APB2PERIPH_BASE + 0x00002C00) 
.equ SPI1_BASE, (APB2PERIPH_BASE + 0x00003000) 
.equ TIM8_BASE, (APB2PERIPH_BASE + 0x00003400) 
.equ USART1_BASE, (APB2PERIPH_BASE + 0x00003800) 
.equ SPI4_BASE, (APB2PERIPH_BASE + 0x00003C00) 
.equ TIM15_BASE, (APB2PERIPH_BASE + 0x00004000) 
.equ TIM16_BASE, (APB2PERIPH_BASE + 0x00004400) 
.equ TIM17_BASE, (APB2PERIPH_BASE + 0x00004800) 
.equ TIM20_BASE, (APB2PERIPH_BASE + 0x00005000) 
//!< AHB1 peripherals
.equ DMA1_BASE, (AHB1PERIPH_BASE + 0x00000000) 
.equ DMA1_Channel1_BASE, (AHB1PERIPH_BASE + 0x00000008) 
.equ DMA1_Channel2_BASE, (AHB1PERIPH_BASE + 0x0000001C) 
.equ DMA1_Channel3_BASE, (AHB1PERIPH_BASE + 0x00000030) 
.equ DMA1_Channel4_BASE, (AHB1PERIPH_BASE + 0x00000044) 
.equ DMA1_Channel5_BASE, (AHB1PERIPH_BASE + 0x00000058) 
.equ DMA1_Channel6_BASE, (AHB1PERIPH_BASE + 0x0000006C) 
.equ DMA1_Channel7_BASE, (AHB1PERIPH_BASE + 0x00000080) 
.equ DMA2_BASE, (AHB1PERIPH_BASE + 0x00000400) 
.equ DMA2_Channel1_BASE, (AHB1PERIPH_BASE + 0x00000408) 
.equ DMA2_Channel2_BASE, (AHB1PERIPH_BASE + 0x0000041C) 
.equ DMA2_Channel3_BASE, (AHB1PERIPH_BASE + 0x00000430) 
.equ DMA2_Channel4_BASE, (AHB1PERIPH_BASE + 0x00000444) 
.equ DMA2_Channel5_BASE, (AHB1PERIPH_BASE + 0x00000458) 
.equ RCC_BASE, (AHB1PERIPH_BASE + 0x00001000) 
.equ FLASH_R_BASE, (AHB1PERIPH_BASE + 0x00002000) //!< Flash registers base address 
.equ OB_BASE, (0x1FFFF800) //!< Flash Option Bytes base address 
.equ FLASHSIZE_BASE, (0x1FFFF7CC) //!< FLASH Size register base address 
.equ UID_BASE, (0x1FFFF7AC) //!< Unique device ID register base address 
.equ CRC_BASE, (AHB1PERIPH_BASE + 0x00003000) 
.equ TSC_BASE, (AHB1PERIPH_BASE + 0x00004000) 
//!< AHB2 peripherals
.equ GPIOA_BASE, (AHB2PERIPH_BASE + 0x00000000) 
.equ GPIOB_BASE, (AHB2PERIPH_BASE + 0x00000400) 
.equ GPIOC_BASE, (AHB2PERIPH_BASE + 0x00000800) 
.equ GPIOD_BASE, (AHB2PERIPH_BASE + 0x00000C00) 
.equ GPIOE_BASE, (AHB2PERIPH_BASE + 0x00001000) 
.equ GPIOF_BASE, (AHB2PERIPH_BASE + 0x00001400) 
.equ GPIOG_BASE, (AHB2PERIPH_BASE + 0x00001800) 
.equ GPIOH_BASE, (AHB2PERIPH_BASE + 0x00001C00) 
//!< AHB3 peripherals
.equ ADC1_BASE, (AHB3PERIPH_BASE + 0x00000000) 
.equ ADC2_BASE, (AHB3PERIPH_BASE + 0x00000100) 
.equ ADC1_2_COMMON_BASE, (AHB3PERIPH_BASE + 0x00000300) 
.equ ADC3_BASE, (AHB3PERIPH_BASE + 0x00000400) 
.equ ADC4_BASE, (AHB3PERIPH_BASE + 0x00000500) 
.equ ADC3_4_COMMON_BASE, (AHB3PERIPH_BASE + 0x00000700) 
//!< FMC Bankx base address
.equ FMC_BANK1, (FMC_BASE) //!< FMC Bank1 base address 
.equ FMC_BANK1_1, (FMC_BANK1) //!< FMC Bank1_1 base address 
.equ FMC_BANK1_2, (FMC_BANK1 + 0x04000000) //!< FMC Bank1_2 base address 
.equ FMC_BANK1_3, (FMC_BANK1 + 0x08000000) //!< FMC Bank1_3 base address 
.equ FMC_BANK1_4, (FMC_BANK1 + 0x0C000000) //!< FMC Bank1_4 base address 
.equ FMC_BANK2, (FMC_BASE + 0x10000000) //!< FMC Bank2 base address 
.equ FMC_BANK3, (FMC_BASE + 0x20000000) //!< FMC Bank3 base address 
.equ FMC_BANK4, (FMC_BASE + 0x30000000) //!< FMC Bank4 base address 
//!< FMC Bankx registers base address
.equ FMC_Bank1_R_BASE, (FMC_R_BASE + 0x0000) 
.equ FMC_Bank1E_R_BASE, (FMC_R_BASE + 0x0104) 
.equ FMC_Bank2_3_R_BASE, (FMC_R_BASE + 0x0060) 
.equ FMC_Bank4_R_BASE, (FMC_R_BASE + 0x00A0) 
.equ DBGMCU_BASE, (0xE0042000) //!< Debug MCU registers base address 
// Peripheral_declaration
.equ TIM2, (TIM2_BASE) 
.equ TIM3, (TIM3_BASE) 
.equ TIM4, (TIM4_BASE) 
.equ TIM6, (TIM6_BASE) 
.equ TIM7, (TIM7_BASE) 
.equ RTC, (RTC_BASE) 
.equ WWDG, (WWDG_BASE) 
.equ IWDG, (IWDG_BASE) 
.equ I2S2ext, (I2S2ext_BASE) 
.equ SPI2, (SPI2_BASE) 
.equ SPI3, (SPI3_BASE) 
.equ I2S3ext, (I2S3ext_BASE) 
.equ USART2, (USART2_BASE) 
.equ USART3, (USART3_BASE) 
.equ UART4, (UART4_BASE) 
.equ UART5, (UART5_BASE) 
.equ I2C1, (I2C1_BASE) 
.equ I2C2, (I2C2_BASE) 
.equ I2C3, (I2C3_BASE) 
.equ CAN, (CAN_BASE) 
.equ PWR, (PWR_BASE) 
.equ DAC, (DAC_BASE) 
.equ DAC1, (DAC1_BASE) 
.equ COMP1, (COMP1_BASE) 
.equ COMP2, (COMP2_BASE) 
.equ COMP12_COMMON, (COMP2_BASE) 
.equ COMP3, (COMP3_BASE) 
.equ COMP4, (COMP4_BASE) 
.equ COMP34_COMMON, (COMP4_BASE) 
.equ COMP5, (COMP5_BASE) 
.equ COMP6, (COMP6_BASE) 
.equ COMP56_COMMON, (COMP6_BASE) 
.equ COMP7, (COMP7_BASE) 
// Legacy define
.equ COMP, (COMP_BASE) 
.equ OPAMP1, (OPAMP1_BASE) 
.equ OPAMP, (OPAMP_BASE) 
.equ OPAMP2, (OPAMP2_BASE) 
.equ OPAMP3, (OPAMP3_BASE) 
.equ OPAMP4, (OPAMP4_BASE) 
.equ SYSCFG, (SYSCFG_BASE) 
.equ EXTI, (EXTI_BASE) 
.equ TIM1, (TIM1_BASE) 
.equ SPI1, (SPI1_BASE) 
.equ TIM8, (TIM8_BASE) 
.equ USART1, (USART1_BASE) 
.equ SPI4, (SPI4_BASE) 
.equ TIM15, (TIM15_BASE) 
.equ TIM16, (TIM16_BASE) 
.equ TIM17, (TIM17_BASE) 
.equ TIM20, (TIM20_BASE) 
.equ DBGMCU, (DBGMCU_BASE) 
.equ DMA1, (DMA1_BASE) 
.equ DMA1_Channel1, (DMA1_Channel1_BASE) 
.equ DMA1_Channel2, (DMA1_Channel2_BASE) 
.equ DMA1_Channel3, (DMA1_Channel3_BASE) 
.equ DMA1_Channel4, (DMA1_Channel4_BASE) 
.equ DMA1_Channel5, (DMA1_Channel5_BASE) 
.equ DMA1_Channel6, (DMA1_Channel6_BASE) 
.equ DMA1_Channel7, (DMA1_Channel7_BASE) 
.equ DMA2, (DMA2_BASE) 
.equ DMA2_Channel1, (DMA2_Channel1_BASE) 
.equ DMA2_Channel2, (DMA2_Channel2_BASE) 
.equ DMA2_Channel3, (DMA2_Channel3_BASE) 
.equ DMA2_Channel4, (DMA2_Channel4_BASE) 
.equ DMA2_Channel5, (DMA2_Channel5_BASE) 
.equ RCC, (RCC_BASE) 
.equ FLASH, (FLASH_R_BASE) 
.equ OB, (OB_BASE) 
.equ CRC, (CRC_BASE) 
.equ TSC, (TSC_BASE) 
.equ GPIOA, (GPIOA_BASE) 
.equ GPIOB, (GPIOB_BASE) 
.equ GPIOC, (GPIOC_BASE) 
.equ GPIOD, (GPIOD_BASE) 
.equ GPIOE, (GPIOE_BASE) 
.equ GPIOF, (GPIOF_BASE) 
.equ GPIOG, (GPIOG_BASE) 
.equ GPIOH, (GPIOH_BASE) 
.equ ADC1, (ADC1_BASE) 
.equ ADC2, (ADC2_BASE) 
.equ ADC3, (ADC3_BASE) 
.equ ADC4, (ADC4_BASE) 
.equ ADC12_COMMON, (ADC1_2_COMMON_BASE) 
.equ ADC34_COMMON, (ADC3_4_COMMON_BASE) 
// Legacy defines
.equ ADC1_2_COMMON, ADC12_COMMON 
.equ ADC3_4_COMMON, ADC34_COMMON 
.equ USB, (USB_BASE) 
.equ FMC_Bank1, (FMC_Bank1_R_BASE) 
.equ FMC_Bank1E, (FMC_Bank1E_R_BASE) 
.equ FMC_Bank2_3, (FMC_Bank2_3_R_BASE) 
.equ FMC_Bank4, (FMC_Bank4_R_BASE) 
//****************************************************************************
//                         Peripheral Registers definitions
//****************************************************************************
.equ TIM1_CR1, (TIM1 + 0x00) //!< TIM control register 1, Address offset: 0x00 
.equ TIM1_CR2, (TIM1 + 0x04) //!< TIM control register 2, Address offset: 0x04 
.equ TIM1_SMCR, (TIM1 + 0x08) //!< TIM slave mode control register, Address offset: 0x08 
.equ TIM1_DIER, (TIM1 + 0x0C) //!< TIM DMA/interrupt enable register, Address offset: 0x0C 
.equ TIM1_SR, (TIM1 + 0x10) //!< TIM status register, Address offset: 0x10 
.equ TIM1_EGR, (TIM1 + 0x14) //!< TIM event generation register, Address offset: 0x14 
.equ TIM1_CCMR1, (TIM1 + 0x18) //!< TIM capture/compare mode register 1, Address offset: 0x18 
.equ TIM1_CCMR2, (TIM1 + 0x1C) //!< TIM capture/compare mode register 2, Address offset: 0x1C 
.equ TIM1_CCER, (TIM1 + 0x20) //!< TIM capture/compare enable register, Address offset: 0x20 
.equ TIM1_CNT, (TIM1 + 0x24) //!< TIM counter register, Address offset: 0x24 
.equ TIM1_PSC, (TIM1 + 0x28) //!< TIM prescaler, Address offset: 0x28 
.equ TIM1_ARR, (TIM1 + 0x2C) //!< TIM auto-reload register, Address offset: 0x2C 
.equ TIM1_RCR, (TIM1 + 0x30) //!< TIM repetition counter register, Address offset: 0x30 
.equ TIM1_CCR1, (TIM1 + 0x34) //!< TIM capture/compare register 1, Address offset: 0x34 
.equ TIM1_CCR2, (TIM1 + 0x38) //!< TIM capture/compare register 2, Address offset: 0x38 
.equ TIM1_CCR3, (TIM1 + 0x3C) //!< TIM capture/compare register 3, Address offset: 0x3C 
.equ TIM1_CCR4, (TIM1 + 0x40) //!< TIM capture/compare register 4, Address offset: 0x40 
.equ TIM1_BDTR, (TIM1 + 0x44) //!< TIM break and dead-time register, Address offset: 0x44 
.equ TIM1_DCR, (TIM1 + 0x48) //!< TIM DMA control register, Address offset: 0x48 
.equ TIM1_DMAR, (TIM1 + 0x4C) //!< TIM DMA address for full transfer, Address offset: 0x4C 
.equ TIM1_OR, (TIM1 + 0x50) //!< TIM option register, Address offset: 0x50 
.equ TIM1_CCMR3, (TIM1 + 0x54) //!< TIM capture/compare mode register 3, Address offset: 0x54 
.equ TIM1_CCR5, (TIM1 + 0x58) //!< TIM capture/compare register5, Address offset: 0x58 
.equ TIM1_CCR6, (TIM1 + 0x5C) //!< TIM capture/compare register 4, Address offset: 0x5C 
.equ TIM2_CR1, (TIM2 + 0x00) //!< TIM control register 1, Address offset: 0x00 
.equ TIM2_CR2, (TIM2 + 0x04) //!< TIM control register 2, Address offset: 0x04 
.equ TIM2_SMCR, (TIM2 + 0x08) //!< TIM slave mode control register, Address offset: 0x08 
.equ TIM2_DIER, (TIM2 + 0x0C) //!< TIM DMA/interrupt enable register, Address offset: 0x0C 
.equ TIM2_SR, (TIM2 + 0x10) //!< TIM status register, Address offset: 0x10 
.equ TIM2_EGR, (TIM2 + 0x14) //!< TIM event generation register, Address offset: 0x14 
.equ TIM2_CCMR1, (TIM2 + 0x18) //!< TIM capture/compare mode register 1, Address offset: 0x18 
.equ TIM2_CCMR2, (TIM2 + 0x1C) //!< TIM capture/compare mode register 2, Address offset: 0x1C 
.equ TIM2_CCER, (TIM2 + 0x20) //!< TIM capture/compare enable register, Address offset: 0x20 
.equ TIM2_CNT, (TIM2 + 0x24) //!< TIM counter register, Address offset: 0x24 
.equ TIM2_PSC, (TIM2 + 0x28) //!< TIM prescaler, Address offset: 0x28 
.equ TIM2_ARR, (TIM2 + 0x2C) //!< TIM auto-reload register, Address offset: 0x2C 
.equ TIM2_RCR, (TIM2 + 0x30) //!< TIM repetition counter register, Address offset: 0x30 
.equ TIM2_CCR1, (TIM2 + 0x34) //!< TIM capture/compare register 1, Address offset: 0x34 
.equ TIM2_CCR2, (TIM2 + 0x38) //!< TIM capture/compare register 2, Address offset: 0x38 
.equ TIM2_CCR3, (TIM2 + 0x3C) //!< TIM capture/compare register 3, Address offset: 0x3C 
.equ TIM2_CCR4, (TIM2 + 0x40) //!< TIM capture/compare register 4, Address offset: 0x40 
.equ TIM2_BDTR, (TIM2 + 0x44) //!< TIM break and dead-time register, Address offset: 0x44 
.equ TIM2_DCR, (TIM2 + 0x48) //!< TIM DMA control register, Address offset: 0x48 
.equ TIM2_DMAR, (TIM2 + 0x4C) //!< TIM DMA address for full transfer, Address offset: 0x4C 
.equ TIM2_OR, (TIM2 + 0x50) //!< TIM option register, Address offset: 0x50 
.equ TIM2_CCMR3, (TIM2 + 0x54) //!< TIM capture/compare mode register 3, Address offset: 0x54 
.equ TIM2_CCR5, (TIM2 + 0x58) //!< TIM capture/compare register5, Address offset: 0x58 
.equ TIM2_CCR6, (TIM2 + 0x5C) //!< TIM capture/compare register 4, Address offset: 0x5C 
.equ TIM3_CR1, (TIM3 + 0x00) //!< TIM control register 1, Address offset: 0x00 
.equ TIM3_CR2, (TIM3 + 0x04) //!< TIM control register 2, Address offset: 0x04 
.equ TIM3_SMCR, (TIM3 + 0x08) //!< TIM slave mode control register, Address offset: 0x08 
.equ TIM3_DIER, (TIM3 + 0x0C) //!< TIM DMA/interrupt enable register, Address offset: 0x0C 
.equ TIM3_SR, (TIM3 + 0x10) //!< TIM status register, Address offset: 0x10 
.equ TIM3_EGR, (TIM3 + 0x14) //!< TIM event generation register, Address offset: 0x14 
.equ TIM3_CCMR1, (TIM3 + 0x18) //!< TIM capture/compare mode register 1, Address offset: 0x18 
.equ TIM3_CCMR2, (TIM3 + 0x1C) //!< TIM capture/compare mode register 2, Address offset: 0x1C 
.equ TIM3_CCER, (TIM3 + 0x20) //!< TIM capture/compare enable register, Address offset: 0x20 
.equ TIM3_CNT, (TIM3 + 0x24) //!< TIM counter register, Address offset: 0x24 
.equ TIM3_PSC, (TIM3 + 0x28) //!< TIM prescaler, Address offset: 0x28 
.equ TIM3_ARR, (TIM3 + 0x2C) //!< TIM auto-reload register, Address offset: 0x2C 
.equ TIM3_RCR, (TIM3 + 0x30) //!< TIM repetition counter register, Address offset: 0x30 
.equ TIM3_CCR1, (TIM3 + 0x34) //!< TIM capture/compare register 1, Address offset: 0x34 
.equ TIM3_CCR2, (TIM3 + 0x38) //!< TIM capture/compare register 2, Address offset: 0x38 
.equ TIM3_CCR3, (TIM3 + 0x3C) //!< TIM capture/compare register 3, Address offset: 0x3C 
.equ TIM3_CCR4, (TIM3 + 0x40) //!< TIM capture/compare register 4, Address offset: 0x40 
.equ TIM3_BDTR, (TIM3 + 0x44) //!< TIM break and dead-time register, Address offset: 0x44 
.equ TIM3_DCR, (TIM3 + 0x48) //!< TIM DMA control register, Address offset: 0x48 
.equ TIM3_DMAR, (TIM3 + 0x4C) //!< TIM DMA address for full transfer, Address offset: 0x4C 
.equ TIM3_OR, (TIM3 + 0x50) //!< TIM option register, Address offset: 0x50 
.equ TIM3_CCMR3, (TIM3 + 0x54) //!< TIM capture/compare mode register 3, Address offset: 0x54 
.equ TIM3_CCR5, (TIM3 + 0x58) //!< TIM capture/compare register5, Address offset: 0x58 
.equ TIM3_CCR6, (TIM3 + 0x5C) //!< TIM capture/compare register 4, Address offset: 0x5C 
.equ TIM4_CR1, (TIM4 + 0x00) //!< TIM control register 1, Address offset: 0x00 
.equ TIM4_CR2, (TIM4 + 0x04) //!< TIM control register 2, Address offset: 0x04 
.equ TIM4_SMCR, (TIM4 + 0x08) //!< TIM slave mode control register, Address offset: 0x08 
.equ TIM4_DIER, (TIM4 + 0x0C) //!< TIM DMA/interrupt enable register, Address offset: 0x0C 
.equ TIM4_SR, (TIM4 + 0x10) //!< TIM status register, Address offset: 0x10 
.equ TIM4_EGR, (TIM4 + 0x14) //!< TIM event generation register, Address offset: 0x14 
.equ TIM4_CCMR1, (TIM4 + 0x18) //!< TIM capture/compare mode register 1, Address offset: 0x18 
.equ TIM4_CCMR2, (TIM4 + 0x1C) //!< TIM capture/compare mode register 2, Address offset: 0x1C 
.equ TIM4_CCER, (TIM4 + 0x20) //!< TIM capture/compare enable register, Address offset: 0x20 
.equ TIM4_CNT, (TIM4 + 0x24) //!< TIM counter register, Address offset: 0x24 
.equ TIM4_PSC, (TIM4 + 0x28) //!< TIM prescaler, Address offset: 0x28 
.equ TIM4_ARR, (TIM4 + 0x2C) //!< TIM auto-reload register, Address offset: 0x2C 
.equ TIM4_RCR, (TIM4 + 0x30) //!< TIM repetition counter register, Address offset: 0x30 
.equ TIM4_CCR1, (TIM4 + 0x34) //!< TIM capture/compare register 1, Address offset: 0x34 
.equ TIM4_CCR2, (TIM4 + 0x38) //!< TIM capture/compare register 2, Address offset: 0x38 
.equ TIM4_CCR3, (TIM4 + 0x3C) //!< TIM capture/compare register 3, Address offset: 0x3C 
.equ TIM4_CCR4, (TIM4 + 0x40) //!< TIM capture/compare register 4, Address offset: 0x40 
.equ TIM4_BDTR, (TIM4 + 0x44) //!< TIM break and dead-time register, Address offset: 0x44 
.equ TIM4_DCR, (TIM4 + 0x48) //!< TIM DMA control register, Address offset: 0x48 
.equ TIM4_DMAR, (TIM4 + 0x4C) //!< TIM DMA address for full transfer, Address offset: 0x4C 
.equ TIM4_OR, (TIM4 + 0x50) //!< TIM option register, Address offset: 0x50 
.equ TIM4_CCMR3, (TIM4 + 0x54) //!< TIM capture/compare mode register 3, Address offset: 0x54 
.equ TIM4_CCR5, (TIM4 + 0x58) //!< TIM capture/compare register5, Address offset: 0x58 
.equ TIM4_CCR6, (TIM4 + 0x5C) //!< TIM capture/compare register 4, Address offset: 0x5C 
.equ TIM6_CR1, (TIM6 + 0x00) //!< TIM control register 1, Address offset: 0x00 
.equ TIM6_CR2, (TIM6 + 0x04) //!< TIM control register 2, Address offset: 0x04 
.equ TIM6_SMCR, (TIM6 + 0x08) //!< TIM slave mode control register, Address offset: 0x08 
.equ TIM6_DIER, (TIM6 + 0x0C) //!< TIM DMA/interrupt enable register, Address offset: 0x0C 
.equ TIM6_SR, (TIM6 + 0x10) //!< TIM status register, Address offset: 0x10 
.equ TIM6_EGR, (TIM6 + 0x14) //!< TIM event generation register, Address offset: 0x14 
.equ TIM6_CCMR1, (TIM6 + 0x18) //!< TIM capture/compare mode register 1, Address offset: 0x18 
.equ TIM6_CCMR2, (TIM6 + 0x1C) //!< TIM capture/compare mode register 2, Address offset: 0x1C 
.equ TIM6_CCER, (TIM6 + 0x20) //!< TIM capture/compare enable register, Address offset: 0x20 
.equ TIM6_CNT, (TIM6 + 0x24) //!< TIM counter register, Address offset: 0x24 
.equ TIM6_PSC, (TIM6 + 0x28) //!< TIM prescaler, Address offset: 0x28 
.equ TIM6_ARR, (TIM6 + 0x2C) //!< TIM auto-reload register, Address offset: 0x2C 
.equ TIM6_RCR, (TIM6 + 0x30) //!< TIM repetition counter register, Address offset: 0x30 
.equ TIM6_CCR1, (TIM6 + 0x34) //!< TIM capture/compare register 1, Address offset: 0x34 
.equ TIM6_CCR2, (TIM6 + 0x38) //!< TIM capture/compare register 2, Address offset: 0x38 
.equ TIM6_CCR3, (TIM6 + 0x3C) //!< TIM capture/compare register 3, Address offset: 0x3C 
.equ TIM6_CCR4, (TIM6 + 0x40) //!< TIM capture/compare register 4, Address offset: 0x40 
.equ TIM6_BDTR, (TIM6 + 0x44) //!< TIM break and dead-time register, Address offset: 0x44 
.equ TIM6_DCR, (TIM6 + 0x48) //!< TIM DMA control register, Address offset: 0x48 
.equ TIM6_DMAR, (TIM6 + 0x4C) //!< TIM DMA address for full transfer, Address offset: 0x4C 
.equ TIM6_OR, (TIM6 + 0x50) //!< TIM option register, Address offset: 0x50 
.equ TIM6_CCMR3, (TIM6 + 0x54) //!< TIM capture/compare mode register 3, Address offset: 0x54 
.equ TIM6_CCR5, (TIM6 + 0x58) //!< TIM capture/compare register5, Address offset: 0x58 
.equ TIM6_CCR6, (TIM6 + 0x5C) //!< TIM capture/compare register 4, Address offset: 0x5C 
.equ TIM7_CR1, (TIM7 + 0x00) //!< TIM control register 1, Address offset: 0x00 
.equ TIM7_CR2, (TIM7 + 0x04) //!< TIM control register 2, Address offset: 0x04 
.equ TIM7_SMCR, (TIM7 + 0x08) //!< TIM slave mode control register, Address offset: 0x08 
.equ TIM7_DIER, (TIM7 + 0x0C) //!< TIM DMA/interrupt enable register, Address offset: 0x0C 
.equ TIM7_SR, (TIM7 + 0x10) //!< TIM status register, Address offset: 0x10 
.equ TIM7_EGR, (TIM7 + 0x14) //!< TIM event generation register, Address offset: 0x14 
.equ TIM7_CCMR1, (TIM7 + 0x18) //!< TIM capture/compare mode register 1, Address offset: 0x18 
.equ TIM7_CCMR2, (TIM7 + 0x1C) //!< TIM capture/compare mode register 2, Address offset: 0x1C 
.equ TIM7_CCER, (TIM7 + 0x20) //!< TIM capture/compare enable register, Address offset: 0x20 
.equ TIM7_CNT, (TIM7 + 0x24) //!< TIM counter register, Address offset: 0x24 
.equ TIM7_PSC, (TIM7 + 0x28) //!< TIM prescaler, Address offset: 0x28 
.equ TIM7_ARR, (TIM7 + 0x2C) //!< TIM auto-reload register, Address offset: 0x2C 
.equ TIM7_RCR, (TIM7 + 0x30) //!< TIM repetition counter register, Address offset: 0x30 
.equ TIM7_CCR1, (TIM7 + 0x34) //!< TIM capture/compare register 1, Address offset: 0x34 
.equ TIM7_CCR2, (TIM7 + 0x38) //!< TIM capture/compare register 2, Address offset: 0x38 
.equ TIM7_CCR3, (TIM7 + 0x3C) //!< TIM capture/compare register 3, Address offset: 0x3C 
.equ TIM7_CCR4, (TIM7 + 0x40) //!< TIM capture/compare register 4, Address offset: 0x40 
.equ TIM7_BDTR, (TIM7 + 0x44) //!< TIM break and dead-time register, Address offset: 0x44 
.equ TIM7_DCR, (TIM7 + 0x48) //!< TIM DMA control register, Address offset: 0x48 
.equ TIM7_DMAR, (TIM7 + 0x4C) //!< TIM DMA address for full transfer, Address offset: 0x4C 
.equ TIM7_OR, (TIM7 + 0x50) //!< TIM option register, Address offset: 0x50 
.equ TIM7_CCMR3, (TIM7 + 0x54) //!< TIM capture/compare mode register 3, Address offset: 0x54 
.equ TIM7_CCR5, (TIM7 + 0x58) //!< TIM capture/compare register5, Address offset: 0x58 
.equ TIM7_CCR6, (TIM7 + 0x5C) //!< TIM capture/compare register 4, Address offset: 0x5C 
.equ TIM8_CR1, (TIM8 + 0x00) //!< TIM control register 1, Address offset: 0x00 
.equ TIM8_CR2, (TIM8 + 0x04) //!< TIM control register 2, Address offset: 0x04 
.equ TIM8_SMCR, (TIM8 + 0x08) //!< TIM slave mode control register, Address offset: 0x08 
.equ TIM8_DIER, (TIM8 + 0x0C) //!< TIM DMA/interrupt enable register, Address offset: 0x0C 
.equ TIM8_SR, (TIM8 + 0x10) //!< TIM status register, Address offset: 0x10 
.equ TIM8_EGR, (TIM8 + 0x14) //!< TIM event generation register, Address offset: 0x14 
.equ TIM8_CCMR1, (TIM8 + 0x18) //!< TIM capture/compare mode register 1, Address offset: 0x18 
.equ TIM8_CCMR2, (TIM8 + 0x1C) //!< TIM capture/compare mode register 2, Address offset: 0x1C 
.equ TIM8_CCER, (TIM8 + 0x20) //!< TIM capture/compare enable register, Address offset: 0x20 
.equ TIM8_CNT, (TIM8 + 0x24) //!< TIM counter register, Address offset: 0x24 
.equ TIM8_PSC, (TIM8 + 0x28) //!< TIM prescaler, Address offset: 0x28 
.equ TIM8_ARR, (TIM8 + 0x2C) //!< TIM auto-reload register, Address offset: 0x2C 
.equ TIM8_RCR, (TIM8 + 0x30) //!< TIM repetition counter register, Address offset: 0x30 
.equ TIM8_CCR1, (TIM8 + 0x34) //!< TIM capture/compare register 1, Address offset: 0x34 
.equ TIM8_CCR2, (TIM8 + 0x38) //!< TIM capture/compare register 2, Address offset: 0x38 
.equ TIM8_CCR3, (TIM8 + 0x3C) //!< TIM capture/compare register 3, Address offset: 0x3C 
.equ TIM8_CCR4, (TIM8 + 0x40) //!< TIM capture/compare register 4, Address offset: 0x40 
.equ TIM8_BDTR, (TIM8 + 0x44) //!< TIM break and dead-time register, Address offset: 0x44 
.equ TIM8_DCR, (TIM8 + 0x48) //!< TIM DMA control register, Address offset: 0x48 
.equ TIM8_DMAR, (TIM8 + 0x4C) //!< TIM DMA address for full transfer, Address offset: 0x4C 
.equ TIM8_OR, (TIM8 + 0x50) //!< TIM option register, Address offset: 0x50 
.equ TIM8_CCMR3, (TIM8 + 0x54) //!< TIM capture/compare mode register 3, Address offset: 0x54 
.equ TIM8_CCR5, (TIM8 + 0x58) //!< TIM capture/compare register5, Address offset: 0x58 
.equ TIM8_CCR6, (TIM8 + 0x5C) //!< TIM capture/compare register 4, Address offset: 0x5C 
.equ TIM15_CR1, (TIM15 + 0x00) //!< TIM control register 1, Address offset: 0x00 
.equ TIM15_CR2, (TIM15 + 0x04) //!< TIM control register 2, Address offset: 0x04 
.equ TIM15_SMCR, (TIM15 + 0x08) //!< TIM slave mode control register, Address offset: 0x08 
.equ TIM15_DIER, (TIM15 + 0x0C) //!< TIM DMA/interrupt enable register, Address offset: 0x0C 
.equ TIM15_SR, (TIM15 + 0x10) //!< TIM status register, Address offset: 0x10 
.equ TIM15_EGR, (TIM15 + 0x14) //!< TIM event generation register, Address offset: 0x14 
.equ TIM15_CCMR1, (TIM15 + 0x18) //!< TIM capture/compare mode register 1, Address offset: 0x18 
.equ TIM15_CCMR2, (TIM15 + 0x1C) //!< TIM capture/compare mode register 2, Address offset: 0x1C 
.equ TIM15_CCER, (TIM15 + 0x20) //!< TIM capture/compare enable register, Address offset: 0x20 
.equ TIM15_CNT, (TIM15 + 0x24) //!< TIM counter register, Address offset: 0x24 
.equ TIM15_PSC, (TIM15 + 0x28) //!< TIM prescaler, Address offset: 0x28 
.equ TIM15_ARR, (TIM15 + 0x2C) //!< TIM auto-reload register, Address offset: 0x2C 
.equ TIM15_RCR, (TIM15 + 0x30) //!< TIM repetition counter register, Address offset: 0x30 
.equ TIM15_CCR1, (TIM15 + 0x34) //!< TIM capture/compare register 1, Address offset: 0x34 
.equ TIM15_CCR2, (TIM15 + 0x38) //!< TIM capture/compare register 2, Address offset: 0x38 
.equ TIM15_CCR3, (TIM15 + 0x3C) //!< TIM capture/compare register 3, Address offset: 0x3C 
.equ TIM15_CCR4, (TIM15 + 0x40) //!< TIM capture/compare register 4, Address offset: 0x40 
.equ TIM15_BDTR, (TIM15 + 0x44) //!< TIM break and dead-time register, Address offset: 0x44 
.equ TIM15_DCR, (TIM15 + 0x48) //!< TIM DMA control register, Address offset: 0x48 
.equ TIM15_DMAR, (TIM15 + 0x4C) //!< TIM DMA address for full transfer, Address offset: 0x4C 
.equ TIM15_OR, (TIM15 + 0x50) //!< TIM option register, Address offset: 0x50 
.equ TIM15_CCMR3, (TIM15 + 0x54) //!< TIM capture/compare mode register 3, Address offset: 0x54 
.equ TIM15_CCR5, (TIM15 + 0x58) //!< TIM capture/compare register5, Address offset: 0x58 
.equ TIM15_CCR6, (TIM15 + 0x5C) //!< TIM capture/compare register 4, Address offset: 0x5C 
.equ TIM16_CR1, (TIM16 + 0x00) //!< TIM control register 1, Address offset: 0x00 
.equ TIM16_CR2, (TIM16 + 0x04) //!< TIM control register 2, Address offset: 0x04 
.equ TIM16_SMCR, (TIM16 + 0x08) //!< TIM slave mode control register, Address offset: 0x08 
.equ TIM16_DIER, (TIM16 + 0x0C) //!< TIM DMA/interrupt enable register, Address offset: 0x0C 
.equ TIM16_SR, (TIM16 + 0x10) //!< TIM status register, Address offset: 0x10 
.equ TIM16_EGR, (TIM16 + 0x14) //!< TIM event generation register, Address offset: 0x14 
.equ TIM16_CCMR1, (TIM16 + 0x18) //!< TIM capture/compare mode register 1, Address offset: 0x18 
.equ TIM16_CCMR2, (TIM16 + 0x1C) //!< TIM capture/compare mode register 2, Address offset: 0x1C 
.equ TIM16_CCER, (TIM16 + 0x20) //!< TIM capture/compare enable register, Address offset: 0x20 
.equ TIM16_CNT, (TIM16 + 0x24) //!< TIM counter register, Address offset: 0x24 
.equ TIM16_PSC, (TIM16 + 0x28) //!< TIM prescaler, Address offset: 0x28 
.equ TIM16_ARR, (TIM16 + 0x2C) //!< TIM auto-reload register, Address offset: 0x2C 
.equ TIM16_RCR, (TIM16 + 0x30) //!< TIM repetition counter register, Address offset: 0x30 
.equ TIM16_CCR1, (TIM16 + 0x34) //!< TIM capture/compare register 1, Address offset: 0x34 
.equ TIM16_CCR2, (TIM16 + 0x38) //!< TIM capture/compare register 2, Address offset: 0x38 
.equ TIM16_CCR3, (TIM16 + 0x3C) //!< TIM capture/compare register 3, Address offset: 0x3C 
.equ TIM16_CCR4, (TIM16 + 0x40) //!< TIM capture/compare register 4, Address offset: 0x40 
.equ TIM16_BDTR, (TIM16 + 0x44) //!< TIM break and dead-time register, Address offset: 0x44 
.equ TIM16_DCR, (TIM16 + 0x48) //!< TIM DMA control register, Address offset: 0x48 
.equ TIM16_DMAR, (TIM16 + 0x4C) //!< TIM DMA address for full transfer, Address offset: 0x4C 
.equ TIM16_OR, (TIM16 + 0x50) //!< TIM option register, Address offset: 0x50 
.equ TIM16_CCMR3, (TIM16 + 0x54) //!< TIM capture/compare mode register 3, Address offset: 0x54 
.equ TIM16_CCR5, (TIM16 + 0x58) //!< TIM capture/compare register5, Address offset: 0x58 
.equ TIM16_CCR6, (TIM16 + 0x5C) //!< TIM capture/compare register 4, Address offset: 0x5C 
.equ TIM17_CR1, (TIM17 + 0x00) //!< TIM control register 1, Address offset: 0x00 
.equ TIM17_CR2, (TIM17 + 0x04) //!< TIM control register 2, Address offset: 0x04 
.equ TIM17_SMCR, (TIM17 + 0x08) //!< TIM slave mode control register, Address offset: 0x08 
.equ TIM17_DIER, (TIM17 + 0x0C) //!< TIM DMA/interrupt enable register, Address offset: 0x0C 
.equ TIM17_SR, (TIM17 + 0x10) //!< TIM status register, Address offset: 0x10 
.equ TIM17_EGR, (TIM17 + 0x14) //!< TIM event generation register, Address offset: 0x14 
.equ TIM17_CCMR1, (TIM17 + 0x18) //!< TIM capture/compare mode register 1, Address offset: 0x18 
.equ TIM17_CCMR2, (TIM17 + 0x1C) //!< TIM capture/compare mode register 2, Address offset: 0x1C 
.equ TIM17_CCER, (TIM17 + 0x20) //!< TIM capture/compare enable register, Address offset: 0x20 
.equ TIM17_CNT, (TIM17 + 0x24) //!< TIM counter register, Address offset: 0x24 
.equ TIM17_PSC, (TIM17 + 0x28) //!< TIM prescaler, Address offset: 0x28 
.equ TIM17_ARR, (TIM17 + 0x2C) //!< TIM auto-reload register, Address offset: 0x2C 
.equ TIM17_RCR, (TIM17 + 0x30) //!< TIM repetition counter register, Address offset: 0x30 
.equ TIM17_CCR1, (TIM17 + 0x34) //!< TIM capture/compare register 1, Address offset: 0x34 
.equ TIM17_CCR2, (TIM17 + 0x38) //!< TIM capture/compare register 2, Address offset: 0x38 
.equ TIM17_CCR3, (TIM17 + 0x3C) //!< TIM capture/compare register 3, Address offset: 0x3C 
.equ TIM17_CCR4, (TIM17 + 0x40) //!< TIM capture/compare register 4, Address offset: 0x40 
.equ TIM17_BDTR, (TIM17 + 0x44) //!< TIM break and dead-time register, Address offset: 0x44 
.equ TIM17_DCR, (TIM17 + 0x48) //!< TIM DMA control register, Address offset: 0x48 
.equ TIM17_DMAR, (TIM17 + 0x4C) //!< TIM DMA address for full transfer, Address offset: 0x4C 
.equ TIM17_OR, (TIM17 + 0x50) //!< TIM option register, Address offset: 0x50 
.equ TIM17_CCMR3, (TIM17 + 0x54) //!< TIM capture/compare mode register 3, Address offset: 0x54 
.equ TIM17_CCR5, (TIM17 + 0x58) //!< TIM capture/compare register5, Address offset: 0x58 
.equ TIM17_CCR6, (TIM17 + 0x5C) //!< TIM capture/compare register 4, Address offset: 0x5C 
.equ TIM20_CR1, (TIM20 + 0x00) //!< TIM control register 1, Address offset: 0x00 
.equ TIM20_CR2, (TIM20 + 0x04) //!< TIM control register 2, Address offset: 0x04 
.equ TIM20_SMCR, (TIM20 + 0x08) //!< TIM slave mode control register, Address offset: 0x08 
.equ TIM20_DIER, (TIM20 + 0x0C) //!< TIM DMA/interrupt enable register, Address offset: 0x0C 
.equ TIM20_SR, (TIM20 + 0x10) //!< TIM status register, Address offset: 0x10 
.equ TIM20_EGR, (TIM20 + 0x14) //!< TIM event generation register, Address offset: 0x14 
.equ TIM20_CCMR1, (TIM20 + 0x18) //!< TIM capture/compare mode register 1, Address offset: 0x18 
.equ TIM20_CCMR2, (TIM20 + 0x1C) //!< TIM capture/compare mode register 2, Address offset: 0x1C 
.equ TIM20_CCER, (TIM20 + 0x20) //!< TIM capture/compare enable register, Address offset: 0x20 
.equ TIM20_CNT, (TIM20 + 0x24) //!< TIM counter register, Address offset: 0x24 
.equ TIM20_PSC, (TIM20 + 0x28) //!< TIM prescaler, Address offset: 0x28 
.equ TIM20_ARR, (TIM20 + 0x2C) //!< TIM auto-reload register, Address offset: 0x2C 
.equ TIM20_RCR, (TIM20 + 0x30) //!< TIM repetition counter register, Address offset: 0x30 
.equ TIM20_CCR1, (TIM20 + 0x34) //!< TIM capture/compare register 1, Address offset: 0x34 
.equ TIM20_CCR2, (TIM20 + 0x38) //!< TIM capture/compare register 2, Address offset: 0x38 
.equ TIM20_CCR3, (TIM20 + 0x3C) //!< TIM capture/compare register 3, Address offset: 0x3C 
.equ TIM20_CCR4, (TIM20 + 0x40) //!< TIM capture/compare register 4, Address offset: 0x40 
.equ TIM20_BDTR, (TIM20 + 0x44) //!< TIM break and dead-time register, Address offset: 0x44 
.equ TIM20_DCR, (TIM20 + 0x48) //!< TIM DMA control register, Address offset: 0x48 
.equ TIM20_DMAR, (TIM20 + 0x4C) //!< TIM DMA address for full transfer, Address offset: 0x4C 
.equ TIM20_OR, (TIM20 + 0x50) //!< TIM option register, Address offset: 0x50 
.equ TIM20_CCMR3, (TIM20 + 0x54) //!< TIM capture/compare mode register 3, Address offset: 0x54 
.equ TIM20_CCR5, (TIM20 + 0x58) //!< TIM capture/compare register5, Address offset: 0x58 
.equ TIM20_CCR6, (TIM20 + 0x5C) //!< TIM capture/compare register 4, Address offset: 0x5C 
.equ RTC_TR, (RTC + 0x00) //!< RTC time register, Address offset: 0x00 
.equ RTC_DR, (RTC + 0x04) //!< RTC date register, Address offset: 0x04 
.equ RTC_CR, (RTC + 0x08) //!< RTC control register, Address offset: 0x08 
.equ RTC_ISR, (RTC + 0x0C) //!< RTC initialization and status register, Address offset: 0x0C 
.equ RTC_PRER, (RTC + 0x10) //!< RTC prescaler register, Address offset: 0x10 
.equ RTC_WUTR, (RTC + 0x14) //!< RTC wakeup timer register, Address offset: 0x14 
.equ RTC_ALRMAR, (RTC + 0x1C) //!< RTC alarm A register, Address offset: 0x1C 
.equ RTC_ALRMBR, (RTC + 0x20) //!< RTC alarm B register, Address offset: 0x20 
.equ RTC_WPR, (RTC + 0x24) //!< RTC write protection register, Address offset: 0x24 
.equ RTC_SSR, (RTC + 0x28) //!< RTC sub second register, Address offset: 0x28 
.equ RTC_SHIFTR, (RTC + 0x2C) //!< RTC shift control register, Address offset: 0x2C 
.equ RTC_TSTR, (RTC + 0x30) //!< RTC time stamp time register, Address offset: 0x30 
.equ RTC_TSDR, (RTC + 0x34) //!< RTC time stamp date register, Address offset: 0x34 
.equ RTC_TSSSR, (RTC + 0x38) //!< RTC time-stamp sub second register, Address offset: 0x38 
.equ RTC_CALR, (RTC + 0x3C) //!< RTC calibration register, Address offset: 0x3C 
.equ RTC_TAFCR, (RTC + 0x40) //!< RTC tamper and alternate function configuration register, Address offset: 0x40 
.equ RTC_ALRMASSR, (RTC + 0x44) //!< RTC alarm A sub second register, Address offset: 0x44 
.equ RTC_ALRMBSSR, (RTC + 0x48) //!< RTC alarm B sub second register, Address offset: 0x48 
.equ RTC_BKP0R, (RTC + 0x50) //!< RTC backup register 0, Address offset: 0x50 
.equ RTC_BKP1R, (RTC + 0x54) //!< RTC backup register 1, Address offset: 0x54 
.equ RTC_BKP2R, (RTC + 0x58) //!< RTC backup register 2, Address offset: 0x58 
.equ RTC_BKP3R, (RTC + 0x5C) //!< RTC backup register 3, Address offset: 0x5C 
.equ RTC_BKP4R, (RTC + 0x60) //!< RTC backup register 4, Address offset: 0x60 
.equ RTC_BKP5R, (RTC + 0x64) //!< RTC backup register 5, Address offset: 0x64 
.equ RTC_BKP6R, (RTC + 0x68) //!< RTC backup register 6, Address offset: 0x68 
.equ RTC_BKP7R, (RTC + 0x6C) //!< RTC backup register 7, Address offset: 0x6C 
.equ RTC_BKP8R, (RTC + 0x70) //!< RTC backup register 8, Address offset: 0x70 
.equ RTC_BKP9R, (RTC + 0x74) //!< RTC backup register 9, Address offset: 0x74 
.equ RTC_BKP10R, (RTC + 0x78) //!< RTC backup register 10, Address offset: 0x78 
.equ RTC_BKP11R, (RTC + 0x7C) //!< RTC backup register 11, Address offset: 0x7C 
.equ RTC_BKP12R, (RTC + 0x80) //!< RTC backup register 12, Address offset: 0x80 
.equ RTC_BKP13R, (RTC + 0x84) //!< RTC backup register 13, Address offset: 0x84 
.equ RTC_BKP14R, (RTC + 0x88) //!< RTC backup register 14, Address offset: 0x88 
.equ RTC_BKP15R, (RTC + 0x8C) //!< RTC backup register 15, Address offset: 0x8C 
.equ WWDG_CR, (WWDG + 0x00) //!< WWDG Control register, Address offset: 0x00 
.equ WWDG_CFR, (WWDG + 0x04) //!< WWDG Configuration register, Address offset: 0x04 
.equ WWDG_SR, (WWDG + 0x08) //!< WWDG Status register, Address offset: 0x08 
.equ IWDG_KR, (IWDG + 0x00) //!< IWDG Key register, Address offset: 0x00 
.equ IWDG_PR, (IWDG + 0x04) //!< IWDG Prescaler register, Address offset: 0x04 
.equ IWDG_RLR, (IWDG + 0x08) //!< IWDG Reload register, Address offset: 0x08 
.equ IWDG_SR, (IWDG + 0x0C) //!< IWDG Status register, Address offset: 0x0C 
.equ IWDG_WINR, (IWDG + 0x10) //!< IWDG Window register, Address offset: 0x10 
.equ I2S2_CR1, (I2S2ext + 0x00) //!< SPI Control register 1, Address offset: 0x00 
.equ I2S2_CR2, (I2S2ext + 0x04) //!< SPI Control register 2, Address offset: 0x04 
.equ I2S2_SR, (I2S2ext + 0x08) //!< SPI Status register, Address offset: 0x08 
.equ I2S2_DR, (I2S2ext + 0x0C) //!< SPI data register, Address offset: 0x0C 
.equ I2S2_CRCPR, (I2S2ext + 0x10) //!< SPI CRC polynomial register, Address offset: 0x10 
.equ I2S2_RXCRCR, (I2S2ext + 0x14) //!< SPI Rx CRC register, Address offset: 0x14 
.equ I2S2_TXCRCR, (I2S2ext + 0x18) //!< SPI Tx CRC register, Address offset: 0x18 
.equ I2S2_I2SCFGR, (I2S2ext + 0x1C) //!< SPI_I2S configuration register, Address offset: 0x1C 
.equ I2S2_I2SPR, (I2S2ext + 0x20) //!< SPI_I2S prescaler register, Address offset: 0x20 
.equ SPI1_CR1, (SPI1 + 0x00) //!< SPI Control register 1, Address offset: 0x00 
.equ SPI1_CR2, (SPI1 + 0x04) //!< SPI Control register 2, Address offset: 0x04 
.equ SPI1_SR, (SPI1 + 0x08) //!< SPI Status register, Address offset: 0x08 
.equ SPI1_DR, (SPI1 + 0x0C) //!< SPI data register, Address offset: 0x0C 
.equ SPI1_CRCPR, (SPI1 + 0x10) //!< SPI CRC polynomial register, Address offset: 0x10 
.equ SPI1_RXCRCR, (SPI1 + 0x14) //!< SPI Rx CRC register, Address offset: 0x14 
.equ SPI1_TXCRCR, (SPI1 + 0x18) //!< SPI Tx CRC register, Address offset: 0x18 
.equ SPI1_I2SCFGR, (SPI1 + 0x1C) //!< SPI_I2S configuration register, Address offset: 0x1C 
.equ SPI1_I2SPR, (SPI1 + 0x20) //!< SPI_I2S prescaler register, Address offset: 0x20 
.equ SPI2_CR1, (SPI2 + 0x00) //!< SPI Control register 1, Address offset: 0x00 
.equ SPI2_CR2, (SPI2 + 0x04) //!< SPI Control register 2, Address offset: 0x04 
.equ SPI2_SR, (SPI2 + 0x08) //!< SPI Status register, Address offset: 0x08 
.equ SPI2_DR, (SPI2 + 0x0C) //!< SPI data register, Address offset: 0x0C 
.equ SPI2_CRCPR, (SPI2 + 0x10) //!< SPI CRC polynomial register, Address offset: 0x10 
.equ SPI2_RXCRCR, (SPI2 + 0x14) //!< SPI Rx CRC register, Address offset: 0x14 
.equ SPI2_TXCRCR, (SPI2 + 0x18) //!< SPI Tx CRC register, Address offset: 0x18 
.equ SPI2_I2SCFGR, (SPI2 + 0x1C) //!< SPI_I2S configuration register, Address offset: 0x1C 
.equ SPI2_I2SPR, (SPI2 + 0x20) //!< SPI_I2S prescaler register, Address offset: 0x20 
.equ SPI3_CR1, (SPI3 + 0x00) //!< SPI Control register 1, Address offset: 0x00 
.equ SPI3_CR2, (SPI3 + 0x04) //!< SPI Control register 2, Address offset: 0x04 
.equ SPI3_SR, (SPI3 + 0x08) //!< SPI Status register, Address offset: 0x08 
.equ SPI3_DR, (SPI3 + 0x0C) //!< SPI data register, Address offset: 0x0C 
.equ SPI3_CRCPR, (SPI3 + 0x10) //!< SPI CRC polynomial register, Address offset: 0x10 
.equ SPI3_RXCRCR, (SPI3 + 0x14) //!< SPI Rx CRC register, Address offset: 0x14 
.equ SPI3_TXCRCR, (SPI3 + 0x18) //!< SPI Tx CRC register, Address offset: 0x18 
.equ SPI3_I2SCFGR, (SPI3 + 0x1C) //!< SPI_I2S configuration register, Address offset: 0x1C 
.equ SPI3_I2SPR, (SPI3 + 0x20) //!< SPI_I2S prescaler register, Address offset: 0x20 
.equ I2S3_CR1, (I2S3ext + 0x00) //!< SPI Control register 1, Address offset: 0x00 
.equ I2S3_CR2, (I2S3ext + 0x04) //!< SPI Control register 2, Address offset: 0x04 
.equ I2S3_SR, (I2S3ext + 0x08) //!< SPI Status register, Address offset: 0x08 
.equ I2S3_DR, (I2S3ext + 0x0C) //!< SPI data register, Address offset: 0x0C 
.equ I2S3_CRCPR, (I2S3ext + 0x10) //!< SPI CRC polynomial register, Address offset: 0x10 
.equ I2S3_RXCRCR, (I2S3ext + 0x14) //!< SPI Rx CRC register, Address offset: 0x14 
.equ I2S3_TXCRCR, (I2S3ext + 0x18) //!< SPI Tx CRC register, Address offset: 0x18 
.equ I2S3_I2SCFGR, (I2S3ext + 0x1C) //!< SPI_I2S configuration register, Address offset: 0x1C 
.equ I2S3_I2SPR, (I2S3ext + 0x20) //!< SPI_I2S prescaler register, Address offset: 0x20 
.equ SPI4_CR1, (SPI4 + 0x00) //!< SPI Control register 1, Address offset: 0x00 
.equ SPI4_CR2, (SPI4 + 0x04) //!< SPI Control register 2, Address offset: 0x04 
.equ SPI4_SR, (SPI4 + 0x08) //!< SPI Status register, Address offset: 0x08 
.equ SPI4_DR, (SPI4 + 0x0C) //!< SPI data register, Address offset: 0x0C 
.equ SPI4_CRCPR, (SPI4 + 0x10) //!< SPI CRC polynomial register, Address offset: 0x10 
.equ SPI4_RXCRCR, (SPI4 + 0x14) //!< SPI Rx CRC register, Address offset: 0x14 
.equ SPI4_TXCRCR, (SPI4 + 0x18) //!< SPI Tx CRC register, Address offset: 0x18 
.equ SPI4_I2SCFGR, (SPI4 + 0x1C) //!< SPI_I2S configuration register, Address offset: 0x1C 
.equ SPI4_I2SPR, (SPI4 + 0x20) //!< SPI_I2S prescaler register, Address offset: 0x20 
.equ USART1_CR1, (USART1 + 0x00) //!< USART Control register 1, Address offset: 0x00 
.equ USART1_CR2, (USART1 + 0x04) //!< USART Control register 2, Address offset: 0x04 
.equ USART1_CR3, (USART1 + 0x08) //!< USART Control register 3, Address offset: 0x08 
.equ USART1_BRR, (USART1 + 0x0C) //!< USART Baud rate register, Address offset: 0x0C 
.equ USART1_GTPR, (USART1 + 0x10) //!< USART Guard time and prescaler register, Address offset: 0x10 
.equ USART1_RTOR, (USART1 + 0x14) //!< USART Receiver Time Out register, Address offset: 0x14 
.equ USART1_RQR, (USART1 + 0x18) //!< USART Request register, Address offset: 0x18 
.equ USART1_ISR, (USART1 + 0x1C) //!< USART Interrupt and status register, Address offset: 0x1C 
.equ USART1_ICR, (USART1 + 0x20) //!< USART Interrupt flag Clear register, Address offset: 0x20 
.equ USART1_RDR, (USART1 + 0x24) //!< USART Receive Data register, Address offset: 0x24 
.equ USART1_TDR, (USART1 + 0x28) //!< USART Transmit Data register, Address offset: 0x28 
.equ USART2_CR1, (USART2 + 0x00) //!< USART Control register 1, Address offset: 0x00 
.equ USART2_CR2, (USART2 + 0x04) //!< USART Control register 2, Address offset: 0x04 
.equ USART2_CR3, (USART2 + 0x08) //!< USART Control register 3, Address offset: 0x08 
.equ USART2_BRR, (USART2 + 0x0C) //!< USART Baud rate register, Address offset: 0x0C 
.equ USART2_GTPR, (USART2 + 0x10) //!< USART Guard time and prescaler register, Address offset: 0x10 
.equ USART2_RTOR, (USART2 + 0x14) //!< USART Receiver Time Out register, Address offset: 0x14 
.equ USART2_RQR, (USART2 + 0x18) //!< USART Request register, Address offset: 0x18 
.equ USART2_ISR, (USART2 + 0x1C) //!< USART Interrupt and status register, Address offset: 0x1C 
.equ USART2_ICR, (USART2 + 0x20) //!< USART Interrupt flag Clear register, Address offset: 0x20 
.equ USART2_RDR, (USART2 + 0x24) //!< USART Receive Data register, Address offset: 0x24 
.equ USART2_TDR, (USART2 + 0x28) //!< USART Transmit Data register, Address offset: 0x28 
.equ USART3_CR1, (USART3 + 0x00) //!< USART Control register 1, Address offset: 0x00 
.equ USART3_CR2, (USART3 + 0x04) //!< USART Control register 2, Address offset: 0x04 
.equ USART3_CR3, (USART3 + 0x08) //!< USART Control register 3, Address offset: 0x08 
.equ USART3_BRR, (USART3 + 0x0C) //!< USART Baud rate register, Address offset: 0x0C 
.equ USART3_GTPR, (USART3 + 0x10) //!< USART Guard time and prescaler register, Address offset: 0x10 
.equ USART3_RTOR, (USART3 + 0x14) //!< USART Receiver Time Out register, Address offset: 0x14 
.equ USART3_RQR, (USART3 + 0x18) //!< USART Request register, Address offset: 0x18 
.equ USART3_ISR, (USART3 + 0x1C) //!< USART Interrupt and status register, Address offset: 0x1C 
.equ USART3_ICR, (USART3 + 0x20) //!< USART Interrupt flag Clear register, Address offset: 0x20 
.equ USART3_RDR, (USART3 + 0x24) //!< USART Receive Data register, Address offset: 0x24 
.equ USART3_TDR, (USART3 + 0x28) //!< USART Transmit Data register, Address offset: 0x28 
.equ UART4_CR1, (UART4 + 0x00) //!< USART Control register 1, Address offset: 0x00 
.equ UART4_CR2, (UART4 + 0x04) //!< USART Control register 2, Address offset: 0x04 
.equ UART4_CR3, (UART4 + 0x08) //!< USART Control register 3, Address offset: 0x08 
.equ UART4_BRR, (UART4 + 0x0C) //!< USART Baud rate register, Address offset: 0x0C 
.equ UART4_GTPR, (UART4 + 0x10) //!< USART Guard time and prescaler register, Address offset: 0x10 
.equ UART4_RTOR, (UART4 + 0x14) //!< USART Receiver Time Out register, Address offset: 0x14 
.equ UART4_RQR, (UART4 + 0x18) //!< USART Request register, Address offset: 0x18 
.equ UART4_ISR, (UART4 + 0x1C) //!< USART Interrupt and status register, Address offset: 0x1C 
.equ UART4_ICR, (UART4 + 0x20) //!< USART Interrupt flag Clear register, Address offset: 0x20 
.equ UART4_RDR, (UART4 + 0x24) //!< USART Receive Data register, Address offset: 0x24 
.equ UART4_TDR, (UART4 + 0x28) //!< USART Transmit Data register, Address offset: 0x28 
.equ UART5_CR1, (UART5 + 0x00) //!< USART Control register 1, Address offset: 0x00 
.equ UART5_CR2, (UART5 + 0x04) //!< USART Control register 2, Address offset: 0x04 
.equ UART5_CR3, (UART5 + 0x08) //!< USART Control register 3, Address offset: 0x08 
.equ UART5_BRR, (UART5 + 0x0C) //!< USART Baud rate register, Address offset: 0x0C 
.equ UART5_GTPR, (UART5 + 0x10) //!< USART Guard time and prescaler register, Address offset: 0x10 
.equ UART5_RTOR, (UART5 + 0x14) //!< USART Receiver Time Out register, Address offset: 0x14 
.equ UART5_RQR, (UART5 + 0x18) //!< USART Request register, Address offset: 0x18 
.equ UART5_ISR, (UART5 + 0x1C) //!< USART Interrupt and status register, Address offset: 0x1C 
.equ UART5_ICR, (UART5 + 0x20) //!< USART Interrupt flag Clear register, Address offset: 0x20 
.equ UART5_RDR, (UART5 + 0x24) //!< USART Receive Data register, Address offset: 0x24 
.equ UART5_TDR, (UART5 + 0x28) //!< USART Transmit Data register, Address offset: 0x28 
.equ I2C1_CR1, (I2C1 + 0x00) //!< I2C Control register 1, Address offset: 0x00 
.equ I2C1_CR2, (I2C1 + 0x04) //!< I2C Control register 2, Address offset: 0x04 
.equ I2C1_OAR1, (I2C1 + 0x08) //!< I2C Own address 1 register, Address offset: 0x08 
.equ I2C1_OAR2, (I2C1 + 0x0C) //!< I2C Own address 2 register, Address offset: 0x0C 
.equ I2C1_TIMINGR, (I2C1 + 0x10) //!< I2C Timing register, Address offset: 0x10 
.equ I2C1_TIMEOUTR, (I2C1 + 0x14) //!< I2C Timeout register, Address offset: 0x14 
.equ I2C1_ISR, (I2C1 + 0x18) //!< I2C Interrupt and status register, Address offset: 0x18 
.equ I2C1_ICR, (I2C1 + 0x1C) //!< I2C Interrupt clear register, Address offset: 0x1C 
.equ I2C1_PECR, (I2C1 + 0x20) //!< I2C PEC register, Address offset: 0x20 
.equ I2C1_RXDR, (I2C1 + 0x24) //!< I2C Receive data register, Address offset: 0x24 
.equ I2C1_TXDR, (I2C1 + 0x28) //!< I2C Transmit data register, Address offset: 0x28 
.equ I2C2_CR1, (I2C2 + 0x00) //!< I2C Control register 1, Address offset: 0x00 
.equ I2C2_CR2, (I2C2 + 0x04) //!< I2C Control register 2, Address offset: 0x04 
.equ I2C2_OAR1, (I2C2 + 0x08) //!< I2C Own address 1 register, Address offset: 0x08 
.equ I2C2_OAR2, (I2C2 + 0x0C) //!< I2C Own address 2 register, Address offset: 0x0C 
.equ I2C2_TIMINGR, (I2C2 + 0x10) //!< I2C Timing register, Address offset: 0x10 
.equ I2C2_TIMEOUTR, (I2C2 + 0x14) //!< I2C Timeout register, Address offset: 0x14 
.equ I2C2_ISR, (I2C2 + 0x18) //!< I2C Interrupt and status register, Address offset: 0x18 
.equ I2C2_ICR, (I2C2 + 0x1C) //!< I2C Interrupt clear register, Address offset: 0x1C 
.equ I2C2_PECR, (I2C2 + 0x20) //!< I2C PEC register, Address offset: 0x20 
.equ I2C2_RXDR, (I2C2 + 0x24) //!< I2C Receive data register, Address offset: 0x24 
.equ I2C2_TXDR, (I2C2 + 0x28) //!< I2C Transmit data register, Address offset: 0x28 
.equ I2C3_CR1, (I2C3 + 0x00) //!< I2C Control register 1, Address offset: 0x00 
.equ I2C3_CR2, (I2C3 + 0x04) //!< I2C Control register 2, Address offset: 0x04 
.equ I2C3_OAR1, (I2C3 + 0x08) //!< I2C Own address 1 register, Address offset: 0x08 
.equ I2C3_OAR2, (I2C3 + 0x0C) //!< I2C Own address 2 register, Address offset: 0x0C 
.equ I2C3_TIMINGR, (I2C3 + 0x10) //!< I2C Timing register, Address offset: 0x10 
.equ I2C3_TIMEOUTR, (I2C3 + 0x14) //!< I2C Timeout register, Address offset: 0x14 
.equ I2C3_ISR, (I2C3 + 0x18) //!< I2C Interrupt and status register, Address offset: 0x18 
.equ I2C3_ICR, (I2C3 + 0x1C) //!< I2C Interrupt clear register, Address offset: 0x1C 
.equ I2C3_PECR, (I2C3 + 0x20) //!< I2C PEC register, Address offset: 0x20 
.equ I2C3_RXDR, (I2C3 + 0x24) //!< I2C Receive data register, Address offset: 0x24 
.equ I2C3_TXDR, (I2C3 + 0x28) //!< I2C Transmit data register, Address offset: 0x28 
.equ PWR_CR, (PWR + 0x00) //!< PWR power control register, Address offset: 0x00 
.equ PWR_CSR, (PWR + 0x04) //!< PWR power control/status register, Address offset: 0x04 
.equ DAC1_CR, (DAC1 + 0x00) //!< DAC control register, Address offset: 0x00 
.equ DAC1_SWTRIGR, (DAC1 + 0x04) //!< DAC software trigger register, Address offset: 0x04 
.equ DAC1_DHR12R1, (DAC1 + 0x08) //!< DAC channel1 12-bit right-aligned data holding register, Address offset: 0x08 
.equ DAC1_DHR12L1, (DAC1 + 0x0C) //!< DAC channel1 12-bit left aligned data holding register, Address offset: 0x0C 
.equ DAC1_DHR8R1, (DAC1 + 0x10) //!< DAC channel1 8-bit right aligned data holding register, Address offset: 0x10 
.equ DAC1_DHR12R2, (DAC1 + 0x14) //!< DAC channel2 12-bit right aligned data holding register, Address offset: 0x14 
.equ DAC1_DHR12L2, (DAC1 + 0x18) //!< DAC channel2 12-bit left aligned data holding register, Address offset: 0x18 
.equ DAC1_DHR8R2, (DAC1 + 0x1C) //!< DAC channel2 8-bit right-aligned data holding register, Address offset: 0x1C 
.equ DAC1_DHR12RD, (DAC1 + 0x20) //!< Dual DAC 12-bit right-aligned data holding register, Address offset: 0x20 
.equ DAC1_DHR12LD, (DAC1 + 0x24) //!< DUAL DAC 12-bit left aligned data holding register, Address offset: 0x24 
.equ DAC1_DHR8RD, (DAC1 + 0x28) //!< DUAL DAC 8-bit right aligned data holding register, Address offset: 0x28 
.equ DAC1_DOR1, (DAC1 + 0x2C) //!< DAC channel1 data output register, Address offset: 0x2C 
.equ DAC1_DOR2, (DAC1 + 0x30) //!< DAC channel2 data output register, Address offset: 0x30 
.equ DAC1_SR, (DAC1 + 0x34) //!< DAC status register, Address offset: 0x34 
.equ COMP1_CSR, (COMP1 + 0x00) //!< COMP control and status register, Address offset: 0x00 
.equ COMP2_CSR, (COMP2 + 0x00) //!< COMP control and status register, Address offset: 0x00 
.equ COMP3_CSR, (COMP3 + 0x00) //!< COMP control and status register, Address offset: 0x00 
.equ COMP4_CSR, (COMP4 + 0x00) //!< COMP control and status register, Address offset: 0x00 
.equ COMP5_CSR, (COMP5 + 0x00) //!< COMP control and status register, Address offset: 0x00 
.equ COMP6_CSR, (COMP6 + 0x00) //!< COMP control and status register, Address offset: 0x00 
.equ COMP7_CSR, (COMP7 + 0x00) //!< COMP control and status register, Address offset: 0x00 
.equ COMP12_COMMON_CSR, (COMP12_COMMON + 0x00) //!< COMP control and status register, used for bits common to several COMP instances, Address offset: 0x00 
.equ COMP34_COMMON_CSR, (COMP34_COMMON + 0x00) //!< COMP control and status register, used for bits common to several COMP instances, Address offset: 0x00 
.equ COMP56_COMMON_CSR, (COMP56_COMMON + 0x00) //!< COMP control and status register, used for bits common to several COMP instances, Address offset: 0x00 
.equ OPAMP1_CSR, (OPAMP1 + 0x00) //!< OPAMP control and status register, Address offset: 0x00 
.equ OPAMP2_CSR, (OPAMP2 + 0x00) //!< OPAMP control and status register, Address offset: 0x00 
.equ OPAMP3_CSR, (OPAMP3 + 0x00) //!< OPAMP control and status register, Address offset: 0x00 
.equ OPAMP4_CSR, (OPAMP4 + 0x00) //!< OPAMP control and status register, Address offset: 0x00 
.equ SYSCFG_CFGR1, (SYSCFG + 0x00) //!< SYSCFG configuration register 1, Address offset: 0x00 
.equ SYSCFG_RCR, (SYSCFG + 0x04) //!< SYSCFG CCM SRAM protection register, Address offset: 0x04 
.equ SYSCFG_EXTICR, (SYSCFG + 0x08) //!< SYSCFG external interrupt configuration registers, Address offset: 0x14-0x08 
.equ SYSCFG_CFGR2, (SYSCFG + 0x18) //!< SYSCFG configuration register 2, Address offset: 0x18 
.equ SYSCFG_CFGR4, (SYSCFG + 0x48) //!< SYSCFG configuration register 4, Address offset: 0x48 
.equ EXTI_IMR, (EXTI + 0x00) //!<EXTI Interrupt mask register, Address offset: 0x00 
.equ EXTI_EMR, (EXTI + 0x04) //!<EXTI Event mask register, Address offset: 0x04 
.equ EXTI_RTSR, (EXTI + 0x08) //!<EXTI Rising trigger selection register , Address offset: 0x08 
.equ EXTI_FTSR, (EXTI + 0x0C) //!<EXTI Falling trigger selection register, Address offset: 0x0C 
.equ EXTI_SWIER, (EXTI + 0x10) //!<EXTI Software interrupt event register, Address offset: 0x10 
.equ EXTI_PR, (EXTI + 0x14) //!<EXTI Pending register, Address offset: 0x14 
.equ EXTI_IMR2, (EXTI + 0x20) //!< EXTI Interrupt mask register, Address offset: 0x20 
.equ EXTI_EMR2, (EXTI + 0x24) //!< EXTI Event mask register, Address offset: 0x24 
.equ EXTI_RTSR2, (EXTI + 0x28) //!< EXTI Rising trigger selection register, Address offset: 0x28 
.equ EXTI_FTSR2, (EXTI + 0x2C) //!< EXTI Falling trigger selection register, Address offset: 0x2C 
.equ EXTI_SWIER2, (EXTI + 0x30) //!< EXTI Software interrupt event register, Address offset: 0x30 
.equ EXTI_PR2, (EXTI + 0x34) //!< EXTI Pending register, Address offset: 0x34 
.equ DBGMCU_IDCODE, (DBGMCU + 0x00) //!< MCU device ID code, Address offset: 0x00 
.equ DBGMCU_CR, (DBGMCU + 0x04) //!< Debug MCU configuration register, Address offset: 0x04 
.equ DBGMCU_APB1FZ, (DBGMCU + 0x08) //!< Debug MCU APB1 freeze register, Address offset: 0x08 
.equ DBGMCU_APB2FZ, (DBGMCU + 0x0C) //!< Debug MCU APB2 freeze register, Address offset: 0x0C 
.equ DMA1_ISR, (DMA1 + 0x00) //!< DMA interrupt status register, Address offset: 0x00 
.equ DMA1_IFCR, (DMA1 + 0x04) //!< DMA interrupt flag clear register, Address offset: 0x04 
.equ DMA1_Channel1_CCR, (DMA1_Channel1 + 0x00) //!< DMA channel x configuration register Address offset: 0x00 
.equ DMA1_Channel1_CNDTR, (DMA1_Channel1 + 0x04) //!< DMA channel x number of data register Address offset: 0x04 
.equ DMA1_Channel1_CPAR, (DMA1_Channel1 + 0x08) //!< DMA channel x peripheral address register Address offset: 0x08 
.equ DMA1_Channel1_CMAR, (DMA1_Channel1 + 0x0C) //!< DMA channel x memory address register Address offset: 0x0C 
.equ DMA1_Channel2_CCR, (DMA1_Channel2 + 0x00) //!< DMA channel x configuration register Address offset: 0x00 
.equ DMA1_Channel2_CNDTR, (DMA1_Channel2 + 0x04) //!< DMA channel x number of data register Address offset: 0x04 
.equ DMA1_Channel2_CPAR, (DMA1_Channel2 + 0x08) //!< DMA channel x peripheral address register Address offset: 0x08 
.equ DMA1_Channel2_CMAR, (DMA1_Channel2 + 0x0C) //!< DMA channel x memory address register Address offset: 0x0C 
.equ DMA1_Channel3_CCR, (DMA1_Channel3 + 0x00) //!< DMA channel x configuration register Address offset: 0x00 
.equ DMA1_Channel3_CNDTR, (DMA1_Channel3 + 0x04) //!< DMA channel x number of data register Address offset: 0x04 
.equ DMA1_Channel3_CPAR, (DMA1_Channel3 + 0x08) //!< DMA channel x peripheral address register Address offset: 0x08 
.equ DMA1_Channel3_CMAR, (DMA1_Channel3 + 0x0C) //!< DMA channel x memory address register Address offset: 0x0C 
.equ DMA1_Channel4_CCR, (DMA1_Channel4 + 0x00) //!< DMA channel x configuration register Address offset: 0x00 
.equ DMA1_Channel4_CNDTR, (DMA1_Channel4 + 0x04) //!< DMA channel x number of data register Address offset: 0x04 
.equ DMA1_Channel4_CPAR, (DMA1_Channel4 + 0x08) //!< DMA channel x peripheral address register Address offset: 0x08 
.equ DMA1_Channel4_CMAR, (DMA1_Channel4 + 0x0C) //!< DMA channel x memory address register Address offset: 0x0C 
.equ DMA1_Channel5_CCR, (DMA1_Channel5 + 0x00) //!< DMA channel x configuration register Address offset: 0x00 
.equ DMA1_Channel5_CNDTR, (DMA1_Channel5 + 0x04) //!< DMA channel x number of data register Address offset: 0x04 
.equ DMA1_Channel5_CPAR, (DMA1_Channel5 + 0x08) //!< DMA channel x peripheral address register Address offset: 0x08 
.equ DMA1_Channel5_CMAR, (DMA1_Channel5 + 0x0C) //!< DMA channel x memory address register Address offset: 0x0C 
.equ DMA1_Channel6_CCR, (DMA1_Channel6 + 0x00) //!< DMA channel x configuration register Address offset: 0x00 
.equ DMA1_Channel6_CNDTR, (DMA1_Channel6 + 0x04) //!< DMA channel x number of data register Address offset: 0x04 
.equ DMA1_Channel6_CPAR, (DMA1_Channel6 + 0x08) //!< DMA channel x peripheral address register Address offset: 0x08 
.equ DMA1_Channel6_CMAR, (DMA1_Channel6 + 0x0C) //!< DMA channel x memory address register Address offset: 0x0C 
.equ DMA1_Channel7_CCR, (DMA1_Channel7 + 0x00) //!< DMA channel x configuration register Address offset: 0x00 
.equ DMA1_Channel7_CNDTR, (DMA1_Channel7 + 0x04) //!< DMA channel x number of data register Address offset: 0x04 
.equ DMA1_Channel7_CPAR, (DMA1_Channel7 + 0x08) //!< DMA channel x peripheral address register Address offset: 0x08 
.equ DMA1_Channel7_CMAR, (DMA1_Channel7 + 0x0C) //!< DMA channel x memory address register Address offset: 0x0C 
.equ DMA2_ISR, (DMA2 + 0x00) //!< DMA interrupt status register, Address offset: 0x00 
.equ DMA2_IFCR, (DMA2 + 0x04) //!< DMA interrupt flag clear register, Address offset: 0x04 
.equ DMA2_Channel1_CCR, (DMA2_Channel1 + 0x00) //!< DMA channel x configuration register Address offset: 0x00 
.equ DMA2_Channel1_CNDTR, (DMA2_Channel1 + 0x04) //!< DMA channel x number of data register Address offset: 0x04 
.equ DMA2_Channel1_CPAR, (DMA2_Channel1 + 0x08) //!< DMA channel x peripheral address register Address offset: 0x08 
.equ DMA2_Channel1_CMAR, (DMA2_Channel1 + 0x0C) //!< DMA channel x memory address register Address offset: 0x0C 
.equ DMA2_Channel2_CCR, (DMA2_Channel2 + 0x00) //!< DMA channel x configuration register Address offset: 0x00 
.equ DMA2_Channel2_CNDTR, (DMA2_Channel2 + 0x04) //!< DMA channel x number of data register Address offset: 0x04 
.equ DMA2_Channel2_CPAR, (DMA2_Channel2 + 0x08) //!< DMA channel x peripheral address register Address offset: 0x08 
.equ DMA2_Channel2_CMAR, (DMA2_Channel2 + 0x0C) //!< DMA channel x memory address register Address offset: 0x0C 
.equ DMA2_Channel3_CCR, (DMA2_Channel3 + 0x00) //!< DMA channel x configuration register Address offset: 0x00 
.equ DMA2_Channel3_CNDTR, (DMA2_Channel3 + 0x04) //!< DMA channel x number of data register Address offset: 0x04 
.equ DMA2_Channel3_CPAR, (DMA2_Channel3 + 0x08) //!< DMA channel x peripheral address register Address offset: 0x08 
.equ DMA2_Channel3_CMAR, (DMA2_Channel3 + 0x0C) //!< DMA channel x memory address register Address offset: 0x0C 
.equ DMA2_Channel4_CCR, (DMA2_Channel4 + 0x00) //!< DMA channel x configuration register Address offset: 0x00 
.equ DMA2_Channel4_CNDTR, (DMA2_Channel4 + 0x04) //!< DMA channel x number of data register Address offset: 0x04 
.equ DMA2_Channel4_CPAR, (DMA2_Channel4 + 0x08) //!< DMA channel x peripheral address register Address offset: 0x08 
.equ DMA2_Channel4_CMAR, (DMA2_Channel4 + 0x0C) //!< DMA channel x memory address register Address offset: 0x0C 
.equ DMA2_Channel5_CCR, (DMA2_Channel5 + 0x00) //!< DMA channel x configuration register Address offset: 0x00 
.equ DMA2_Channel5_CNDTR, (DMA2_Channel5 + 0x04) //!< DMA channel x number of data register Address offset: 0x04 
.equ DMA2_Channel5_CPAR, (DMA2_Channel5 + 0x08) //!< DMA channel x peripheral address register Address offset: 0x08 
.equ DMA2_Channel5_CMAR, (DMA2_Channel5 + 0x0C) //!< DMA channel x memory address register Address offset: 0x0C 
.equ RCC_CR, (RCC + 0x00) //!< RCC clock control register, Address offset: 0x00 
.equ RCC_CFGR, (RCC + 0x04) //!< RCC clock configuration register, Address offset: 0x04 
.equ RCC_CIR, (RCC + 0x08) //!< RCC clock interrupt register, Address offset: 0x08 
.equ RCC_APB2RSTR, (RCC + 0x0C) //!< RCC APB2 peripheral reset register, Address offset: 0x0C 
.equ RCC_APB1RSTR, (RCC + 0x10) //!< RCC APB1 peripheral reset register, Address offset: 0x10 
.equ RCC_AHBENR, (RCC + 0x14) //!< RCC AHB peripheral clock register, Address offset: 0x14 
.equ RCC_APB2ENR, (RCC + 0x18) //!< RCC APB2 peripheral clock enable register, Address offset: 0x18 
.equ RCC_APB1ENR, (RCC + 0x1C) //!< RCC APB1 peripheral clock enable register, Address offset: 0x1C 
.equ RCC_BDCR, (RCC + 0x20) //!< RCC Backup domain control register, Address offset: 0x20 
.equ RCC_CSR, (RCC + 0x24) //!< RCC clock control & status register, Address offset: 0x24 
.equ RCC_AHBRSTR, (RCC + 0x28) //!< RCC AHB peripheral reset register, Address offset: 0x28 
.equ RCC_CFGR2, (RCC + 0x2C) //!< RCC clock configuration register 2, Address offset: 0x2C 
.equ RCC_CFGR3, (RCC + 0x30) //!< RCC clock configuration register 3, Address offset: 0x30 
.equ FLASH_ACR, (FLASH + 0x00) //!< FLASH access control register, Address offset: 0x00 
.equ FLASH_KEYR, (FLASH + 0x04) //!< FLASH key register, Address offset: 0x04 
.equ FLASH_OPTKEYR, (FLASH + 0x08) //!< FLASH option key register, Address offset: 0x08 
.equ FLASH_SR, (FLASH + 0x0C) //!< FLASH status register, Address offset: 0x0C 
.equ FLASH_CR, (FLASH + 0x10) //!< FLASH control register, Address offset: 0x10 
.equ FLASH_AR, (FLASH + 0x14) //!< FLASH address register, Address offset: 0x14 
.equ FLASH_OBR, (FLASH + 0x1C) //!< FLASH Option byte register, Address offset: 0x1C 
.equ FLASH_WRPR, (FLASH + 0x20) //!< FLASH Write register, Address offset: 0x20 
.equ OB_RDP, (OB + 0x00) //!<FLASH option byte Read protection, Address offset: 0x00 
.equ OB_USER, (OB + 0x02) //!<FLASH option byte user options, Address offset: 0x02 
.equ OB_WRP0, (OB + 0x08) //!<FLASH option byte write protection 0, Address offset: 0x08 
.equ OB_WRP1, (OB + 0x0C) //!<FLASH option byte write protection 1, Address offset: 0x0C 
.equ OB_WRP2, (OB + 0x10) //!<FLASH option byte write protection 2, Address offset: 0x10 
.equ OB_WRP3, (OB + 0x12) //!<FLASH option byte write protection 3, Address offset: 0x12 
.equ CRC_DR, (CRC + 0x00) //!< CRC Data register, Address offset: 0x00 
.equ CRC_IDR, (CRC + 0x04) //!< CRC Independent data register, Address offset: 0x04 
.equ CRC_CR, (CRC + 0x08) //!< CRC Control register, Address offset: 0x08 
.equ CRC_INIT, (CRC + 0x10) //!< Initial CRC value register, Address offset: 0x10 
.equ CRC_POL, (CRC + 0x14) //!< CRC polynomial register, Address offset: 0x14 
.equ TSC_CR, (TSC + 0x00) //!< TSC control register, Address offset: 0x00 
.equ TSC_IER, (TSC + 0x04) //!< TSC interrupt enable register, Address offset: 0x04 
.equ TSC_ICR, (TSC + 0x08) //!< TSC interrupt clear register, Address offset: 0x08 
.equ TSC_ISR, (TSC + 0x0C) //!< TSC interrupt status register, Address offset: 0x0C 
.equ TSC_IOHCR, (TSC + 0x10) //!< TSC I/O hysteresis control register, Address offset: 0x10 
.equ TSC_IOASCR, (TSC + 0x18) //!< TSC I/O analog switch control register, Address offset: 0x18 
.equ TSC_IOSCR, (TSC + 0x20) //!< TSC I/O sampling control register, Address offset: 0x20 
.equ TSC_IOCCR, (TSC + 0x28) //!< TSC I/O channel control register, Address offset: 0x28 
.equ TSC_IOGCSR, (TSC + 0x30) //!< TSC I/O group control status register, Address offset: 0x30 
.equ TSC_IOGXCR, (TSC + 0x34) //!< TSC I/O group x counter register, Address offset: 0x34-50 
.equ GPIOA_MODER, (GPIOA + 0x00) //!< GPIO port mode register, Address offset: 0x00 
.equ GPIOA_OTYPER, (GPIOA + 0x04) //!< GPIO port output type register, Address offset: 0x04 
.equ GPIOA_OSPEEDR, (GPIOA + 0x08) //!< GPIO port output speed register, Address offset: 0x08 
.equ GPIOA_PUPDR, (GPIOA + 0x0C) //!< GPIO port pull-up/pull-down register, Address offset: 0x0C 
.equ GPIOA_IDR, (GPIOA + 0x10) //!< GPIO port input data register, Address offset: 0x10 
.equ GPIOA_ODR, (GPIOA + 0x14) //!< GPIO port output data register, Address offset: 0x14 
.equ GPIOA_BSRR, (GPIOA + 0x18) //!< GPIO port bit set/reset register, Address offset: 0x1A 
.equ GPIOA_LCKR, (GPIOA + 0x1C) //!< GPIO port configuration lock register, Address offset: 0x1C 
.equ GPIOA_AFRL, (GPIOA + 0x20) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOA_AFRH, (GPIOA + 0x24) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOA_BRR, (GPIOA + 0x28) //!< GPIO bit reset register, Address offset: 0x28 
.equ GPIOB_MODER, (GPIOB + 0x00) //!< GPIO port mode register, Address offset: 0x00 
.equ GPIOB_OTYPER, (GPIOB + 0x04) //!< GPIO port output type register, Address offset: 0x04 
.equ GPIOB_OSPEEDR, (GPIOB + 0x08) //!< GPIO port output speed register, Address offset: 0x08 
.equ GPIOB_PUPDR, (GPIOB + 0x0C) //!< GPIO port pull-up/pull-down register, Address offset: 0x0C 
.equ GPIOB_IDR, (GPIOB + 0x10) //!< GPIO port input data register, Address offset: 0x10 
.equ GPIOB_ODR, (GPIOB + 0x14) //!< GPIO port output data register, Address offset: 0x14 
.equ GPIOB_BSRR, (GPIOB + 0x18) //!< GPIO port bit set/reset register, Address offset: 0x1A 
.equ GPIOB_LCKR, (GPIOB + 0x1C) //!< GPIO port configuration lock register, Address offset: 0x1C 
.equ GPIOB_AFRL, (GPIOB + 0x20) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOB_AFRH, (GPIOB + 0x24) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOB_BRR, (GPIOB + 0x28) //!< GPIO bit reset register, Address offset: 0x28 
.equ GPIOC_MODER, (GPIOC + 0x00) //!< GPIO port mode register, Address offset: 0x00 
.equ GPIOC_OTYPER, (GPIOC + 0x04) //!< GPIO port output type register, Address offset: 0x04 
.equ GPIOC_OSPEEDR, (GPIOC + 0x08) //!< GPIO port output speed register, Address offset: 0x08 
.equ GPIOC_PUPDR, (GPIOC + 0x0C) //!< GPIO port pull-up/pull-down register, Address offset: 0x0C 
.equ GPIOC_IDR, (GPIOC + 0x10) //!< GPIO port input data register, Address offset: 0x10 
.equ GPIOC_ODR, (GPIOC + 0x14) //!< GPIO port output data register, Address offset: 0x14 
.equ GPIOC_BSRR, (GPIOC + 0x18) //!< GPIO port bit set/reset register, Address offset: 0x1A 
.equ GPIOC_LCKR, (GPIOC + 0x1C) //!< GPIO port configuration lock register, Address offset: 0x1C 
.equ GPIOC_AFRL, (GPIOC + 0x20) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOC_AFRH, (GPIOC + 0x24) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOC_BRR, (GPIOC + 0x28) //!< GPIO bit reset register, Address offset: 0x28 
.equ GPIOD_MODER, (GPIOD + 0x00) //!< GPIO port mode register, Address offset: 0x00 
.equ GPIOD_OTYPER, (GPIOD + 0x04) //!< GPIO port output type register, Address offset: 0x04 
.equ GPIOD_OSPEEDR, (GPIOD + 0x08) //!< GPIO port output speed register, Address offset: 0x08 
.equ GPIOD_PUPDR, (GPIOD + 0x0C) //!< GPIO port pull-up/pull-down register, Address offset: 0x0C 
.equ GPIOD_IDR, (GPIOD + 0x10) //!< GPIO port input data register, Address offset: 0x10 
.equ GPIOD_ODR, (GPIOD + 0x14) //!< GPIO port output data register, Address offset: 0x14 
.equ GPIOD_BSRR, (GPIOD + 0x18) //!< GPIO port bit set/reset register, Address offset: 0x1A 
.equ GPIOD_LCKR, (GPIOD + 0x1C) //!< GPIO port configuration lock register, Address offset: 0x1C 
.equ GPIOD_AFRL, (GPIOD + 0x20) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOD_AFRH, (GPIOD + 0x24) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOD_BRR, (GPIOD + 0x28) //!< GPIO bit reset register, Address offset: 0x28 
.equ GPIOE_MODER, (GPIOE + 0x00) //!< GPIO port mode register, Address offset: 0x00 
.equ GPIOE_OTYPER, (GPIOE + 0x04) //!< GPIO port output type register, Address offset: 0x04 
.equ GPIOE_OSPEEDR, (GPIOE + 0x08) //!< GPIO port output speed register, Address offset: 0x08 
.equ GPIOE_PUPDR, (GPIOE + 0x0C) //!< GPIO port pull-up/pull-down register, Address offset: 0x0C 
.equ GPIOE_IDR, (GPIOE + 0x10) //!< GPIO port input data register, Address offset: 0x10 
.equ GPIOE_ODR, (GPIOE + 0x14) //!< GPIO port output data register, Address offset: 0x14 
.equ GPIOE_BSRR, (GPIOE + 0x18) //!< GPIO port bit set/reset register, Address offset: 0x1A 
.equ GPIOE_LCKR, (GPIOE + 0x1C) //!< GPIO port configuration lock register, Address offset: 0x1C 
.equ GPIOE_AFRL, (GPIOE + 0x20) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOE_AFRH, (GPIOE + 0x24) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOE_BRR, (GPIOE + 0x28) //!< GPIO bit reset register, Address offset: 0x28 
.equ GPIOF_MODER, (GPIOF + 0x00) //!< GPIO port mode register, Address offset: 0x00 
.equ GPIOF_OTYPER, (GPIOF + 0x04) //!< GPIO port output type register, Address offset: 0x04 
.equ GPIOF_OSPEEDR, (GPIOF + 0x08) //!< GPIO port output speed register, Address offset: 0x08 
.equ GPIOF_PUPDR, (GPIOF + 0x0C) //!< GPIO port pull-up/pull-down register, Address offset: 0x0C 
.equ GPIOF_IDR, (GPIOF + 0x10) //!< GPIO port input data register, Address offset: 0x10 
.equ GPIOF_ODR, (GPIOF + 0x14) //!< GPIO port output data register, Address offset: 0x14 
.equ GPIOF_BSRR, (GPIOF + 0x18) //!< GPIO port bit set/reset register, Address offset: 0x1A 
.equ GPIOF_LCKR, (GPIOF + 0x1C) //!< GPIO port configuration lock register, Address offset: 0x1C 
.equ GPIOF_AFRL, (GPIOF + 0x20) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOF_AFRH, (GPIOF + 0x24) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOF_BRR, (GPIOF + 0x28) //!< GPIO bit reset register, Address offset: 0x28 
.equ GPIOG_MODER, (GPIOG + 0x00) //!< GPIO port mode register, Address offset: 0x00 
.equ GPIOG_OTYPER, (GPIOG + 0x04) //!< GPIO port output type register, Address offset: 0x04 
.equ GPIOG_OSPEEDR, (GPIOG + 0x08) //!< GPIO port output speed register, Address offset: 0x08 
.equ GPIOG_PUPDR, (GPIOG + 0x0C) //!< GPIO port pull-up/pull-down register, Address offset: 0x0C 
.equ GPIOG_IDR, (GPIOG + 0x10) //!< GPIO port input data register, Address offset: 0x10 
.equ GPIOG_ODR, (GPIOG + 0x14) //!< GPIO port output data register, Address offset: 0x14 
.equ GPIOG_BSRR, (GPIOG + 0x18) //!< GPIO port bit set/reset register, Address offset: 0x1A 
.equ GPIOG_LCKR, (GPIOG + 0x1C) //!< GPIO port configuration lock register, Address offset: 0x1C 
.equ GPIOG_AFRL, (GPIOG + 0x20) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOG_AFRH, (GPIOG + 0x24) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOG_BRR, (GPIOG + 0x28) //!< GPIO bit reset register, Address offset: 0x28 
.equ GPIOH_MODER, (GPIOH + 0x00) //!< GPIO port mode register, Address offset: 0x00 
.equ GPIOH_OTYPER, (GPIOH + 0x04) //!< GPIO port output type register, Address offset: 0x04 
.equ GPIOH_OSPEEDR, (GPIOH + 0x08) //!< GPIO port output speed register, Address offset: 0x08 
.equ GPIOH_PUPDR, (GPIOH + 0x0C) //!< GPIO port pull-up/pull-down register, Address offset: 0x0C 
.equ GPIOH_IDR, (GPIOH + 0x10) //!< GPIO port input data register, Address offset: 0x10 
.equ GPIOH_ODR, (GPIOH + 0x14) //!< GPIO port output data register, Address offset: 0x14 
.equ GPIOH_BSRR, (GPIOH + 0x18) //!< GPIO port bit set/reset register, Address offset: 0x1A 
.equ GPIOH_LCKR, (GPIOH + 0x1C) //!< GPIO port configuration lock register, Address offset: 0x1C 
.equ GPIOH_AFRL, (GPIOH + 0x20) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOH_AFRH, (GPIOH + 0x24) //!< GPIO alternate function registers, Address offset: 0x20-0x24 
.equ GPIOH_BRR, (GPIOH + 0x28) //!< GPIO bit reset register, Address offset: 0x28 
.equ ADC1_ISR, (ADC1 + 0x00) //!< ADC Interrupt and Status Register, Address offset: 0x00 
.equ ADC1_IER, (ADC1 + 0x04) //!< ADC Interrupt Enable Register, Address offset: 0x04 
.equ ADC1_CR, (ADC1 + 0x08) //!< ADC control register, Address offset: 0x08 
.equ ADC1_CFGR, (ADC1 + 0x0C) //!< ADC Configuration register, Address offset: 0x0C 
.equ ADC1_SMPR1, (ADC1 + 0x14) //!< ADC sample time register 1, Address offset: 0x14 
.equ ADC1_SMPR2, (ADC1 + 0x18) //!< ADC sample time register 2, Address offset: 0x18 
.equ ADC1_TR1, (ADC1 + 0x20) //!< ADC watchdog threshold register 1, Address offset: 0x20 
.equ ADC1_TR2, (ADC1 + 0x24) //!< ADC watchdog threshold register 2, Address offset: 0x24 
.equ ADC1_TR3, (ADC1 + 0x28) //!< ADC watchdog threshold register 3, Address offset: 0x28 
.equ ADC1_SQR1, (ADC1 + 0x30) //!< ADC regular sequence register 1, Address offset: 0x30 
.equ ADC1_SQR2, (ADC1 + 0x34) //!< ADC regular sequence register 2, Address offset: 0x34 
.equ ADC1_SQR3, (ADC1 + 0x38) //!< ADC regular sequence register 3, Address offset: 0x38 
.equ ADC1_SQR4, (ADC1 + 0x3C) //!< ADC regular sequence register 4, Address offset: 0x3C 
.equ ADC1_DR, (ADC1 + 0x40) //!< ADC regular data register, Address offset: 0x40 
.equ ADC1_JSQR, (ADC1 + 0x4C) //!< ADC injected sequence register, Address offset: 0x4C 
.equ ADC1_OFR1, (ADC1 + 0x60) //!< ADC offset register 1, Address offset: 0x60 
.equ ADC1_OFR2, (ADC1 + 0x64) //!< ADC offset register 2, Address offset: 0x64 
.equ ADC1_OFR3, (ADC1 + 0x68) //!< ADC offset register 3, Address offset: 0x68 
.equ ADC1_OFR4, (ADC1 + 0x6C) //!< ADC offset register 4, Address offset: 0x6C 
.equ ADC1_JDR1, (ADC1 + 0x80) //!< ADC injected data register 1, Address offset: 0x80 
.equ ADC1_JDR2, (ADC1 + 0x84) //!< ADC injected data register 2, Address offset: 0x84 
.equ ADC1_JDR3, (ADC1 + 0x88) //!< ADC injected data register 3, Address offset: 0x88 
.equ ADC1_JDR4, (ADC1 + 0x8C) //!< ADC injected data register 4, Address offset: 0x8C 
.equ ADC1_AWD2CR, (ADC1 + 0xA0) //!< ADC Analog Watchdog 2 Configuration Register, Address offset: 0xA0 
.equ ADC1_AWD3CR, (ADC1 + 0xA4) //!< ADC Analog Watchdog 3 Configuration Register, Address offset: 0xA4 
.equ ADC1_DIFSEL, (ADC1 + 0xB0) //!< ADC Differential Mode Selection Register, Address offset: 0xB0 
.equ ADC1_CALFACT, (ADC1 + 0xB4) //!< ADC Calibration Factors, Address offset: 0xB4 
.equ ADC2_ISR, (ADC2 + 0x00) //!< ADC Interrupt and Status Register, Address offset: 0x00 
.equ ADC2_IER, (ADC2 + 0x04) //!< ADC Interrupt Enable Register, Address offset: 0x04 
.equ ADC2_CR, (ADC2 + 0x08) //!< ADC control register, Address offset: 0x08 
.equ ADC2_CFGR, (ADC2 + 0x0C) //!< ADC Configuration register, Address offset: 0x0C 
.equ ADC2_SMPR1, (ADC2 + 0x14) //!< ADC sample time register 1, Address offset: 0x14 
.equ ADC2_SMPR2, (ADC2 + 0x18) //!< ADC sample time register 2, Address offset: 0x18 
.equ ADC2_TR1, (ADC2 + 0x20) //!< ADC watchdog threshold register 1, Address offset: 0x20 
.equ ADC2_TR2, (ADC2 + 0x24) //!< ADC watchdog threshold register 2, Address offset: 0x24 
.equ ADC2_TR3, (ADC2 + 0x28) //!< ADC watchdog threshold register 3, Address offset: 0x28 
.equ ADC2_SQR1, (ADC2 + 0x30) //!< ADC regular sequence register 1, Address offset: 0x30 
.equ ADC2_SQR2, (ADC2 + 0x34) //!< ADC regular sequence register 2, Address offset: 0x34 
.equ ADC2_SQR3, (ADC2 + 0x38) //!< ADC regular sequence register 3, Address offset: 0x38 
.equ ADC2_SQR4, (ADC2 + 0x3C) //!< ADC regular sequence register 4, Address offset: 0x3C 
.equ ADC2_DR, (ADC2 + 0x40) //!< ADC regular data register, Address offset: 0x40 
.equ ADC2_JSQR, (ADC2 + 0x4C) //!< ADC injected sequence register, Address offset: 0x4C 
.equ ADC2_OFR1, (ADC2 + 0x60) //!< ADC offset register 1, Address offset: 0x60 
.equ ADC2_OFR2, (ADC2 + 0x64) //!< ADC offset register 2, Address offset: 0x64 
.equ ADC2_OFR3, (ADC2 + 0x68) //!< ADC offset register 3, Address offset: 0x68 
.equ ADC2_OFR4, (ADC2 + 0x6C) //!< ADC offset register 4, Address offset: 0x6C 
.equ ADC2_JDR1, (ADC2 + 0x80) //!< ADC injected data register 1, Address offset: 0x80 
.equ ADC2_JDR2, (ADC2 + 0x84) //!< ADC injected data register 2, Address offset: 0x84 
.equ ADC2_JDR3, (ADC2 + 0x88) //!< ADC injected data register 3, Address offset: 0x88 
.equ ADC2_JDR4, (ADC2 + 0x8C) //!< ADC injected data register 4, Address offset: 0x8C 
.equ ADC2_AWD2CR, (ADC2 + 0xA0) //!< ADC Analog Watchdog 2 Configuration Register, Address offset: 0xA0 
.equ ADC2_AWD3CR, (ADC2 + 0xA4) //!< ADC Analog Watchdog 3 Configuration Register, Address offset: 0xA4 
.equ ADC2_DIFSEL, (ADC2 + 0xB0) //!< ADC Differential Mode Selection Register, Address offset: 0xB0 
.equ ADC2_CALFACT, (ADC2 + 0xB4) //!< ADC Calibration Factors, Address offset: 0xB4 
.equ ADC3_ISR, (ADC3 + 0x00) //!< ADC Interrupt and Status Register, Address offset: 0x00 
.equ ADC3_IER, (ADC3 + 0x04) //!< ADC Interrupt Enable Register, Address offset: 0x04 
.equ ADC3_CR, (ADC3 + 0x08) //!< ADC control register, Address offset: 0x08 
.equ ADC3_CFGR, (ADC3 + 0x0C) //!< ADC Configuration register, Address offset: 0x0C 
.equ ADC3_SMPR1, (ADC3 + 0x14) //!< ADC sample time register 1, Address offset: 0x14 
.equ ADC3_SMPR2, (ADC3 + 0x18) //!< ADC sample time register 2, Address offset: 0x18 
.equ ADC3_TR1, (ADC3 + 0x20) //!< ADC watchdog threshold register 1, Address offset: 0x20 
.equ ADC3_TR2, (ADC3 + 0x24) //!< ADC watchdog threshold register 2, Address offset: 0x24 
.equ ADC3_TR3, (ADC3 + 0x28) //!< ADC watchdog threshold register 3, Address offset: 0x28 
.equ ADC3_SQR1, (ADC3 + 0x30) //!< ADC regular sequence register 1, Address offset: 0x30 
.equ ADC3_SQR2, (ADC3 + 0x34) //!< ADC regular sequence register 2, Address offset: 0x34 
.equ ADC3_SQR3, (ADC3 + 0x38) //!< ADC regular sequence register 3, Address offset: 0x38 
.equ ADC3_SQR4, (ADC3 + 0x3C) //!< ADC regular sequence register 4, Address offset: 0x3C 
.equ ADC3_DR, (ADC3 + 0x40) //!< ADC regular data register, Address offset: 0x40 
.equ ADC3_JSQR, (ADC3 + 0x4C) //!< ADC injected sequence register, Address offset: 0x4C 
.equ ADC3_OFR1, (ADC3 + 0x60) //!< ADC offset register 1, Address offset: 0x60 
.equ ADC3_OFR2, (ADC3 + 0x64) //!< ADC offset register 2, Address offset: 0x64 
.equ ADC3_OFR3, (ADC3 + 0x68) //!< ADC offset register 3, Address offset: 0x68 
.equ ADC3_OFR4, (ADC3 + 0x6C) //!< ADC offset register 4, Address offset: 0x6C 
.equ ADC3_JDR1, (ADC3 + 0x80) //!< ADC injected data register 1, Address offset: 0x80 
.equ ADC3_JDR2, (ADC3 + 0x84) //!< ADC injected data register 2, Address offset: 0x84 
.equ ADC3_JDR3, (ADC3 + 0x88) //!< ADC injected data register 3, Address offset: 0x88 
.equ ADC3_JDR4, (ADC3 + 0x8C) //!< ADC injected data register 4, Address offset: 0x8C 
.equ ADC3_AWD2CR, (ADC3 + 0xA0) //!< ADC Analog Watchdog 2 Configuration Register, Address offset: 0xA0 
.equ ADC3_AWD3CR, (ADC3 + 0xA4) //!< ADC Analog Watchdog 3 Configuration Register, Address offset: 0xA4 
.equ ADC3_DIFSEL, (ADC3 + 0xB0) //!< ADC Differential Mode Selection Register, Address offset: 0xB0 
.equ ADC3_CALFACT, (ADC3 + 0xB4) //!< ADC Calibration Factors, Address offset: 0xB4 
.equ ADC4_ISR, (ADC4 + 0x00) //!< ADC Interrupt and Status Register, Address offset: 0x00 
.equ ADC4_IER, (ADC4 + 0x04) //!< ADC Interrupt Enable Register, Address offset: 0x04 
.equ ADC4_CR, (ADC4 + 0x08) //!< ADC control register, Address offset: 0x08 
.equ ADC4_CFGR, (ADC4 + 0x0C) //!< ADC Configuration register, Address offset: 0x0C 
.equ ADC4_SMPR1, (ADC4 + 0x14) //!< ADC sample time register 1, Address offset: 0x14 
.equ ADC4_SMPR2, (ADC4 + 0x18) //!< ADC sample time register 2, Address offset: 0x18 
.equ ADC4_TR1, (ADC4 + 0x20) //!< ADC watchdog threshold register 1, Address offset: 0x20 
.equ ADC4_TR2, (ADC4 + 0x24) //!< ADC watchdog threshold register 2, Address offset: 0x24 
.equ ADC4_TR3, (ADC4 + 0x28) //!< ADC watchdog threshold register 3, Address offset: 0x28 
.equ ADC4_SQR1, (ADC4 + 0x30) //!< ADC regular sequence register 1, Address offset: 0x30 
.equ ADC4_SQR2, (ADC4 + 0x34) //!< ADC regular sequence register 2, Address offset: 0x34 
.equ ADC4_SQR3, (ADC4 + 0x38) //!< ADC regular sequence register 3, Address offset: 0x38 
.equ ADC4_SQR4, (ADC4 + 0x3C) //!< ADC regular sequence register 4, Address offset: 0x3C 
.equ ADC4_DR, (ADC4 + 0x40) //!< ADC regular data register, Address offset: 0x40 
.equ ADC4_JSQR, (ADC4 + 0x4C) //!< ADC injected sequence register, Address offset: 0x4C 
.equ ADC4_OFR1, (ADC4 + 0x60) //!< ADC offset register 1, Address offset: 0x60 
.equ ADC4_OFR2, (ADC4 + 0x64) //!< ADC offset register 2, Address offset: 0x64 
.equ ADC4_OFR3, (ADC4 + 0x68) //!< ADC offset register 3, Address offset: 0x68 
.equ ADC4_OFR4, (ADC4 + 0x6C) //!< ADC offset register 4, Address offset: 0x6C 
.equ ADC4_JDR1, (ADC4 + 0x80) //!< ADC injected data register 1, Address offset: 0x80 
.equ ADC4_JDR2, (ADC4 + 0x84) //!< ADC injected data register 2, Address offset: 0x84 
.equ ADC4_JDR3, (ADC4 + 0x88) //!< ADC injected data register 3, Address offset: 0x88 
.equ ADC4_JDR4, (ADC4 + 0x8C) //!< ADC injected data register 4, Address offset: 0x8C 
.equ ADC4_AWD2CR, (ADC4 + 0xA0) //!< ADC Analog Watchdog 2 Configuration Register, Address offset: 0xA0 
.equ ADC4_AWD3CR, (ADC4 + 0xA4) //!< ADC Analog Watchdog 3 Configuration Register, Address offset: 0xA4 
.equ ADC4_DIFSEL, (ADC4 + 0xB0) //!< ADC Differential Mode Selection Register, Address offset: 0xB0 
.equ ADC4_CALFACT, (ADC4 + 0xB4) //!< ADC Calibration Factors, Address offset: 0xB4 
.equ ADC12_COMMON_CSR, (ADC12_COMMON + 0x00) //!< ADC Common status register, Address offset: ADC1/3 base address + 0x300 
.equ ADC12_COMMON_CCR, (ADC12_COMMON + 0x08) //!< ADC common control register, Address offset: ADC1/3 base address + 0x308 
.equ ADC12_COMMON_CDR, (ADC12_COMMON + 0x0C) //!< ADC common regular data register for dual Address offset: ADC1/3 base address + 0x30C 
.equ ADC34_COMMON_CSR, (ADC34_COMMON + 0x00) //!< ADC Common status register, Address offset: ADC1/3 base address + 0x300 
.equ ADC34_COMMON_CCR, (ADC34_COMMON + 0x08) //!< ADC common control register, Address offset: ADC1/3 base address + 0x308 
.equ ADC34_COMMON_CDR, (ADC34_COMMON + 0x0C) //!< ADC common regular data register for dual Address offset: ADC1/3 base address + 0x30C 
.equ USB_EP0R, (USB + 0x00) //!< USB Endpoint 0 register, Address offset: 0x00 
.equ USB_EP1R, (USB + 0x04) //!< USB Endpoint 1 register, Address offset: 0x04 
.equ USB_EP2R, (USB + 0x08) //!< USB Endpoint 2 register, Address offset: 0x08 
.equ USB_EP3R, (USB + 0x0C) //!< USB Endpoint 3 register, Address offset: 0x0C 
.equ USB_EP4R, (USB + 0x10) //!< USB Endpoint 4 register, Address offset: 0x10 
.equ USB_EP5R, (USB + 0x14) //!< USB Endpoint 5 register, Address offset: 0x14 
.equ USB_EP6R, (USB + 0x18) //!< USB Endpoint 6 register, Address offset: 0x18 
.equ USB_EP7R, (USB + 0x1C) //!< USB Endpoint 7 register, Address offset: 0x1C 
.equ USB_CNTR, (USB + 0x40) //!< Control register, Address offset: 0x40 
.equ USB_ISTR, (USB + 0x44) //!< Interrupt status register, Address offset: 0x44 
.equ USB_FNR, (USB + 0x48) //!< Frame number register, Address offset: 0x48 
.equ USB_DADDR, (USB + 0x4C) //!< Device address register, Address offset: 0x4C 
.equ USB_BTABLE, (USB + 0x50) //!< Buffer Table address register, Address offset: 0x50 
.equ USB_LPMCSR, (USB + 0x54) //!< LPM Control and Status register, Address offset: 0x54 
//****************************************************************************
//                         Peripheral Registers_Bits_Definition
//****************************************************************************
//****************************************************************************
//
//                        Analog to Digital Converter SAR (ADC)
//
//****************************************************************************
//*******************  Bit definition for ADC_ISR register  *******************
.equ ADC_ISR_ADRDY_Pos, (0) 
.equ ADC_ISR_ADRDY_Msk, (0x1 << ADC_ISR_ADRDY_Pos) //!< 0x00000001 
.equ ADC_ISR_ADRDY, ADC_ISR_ADRDY_Msk //!< ADC ready flag 
.equ ADC_ISR_EOSMP_Pos, (1) 
.equ ADC_ISR_EOSMP_Msk, (0x1 << ADC_ISR_EOSMP_Pos) //!< 0x00000002 
.equ ADC_ISR_EOSMP, ADC_ISR_EOSMP_Msk //!< ADC group regular end of sampling flag 
.equ ADC_ISR_EOC_Pos, (2) 
.equ ADC_ISR_EOC_Msk, (0x1 << ADC_ISR_EOC_Pos) //!< 0x00000004 
.equ ADC_ISR_EOC, ADC_ISR_EOC_Msk //!< ADC group regular end of unitary conversion flag 
.equ ADC_ISR_EOS_Pos, (3) 
.equ ADC_ISR_EOS_Msk, (0x1 << ADC_ISR_EOS_Pos) //!< 0x00000008 
.equ ADC_ISR_EOS, ADC_ISR_EOS_Msk //!< ADC group regular end of sequence conversions flag 
.equ ADC_ISR_OVR_Pos, (4) 
.equ ADC_ISR_OVR_Msk, (0x1 << ADC_ISR_OVR_Pos) //!< 0x00000010 
.equ ADC_ISR_OVR, ADC_ISR_OVR_Msk //!< ADC group regular overrun flag 
.equ ADC_ISR_JEOC_Pos, (5) 
.equ ADC_ISR_JEOC_Msk, (0x1 << ADC_ISR_JEOC_Pos) //!< 0x00000020 
.equ ADC_ISR_JEOC, ADC_ISR_JEOC_Msk //!< ADC group injected end of unitary conversion flag 
.equ ADC_ISR_JEOS_Pos, (6) 
.equ ADC_ISR_JEOS_Msk, (0x1 << ADC_ISR_JEOS_Pos) //!< 0x00000040 
.equ ADC_ISR_JEOS, ADC_ISR_JEOS_Msk //!< ADC group injected end of sequence conversions flag 
.equ ADC_ISR_AWD1_Pos, (7) 
.equ ADC_ISR_AWD1_Msk, (0x1 << ADC_ISR_AWD1_Pos) //!< 0x00000080 
.equ ADC_ISR_AWD1, ADC_ISR_AWD1_Msk //!< ADC analog watchdog 1 flag 
.equ ADC_ISR_AWD2_Pos, (8) 
.equ ADC_ISR_AWD2_Msk, (0x1 << ADC_ISR_AWD2_Pos) //!< 0x00000100 
.equ ADC_ISR_AWD2, ADC_ISR_AWD2_Msk //!< ADC analog watchdog 2 flag 
.equ ADC_ISR_AWD3_Pos, (9) 
.equ ADC_ISR_AWD3_Msk, (0x1 << ADC_ISR_AWD3_Pos) //!< 0x00000200 
.equ ADC_ISR_AWD3, ADC_ISR_AWD3_Msk //!< ADC analog watchdog 3 flag 
.equ ADC_ISR_JQOVF_Pos, (10) 
.equ ADC_ISR_JQOVF_Msk, (0x1 << ADC_ISR_JQOVF_Pos) //!< 0x00000400 
.equ ADC_ISR_JQOVF, ADC_ISR_JQOVF_Msk //!< ADC group injected contexts queue overflow flag 
// Legacy defines
.equ ADC_ISR_ADRD, (ADC_ISR_ADRDY) 
//*******************  Bit definition for ADC_IER register  *******************
.equ ADC_IER_ADRDYIE_Pos, (0) 
.equ ADC_IER_ADRDYIE_Msk, (0x1 << ADC_IER_ADRDYIE_Pos) //!< 0x00000001 
.equ ADC_IER_ADRDYIE, ADC_IER_ADRDYIE_Msk //!< ADC ready interrupt 
.equ ADC_IER_EOSMPIE_Pos, (1) 
.equ ADC_IER_EOSMPIE_Msk, (0x1 << ADC_IER_EOSMPIE_Pos) //!< 0x00000002 
.equ ADC_IER_EOSMPIE, ADC_IER_EOSMPIE_Msk //!< ADC group regular end of sampling interrupt 
.equ ADC_IER_EOCIE_Pos, (2) 
.equ ADC_IER_EOCIE_Msk, (0x1 << ADC_IER_EOCIE_Pos) //!< 0x00000004 
.equ ADC_IER_EOCIE, ADC_IER_EOCIE_Msk //!< ADC group regular end of unitary conversion interrupt 
.equ ADC_IER_EOSIE_Pos, (3) 
.equ ADC_IER_EOSIE_Msk, (0x1 << ADC_IER_EOSIE_Pos) //!< 0x00000008 
.equ ADC_IER_EOSIE, ADC_IER_EOSIE_Msk //!< ADC group regular end of sequence conversions interrupt 
.equ ADC_IER_OVRIE_Pos, (4) 
.equ ADC_IER_OVRIE_Msk, (0x1 << ADC_IER_OVRIE_Pos) //!< 0x00000010 
.equ ADC_IER_OVRIE, ADC_IER_OVRIE_Msk //!< ADC group regular overrun interrupt 
.equ ADC_IER_JEOCIE_Pos, (5) 
.equ ADC_IER_JEOCIE_Msk, (0x1 << ADC_IER_JEOCIE_Pos) //!< 0x00000020 
.equ ADC_IER_JEOCIE, ADC_IER_JEOCIE_Msk //!< ADC group injected end of unitary conversion interrupt 
.equ ADC_IER_JEOSIE_Pos, (6) 
.equ ADC_IER_JEOSIE_Msk, (0x1 << ADC_IER_JEOSIE_Pos) //!< 0x00000040 
.equ ADC_IER_JEOSIE, ADC_IER_JEOSIE_Msk //!< ADC group injected end of sequence conversions interrupt 
.equ ADC_IER_AWD1IE_Pos, (7) 
.equ ADC_IER_AWD1IE_Msk, (0x1 << ADC_IER_AWD1IE_Pos) //!< 0x00000080 
.equ ADC_IER_AWD1IE, ADC_IER_AWD1IE_Msk //!< ADC analog watchdog 1 interrupt 
.equ ADC_IER_AWD2IE_Pos, (8) 
.equ ADC_IER_AWD2IE_Msk, (0x1 << ADC_IER_AWD2IE_Pos) //!< 0x00000100 
.equ ADC_IER_AWD2IE, ADC_IER_AWD2IE_Msk //!< ADC analog watchdog 2 interrupt 
.equ ADC_IER_AWD3IE_Pos, (9) 
.equ ADC_IER_AWD3IE_Msk, (0x1 << ADC_IER_AWD3IE_Pos) //!< 0x00000200 
.equ ADC_IER_AWD3IE, ADC_IER_AWD3IE_Msk //!< ADC analog watchdog 3 interrupt 
.equ ADC_IER_JQOVFIE_Pos, (10) 
.equ ADC_IER_JQOVFIE_Msk, (0x1 << ADC_IER_JQOVFIE_Pos) //!< 0x00000400 
.equ ADC_IER_JQOVFIE, ADC_IER_JQOVFIE_Msk //!< ADC group injected contexts queue overflow interrupt 
// Legacy defines
.equ ADC_IER_RDY, (ADC_IER_ADRDYIE) 
.equ ADC_IER_EOSMP, (ADC_IER_EOSMPIE) 
.equ ADC_IER_EOC, (ADC_IER_EOCIE) 
.equ ADC_IER_EOS, (ADC_IER_EOSIE) 
.equ ADC_IER_OVR, (ADC_IER_OVRIE) 
.equ ADC_IER_JEOC, (ADC_IER_JEOCIE) 
.equ ADC_IER_JEOS, (ADC_IER_JEOSIE) 
.equ ADC_IER_AWD1, (ADC_IER_AWD1IE) 
.equ ADC_IER_AWD2, (ADC_IER_AWD2IE) 
.equ ADC_IER_AWD3, (ADC_IER_AWD3IE) 
.equ ADC_IER_JQOVF, (ADC_IER_JQOVFIE) 
//*******************  Bit definition for ADC_CR register  *******************
.equ ADC_CR_ADEN_Pos, (0) 
.equ ADC_CR_ADEN_Msk, (0x1 << ADC_CR_ADEN_Pos) //!< 0x00000001 
.equ ADC_CR_ADEN, ADC_CR_ADEN_Msk //!< ADC enable 
.equ ADC_CR_ADDIS_Pos, (1) 
.equ ADC_CR_ADDIS_Msk, (0x1 << ADC_CR_ADDIS_Pos) //!< 0x00000002 
.equ ADC_CR_ADDIS, ADC_CR_ADDIS_Msk //!< ADC disable 
.equ ADC_CR_ADSTART_Pos, (2) 
.equ ADC_CR_ADSTART_Msk, (0x1 << ADC_CR_ADSTART_Pos) //!< 0x00000004 
.equ ADC_CR_ADSTART, ADC_CR_ADSTART_Msk //!< ADC group regular conversion start 
.equ ADC_CR_JADSTART_Pos, (3) 
.equ ADC_CR_JADSTART_Msk, (0x1 << ADC_CR_JADSTART_Pos) //!< 0x00000008 
.equ ADC_CR_JADSTART, ADC_CR_JADSTART_Msk //!< ADC group injected conversion start 
.equ ADC_CR_ADSTP_Pos, (4) 
.equ ADC_CR_ADSTP_Msk, (0x1 << ADC_CR_ADSTP_Pos) //!< 0x00000010 
.equ ADC_CR_ADSTP, ADC_CR_ADSTP_Msk //!< ADC group regular conversion stop 
.equ ADC_CR_JADSTP_Pos, (5) 
.equ ADC_CR_JADSTP_Msk, (0x1 << ADC_CR_JADSTP_Pos) //!< 0x00000020 
.equ ADC_CR_JADSTP, ADC_CR_JADSTP_Msk //!< ADC group injected conversion stop 
.equ ADC_CR_ADVREGEN_Pos, (28) 
.equ ADC_CR_ADVREGEN_Msk, (0x3 << ADC_CR_ADVREGEN_Pos) //!< 0x30000000 
.equ ADC_CR_ADVREGEN, ADC_CR_ADVREGEN_Msk //!< ADC voltage regulator enable 
.equ ADC_CR_ADVREGEN_0, (0x1 << ADC_CR_ADVREGEN_Pos) //!< 0x10000000 
.equ ADC_CR_ADVREGEN_1, (0x2 << ADC_CR_ADVREGEN_Pos) //!< 0x20000000 
.equ ADC_CR_ADCALDIF_Pos, (30) 
.equ ADC_CR_ADCALDIF_Msk, (0x1 << ADC_CR_ADCALDIF_Pos) //!< 0x40000000 
.equ ADC_CR_ADCALDIF, ADC_CR_ADCALDIF_Msk //!< ADC differential mode for calibration 
.equ ADC_CR_ADCAL_Pos, (31) 
.equ ADC_CR_ADCAL_Msk, (0x1 << ADC_CR_ADCAL_Pos) //!< 0x80000000 
.equ ADC_CR_ADCAL, ADC_CR_ADCAL_Msk //!< ADC calibration 
//*******************  Bit definition for ADC_CFGR register  *****************
.equ ADC_CFGR_DMAEN_Pos, (0) 
.equ ADC_CFGR_DMAEN_Msk, (0x1 << ADC_CFGR_DMAEN_Pos) //!< 0x00000001 
.equ ADC_CFGR_DMAEN, ADC_CFGR_DMAEN_Msk //!< ADC DMA enable 
.equ ADC_CFGR_DMACFG_Pos, (1) 
.equ ADC_CFGR_DMACFG_Msk, (0x1 << ADC_CFGR_DMACFG_Pos) //!< 0x00000002 
.equ ADC_CFGR_DMACFG, ADC_CFGR_DMACFG_Msk //!< ADC DMA configuration 
.equ ADC_CFGR_RES_Pos, (3) 
.equ ADC_CFGR_RES_Msk, (0x3 << ADC_CFGR_RES_Pos) //!< 0x00000018 
.equ ADC_CFGR_RES, ADC_CFGR_RES_Msk //!< ADC data resolution 
.equ ADC_CFGR_RES_0, (0x1 << ADC_CFGR_RES_Pos) //!< 0x00000008 
.equ ADC_CFGR_RES_1, (0x2 << ADC_CFGR_RES_Pos) //!< 0x00000010 
.equ ADC_CFGR_ALIGN_Pos, (5) 
.equ ADC_CFGR_ALIGN_Msk, (0x1 << ADC_CFGR_ALIGN_Pos) //!< 0x00000020 
.equ ADC_CFGR_ALIGN, ADC_CFGR_ALIGN_Msk //!< ADC data alignement 
.equ ADC_CFGR_EXTSEL_Pos, (6) 
.equ ADC_CFGR_EXTSEL_Msk, (0xF << ADC_CFGR_EXTSEL_Pos) //!< 0x000003C0 
.equ ADC_CFGR_EXTSEL, ADC_CFGR_EXTSEL_Msk //!< ADC group regular external trigger source 
.equ ADC_CFGR_EXTSEL_0, (0x1 << ADC_CFGR_EXTSEL_Pos) //!< 0x00000040 
.equ ADC_CFGR_EXTSEL_1, (0x2 << ADC_CFGR_EXTSEL_Pos) //!< 0x00000080 
.equ ADC_CFGR_EXTSEL_2, (0x4 << ADC_CFGR_EXTSEL_Pos) //!< 0x00000100 
.equ ADC_CFGR_EXTSEL_3, (0x8 << ADC_CFGR_EXTSEL_Pos) //!< 0x00000200 
.equ ADC_CFGR_EXTEN_Pos, (10) 
.equ ADC_CFGR_EXTEN_Msk, (0x3 << ADC_CFGR_EXTEN_Pos) //!< 0x00000C00 
.equ ADC_CFGR_EXTEN, ADC_CFGR_EXTEN_Msk //!< ADC group regular external trigger polarity 
.equ ADC_CFGR_EXTEN_0, (0x1 << ADC_CFGR_EXTEN_Pos) //!< 0x00000400 
.equ ADC_CFGR_EXTEN_1, (0x2 << ADC_CFGR_EXTEN_Pos) //!< 0x00000800 
.equ ADC_CFGR_OVRMOD_Pos, (12) 
.equ ADC_CFGR_OVRMOD_Msk, (0x1 << ADC_CFGR_OVRMOD_Pos) //!< 0x00001000 
.equ ADC_CFGR_OVRMOD, ADC_CFGR_OVRMOD_Msk //!< ADC group regular overrun configuration 
.equ ADC_CFGR_CONT_Pos, (13) 
.equ ADC_CFGR_CONT_Msk, (0x1 << ADC_CFGR_CONT_Pos) //!< 0x00002000 
.equ ADC_CFGR_CONT, ADC_CFGR_CONT_Msk //!< ADC group regular continuous conversion mode 
.equ ADC_CFGR_AUTDLY_Pos, (14) 
.equ ADC_CFGR_AUTDLY_Msk, (0x1 << ADC_CFGR_AUTDLY_Pos) //!< 0x00004000 
.equ ADC_CFGR_AUTDLY, ADC_CFGR_AUTDLY_Msk //!< ADC low power auto wait 
.equ ADC_CFGR_DISCEN_Pos, (16) 
.equ ADC_CFGR_DISCEN_Msk, (0x1 << ADC_CFGR_DISCEN_Pos) //!< 0x00010000 
.equ ADC_CFGR_DISCEN, ADC_CFGR_DISCEN_Msk //!< ADC group regular sequencer discontinuous mode 
.equ ADC_CFGR_DISCNUM_Pos, (17) 
.equ ADC_CFGR_DISCNUM_Msk, (0x7 << ADC_CFGR_DISCNUM_Pos) //!< 0x000E0000 
.equ ADC_CFGR_DISCNUM, ADC_CFGR_DISCNUM_Msk //!< ADC Discontinuous mode channel count 
.equ ADC_CFGR_DISCNUM_0, (0x1 << ADC_CFGR_DISCNUM_Pos) //!< 0x00020000 
.equ ADC_CFGR_DISCNUM_1, (0x2 << ADC_CFGR_DISCNUM_Pos) //!< 0x00040000 
.equ ADC_CFGR_DISCNUM_2, (0x4 << ADC_CFGR_DISCNUM_Pos) //!< 0x00080000 
.equ ADC_CFGR_JDISCEN_Pos, (20) 
.equ ADC_CFGR_JDISCEN_Msk, (0x1 << ADC_CFGR_JDISCEN_Pos) //!< 0x00100000 
.equ ADC_CFGR_JDISCEN, ADC_CFGR_JDISCEN_Msk //!< ADC Discontinuous mode on injected channels 
.equ ADC_CFGR_JQM_Pos, (21) 
.equ ADC_CFGR_JQM_Msk, (0x1 << ADC_CFGR_JQM_Pos) //!< 0x00200000 
.equ ADC_CFGR_JQM, ADC_CFGR_JQM_Msk //!< ADC group injected contexts queue mode 
.equ ADC_CFGR_AWD1SGL_Pos, (22) 
.equ ADC_CFGR_AWD1SGL_Msk, (0x1 << ADC_CFGR_AWD1SGL_Pos) //!< 0x00400000 
.equ ADC_CFGR_AWD1SGL, ADC_CFGR_AWD1SGL_Msk //!< ADC analog watchdog 1 monitoring a single channel or all channels 
.equ ADC_CFGR_AWD1EN_Pos, (23) 
.equ ADC_CFGR_AWD1EN_Msk, (0x1 << ADC_CFGR_AWD1EN_Pos) //!< 0x00800000 
.equ ADC_CFGR_AWD1EN, ADC_CFGR_AWD1EN_Msk //!< ADC analog watchdog 1 enable on scope ADC group regular 
.equ ADC_CFGR_JAWD1EN_Pos, (24) 
.equ ADC_CFGR_JAWD1EN_Msk, (0x1 << ADC_CFGR_JAWD1EN_Pos) //!< 0x01000000 
.equ ADC_CFGR_JAWD1EN, ADC_CFGR_JAWD1EN_Msk //!< ADC analog watchdog 1 enable on scope ADC group injected 
.equ ADC_CFGR_JAUTO_Pos, (25) 
.equ ADC_CFGR_JAUTO_Msk, (0x1 << ADC_CFGR_JAUTO_Pos) //!< 0x02000000 
.equ ADC_CFGR_JAUTO, ADC_CFGR_JAUTO_Msk //!< ADC group injected automatic trigger mode 
.equ ADC_CFGR_AWD1CH_Pos, (26) 
.equ ADC_CFGR_AWD1CH_Msk, (0x1F << ADC_CFGR_AWD1CH_Pos) //!< 0x7C000000 
.equ ADC_CFGR_AWD1CH, ADC_CFGR_AWD1CH_Msk //!< ADC analog watchdog 1 monitored channel selection 
.equ ADC_CFGR_AWD1CH_0, (0x01 << ADC_CFGR_AWD1CH_Pos) //!< 0x04000000 
.equ ADC_CFGR_AWD1CH_1, (0x02 << ADC_CFGR_AWD1CH_Pos) //!< 0x08000000 
.equ ADC_CFGR_AWD1CH_2, (0x04 << ADC_CFGR_AWD1CH_Pos) //!< 0x10000000 
.equ ADC_CFGR_AWD1CH_3, (0x08 << ADC_CFGR_AWD1CH_Pos) //!< 0x20000000 
.equ ADC_CFGR_AWD1CH_4, (0x10 << ADC_CFGR_AWD1CH_Pos) //!< 0x40000000 
// Legacy defines
.equ ADC_CFGR_AUTOFF_Pos, (15) 
.equ ADC_CFGR_AUTOFF_Msk, (0x1 << ADC_CFGR_AUTOFF_Pos) //!< 0x00008000 
.equ ADC_CFGR_AUTOFF, ADC_CFGR_AUTOFF_Msk //!< ADC low power auto power off 
//*******************  Bit definition for ADC_SMPR1 register  ****************
.equ ADC_SMPR1_SMP0_Pos, (0) 
.equ ADC_SMPR1_SMP0_Msk, (0x7 << ADC_SMPR1_SMP0_Pos) //!< 0x00000007 
.equ ADC_SMPR1_SMP0, ADC_SMPR1_SMP0_Msk //!< ADC channel 0 sampling time selection 
.equ ADC_SMPR1_SMP0_0, (0x1 << ADC_SMPR1_SMP0_Pos) //!< 0x00000001 
.equ ADC_SMPR1_SMP0_1, (0x2 << ADC_SMPR1_SMP0_Pos) //!< 0x00000002 
.equ ADC_SMPR1_SMP0_2, (0x4 << ADC_SMPR1_SMP0_Pos) //!< 0x00000004 
.equ ADC_SMPR1_SMP1_Pos, (3) 
.equ ADC_SMPR1_SMP1_Msk, (0x7 << ADC_SMPR1_SMP1_Pos) //!< 0x00000038 
.equ ADC_SMPR1_SMP1, ADC_SMPR1_SMP1_Msk //!< ADC channel 1 sampling time selection 
.equ ADC_SMPR1_SMP1_0, (0x1 << ADC_SMPR1_SMP1_Pos) //!< 0x00000008 
.equ ADC_SMPR1_SMP1_1, (0x2 << ADC_SMPR1_SMP1_Pos) //!< 0x00000010 
.equ ADC_SMPR1_SMP1_2, (0x4 << ADC_SMPR1_SMP1_Pos) //!< 0x00000020 
.equ ADC_SMPR1_SMP2_Pos, (6) 
.equ ADC_SMPR1_SMP2_Msk, (0x7 << ADC_SMPR1_SMP2_Pos) //!< 0x000001C0 
.equ ADC_SMPR1_SMP2, ADC_SMPR1_SMP2_Msk //!< ADC channel 2 sampling time selection 
.equ ADC_SMPR1_SMP2_0, (0x1 << ADC_SMPR1_SMP2_Pos) //!< 0x00000040 
.equ ADC_SMPR1_SMP2_1, (0x2 << ADC_SMPR1_SMP2_Pos) //!< 0x00000080 
.equ ADC_SMPR1_SMP2_2, (0x4 << ADC_SMPR1_SMP2_Pos) //!< 0x00000100 
.equ ADC_SMPR1_SMP3_Pos, (9) 
.equ ADC_SMPR1_SMP3_Msk, (0x7 << ADC_SMPR1_SMP3_Pos) //!< 0x00000E00 
.equ ADC_SMPR1_SMP3, ADC_SMPR1_SMP3_Msk //!< ADC channel 3 sampling time selection 
.equ ADC_SMPR1_SMP3_0, (0x1 << ADC_SMPR1_SMP3_Pos) //!< 0x00000200 
.equ ADC_SMPR1_SMP3_1, (0x2 << ADC_SMPR1_SMP3_Pos) //!< 0x00000400 
.equ ADC_SMPR1_SMP3_2, (0x4 << ADC_SMPR1_SMP3_Pos) //!< 0x00000800 
.equ ADC_SMPR1_SMP4_Pos, (12) 
.equ ADC_SMPR1_SMP4_Msk, (0x7 << ADC_SMPR1_SMP4_Pos) //!< 0x00007000 
.equ ADC_SMPR1_SMP4, ADC_SMPR1_SMP4_Msk //!< ADC channel 4 sampling time selection 
.equ ADC_SMPR1_SMP4_0, (0x1 << ADC_SMPR1_SMP4_Pos) //!< 0x00001000 
.equ ADC_SMPR1_SMP4_1, (0x2 << ADC_SMPR1_SMP4_Pos) //!< 0x00002000 
.equ ADC_SMPR1_SMP4_2, (0x4 << ADC_SMPR1_SMP4_Pos) //!< 0x00004000 
.equ ADC_SMPR1_SMP5_Pos, (15) 
.equ ADC_SMPR1_SMP5_Msk, (0x7 << ADC_SMPR1_SMP5_Pos) //!< 0x00038000 
.equ ADC_SMPR1_SMP5, ADC_SMPR1_SMP5_Msk //!< ADC channel 5 sampling time selection 
.equ ADC_SMPR1_SMP5_0, (0x1 << ADC_SMPR1_SMP5_Pos) //!< 0x00008000 
.equ ADC_SMPR1_SMP5_1, (0x2 << ADC_SMPR1_SMP5_Pos) //!< 0x00010000 
.equ ADC_SMPR1_SMP5_2, (0x4 << ADC_SMPR1_SMP5_Pos) //!< 0x00020000 
.equ ADC_SMPR1_SMP6_Pos, (18) 
.equ ADC_SMPR1_SMP6_Msk, (0x7 << ADC_SMPR1_SMP6_Pos) //!< 0x001C0000 
.equ ADC_SMPR1_SMP6, ADC_SMPR1_SMP6_Msk //!< ADC channel 6 sampling time selection 
.equ ADC_SMPR1_SMP6_0, (0x1 << ADC_SMPR1_SMP6_Pos) //!< 0x00040000 
.equ ADC_SMPR1_SMP6_1, (0x2 << ADC_SMPR1_SMP6_Pos) //!< 0x00080000 
.equ ADC_SMPR1_SMP6_2, (0x4 << ADC_SMPR1_SMP6_Pos) //!< 0x00100000 
.equ ADC_SMPR1_SMP7_Pos, (21) 
.equ ADC_SMPR1_SMP7_Msk, (0x7 << ADC_SMPR1_SMP7_Pos) //!< 0x00E00000 
.equ ADC_SMPR1_SMP7, ADC_SMPR1_SMP7_Msk //!< ADC channel 7 sampling time selection 
.equ ADC_SMPR1_SMP7_0, (0x1 << ADC_SMPR1_SMP7_Pos) //!< 0x00200000 
.equ ADC_SMPR1_SMP7_1, (0x2 << ADC_SMPR1_SMP7_Pos) //!< 0x00400000 
.equ ADC_SMPR1_SMP7_2, (0x4 << ADC_SMPR1_SMP7_Pos) //!< 0x00800000 
.equ ADC_SMPR1_SMP8_Pos, (24) 
.equ ADC_SMPR1_SMP8_Msk, (0x7 << ADC_SMPR1_SMP8_Pos) //!< 0x07000000 
.equ ADC_SMPR1_SMP8, ADC_SMPR1_SMP8_Msk //!< ADC channel 8 sampling time selection 
.equ ADC_SMPR1_SMP8_0, (0x1 << ADC_SMPR1_SMP8_Pos) //!< 0x01000000 
.equ ADC_SMPR1_SMP8_1, (0x2 << ADC_SMPR1_SMP8_Pos) //!< 0x02000000 
.equ ADC_SMPR1_SMP8_2, (0x4 << ADC_SMPR1_SMP8_Pos) //!< 0x04000000 
.equ ADC_SMPR1_SMP9_Pos, (27) 
.equ ADC_SMPR1_SMP9_Msk, (0x7 << ADC_SMPR1_SMP9_Pos) //!< 0x38000000 
.equ ADC_SMPR1_SMP9, ADC_SMPR1_SMP9_Msk //!< ADC channel 9 sampling time selection 
.equ ADC_SMPR1_SMP9_0, (0x1 << ADC_SMPR1_SMP9_Pos) //!< 0x08000000 
.equ ADC_SMPR1_SMP9_1, (0x2 << ADC_SMPR1_SMP9_Pos) //!< 0x10000000 
.equ ADC_SMPR1_SMP9_2, (0x4 << ADC_SMPR1_SMP9_Pos) //!< 0x20000000 
//*******************  Bit definition for ADC_SMPR2 register  ****************
.equ ADC_SMPR2_SMP10_Pos, (0) 
.equ ADC_SMPR2_SMP10_Msk, (0x7 << ADC_SMPR2_SMP10_Pos) //!< 0x00000007 
.equ ADC_SMPR2_SMP10, ADC_SMPR2_SMP10_Msk //!< ADC channel 10 sampling time selection 
.equ ADC_SMPR2_SMP10_0, (0x1 << ADC_SMPR2_SMP10_Pos) //!< 0x00000001 
.equ ADC_SMPR2_SMP10_1, (0x2 << ADC_SMPR2_SMP10_Pos) //!< 0x00000002 
.equ ADC_SMPR2_SMP10_2, (0x4 << ADC_SMPR2_SMP10_Pos) //!< 0x00000004 
.equ ADC_SMPR2_SMP11_Pos, (3) 
.equ ADC_SMPR2_SMP11_Msk, (0x7 << ADC_SMPR2_SMP11_Pos) //!< 0x00000038 
.equ ADC_SMPR2_SMP11, ADC_SMPR2_SMP11_Msk //!< ADC channel 11 sampling time selection 
.equ ADC_SMPR2_SMP11_0, (0x1 << ADC_SMPR2_SMP11_Pos) //!< 0x00000008 
.equ ADC_SMPR2_SMP11_1, (0x2 << ADC_SMPR2_SMP11_Pos) //!< 0x00000010 
.equ ADC_SMPR2_SMP11_2, (0x4 << ADC_SMPR2_SMP11_Pos) //!< 0x00000020 
.equ ADC_SMPR2_SMP12_Pos, (6) 
.equ ADC_SMPR2_SMP12_Msk, (0x7 << ADC_SMPR2_SMP12_Pos) //!< 0x000001C0 
.equ ADC_SMPR2_SMP12, ADC_SMPR2_SMP12_Msk //!< ADC channel 12 sampling time selection 
.equ ADC_SMPR2_SMP12_0, (0x1 << ADC_SMPR2_SMP12_Pos) //!< 0x00000040 
.equ ADC_SMPR2_SMP12_1, (0x2 << ADC_SMPR2_SMP12_Pos) //!< 0x00000080 
.equ ADC_SMPR2_SMP12_2, (0x4 << ADC_SMPR2_SMP12_Pos) //!< 0x00000100 
.equ ADC_SMPR2_SMP13_Pos, (9) 
.equ ADC_SMPR2_SMP13_Msk, (0x7 << ADC_SMPR2_SMP13_Pos) //!< 0x00000E00 
.equ ADC_SMPR2_SMP13, ADC_SMPR2_SMP13_Msk //!< ADC channel 13 sampling time selection 
.equ ADC_SMPR2_SMP13_0, (0x1 << ADC_SMPR2_SMP13_Pos) //!< 0x00000200 
.equ ADC_SMPR2_SMP13_1, (0x2 << ADC_SMPR2_SMP13_Pos) //!< 0x00000400 
.equ ADC_SMPR2_SMP13_2, (0x4 << ADC_SMPR2_SMP13_Pos) //!< 0x00000800 
.equ ADC_SMPR2_SMP14_Pos, (12) 
.equ ADC_SMPR2_SMP14_Msk, (0x7 << ADC_SMPR2_SMP14_Pos) //!< 0x00007000 
.equ ADC_SMPR2_SMP14, ADC_SMPR2_SMP14_Msk //!< ADC channel 14 sampling time selection 
.equ ADC_SMPR2_SMP14_0, (0x1 << ADC_SMPR2_SMP14_Pos) //!< 0x00001000 
.equ ADC_SMPR2_SMP14_1, (0x2 << ADC_SMPR2_SMP14_Pos) //!< 0x00002000 
.equ ADC_SMPR2_SMP14_2, (0x4 << ADC_SMPR2_SMP14_Pos) //!< 0x00004000 
.equ ADC_SMPR2_SMP15_Pos, (15) 
.equ ADC_SMPR2_SMP15_Msk, (0x7 << ADC_SMPR2_SMP15_Pos) //!< 0x00038000 
.equ ADC_SMPR2_SMP15, ADC_SMPR2_SMP15_Msk //!< ADC channel 15 sampling time selection 
.equ ADC_SMPR2_SMP15_0, (0x1 << ADC_SMPR2_SMP15_Pos) //!< 0x00008000 
.equ ADC_SMPR2_SMP15_1, (0x2 << ADC_SMPR2_SMP15_Pos) //!< 0x00010000 
.equ ADC_SMPR2_SMP15_2, (0x4 << ADC_SMPR2_SMP15_Pos) //!< 0x00020000 
.equ ADC_SMPR2_SMP16_Pos, (18) 
.equ ADC_SMPR2_SMP16_Msk, (0x7 << ADC_SMPR2_SMP16_Pos) //!< 0x001C0000 
.equ ADC_SMPR2_SMP16, ADC_SMPR2_SMP16_Msk //!< ADC channel 16 sampling time selection 
.equ ADC_SMPR2_SMP16_0, (0x1 << ADC_SMPR2_SMP16_Pos) //!< 0x00040000 
.equ ADC_SMPR2_SMP16_1, (0x2 << ADC_SMPR2_SMP16_Pos) //!< 0x00080000 
.equ ADC_SMPR2_SMP16_2, (0x4 << ADC_SMPR2_SMP16_Pos) //!< 0x00100000 
.equ ADC_SMPR2_SMP17_Pos, (21) 
.equ ADC_SMPR2_SMP17_Msk, (0x7 << ADC_SMPR2_SMP17_Pos) //!< 0x00E00000 
.equ ADC_SMPR2_SMP17, ADC_SMPR2_SMP17_Msk //!< ADC channel 17 sampling time selection 
.equ ADC_SMPR2_SMP17_0, (0x1 << ADC_SMPR2_SMP17_Pos) //!< 0x00200000 
.equ ADC_SMPR2_SMP17_1, (0x2 << ADC_SMPR2_SMP17_Pos) //!< 0x00400000 
.equ ADC_SMPR2_SMP17_2, (0x4 << ADC_SMPR2_SMP17_Pos) //!< 0x00800000 
.equ ADC_SMPR2_SMP18_Pos, (24) 
.equ ADC_SMPR2_SMP18_Msk, (0x7 << ADC_SMPR2_SMP18_Pos) //!< 0x07000000 
.equ ADC_SMPR2_SMP18, ADC_SMPR2_SMP18_Msk //!< ADC channel 18 sampling time selection 
.equ ADC_SMPR2_SMP18_0, (0x1 << ADC_SMPR2_SMP18_Pos) //!< 0x01000000 
.equ ADC_SMPR2_SMP18_1, (0x2 << ADC_SMPR2_SMP18_Pos) //!< 0x02000000 
.equ ADC_SMPR2_SMP18_2, (0x4 << ADC_SMPR2_SMP18_Pos) //!< 0x04000000 
//*******************  Bit definition for ADC_TR1 register  ******************
.equ ADC_TR1_LT1_Pos, (0) 
.equ ADC_TR1_LT1_Msk, (0xFFF << ADC_TR1_LT1_Pos) //!< 0x00000FFF 
.equ ADC_TR1_LT1, ADC_TR1_LT1_Msk //!< ADC analog watchdog 1 threshold low 
.equ ADC_TR1_LT1_0, (0x001 << ADC_TR1_LT1_Pos) //!< 0x00000001 
.equ ADC_TR1_LT1_1, (0x002 << ADC_TR1_LT1_Pos) //!< 0x00000002 
.equ ADC_TR1_LT1_2, (0x004 << ADC_TR1_LT1_Pos) //!< 0x00000004 
.equ ADC_TR1_LT1_3, (0x008 << ADC_TR1_LT1_Pos) //!< 0x00000008 
.equ ADC_TR1_LT1_4, (0x010 << ADC_TR1_LT1_Pos) //!< 0x00000010 
.equ ADC_TR1_LT1_5, (0x020 << ADC_TR1_LT1_Pos) //!< 0x00000020 
.equ ADC_TR1_LT1_6, (0x040 << ADC_TR1_LT1_Pos) //!< 0x00000040 
.equ ADC_TR1_LT1_7, (0x080 << ADC_TR1_LT1_Pos) //!< 0x00000080 
.equ ADC_TR1_LT1_8, (0x100 << ADC_TR1_LT1_Pos) //!< 0x00000100 
.equ ADC_TR1_LT1_9, (0x200 << ADC_TR1_LT1_Pos) //!< 0x00000200 
.equ ADC_TR1_LT1_10, (0x400 << ADC_TR1_LT1_Pos) //!< 0x00000400 
.equ ADC_TR1_LT1_11, (0x800 << ADC_TR1_LT1_Pos) //!< 0x00000800 
.equ ADC_TR1_HT1_Pos, (16) 
.equ ADC_TR1_HT1_Msk, (0xFFF << ADC_TR1_HT1_Pos) //!< 0x0FFF0000 
.equ ADC_TR1_HT1, ADC_TR1_HT1_Msk //!< ADC Analog watchdog 1 threshold high 
.equ ADC_TR1_HT1_0, (0x001 << ADC_TR1_HT1_Pos) //!< 0x00010000 
.equ ADC_TR1_HT1_1, (0x002 << ADC_TR1_HT1_Pos) //!< 0x00020000 
.equ ADC_TR1_HT1_2, (0x004 << ADC_TR1_HT1_Pos) //!< 0x00040000 
.equ ADC_TR1_HT1_3, (0x008 << ADC_TR1_HT1_Pos) //!< 0x00080000 
.equ ADC_TR1_HT1_4, (0x010 << ADC_TR1_HT1_Pos) //!< 0x00100000 
.equ ADC_TR1_HT1_5, (0x020 << ADC_TR1_HT1_Pos) //!< 0x00200000 
.equ ADC_TR1_HT1_6, (0x040 << ADC_TR1_HT1_Pos) //!< 0x00400000 
.equ ADC_TR1_HT1_7, (0x080 << ADC_TR1_HT1_Pos) //!< 0x00800000 
.equ ADC_TR1_HT1_8, (0x100 << ADC_TR1_HT1_Pos) //!< 0x01000000 
.equ ADC_TR1_HT1_9, (0x200 << ADC_TR1_HT1_Pos) //!< 0x02000000 
.equ ADC_TR1_HT1_10, (0x400 << ADC_TR1_HT1_Pos) //!< 0x04000000 
.equ ADC_TR1_HT1_11, (0x800 << ADC_TR1_HT1_Pos) //!< 0x08000000 
//*******************  Bit definition for ADC_TR2 register  ******************
.equ ADC_TR2_LT2_Pos, (0) 
.equ ADC_TR2_LT2_Msk, (0xFF << ADC_TR2_LT2_Pos) //!< 0x000000FF 
.equ ADC_TR2_LT2, ADC_TR2_LT2_Msk //!< ADC analog watchdog 2 threshold low 
.equ ADC_TR2_LT2_0, (0x01 << ADC_TR2_LT2_Pos) //!< 0x00000001 
.equ ADC_TR2_LT2_1, (0x02 << ADC_TR2_LT2_Pos) //!< 0x00000002 
.equ ADC_TR2_LT2_2, (0x04 << ADC_TR2_LT2_Pos) //!< 0x00000004 
.equ ADC_TR2_LT2_3, (0x08 << ADC_TR2_LT2_Pos) //!< 0x00000008 
.equ ADC_TR2_LT2_4, (0x10 << ADC_TR2_LT2_Pos) //!< 0x00000010 
.equ ADC_TR2_LT2_5, (0x20 << ADC_TR2_LT2_Pos) //!< 0x00000020 
.equ ADC_TR2_LT2_6, (0x40 << ADC_TR2_LT2_Pos) //!< 0x00000040 
.equ ADC_TR2_LT2_7, (0x80 << ADC_TR2_LT2_Pos) //!< 0x00000080 
.equ ADC_TR2_HT2_Pos, (16) 
.equ ADC_TR2_HT2_Msk, (0xFF << ADC_TR2_HT2_Pos) //!< 0x00FF0000 
.equ ADC_TR2_HT2, ADC_TR2_HT2_Msk //!< ADC analog watchdog 2 threshold high 
.equ ADC_TR2_HT2_0, (0x01 << ADC_TR2_HT2_Pos) //!< 0x00010000 
.equ ADC_TR2_HT2_1, (0x02 << ADC_TR2_HT2_Pos) //!< 0x00020000 
.equ ADC_TR2_HT2_2, (0x04 << ADC_TR2_HT2_Pos) //!< 0x00040000 
.equ ADC_TR2_HT2_3, (0x08 << ADC_TR2_HT2_Pos) //!< 0x00080000 
.equ ADC_TR2_HT2_4, (0x10 << ADC_TR2_HT2_Pos) //!< 0x00100000 
.equ ADC_TR2_HT2_5, (0x20 << ADC_TR2_HT2_Pos) //!< 0x00200000 
.equ ADC_TR2_HT2_6, (0x40 << ADC_TR2_HT2_Pos) //!< 0x00400000 
.equ ADC_TR2_HT2_7, (0x80 << ADC_TR2_HT2_Pos) //!< 0x00800000 
//*******************  Bit definition for ADC_TR3 register  ******************
.equ ADC_TR3_LT3_Pos, (0) 
.equ ADC_TR3_LT3_Msk, (0xFF << ADC_TR3_LT3_Pos) //!< 0x000000FF 
.equ ADC_TR3_LT3, ADC_TR3_LT3_Msk //!< ADC analog watchdog 3 threshold low 
.equ ADC_TR3_LT3_0, (0x01 << ADC_TR3_LT3_Pos) //!< 0x00000001 
.equ ADC_TR3_LT3_1, (0x02 << ADC_TR3_LT3_Pos) //!< 0x00000002 
.equ ADC_TR3_LT3_2, (0x04 << ADC_TR3_LT3_Pos) //!< 0x00000004 
.equ ADC_TR3_LT3_3, (0x08 << ADC_TR3_LT3_Pos) //!< 0x00000008 
.equ ADC_TR3_LT3_4, (0x10 << ADC_TR3_LT3_Pos) //!< 0x00000010 
.equ ADC_TR3_LT3_5, (0x20 << ADC_TR3_LT3_Pos) //!< 0x00000020 
.equ ADC_TR3_LT3_6, (0x40 << ADC_TR3_LT3_Pos) //!< 0x00000040 
.equ ADC_TR3_LT3_7, (0x80 << ADC_TR3_LT3_Pos) //!< 0x00000080 
.equ ADC_TR3_HT3_Pos, (16) 
.equ ADC_TR3_HT3_Msk, (0xFF << ADC_TR3_HT3_Pos) //!< 0x00FF0000 
.equ ADC_TR3_HT3, ADC_TR3_HT3_Msk //!< ADC analog watchdog 3 threshold high 
.equ ADC_TR3_HT3_0, (0x01 << ADC_TR3_HT3_Pos) //!< 0x00010000 
.equ ADC_TR3_HT3_1, (0x02 << ADC_TR3_HT3_Pos) //!< 0x00020000 
.equ ADC_TR3_HT3_2, (0x04 << ADC_TR3_HT3_Pos) //!< 0x00040000 
.equ ADC_TR3_HT3_3, (0x08 << ADC_TR3_HT3_Pos) //!< 0x00080000 
.equ ADC_TR3_HT3_4, (0x10 << ADC_TR3_HT3_Pos) //!< 0x00100000 
.equ ADC_TR3_HT3_5, (0x20 << ADC_TR3_HT3_Pos) //!< 0x00200000 
.equ ADC_TR3_HT3_6, (0x40 << ADC_TR3_HT3_Pos) //!< 0x00400000 
.equ ADC_TR3_HT3_7, (0x80 << ADC_TR3_HT3_Pos) //!< 0x00800000 
//*******************  Bit definition for ADC_SQR1 register  *****************
.equ ADC_SQR1_L_Pos, (0) 
.equ ADC_SQR1_L_Msk, (0xF << ADC_SQR1_L_Pos) //!< 0x0000000F 
.equ ADC_SQR1_L, ADC_SQR1_L_Msk //!< ADC group regular sequencer scan length 
.equ ADC_SQR1_L_0, (0x1 << ADC_SQR1_L_Pos) //!< 0x00000001 
.equ ADC_SQR1_L_1, (0x2 << ADC_SQR1_L_Pos) //!< 0x00000002 
.equ ADC_SQR1_L_2, (0x4 << ADC_SQR1_L_Pos) //!< 0x00000004 
.equ ADC_SQR1_L_3, (0x8 << ADC_SQR1_L_Pos) //!< 0x00000008 
.equ ADC_SQR1_SQ1_Pos, (6) 
.equ ADC_SQR1_SQ1_Msk, (0x1F << ADC_SQR1_SQ1_Pos) //!< 0x000007C0 
.equ ADC_SQR1_SQ1, ADC_SQR1_SQ1_Msk //!< ADC group regular sequencer rank 1 
.equ ADC_SQR1_SQ1_0, (0x01 << ADC_SQR1_SQ1_Pos) //!< 0x00000040 
.equ ADC_SQR1_SQ1_1, (0x02 << ADC_SQR1_SQ1_Pos) //!< 0x00000080 
.equ ADC_SQR1_SQ1_2, (0x04 << ADC_SQR1_SQ1_Pos) //!< 0x00000100 
.equ ADC_SQR1_SQ1_3, (0x08 << ADC_SQR1_SQ1_Pos) //!< 0x00000200 
.equ ADC_SQR1_SQ1_4, (0x10 << ADC_SQR1_SQ1_Pos) //!< 0x00000400 
.equ ADC_SQR1_SQ2_Pos, (12) 
.equ ADC_SQR1_SQ2_Msk, (0x1F << ADC_SQR1_SQ2_Pos) //!< 0x0001F000 
.equ ADC_SQR1_SQ2, ADC_SQR1_SQ2_Msk //!< ADC group regular sequencer rank 2 
.equ ADC_SQR1_SQ2_0, (0x01 << ADC_SQR1_SQ2_Pos) //!< 0x00001000 
.equ ADC_SQR1_SQ2_1, (0x02 << ADC_SQR1_SQ2_Pos) //!< 0x00002000 
.equ ADC_SQR1_SQ2_2, (0x04 << ADC_SQR1_SQ2_Pos) //!< 0x00004000 
.equ ADC_SQR1_SQ2_3, (0x08 << ADC_SQR1_SQ2_Pos) //!< 0x00008000 
.equ ADC_SQR1_SQ2_4, (0x10 << ADC_SQR1_SQ2_Pos) //!< 0x00010000 
.equ ADC_SQR1_SQ3_Pos, (18) 
.equ ADC_SQR1_SQ3_Msk, (0x1F << ADC_SQR1_SQ3_Pos) //!< 0x007C0000 
.equ ADC_SQR1_SQ3, ADC_SQR1_SQ3_Msk //!< ADC group regular sequencer rank 3 
.equ ADC_SQR1_SQ3_0, (0x01 << ADC_SQR1_SQ3_Pos) //!< 0x00040000 
.equ ADC_SQR1_SQ3_1, (0x02 << ADC_SQR1_SQ3_Pos) //!< 0x00080000 
.equ ADC_SQR1_SQ3_2, (0x04 << ADC_SQR1_SQ3_Pos) //!< 0x00100000 
.equ ADC_SQR1_SQ3_3, (0x08 << ADC_SQR1_SQ3_Pos) //!< 0x00200000 
.equ ADC_SQR1_SQ3_4, (0x10 << ADC_SQR1_SQ3_Pos) //!< 0x00400000 
.equ ADC_SQR1_SQ4_Pos, (24) 
.equ ADC_SQR1_SQ4_Msk, (0x1F << ADC_SQR1_SQ4_Pos) //!< 0x1F000000 
.equ ADC_SQR1_SQ4, ADC_SQR1_SQ4_Msk //!< ADC group regular sequencer rank 4 
.equ ADC_SQR1_SQ4_0, (0x01 << ADC_SQR1_SQ4_Pos) //!< 0x01000000 
.equ ADC_SQR1_SQ4_1, (0x02 << ADC_SQR1_SQ4_Pos) //!< 0x02000000 
.equ ADC_SQR1_SQ4_2, (0x04 << ADC_SQR1_SQ4_Pos) //!< 0x04000000 
.equ ADC_SQR1_SQ4_3, (0x08 << ADC_SQR1_SQ4_Pos) //!< 0x08000000 
.equ ADC_SQR1_SQ4_4, (0x10 << ADC_SQR1_SQ4_Pos) //!< 0x10000000 
//*******************  Bit definition for ADC_SQR2 register  *****************
.equ ADC_SQR2_SQ5_Pos, (0) 
.equ ADC_SQR2_SQ5_Msk, (0x1F << ADC_SQR2_SQ5_Pos) //!< 0x0000001F 
.equ ADC_SQR2_SQ5, ADC_SQR2_SQ5_Msk //!< ADC group regular sequencer rank 5 
.equ ADC_SQR2_SQ5_0, (0x01 << ADC_SQR2_SQ5_Pos) //!< 0x00000001 
.equ ADC_SQR2_SQ5_1, (0x02 << ADC_SQR2_SQ5_Pos) //!< 0x00000002 
.equ ADC_SQR2_SQ5_2, (0x04 << ADC_SQR2_SQ5_Pos) //!< 0x00000004 
.equ ADC_SQR2_SQ5_3, (0x08 << ADC_SQR2_SQ5_Pos) //!< 0x00000008 
.equ ADC_SQR2_SQ5_4, (0x10 << ADC_SQR2_SQ5_Pos) //!< 0x00000010 
.equ ADC_SQR2_SQ6_Pos, (6) 
.equ ADC_SQR2_SQ6_Msk, (0x1F << ADC_SQR2_SQ6_Pos) //!< 0x000007C0 
.equ ADC_SQR2_SQ6, ADC_SQR2_SQ6_Msk //!< ADC group regular sequencer rank 6 
.equ ADC_SQR2_SQ6_0, (0x01 << ADC_SQR2_SQ6_Pos) //!< 0x00000040 
.equ ADC_SQR2_SQ6_1, (0x02 << ADC_SQR2_SQ6_Pos) //!< 0x00000080 
.equ ADC_SQR2_SQ6_2, (0x04 << ADC_SQR2_SQ6_Pos) //!< 0x00000100 
.equ ADC_SQR2_SQ6_3, (0x08 << ADC_SQR2_SQ6_Pos) //!< 0x00000200 
.equ ADC_SQR2_SQ6_4, (0x10 << ADC_SQR2_SQ6_Pos) //!< 0x00000400 
.equ ADC_SQR2_SQ7_Pos, (12) 
.equ ADC_SQR2_SQ7_Msk, (0x1F << ADC_SQR2_SQ7_Pos) //!< 0x0001F000 
.equ ADC_SQR2_SQ7, ADC_SQR2_SQ7_Msk //!< ADC group regular sequencer rank 7 
.equ ADC_SQR2_SQ7_0, (0x01 << ADC_SQR2_SQ7_Pos) //!< 0x00001000 
.equ ADC_SQR2_SQ7_1, (0x02 << ADC_SQR2_SQ7_Pos) //!< 0x00002000 
.equ ADC_SQR2_SQ7_2, (0x04 << ADC_SQR2_SQ7_Pos) //!< 0x00004000 
.equ ADC_SQR2_SQ7_3, (0x08 << ADC_SQR2_SQ7_Pos) //!< 0x00008000 
.equ ADC_SQR2_SQ7_4, (0x10 << ADC_SQR2_SQ7_Pos) //!< 0x00010000 
.equ ADC_SQR2_SQ8_Pos, (18) 
.equ ADC_SQR2_SQ8_Msk, (0x1F << ADC_SQR2_SQ8_Pos) //!< 0x007C0000 
.equ ADC_SQR2_SQ8, ADC_SQR2_SQ8_Msk //!< ADC group regular sequencer rank 8 
.equ ADC_SQR2_SQ8_0, (0x01 << ADC_SQR2_SQ8_Pos) //!< 0x00040000 
.equ ADC_SQR2_SQ8_1, (0x02 << ADC_SQR2_SQ8_Pos) //!< 0x00080000 
.equ ADC_SQR2_SQ8_2, (0x04 << ADC_SQR2_SQ8_Pos) //!< 0x00100000 
.equ ADC_SQR2_SQ8_3, (0x08 << ADC_SQR2_SQ8_Pos) //!< 0x00200000 
.equ ADC_SQR2_SQ8_4, (0x10 << ADC_SQR2_SQ8_Pos) //!< 0x00400000 
.equ ADC_SQR2_SQ9_Pos, (24) 
.equ ADC_SQR2_SQ9_Msk, (0x1F << ADC_SQR2_SQ9_Pos) //!< 0x1F000000 
.equ ADC_SQR2_SQ9, ADC_SQR2_SQ9_Msk //!< ADC group regular sequencer rank 9 
.equ ADC_SQR2_SQ9_0, (0x01 << ADC_SQR2_SQ9_Pos) //!< 0x01000000 
.equ ADC_SQR2_SQ9_1, (0x02 << ADC_SQR2_SQ9_Pos) //!< 0x02000000 
.equ ADC_SQR2_SQ9_2, (0x04 << ADC_SQR2_SQ9_Pos) //!< 0x04000000 
.equ ADC_SQR2_SQ9_3, (0x08 << ADC_SQR2_SQ9_Pos) //!< 0x08000000 
.equ ADC_SQR2_SQ9_4, (0x10 << ADC_SQR2_SQ9_Pos) //!< 0x10000000 
//*******************  Bit definition for ADC_SQR3 register  *****************
.equ ADC_SQR3_SQ10_Pos, (0) 
.equ ADC_SQR3_SQ10_Msk, (0x1F << ADC_SQR3_SQ10_Pos) //!< 0x0000001F 
.equ ADC_SQR3_SQ10, ADC_SQR3_SQ10_Msk //!< ADC group regular sequencer rank 10 
.equ ADC_SQR3_SQ10_0, (0x01 << ADC_SQR3_SQ10_Pos) //!< 0x00000001 
.equ ADC_SQR3_SQ10_1, (0x02 << ADC_SQR3_SQ10_Pos) //!< 0x00000002 
.equ ADC_SQR3_SQ10_2, (0x04 << ADC_SQR3_SQ10_Pos) //!< 0x00000004 
.equ ADC_SQR3_SQ10_3, (0x08 << ADC_SQR3_SQ10_Pos) //!< 0x00000008 
.equ ADC_SQR3_SQ10_4, (0x10 << ADC_SQR3_SQ10_Pos) //!< 0x00000010 
.equ ADC_SQR3_SQ11_Pos, (6) 
.equ ADC_SQR3_SQ11_Msk, (0x1F << ADC_SQR3_SQ11_Pos) //!< 0x000007C0 
.equ ADC_SQR3_SQ11, ADC_SQR3_SQ11_Msk //!< ADC group regular sequencer rank 11 
.equ ADC_SQR3_SQ11_0, (0x01 << ADC_SQR3_SQ11_Pos) //!< 0x00000040 
.equ ADC_SQR3_SQ11_1, (0x02 << ADC_SQR3_SQ11_Pos) //!< 0x00000080 
.equ ADC_SQR3_SQ11_2, (0x04 << ADC_SQR3_SQ11_Pos) //!< 0x00000100 
.equ ADC_SQR3_SQ11_3, (0x08 << ADC_SQR3_SQ11_Pos) //!< 0x00000200 
.equ ADC_SQR3_SQ11_4, (0x10 << ADC_SQR3_SQ11_Pos) //!< 0x00000400 
.equ ADC_SQR3_SQ12_Pos, (12) 
.equ ADC_SQR3_SQ12_Msk, (0x1F << ADC_SQR3_SQ12_Pos) //!< 0x0001F000 
.equ ADC_SQR3_SQ12, ADC_SQR3_SQ12_Msk //!< ADC group regular sequencer rank 12 
.equ ADC_SQR3_SQ12_0, (0x01 << ADC_SQR3_SQ12_Pos) //!< 0x00001000 
.equ ADC_SQR3_SQ12_1, (0x02 << ADC_SQR3_SQ12_Pos) //!< 0x00002000 
.equ ADC_SQR3_SQ12_2, (0x04 << ADC_SQR3_SQ12_Pos) //!< 0x00004000 
.equ ADC_SQR3_SQ12_3, (0x08 << ADC_SQR3_SQ12_Pos) //!< 0x00008000 
.equ ADC_SQR3_SQ12_4, (0x10 << ADC_SQR3_SQ12_Pos) //!< 0x00010000 
.equ ADC_SQR3_SQ13_Pos, (18) 
.equ ADC_SQR3_SQ13_Msk, (0x1F << ADC_SQR3_SQ13_Pos) //!< 0x007C0000 
.equ ADC_SQR3_SQ13, ADC_SQR3_SQ13_Msk //!< ADC group regular sequencer rank 13 
.equ ADC_SQR3_SQ13_0, (0x01 << ADC_SQR3_SQ13_Pos) //!< 0x00040000 
.equ ADC_SQR3_SQ13_1, (0x02 << ADC_SQR3_SQ13_Pos) //!< 0x00080000 
.equ ADC_SQR3_SQ13_2, (0x04 << ADC_SQR3_SQ13_Pos) //!< 0x00100000 
.equ ADC_SQR3_SQ13_3, (0x08 << ADC_SQR3_SQ13_Pos) //!< 0x00200000 
.equ ADC_SQR3_SQ13_4, (0x10 << ADC_SQR3_SQ13_Pos) //!< 0x00400000 
.equ ADC_SQR3_SQ14_Pos, (24) 
.equ ADC_SQR3_SQ14_Msk, (0x1F << ADC_SQR3_SQ14_Pos) //!< 0x1F000000 
.equ ADC_SQR3_SQ14, ADC_SQR3_SQ14_Msk //!< ADC group regular sequencer rank 14 
.equ ADC_SQR3_SQ14_0, (0x01 << ADC_SQR3_SQ14_Pos) //!< 0x01000000 
.equ ADC_SQR3_SQ14_1, (0x02 << ADC_SQR3_SQ14_Pos) //!< 0x02000000 
.equ ADC_SQR3_SQ14_2, (0x04 << ADC_SQR3_SQ14_Pos) //!< 0x04000000 
.equ ADC_SQR3_SQ14_3, (0x08 << ADC_SQR3_SQ14_Pos) //!< 0x08000000 
.equ ADC_SQR3_SQ14_4, (0x10 << ADC_SQR3_SQ14_Pos) //!< 0x10000000 
//*******************  Bit definition for ADC_SQR4 register  *****************
.equ ADC_SQR4_SQ15_Pos, (0) 
.equ ADC_SQR4_SQ15_Msk, (0x1F << ADC_SQR4_SQ15_Pos) //!< 0x0000001F 
.equ ADC_SQR4_SQ15, ADC_SQR4_SQ15_Msk //!< ADC group regular sequencer rank 15 
.equ ADC_SQR4_SQ15_0, (0x01 << ADC_SQR4_SQ15_Pos) //!< 0x00000001 
.equ ADC_SQR4_SQ15_1, (0x02 << ADC_SQR4_SQ15_Pos) //!< 0x00000002 
.equ ADC_SQR4_SQ15_2, (0x04 << ADC_SQR4_SQ15_Pos) //!< 0x00000004 
.equ ADC_SQR4_SQ15_3, (0x08 << ADC_SQR4_SQ15_Pos) //!< 0x00000008 
.equ ADC_SQR4_SQ15_4, (0x10 << ADC_SQR4_SQ15_Pos) //!< 0x00000010 
.equ ADC_SQR4_SQ16_Pos, (6) 
.equ ADC_SQR4_SQ16_Msk, (0x1F << ADC_SQR4_SQ16_Pos) //!< 0x000007C0 
.equ ADC_SQR4_SQ16, ADC_SQR4_SQ16_Msk //!< ADC group regular sequencer rank 16 
.equ ADC_SQR4_SQ16_0, (0x01 << ADC_SQR4_SQ16_Pos) //!< 0x00000040 
.equ ADC_SQR4_SQ16_1, (0x02 << ADC_SQR4_SQ16_Pos) //!< 0x00000080 
.equ ADC_SQR4_SQ16_2, (0x04 << ADC_SQR4_SQ16_Pos) //!< 0x00000100 
.equ ADC_SQR4_SQ16_3, (0x08 << ADC_SQR4_SQ16_Pos) //!< 0x00000200 
.equ ADC_SQR4_SQ16_4, (0x10 << ADC_SQR4_SQ16_Pos) //!< 0x00000400 
//*******************  Bit definition for ADC_DR register  *******************
.equ ADC_DR_RDATA_Pos, (0) 
.equ ADC_DR_RDATA_Msk, (0xFFFF << ADC_DR_RDATA_Pos) //!< 0x0000FFFF 
.equ ADC_DR_RDATA, ADC_DR_RDATA_Msk //!< ADC group regular conversion data 
.equ ADC_DR_RDATA_0, (0x0001 << ADC_DR_RDATA_Pos) //!< 0x00000001 
.equ ADC_DR_RDATA_1, (0x0002 << ADC_DR_RDATA_Pos) //!< 0x00000002 
.equ ADC_DR_RDATA_2, (0x0004 << ADC_DR_RDATA_Pos) //!< 0x00000004 
.equ ADC_DR_RDATA_3, (0x0008 << ADC_DR_RDATA_Pos) //!< 0x00000008 
.equ ADC_DR_RDATA_4, (0x0010 << ADC_DR_RDATA_Pos) //!< 0x00000010 
.equ ADC_DR_RDATA_5, (0x0020 << ADC_DR_RDATA_Pos) //!< 0x00000020 
.equ ADC_DR_RDATA_6, (0x0040 << ADC_DR_RDATA_Pos) //!< 0x00000040 
.equ ADC_DR_RDATA_7, (0x0080 << ADC_DR_RDATA_Pos) //!< 0x00000080 
.equ ADC_DR_RDATA_8, (0x0100 << ADC_DR_RDATA_Pos) //!< 0x00000100 
.equ ADC_DR_RDATA_9, (0x0200 << ADC_DR_RDATA_Pos) //!< 0x00000200 
.equ ADC_DR_RDATA_10, (0x0400 << ADC_DR_RDATA_Pos) //!< 0x00000400 
.equ ADC_DR_RDATA_11, (0x0800 << ADC_DR_RDATA_Pos) //!< 0x00000800 
.equ ADC_DR_RDATA_12, (0x1000 << ADC_DR_RDATA_Pos) //!< 0x00001000 
.equ ADC_DR_RDATA_13, (0x2000 << ADC_DR_RDATA_Pos) //!< 0x00002000 
.equ ADC_DR_RDATA_14, (0x4000 << ADC_DR_RDATA_Pos) //!< 0x00004000 
.equ ADC_DR_RDATA_15, (0x8000 << ADC_DR_RDATA_Pos) //!< 0x00008000 
//*******************  Bit definition for ADC_JSQR register  *****************
.equ ADC_JSQR_JL_Pos, (0) 
.equ ADC_JSQR_JL_Msk, (0x3 << ADC_JSQR_JL_Pos) //!< 0x00000003 
.equ ADC_JSQR_JL, ADC_JSQR_JL_Msk //!< ADC group injected sequencer scan length 
.equ ADC_JSQR_JL_0, (0x1 << ADC_JSQR_JL_Pos) //!< 0x00000001 
.equ ADC_JSQR_JL_1, (0x2 << ADC_JSQR_JL_Pos) //!< 0x00000002 
.equ ADC_JSQR_JEXTSEL_Pos, (2) 
.equ ADC_JSQR_JEXTSEL_Msk, (0xF << ADC_JSQR_JEXTSEL_Pos) //!< 0x0000003C 
.equ ADC_JSQR_JEXTSEL, ADC_JSQR_JEXTSEL_Msk //!< ADC group injected external trigger source 
.equ ADC_JSQR_JEXTSEL_0, (0x1 << ADC_JSQR_JEXTSEL_Pos) //!< 0x00000004 
.equ ADC_JSQR_JEXTSEL_1, (0x2 << ADC_JSQR_JEXTSEL_Pos) //!< 0x00000008 
.equ ADC_JSQR_JEXTSEL_2, (0x4 << ADC_JSQR_JEXTSEL_Pos) //!< 0x00000010 
.equ ADC_JSQR_JEXTSEL_3, (0x8 << ADC_JSQR_JEXTSEL_Pos) //!< 0x00000020 
.equ ADC_JSQR_JEXTEN_Pos, (6) 
.equ ADC_JSQR_JEXTEN_Msk, (0x3 << ADC_JSQR_JEXTEN_Pos) //!< 0x000000C0 
.equ ADC_JSQR_JEXTEN, ADC_JSQR_JEXTEN_Msk //!< ADC group injected external trigger polarity 
.equ ADC_JSQR_JEXTEN_0, (0x1 << ADC_JSQR_JEXTEN_Pos) //!< 0x00000040 
.equ ADC_JSQR_JEXTEN_1, (0x2 << ADC_JSQR_JEXTEN_Pos) //!< 0x00000080 
.equ ADC_JSQR_JSQ1_Pos, (8) 
.equ ADC_JSQR_JSQ1_Msk, (0x1F << ADC_JSQR_JSQ1_Pos) //!< 0x00001F00 
.equ ADC_JSQR_JSQ1, ADC_JSQR_JSQ1_Msk //!< ADC group injected sequencer rank 1 
.equ ADC_JSQR_JSQ1_0, (0x01 << ADC_JSQR_JSQ1_Pos) //!< 0x00000100 
.equ ADC_JSQR_JSQ1_1, (0x02 << ADC_JSQR_JSQ1_Pos) //!< 0x00000200 
.equ ADC_JSQR_JSQ1_2, (0x04 << ADC_JSQR_JSQ1_Pos) //!< 0x00000400 
.equ ADC_JSQR_JSQ1_3, (0x08 << ADC_JSQR_JSQ1_Pos) //!< 0x00000800 
.equ ADC_JSQR_JSQ1_4, (0x10 << ADC_JSQR_JSQ1_Pos) //!< 0x00001000 
.equ ADC_JSQR_JSQ2_Pos, (14) 
.equ ADC_JSQR_JSQ2_Msk, (0x1F << ADC_JSQR_JSQ2_Pos) //!< 0x0007C000 
.equ ADC_JSQR_JSQ2, ADC_JSQR_JSQ2_Msk //!< ADC group injected sequencer rank 2 
.equ ADC_JSQR_JSQ2_0, (0x01 << ADC_JSQR_JSQ2_Pos) //!< 0x00004000 
.equ ADC_JSQR_JSQ2_1, (0x02 << ADC_JSQR_JSQ2_Pos) //!< 0x00008000 
.equ ADC_JSQR_JSQ2_2, (0x04 << ADC_JSQR_JSQ2_Pos) //!< 0x00010000 
.equ ADC_JSQR_JSQ2_3, (0x08 << ADC_JSQR_JSQ2_Pos) //!< 0x00020000 
.equ ADC_JSQR_JSQ2_4, (0x10 << ADC_JSQR_JSQ2_Pos) //!< 0x00040000 
.equ ADC_JSQR_JSQ3_Pos, (20) 
.equ ADC_JSQR_JSQ3_Msk, (0x1F << ADC_JSQR_JSQ3_Pos) //!< 0x01F00000 
.equ ADC_JSQR_JSQ3, ADC_JSQR_JSQ3_Msk //!< ADC group injected sequencer rank 3 
.equ ADC_JSQR_JSQ3_0, (0x01 << ADC_JSQR_JSQ3_Pos) //!< 0x00100000 
.equ ADC_JSQR_JSQ3_1, (0x02 << ADC_JSQR_JSQ3_Pos) //!< 0x00200000 
.equ ADC_JSQR_JSQ3_2, (0x04 << ADC_JSQR_JSQ3_Pos) //!< 0x00400000 
.equ ADC_JSQR_JSQ3_3, (0x08 << ADC_JSQR_JSQ3_Pos) //!< 0x00800000 
.equ ADC_JSQR_JSQ3_4, (0x10 << ADC_JSQR_JSQ3_Pos) //!< 0x01000000 
.equ ADC_JSQR_JSQ4_Pos, (26) 
.equ ADC_JSQR_JSQ4_Msk, (0x1F << ADC_JSQR_JSQ4_Pos) //!< 0x7C000000 
.equ ADC_JSQR_JSQ4, ADC_JSQR_JSQ4_Msk //!< ADC group injected sequencer rank 4 
.equ ADC_JSQR_JSQ4_0, (0x01 << ADC_JSQR_JSQ4_Pos) //!< 0x04000000 
.equ ADC_JSQR_JSQ4_1, (0x02 << ADC_JSQR_JSQ4_Pos) //!< 0x08000000 
.equ ADC_JSQR_JSQ4_2, (0x04 << ADC_JSQR_JSQ4_Pos) //!< 0x10000000 
.equ ADC_JSQR_JSQ4_3, (0x08 << ADC_JSQR_JSQ4_Pos) //!< 0x20000000 
.equ ADC_JSQR_JSQ4_4, (0x10 << ADC_JSQR_JSQ4_Pos) //!< 0x40000000 
//*******************  Bit definition for ADC_OFR1 register  *****************
.equ ADC_OFR1_OFFSET1_Pos, (0) 
.equ ADC_OFR1_OFFSET1_Msk, (0xFFF << ADC_OFR1_OFFSET1_Pos) //!< 0x00000FFF 
.equ ADC_OFR1_OFFSET1, ADC_OFR1_OFFSET1_Msk //!< ADC offset number 1 offset level 
.equ ADC_OFR1_OFFSET1_0, (0x001 << ADC_OFR1_OFFSET1_Pos) //!< 0x00000001 
.equ ADC_OFR1_OFFSET1_1, (0x002 << ADC_OFR1_OFFSET1_Pos) //!< 0x00000002 
.equ ADC_OFR1_OFFSET1_2, (0x004 << ADC_OFR1_OFFSET1_Pos) //!< 0x00000004 
.equ ADC_OFR1_OFFSET1_3, (0x008 << ADC_OFR1_OFFSET1_Pos) //!< 0x00000008 
.equ ADC_OFR1_OFFSET1_4, (0x010 << ADC_OFR1_OFFSET1_Pos) //!< 0x00000010 
.equ ADC_OFR1_OFFSET1_5, (0x020 << ADC_OFR1_OFFSET1_Pos) //!< 0x00000020 
.equ ADC_OFR1_OFFSET1_6, (0x040 << ADC_OFR1_OFFSET1_Pos) //!< 0x00000040 
.equ ADC_OFR1_OFFSET1_7, (0x080 << ADC_OFR1_OFFSET1_Pos) //!< 0x00000080 
.equ ADC_OFR1_OFFSET1_8, (0x100 << ADC_OFR1_OFFSET1_Pos) //!< 0x00000100 
.equ ADC_OFR1_OFFSET1_9, (0x200 << ADC_OFR1_OFFSET1_Pos) //!< 0x00000200 
.equ ADC_OFR1_OFFSET1_10, (0x400 << ADC_OFR1_OFFSET1_Pos) //!< 0x00000400 
.equ ADC_OFR1_OFFSET1_11, (0x800 << ADC_OFR1_OFFSET1_Pos) //!< 0x00000800 
.equ ADC_OFR1_OFFSET1_CH_Pos, (26) 
.equ ADC_OFR1_OFFSET1_CH_Msk, (0x1F << ADC_OFR1_OFFSET1_CH_Pos) //!< 0x7C000000 
.equ ADC_OFR1_OFFSET1_CH, ADC_OFR1_OFFSET1_CH_Msk //!< ADC offset number 1 channel selection 
.equ ADC_OFR1_OFFSET1_CH_0, (0x01 << ADC_OFR1_OFFSET1_CH_Pos) //!< 0x04000000 
.equ ADC_OFR1_OFFSET1_CH_1, (0x02 << ADC_OFR1_OFFSET1_CH_Pos) //!< 0x08000000 
.equ ADC_OFR1_OFFSET1_CH_2, (0x04 << ADC_OFR1_OFFSET1_CH_Pos) //!< 0x10000000 
.equ ADC_OFR1_OFFSET1_CH_3, (0x08 << ADC_OFR1_OFFSET1_CH_Pos) //!< 0x20000000 
.equ ADC_OFR1_OFFSET1_CH_4, (0x10 << ADC_OFR1_OFFSET1_CH_Pos) //!< 0x40000000 
.equ ADC_OFR1_OFFSET1_EN_Pos, (31) 
.equ ADC_OFR1_OFFSET1_EN_Msk, (0x1 << ADC_OFR1_OFFSET1_EN_Pos) //!< 0x80000000 
.equ ADC_OFR1_OFFSET1_EN, ADC_OFR1_OFFSET1_EN_Msk //!< ADC offset number 1 enable 
//*******************  Bit definition for ADC_OFR2 register  *****************
.equ ADC_OFR2_OFFSET2_Pos, (0) 
.equ ADC_OFR2_OFFSET2_Msk, (0xFFF << ADC_OFR2_OFFSET2_Pos) //!< 0x00000FFF 
.equ ADC_OFR2_OFFSET2, ADC_OFR2_OFFSET2_Msk //!< ADC offset number 2 offset level 
.equ ADC_OFR2_OFFSET2_0, (0x001 << ADC_OFR2_OFFSET2_Pos) //!< 0x00000001 
.equ ADC_OFR2_OFFSET2_1, (0x002 << ADC_OFR2_OFFSET2_Pos) //!< 0x00000002 
.equ ADC_OFR2_OFFSET2_2, (0x004 << ADC_OFR2_OFFSET2_Pos) //!< 0x00000004 
.equ ADC_OFR2_OFFSET2_3, (0x008 << ADC_OFR2_OFFSET2_Pos) //!< 0x00000008 
.equ ADC_OFR2_OFFSET2_4, (0x010 << ADC_OFR2_OFFSET2_Pos) //!< 0x00000010 
.equ ADC_OFR2_OFFSET2_5, (0x020 << ADC_OFR2_OFFSET2_Pos) //!< 0x00000020 
.equ ADC_OFR2_OFFSET2_6, (0x040 << ADC_OFR2_OFFSET2_Pos) //!< 0x00000040 
.equ ADC_OFR2_OFFSET2_7, (0x080 << ADC_OFR2_OFFSET2_Pos) //!< 0x00000080 
.equ ADC_OFR2_OFFSET2_8, (0x100 << ADC_OFR2_OFFSET2_Pos) //!< 0x00000100 
.equ ADC_OFR2_OFFSET2_9, (0x200 << ADC_OFR2_OFFSET2_Pos) //!< 0x00000200 
.equ ADC_OFR2_OFFSET2_10, (0x400 << ADC_OFR2_OFFSET2_Pos) //!< 0x00000400 
.equ ADC_OFR2_OFFSET2_11, (0x800 << ADC_OFR2_OFFSET2_Pos) //!< 0x00000800 
.equ ADC_OFR2_OFFSET2_CH_Pos, (26) 
.equ ADC_OFR2_OFFSET2_CH_Msk, (0x1F << ADC_OFR2_OFFSET2_CH_Pos) //!< 0x7C000000 
.equ ADC_OFR2_OFFSET2_CH, ADC_OFR2_OFFSET2_CH_Msk //!< ADC offset number 2 channel selection 
.equ ADC_OFR2_OFFSET2_CH_0, (0x01 << ADC_OFR2_OFFSET2_CH_Pos) //!< 0x04000000 
.equ ADC_OFR2_OFFSET2_CH_1, (0x02 << ADC_OFR2_OFFSET2_CH_Pos) //!< 0x08000000 
.equ ADC_OFR2_OFFSET2_CH_2, (0x04 << ADC_OFR2_OFFSET2_CH_Pos) //!< 0x10000000 
.equ ADC_OFR2_OFFSET2_CH_3, (0x08 << ADC_OFR2_OFFSET2_CH_Pos) //!< 0x20000000 
.equ ADC_OFR2_OFFSET2_CH_4, (0x10 << ADC_OFR2_OFFSET2_CH_Pos) //!< 0x40000000 
.equ ADC_OFR2_OFFSET2_EN_Pos, (31) 
.equ ADC_OFR2_OFFSET2_EN_Msk, (0x1 << ADC_OFR2_OFFSET2_EN_Pos) //!< 0x80000000 
.equ ADC_OFR2_OFFSET2_EN, ADC_OFR2_OFFSET2_EN_Msk //!< ADC offset number 2 enable 
//*******************  Bit definition for ADC_OFR3 register  *****************
.equ ADC_OFR3_OFFSET3_Pos, (0) 
.equ ADC_OFR3_OFFSET3_Msk, (0xFFF << ADC_OFR3_OFFSET3_Pos) //!< 0x00000FFF 
.equ ADC_OFR3_OFFSET3, ADC_OFR3_OFFSET3_Msk //!< ADC offset number 3 offset level 
.equ ADC_OFR3_OFFSET3_0, (0x001 << ADC_OFR3_OFFSET3_Pos) //!< 0x00000001 
.equ ADC_OFR3_OFFSET3_1, (0x002 << ADC_OFR3_OFFSET3_Pos) //!< 0x00000002 
.equ ADC_OFR3_OFFSET3_2, (0x004 << ADC_OFR3_OFFSET3_Pos) //!< 0x00000004 
.equ ADC_OFR3_OFFSET3_3, (0x008 << ADC_OFR3_OFFSET3_Pos) //!< 0x00000008 
.equ ADC_OFR3_OFFSET3_4, (0x010 << ADC_OFR3_OFFSET3_Pos) //!< 0x00000010 
.equ ADC_OFR3_OFFSET3_5, (0x020 << ADC_OFR3_OFFSET3_Pos) //!< 0x00000020 
.equ ADC_OFR3_OFFSET3_6, (0x040 << ADC_OFR3_OFFSET3_Pos) //!< 0x00000040 
.equ ADC_OFR3_OFFSET3_7, (0x080 << ADC_OFR3_OFFSET3_Pos) //!< 0x00000080 
.equ ADC_OFR3_OFFSET3_8, (0x100 << ADC_OFR3_OFFSET3_Pos) //!< 0x00000100 
.equ ADC_OFR3_OFFSET3_9, (0x200 << ADC_OFR3_OFFSET3_Pos) //!< 0x00000200 
.equ ADC_OFR3_OFFSET3_10, (0x400 << ADC_OFR3_OFFSET3_Pos) //!< 0x00000400 
.equ ADC_OFR3_OFFSET3_11, (0x800 << ADC_OFR3_OFFSET3_Pos) //!< 0x00000800 
.equ ADC_OFR3_OFFSET3_CH_Pos, (26) 
.equ ADC_OFR3_OFFSET3_CH_Msk, (0x1F << ADC_OFR3_OFFSET3_CH_Pos) //!< 0x7C000000 
.equ ADC_OFR3_OFFSET3_CH, ADC_OFR3_OFFSET3_CH_Msk //!< ADC offset number 3 channel selection 
.equ ADC_OFR3_OFFSET3_CH_0, (0x01 << ADC_OFR3_OFFSET3_CH_Pos) //!< 0x04000000 
.equ ADC_OFR3_OFFSET3_CH_1, (0x02 << ADC_OFR3_OFFSET3_CH_Pos) //!< 0x08000000 
.equ ADC_OFR3_OFFSET3_CH_2, (0x04 << ADC_OFR3_OFFSET3_CH_Pos) //!< 0x10000000 
.equ ADC_OFR3_OFFSET3_CH_3, (0x08 << ADC_OFR3_OFFSET3_CH_Pos) //!< 0x20000000 
.equ ADC_OFR3_OFFSET3_CH_4, (0x10 << ADC_OFR3_OFFSET3_CH_Pos) //!< 0x40000000 
.equ ADC_OFR3_OFFSET3_EN_Pos, (31) 
.equ ADC_OFR3_OFFSET3_EN_Msk, (0x1 << ADC_OFR3_OFFSET3_EN_Pos) //!< 0x80000000 
.equ ADC_OFR3_OFFSET3_EN, ADC_OFR3_OFFSET3_EN_Msk //!< ADC offset number 3 enable 
//*******************  Bit definition for ADC_OFR4 register  *****************
.equ ADC_OFR4_OFFSET4_Pos, (0) 
.equ ADC_OFR4_OFFSET4_Msk, (0xFFF << ADC_OFR4_OFFSET4_Pos) //!< 0x00000FFF 
.equ ADC_OFR4_OFFSET4, ADC_OFR4_OFFSET4_Msk //!< ADC offset number 4 offset level 
.equ ADC_OFR4_OFFSET4_0, (0x001 << ADC_OFR4_OFFSET4_Pos) //!< 0x00000001 
.equ ADC_OFR4_OFFSET4_1, (0x002 << ADC_OFR4_OFFSET4_Pos) //!< 0x00000002 
.equ ADC_OFR4_OFFSET4_2, (0x004 << ADC_OFR4_OFFSET4_Pos) //!< 0x00000004 
.equ ADC_OFR4_OFFSET4_3, (0x008 << ADC_OFR4_OFFSET4_Pos) //!< 0x00000008 
.equ ADC_OFR4_OFFSET4_4, (0x010 << ADC_OFR4_OFFSET4_Pos) //!< 0x00000010 
.equ ADC_OFR4_OFFSET4_5, (0x020 << ADC_OFR4_OFFSET4_Pos) //!< 0x00000020 
.equ ADC_OFR4_OFFSET4_6, (0x040 << ADC_OFR4_OFFSET4_Pos) //!< 0x00000040 
.equ ADC_OFR4_OFFSET4_7, (0x080 << ADC_OFR4_OFFSET4_Pos) //!< 0x00000080 
.equ ADC_OFR4_OFFSET4_8, (0x100 << ADC_OFR4_OFFSET4_Pos) //!< 0x00000100 
.equ ADC_OFR4_OFFSET4_9, (0x200 << ADC_OFR4_OFFSET4_Pos) //!< 0x00000200 
.equ ADC_OFR4_OFFSET4_10, (0x400 << ADC_OFR4_OFFSET4_Pos) //!< 0x00000400 
.equ ADC_OFR4_OFFSET4_11, (0x800 << ADC_OFR4_OFFSET4_Pos) //!< 0x00000800 
.equ ADC_OFR4_OFFSET4_CH_Pos, (26) 
.equ ADC_OFR4_OFFSET4_CH_Msk, (0x1F << ADC_OFR4_OFFSET4_CH_Pos) //!< 0x7C000000 
.equ ADC_OFR4_OFFSET4_CH, ADC_OFR4_OFFSET4_CH_Msk //!< ADC offset number 4 channel selection 
.equ ADC_OFR4_OFFSET4_CH_0, (0x01 << ADC_OFR4_OFFSET4_CH_Pos) //!< 0x04000000 
.equ ADC_OFR4_OFFSET4_CH_1, (0x02 << ADC_OFR4_OFFSET4_CH_Pos) //!< 0x08000000 
.equ ADC_OFR4_OFFSET4_CH_2, (0x04 << ADC_OFR4_OFFSET4_CH_Pos) //!< 0x10000000 
.equ ADC_OFR4_OFFSET4_CH_3, (0x08 << ADC_OFR4_OFFSET4_CH_Pos) //!< 0x20000000 
.equ ADC_OFR4_OFFSET4_CH_4, (0x10 << ADC_OFR4_OFFSET4_CH_Pos) //!< 0x40000000 
.equ ADC_OFR4_OFFSET4_EN_Pos, (31) 
.equ ADC_OFR4_OFFSET4_EN_Msk, (0x1 << ADC_OFR4_OFFSET4_EN_Pos) //!< 0x80000000 
.equ ADC_OFR4_OFFSET4_EN, ADC_OFR4_OFFSET4_EN_Msk //!< ADC offset number 4 enable 
//*******************  Bit definition for ADC_JDR1 register  *****************
.equ ADC_JDR1_JDATA_Pos, (0) 
.equ ADC_JDR1_JDATA_Msk, (0xFFFF << ADC_JDR1_JDATA_Pos) //!< 0x0000FFFF 
.equ ADC_JDR1_JDATA, ADC_JDR1_JDATA_Msk //!< ADC group injected sequencer rank 1 conversion data 
.equ ADC_JDR1_JDATA_0, (0x0001 << ADC_JDR1_JDATA_Pos) //!< 0x00000001 
.equ ADC_JDR1_JDATA_1, (0x0002 << ADC_JDR1_JDATA_Pos) //!< 0x00000002 
.equ ADC_JDR1_JDATA_2, (0x0004 << ADC_JDR1_JDATA_Pos) //!< 0x00000004 
.equ ADC_JDR1_JDATA_3, (0x0008 << ADC_JDR1_JDATA_Pos) //!< 0x00000008 
.equ ADC_JDR1_JDATA_4, (0x0010 << ADC_JDR1_JDATA_Pos) //!< 0x00000010 
.equ ADC_JDR1_JDATA_5, (0x0020 << ADC_JDR1_JDATA_Pos) //!< 0x00000020 
.equ ADC_JDR1_JDATA_6, (0x0040 << ADC_JDR1_JDATA_Pos) //!< 0x00000040 
.equ ADC_JDR1_JDATA_7, (0x0080 << ADC_JDR1_JDATA_Pos) //!< 0x00000080 
.equ ADC_JDR1_JDATA_8, (0x0100 << ADC_JDR1_JDATA_Pos) //!< 0x00000100 
.equ ADC_JDR1_JDATA_9, (0x0200 << ADC_JDR1_JDATA_Pos) //!< 0x00000200 
.equ ADC_JDR1_JDATA_10, (0x0400 << ADC_JDR1_JDATA_Pos) //!< 0x00000400 
.equ ADC_JDR1_JDATA_11, (0x0800 << ADC_JDR1_JDATA_Pos) //!< 0x00000800 
.equ ADC_JDR1_JDATA_12, (0x1000 << ADC_JDR1_JDATA_Pos) //!< 0x00001000 
.equ ADC_JDR1_JDATA_13, (0x2000 << ADC_JDR1_JDATA_Pos) //!< 0x00002000 
.equ ADC_JDR1_JDATA_14, (0x4000 << ADC_JDR1_JDATA_Pos) //!< 0x00004000 
.equ ADC_JDR1_JDATA_15, (0x8000 << ADC_JDR1_JDATA_Pos) //!< 0x00008000 
//*******************  Bit definition for ADC_JDR2 register  *****************
.equ ADC_JDR2_JDATA_Pos, (0) 
.equ ADC_JDR2_JDATA_Msk, (0xFFFF << ADC_JDR2_JDATA_Pos) //!< 0x0000FFFF 
.equ ADC_JDR2_JDATA, ADC_JDR2_JDATA_Msk //!< ADC group injected sequencer rank 2 conversion data 
.equ ADC_JDR2_JDATA_0, (0x0001 << ADC_JDR2_JDATA_Pos) //!< 0x00000001 
.equ ADC_JDR2_JDATA_1, (0x0002 << ADC_JDR2_JDATA_Pos) //!< 0x00000002 
.equ ADC_JDR2_JDATA_2, (0x0004 << ADC_JDR2_JDATA_Pos) //!< 0x00000004 
.equ ADC_JDR2_JDATA_3, (0x0008 << ADC_JDR2_JDATA_Pos) //!< 0x00000008 
.equ ADC_JDR2_JDATA_4, (0x0010 << ADC_JDR2_JDATA_Pos) //!< 0x00000010 
.equ ADC_JDR2_JDATA_5, (0x0020 << ADC_JDR2_JDATA_Pos) //!< 0x00000020 
.equ ADC_JDR2_JDATA_6, (0x0040 << ADC_JDR2_JDATA_Pos) //!< 0x00000040 
.equ ADC_JDR2_JDATA_7, (0x0080 << ADC_JDR2_JDATA_Pos) //!< 0x00000080 
.equ ADC_JDR2_JDATA_8, (0x0100 << ADC_JDR2_JDATA_Pos) //!< 0x00000100 
.equ ADC_JDR2_JDATA_9, (0x0200 << ADC_JDR2_JDATA_Pos) //!< 0x00000200 
.equ ADC_JDR2_JDATA_10, (0x0400 << ADC_JDR2_JDATA_Pos) //!< 0x00000400 
.equ ADC_JDR2_JDATA_11, (0x0800 << ADC_JDR2_JDATA_Pos) //!< 0x00000800 
.equ ADC_JDR2_JDATA_12, (0x1000 << ADC_JDR2_JDATA_Pos) //!< 0x00001000 
.equ ADC_JDR2_JDATA_13, (0x2000 << ADC_JDR2_JDATA_Pos) //!< 0x00002000 
.equ ADC_JDR2_JDATA_14, (0x4000 << ADC_JDR2_JDATA_Pos) //!< 0x00004000 
.equ ADC_JDR2_JDATA_15, (0x8000 << ADC_JDR2_JDATA_Pos) //!< 0x00008000 
//*******************  Bit definition for ADC_JDR3 register  *****************
.equ ADC_JDR3_JDATA_Pos, (0) 
.equ ADC_JDR3_JDATA_Msk, (0xFFFF << ADC_JDR3_JDATA_Pos) //!< 0x0000FFFF 
.equ ADC_JDR3_JDATA, ADC_JDR3_JDATA_Msk //!< ADC group injected sequencer rank 3 conversion data 
.equ ADC_JDR3_JDATA_0, (0x0001 << ADC_JDR3_JDATA_Pos) //!< 0x00000001 
.equ ADC_JDR3_JDATA_1, (0x0002 << ADC_JDR3_JDATA_Pos) //!< 0x00000002 
.equ ADC_JDR3_JDATA_2, (0x0004 << ADC_JDR3_JDATA_Pos) //!< 0x00000004 
.equ ADC_JDR3_JDATA_3, (0x0008 << ADC_JDR3_JDATA_Pos) //!< 0x00000008 
.equ ADC_JDR3_JDATA_4, (0x0010 << ADC_JDR3_JDATA_Pos) //!< 0x00000010 
.equ ADC_JDR3_JDATA_5, (0x0020 << ADC_JDR3_JDATA_Pos) //!< 0x00000020 
.equ ADC_JDR3_JDATA_6, (0x0040 << ADC_JDR3_JDATA_Pos) //!< 0x00000040 
.equ ADC_JDR3_JDATA_7, (0x0080 << ADC_JDR3_JDATA_Pos) //!< 0x00000080 
.equ ADC_JDR3_JDATA_8, (0x0100 << ADC_JDR3_JDATA_Pos) //!< 0x00000100 
.equ ADC_JDR3_JDATA_9, (0x0200 << ADC_JDR3_JDATA_Pos) //!< 0x00000200 
.equ ADC_JDR3_JDATA_10, (0x0400 << ADC_JDR3_JDATA_Pos) //!< 0x00000400 
.equ ADC_JDR3_JDATA_11, (0x0800 << ADC_JDR3_JDATA_Pos) //!< 0x00000800 
.equ ADC_JDR3_JDATA_12, (0x1000 << ADC_JDR3_JDATA_Pos) //!< 0x00001000 
.equ ADC_JDR3_JDATA_13, (0x2000 << ADC_JDR3_JDATA_Pos) //!< 0x00002000 
.equ ADC_JDR3_JDATA_14, (0x4000 << ADC_JDR3_JDATA_Pos) //!< 0x00004000 
.equ ADC_JDR3_JDATA_15, (0x8000 << ADC_JDR3_JDATA_Pos) //!< 0x00008000 
//*******************  Bit definition for ADC_JDR4 register  *****************
.equ ADC_JDR4_JDATA_Pos, (0) 
.equ ADC_JDR4_JDATA_Msk, (0xFFFF << ADC_JDR4_JDATA_Pos) //!< 0x0000FFFF 
.equ ADC_JDR4_JDATA, ADC_JDR4_JDATA_Msk //!< ADC group injected sequencer rank 4 conversion data 
.equ ADC_JDR4_JDATA_0, (0x0001 << ADC_JDR4_JDATA_Pos) //!< 0x00000001 
.equ ADC_JDR4_JDATA_1, (0x0002 << ADC_JDR4_JDATA_Pos) //!< 0x00000002 
.equ ADC_JDR4_JDATA_2, (0x0004 << ADC_JDR4_JDATA_Pos) //!< 0x00000004 
.equ ADC_JDR4_JDATA_3, (0x0008 << ADC_JDR4_JDATA_Pos) //!< 0x00000008 
.equ ADC_JDR4_JDATA_4, (0x0010 << ADC_JDR4_JDATA_Pos) //!< 0x00000010 
.equ ADC_JDR4_JDATA_5, (0x0020 << ADC_JDR4_JDATA_Pos) //!< 0x00000020 
.equ ADC_JDR4_JDATA_6, (0x0040 << ADC_JDR4_JDATA_Pos) //!< 0x00000040 
.equ ADC_JDR4_JDATA_7, (0x0080 << ADC_JDR4_JDATA_Pos) //!< 0x00000080 
.equ ADC_JDR4_JDATA_8, (0x0100 << ADC_JDR4_JDATA_Pos) //!< 0x00000100 
.equ ADC_JDR4_JDATA_9, (0x0200 << ADC_JDR4_JDATA_Pos) //!< 0x00000200 
.equ ADC_JDR4_JDATA_10, (0x0400 << ADC_JDR4_JDATA_Pos) //!< 0x00000400 
.equ ADC_JDR4_JDATA_11, (0x0800 << ADC_JDR4_JDATA_Pos) //!< 0x00000800 
.equ ADC_JDR4_JDATA_12, (0x1000 << ADC_JDR4_JDATA_Pos) //!< 0x00001000 
.equ ADC_JDR4_JDATA_13, (0x2000 << ADC_JDR4_JDATA_Pos) //!< 0x00002000 
.equ ADC_JDR4_JDATA_14, (0x4000 << ADC_JDR4_JDATA_Pos) //!< 0x00004000 
.equ ADC_JDR4_JDATA_15, (0x8000 << ADC_JDR4_JDATA_Pos) //!< 0x00008000 
//*******************  Bit definition for ADC_AWD2CR register  ***************
.equ ADC_AWD2CR_AWD2CH_Pos, (0) 
.equ ADC_AWD2CR_AWD2CH_Msk, (0x7FFFF << ADC_AWD2CR_AWD2CH_Pos) //!< 0x0007FFFF 
.equ ADC_AWD2CR_AWD2CH, ADC_AWD2CR_AWD2CH_Msk //!< ADC analog watchdog 2 monitored channel selection 
.equ ADC_AWD2CR_AWD2CH_0, (0x00001 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00000001 
.equ ADC_AWD2CR_AWD2CH_1, (0x00002 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00000002 
.equ ADC_AWD2CR_AWD2CH_2, (0x00004 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00000004 
.equ ADC_AWD2CR_AWD2CH_3, (0x00008 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00000008 
.equ ADC_AWD2CR_AWD2CH_4, (0x00010 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00000010 
.equ ADC_AWD2CR_AWD2CH_5, (0x00020 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00000020 
.equ ADC_AWD2CR_AWD2CH_6, (0x00040 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00000040 
.equ ADC_AWD2CR_AWD2CH_7, (0x00080 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00000080 
.equ ADC_AWD2CR_AWD2CH_8, (0x00100 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00000100 
.equ ADC_AWD2CR_AWD2CH_9, (0x00200 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00000200 
.equ ADC_AWD2CR_AWD2CH_10, (0x00400 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00000400 
.equ ADC_AWD2CR_AWD2CH_11, (0x00800 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00000800 
.equ ADC_AWD2CR_AWD2CH_12, (0x01000 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00001000 
.equ ADC_AWD2CR_AWD2CH_13, (0x02000 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00002000 
.equ ADC_AWD2CR_AWD2CH_14, (0x04000 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00004000 
.equ ADC_AWD2CR_AWD2CH_15, (0x08000 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00008000 
.equ ADC_AWD2CR_AWD2CH_16, (0x10000 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00010000 
.equ ADC_AWD2CR_AWD2CH_17, (0x20000 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00020000 
.equ ADC_AWD2CR_AWD2CH_18, (0x40000 << ADC_AWD2CR_AWD2CH_Pos) //!< 0x00040000 
//*******************  Bit definition for ADC_AWD3CR register  ***************
.equ ADC_AWD3CR_AWD3CH_Pos, (0) 
.equ ADC_AWD3CR_AWD3CH_Msk, (0x7FFFF << ADC_AWD3CR_AWD3CH_Pos) //!< 0x0007FFFF 
.equ ADC_AWD3CR_AWD3CH, ADC_AWD3CR_AWD3CH_Msk //!< ADC analog watchdog 3 monitored channel selection 
.equ ADC_AWD3CR_AWD3CH_0, (0x00001 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00000001 
.equ ADC_AWD3CR_AWD3CH_1, (0x00002 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00000002 
.equ ADC_AWD3CR_AWD3CH_2, (0x00004 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00000004 
.equ ADC_AWD3CR_AWD3CH_3, (0x00008 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00000008 
.equ ADC_AWD3CR_AWD3CH_4, (0x00010 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00000010 
.equ ADC_AWD3CR_AWD3CH_5, (0x00020 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00000020 
.equ ADC_AWD3CR_AWD3CH_6, (0x00040 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00000040 
.equ ADC_AWD3CR_AWD3CH_7, (0x00080 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00000080 
.equ ADC_AWD3CR_AWD3CH_8, (0x00100 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00000100 
.equ ADC_AWD3CR_AWD3CH_9, (0x00200 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00000200 
.equ ADC_AWD3CR_AWD3CH_10, (0x00400 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00000400 
.equ ADC_AWD3CR_AWD3CH_11, (0x00800 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00000800 
.equ ADC_AWD3CR_AWD3CH_12, (0x01000 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00001000 
.equ ADC_AWD3CR_AWD3CH_13, (0x02000 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00002000 
.equ ADC_AWD3CR_AWD3CH_14, (0x04000 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00004000 
.equ ADC_AWD3CR_AWD3CH_15, (0x08000 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00008000 
.equ ADC_AWD3CR_AWD3CH_16, (0x10000 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00010000 
.equ ADC_AWD3CR_AWD3CH_17, (0x20000 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00020000 
.equ ADC_AWD3CR_AWD3CH_18, (0x40000 << ADC_AWD3CR_AWD3CH_Pos) //!< 0x00040000 
//*******************  Bit definition for ADC_DIFSEL register  ***************
.equ ADC_DIFSEL_DIFSEL_Pos, (0) 
.equ ADC_DIFSEL_DIFSEL_Msk, (0x7FFFF << ADC_DIFSEL_DIFSEL_Pos) //!< 0x0007FFFF 
.equ ADC_DIFSEL_DIFSEL, ADC_DIFSEL_DIFSEL_Msk //!< ADC channel differential or single-ended mode 
.equ ADC_DIFSEL_DIFSEL_0, (0x00001 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00000001 
.equ ADC_DIFSEL_DIFSEL_1, (0x00002 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00000002 
.equ ADC_DIFSEL_DIFSEL_2, (0x00004 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00000004 
.equ ADC_DIFSEL_DIFSEL_3, (0x00008 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00000008 
.equ ADC_DIFSEL_DIFSEL_4, (0x00010 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00000010 
.equ ADC_DIFSEL_DIFSEL_5, (0x00020 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00000020 
.equ ADC_DIFSEL_DIFSEL_6, (0x00040 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00000040 
.equ ADC_DIFSEL_DIFSEL_7, (0x00080 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00000080 
.equ ADC_DIFSEL_DIFSEL_8, (0x00100 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00000100 
.equ ADC_DIFSEL_DIFSEL_9, (0x00200 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00000200 
.equ ADC_DIFSEL_DIFSEL_10, (0x00400 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00000400 
.equ ADC_DIFSEL_DIFSEL_11, (0x00800 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00000800 
.equ ADC_DIFSEL_DIFSEL_12, (0x01000 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00001000 
.equ ADC_DIFSEL_DIFSEL_13, (0x02000 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00002000 
.equ ADC_DIFSEL_DIFSEL_14, (0x04000 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00004000 
.equ ADC_DIFSEL_DIFSEL_15, (0x08000 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00008000 
.equ ADC_DIFSEL_DIFSEL_16, (0x10000 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00010000 
.equ ADC_DIFSEL_DIFSEL_17, (0x20000 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00020000 
.equ ADC_DIFSEL_DIFSEL_18, (0x40000 << ADC_DIFSEL_DIFSEL_Pos) //!< 0x00040000 
//*******************  Bit definition for ADC_CALFACT register  **************
.equ ADC_CALFACT_CALFACT_S_Pos, (0) 
.equ ADC_CALFACT_CALFACT_S_Msk, (0x7F << ADC_CALFACT_CALFACT_S_Pos) //!< 0x0000007F 
.equ ADC_CALFACT_CALFACT_S, ADC_CALFACT_CALFACT_S_Msk //!< ADC calibration factor in single-ended mode 
.equ ADC_CALFACT_CALFACT_S_0, (0x01 << ADC_CALFACT_CALFACT_S_Pos) //!< 0x00000001 
.equ ADC_CALFACT_CALFACT_S_1, (0x02 << ADC_CALFACT_CALFACT_S_Pos) //!< 0x00000002 
.equ ADC_CALFACT_CALFACT_S_2, (0x04 << ADC_CALFACT_CALFACT_S_Pos) //!< 0x00000004 
.equ ADC_CALFACT_CALFACT_S_3, (0x08 << ADC_CALFACT_CALFACT_S_Pos) //!< 0x00000008 
.equ ADC_CALFACT_CALFACT_S_4, (0x10 << ADC_CALFACT_CALFACT_S_Pos) //!< 0x00000010 
.equ ADC_CALFACT_CALFACT_S_5, (0x20 << ADC_CALFACT_CALFACT_S_Pos) //!< 0x00000020 
.equ ADC_CALFACT_CALFACT_S_6, (0x40 << ADC_CALFACT_CALFACT_S_Pos) //!< 0x00000040 
.equ ADC_CALFACT_CALFACT_D_Pos, (16) 
.equ ADC_CALFACT_CALFACT_D_Msk, (0x7F << ADC_CALFACT_CALFACT_D_Pos) //!< 0x007F0000 
.equ ADC_CALFACT_CALFACT_D, ADC_CALFACT_CALFACT_D_Msk //!< ADC calibration factor in differential mode 
.equ ADC_CALFACT_CALFACT_D_0, (0x01 << ADC_CALFACT_CALFACT_D_Pos) //!< 0x00010000 
.equ ADC_CALFACT_CALFACT_D_1, (0x02 << ADC_CALFACT_CALFACT_D_Pos) //!< 0x00020000 
.equ ADC_CALFACT_CALFACT_D_2, (0x04 << ADC_CALFACT_CALFACT_D_Pos) //!< 0x00040000 
.equ ADC_CALFACT_CALFACT_D_3, (0x08 << ADC_CALFACT_CALFACT_D_Pos) //!< 0x00080000 
.equ ADC_CALFACT_CALFACT_D_4, (0x10 << ADC_CALFACT_CALFACT_D_Pos) //!< 0x00100000 
.equ ADC_CALFACT_CALFACT_D_5, (0x20 << ADC_CALFACT_CALFACT_D_Pos) //!< 0x00200000 
.equ ADC_CALFACT_CALFACT_D_6, (0x40 << ADC_CALFACT_CALFACT_D_Pos) //!< 0x00400000 
//************************  ADC Common registers  ****************************
//**************  Bit definition for ADC12_COMMON_CSR register  **************
.equ ADC12_CSR_ADRDY_MST_Pos, (0) 
.equ ADC12_CSR_ADRDY_MST_Msk, (0x1 << ADC12_CSR_ADRDY_MST_Pos) //!< 0x00000001 
.equ ADC12_CSR_ADRDY_MST, ADC12_CSR_ADRDY_MST_Msk //!< Master ADC ready 
.equ ADC12_CSR_ADRDY_EOSMP_MST_Pos, (1) 
.equ ADC12_CSR_ADRDY_EOSMP_MST_Msk, (0x1 << ADC12_CSR_ADRDY_EOSMP_MST_Pos) //!< 0x00000002 
.equ ADC12_CSR_ADRDY_EOSMP_MST, ADC12_CSR_ADRDY_EOSMP_MST_Msk //!< End of sampling phase flag of the master ADC 
.equ ADC12_CSR_ADRDY_EOC_MST_Pos, (2) 
.equ ADC12_CSR_ADRDY_EOC_MST_Msk, (0x1 << ADC12_CSR_ADRDY_EOC_MST_Pos) //!< 0x00000004 
.equ ADC12_CSR_ADRDY_EOC_MST, ADC12_CSR_ADRDY_EOC_MST_Msk //!< End of regular conversion of the master ADC 
.equ ADC12_CSR_ADRDY_EOS_MST_Pos, (3) 
.equ ADC12_CSR_ADRDY_EOS_MST_Msk, (0x1 << ADC12_CSR_ADRDY_EOS_MST_Pos) //!< 0x00000008 
.equ ADC12_CSR_ADRDY_EOS_MST, ADC12_CSR_ADRDY_EOS_MST_Msk //!< End of regular sequence flag of the master ADC 
.equ ADC12_CSR_ADRDY_OVR_MST_Pos, (4) 
.equ ADC12_CSR_ADRDY_OVR_MST_Msk, (0x1 << ADC12_CSR_ADRDY_OVR_MST_Pos) //!< 0x00000010 
.equ ADC12_CSR_ADRDY_OVR_MST, ADC12_CSR_ADRDY_OVR_MST_Msk //!< Overrun flag of the master ADC 
.equ ADC12_CSR_ADRDY_JEOC_MST_Pos, (5) 
.equ ADC12_CSR_ADRDY_JEOC_MST_Msk, (0x1 << ADC12_CSR_ADRDY_JEOC_MST_Pos) //!< 0x00000020 
.equ ADC12_CSR_ADRDY_JEOC_MST, ADC12_CSR_ADRDY_JEOC_MST_Msk //!< End of injected conversion of the master ADC 
.equ ADC12_CSR_ADRDY_JEOS_MST_Pos, (6) 
.equ ADC12_CSR_ADRDY_JEOS_MST_Msk, (0x1 << ADC12_CSR_ADRDY_JEOS_MST_Pos) //!< 0x00000040 
.equ ADC12_CSR_ADRDY_JEOS_MST, ADC12_CSR_ADRDY_JEOS_MST_Msk //!< End of injected sequence flag of the master ADC 
.equ ADC12_CSR_AWD1_MST_Pos, (7) 
.equ ADC12_CSR_AWD1_MST_Msk, (0x1 << ADC12_CSR_AWD1_MST_Pos) //!< 0x00000080 
.equ ADC12_CSR_AWD1_MST, ADC12_CSR_AWD1_MST_Msk //!< Analog watchdog 1 flag of the master ADC 
.equ ADC12_CSR_AWD2_MST_Pos, (8) 
.equ ADC12_CSR_AWD2_MST_Msk, (0x1 << ADC12_CSR_AWD2_MST_Pos) //!< 0x00000100 
.equ ADC12_CSR_AWD2_MST, ADC12_CSR_AWD2_MST_Msk //!< Analog watchdog 2 flag of the master ADC 
.equ ADC12_CSR_AWD3_MST_Pos, (9) 
.equ ADC12_CSR_AWD3_MST_Msk, (0x1 << ADC12_CSR_AWD3_MST_Pos) //!< 0x00000200 
.equ ADC12_CSR_AWD3_MST, ADC12_CSR_AWD3_MST_Msk //!< Analog watchdog 3 flag of the master ADC 
.equ ADC12_CSR_JQOVF_MST_Pos, (10) 
.equ ADC12_CSR_JQOVF_MST_Msk, (0x1 << ADC12_CSR_JQOVF_MST_Pos) //!< 0x00000400 
.equ ADC12_CSR_JQOVF_MST, ADC12_CSR_JQOVF_MST_Msk //!< Injected context queue overflow flag of the master ADC 
.equ ADC12_CSR_ADRDY_SLV_Pos, (16) 
.equ ADC12_CSR_ADRDY_SLV_Msk, (0x1 << ADC12_CSR_ADRDY_SLV_Pos) //!< 0x00010000 
.equ ADC12_CSR_ADRDY_SLV, ADC12_CSR_ADRDY_SLV_Msk //!< Slave ADC ready 
.equ ADC12_CSR_ADRDY_EOSMP_SLV_Pos, (17) 
.equ ADC12_CSR_ADRDY_EOSMP_SLV_Msk, (0x1 << ADC12_CSR_ADRDY_EOSMP_SLV_Pos) //!< 0x00020000 
.equ ADC12_CSR_ADRDY_EOSMP_SLV, ADC12_CSR_ADRDY_EOSMP_SLV_Msk //!< End of sampling phase flag of the slave ADC 
.equ ADC12_CSR_ADRDY_EOC_SLV_Pos, (18) 
.equ ADC12_CSR_ADRDY_EOC_SLV_Msk, (0x1 << ADC12_CSR_ADRDY_EOC_SLV_Pos) //!< 0x00040000 
.equ ADC12_CSR_ADRDY_EOC_SLV, ADC12_CSR_ADRDY_EOC_SLV_Msk //!< End of regular conversion of the slave ADC 
.equ ADC12_CSR_ADRDY_EOS_SLV_Pos, (19) 
.equ ADC12_CSR_ADRDY_EOS_SLV_Msk, (0x1 << ADC12_CSR_ADRDY_EOS_SLV_Pos) //!< 0x00080000 
.equ ADC12_CSR_ADRDY_EOS_SLV, ADC12_CSR_ADRDY_EOS_SLV_Msk //!< End of regular sequence flag of the slave ADC 
.equ ADC12_CSR_ADRDY_OVR_SLV_Pos, (20) 
.equ ADC12_CSR_ADRDY_OVR_SLV_Msk, (0x1 << ADC12_CSR_ADRDY_OVR_SLV_Pos) //!< 0x00100000 
.equ ADC12_CSR_ADRDY_OVR_SLV, ADC12_CSR_ADRDY_OVR_SLV_Msk //!< Overrun flag of the slave ADC 
.equ ADC12_CSR_ADRDY_JEOC_SLV_Pos, (21) 
.equ ADC12_CSR_ADRDY_JEOC_SLV_Msk, (0x1 << ADC12_CSR_ADRDY_JEOC_SLV_Pos) //!< 0x00200000 
.equ ADC12_CSR_ADRDY_JEOC_SLV, ADC12_CSR_ADRDY_JEOC_SLV_Msk //!< End of injected conversion of the slave ADC 
.equ ADC12_CSR_ADRDY_JEOS_SLV_Pos, (22) 
.equ ADC12_CSR_ADRDY_JEOS_SLV_Msk, (0x1 << ADC12_CSR_ADRDY_JEOS_SLV_Pos) //!< 0x00400000 
.equ ADC12_CSR_ADRDY_JEOS_SLV, ADC12_CSR_ADRDY_JEOS_SLV_Msk //!< End of injected sequence flag of the slave ADC 
.equ ADC12_CSR_AWD1_SLV_Pos, (23) 
.equ ADC12_CSR_AWD1_SLV_Msk, (0x1 << ADC12_CSR_AWD1_SLV_Pos) //!< 0x00800000 
.equ ADC12_CSR_AWD1_SLV, ADC12_CSR_AWD1_SLV_Msk //!< Analog watchdog 1 flag of the slave ADC 
.equ ADC12_CSR_AWD2_SLV_Pos, (24) 
.equ ADC12_CSR_AWD2_SLV_Msk, (0x1 << ADC12_CSR_AWD2_SLV_Pos) //!< 0x01000000 
.equ ADC12_CSR_AWD2_SLV, ADC12_CSR_AWD2_SLV_Msk //!< Analog watchdog 2 flag of the slave ADC 
.equ ADC12_CSR_AWD3_SLV_Pos, (25) 
.equ ADC12_CSR_AWD3_SLV_Msk, (0x1 << ADC12_CSR_AWD3_SLV_Pos) //!< 0x02000000 
.equ ADC12_CSR_AWD3_SLV, ADC12_CSR_AWD3_SLV_Msk //!< Analog watchdog 3 flag of the slave ADC 
.equ ADC12_CSR_JQOVF_SLV_Pos, (26) 
.equ ADC12_CSR_JQOVF_SLV_Msk, (0x1 << ADC12_CSR_JQOVF_SLV_Pos) //!< 0x04000000 
.equ ADC12_CSR_JQOVF_SLV, ADC12_CSR_JQOVF_SLV_Msk //!< Injected context queue overflow flag of the slave ADC 
//**************  Bit definition for ADC34_COMMON_CSR register  **************
.equ ADC34_CSR_ADRDY_MST_Pos, (0) 
.equ ADC34_CSR_ADRDY_MST_Msk, (0x1 << ADC34_CSR_ADRDY_MST_Pos) //!< 0x00000001 
.equ ADC34_CSR_ADRDY_MST, ADC34_CSR_ADRDY_MST_Msk //!< Master ADC ready 
.equ ADC34_CSR_ADRDY_EOSMP_MST_Pos, (1) 
.equ ADC34_CSR_ADRDY_EOSMP_MST_Msk, (0x1 << ADC34_CSR_ADRDY_EOSMP_MST_Pos) //!< 0x00000002 
.equ ADC34_CSR_ADRDY_EOSMP_MST, ADC34_CSR_ADRDY_EOSMP_MST_Msk //!< End of sampling phase flag of the master ADC 
.equ ADC34_CSR_ADRDY_EOC_MST_Pos, (2) 
.equ ADC34_CSR_ADRDY_EOC_MST_Msk, (0x1 << ADC34_CSR_ADRDY_EOC_MST_Pos) //!< 0x00000004 
.equ ADC34_CSR_ADRDY_EOC_MST, ADC34_CSR_ADRDY_EOC_MST_Msk //!< End of regular conversion of the master ADC 
.equ ADC34_CSR_ADRDY_EOS_MST_Pos, (3) 
.equ ADC34_CSR_ADRDY_EOS_MST_Msk, (0x1 << ADC34_CSR_ADRDY_EOS_MST_Pos) //!< 0x00000008 
.equ ADC34_CSR_ADRDY_EOS_MST, ADC34_CSR_ADRDY_EOS_MST_Msk //!< End of regular sequence flag of the master ADC 
.equ ADC34_CSR_ADRDY_OVR_MST_Pos, (4) 
.equ ADC34_CSR_ADRDY_OVR_MST_Msk, (0x1 << ADC34_CSR_ADRDY_OVR_MST_Pos) //!< 0x00000010 
.equ ADC34_CSR_ADRDY_OVR_MST, ADC34_CSR_ADRDY_OVR_MST_Msk //!< Overrun flag of the master ADC 
.equ ADC34_CSR_ADRDY_JEOC_MST_Pos, (5) 
.equ ADC34_CSR_ADRDY_JEOC_MST_Msk, (0x1 << ADC34_CSR_ADRDY_JEOC_MST_Pos) //!< 0x00000020 
.equ ADC34_CSR_ADRDY_JEOC_MST, ADC34_CSR_ADRDY_JEOC_MST_Msk //!< End of injected conversion of the master ADC 
.equ ADC34_CSR_ADRDY_JEOS_MST_Pos, (6) 
.equ ADC34_CSR_ADRDY_JEOS_MST_Msk, (0x1 << ADC34_CSR_ADRDY_JEOS_MST_Pos) //!< 0x00000040 
.equ ADC34_CSR_ADRDY_JEOS_MST, ADC34_CSR_ADRDY_JEOS_MST_Msk //!< End of injected sequence flag of the master ADC 
.equ ADC34_CSR_AWD1_MST_Pos, (7) 
.equ ADC34_CSR_AWD1_MST_Msk, (0x1 << ADC34_CSR_AWD1_MST_Pos) //!< 0x00000080 
.equ ADC34_CSR_AWD1_MST, ADC34_CSR_AWD1_MST_Msk //!< Analog watchdog 1 flag of the master ADC 
.equ ADC34_CSR_AWD2_MST_Pos, (8) 
.equ ADC34_CSR_AWD2_MST_Msk, (0x1 << ADC34_CSR_AWD2_MST_Pos) //!< 0x00000100 
.equ ADC34_CSR_AWD2_MST, ADC34_CSR_AWD2_MST_Msk //!< Analog watchdog 2 flag of the master ADC 
.equ ADC34_CSR_AWD3_MST_Pos, (9) 
.equ ADC34_CSR_AWD3_MST_Msk, (0x1 << ADC34_CSR_AWD3_MST_Pos) //!< 0x00000200 
.equ ADC34_CSR_AWD3_MST, ADC34_CSR_AWD3_MST_Msk //!< Analog watchdog 3 flag of the master ADC 
.equ ADC34_CSR_JQOVF_MST_Pos, (10) 
.equ ADC34_CSR_JQOVF_MST_Msk, (0x1 << ADC34_CSR_JQOVF_MST_Pos) //!< 0x00000400 
.equ ADC34_CSR_JQOVF_MST, ADC34_CSR_JQOVF_MST_Msk //!< Injected context queue overflow flag of the master ADC 
.equ ADC34_CSR_ADRDY_SLV_Pos, (16) 
.equ ADC34_CSR_ADRDY_SLV_Msk, (0x1 << ADC34_CSR_ADRDY_SLV_Pos) //!< 0x00010000 
.equ ADC34_CSR_ADRDY_SLV, ADC34_CSR_ADRDY_SLV_Msk //!< Slave ADC ready 
.equ ADC34_CSR_ADRDY_EOSMP_SLV_Pos, (17) 
.equ ADC34_CSR_ADRDY_EOSMP_SLV_Msk, (0x1 << ADC34_CSR_ADRDY_EOSMP_SLV_Pos) //!< 0x00020000 
.equ ADC34_CSR_ADRDY_EOSMP_SLV, ADC34_CSR_ADRDY_EOSMP_SLV_Msk //!< End of sampling phase flag of the slave ADC 
.equ ADC34_CSR_ADRDY_EOC_SLV_Pos, (18) 
.equ ADC34_CSR_ADRDY_EOC_SLV_Msk, (0x1 << ADC34_CSR_ADRDY_EOC_SLV_Pos) //!< 0x00040000 
.equ ADC34_CSR_ADRDY_EOC_SLV, ADC34_CSR_ADRDY_EOC_SLV_Msk //!< End of regular conversion of the slave ADC 
.equ ADC34_CSR_ADRDY_EOS_SLV_Pos, (19) 
.equ ADC34_CSR_ADRDY_EOS_SLV_Msk, (0x1 << ADC34_CSR_ADRDY_EOS_SLV_Pos) //!< 0x00080000 
.equ ADC34_CSR_ADRDY_EOS_SLV, ADC34_CSR_ADRDY_EOS_SLV_Msk //!< End of regular sequence flag of the slave ADC 
.equ ADC34_CSR_ADRDY_OVR_SLV_Pos, (20) 
.equ ADC34_CSR_ADRDY_OVR_SLV_Msk, (0x1 << ADC12_CSR_ADRDY_OVR_SLV_Pos) //!< 0x00100000 
.equ ADC34_CSR_ADRDY_OVR_SLV, ADC12_CSR_ADRDY_OVR_SLV_Msk //!< Overrun flag of the slave ADC 
.equ ADC34_CSR_ADRDY_JEOC_SLV_Pos, (21) 
.equ ADC34_CSR_ADRDY_JEOC_SLV_Msk, (0x1 << ADC34_CSR_ADRDY_JEOC_SLV_Pos) //!< 0x00200000 
.equ ADC34_CSR_ADRDY_JEOC_SLV, ADC34_CSR_ADRDY_JEOC_SLV_Msk //!< End of injected conversion of the slave ADC 
.equ ADC34_CSR_ADRDY_JEOS_SLV_Pos, (22) 
.equ ADC34_CSR_ADRDY_JEOS_SLV_Msk, (0x1 << ADC34_CSR_ADRDY_JEOS_SLV_Pos) //!< 0x00400000 
.equ ADC34_CSR_ADRDY_JEOS_SLV, ADC34_CSR_ADRDY_JEOS_SLV_Msk //!< End of injected sequence flag of the slave ADC 
.equ ADC34_CSR_AWD1_SLV_Pos, (23) 
.equ ADC34_CSR_AWD1_SLV_Msk, (0x1 << ADC34_CSR_AWD1_SLV_Pos) //!< 0x00800000 
.equ ADC34_CSR_AWD1_SLV, ADC34_CSR_AWD1_SLV_Msk //!< Analog watchdog 1 flag of the slave ADC 
.equ ADC34_CSR_AWD2_SLV_Pos, (24) 
.equ ADC34_CSR_AWD2_SLV_Msk, (0x1 << ADC34_CSR_AWD2_SLV_Pos) //!< 0x01000000 
.equ ADC34_CSR_AWD2_SLV, ADC34_CSR_AWD2_SLV_Msk //!< Analog watchdog 2 flag of the slave ADC 
.equ ADC34_CSR_AWD3_SLV_Pos, (25) 
.equ ADC34_CSR_AWD3_SLV_Msk, (0x1 << ADC34_CSR_AWD3_SLV_Pos) //!< 0x02000000 
.equ ADC34_CSR_AWD3_SLV, ADC34_CSR_AWD3_SLV_Msk //!< Analog watchdog 3 flag of the slave ADC 
.equ ADC34_CSR_JQOVF_SLV_Pos, (26) 
.equ ADC34_CSR_JQOVF_SLV_Msk, (0x1 << ADC34_CSR_JQOVF_SLV_Pos) //!< 0x04000000 
.equ ADC34_CSR_JQOVF_SLV, ADC34_CSR_JQOVF_SLV_Msk //!< Injected context queue overflow flag of the slave ADC 
//**************  Bit definition for ADC12_COMMON_CCR register  **************
.equ ADC12_CCR_MULTI_Pos, (0) 
.equ ADC12_CCR_MULTI_Msk, (0x1F << ADC12_CCR_MULTI_Pos) //!< 0x0000001F 
.equ ADC12_CCR_MULTI, ADC12_CCR_MULTI_Msk //!< Multi ADC mode selection 
.equ ADC12_CCR_MULTI_0, (0x01 << ADC12_CCR_MULTI_Pos) //!< 0x00000001 
.equ ADC12_CCR_MULTI_1, (0x02 << ADC12_CCR_MULTI_Pos) //!< 0x00000002 
.equ ADC12_CCR_MULTI_2, (0x04 << ADC12_CCR_MULTI_Pos) //!< 0x00000004 
.equ ADC12_CCR_MULTI_3, (0x08 << ADC12_CCR_MULTI_Pos) //!< 0x00000008 
.equ ADC12_CCR_MULTI_4, (0x10 << ADC12_CCR_MULTI_Pos) //!< 0x00000010 
.equ ADC12_CCR_DELAY_Pos, (8) 
.equ ADC12_CCR_DELAY_Msk, (0xF << ADC12_CCR_DELAY_Pos) //!< 0x00000F00 
.equ ADC12_CCR_DELAY, ADC12_CCR_DELAY_Msk //!< Delay between 2 sampling phases 
.equ ADC12_CCR_DELAY_0, (0x1 << ADC12_CCR_DELAY_Pos) //!< 0x00000100 
.equ ADC12_CCR_DELAY_1, (0x2 << ADC12_CCR_DELAY_Pos) //!< 0x00000200 
.equ ADC12_CCR_DELAY_2, (0x4 << ADC12_CCR_DELAY_Pos) //!< 0x00000400 
.equ ADC12_CCR_DELAY_3, (0x8 << ADC12_CCR_DELAY_Pos) //!< 0x00000800 
.equ ADC12_CCR_DMACFG_Pos, (13) 
.equ ADC12_CCR_DMACFG_Msk, (0x1 << ADC12_CCR_DMACFG_Pos) //!< 0x00002000 
.equ ADC12_CCR_DMACFG, ADC12_CCR_DMACFG_Msk //!< DMA configuration for multi-ADC mode 
.equ ADC12_CCR_MDMA_Pos, (14) 
.equ ADC12_CCR_MDMA_Msk, (0x3 << ADC12_CCR_MDMA_Pos) //!< 0x0000C000 
.equ ADC12_CCR_MDMA, ADC12_CCR_MDMA_Msk //!< DMA mode for multi-ADC mode 
.equ ADC12_CCR_MDMA_0, (0x1 << ADC12_CCR_MDMA_Pos) //!< 0x00004000 
.equ ADC12_CCR_MDMA_1, (0x2 << ADC12_CCR_MDMA_Pos) //!< 0x00008000 
.equ ADC12_CCR_CKMODE_Pos, (16) 
.equ ADC12_CCR_CKMODE_Msk, (0x3 << ADC12_CCR_CKMODE_Pos) //!< 0x00030000 
.equ ADC12_CCR_CKMODE, ADC12_CCR_CKMODE_Msk //!< ADC clock mode 
.equ ADC12_CCR_CKMODE_0, (0x1 << ADC12_CCR_CKMODE_Pos) //!< 0x00010000 
.equ ADC12_CCR_CKMODE_1, (0x2 << ADC12_CCR_CKMODE_Pos) //!< 0x00020000 
.equ ADC12_CCR_VREFEN_Pos, (22) 
.equ ADC12_CCR_VREFEN_Msk, (0x1 << ADC12_CCR_VREFEN_Pos) //!< 0x00400000 
.equ ADC12_CCR_VREFEN, ADC12_CCR_VREFEN_Msk //!< VREFINT enable 
.equ ADC12_CCR_TSEN_Pos, (23) 
.equ ADC12_CCR_TSEN_Msk, (0x1 << ADC12_CCR_TSEN_Pos) //!< 0x00800000 
.equ ADC12_CCR_TSEN, ADC12_CCR_TSEN_Msk //!< Temperature sensor enable 
.equ ADC12_CCR_VBATEN_Pos, (24) 
.equ ADC12_CCR_VBATEN_Msk, (0x1 << ADC12_CCR_VBATEN_Pos) //!< 0x01000000 
.equ ADC12_CCR_VBATEN, ADC12_CCR_VBATEN_Msk //!< VBAT enable 
//**************  Bit definition for ADC34_COMMON_CCR register  **************
.equ ADC34_CCR_MULTI_Pos, (0) 
.equ ADC34_CCR_MULTI_Msk, (0x1F << ADC34_CCR_MULTI_Pos) //!< 0x0000001F 
.equ ADC34_CCR_MULTI, ADC34_CCR_MULTI_Msk //!< Multi ADC mode selection 
.equ ADC34_CCR_MULTI_0, (0x01 << ADC34_CCR_MULTI_Pos) //!< 0x00000001 
.equ ADC34_CCR_MULTI_1, (0x02 << ADC34_CCR_MULTI_Pos) //!< 0x00000002 
.equ ADC34_CCR_MULTI_2, (0x04 << ADC34_CCR_MULTI_Pos) //!< 0x00000004 
.equ ADC34_CCR_MULTI_3, (0x08 << ADC34_CCR_MULTI_Pos) //!< 0x00000008 
.equ ADC34_CCR_MULTI_4, (0x10 << ADC34_CCR_MULTI_Pos) //!< 0x00000010 
.equ ADC34_CCR_DELAY_Pos, (8) 
.equ ADC34_CCR_DELAY_Msk, (0xF << ADC34_CCR_DELAY_Pos) //!< 0x00000F00 
.equ ADC34_CCR_DELAY, ADC34_CCR_DELAY_Msk //!< Delay between 2 sampling phases 
.equ ADC34_CCR_DELAY_0, (0x1 << ADC34_CCR_DELAY_Pos) //!< 0x00000100 
.equ ADC34_CCR_DELAY_1, (0x2 << ADC34_CCR_DELAY_Pos) //!< 0x00000200 
.equ ADC34_CCR_DELAY_2, (0x4 << ADC34_CCR_DELAY_Pos) //!< 0x00000400 
.equ ADC34_CCR_DELAY_3, (0x8 << ADC34_CCR_DELAY_Pos) //!< 0x00000800 
.equ ADC34_CCR_DMACFG_Pos, (13) 
.equ ADC34_CCR_DMACFG_Msk, (0x1 << ADC34_CCR_DMACFG_Pos) //!< 0x00002000 
.equ ADC34_CCR_DMACFG, ADC34_CCR_DMACFG_Msk //!< DMA configuration for multi-ADC mode 
.equ ADC34_CCR_MDMA_Pos, (14) 
.equ ADC34_CCR_MDMA_Msk, (0x3 << ADC34_CCR_MDMA_Pos) //!< 0x0000C000 
.equ ADC34_CCR_MDMA, ADC34_CCR_MDMA_Msk //!< DMA mode for multi-ADC mode 
.equ ADC34_CCR_MDMA_0, (0x1 << ADC34_CCR_MDMA_Pos) //!< 0x00004000 
.equ ADC34_CCR_MDMA_1, (0x2 << ADC34_CCR_MDMA_Pos) //!< 0x00008000 
.equ ADC34_CCR_CKMODE_Pos, (16) 
.equ ADC34_CCR_CKMODE_Msk, (0x3 << ADC34_CCR_CKMODE_Pos) //!< 0x00030000 
.equ ADC34_CCR_CKMODE, ADC34_CCR_CKMODE_Msk //!< ADC clock mode 
.equ ADC34_CCR_CKMODE_0, (0x1 << ADC34_CCR_CKMODE_Pos) //!< 0x00010000 
.equ ADC34_CCR_CKMODE_1, (0x2 << ADC34_CCR_CKMODE_Pos) //!< 0x00020000 
.equ ADC34_CCR_VREFEN_Pos, (22) 
.equ ADC34_CCR_VREFEN_Msk, (0x1 << ADC34_CCR_VREFEN_Pos) //!< 0x00400000 
.equ ADC34_CCR_VREFEN, ADC34_CCR_VREFEN_Msk //!< VREFINT enable 
.equ ADC34_CCR_TSEN_Pos, (23) 
.equ ADC34_CCR_TSEN_Msk, (0x1 << ADC34_CCR_TSEN_Pos) //!< 0x00800000 
.equ ADC34_CCR_TSEN, ADC34_CCR_TSEN_Msk //!< Temperature sensor enable 
.equ ADC34_CCR_VBATEN_Pos, (24) 
.equ ADC34_CCR_VBATEN_Msk, (0x1 << ADC34_CCR_VBATEN_Pos) //!< 0x01000000 
.equ ADC34_CCR_VBATEN, ADC34_CCR_VBATEN_Msk //!< VBAT enable 
//**************  Bit definition for ADC12_COMMON_CDR register  **************
.equ ADC12_CDR_RDATA_MST_Pos, (0) 
.equ ADC12_CDR_RDATA_MST_Msk, (0xFFFF << ADC12_CDR_RDATA_MST_Pos) //!< 0x0000FFFF 
.equ ADC12_CDR_RDATA_MST, ADC12_CDR_RDATA_MST_Msk //!< Regular Data of the master ADC 
.equ ADC12_CDR_RDATA_MST_0, (0x0001 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00000001 
.equ ADC12_CDR_RDATA_MST_1, (0x0002 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00000002 
.equ ADC12_CDR_RDATA_MST_2, (0x0004 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00000004 
.equ ADC12_CDR_RDATA_MST_3, (0x0008 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00000008 
.equ ADC12_CDR_RDATA_MST_4, (0x0010 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00000010 
.equ ADC12_CDR_RDATA_MST_5, (0x0020 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00000020 
.equ ADC12_CDR_RDATA_MST_6, (0x0040 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00000040 
.equ ADC12_CDR_RDATA_MST_7, (0x0080 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00000080 
.equ ADC12_CDR_RDATA_MST_8, (0x0100 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00000100 
.equ ADC12_CDR_RDATA_MST_9, (0x0200 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00000200 
.equ ADC12_CDR_RDATA_MST_10, (0x0400 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00000400 
.equ ADC12_CDR_RDATA_MST_11, (0x0800 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00000800 
.equ ADC12_CDR_RDATA_MST_12, (0x1000 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00001000 
.equ ADC12_CDR_RDATA_MST_13, (0x2000 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00002000 
.equ ADC12_CDR_RDATA_MST_14, (0x4000 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00004000 
.equ ADC12_CDR_RDATA_MST_15, (0x8000 << ADC12_CDR_RDATA_MST_Pos) //!< 0x00008000 
.equ ADC12_CDR_RDATA_SLV_Pos, (16) 
.equ ADC12_CDR_RDATA_SLV_Msk, (0xFFFF << ADC12_CDR_RDATA_SLV_Pos) //!< 0xFFFF0000 
.equ ADC12_CDR_RDATA_SLV, ADC12_CDR_RDATA_SLV_Msk //!< Regular Data of the master ADC 
.equ ADC12_CDR_RDATA_SLV_0, (0x0001 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x00010000 
.equ ADC12_CDR_RDATA_SLV_1, (0x0002 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x00020000 
.equ ADC12_CDR_RDATA_SLV_2, (0x0004 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x00040000 
.equ ADC12_CDR_RDATA_SLV_3, (0x0008 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x00080000 
.equ ADC12_CDR_RDATA_SLV_4, (0x0010 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x00100000 
.equ ADC12_CDR_RDATA_SLV_5, (0x0020 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x00200000 
.equ ADC12_CDR_RDATA_SLV_6, (0x0040 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x00400000 
.equ ADC12_CDR_RDATA_SLV_7, (0x0080 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x00800000 
.equ ADC12_CDR_RDATA_SLV_8, (0x0100 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x01000000 
.equ ADC12_CDR_RDATA_SLV_9, (0x0200 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x02000000 
.equ ADC12_CDR_RDATA_SLV_10, (0x0400 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x04000000 
.equ ADC12_CDR_RDATA_SLV_11, (0x0800 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x08000000 
.equ ADC12_CDR_RDATA_SLV_12, (0x1000 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x10000000 
.equ ADC12_CDR_RDATA_SLV_13, (0x2000 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x20000000 
.equ ADC12_CDR_RDATA_SLV_14, (0x4000 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x40000000 
.equ ADC12_CDR_RDATA_SLV_15, (0x8000 << ADC12_CDR_RDATA_SLV_Pos) //!< 0x80000000 
//**************  Bit definition for ADC34_COMMON_CDR register  **************
.equ ADC34_CDR_RDATA_MST_Pos, (0) 
.equ ADC34_CDR_RDATA_MST_Msk, (0xFFFF << ADC34_CDR_RDATA_MST_Pos) //!< 0x0000FFFF 
.equ ADC34_CDR_RDATA_MST, ADC34_CDR_RDATA_MST_Msk //!< Regular Data of the master ADC 
.equ ADC34_CDR_RDATA_MST_0, (0x0001 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00000001 
.equ ADC34_CDR_RDATA_MST_1, (0x0002 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00000002 
.equ ADC34_CDR_RDATA_MST_2, (0x0004 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00000004 
.equ ADC34_CDR_RDATA_MST_3, (0x0008 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00000008 
.equ ADC34_CDR_RDATA_MST_4, (0x0010 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00000010 
.equ ADC34_CDR_RDATA_MST_5, (0x0020 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00000020 
.equ ADC34_CDR_RDATA_MST_6, (0x0040 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00000040 
.equ ADC34_CDR_RDATA_MST_7, (0x0080 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00000080 
.equ ADC34_CDR_RDATA_MST_8, (0x0100 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00000100 
.equ ADC34_CDR_RDATA_MST_9, (0x0200 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00000200 
.equ ADC34_CDR_RDATA_MST_10, (0x0400 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00000400 
.equ ADC34_CDR_RDATA_MST_11, (0x0800 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00000800 
.equ ADC34_CDR_RDATA_MST_12, (0x1000 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00001000 
.equ ADC34_CDR_RDATA_MST_13, (0x2000 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00002000 
.equ ADC34_CDR_RDATA_MST_14, (0x4000 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00004000 
.equ ADC34_CDR_RDATA_MST_15, (0x8000 << ADC34_CDR_RDATA_MST_Pos) //!< 0x00008000 
.equ ADC34_CDR_RDATA_SLV_Pos, (16) 
.equ ADC34_CDR_RDATA_SLV_Msk, (0xFFFF << ADC34_CDR_RDATA_SLV_Pos) //!< 0xFFFF0000 
.equ ADC34_CDR_RDATA_SLV, ADC34_CDR_RDATA_SLV_Msk //!< Regular Data of the master ADC 
.equ ADC34_CDR_RDATA_SLV_0, (0x0001 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x00010000 
.equ ADC34_CDR_RDATA_SLV_1, (0x0002 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x00020000 
.equ ADC34_CDR_RDATA_SLV_2, (0x0004 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x00040000 
.equ ADC34_CDR_RDATA_SLV_3, (0x0008 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x00080000 
.equ ADC34_CDR_RDATA_SLV_4, (0x0010 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x00100000 
.equ ADC34_CDR_RDATA_SLV_5, (0x0020 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x00200000 
.equ ADC34_CDR_RDATA_SLV_6, (0x0040 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x00400000 
.equ ADC34_CDR_RDATA_SLV_7, (0x0080 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x00800000 
.equ ADC34_CDR_RDATA_SLV_8, (0x0100 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x01000000 
.equ ADC34_CDR_RDATA_SLV_9, (0x0200 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x02000000 
.equ ADC34_CDR_RDATA_SLV_10, (0x0400 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x04000000 
.equ ADC34_CDR_RDATA_SLV_11, (0x0800 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x08000000 
.equ ADC34_CDR_RDATA_SLV_12, (0x1000 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x10000000 
.equ ADC34_CDR_RDATA_SLV_13, (0x2000 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x20000000 
.equ ADC34_CDR_RDATA_SLV_14, (0x4000 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x40000000 
.equ ADC34_CDR_RDATA_SLV_15, (0x8000 << ADC34_CDR_RDATA_SLV_Pos) //!< 0x80000000 
//*******************  Bit definition for ADC_CSR register  ******************
.equ ADC_CSR_ADRDY_MST_Pos, (0) 
.equ ADC_CSR_ADRDY_MST_Msk, (0x1 << ADC_CSR_ADRDY_MST_Pos) //!< 0x00000001 
.equ ADC_CSR_ADRDY_MST, ADC_CSR_ADRDY_MST_Msk //!< ADC multimode master ready flag 
.equ ADC_CSR_EOSMP_MST_Pos, (1) 
.equ ADC_CSR_EOSMP_MST_Msk, (0x1 << ADC_CSR_EOSMP_MST_Pos) //!< 0x00000002 
.equ ADC_CSR_EOSMP_MST, ADC_CSR_EOSMP_MST_Msk //!< ADC multimode master group regular end of sampling flag 
.equ ADC_CSR_EOC_MST_Pos, (2) 
.equ ADC_CSR_EOC_MST_Msk, (0x1 << ADC_CSR_EOC_MST_Pos) //!< 0x00000004 
.equ ADC_CSR_EOC_MST, ADC_CSR_EOC_MST_Msk //!< ADC multimode master group regular end of unitary conversion flag 
.equ ADC_CSR_EOS_MST_Pos, (3) 
.equ ADC_CSR_EOS_MST_Msk, (0x1 << ADC_CSR_EOS_MST_Pos) //!< 0x00000008 
.equ ADC_CSR_EOS_MST, ADC_CSR_EOS_MST_Msk //!< ADC multimode master group regular end of sequence conversions flag 
.equ ADC_CSR_OVR_MST_Pos, (4) 
.equ ADC_CSR_OVR_MST_Msk, (0x1 << ADC_CSR_OVR_MST_Pos) //!< 0x00000010 
.equ ADC_CSR_OVR_MST, ADC_CSR_OVR_MST_Msk //!< ADC multimode master group regular overrun flag 
.equ ADC_CSR_JEOC_MST_Pos, (5) 
.equ ADC_CSR_JEOC_MST_Msk, (0x1 << ADC_CSR_JEOC_MST_Pos) //!< 0x00000020 
.equ ADC_CSR_JEOC_MST, ADC_CSR_JEOC_MST_Msk //!< ADC multimode master group injected end of unitary conversion flag 
.equ ADC_CSR_JEOS_MST_Pos, (6) 
.equ ADC_CSR_JEOS_MST_Msk, (0x1 << ADC_CSR_JEOS_MST_Pos) //!< 0x00000040 
.equ ADC_CSR_JEOS_MST, ADC_CSR_JEOS_MST_Msk //!< ADC multimode master group injected end of sequence conversions flag 
.equ ADC_CSR_AWD1_MST_Pos, (7) 
.equ ADC_CSR_AWD1_MST_Msk, (0x1 << ADC_CSR_AWD1_MST_Pos) //!< 0x00000080 
.equ ADC_CSR_AWD1_MST, ADC_CSR_AWD1_MST_Msk //!< ADC multimode master analog watchdog 1 flag 
.equ ADC_CSR_AWD2_MST_Pos, (8) 
.equ ADC_CSR_AWD2_MST_Msk, (0x1 << ADC_CSR_AWD2_MST_Pos) //!< 0x00000100 
.equ ADC_CSR_AWD2_MST, ADC_CSR_AWD2_MST_Msk //!< ADC multimode master analog watchdog 2 flag 
.equ ADC_CSR_AWD3_MST_Pos, (9) 
.equ ADC_CSR_AWD3_MST_Msk, (0x1 << ADC_CSR_AWD3_MST_Pos) //!< 0x00000200 
.equ ADC_CSR_AWD3_MST, ADC_CSR_AWD3_MST_Msk //!< ADC multimode master analog watchdog 3 flag 
.equ ADC_CSR_JQOVF_MST_Pos, (10) 
.equ ADC_CSR_JQOVF_MST_Msk, (0x1 << ADC_CSR_JQOVF_MST_Pos) //!< 0x00000400 
.equ ADC_CSR_JQOVF_MST, ADC_CSR_JQOVF_MST_Msk //!< ADC multimode master group injected contexts queue overflow flag 
.equ ADC_CSR_ADRDY_SLV_Pos, (16) 
.equ ADC_CSR_ADRDY_SLV_Msk, (0x1 << ADC_CSR_ADRDY_SLV_Pos) //!< 0x00010000 
.equ ADC_CSR_ADRDY_SLV, ADC_CSR_ADRDY_SLV_Msk //!< ADC multimode slave ready flag 
.equ ADC_CSR_EOSMP_SLV_Pos, (17) 
.equ ADC_CSR_EOSMP_SLV_Msk, (0x1 << ADC_CSR_EOSMP_SLV_Pos) //!< 0x00020000 
.equ ADC_CSR_EOSMP_SLV, ADC_CSR_EOSMP_SLV_Msk //!< ADC multimode slave group regular end of sampling flag 
.equ ADC_CSR_EOC_SLV_Pos, (18) 
.equ ADC_CSR_EOC_SLV_Msk, (0x1 << ADC_CSR_EOC_SLV_Pos) //!< 0x00040000 
.equ ADC_CSR_EOC_SLV, ADC_CSR_EOC_SLV_Msk //!< ADC multimode slave group regular end of unitary conversion flag 
.equ ADC_CSR_EOS_SLV_Pos, (19) 
.equ ADC_CSR_EOS_SLV_Msk, (0x1 << ADC_CSR_EOS_SLV_Pos) //!< 0x00080000 
.equ ADC_CSR_EOS_SLV, ADC_CSR_EOS_SLV_Msk //!< ADC multimode slave group regular end of sequence conversions flag 
.equ ADC_CSR_OVR_SLV_Pos, (20) 
.equ ADC_CSR_OVR_SLV_Msk, (0x1 << ADC_CSR_OVR_SLV_Pos) //!< 0x00100000 
.equ ADC_CSR_OVR_SLV, ADC_CSR_OVR_SLV_Msk //!< ADC multimode slave group regular overrun flag 
.equ ADC_CSR_JEOC_SLV_Pos, (21) 
.equ ADC_CSR_JEOC_SLV_Msk, (0x1 << ADC_CSR_JEOC_SLV_Pos) //!< 0x00200000 
.equ ADC_CSR_JEOC_SLV, ADC_CSR_JEOC_SLV_Msk //!< ADC multimode slave group injected end of unitary conversion flag 
.equ ADC_CSR_JEOS_SLV_Pos, (22) 
.equ ADC_CSR_JEOS_SLV_Msk, (0x1 << ADC_CSR_JEOS_SLV_Pos) //!< 0x00400000 
.equ ADC_CSR_JEOS_SLV, ADC_CSR_JEOS_SLV_Msk //!< ADC multimode slave group injected end of sequence conversions flag 
.equ ADC_CSR_AWD1_SLV_Pos, (23) 
.equ ADC_CSR_AWD1_SLV_Msk, (0x1 << ADC_CSR_AWD1_SLV_Pos) //!< 0x00800000 
.equ ADC_CSR_AWD1_SLV, ADC_CSR_AWD1_SLV_Msk //!< ADC multimode slave analog watchdog 1 flag 
.equ ADC_CSR_AWD2_SLV_Pos, (24) 
.equ ADC_CSR_AWD2_SLV_Msk, (0x1 << ADC_CSR_AWD2_SLV_Pos) //!< 0x01000000 
.equ ADC_CSR_AWD2_SLV, ADC_CSR_AWD2_SLV_Msk //!< ADC multimode slave analog watchdog 2 flag 
.equ ADC_CSR_AWD3_SLV_Pos, (25) 
.equ ADC_CSR_AWD3_SLV_Msk, (0x1 << ADC_CSR_AWD3_SLV_Pos) //!< 0x02000000 
.equ ADC_CSR_AWD3_SLV, ADC_CSR_AWD3_SLV_Msk //!< ADC multimode slave analog watchdog 3 flag 
.equ ADC_CSR_JQOVF_SLV_Pos, (26) 
.equ ADC_CSR_JQOVF_SLV_Msk, (0x1 << ADC_CSR_JQOVF_SLV_Pos) //!< 0x04000000 
.equ ADC_CSR_JQOVF_SLV, ADC_CSR_JQOVF_SLV_Msk //!< ADC multimode slave group injected contexts queue overflow flag 
// Legacy defines
.equ ADC_CSR_ADRDY_EOSMP_MST, ADC_CSR_EOSMP_MST 
.equ ADC_CSR_ADRDY_EOC_MST, ADC_CSR_EOC_MST 
.equ ADC_CSR_ADRDY_EOS_MST, ADC_CSR_EOS_MST 
.equ ADC_CSR_ADRDY_OVR_MST, ADC_CSR_OVR_MST 
.equ ADC_CSR_ADRDY_JEOC_MST, ADC_CSR_JEOC_MST 
.equ ADC_CSR_ADRDY_JEOS_MST, ADC_CSR_JEOS_MST 
.equ ADC_CSR_ADRDY_EOSMP_SLV, ADC_CSR_EOSMP_SLV 
.equ ADC_CSR_ADRDY_EOC_SLV, ADC_CSR_EOC_SLV 
.equ ADC_CSR_ADRDY_EOS_SLV, ADC_CSR_EOS_SLV 
.equ ADC_CSR_ADRDY_OVR_SLV, ADC_CSR_OVR_SLV 
.equ ADC_CSR_ADRDY_JEOC_SLV, ADC_CSR_JEOC_SLV 
.equ ADC_CSR_ADRDY_JEOS_SLV, ADC_CSR_JEOS_SLV 
//*******************  Bit definition for ADC_CCR register  ******************
.equ ADC_CCR_DUAL_Pos, (0) 
.equ ADC_CCR_DUAL_Msk, (0x1F << ADC_CCR_DUAL_Pos) //!< 0x0000001F 
.equ ADC_CCR_DUAL, ADC_CCR_DUAL_Msk //!< ADC multimode mode selection 
.equ ADC_CCR_DUAL_0, (0x01 << ADC_CCR_DUAL_Pos) //!< 0x00000001 
.equ ADC_CCR_DUAL_1, (0x02 << ADC_CCR_DUAL_Pos) //!< 0x00000002 
.equ ADC_CCR_DUAL_2, (0x04 << ADC_CCR_DUAL_Pos) //!< 0x00000004 
.equ ADC_CCR_DUAL_3, (0x08 << ADC_CCR_DUAL_Pos) //!< 0x00000008 
.equ ADC_CCR_DUAL_4, (0x10 << ADC_CCR_DUAL_Pos) //!< 0x00000010 
.equ ADC_CCR_DELAY_Pos, (8) 
.equ ADC_CCR_DELAY_Msk, (0xF << ADC_CCR_DELAY_Pos) //!< 0x00000F00 
.equ ADC_CCR_DELAY, ADC_CCR_DELAY_Msk //!< ADC multimode delay between 2 sampling phases 
.equ ADC_CCR_DELAY_0, (0x1 << ADC_CCR_DELAY_Pos) //!< 0x00000100 
.equ ADC_CCR_DELAY_1, (0x2 << ADC_CCR_DELAY_Pos) //!< 0x00000200 
.equ ADC_CCR_DELAY_2, (0x4 << ADC_CCR_DELAY_Pos) //!< 0x00000400 
.equ ADC_CCR_DELAY_3, (0x8 << ADC_CCR_DELAY_Pos) //!< 0x00000800 
.equ ADC_CCR_DMACFG_Pos, (13) 
.equ ADC_CCR_DMACFG_Msk, (0x1 << ADC_CCR_DMACFG_Pos) //!< 0x00002000 
.equ ADC_CCR_DMACFG, ADC_CCR_DMACFG_Msk //!< ADC multimode DMA transfer configuration 
.equ ADC_CCR_MDMA_Pos, (14) 
.equ ADC_CCR_MDMA_Msk, (0x3 << ADC_CCR_MDMA_Pos) //!< 0x0000C000 
.equ ADC_CCR_MDMA, ADC_CCR_MDMA_Msk //!< ADC multimode DMA transfer enable 
.equ ADC_CCR_MDMA_0, (0x1 << ADC_CCR_MDMA_Pos) //!< 0x00004000 
.equ ADC_CCR_MDMA_1, (0x2 << ADC_CCR_MDMA_Pos) //!< 0x00008000 
.equ ADC_CCR_CKMODE_Pos, (16) 
.equ ADC_CCR_CKMODE_Msk, (0x3 << ADC_CCR_CKMODE_Pos) //!< 0x00030000 
.equ ADC_CCR_CKMODE, ADC_CCR_CKMODE_Msk //!< ADC common clock source and prescaler (prescaler only for clock source synchronous) 
.equ ADC_CCR_CKMODE_0, (0x1 << ADC_CCR_CKMODE_Pos) //!< 0x00010000 
.equ ADC_CCR_CKMODE_1, (0x2 << ADC_CCR_CKMODE_Pos) //!< 0x00020000 
.equ ADC_CCR_VREFEN_Pos, (22) 
.equ ADC_CCR_VREFEN_Msk, (0x1 << ADC_CCR_VREFEN_Pos) //!< 0x00400000 
.equ ADC_CCR_VREFEN, ADC_CCR_VREFEN_Msk //!< ADC internal path to VrefInt enable 
.equ ADC_CCR_TSEN_Pos, (23) 
.equ ADC_CCR_TSEN_Msk, (0x1 << ADC_CCR_TSEN_Pos) //!< 0x00800000 
.equ ADC_CCR_TSEN, ADC_CCR_TSEN_Msk //!< ADC internal path to temperature sensor enable 
.equ ADC_CCR_VBATEN_Pos, (24) 
.equ ADC_CCR_VBATEN_Msk, (0x1 << ADC_CCR_VBATEN_Pos) //!< 0x01000000 
.equ ADC_CCR_VBATEN, ADC_CCR_VBATEN_Msk //!< ADC internal path to battery voltage enable 
// Legacy defines
.equ ADC_CCR_MULTI, (ADC_CCR_DUAL) 
.equ ADC_CCR_MULTI_0, (ADC_CCR_DUAL_0) 
.equ ADC_CCR_MULTI_1, (ADC_CCR_DUAL_1) 
.equ ADC_CCR_MULTI_2, (ADC_CCR_DUAL_2) 
.equ ADC_CCR_MULTI_3, (ADC_CCR_DUAL_3) 
.equ ADC_CCR_MULTI_4, (ADC_CCR_DUAL_4) 
//*******************  Bit definition for ADC_CDR register  ******************
.equ ADC_CDR_RDATA_MST_Pos, (0) 
.equ ADC_CDR_RDATA_MST_Msk, (0xFFFF << ADC_CDR_RDATA_MST_Pos) //!< 0x0000FFFF 
.equ ADC_CDR_RDATA_MST, ADC_CDR_RDATA_MST_Msk //!< ADC multimode master group regular conversion data 
.equ ADC_CDR_RDATA_MST_0, (0x0001 << ADC_CDR_RDATA_MST_Pos) //!< 0x00000001 
.equ ADC_CDR_RDATA_MST_1, (0x0002 << ADC_CDR_RDATA_MST_Pos) //!< 0x00000002 
.equ ADC_CDR_RDATA_MST_2, (0x0004 << ADC_CDR_RDATA_MST_Pos) //!< 0x00000004 
.equ ADC_CDR_RDATA_MST_3, (0x0008 << ADC_CDR_RDATA_MST_Pos) //!< 0x00000008 
.equ ADC_CDR_RDATA_MST_4, (0x0010 << ADC_CDR_RDATA_MST_Pos) //!< 0x00000010 
.equ ADC_CDR_RDATA_MST_5, (0x0020 << ADC_CDR_RDATA_MST_Pos) //!< 0x00000020 
.equ ADC_CDR_RDATA_MST_6, (0x0040 << ADC_CDR_RDATA_MST_Pos) //!< 0x00000040 
.equ ADC_CDR_RDATA_MST_7, (0x0080 << ADC_CDR_RDATA_MST_Pos) //!< 0x00000080 
.equ ADC_CDR_RDATA_MST_8, (0x0100 << ADC_CDR_RDATA_MST_Pos) //!< 0x00000100 
.equ ADC_CDR_RDATA_MST_9, (0x0200 << ADC_CDR_RDATA_MST_Pos) //!< 0x00000200 
.equ ADC_CDR_RDATA_MST_10, (0x0400 << ADC_CDR_RDATA_MST_Pos) //!< 0x00000400 
.equ ADC_CDR_RDATA_MST_11, (0x0800 << ADC_CDR_RDATA_MST_Pos) //!< 0x00000800 
.equ ADC_CDR_RDATA_MST_12, (0x1000 << ADC_CDR_RDATA_MST_Pos) //!< 0x00001000 
.equ ADC_CDR_RDATA_MST_13, (0x2000 << ADC_CDR_RDATA_MST_Pos) //!< 0x00002000 
.equ ADC_CDR_RDATA_MST_14, (0x4000 << ADC_CDR_RDATA_MST_Pos) //!< 0x00004000 
.equ ADC_CDR_RDATA_MST_15, (0x8000 << ADC_CDR_RDATA_MST_Pos) //!< 0x00008000 
.equ ADC_CDR_RDATA_SLV_Pos, (16) 
.equ ADC_CDR_RDATA_SLV_Msk, (0xFFFF << ADC_CDR_RDATA_SLV_Pos) //!< 0xFFFF0000 
.equ ADC_CDR_RDATA_SLV, ADC_CDR_RDATA_SLV_Msk //!< ADC multimode slave group regular conversion data 
.equ ADC_CDR_RDATA_SLV_0, (0x0001 << ADC_CDR_RDATA_SLV_Pos) //!< 0x00010000 
.equ ADC_CDR_RDATA_SLV_1, (0x0002 << ADC_CDR_RDATA_SLV_Pos) //!< 0x00020000 
.equ ADC_CDR_RDATA_SLV_2, (0x0004 << ADC_CDR_RDATA_SLV_Pos) //!< 0x00040000 
.equ ADC_CDR_RDATA_SLV_3, (0x0008 << ADC_CDR_RDATA_SLV_Pos) //!< 0x00080000 
.equ ADC_CDR_RDATA_SLV_4, (0x0010 << ADC_CDR_RDATA_SLV_Pos) //!< 0x00100000 
.equ ADC_CDR_RDATA_SLV_5, (0x0020 << ADC_CDR_RDATA_SLV_Pos) //!< 0x00200000 
.equ ADC_CDR_RDATA_SLV_6, (0x0040 << ADC_CDR_RDATA_SLV_Pos) //!< 0x00400000 
.equ ADC_CDR_RDATA_SLV_7, (0x0080 << ADC_CDR_RDATA_SLV_Pos) //!< 0x00800000 
.equ ADC_CDR_RDATA_SLV_8, (0x0100 << ADC_CDR_RDATA_SLV_Pos) //!< 0x01000000 
.equ ADC_CDR_RDATA_SLV_9, (0x0200 << ADC_CDR_RDATA_SLV_Pos) //!< 0x02000000 
.equ ADC_CDR_RDATA_SLV_10, (0x0400 << ADC_CDR_RDATA_SLV_Pos) //!< 0x04000000 
.equ ADC_CDR_RDATA_SLV_11, (0x0800 << ADC_CDR_RDATA_SLV_Pos) //!< 0x08000000 
.equ ADC_CDR_RDATA_SLV_12, (0x1000 << ADC_CDR_RDATA_SLV_Pos) //!< 0x10000000 
.equ ADC_CDR_RDATA_SLV_13, (0x2000 << ADC_CDR_RDATA_SLV_Pos) //!< 0x20000000 
.equ ADC_CDR_RDATA_SLV_14, (0x4000 << ADC_CDR_RDATA_SLV_Pos) //!< 0x40000000 
.equ ADC_CDR_RDATA_SLV_15, (0x8000 << ADC_CDR_RDATA_SLV_Pos) //!< 0x80000000 
//****************************************************************************
//
//                      Analog Comparators (COMP)
//
//****************************************************************************
//*********************  Bit definition for COMP1_CSR register  **************
.equ COMP1_CSR_COMP1EN_Pos, (0) 
.equ COMP1_CSR_COMP1EN_Msk, (0x1 << COMP1_CSR_COMP1EN_Pos) //!< 0x00000001 
.equ COMP1_CSR_COMP1EN, COMP1_CSR_COMP1EN_Msk //!< COMP1 enable 
.equ COMP1_CSR_COMP1SW1_Pos, (1) 
.equ COMP1_CSR_COMP1SW1_Msk, (0x1 << COMP1_CSR_COMP1SW1_Pos) //!< 0x00000002 
.equ COMP1_CSR_COMP1SW1, COMP1_CSR_COMP1SW1_Msk //!< COMP1 SW1 switch control 
// Legacy defines
.equ COMP_CSR_COMP1SW1, COMP1_CSR_COMP1SW1 
.equ COMP1_CSR_COMP1INSEL_Pos, (4) 
.equ COMP1_CSR_COMP1INSEL_Msk, (0x7 << COMP1_CSR_COMP1INSEL_Pos) //!< 0x00000070 
.equ COMP1_CSR_COMP1INSEL, COMP1_CSR_COMP1INSEL_Msk //!< COMP1 inverting input select 
.equ COMP1_CSR_COMP1INSEL_0, (0x1 << COMP1_CSR_COMP1INSEL_Pos) //!< 0x00000010 
.equ COMP1_CSR_COMP1INSEL_1, (0x2 << COMP1_CSR_COMP1INSEL_Pos) //!< 0x00000020 
.equ COMP1_CSR_COMP1INSEL_2, (0x4 << COMP1_CSR_COMP1INSEL_Pos) //!< 0x00000040 
.equ COMP1_CSR_COMP1OUTSEL_Pos, (10) 
.equ COMP1_CSR_COMP1OUTSEL_Msk, (0xF << COMP1_CSR_COMP1OUTSEL_Pos) //!< 0x00003C00 
.equ COMP1_CSR_COMP1OUTSEL, COMP1_CSR_COMP1OUTSEL_Msk //!< COMP1 output select 
.equ COMP1_CSR_COMP1OUTSEL_0, (0x1 << COMP1_CSR_COMP1OUTSEL_Pos) //!< 0x00000400 
.equ COMP1_CSR_COMP1OUTSEL_1, (0x2 << COMP1_CSR_COMP1OUTSEL_Pos) //!< 0x00000800 
.equ COMP1_CSR_COMP1OUTSEL_2, (0x4 << COMP1_CSR_COMP1OUTSEL_Pos) //!< 0x00001000 
.equ COMP1_CSR_COMP1OUTSEL_3, (0x8 << COMP1_CSR_COMP1OUTSEL_Pos) //!< 0x00002000 
.equ COMP1_CSR_COMP1POL_Pos, (15) 
.equ COMP1_CSR_COMP1POL_Msk, (0x1 << COMP1_CSR_COMP1POL_Pos) //!< 0x00008000 
.equ COMP1_CSR_COMP1POL, COMP1_CSR_COMP1POL_Msk //!< COMP1 output polarity 
.equ COMP1_CSR_COMP1BLANKING_Pos, (18) 
.equ COMP1_CSR_COMP1BLANKING_Msk, (0x3 << COMP1_CSR_COMP1BLANKING_Pos) //!< 0x000C0000 
.equ COMP1_CSR_COMP1BLANKING, COMP1_CSR_COMP1BLANKING_Msk //!< COMP1 blanking 
.equ COMP1_CSR_COMP1BLANKING_0, (0x1 << COMP1_CSR_COMP1BLANKING_Pos) //!< 0x00040000 
.equ COMP1_CSR_COMP1BLANKING_1, (0x2 << COMP1_CSR_COMP1BLANKING_Pos) //!< 0x00080000 
.equ COMP1_CSR_COMP1BLANKING_2, (0x4 << COMP1_CSR_COMP1BLANKING_Pos) //!< 0x00100000 
.equ COMP1_CSR_COMP1OUT_Pos, (30) 
.equ COMP1_CSR_COMP1OUT_Msk, (0x1 << COMP1_CSR_COMP1OUT_Pos) //!< 0x40000000 
.equ COMP1_CSR_COMP1OUT, COMP1_CSR_COMP1OUT_Msk //!< COMP1 output level 
.equ COMP1_CSR_COMP1LOCK_Pos, (31) 
.equ COMP1_CSR_COMP1LOCK_Msk, (0x1 << COMP1_CSR_COMP1LOCK_Pos) //!< 0x80000000 
.equ COMP1_CSR_COMP1LOCK, COMP1_CSR_COMP1LOCK_Msk //!< COMP1 lock 
//*********************  Bit definition for COMP2_CSR register  **************
.equ COMP2_CSR_COMP2EN_Pos, (0) 
.equ COMP2_CSR_COMP2EN_Msk, (0x1 << COMP2_CSR_COMP2EN_Pos) //!< 0x00000001 
.equ COMP2_CSR_COMP2EN, COMP2_CSR_COMP2EN_Msk //!< COMP2 enable 
.equ COMP2_CSR_COMP2INSEL_Pos, (4) 
.equ COMP2_CSR_COMP2INSEL_Msk, (0x7 << COMP2_CSR_COMP2INSEL_Pos) //!< 0x00000070 
.equ COMP2_CSR_COMP2INSEL, COMP2_CSR_COMP2INSEL_Msk //!< COMP2 inverting input select 
.equ COMP2_CSR_COMP2INSEL_0, (0x00000010) //!< COMP2 inverting input select bit 0 
.equ COMP2_CSR_COMP2INSEL_1, (0x00000020) //!< COMP2 inverting input select bit 1 
.equ COMP2_CSR_COMP2INSEL_2, (0x00000040) //!< COMP2 inverting input select bit 2 
.equ COMP2_CSR_COMP2OUTSEL_Pos, (10) 
.equ COMP2_CSR_COMP2OUTSEL_Msk, (0xF << COMP2_CSR_COMP2OUTSEL_Pos) //!< 0x00003C00 
.equ COMP2_CSR_COMP2OUTSEL, COMP2_CSR_COMP2OUTSEL_Msk //!< COMP2 output select 
.equ COMP2_CSR_COMP2OUTSEL_0, (0x1 << COMP2_CSR_COMP2OUTSEL_Pos) //!< 0x00000400 
.equ COMP2_CSR_COMP2OUTSEL_1, (0x2 << COMP2_CSR_COMP2OUTSEL_Pos) //!< 0x00000800 
.equ COMP2_CSR_COMP2OUTSEL_2, (0x4 << COMP2_CSR_COMP2OUTSEL_Pos) //!< 0x00001000 
.equ COMP2_CSR_COMP2OUTSEL_3, (0x8 << COMP2_CSR_COMP2OUTSEL_Pos) //!< 0x00002000 
.equ COMP2_CSR_COMP2POL_Pos, (15) 
.equ COMP2_CSR_COMP2POL_Msk, (0x1 << COMP2_CSR_COMP2POL_Pos) //!< 0x00008000 
.equ COMP2_CSR_COMP2POL, COMP2_CSR_COMP2POL_Msk //!< COMP2 output polarity 
.equ COMP2_CSR_COMP2BLANKING_Pos, (18) 
.equ COMP2_CSR_COMP2BLANKING_Msk, (0x3 << COMP2_CSR_COMP2BLANKING_Pos) //!< 0x000C0000 
.equ COMP2_CSR_COMP2BLANKING, COMP2_CSR_COMP2BLANKING_Msk //!< COMP2 blanking 
.equ COMP2_CSR_COMP2BLANKING_0, (0x1 << COMP2_CSR_COMP2BLANKING_Pos) //!< 0x00040000 
.equ COMP2_CSR_COMP2BLANKING_1, (0x2 << COMP2_CSR_COMP2BLANKING_Pos) //!< 0x00080000 
.equ COMP2_CSR_COMP2BLANKING_2, (0x4 << COMP2_CSR_COMP2BLANKING_Pos) //!< 0x00100000 
.equ COMP2_CSR_COMP2OUT_Pos, (30) 
.equ COMP2_CSR_COMP2OUT_Msk, (0x1 << COMP2_CSR_COMP2OUT_Pos) //!< 0x40000000 
.equ COMP2_CSR_COMP2OUT, COMP2_CSR_COMP2OUT_Msk //!< COMP2 output level 
.equ COMP2_CSR_COMP2LOCK_Pos, (31) 
.equ COMP2_CSR_COMP2LOCK_Msk, (0x1 << COMP2_CSR_COMP2LOCK_Pos) //!< 0x80000000 
.equ COMP2_CSR_COMP2LOCK, COMP2_CSR_COMP2LOCK_Msk //!< COMP2 lock 
//*********************  Bit definition for COMP3_CSR register  **************
.equ COMP3_CSR_COMP3EN_Pos, (0) 
.equ COMP3_CSR_COMP3EN_Msk, (0x1 << COMP3_CSR_COMP3EN_Pos) //!< 0x00000001 
.equ COMP3_CSR_COMP3EN, COMP3_CSR_COMP3EN_Msk //!< COMP3 enable 
.equ COMP3_CSR_COMP3INSEL_Pos, (4) 
.equ COMP3_CSR_COMP3INSEL_Msk, (0x7 << COMP3_CSR_COMP3INSEL_Pos) //!< 0x00000070 
.equ COMP3_CSR_COMP3INSEL, COMP3_CSR_COMP3INSEL_Msk //!< COMP3 inverting input select 
.equ COMP3_CSR_COMP3INSEL_0, (0x1 << COMP3_CSR_COMP3INSEL_Pos) //!< 0x00000010 
.equ COMP3_CSR_COMP3INSEL_1, (0x2 << COMP3_CSR_COMP3INSEL_Pos) //!< 0x00000020 
.equ COMP3_CSR_COMP3INSEL_2, (0x4 << COMP3_CSR_COMP3INSEL_Pos) //!< 0x00000040 
.equ COMP3_CSR_COMP3OUTSEL_Pos, (10) 
.equ COMP3_CSR_COMP3OUTSEL_Msk, (0xF << COMP3_CSR_COMP3OUTSEL_Pos) //!< 0x00003C00 
.equ COMP3_CSR_COMP3OUTSEL, COMP3_CSR_COMP3OUTSEL_Msk //!< COMP3 output select 
.equ COMP3_CSR_COMP3OUTSEL_0, (0x1 << COMP3_CSR_COMP3OUTSEL_Pos) //!< 0x00000400 
.equ COMP3_CSR_COMP3OUTSEL_1, (0x2 << COMP3_CSR_COMP3OUTSEL_Pos) //!< 0x00000800 
.equ COMP3_CSR_COMP3OUTSEL_2, (0x4 << COMP3_CSR_COMP3OUTSEL_Pos) //!< 0x00001000 
.equ COMP3_CSR_COMP3OUTSEL_3, (0x8 << COMP3_CSR_COMP3OUTSEL_Pos) //!< 0x00002000 
.equ COMP3_CSR_COMP3POL_Pos, (15) 
.equ COMP3_CSR_COMP3POL_Msk, (0x1 << COMP3_CSR_COMP3POL_Pos) //!< 0x00008000 
.equ COMP3_CSR_COMP3POL, COMP3_CSR_COMP3POL_Msk //!< COMP3 output polarity 
.equ COMP3_CSR_COMP3BLANKING_Pos, (18) 
.equ COMP3_CSR_COMP3BLANKING_Msk, (0x3 << COMP3_CSR_COMP3BLANKING_Pos) //!< 0x000C0000 
.equ COMP3_CSR_COMP3BLANKING, COMP3_CSR_COMP3BLANKING_Msk //!< COMP3 blanking 
.equ COMP3_CSR_COMP3BLANKING_0, (0x1 << COMP3_CSR_COMP3BLANKING_Pos) //!< 0x00040000 
.equ COMP3_CSR_COMP3BLANKING_1, (0x2 << COMP3_CSR_COMP3BLANKING_Pos) //!< 0x00080000 
.equ COMP3_CSR_COMP3BLANKING_2, (0x4 << COMP3_CSR_COMP3BLANKING_Pos) //!< 0x00100000 
.equ COMP3_CSR_COMP3OUT_Pos, (30) 
.equ COMP3_CSR_COMP3OUT_Msk, (0x1 << COMP3_CSR_COMP3OUT_Pos) //!< 0x40000000 
.equ COMP3_CSR_COMP3OUT, COMP3_CSR_COMP3OUT_Msk //!< COMP3 output level 
.equ COMP3_CSR_COMP3LOCK_Pos, (31) 
.equ COMP3_CSR_COMP3LOCK_Msk, (0x1 << COMP3_CSR_COMP3LOCK_Pos) //!< 0x80000000 
.equ COMP3_CSR_COMP3LOCK, COMP3_CSR_COMP3LOCK_Msk //!< COMP3 lock 
//*********************  Bit definition for COMP4_CSR register  **************
.equ COMP4_CSR_COMP4EN_Pos, (0) 
.equ COMP4_CSR_COMP4EN_Msk, (0x1 << COMP4_CSR_COMP4EN_Pos) //!< 0x00000001 
.equ COMP4_CSR_COMP4EN, COMP4_CSR_COMP4EN_Msk //!< COMP4 enable 
.equ COMP4_CSR_COMP4INSEL_Pos, (4) 
.equ COMP4_CSR_COMP4INSEL_Msk, (0x7 << COMP4_CSR_COMP4INSEL_Pos) //!< 0x00000070 
.equ COMP4_CSR_COMP4INSEL, COMP4_CSR_COMP4INSEL_Msk //!< COMP4 inverting input select 
.equ COMP4_CSR_COMP4INSEL_0, (0x00000010) //!< COMP4 inverting input select bit 0 
.equ COMP4_CSR_COMP4INSEL_1, (0x00000020) //!< COMP4 inverting input select bit 1 
.equ COMP4_CSR_COMP4INSEL_2, (0x00000040) //!< COMP4 inverting input select bit 2 
.equ COMP4_CSR_COMP4OUTSEL_Pos, (10) 
.equ COMP4_CSR_COMP4OUTSEL_Msk, (0xF << COMP4_CSR_COMP4OUTSEL_Pos) //!< 0x00003C00 
.equ COMP4_CSR_COMP4OUTSEL, COMP4_CSR_COMP4OUTSEL_Msk //!< COMP4 output select 
.equ COMP4_CSR_COMP4OUTSEL_0, (0x1 << COMP4_CSR_COMP4OUTSEL_Pos) //!< 0x00000400 
.equ COMP4_CSR_COMP4OUTSEL_1, (0x2 << COMP4_CSR_COMP4OUTSEL_Pos) //!< 0x00000800 
.equ COMP4_CSR_COMP4OUTSEL_2, (0x4 << COMP4_CSR_COMP4OUTSEL_Pos) //!< 0x00001000 
.equ COMP4_CSR_COMP4OUTSEL_3, (0x8 << COMP4_CSR_COMP4OUTSEL_Pos) //!< 0x00002000 
.equ COMP4_CSR_COMP4POL_Pos, (15) 
.equ COMP4_CSR_COMP4POL_Msk, (0x1 << COMP4_CSR_COMP4POL_Pos) //!< 0x00008000 
.equ COMP4_CSR_COMP4POL, COMP4_CSR_COMP4POL_Msk //!< COMP4 output polarity 
.equ COMP4_CSR_COMP4BLANKING_Pos, (18) 
.equ COMP4_CSR_COMP4BLANKING_Msk, (0x3 << COMP4_CSR_COMP4BLANKING_Pos) //!< 0x000C0000 
.equ COMP4_CSR_COMP4BLANKING, COMP4_CSR_COMP4BLANKING_Msk //!< COMP4 blanking 
.equ COMP4_CSR_COMP4BLANKING_0, (0x1 << COMP4_CSR_COMP4BLANKING_Pos) //!< 0x00040000 
.equ COMP4_CSR_COMP4BLANKING_1, (0x2 << COMP4_CSR_COMP4BLANKING_Pos) //!< 0x00080000 
.equ COMP4_CSR_COMP4BLANKING_2, (0x4 << COMP4_CSR_COMP4BLANKING_Pos) //!< 0x00100000 
.equ COMP4_CSR_COMP4OUT_Pos, (30) 
.equ COMP4_CSR_COMP4OUT_Msk, (0x1 << COMP4_CSR_COMP4OUT_Pos) //!< 0x40000000 
.equ COMP4_CSR_COMP4OUT, COMP4_CSR_COMP4OUT_Msk //!< COMP4 output level 
.equ COMP4_CSR_COMP4LOCK_Pos, (31) 
.equ COMP4_CSR_COMP4LOCK_Msk, (0x1 << COMP4_CSR_COMP4LOCK_Pos) //!< 0x80000000 
.equ COMP4_CSR_COMP4LOCK, COMP4_CSR_COMP4LOCK_Msk //!< COMP4 lock 
//*********************  Bit definition for COMP5_CSR register  **************
.equ COMP5_CSR_COMP5EN_Pos, (0) 
.equ COMP5_CSR_COMP5EN_Msk, (0x1 << COMP5_CSR_COMP5EN_Pos) //!< 0x00000001 
.equ COMP5_CSR_COMP5EN, COMP5_CSR_COMP5EN_Msk //!< COMP5 enable 
.equ COMP5_CSR_COMP5INSEL_Pos, (4) 
.equ COMP5_CSR_COMP5INSEL_Msk, (0x7 << COMP5_CSR_COMP5INSEL_Pos) //!< 0x00000070 
.equ COMP5_CSR_COMP5INSEL, COMP5_CSR_COMP5INSEL_Msk //!< COMP5 inverting input select 
.equ COMP5_CSR_COMP5INSEL_0, (0x1 << COMP5_CSR_COMP5INSEL_Pos) //!< 0x00000010 
.equ COMP5_CSR_COMP5INSEL_1, (0x2 << COMP5_CSR_COMP5INSEL_Pos) //!< 0x00000020 
.equ COMP5_CSR_COMP5INSEL_2, (0x4 << COMP5_CSR_COMP5INSEL_Pos) //!< 0x00000040 
.equ COMP5_CSR_COMP5OUTSEL_Pos, (10) 
.equ COMP5_CSR_COMP5OUTSEL_Msk, (0xF << COMP5_CSR_COMP5OUTSEL_Pos) //!< 0x00003C00 
.equ COMP5_CSR_COMP5OUTSEL, COMP5_CSR_COMP5OUTSEL_Msk //!< COMP5 output select 
.equ COMP5_CSR_COMP5OUTSEL_0, (0x1 << COMP5_CSR_COMP5OUTSEL_Pos) //!< 0x00000400 
.equ COMP5_CSR_COMP5OUTSEL_1, (0x2 << COMP5_CSR_COMP5OUTSEL_Pos) //!< 0x00000800 
.equ COMP5_CSR_COMP5OUTSEL_2, (0x4 << COMP5_CSR_COMP5OUTSEL_Pos) //!< 0x00001000 
.equ COMP5_CSR_COMP5OUTSEL_3, (0x8 << COMP5_CSR_COMP5OUTSEL_Pos) //!< 0x00002000 
.equ COMP5_CSR_COMP5POL_Pos, (15) 
.equ COMP5_CSR_COMP5POL_Msk, (0x1 << COMP5_CSR_COMP5POL_Pos) //!< 0x00008000 
.equ COMP5_CSR_COMP5POL, COMP5_CSR_COMP5POL_Msk //!< COMP5 output polarity 
.equ COMP5_CSR_COMP5BLANKING_Pos, (18) 
.equ COMP5_CSR_COMP5BLANKING_Msk, (0x3 << COMP5_CSR_COMP5BLANKING_Pos) //!< 0x000C0000 
.equ COMP5_CSR_COMP5BLANKING, COMP5_CSR_COMP5BLANKING_Msk //!< COMP5 blanking 
.equ COMP5_CSR_COMP5BLANKING_0, (0x1 << COMP5_CSR_COMP5BLANKING_Pos) //!< 0x00040000 
.equ COMP5_CSR_COMP5BLANKING_1, (0x2 << COMP5_CSR_COMP5BLANKING_Pos) //!< 0x00080000 
.equ COMP5_CSR_COMP5BLANKING_2, (0x4 << COMP5_CSR_COMP5BLANKING_Pos) //!< 0x00100000 
.equ COMP5_CSR_COMP5OUT_Pos, (30) 
.equ COMP5_CSR_COMP5OUT_Msk, (0x1 << COMP5_CSR_COMP5OUT_Pos) //!< 0x40000000 
.equ COMP5_CSR_COMP5OUT, COMP5_CSR_COMP5OUT_Msk //!< COMP5 output level 
.equ COMP5_CSR_COMP5LOCK_Pos, (31) 
.equ COMP5_CSR_COMP5LOCK_Msk, (0x1 << COMP5_CSR_COMP5LOCK_Pos) //!< 0x80000000 
.equ COMP5_CSR_COMP5LOCK, COMP5_CSR_COMP5LOCK_Msk //!< COMP5 lock 
//*********************  Bit definition for COMP6_CSR register  **************
.equ COMP6_CSR_COMP6EN_Pos, (0) 
.equ COMP6_CSR_COMP6EN_Msk, (0x1 << COMP6_CSR_COMP6EN_Pos) //!< 0x00000001 
.equ COMP6_CSR_COMP6EN, COMP6_CSR_COMP6EN_Msk //!< COMP6 enable 
.equ COMP6_CSR_COMP6INSEL_Pos, (4) 
.equ COMP6_CSR_COMP6INSEL_Msk, (0x7 << COMP6_CSR_COMP6INSEL_Pos) //!< 0x00000070 
.equ COMP6_CSR_COMP6INSEL, COMP6_CSR_COMP6INSEL_Msk //!< COMP6 inverting input select 
.equ COMP6_CSR_COMP6INSEL_0, (0x00000010) //!< COMP6 inverting input select bit 0 
.equ COMP6_CSR_COMP6INSEL_1, (0x00000020) //!< COMP6 inverting input select bit 1 
.equ COMP6_CSR_COMP6INSEL_2, (0x00000040) //!< COMP6 inverting input select bit 2 
.equ COMP6_CSR_COMP6OUTSEL_Pos, (10) 
.equ COMP6_CSR_COMP6OUTSEL_Msk, (0xF << COMP6_CSR_COMP6OUTSEL_Pos) //!< 0x00003C00 
.equ COMP6_CSR_COMP6OUTSEL, COMP6_CSR_COMP6OUTSEL_Msk //!< COMP6 output select 
.equ COMP6_CSR_COMP6OUTSEL_0, (0x1 << COMP6_CSR_COMP6OUTSEL_Pos) //!< 0x00000400 
.equ COMP6_CSR_COMP6OUTSEL_1, (0x2 << COMP6_CSR_COMP6OUTSEL_Pos) //!< 0x00000800 
.equ COMP6_CSR_COMP6OUTSEL_2, (0x4 << COMP6_CSR_COMP6OUTSEL_Pos) //!< 0x00001000 
.equ COMP6_CSR_COMP6OUTSEL_3, (0x8 << COMP6_CSR_COMP6OUTSEL_Pos) //!< 0x00002000 
.equ COMP6_CSR_COMP6POL_Pos, (15) 
.equ COMP6_CSR_COMP6POL_Msk, (0x1 << COMP6_CSR_COMP6POL_Pos) //!< 0x00008000 
.equ COMP6_CSR_COMP6POL, COMP6_CSR_COMP6POL_Msk //!< COMP6 output polarity 
.equ COMP6_CSR_COMP6BLANKING_Pos, (18) 
.equ COMP6_CSR_COMP6BLANKING_Msk, (0x3 << COMP6_CSR_COMP6BLANKING_Pos) //!< 0x000C0000 
.equ COMP6_CSR_COMP6BLANKING, COMP6_CSR_COMP6BLANKING_Msk //!< COMP6 blanking 
.equ COMP6_CSR_COMP6BLANKING_0, (0x1 << COMP6_CSR_COMP6BLANKING_Pos) //!< 0x00040000 
.equ COMP6_CSR_COMP6BLANKING_1, (0x2 << COMP6_CSR_COMP6BLANKING_Pos) //!< 0x00080000 
.equ COMP6_CSR_COMP6BLANKING_2, (0x4 << COMP6_CSR_COMP6BLANKING_Pos) //!< 0x00100000 
.equ COMP6_CSR_COMP6OUT_Pos, (30) 
.equ COMP6_CSR_COMP6OUT_Msk, (0x1 << COMP6_CSR_COMP6OUT_Pos) //!< 0x40000000 
.equ COMP6_CSR_COMP6OUT, COMP6_CSR_COMP6OUT_Msk //!< COMP6 output level 
.equ COMP6_CSR_COMP6LOCK_Pos, (31) 
.equ COMP6_CSR_COMP6LOCK_Msk, (0x1 << COMP6_CSR_COMP6LOCK_Pos) //!< 0x80000000 
.equ COMP6_CSR_COMP6LOCK, COMP6_CSR_COMP6LOCK_Msk //!< COMP6 lock 
//*********************  Bit definition for COMP7_CSR register  **************
.equ COMP7_CSR_COMP7EN_Pos, (0) 
.equ COMP7_CSR_COMP7EN_Msk, (0x1 << COMP7_CSR_COMP7EN_Pos) //!< 0x00000001 
.equ COMP7_CSR_COMP7EN, COMP7_CSR_COMP7EN_Msk //!< COMP7 enable 
.equ COMP7_CSR_COMP7INSEL_Pos, (4) 
.equ COMP7_CSR_COMP7INSEL_Msk, (0x7 << COMP7_CSR_COMP7INSEL_Pos) //!< 0x00000070 
.equ COMP7_CSR_COMP7INSEL, COMP7_CSR_COMP7INSEL_Msk //!< COMP7 inverting input select 
.equ COMP7_CSR_COMP7INSEL_0, (0x1 << COMP7_CSR_COMP7INSEL_Pos) //!< 0x00000010 
.equ COMP7_CSR_COMP7INSEL_1, (0x2 << COMP7_CSR_COMP7INSEL_Pos) //!< 0x00000020 
.equ COMP7_CSR_COMP7INSEL_2, (0x4 << COMP7_CSR_COMP7INSEL_Pos) //!< 0x00000040 
.equ COMP7_CSR_COMP7OUTSEL_Pos, (10) 
.equ COMP7_CSR_COMP7OUTSEL_Msk, (0xF << COMP7_CSR_COMP7OUTSEL_Pos) //!< 0x00003C00 
.equ COMP7_CSR_COMP7OUTSEL, COMP7_CSR_COMP7OUTSEL_Msk //!< COMP7 output select 
.equ COMP7_CSR_COMP7OUTSEL_0, (0x1 << COMP7_CSR_COMP7OUTSEL_Pos) //!< 0x00000400 
.equ COMP7_CSR_COMP7OUTSEL_1, (0x2 << COMP7_CSR_COMP7OUTSEL_Pos) //!< 0x00000800 
.equ COMP7_CSR_COMP7OUTSEL_2, (0x4 << COMP7_CSR_COMP7OUTSEL_Pos) //!< 0x00001000 
.equ COMP7_CSR_COMP7OUTSEL_3, (0x8 << COMP7_CSR_COMP7OUTSEL_Pos) //!< 0x00002000 
.equ COMP7_CSR_COMP7POL_Pos, (15) 
.equ COMP7_CSR_COMP7POL_Msk, (0x1 << COMP7_CSR_COMP7POL_Pos) //!< 0x00008000 
.equ COMP7_CSR_COMP7POL, COMP7_CSR_COMP7POL_Msk //!< COMP7 output polarity 
.equ COMP7_CSR_COMP7BLANKING_Pos, (18) 
.equ COMP7_CSR_COMP7BLANKING_Msk, (0x3 << COMP7_CSR_COMP7BLANKING_Pos) //!< 0x000C0000 
.equ COMP7_CSR_COMP7BLANKING, COMP7_CSR_COMP7BLANKING_Msk //!< COMP7 blanking 
.equ COMP7_CSR_COMP7BLANKING_0, (0x1 << COMP7_CSR_COMP7BLANKING_Pos) //!< 0x00040000 
.equ COMP7_CSR_COMP7BLANKING_1, (0x2 << COMP7_CSR_COMP7BLANKING_Pos) //!< 0x00080000 
.equ COMP7_CSR_COMP7BLANKING_2, (0x4 << COMP7_CSR_COMP7BLANKING_Pos) //!< 0x00100000 
.equ COMP7_CSR_COMP7OUT_Pos, (30) 
.equ COMP7_CSR_COMP7OUT_Msk, (0x1 << COMP7_CSR_COMP7OUT_Pos) //!< 0x40000000 
.equ COMP7_CSR_COMP7OUT, COMP7_CSR_COMP7OUT_Msk //!< COMP7 output level 
.equ COMP7_CSR_COMP7LOCK_Pos, (31) 
.equ COMP7_CSR_COMP7LOCK_Msk, (0x1 << COMP7_CSR_COMP7LOCK_Pos) //!< 0x80000000 
.equ COMP7_CSR_COMP7LOCK, COMP7_CSR_COMP7LOCK_Msk //!< COMP7 lock 
//*********************  Bit definition for COMP_CSR register  ***************
.equ COMP_CSR_COMPxEN_Pos, (0) 
.equ COMP_CSR_COMPxEN_Msk, (0x1 << COMP_CSR_COMPxEN_Pos) //!< 0x00000001 
.equ COMP_CSR_COMPxEN, COMP_CSR_COMPxEN_Msk //!< COMPx enable 
.equ COMP_CSR_COMPxSW1_Pos, (1) 
.equ COMP_CSR_COMPxSW1_Msk, (0x1 << COMP_CSR_COMPxSW1_Pos) //!< 0x00000002 
.equ COMP_CSR_COMPxSW1, COMP_CSR_COMPxSW1_Msk //!< COMPx SW1 switch control 
.equ COMP_CSR_COMPxINSEL_Pos, (4) 
.equ COMP_CSR_COMPxINSEL_Msk, (0x7 << COMP_CSR_COMPxINSEL_Pos) //!< 0x00000070 
.equ COMP_CSR_COMPxINSEL, COMP_CSR_COMPxINSEL_Msk //!< COMPx inverting input select 
.equ COMP_CSR_COMPxINSEL_0, (0x00000010) //!< COMPx inverting input select bit 0 
.equ COMP_CSR_COMPxINSEL_1, (0x00000020) //!< COMPx inverting input select bit 1 
.equ COMP_CSR_COMPxINSEL_2, (0x00000040) //!< COMPx inverting input select bit 2 
.equ COMP_CSR_COMPxOUTSEL_Pos, (10) 
.equ COMP_CSR_COMPxOUTSEL_Msk, (0xF << COMP_CSR_COMPxOUTSEL_Pos) //!< 0x00003C00 
.equ COMP_CSR_COMPxOUTSEL, COMP_CSR_COMPxOUTSEL_Msk //!< COMPx output select 
.equ COMP_CSR_COMPxOUTSEL_0, (0x1 << COMP_CSR_COMPxOUTSEL_Pos) //!< 0x00000400 
.equ COMP_CSR_COMPxOUTSEL_1, (0x2 << COMP_CSR_COMPxOUTSEL_Pos) //!< 0x00000800 
.equ COMP_CSR_COMPxOUTSEL_2, (0x4 << COMP_CSR_COMPxOUTSEL_Pos) //!< 0x00001000 
.equ COMP_CSR_COMPxOUTSEL_3, (0x8 << COMP_CSR_COMPxOUTSEL_Pos) //!< 0x00002000 
.equ COMP_CSR_COMPxPOL_Pos, (15) 
.equ COMP_CSR_COMPxPOL_Msk, (0x1 << COMP_CSR_COMPxPOL_Pos) //!< 0x00008000 
.equ COMP_CSR_COMPxPOL, COMP_CSR_COMPxPOL_Msk //!< COMPx output polarity 
.equ COMP_CSR_COMPxBLANKING_Pos, (18) 
.equ COMP_CSR_COMPxBLANKING_Msk, (0x3 << COMP_CSR_COMPxBLANKING_Pos) //!< 0x000C0000 
.equ COMP_CSR_COMPxBLANKING, COMP_CSR_COMPxBLANKING_Msk //!< COMPx blanking 
.equ COMP_CSR_COMPxBLANKING_0, (0x1 << COMP_CSR_COMPxBLANKING_Pos) //!< 0x00040000 
.equ COMP_CSR_COMPxBLANKING_1, (0x2 << COMP_CSR_COMPxBLANKING_Pos) //!< 0x00080000 
.equ COMP_CSR_COMPxBLANKING_2, (0x4 << COMP_CSR_COMPxBLANKING_Pos) //!< 0x00100000 
.equ COMP_CSR_COMPxOUT_Pos, (30) 
.equ COMP_CSR_COMPxOUT_Msk, (0x1 << COMP_CSR_COMPxOUT_Pos) //!< 0x40000000 
.equ COMP_CSR_COMPxOUT, COMP_CSR_COMPxOUT_Msk //!< COMPx output level 
.equ COMP_CSR_COMPxLOCK_Pos, (31) 
.equ COMP_CSR_COMPxLOCK_Msk, (0x1 << COMP_CSR_COMPxLOCK_Pos) //!< 0x80000000 
.equ COMP_CSR_COMPxLOCK, COMP_CSR_COMPxLOCK_Msk //!< COMPx lock 
//****************************************************************************
//
//                     Operational Amplifier (OPAMP)
//
//****************************************************************************
//********************  Bit definition for OPAMP1_CSR register  **************
.equ OPAMP1_CSR_OPAMP1EN_Pos, (0) 
.equ OPAMP1_CSR_OPAMP1EN_Msk, (0x1 << OPAMP1_CSR_OPAMP1EN_Pos) //!< 0x00000001 
.equ OPAMP1_CSR_OPAMP1EN, OPAMP1_CSR_OPAMP1EN_Msk //!< OPAMP1 enable 
.equ OPAMP1_CSR_FORCEVP_Pos, (1) 
.equ OPAMP1_CSR_FORCEVP_Msk, (0x1 << OPAMP1_CSR_FORCEVP_Pos) //!< 0x00000002 
.equ OPAMP1_CSR_FORCEVP, OPAMP1_CSR_FORCEVP_Msk //!< Connect the internal references to the plus input of the OPAMPX 
.equ OPAMP1_CSR_VPSEL_Pos, (2) 
.equ OPAMP1_CSR_VPSEL_Msk, (0x3 << OPAMP1_CSR_VPSEL_Pos) //!< 0x0000000C 
.equ OPAMP1_CSR_VPSEL, OPAMP1_CSR_VPSEL_Msk //!< Non inverting input selection 
.equ OPAMP1_CSR_VPSEL_0, (0x1 << OPAMP1_CSR_VPSEL_Pos) //!< 0x00000004 
.equ OPAMP1_CSR_VPSEL_1, (0x2 << OPAMP1_CSR_VPSEL_Pos) //!< 0x00000008 
.equ OPAMP1_CSR_VMSEL_Pos, (5) 
.equ OPAMP1_CSR_VMSEL_Msk, (0x3 << OPAMP1_CSR_VMSEL_Pos) //!< 0x00000060 
.equ OPAMP1_CSR_VMSEL, OPAMP1_CSR_VMSEL_Msk //!< Inverting input selection 
.equ OPAMP1_CSR_VMSEL_0, (0x1 << OPAMP1_CSR_VMSEL_Pos) //!< 0x00000020 
.equ OPAMP1_CSR_VMSEL_1, (0x2 << OPAMP1_CSR_VMSEL_Pos) //!< 0x00000040 
.equ OPAMP1_CSR_TCMEN_Pos, (7) 
.equ OPAMP1_CSR_TCMEN_Msk, (0x1 << OPAMP1_CSR_TCMEN_Pos) //!< 0x00000080 
.equ OPAMP1_CSR_TCMEN, OPAMP1_CSR_TCMEN_Msk //!< Timer-Controlled Mux mode enable 
.equ OPAMP1_CSR_VMSSEL_Pos, (8) 
.equ OPAMP1_CSR_VMSSEL_Msk, (0x1 << OPAMP1_CSR_VMSSEL_Pos) //!< 0x00000100 
.equ OPAMP1_CSR_VMSSEL, OPAMP1_CSR_VMSSEL_Msk //!< Inverting input secondary selection 
.equ OPAMP1_CSR_VPSSEL_Pos, (9) 
.equ OPAMP1_CSR_VPSSEL_Msk, (0x3 << OPAMP1_CSR_VPSSEL_Pos) //!< 0x00000600 
.equ OPAMP1_CSR_VPSSEL, OPAMP1_CSR_VPSSEL_Msk //!< Non inverting input secondary selection 
.equ OPAMP1_CSR_VPSSEL_0, (0x1 << OPAMP1_CSR_VPSSEL_Pos) //!< 0x00000200 
.equ OPAMP1_CSR_VPSSEL_1, (0x2 << OPAMP1_CSR_VPSSEL_Pos) //!< 0x00000400 
.equ OPAMP1_CSR_CALON_Pos, (11) 
.equ OPAMP1_CSR_CALON_Msk, (0x1 << OPAMP1_CSR_CALON_Pos) //!< 0x00000800 
.equ OPAMP1_CSR_CALON, OPAMP1_CSR_CALON_Msk //!< Calibration mode enable 
.equ OPAMP1_CSR_CALSEL_Pos, (12) 
.equ OPAMP1_CSR_CALSEL_Msk, (0x3 << OPAMP1_CSR_CALSEL_Pos) //!< 0x00003000 
.equ OPAMP1_CSR_CALSEL, OPAMP1_CSR_CALSEL_Msk //!< Calibration selection 
.equ OPAMP1_CSR_CALSEL_0, (0x1 << OPAMP1_CSR_CALSEL_Pos) //!< 0x00001000 
.equ OPAMP1_CSR_CALSEL_1, (0x2 << OPAMP1_CSR_CALSEL_Pos) //!< 0x00002000 
.equ OPAMP1_CSR_PGGAIN_Pos, (14) 
.equ OPAMP1_CSR_PGGAIN_Msk, (0xF << OPAMP1_CSR_PGGAIN_Pos) //!< 0x0003C000 
.equ OPAMP1_CSR_PGGAIN, OPAMP1_CSR_PGGAIN_Msk //!< Gain in PGA mode 
.equ OPAMP1_CSR_PGGAIN_0, (0x1 << OPAMP1_CSR_PGGAIN_Pos) //!< 0x00004000 
.equ OPAMP1_CSR_PGGAIN_1, (0x2 << OPAMP1_CSR_PGGAIN_Pos) //!< 0x00008000 
.equ OPAMP1_CSR_PGGAIN_2, (0x4 << OPAMP1_CSR_PGGAIN_Pos) //!< 0x00010000 
.equ OPAMP1_CSR_PGGAIN_3, (0x8 << OPAMP1_CSR_PGGAIN_Pos) //!< 0x00020000 
.equ OPAMP1_CSR_USERTRIM_Pos, (18) 
.equ OPAMP1_CSR_USERTRIM_Msk, (0x1 << OPAMP1_CSR_USERTRIM_Pos) //!< 0x00040000 
.equ OPAMP1_CSR_USERTRIM, OPAMP1_CSR_USERTRIM_Msk //!< User trimming enable 
.equ OPAMP1_CSR_TRIMOFFSETP_Pos, (19) 
.equ OPAMP1_CSR_TRIMOFFSETP_Msk, (0x1F << OPAMP1_CSR_TRIMOFFSETP_Pos) //!< 0x00F80000 
.equ OPAMP1_CSR_TRIMOFFSETP, OPAMP1_CSR_TRIMOFFSETP_Msk //!< Offset trimming value (PMOS) 
.equ OPAMP1_CSR_TRIMOFFSETN_Pos, (24) 
.equ OPAMP1_CSR_TRIMOFFSETN_Msk, (0x1F << OPAMP1_CSR_TRIMOFFSETN_Pos) //!< 0x1F000000 
.equ OPAMP1_CSR_TRIMOFFSETN, OPAMP1_CSR_TRIMOFFSETN_Msk //!< Offset trimming value (NMOS) 
.equ OPAMP1_CSR_TSTREF_Pos, (29) 
.equ OPAMP1_CSR_TSTREF_Msk, (0x1 << OPAMP1_CSR_TSTREF_Pos) //!< 0x20000000 
.equ OPAMP1_CSR_TSTREF, OPAMP1_CSR_TSTREF_Msk //!< It enables the switch to put out the internal reference 
.equ OPAMP1_CSR_OUTCAL_Pos, (30) 
.equ OPAMP1_CSR_OUTCAL_Msk, (0x1 << OPAMP1_CSR_OUTCAL_Pos) //!< 0x40000000 
.equ OPAMP1_CSR_OUTCAL, OPAMP1_CSR_OUTCAL_Msk //!< OPAMP ouput status flag 
.equ OPAMP1_CSR_LOCK_Pos, (31) 
.equ OPAMP1_CSR_LOCK_Msk, (0x1 << OPAMP1_CSR_LOCK_Pos) //!< 0x80000000 
.equ OPAMP1_CSR_LOCK, OPAMP1_CSR_LOCK_Msk //!< OPAMP lock 
//********************  Bit definition for OPAMP2_CSR register  **************
.equ OPAMP2_CSR_OPAMP2EN_Pos, (0) 
.equ OPAMP2_CSR_OPAMP2EN_Msk, (0x1 << OPAMP2_CSR_OPAMP2EN_Pos) //!< 0x00000001 
.equ OPAMP2_CSR_OPAMP2EN, OPAMP2_CSR_OPAMP2EN_Msk //!< OPAMP2 enable 
.equ OPAMP2_CSR_FORCEVP_Pos, (1) 
.equ OPAMP2_CSR_FORCEVP_Msk, (0x1 << OPAMP2_CSR_FORCEVP_Pos) //!< 0x00000002 
.equ OPAMP2_CSR_FORCEVP, OPAMP2_CSR_FORCEVP_Msk //!< Connect the internal references to the plus input of the OPAMPX 
.equ OPAMP2_CSR_VPSEL_Pos, (2) 
.equ OPAMP2_CSR_VPSEL_Msk, (0x3 << OPAMP2_CSR_VPSEL_Pos) //!< 0x0000000C 
.equ OPAMP2_CSR_VPSEL, OPAMP2_CSR_VPSEL_Msk //!< Non inverting input selection 
.equ OPAMP2_CSR_VPSEL_0, (0x1 << OPAMP2_CSR_VPSEL_Pos) //!< 0x00000004 
.equ OPAMP2_CSR_VPSEL_1, (0x2 << OPAMP2_CSR_VPSEL_Pos) //!< 0x00000008 
.equ OPAMP2_CSR_VMSEL_Pos, (5) 
.equ OPAMP2_CSR_VMSEL_Msk, (0x3 << OPAMP2_CSR_VMSEL_Pos) //!< 0x00000060 
.equ OPAMP2_CSR_VMSEL, OPAMP2_CSR_VMSEL_Msk //!< Inverting input selection 
.equ OPAMP2_CSR_VMSEL_0, (0x1 << OPAMP2_CSR_VMSEL_Pos) //!< 0x00000020 
.equ OPAMP2_CSR_VMSEL_1, (0x2 << OPAMP2_CSR_VMSEL_Pos) //!< 0x00000040 
.equ OPAMP2_CSR_TCMEN_Pos, (7) 
.equ OPAMP2_CSR_TCMEN_Msk, (0x1 << OPAMP2_CSR_TCMEN_Pos) //!< 0x00000080 
.equ OPAMP2_CSR_TCMEN, OPAMP2_CSR_TCMEN_Msk //!< Timer-Controlled Mux mode enable 
.equ OPAMP2_CSR_VMSSEL_Pos, (8) 
.equ OPAMP2_CSR_VMSSEL_Msk, (0x1 << OPAMP2_CSR_VMSSEL_Pos) //!< 0x00000100 
.equ OPAMP2_CSR_VMSSEL, OPAMP2_CSR_VMSSEL_Msk //!< Inverting input secondary selection 
.equ OPAMP2_CSR_VPSSEL_Pos, (9) 
.equ OPAMP2_CSR_VPSSEL_Msk, (0x3 << OPAMP2_CSR_VPSSEL_Pos) //!< 0x00000600 
.equ OPAMP2_CSR_VPSSEL, OPAMP2_CSR_VPSSEL_Msk //!< Non inverting input secondary selection 
.equ OPAMP2_CSR_VPSSEL_0, (0x1 << OPAMP2_CSR_VPSSEL_Pos) //!< 0x00000200 
.equ OPAMP2_CSR_VPSSEL_1, (0x2 << OPAMP2_CSR_VPSSEL_Pos) //!< 0x00000400 
.equ OPAMP2_CSR_CALON_Pos, (11) 
.equ OPAMP2_CSR_CALON_Msk, (0x1 << OPAMP2_CSR_CALON_Pos) //!< 0x00000800 
.equ OPAMP2_CSR_CALON, OPAMP2_CSR_CALON_Msk //!< Calibration mode enable 
.equ OPAMP2_CSR_CALSEL_Pos, (12) 
.equ OPAMP2_CSR_CALSEL_Msk, (0x3 << OPAMP2_CSR_CALSEL_Pos) //!< 0x00003000 
.equ OPAMP2_CSR_CALSEL, OPAMP2_CSR_CALSEL_Msk //!< Calibration selection 
.equ OPAMP2_CSR_CALSEL_0, (0x1 << OPAMP2_CSR_CALSEL_Pos) //!< 0x00001000 
.equ OPAMP2_CSR_CALSEL_1, (0x2 << OPAMP2_CSR_CALSEL_Pos) //!< 0x00002000 
.equ OPAMP2_CSR_PGGAIN_Pos, (14) 
.equ OPAMP2_CSR_PGGAIN_Msk, (0xF << OPAMP2_CSR_PGGAIN_Pos) //!< 0x0003C000 
.equ OPAMP2_CSR_PGGAIN, OPAMP2_CSR_PGGAIN_Msk //!< Gain in PGA mode 
.equ OPAMP2_CSR_PGGAIN_0, (0x1 << OPAMP2_CSR_PGGAIN_Pos) //!< 0x00004000 
.equ OPAMP2_CSR_PGGAIN_1, (0x2 << OPAMP2_CSR_PGGAIN_Pos) //!< 0x00008000 
.equ OPAMP2_CSR_PGGAIN_2, (0x4 << OPAMP2_CSR_PGGAIN_Pos) //!< 0x00010000 
.equ OPAMP2_CSR_PGGAIN_3, (0x8 << OPAMP2_CSR_PGGAIN_Pos) //!< 0x00020000 
.equ OPAMP2_CSR_USERTRIM_Pos, (18) 
.equ OPAMP2_CSR_USERTRIM_Msk, (0x1 << OPAMP2_CSR_USERTRIM_Pos) //!< 0x00040000 
.equ OPAMP2_CSR_USERTRIM, OPAMP2_CSR_USERTRIM_Msk //!< User trimming enable 
.equ OPAMP2_CSR_TRIMOFFSETP_Pos, (19) 
.equ OPAMP2_CSR_TRIMOFFSETP_Msk, (0x1F << OPAMP2_CSR_TRIMOFFSETP_Pos) //!< 0x00F80000 
.equ OPAMP2_CSR_TRIMOFFSETP, OPAMP2_CSR_TRIMOFFSETP_Msk //!< Offset trimming value (PMOS) 
.equ OPAMP2_CSR_TRIMOFFSETN_Pos, (24) 
.equ OPAMP2_CSR_TRIMOFFSETN_Msk, (0x1F << OPAMP2_CSR_TRIMOFFSETN_Pos) //!< 0x1F000000 
.equ OPAMP2_CSR_TRIMOFFSETN, OPAMP2_CSR_TRIMOFFSETN_Msk //!< Offset trimming value (NMOS) 
.equ OPAMP2_CSR_TSTREF_Pos, (29) 
.equ OPAMP2_CSR_TSTREF_Msk, (0x1 << OPAMP2_CSR_TSTREF_Pos) //!< 0x20000000 
.equ OPAMP2_CSR_TSTREF, OPAMP2_CSR_TSTREF_Msk //!< It enables the switch to put out the internal reference 
.equ OPAMP2_CSR_OUTCAL_Pos, (30) 
.equ OPAMP2_CSR_OUTCAL_Msk, (0x1 << OPAMP2_CSR_OUTCAL_Pos) //!< 0x40000000 
.equ OPAMP2_CSR_OUTCAL, OPAMP2_CSR_OUTCAL_Msk //!< OPAMP ouput status flag 
.equ OPAMP2_CSR_LOCK_Pos, (31) 
.equ OPAMP2_CSR_LOCK_Msk, (0x1 << OPAMP2_CSR_LOCK_Pos) //!< 0x80000000 
.equ OPAMP2_CSR_LOCK, OPAMP2_CSR_LOCK_Msk //!< OPAMP lock 
//********************  Bit definition for OPAMP3_CSR register  **************
.equ OPAMP3_CSR_OPAMP3EN_Pos, (0) 
.equ OPAMP3_CSR_OPAMP3EN_Msk, (0x1 << OPAMP3_CSR_OPAMP3EN_Pos) //!< 0x00000001 
.equ OPAMP3_CSR_OPAMP3EN, OPAMP3_CSR_OPAMP3EN_Msk //!< OPAMP3 enable 
.equ OPAMP3_CSR_FORCEVP_Pos, (1) 
.equ OPAMP3_CSR_FORCEVP_Msk, (0x1 << OPAMP3_CSR_FORCEVP_Pos) //!< 0x00000002 
.equ OPAMP3_CSR_FORCEVP, OPAMP3_CSR_FORCEVP_Msk //!< Connect the internal references to the plus input of the OPAMPX 
.equ OPAMP3_CSR_VPSEL_Pos, (2) 
.equ OPAMP3_CSR_VPSEL_Msk, (0x3 << OPAMP3_CSR_VPSEL_Pos) //!< 0x0000000C 
.equ OPAMP3_CSR_VPSEL, OPAMP3_CSR_VPSEL_Msk //!< Non inverting input selection 
.equ OPAMP3_CSR_VPSEL_0, (0x1 << OPAMP3_CSR_VPSEL_Pos) //!< 0x00000004 
.equ OPAMP3_CSR_VPSEL_1, (0x2 << OPAMP3_CSR_VPSEL_Pos) //!< 0x00000008 
.equ OPAMP3_CSR_VMSEL_Pos, (5) 
.equ OPAMP3_CSR_VMSEL_Msk, (0x3 << OPAMP3_CSR_VMSEL_Pos) //!< 0x00000060 
.equ OPAMP3_CSR_VMSEL, OPAMP3_CSR_VMSEL_Msk //!< Inverting input selection 
.equ OPAMP3_CSR_VMSEL_0, (0x1 << OPAMP3_CSR_VMSEL_Pos) //!< 0x00000020 
.equ OPAMP3_CSR_VMSEL_1, (0x2 << OPAMP3_CSR_VMSEL_Pos) //!< 0x00000040 
.equ OPAMP3_CSR_TCMEN_Pos, (7) 
.equ OPAMP3_CSR_TCMEN_Msk, (0x1 << OPAMP3_CSR_TCMEN_Pos) //!< 0x00000080 
.equ OPAMP3_CSR_TCMEN, OPAMP3_CSR_TCMEN_Msk //!< Timer-Controlled Mux mode enable 
.equ OPAMP3_CSR_VMSSEL_Pos, (8) 
.equ OPAMP3_CSR_VMSSEL_Msk, (0x1 << OPAMP3_CSR_VMSSEL_Pos) //!< 0x00000100 
.equ OPAMP3_CSR_VMSSEL, OPAMP3_CSR_VMSSEL_Msk //!< Inverting input secondary selection 
.equ OPAMP3_CSR_VPSSEL_Pos, (9) 
.equ OPAMP3_CSR_VPSSEL_Msk, (0x3 << OPAMP3_CSR_VPSSEL_Pos) //!< 0x00000600 
.equ OPAMP3_CSR_VPSSEL, OPAMP3_CSR_VPSSEL_Msk //!< Non inverting input secondary selection 
.equ OPAMP3_CSR_VPSSEL_0, (0x1 << OPAMP3_CSR_VPSSEL_Pos) //!< 0x00000200 
.equ OPAMP3_CSR_VPSSEL_1, (0x2 << OPAMP3_CSR_VPSSEL_Pos) //!< 0x00000400 
.equ OPAMP3_CSR_CALON_Pos, (11) 
.equ OPAMP3_CSR_CALON_Msk, (0x1 << OPAMP3_CSR_CALON_Pos) //!< 0x00000800 
.equ OPAMP3_CSR_CALON, OPAMP3_CSR_CALON_Msk //!< Calibration mode enable 
.equ OPAMP3_CSR_CALSEL_Pos, (12) 
.equ OPAMP3_CSR_CALSEL_Msk, (0x3 << OPAMP3_CSR_CALSEL_Pos) //!< 0x00003000 
.equ OPAMP3_CSR_CALSEL, OPAMP3_CSR_CALSEL_Msk //!< Calibration selection 
.equ OPAMP3_CSR_CALSEL_0, (0x1 << OPAMP3_CSR_CALSEL_Pos) //!< 0x00001000 
.equ OPAMP3_CSR_CALSEL_1, (0x2 << OPAMP3_CSR_CALSEL_Pos) //!< 0x00002000 
.equ OPAMP3_CSR_PGGAIN_Pos, (14) 
.equ OPAMP3_CSR_PGGAIN_Msk, (0xF << OPAMP3_CSR_PGGAIN_Pos) //!< 0x0003C000 
.equ OPAMP3_CSR_PGGAIN, OPAMP3_CSR_PGGAIN_Msk //!< Gain in PGA mode 
.equ OPAMP3_CSR_PGGAIN_0, (0x1 << OPAMP3_CSR_PGGAIN_Pos) //!< 0x00004000 
.equ OPAMP3_CSR_PGGAIN_1, (0x2 << OPAMP3_CSR_PGGAIN_Pos) //!< 0x00008000 
.equ OPAMP3_CSR_PGGAIN_2, (0x4 << OPAMP3_CSR_PGGAIN_Pos) //!< 0x00010000 
.equ OPAMP3_CSR_PGGAIN_3, (0x8 << OPAMP3_CSR_PGGAIN_Pos) //!< 0x00020000 
.equ OPAMP3_CSR_USERTRIM_Pos, (18) 
.equ OPAMP3_CSR_USERTRIM_Msk, (0x1 << OPAMP3_CSR_USERTRIM_Pos) //!< 0x00040000 
.equ OPAMP3_CSR_USERTRIM, OPAMP3_CSR_USERTRIM_Msk //!< User trimming enable 
.equ OPAMP3_CSR_TRIMOFFSETP_Pos, (19) 
.equ OPAMP3_CSR_TRIMOFFSETP_Msk, (0x1F << OPAMP3_CSR_TRIMOFFSETP_Pos) //!< 0x00F80000 
.equ OPAMP3_CSR_TRIMOFFSETP, OPAMP3_CSR_TRIMOFFSETP_Msk //!< Offset trimming value (PMOS) 
.equ OPAMP3_CSR_TRIMOFFSETN_Pos, (24) 
.equ OPAMP3_CSR_TRIMOFFSETN_Msk, (0x1F << OPAMP3_CSR_TRIMOFFSETN_Pos) //!< 0x1F000000 
.equ OPAMP3_CSR_TRIMOFFSETN, OPAMP3_CSR_TRIMOFFSETN_Msk //!< Offset trimming value (NMOS) 
.equ OPAMP3_CSR_TSTREF_Pos, (29) 
.equ OPAMP3_CSR_TSTREF_Msk, (0x1 << OPAMP3_CSR_TSTREF_Pos) //!< 0x20000000 
.equ OPAMP3_CSR_TSTREF, OPAMP3_CSR_TSTREF_Msk //!< It enables the switch to put out the internal reference 
.equ OPAMP3_CSR_OUTCAL_Pos, (30) 
.equ OPAMP3_CSR_OUTCAL_Msk, (0x1 << OPAMP3_CSR_OUTCAL_Pos) //!< 0x40000000 
.equ OPAMP3_CSR_OUTCAL, OPAMP3_CSR_OUTCAL_Msk //!< OPAMP ouput status flag 
.equ OPAMP3_CSR_LOCK_Pos, (31) 
.equ OPAMP3_CSR_LOCK_Msk, (0x1 << OPAMP3_CSR_LOCK_Pos) //!< 0x80000000 
.equ OPAMP3_CSR_LOCK, OPAMP3_CSR_LOCK_Msk //!< OPAMP lock 
//********************  Bit definition for OPAMP4_CSR register  **************
.equ OPAMP4_CSR_OPAMP4EN_Pos, (0) 
.equ OPAMP4_CSR_OPAMP4EN_Msk, (0x1 << OPAMP4_CSR_OPAMP4EN_Pos) //!< 0x00000001 
.equ OPAMP4_CSR_OPAMP4EN, OPAMP4_CSR_OPAMP4EN_Msk //!< OPAMP4 enable 
.equ OPAMP4_CSR_FORCEVP_Pos, (1) 
.equ OPAMP4_CSR_FORCEVP_Msk, (0x1 << OPAMP4_CSR_FORCEVP_Pos) //!< 0x00000002 
.equ OPAMP4_CSR_FORCEVP, OPAMP4_CSR_FORCEVP_Msk //!< Connect the internal references to the plus input of the OPAMPX 
.equ OPAMP4_CSR_VPSEL_Pos, (2) 
.equ OPAMP4_CSR_VPSEL_Msk, (0x3 << OPAMP4_CSR_VPSEL_Pos) //!< 0x0000000C 
.equ OPAMP4_CSR_VPSEL, OPAMP4_CSR_VPSEL_Msk //!< Non inverting input selection 
.equ OPAMP4_CSR_VPSEL_0, (0x1 << OPAMP4_CSR_VPSEL_Pos) //!< 0x00000004 
.equ OPAMP4_CSR_VPSEL_1, (0x2 << OPAMP4_CSR_VPSEL_Pos) //!< 0x00000008 
.equ OPAMP4_CSR_VMSEL_Pos, (5) 
.equ OPAMP4_CSR_VMSEL_Msk, (0x3 << OPAMP4_CSR_VMSEL_Pos) //!< 0x00000060 
.equ OPAMP4_CSR_VMSEL, OPAMP4_CSR_VMSEL_Msk //!< Inverting input selection 
.equ OPAMP4_CSR_VMSEL_0, (0x1 << OPAMP4_CSR_VMSEL_Pos) //!< 0x00000020 
.equ OPAMP4_CSR_VMSEL_1, (0x2 << OPAMP4_CSR_VMSEL_Pos) //!< 0x00000040 
.equ OPAMP4_CSR_TCMEN_Pos, (7) 
.equ OPAMP4_CSR_TCMEN_Msk, (0x1 << OPAMP4_CSR_TCMEN_Pos) //!< 0x00000080 
.equ OPAMP4_CSR_TCMEN, OPAMP4_CSR_TCMEN_Msk //!< Timer-Controlled Mux mode enable 
.equ OPAMP4_CSR_VMSSEL_Pos, (8) 
.equ OPAMP4_CSR_VMSSEL_Msk, (0x1 << OPAMP4_CSR_VMSSEL_Pos) //!< 0x00000100 
.equ OPAMP4_CSR_VMSSEL, OPAMP4_CSR_VMSSEL_Msk //!< Inverting input secondary selection 
.equ OPAMP4_CSR_VPSSEL_Pos, (9) 
.equ OPAMP4_CSR_VPSSEL_Msk, (0x3 << OPAMP4_CSR_VPSSEL_Pos) //!< 0x00000600 
.equ OPAMP4_CSR_VPSSEL, OPAMP4_CSR_VPSSEL_Msk //!< Non inverting input secondary selection 
.equ OPAMP4_CSR_VPSSEL_0, (0x1 << OPAMP4_CSR_VPSSEL_Pos) //!< 0x00000200 
.equ OPAMP4_CSR_VPSSEL_1, (0x2 << OPAMP4_CSR_VPSSEL_Pos) //!< 0x00000400 
.equ OPAMP4_CSR_CALON_Pos, (11) 
.equ OPAMP4_CSR_CALON_Msk, (0x1 << OPAMP4_CSR_CALON_Pos) //!< 0x00000800 
.equ OPAMP4_CSR_CALON, OPAMP4_CSR_CALON_Msk //!< Calibration mode enable 
.equ OPAMP4_CSR_CALSEL_Pos, (12) 
.equ OPAMP4_CSR_CALSEL_Msk, (0x3 << OPAMP4_CSR_CALSEL_Pos) //!< 0x00003000 
.equ OPAMP4_CSR_CALSEL, OPAMP4_CSR_CALSEL_Msk //!< Calibration selection 
.equ OPAMP4_CSR_CALSEL_0, (0x1 << OPAMP4_CSR_CALSEL_Pos) //!< 0x00001000 
.equ OPAMP4_CSR_CALSEL_1, (0x2 << OPAMP4_CSR_CALSEL_Pos) //!< 0x00002000 
.equ OPAMP4_CSR_PGGAIN_Pos, (14) 
.equ OPAMP4_CSR_PGGAIN_Msk, (0xF << OPAMP4_CSR_PGGAIN_Pos) //!< 0x0003C000 
.equ OPAMP4_CSR_PGGAIN, OPAMP4_CSR_PGGAIN_Msk //!< Gain in PGA mode 
.equ OPAMP4_CSR_PGGAIN_0, (0x1 << OPAMP4_CSR_PGGAIN_Pos) //!< 0x00004000 
.equ OPAMP4_CSR_PGGAIN_1, (0x2 << OPAMP4_CSR_PGGAIN_Pos) //!< 0x00008000 
.equ OPAMP4_CSR_PGGAIN_2, (0x4 << OPAMP4_CSR_PGGAIN_Pos) //!< 0x00010000 
.equ OPAMP4_CSR_PGGAIN_3, (0x8 << OPAMP4_CSR_PGGAIN_Pos) //!< 0x00020000 
.equ OPAMP4_CSR_USERTRIM_Pos, (18) 
.equ OPAMP4_CSR_USERTRIM_Msk, (0x1 << OPAMP4_CSR_USERTRIM_Pos) //!< 0x00040000 
.equ OPAMP4_CSR_USERTRIM, OPAMP4_CSR_USERTRIM_Msk //!< User trimming enable 
.equ OPAMP4_CSR_TRIMOFFSETP_Pos, (19) 
.equ OPAMP4_CSR_TRIMOFFSETP_Msk, (0x1F << OPAMP4_CSR_TRIMOFFSETP_Pos) //!< 0x00F80000 
.equ OPAMP4_CSR_TRIMOFFSETP, OPAMP4_CSR_TRIMOFFSETP_Msk //!< Offset trimming value (PMOS) 
.equ OPAMP4_CSR_TRIMOFFSETN_Pos, (24) 
.equ OPAMP4_CSR_TRIMOFFSETN_Msk, (0x1F << OPAMP4_CSR_TRIMOFFSETN_Pos) //!< 0x1F000000 
.equ OPAMP4_CSR_TRIMOFFSETN, OPAMP4_CSR_TRIMOFFSETN_Msk //!< Offset trimming value (NMOS) 
.equ OPAMP4_CSR_TSTREF_Pos, (29) 
.equ OPAMP4_CSR_TSTREF_Msk, (0x1 << OPAMP4_CSR_TSTREF_Pos) //!< 0x20000000 
.equ OPAMP4_CSR_TSTREF, OPAMP4_CSR_TSTREF_Msk //!< It enables the switch to put out the internal reference 
.equ OPAMP4_CSR_OUTCAL_Pos, (30) 
.equ OPAMP4_CSR_OUTCAL_Msk, (0x1 << OPAMP4_CSR_OUTCAL_Pos) //!< 0x40000000 
.equ OPAMP4_CSR_OUTCAL, OPAMP4_CSR_OUTCAL_Msk //!< OPAMP ouput status flag 
.equ OPAMP4_CSR_LOCK_Pos, (31) 
.equ OPAMP4_CSR_LOCK_Msk, (0x1 << OPAMP4_CSR_LOCK_Pos) //!< 0x80000000 
.equ OPAMP4_CSR_LOCK, OPAMP4_CSR_LOCK_Msk //!< OPAMP lock 
//********************  Bit definition for OPAMPx_CSR register  **************
.equ OPAMP_CSR_OPAMPxEN_Pos, (0) 
.equ OPAMP_CSR_OPAMPxEN_Msk, (0x1 << OPAMP_CSR_OPAMPxEN_Pos) //!< 0x00000001 
.equ OPAMP_CSR_OPAMPxEN, OPAMP_CSR_OPAMPxEN_Msk //!< OPAMP enable 
.equ OPAMP_CSR_FORCEVP_Pos, (1) 
.equ OPAMP_CSR_FORCEVP_Msk, (0x1 << OPAMP_CSR_FORCEVP_Pos) //!< 0x00000002 
.equ OPAMP_CSR_FORCEVP, OPAMP_CSR_FORCEVP_Msk //!< Connect the internal references to the plus input of the OPAMPX 
.equ OPAMP_CSR_VPSEL_Pos, (2) 
.equ OPAMP_CSR_VPSEL_Msk, (0x3 << OPAMP_CSR_VPSEL_Pos) //!< 0x0000000C 
.equ OPAMP_CSR_VPSEL, OPAMP_CSR_VPSEL_Msk //!< Non inverting input selection 
.equ OPAMP_CSR_VPSEL_0, (0x1 << OPAMP_CSR_VPSEL_Pos) //!< 0x00000004 
.equ OPAMP_CSR_VPSEL_1, (0x2 << OPAMP_CSR_VPSEL_Pos) //!< 0x00000008 
.equ OPAMP_CSR_VMSEL_Pos, (5) 
.equ OPAMP_CSR_VMSEL_Msk, (0x3 << OPAMP_CSR_VMSEL_Pos) //!< 0x00000060 
.equ OPAMP_CSR_VMSEL, OPAMP_CSR_VMSEL_Msk //!< Inverting input selection 
.equ OPAMP_CSR_VMSEL_0, (0x1 << OPAMP_CSR_VMSEL_Pos) //!< 0x00000020 
.equ OPAMP_CSR_VMSEL_1, (0x2 << OPAMP_CSR_VMSEL_Pos) //!< 0x00000040 
.equ OPAMP_CSR_TCMEN_Pos, (7) 
.equ OPAMP_CSR_TCMEN_Msk, (0x1 << OPAMP_CSR_TCMEN_Pos) //!< 0x00000080 
.equ OPAMP_CSR_TCMEN, OPAMP_CSR_TCMEN_Msk //!< Timer-Controlled Mux mode enable 
.equ OPAMP_CSR_VMSSEL_Pos, (8) 
.equ OPAMP_CSR_VMSSEL_Msk, (0x1 << OPAMP_CSR_VMSSEL_Pos) //!< 0x00000100 
.equ OPAMP_CSR_VMSSEL, OPAMP_CSR_VMSSEL_Msk //!< Inverting input secondary selection 
.equ OPAMP_CSR_VPSSEL_Pos, (9) 
.equ OPAMP_CSR_VPSSEL_Msk, (0x3 << OPAMP_CSR_VPSSEL_Pos) //!< 0x00000600 
.equ OPAMP_CSR_VPSSEL, OPAMP_CSR_VPSSEL_Msk //!< Non inverting input secondary selection 
.equ OPAMP_CSR_VPSSEL_0, (0x1 << OPAMP_CSR_VPSSEL_Pos) //!< 0x00000200 
.equ OPAMP_CSR_VPSSEL_1, (0x2 << OPAMP_CSR_VPSSEL_Pos) //!< 0x00000400 
.equ OPAMP_CSR_CALON_Pos, (11) 
.equ OPAMP_CSR_CALON_Msk, (0x1 << OPAMP_CSR_CALON_Pos) //!< 0x00000800 
.equ OPAMP_CSR_CALON, OPAMP_CSR_CALON_Msk //!< Calibration mode enable 
.equ OPAMP_CSR_CALSEL_Pos, (12) 
.equ OPAMP_CSR_CALSEL_Msk, (0x3 << OPAMP_CSR_CALSEL_Pos) //!< 0x00003000 
.equ OPAMP_CSR_CALSEL, OPAMP_CSR_CALSEL_Msk //!< Calibration selection 
.equ OPAMP_CSR_CALSEL_0, (0x1 << OPAMP_CSR_CALSEL_Pos) //!< 0x00001000 
.equ OPAMP_CSR_CALSEL_1, (0x2 << OPAMP_CSR_CALSEL_Pos) //!< 0x00002000 
.equ OPAMP_CSR_PGGAIN_Pos, (14) 
.equ OPAMP_CSR_PGGAIN_Msk, (0xF << OPAMP_CSR_PGGAIN_Pos) //!< 0x0003C000 
.equ OPAMP_CSR_PGGAIN, OPAMP_CSR_PGGAIN_Msk //!< Gain in PGA mode 
.equ OPAMP_CSR_PGGAIN_0, (0x1 << OPAMP_CSR_PGGAIN_Pos) //!< 0x00004000 
.equ OPAMP_CSR_PGGAIN_1, (0x2 << OPAMP_CSR_PGGAIN_Pos) //!< 0x00008000 
.equ OPAMP_CSR_PGGAIN_2, (0x4 << OPAMP_CSR_PGGAIN_Pos) //!< 0x00010000 
.equ OPAMP_CSR_PGGAIN_3, (0x8 << OPAMP_CSR_PGGAIN_Pos) //!< 0x00020000 
.equ OPAMP_CSR_USERTRIM_Pos, (18) 
.equ OPAMP_CSR_USERTRIM_Msk, (0x1 << OPAMP_CSR_USERTRIM_Pos) //!< 0x00040000 
.equ OPAMP_CSR_USERTRIM, OPAMP_CSR_USERTRIM_Msk //!< User trimming enable 
.equ OPAMP_CSR_TRIMOFFSETP_Pos, (19) 
.equ OPAMP_CSR_TRIMOFFSETP_Msk, (0x1F << OPAMP_CSR_TRIMOFFSETP_Pos) //!< 0x00F80000 
.equ OPAMP_CSR_TRIMOFFSETP, OPAMP_CSR_TRIMOFFSETP_Msk //!< Offset trimming value (PMOS) 
.equ OPAMP_CSR_TRIMOFFSETN_Pos, (24) 
.equ OPAMP_CSR_TRIMOFFSETN_Msk, (0x1F << OPAMP_CSR_TRIMOFFSETN_Pos) //!< 0x1F000000 
.equ OPAMP_CSR_TRIMOFFSETN, OPAMP_CSR_TRIMOFFSETN_Msk //!< Offset trimming value (NMOS) 
.equ OPAMP_CSR_TSTREF_Pos, (29) 
.equ OPAMP_CSR_TSTREF_Msk, (0x1 << OPAMP_CSR_TSTREF_Pos) //!< 0x20000000 
.equ OPAMP_CSR_TSTREF, OPAMP_CSR_TSTREF_Msk //!< It enables the switch to put out the internal reference 
.equ OPAMP_CSR_OUTCAL_Pos, (30) 
.equ OPAMP_CSR_OUTCAL_Msk, (0x1 << OPAMP_CSR_OUTCAL_Pos) //!< 0x40000000 
.equ OPAMP_CSR_OUTCAL, OPAMP_CSR_OUTCAL_Msk //!< OPAMP ouput status flag 
.equ OPAMP_CSR_LOCK_Pos, (31) 
.equ OPAMP_CSR_LOCK_Msk, (0x1 << OPAMP_CSR_LOCK_Pos) //!< 0x80000000 
.equ OPAMP_CSR_LOCK, OPAMP_CSR_LOCK_Msk //!< OPAMP lock 
//****************************************************************************
//
//                   Controller Area Network (CAN )
//
//****************************************************************************
//******************  Bit definition for CAN_MCR register  *******************
.equ CAN_MCR_INRQ_Pos, (0) 
.equ CAN_MCR_INRQ_Msk, (0x1 << CAN_MCR_INRQ_Pos) //!< 0x00000001 
.equ CAN_MCR_INRQ, CAN_MCR_INRQ_Msk //!<Initialization Request 
.equ CAN_MCR_SLEEP_Pos, (1) 
.equ CAN_MCR_SLEEP_Msk, (0x1 << CAN_MCR_SLEEP_Pos) //!< 0x00000002 
.equ CAN_MCR_SLEEP, CAN_MCR_SLEEP_Msk //!<Sleep Mode Request 
.equ CAN_MCR_TXFP_Pos, (2) 
.equ CAN_MCR_TXFP_Msk, (0x1 << CAN_MCR_TXFP_Pos) //!< 0x00000004 
.equ CAN_MCR_TXFP, CAN_MCR_TXFP_Msk //!<Transmit FIFO Priority 
.equ CAN_MCR_RFLM_Pos, (3) 
.equ CAN_MCR_RFLM_Msk, (0x1 << CAN_MCR_RFLM_Pos) //!< 0x00000008 
.equ CAN_MCR_RFLM, CAN_MCR_RFLM_Msk //!<Receive FIFO Locked Mode 
.equ CAN_MCR_NART_Pos, (4) 
.equ CAN_MCR_NART_Msk, (0x1 << CAN_MCR_NART_Pos) //!< 0x00000010 
.equ CAN_MCR_NART, CAN_MCR_NART_Msk //!<No Automatic Retransmission 
.equ CAN_MCR_AWUM_Pos, (5) 
.equ CAN_MCR_AWUM_Msk, (0x1 << CAN_MCR_AWUM_Pos) //!< 0x00000020 
.equ CAN_MCR_AWUM, CAN_MCR_AWUM_Msk //!<Automatic Wakeup Mode 
.equ CAN_MCR_ABOM_Pos, (6) 
.equ CAN_MCR_ABOM_Msk, (0x1 << CAN_MCR_ABOM_Pos) //!< 0x00000040 
.equ CAN_MCR_ABOM, CAN_MCR_ABOM_Msk //!<Automatic Bus-Off Management 
.equ CAN_MCR_TTCM_Pos, (7) 
.equ CAN_MCR_TTCM_Msk, (0x1 << CAN_MCR_TTCM_Pos) //!< 0x00000080 
.equ CAN_MCR_TTCM, CAN_MCR_TTCM_Msk //!<Time Triggered Communication Mode 
.equ CAN_MCR_RESET_Pos, (15) 
.equ CAN_MCR_RESET_Msk, (0x1 << CAN_MCR_RESET_Pos) //!< 0x00008000 
.equ CAN_MCR_RESET, CAN_MCR_RESET_Msk //!<bxCAN software master reset 
//******************  Bit definition for CAN_MSR register  *******************
.equ CAN_MSR_INAK_Pos, (0) 
.equ CAN_MSR_INAK_Msk, (0x1 << CAN_MSR_INAK_Pos) //!< 0x00000001 
.equ CAN_MSR_INAK, CAN_MSR_INAK_Msk //!<Initialization Acknowledge 
.equ CAN_MSR_SLAK_Pos, (1) 
.equ CAN_MSR_SLAK_Msk, (0x1 << CAN_MSR_SLAK_Pos) //!< 0x00000002 
.equ CAN_MSR_SLAK, CAN_MSR_SLAK_Msk //!<Sleep Acknowledge 
.equ CAN_MSR_ERRI_Pos, (2) 
.equ CAN_MSR_ERRI_Msk, (0x1 << CAN_MSR_ERRI_Pos) //!< 0x00000004 
.equ CAN_MSR_ERRI, CAN_MSR_ERRI_Msk //!<Error Interrupt 
.equ CAN_MSR_WKUI_Pos, (3) 
.equ CAN_MSR_WKUI_Msk, (0x1 << CAN_MSR_WKUI_Pos) //!< 0x00000008 
.equ CAN_MSR_WKUI, CAN_MSR_WKUI_Msk //!<Wakeup Interrupt 
.equ CAN_MSR_SLAKI_Pos, (4) 
.equ CAN_MSR_SLAKI_Msk, (0x1 << CAN_MSR_SLAKI_Pos) //!< 0x00000010 
.equ CAN_MSR_SLAKI, CAN_MSR_SLAKI_Msk //!<Sleep Acknowledge Interrupt 
.equ CAN_MSR_TXM_Pos, (8) 
.equ CAN_MSR_TXM_Msk, (0x1 << CAN_MSR_TXM_Pos) //!< 0x00000100 
.equ CAN_MSR_TXM, CAN_MSR_TXM_Msk //!<Transmit Mode 
.equ CAN_MSR_RXM_Pos, (9) 
.equ CAN_MSR_RXM_Msk, (0x1 << CAN_MSR_RXM_Pos) //!< 0x00000200 
.equ CAN_MSR_RXM, CAN_MSR_RXM_Msk //!<Receive Mode 
.equ CAN_MSR_SAMP_Pos, (10) 
.equ CAN_MSR_SAMP_Msk, (0x1 << CAN_MSR_SAMP_Pos) //!< 0x00000400 
.equ CAN_MSR_SAMP, CAN_MSR_SAMP_Msk //!<Last Sample Point 
.equ CAN_MSR_RX_Pos, (11) 
.equ CAN_MSR_RX_Msk, (0x1 << CAN_MSR_RX_Pos) //!< 0x00000800 
.equ CAN_MSR_RX, CAN_MSR_RX_Msk //!<CAN Rx Signal 
//******************  Bit definition for CAN_TSR register  *******************
.equ CAN_TSR_RQCP0_Pos, (0) 
.equ CAN_TSR_RQCP0_Msk, (0x1 << CAN_TSR_RQCP0_Pos) //!< 0x00000001 
.equ CAN_TSR_RQCP0, CAN_TSR_RQCP0_Msk //!<Request Completed Mailbox0 
.equ CAN_TSR_TXOK0_Pos, (1) 
.equ CAN_TSR_TXOK0_Msk, (0x1 << CAN_TSR_TXOK0_Pos) //!< 0x00000002 
.equ CAN_TSR_TXOK0, CAN_TSR_TXOK0_Msk //!<Transmission OK of Mailbox0 
.equ CAN_TSR_ALST0_Pos, (2) 
.equ CAN_TSR_ALST0_Msk, (0x1 << CAN_TSR_ALST0_Pos) //!< 0x00000004 
.equ CAN_TSR_ALST0, CAN_TSR_ALST0_Msk //!<Arbitration Lost for Mailbox0 
.equ CAN_TSR_TERR0_Pos, (3) 
.equ CAN_TSR_TERR0_Msk, (0x1 << CAN_TSR_TERR0_Pos) //!< 0x00000008 
.equ CAN_TSR_TERR0, CAN_TSR_TERR0_Msk //!<Transmission Error of Mailbox0 
.equ CAN_TSR_ABRQ0_Pos, (7) 
.equ CAN_TSR_ABRQ0_Msk, (0x1 << CAN_TSR_ABRQ0_Pos) //!< 0x00000080 
.equ CAN_TSR_ABRQ0, CAN_TSR_ABRQ0_Msk //!<Abort Request for Mailbox0 
.equ CAN_TSR_RQCP1_Pos, (8) 
.equ CAN_TSR_RQCP1_Msk, (0x1 << CAN_TSR_RQCP1_Pos) //!< 0x00000100 
.equ CAN_TSR_RQCP1, CAN_TSR_RQCP1_Msk //!<Request Completed Mailbox1 
.equ CAN_TSR_TXOK1_Pos, (9) 
.equ CAN_TSR_TXOK1_Msk, (0x1 << CAN_TSR_TXOK1_Pos) //!< 0x00000200 
.equ CAN_TSR_TXOK1, CAN_TSR_TXOK1_Msk //!<Transmission OK of Mailbox1 
.equ CAN_TSR_ALST1_Pos, (10) 
.equ CAN_TSR_ALST1_Msk, (0x1 << CAN_TSR_ALST1_Pos) //!< 0x00000400 
.equ CAN_TSR_ALST1, CAN_TSR_ALST1_Msk //!<Arbitration Lost for Mailbox1 
.equ CAN_TSR_TERR1_Pos, (11) 
.equ CAN_TSR_TERR1_Msk, (0x1 << CAN_TSR_TERR1_Pos) //!< 0x00000800 
.equ CAN_TSR_TERR1, CAN_TSR_TERR1_Msk //!<Transmission Error of Mailbox1 
.equ CAN_TSR_ABRQ1_Pos, (15) 
.equ CAN_TSR_ABRQ1_Msk, (0x1 << CAN_TSR_ABRQ1_Pos) //!< 0x00008000 
.equ CAN_TSR_ABRQ1, CAN_TSR_ABRQ1_Msk //!<Abort Request for Mailbox 1 
.equ CAN_TSR_RQCP2_Pos, (16) 
.equ CAN_TSR_RQCP2_Msk, (0x1 << CAN_TSR_RQCP2_Pos) //!< 0x00010000 
.equ CAN_TSR_RQCP2, CAN_TSR_RQCP2_Msk //!<Request Completed Mailbox2 
.equ CAN_TSR_TXOK2_Pos, (17) 
.equ CAN_TSR_TXOK2_Msk, (0x1 << CAN_TSR_TXOK2_Pos) //!< 0x00020000 
.equ CAN_TSR_TXOK2, CAN_TSR_TXOK2_Msk //!<Transmission OK of Mailbox 2 
.equ CAN_TSR_ALST2_Pos, (18) 
.equ CAN_TSR_ALST2_Msk, (0x1 << CAN_TSR_ALST2_Pos) //!< 0x00040000 
.equ CAN_TSR_ALST2, CAN_TSR_ALST2_Msk //!<Arbitration Lost for mailbox 2 
.equ CAN_TSR_TERR2_Pos, (19) 
.equ CAN_TSR_TERR2_Msk, (0x1 << CAN_TSR_TERR2_Pos) //!< 0x00080000 
.equ CAN_TSR_TERR2, CAN_TSR_TERR2_Msk //!<Transmission Error of Mailbox 2 
.equ CAN_TSR_ABRQ2_Pos, (23) 
.equ CAN_TSR_ABRQ2_Msk, (0x1 << CAN_TSR_ABRQ2_Pos) //!< 0x00800000 
.equ CAN_TSR_ABRQ2, CAN_TSR_ABRQ2_Msk //!<Abort Request for Mailbox 2 
.equ CAN_TSR_CODE_Pos, (24) 
.equ CAN_TSR_CODE_Msk, (0x3 << CAN_TSR_CODE_Pos) //!< 0x03000000 
.equ CAN_TSR_CODE, CAN_TSR_CODE_Msk //!<Mailbox Code 
.equ CAN_TSR_TME_Pos, (26) 
.equ CAN_TSR_TME_Msk, (0x7 << CAN_TSR_TME_Pos) //!< 0x1C000000 
.equ CAN_TSR_TME, CAN_TSR_TME_Msk //!<TME[2:0] bits 
.equ CAN_TSR_TME0_Pos, (26) 
.equ CAN_TSR_TME0_Msk, (0x1 << CAN_TSR_TME0_Pos) //!< 0x04000000 
.equ CAN_TSR_TME0, CAN_TSR_TME0_Msk //!<Transmit Mailbox 0 Empty 
.equ CAN_TSR_TME1_Pos, (27) 
.equ CAN_TSR_TME1_Msk, (0x1 << CAN_TSR_TME1_Pos) //!< 0x08000000 
.equ CAN_TSR_TME1, CAN_TSR_TME1_Msk //!<Transmit Mailbox 1 Empty 
.equ CAN_TSR_TME2_Pos, (28) 
.equ CAN_TSR_TME2_Msk, (0x1 << CAN_TSR_TME2_Pos) //!< 0x10000000 
.equ CAN_TSR_TME2, CAN_TSR_TME2_Msk //!<Transmit Mailbox 2 Empty 
.equ CAN_TSR_LOW_Pos, (29) 
.equ CAN_TSR_LOW_Msk, (0x7 << CAN_TSR_LOW_Pos) //!< 0xE0000000 
.equ CAN_TSR_LOW, CAN_TSR_LOW_Msk //!<LOW[2:0] bits 
.equ CAN_TSR_LOW0_Pos, (29) 
.equ CAN_TSR_LOW0_Msk, (0x1 << CAN_TSR_LOW0_Pos) //!< 0x20000000 
.equ CAN_TSR_LOW0, CAN_TSR_LOW0_Msk //!<Lowest Priority Flag for Mailbox 0 
.equ CAN_TSR_LOW1_Pos, (30) 
.equ CAN_TSR_LOW1_Msk, (0x1 << CAN_TSR_LOW1_Pos) //!< 0x40000000 
.equ CAN_TSR_LOW1, CAN_TSR_LOW1_Msk //!<Lowest Priority Flag for Mailbox 1 
.equ CAN_TSR_LOW2_Pos, (31) 
.equ CAN_TSR_LOW2_Msk, (0x1 << CAN_TSR_LOW2_Pos) //!< 0x80000000 
.equ CAN_TSR_LOW2, CAN_TSR_LOW2_Msk //!<Lowest Priority Flag for Mailbox 2 
//******************  Bit definition for CAN_RF0R register  ******************
.equ CAN_RF0R_FMP0_Pos, (0) 
.equ CAN_RF0R_FMP0_Msk, (0x3 << CAN_RF0R_FMP0_Pos) //!< 0x00000003 
.equ CAN_RF0R_FMP0, CAN_RF0R_FMP0_Msk //!<FIFO 0 Message Pending 
.equ CAN_RF0R_FULL0_Pos, (3) 
.equ CAN_RF0R_FULL0_Msk, (0x1 << CAN_RF0R_FULL0_Pos) //!< 0x00000008 
.equ CAN_RF0R_FULL0, CAN_RF0R_FULL0_Msk //!<FIFO 0 Full 
.equ CAN_RF0R_FOVR0_Pos, (4) 
.equ CAN_RF0R_FOVR0_Msk, (0x1 << CAN_RF0R_FOVR0_Pos) //!< 0x00000010 
.equ CAN_RF0R_FOVR0, CAN_RF0R_FOVR0_Msk //!<FIFO 0 Overrun 
.equ CAN_RF0R_RFOM0_Pos, (5) 
.equ CAN_RF0R_RFOM0_Msk, (0x1 << CAN_RF0R_RFOM0_Pos) //!< 0x00000020 
.equ CAN_RF0R_RFOM0, CAN_RF0R_RFOM0_Msk //!<Release FIFO 0 Output Mailbox 
//******************  Bit definition for CAN_RF1R register  ******************
.equ CAN_RF1R_FMP1_Pos, (0) 
.equ CAN_RF1R_FMP1_Msk, (0x3 << CAN_RF1R_FMP1_Pos) //!< 0x00000003 
.equ CAN_RF1R_FMP1, CAN_RF1R_FMP1_Msk //!<FIFO 1 Message Pending 
.equ CAN_RF1R_FULL1_Pos, (3) 
.equ CAN_RF1R_FULL1_Msk, (0x1 << CAN_RF1R_FULL1_Pos) //!< 0x00000008 
.equ CAN_RF1R_FULL1, CAN_RF1R_FULL1_Msk //!<FIFO 1 Full 
.equ CAN_RF1R_FOVR1_Pos, (4) 
.equ CAN_RF1R_FOVR1_Msk, (0x1 << CAN_RF1R_FOVR1_Pos) //!< 0x00000010 
.equ CAN_RF1R_FOVR1, CAN_RF1R_FOVR1_Msk //!<FIFO 1 Overrun 
.equ CAN_RF1R_RFOM1_Pos, (5) 
.equ CAN_RF1R_RFOM1_Msk, (0x1 << CAN_RF1R_RFOM1_Pos) //!< 0x00000020 
.equ CAN_RF1R_RFOM1, CAN_RF1R_RFOM1_Msk //!<Release FIFO 1 Output Mailbox 
//*******************  Bit definition for CAN_IER register  ******************
.equ CAN_IER_TMEIE_Pos, (0) 
.equ CAN_IER_TMEIE_Msk, (0x1 << CAN_IER_TMEIE_Pos) //!< 0x00000001 
.equ CAN_IER_TMEIE, CAN_IER_TMEIE_Msk //!<Transmit Mailbox Empty Interrupt Enable 
.equ CAN_IER_FMPIE0_Pos, (1) 
.equ CAN_IER_FMPIE0_Msk, (0x1 << CAN_IER_FMPIE0_Pos) //!< 0x00000002 
.equ CAN_IER_FMPIE0, CAN_IER_FMPIE0_Msk //!<FIFO Message Pending Interrupt Enable 
.equ CAN_IER_FFIE0_Pos, (2) 
.equ CAN_IER_FFIE0_Msk, (0x1 << CAN_IER_FFIE0_Pos) //!< 0x00000004 
.equ CAN_IER_FFIE0, CAN_IER_FFIE0_Msk //!<FIFO Full Interrupt Enable 
.equ CAN_IER_FOVIE0_Pos, (3) 
.equ CAN_IER_FOVIE0_Msk, (0x1 << CAN_IER_FOVIE0_Pos) //!< 0x00000008 
.equ CAN_IER_FOVIE0, CAN_IER_FOVIE0_Msk //!<FIFO Overrun Interrupt Enable 
.equ CAN_IER_FMPIE1_Pos, (4) 
.equ CAN_IER_FMPIE1_Msk, (0x1 << CAN_IER_FMPIE1_Pos) //!< 0x00000010 
.equ CAN_IER_FMPIE1, CAN_IER_FMPIE1_Msk //!<FIFO Message Pending Interrupt Enable 
.equ CAN_IER_FFIE1_Pos, (5) 
.equ CAN_IER_FFIE1_Msk, (0x1 << CAN_IER_FFIE1_Pos) //!< 0x00000020 
.equ CAN_IER_FFIE1, CAN_IER_FFIE1_Msk //!<FIFO Full Interrupt Enable 
.equ CAN_IER_FOVIE1_Pos, (6) 
.equ CAN_IER_FOVIE1_Msk, (0x1 << CAN_IER_FOVIE1_Pos) //!< 0x00000040 
.equ CAN_IER_FOVIE1, CAN_IER_FOVIE1_Msk //!<FIFO Overrun Interrupt Enable 
.equ CAN_IER_EWGIE_Pos, (8) 
.equ CAN_IER_EWGIE_Msk, (0x1 << CAN_IER_EWGIE_Pos) //!< 0x00000100 
.equ CAN_IER_EWGIE, CAN_IER_EWGIE_Msk //!<Error Warning Interrupt Enable 
.equ CAN_IER_EPVIE_Pos, (9) 
.equ CAN_IER_EPVIE_Msk, (0x1 << CAN_IER_EPVIE_Pos) //!< 0x00000200 
.equ CAN_IER_EPVIE, CAN_IER_EPVIE_Msk //!<Error Passive Interrupt Enable 
.equ CAN_IER_BOFIE_Pos, (10) 
.equ CAN_IER_BOFIE_Msk, (0x1 << CAN_IER_BOFIE_Pos) //!< 0x00000400 
.equ CAN_IER_BOFIE, CAN_IER_BOFIE_Msk //!<Bus-Off Interrupt Enable 
.equ CAN_IER_LECIE_Pos, (11) 
.equ CAN_IER_LECIE_Msk, (0x1 << CAN_IER_LECIE_Pos) //!< 0x00000800 
.equ CAN_IER_LECIE, CAN_IER_LECIE_Msk //!<Last Error Code Interrupt Enable 
.equ CAN_IER_ERRIE_Pos, (15) 
.equ CAN_IER_ERRIE_Msk, (0x1 << CAN_IER_ERRIE_Pos) //!< 0x00008000 
.equ CAN_IER_ERRIE, CAN_IER_ERRIE_Msk //!<Error Interrupt Enable 
.equ CAN_IER_WKUIE_Pos, (16) 
.equ CAN_IER_WKUIE_Msk, (0x1 << CAN_IER_WKUIE_Pos) //!< 0x00010000 
.equ CAN_IER_WKUIE, CAN_IER_WKUIE_Msk //!<Wakeup Interrupt Enable 
.equ CAN_IER_SLKIE_Pos, (17) 
.equ CAN_IER_SLKIE_Msk, (0x1 << CAN_IER_SLKIE_Pos) //!< 0x00020000 
.equ CAN_IER_SLKIE, CAN_IER_SLKIE_Msk //!<Sleep Interrupt Enable 
//*******************  Bit definition for CAN_ESR register  ******************
.equ CAN_ESR_EWGF_Pos, (0) 
.equ CAN_ESR_EWGF_Msk, (0x1 << CAN_ESR_EWGF_Pos) //!< 0x00000001 
.equ CAN_ESR_EWGF, CAN_ESR_EWGF_Msk //!<Error Warning Flag 
.equ CAN_ESR_EPVF_Pos, (1) 
.equ CAN_ESR_EPVF_Msk, (0x1 << CAN_ESR_EPVF_Pos) //!< 0x00000002 
.equ CAN_ESR_EPVF, CAN_ESR_EPVF_Msk //!<Error Passive Flag 
.equ CAN_ESR_BOFF_Pos, (2) 
.equ CAN_ESR_BOFF_Msk, (0x1 << CAN_ESR_BOFF_Pos) //!< 0x00000004 
.equ CAN_ESR_BOFF, CAN_ESR_BOFF_Msk //!<Bus-Off Flag 
.equ CAN_ESR_LEC_Pos, (4) 
.equ CAN_ESR_LEC_Msk, (0x7 << CAN_ESR_LEC_Pos) //!< 0x00000070 
.equ CAN_ESR_LEC, CAN_ESR_LEC_Msk //!<LEC[2:0] bits (Last Error Code) 
.equ CAN_ESR_LEC_0, (0x1 << CAN_ESR_LEC_Pos) //!< 0x00000010 
.equ CAN_ESR_LEC_1, (0x2 << CAN_ESR_LEC_Pos) //!< 0x00000020 
.equ CAN_ESR_LEC_2, (0x4 << CAN_ESR_LEC_Pos) //!< 0x00000040 
.equ CAN_ESR_TEC_Pos, (16) 
.equ CAN_ESR_TEC_Msk, (0xFF << CAN_ESR_TEC_Pos) //!< 0x00FF0000 
.equ CAN_ESR_TEC, CAN_ESR_TEC_Msk //!<Least significant byte of the 9-bit Transmit Error Counter 
.equ CAN_ESR_REC_Pos, (24) 
.equ CAN_ESR_REC_Msk, (0xFF << CAN_ESR_REC_Pos) //!< 0xFF000000 
.equ CAN_ESR_REC, CAN_ESR_REC_Msk //!<Receive Error Counter 
//******************  Bit definition for CAN_BTR register  *******************
.equ CAN_BTR_BRP_Pos, (0) 
.equ CAN_BTR_BRP_Msk, (0x3FF << CAN_BTR_BRP_Pos) //!< 0x000003FF 
.equ CAN_BTR_BRP, CAN_BTR_BRP_Msk //!<Baud Rate Prescaler 
.equ CAN_BTR_TS1_Pos, (16) 
.equ CAN_BTR_TS1_Msk, (0xF << CAN_BTR_TS1_Pos) //!< 0x000F0000 
.equ CAN_BTR_TS1, CAN_BTR_TS1_Msk //!<Time Segment 1 
.equ CAN_BTR_TS1_0, (0x1 << CAN_BTR_TS1_Pos) //!< 0x00010000 
.equ CAN_BTR_TS1_1, (0x2 << CAN_BTR_TS1_Pos) //!< 0x00020000 
.equ CAN_BTR_TS1_2, (0x4 << CAN_BTR_TS1_Pos) //!< 0x00040000 
.equ CAN_BTR_TS1_3, (0x8 << CAN_BTR_TS1_Pos) //!< 0x00080000 
.equ CAN_BTR_TS2_Pos, (20) 
.equ CAN_BTR_TS2_Msk, (0x7 << CAN_BTR_TS2_Pos) //!< 0x00700000 
.equ CAN_BTR_TS2, CAN_BTR_TS2_Msk //!<Time Segment 2 
.equ CAN_BTR_TS2_0, (0x1 << CAN_BTR_TS2_Pos) //!< 0x00100000 
.equ CAN_BTR_TS2_1, (0x2 << CAN_BTR_TS2_Pos) //!< 0x00200000 
.equ CAN_BTR_TS2_2, (0x4 << CAN_BTR_TS2_Pos) //!< 0x00400000 
.equ CAN_BTR_SJW_Pos, (24) 
.equ CAN_BTR_SJW_Msk, (0x3 << CAN_BTR_SJW_Pos) //!< 0x03000000 
.equ CAN_BTR_SJW, CAN_BTR_SJW_Msk //!<Resynchronization Jump Width 
.equ CAN_BTR_SJW_0, (0x1 << CAN_BTR_SJW_Pos) //!< 0x01000000 
.equ CAN_BTR_SJW_1, (0x2 << CAN_BTR_SJW_Pos) //!< 0x02000000 
.equ CAN_BTR_LBKM_Pos, (30) 
.equ CAN_BTR_LBKM_Msk, (0x1 << CAN_BTR_LBKM_Pos) //!< 0x40000000 
.equ CAN_BTR_LBKM, CAN_BTR_LBKM_Msk //!<Loop Back Mode (Debug) 
.equ CAN_BTR_SILM_Pos, (31) 
.equ CAN_BTR_SILM_Msk, (0x1 << CAN_BTR_SILM_Pos) //!< 0x80000000 
.equ CAN_BTR_SILM, CAN_BTR_SILM_Msk //!<Silent Mode 
//!<Mailbox registers
//*****************  Bit definition for CAN_TI0R register  *******************
.equ CAN_TI0R_TXRQ_Pos, (0) 
.equ CAN_TI0R_TXRQ_Msk, (0x1 << CAN_TI0R_TXRQ_Pos) //!< 0x00000001 
.equ CAN_TI0R_TXRQ, CAN_TI0R_TXRQ_Msk //!<Transmit Mailbox Request 
.equ CAN_TI0R_RTR_Pos, (1) 
.equ CAN_TI0R_RTR_Msk, (0x1 << CAN_TI0R_RTR_Pos) //!< 0x00000002 
.equ CAN_TI0R_RTR, CAN_TI0R_RTR_Msk //!<Remote Transmission Request 
.equ CAN_TI0R_IDE_Pos, (2) 
.equ CAN_TI0R_IDE_Msk, (0x1 << CAN_TI0R_IDE_Pos) //!< 0x00000004 
.equ CAN_TI0R_IDE, CAN_TI0R_IDE_Msk //!<Identifier Extension 
.equ CAN_TI0R_EXID_Pos, (3) 
.equ CAN_TI0R_EXID_Msk, (0x3FFFF << CAN_TI0R_EXID_Pos) //!< 0x001FFFF8 
.equ CAN_TI0R_EXID, CAN_TI0R_EXID_Msk //!<Extended Identifier 
.equ CAN_TI0R_STID_Pos, (21) 
.equ CAN_TI0R_STID_Msk, (0x7FF << CAN_TI0R_STID_Pos) //!< 0xFFE00000 
.equ CAN_TI0R_STID, CAN_TI0R_STID_Msk //!<Standard Identifier or Extended Identifier 
//*****************  Bit definition for CAN_TDT0R register  ******************
.equ CAN_TDT0R_DLC_Pos, (0) 
.equ CAN_TDT0R_DLC_Msk, (0xF << CAN_TDT0R_DLC_Pos) //!< 0x0000000F 
.equ CAN_TDT0R_DLC, CAN_TDT0R_DLC_Msk //!<Data Length Code 
.equ CAN_TDT0R_TGT_Pos, (8) 
.equ CAN_TDT0R_TGT_Msk, (0x1 << CAN_TDT0R_TGT_Pos) //!< 0x00000100 
.equ CAN_TDT0R_TGT, CAN_TDT0R_TGT_Msk //!<Transmit Global Time 
.equ CAN_TDT0R_TIME_Pos, (16) 
.equ CAN_TDT0R_TIME_Msk, (0xFFFF << CAN_TDT0R_TIME_Pos) //!< 0xFFFF0000 
.equ CAN_TDT0R_TIME, CAN_TDT0R_TIME_Msk //!<Message Time Stamp 
//*****************  Bit definition for CAN_TDL0R register  ******************
.equ CAN_TDL0R_DATA0_Pos, (0) 
.equ CAN_TDL0R_DATA0_Msk, (0xFF << CAN_TDL0R_DATA0_Pos) //!< 0x000000FF 
.equ CAN_TDL0R_DATA0, CAN_TDL0R_DATA0_Msk //!<Data byte 0 
.equ CAN_TDL0R_DATA1_Pos, (8) 
.equ CAN_TDL0R_DATA1_Msk, (0xFF << CAN_TDL0R_DATA1_Pos) //!< 0x0000FF00 
.equ CAN_TDL0R_DATA1, CAN_TDL0R_DATA1_Msk //!<Data byte 1 
.equ CAN_TDL0R_DATA2_Pos, (16) 
.equ CAN_TDL0R_DATA2_Msk, (0xFF << CAN_TDL0R_DATA2_Pos) //!< 0x00FF0000 
.equ CAN_TDL0R_DATA2, CAN_TDL0R_DATA2_Msk //!<Data byte 2 
.equ CAN_TDL0R_DATA3_Pos, (24) 
.equ CAN_TDL0R_DATA3_Msk, (0xFF << CAN_TDL0R_DATA3_Pos) //!< 0xFF000000 
.equ CAN_TDL0R_DATA3, CAN_TDL0R_DATA3_Msk //!<Data byte 3 
//*****************  Bit definition for CAN_TDH0R register  ******************
.equ CAN_TDH0R_DATA4_Pos, (0) 
.equ CAN_TDH0R_DATA4_Msk, (0xFF << CAN_TDH0R_DATA4_Pos) //!< 0x000000FF 
.equ CAN_TDH0R_DATA4, CAN_TDH0R_DATA4_Msk //!<Data byte 4 
.equ CAN_TDH0R_DATA5_Pos, (8) 
.equ CAN_TDH0R_DATA5_Msk, (0xFF << CAN_TDH0R_DATA5_Pos) //!< 0x0000FF00 
.equ CAN_TDH0R_DATA5, CAN_TDH0R_DATA5_Msk //!<Data byte 5 
.equ CAN_TDH0R_DATA6_Pos, (16) 
.equ CAN_TDH0R_DATA6_Msk, (0xFF << CAN_TDH0R_DATA6_Pos) //!< 0x00FF0000 
.equ CAN_TDH0R_DATA6, CAN_TDH0R_DATA6_Msk //!<Data byte 6 
.equ CAN_TDH0R_DATA7_Pos, (24) 
.equ CAN_TDH0R_DATA7_Msk, (0xFF << CAN_TDH0R_DATA7_Pos) //!< 0xFF000000 
.equ CAN_TDH0R_DATA7, CAN_TDH0R_DATA7_Msk //!<Data byte 7 
//******************  Bit definition for CAN_TI1R register  ******************
.equ CAN_TI1R_TXRQ_Pos, (0) 
.equ CAN_TI1R_TXRQ_Msk, (0x1 << CAN_TI1R_TXRQ_Pos) //!< 0x00000001 
.equ CAN_TI1R_TXRQ, CAN_TI1R_TXRQ_Msk //!<Transmit Mailbox Request 
.equ CAN_TI1R_RTR_Pos, (1) 
.equ CAN_TI1R_RTR_Msk, (0x1 << CAN_TI1R_RTR_Pos) //!< 0x00000002 
.equ CAN_TI1R_RTR, CAN_TI1R_RTR_Msk //!<Remote Transmission Request 
.equ CAN_TI1R_IDE_Pos, (2) 
.equ CAN_TI1R_IDE_Msk, (0x1 << CAN_TI1R_IDE_Pos) //!< 0x00000004 
.equ CAN_TI1R_IDE, CAN_TI1R_IDE_Msk //!<Identifier Extension 
.equ CAN_TI1R_EXID_Pos, (3) 
.equ CAN_TI1R_EXID_Msk, (0x3FFFF << CAN_TI1R_EXID_Pos) //!< 0x001FFFF8 
.equ CAN_TI1R_EXID, CAN_TI1R_EXID_Msk //!<Extended Identifier 
.equ CAN_TI1R_STID_Pos, (21) 
.equ CAN_TI1R_STID_Msk, (0x7FF << CAN_TI1R_STID_Pos) //!< 0xFFE00000 
.equ CAN_TI1R_STID, CAN_TI1R_STID_Msk //!<Standard Identifier or Extended Identifier 
//******************  Bit definition for CAN_TDT1R register  *****************
.equ CAN_TDT1R_DLC_Pos, (0) 
.equ CAN_TDT1R_DLC_Msk, (0xF << CAN_TDT1R_DLC_Pos) //!< 0x0000000F 
.equ CAN_TDT1R_DLC, CAN_TDT1R_DLC_Msk //!<Data Length Code 
.equ CAN_TDT1R_TGT_Pos, (8) 
.equ CAN_TDT1R_TGT_Msk, (0x1 << CAN_TDT1R_TGT_Pos) //!< 0x00000100 
.equ CAN_TDT1R_TGT, CAN_TDT1R_TGT_Msk //!<Transmit Global Time 
.equ CAN_TDT1R_TIME_Pos, (16) 
.equ CAN_TDT1R_TIME_Msk, (0xFFFF << CAN_TDT1R_TIME_Pos) //!< 0xFFFF0000 
.equ CAN_TDT1R_TIME, CAN_TDT1R_TIME_Msk //!<Message Time Stamp 
//******************  Bit definition for CAN_TDL1R register  *****************
.equ CAN_TDL1R_DATA0_Pos, (0) 
.equ CAN_TDL1R_DATA0_Msk, (0xFF << CAN_TDL1R_DATA0_Pos) //!< 0x000000FF 
.equ CAN_TDL1R_DATA0, CAN_TDL1R_DATA0_Msk //!<Data byte 0 
.equ CAN_TDL1R_DATA1_Pos, (8) 
.equ CAN_TDL1R_DATA1_Msk, (0xFF << CAN_TDL1R_DATA1_Pos) //!< 0x0000FF00 
.equ CAN_TDL1R_DATA1, CAN_TDL1R_DATA1_Msk //!<Data byte 1 
.equ CAN_TDL1R_DATA2_Pos, (16) 
.equ CAN_TDL1R_DATA2_Msk, (0xFF << CAN_TDL1R_DATA2_Pos) //!< 0x00FF0000 
.equ CAN_TDL1R_DATA2, CAN_TDL1R_DATA2_Msk //!<Data byte 2 
.equ CAN_TDL1R_DATA3_Pos, (24) 
.equ CAN_TDL1R_DATA3_Msk, (0xFF << CAN_TDL1R_DATA3_Pos) //!< 0xFF000000 
.equ CAN_TDL1R_DATA3, CAN_TDL1R_DATA3_Msk //!<Data byte 3 
//******************  Bit definition for CAN_TDH1R register  *****************
.equ CAN_TDH1R_DATA4_Pos, (0) 
.equ CAN_TDH1R_DATA4_Msk, (0xFF << CAN_TDH1R_DATA4_Pos) //!< 0x000000FF 
.equ CAN_TDH1R_DATA4, CAN_TDH1R_DATA4_Msk //!<Data byte 4 
.equ CAN_TDH1R_DATA5_Pos, (8) 
.equ CAN_TDH1R_DATA5_Msk, (0xFF << CAN_TDH1R_DATA5_Pos) //!< 0x0000FF00 
.equ CAN_TDH1R_DATA5, CAN_TDH1R_DATA5_Msk //!<Data byte 5 
.equ CAN_TDH1R_DATA6_Pos, (16) 
.equ CAN_TDH1R_DATA6_Msk, (0xFF << CAN_TDH1R_DATA6_Pos) //!< 0x00FF0000 
.equ CAN_TDH1R_DATA6, CAN_TDH1R_DATA6_Msk //!<Data byte 6 
.equ CAN_TDH1R_DATA7_Pos, (24) 
.equ CAN_TDH1R_DATA7_Msk, (0xFF << CAN_TDH1R_DATA7_Pos) //!< 0xFF000000 
.equ CAN_TDH1R_DATA7, CAN_TDH1R_DATA7_Msk //!<Data byte 7 
//******************  Bit definition for CAN_TI2R register  ******************
.equ CAN_TI2R_TXRQ_Pos, (0) 
.equ CAN_TI2R_TXRQ_Msk, (0x1 << CAN_TI2R_TXRQ_Pos) //!< 0x00000001 
.equ CAN_TI2R_TXRQ, CAN_TI2R_TXRQ_Msk //!<Transmit Mailbox Request 
.equ CAN_TI2R_RTR_Pos, (1) 
.equ CAN_TI2R_RTR_Msk, (0x1 << CAN_TI2R_RTR_Pos) //!< 0x00000002 
.equ CAN_TI2R_RTR, CAN_TI2R_RTR_Msk //!<Remote Transmission Request 
.equ CAN_TI2R_IDE_Pos, (2) 
.equ CAN_TI2R_IDE_Msk, (0x1 << CAN_TI2R_IDE_Pos) //!< 0x00000004 
.equ CAN_TI2R_IDE, CAN_TI2R_IDE_Msk //!<Identifier Extension 
.equ CAN_TI2R_EXID_Pos, (3) 
.equ CAN_TI2R_EXID_Msk, (0x3FFFF << CAN_TI2R_EXID_Pos) //!< 0x001FFFF8 
.equ CAN_TI2R_EXID, CAN_TI2R_EXID_Msk //!<Extended identifier 
.equ CAN_TI2R_STID_Pos, (21) 
.equ CAN_TI2R_STID_Msk, (0x7FF << CAN_TI2R_STID_Pos) //!< 0xFFE00000 
.equ CAN_TI2R_STID, CAN_TI2R_STID_Msk //!<Standard Identifier or Extended Identifier 
//******************  Bit definition for CAN_TDT2R register  *****************
.equ CAN_TDT2R_DLC_Pos, (0) 
.equ CAN_TDT2R_DLC_Msk, (0xF << CAN_TDT2R_DLC_Pos) //!< 0x0000000F 
.equ CAN_TDT2R_DLC, CAN_TDT2R_DLC_Msk //!<Data Length Code 
.equ CAN_TDT2R_TGT_Pos, (8) 
.equ CAN_TDT2R_TGT_Msk, (0x1 << CAN_TDT2R_TGT_Pos) //!< 0x00000100 
.equ CAN_TDT2R_TGT, CAN_TDT2R_TGT_Msk //!<Transmit Global Time 
.equ CAN_TDT2R_TIME_Pos, (16) 
.equ CAN_TDT2R_TIME_Msk, (0xFFFF << CAN_TDT2R_TIME_Pos) //!< 0xFFFF0000 
.equ CAN_TDT2R_TIME, CAN_TDT2R_TIME_Msk //!<Message Time Stamp 
//******************  Bit definition for CAN_TDL2R register  *****************
.equ CAN_TDL2R_DATA0_Pos, (0) 
.equ CAN_TDL2R_DATA0_Msk, (0xFF << CAN_TDL2R_DATA0_Pos) //!< 0x000000FF 
.equ CAN_TDL2R_DATA0, CAN_TDL2R_DATA0_Msk //!<Data byte 0 
.equ CAN_TDL2R_DATA1_Pos, (8) 
.equ CAN_TDL2R_DATA1_Msk, (0xFF << CAN_TDL2R_DATA1_Pos) //!< 0x0000FF00 
.equ CAN_TDL2R_DATA1, CAN_TDL2R_DATA1_Msk //!<Data byte 1 
.equ CAN_TDL2R_DATA2_Pos, (16) 
.equ CAN_TDL2R_DATA2_Msk, (0xFF << CAN_TDL2R_DATA2_Pos) //!< 0x00FF0000 
.equ CAN_TDL2R_DATA2, CAN_TDL2R_DATA2_Msk //!<Data byte 2 
.equ CAN_TDL2R_DATA3_Pos, (24) 
.equ CAN_TDL2R_DATA3_Msk, (0xFF << CAN_TDL2R_DATA3_Pos) //!< 0xFF000000 
.equ CAN_TDL2R_DATA3, CAN_TDL2R_DATA3_Msk //!<Data byte 3 
//******************  Bit definition for CAN_TDH2R register  *****************
.equ CAN_TDH2R_DATA4_Pos, (0) 
.equ CAN_TDH2R_DATA4_Msk, (0xFF << CAN_TDH2R_DATA4_Pos) //!< 0x000000FF 
.equ CAN_TDH2R_DATA4, CAN_TDH2R_DATA4_Msk //!<Data byte 4 
.equ CAN_TDH2R_DATA5_Pos, (8) 
.equ CAN_TDH2R_DATA5_Msk, (0xFF << CAN_TDH2R_DATA5_Pos) //!< 0x0000FF00 
.equ CAN_TDH2R_DATA5, CAN_TDH2R_DATA5_Msk //!<Data byte 5 
.equ CAN_TDH2R_DATA6_Pos, (16) 
.equ CAN_TDH2R_DATA6_Msk, (0xFF << CAN_TDH2R_DATA6_Pos) //!< 0x00FF0000 
.equ CAN_TDH2R_DATA6, CAN_TDH2R_DATA6_Msk //!<Data byte 6 
.equ CAN_TDH2R_DATA7_Pos, (24) 
.equ CAN_TDH2R_DATA7_Msk, (0xFF << CAN_TDH2R_DATA7_Pos) //!< 0xFF000000 
.equ CAN_TDH2R_DATA7, CAN_TDH2R_DATA7_Msk //!<Data byte 7 
//******************  Bit definition for CAN_RI0R register  ******************
.equ CAN_RI0R_RTR_Pos, (1) 
.equ CAN_RI0R_RTR_Msk, (0x1 << CAN_RI0R_RTR_Pos) //!< 0x00000002 
.equ CAN_RI0R_RTR, CAN_RI0R_RTR_Msk //!<Remote Transmission Request 
.equ CAN_RI0R_IDE_Pos, (2) 
.equ CAN_RI0R_IDE_Msk, (0x1 << CAN_RI0R_IDE_Pos) //!< 0x00000004 
.equ CAN_RI0R_IDE, CAN_RI0R_IDE_Msk //!<Identifier Extension 
.equ CAN_RI0R_EXID_Pos, (3) 
.equ CAN_RI0R_EXID_Msk, (0x3FFFF << CAN_RI0R_EXID_Pos) //!< 0x001FFFF8 
.equ CAN_RI0R_EXID, CAN_RI0R_EXID_Msk //!<Extended Identifier 
.equ CAN_RI0R_STID_Pos, (21) 
.equ CAN_RI0R_STID_Msk, (0x7FF << CAN_RI0R_STID_Pos) //!< 0xFFE00000 
.equ CAN_RI0R_STID, CAN_RI0R_STID_Msk //!<Standard Identifier or Extended Identifier 
//******************  Bit definition for CAN_RDT0R register  *****************
.equ CAN_RDT0R_DLC_Pos, (0) 
.equ CAN_RDT0R_DLC_Msk, (0xF << CAN_RDT0R_DLC_Pos) //!< 0x0000000F 
.equ CAN_RDT0R_DLC, CAN_RDT0R_DLC_Msk //!<Data Length Code 
.equ CAN_RDT0R_FMI_Pos, (8) 
.equ CAN_RDT0R_FMI_Msk, (0xFF << CAN_RDT0R_FMI_Pos) //!< 0x0000FF00 
.equ CAN_RDT0R_FMI, CAN_RDT0R_FMI_Msk //!<Filter Match Index 
.equ CAN_RDT0R_TIME_Pos, (16) 
.equ CAN_RDT0R_TIME_Msk, (0xFFFF << CAN_RDT0R_TIME_Pos) //!< 0xFFFF0000 
.equ CAN_RDT0R_TIME, CAN_RDT0R_TIME_Msk //!<Message Time Stamp 
//******************  Bit definition for CAN_RDL0R register  *****************
.equ CAN_RDL0R_DATA0_Pos, (0) 
.equ CAN_RDL0R_DATA0_Msk, (0xFF << CAN_RDL0R_DATA0_Pos) //!< 0x000000FF 
.equ CAN_RDL0R_DATA0, CAN_RDL0R_DATA0_Msk //!<Data byte 0 
.equ CAN_RDL0R_DATA1_Pos, (8) 
.equ CAN_RDL0R_DATA1_Msk, (0xFF << CAN_RDL0R_DATA1_Pos) //!< 0x0000FF00 
.equ CAN_RDL0R_DATA1, CAN_RDL0R_DATA1_Msk //!<Data byte 1 
.equ CAN_RDL0R_DATA2_Pos, (16) 
.equ CAN_RDL0R_DATA2_Msk, (0xFF << CAN_RDL0R_DATA2_Pos) //!< 0x00FF0000 
.equ CAN_RDL0R_DATA2, CAN_RDL0R_DATA2_Msk //!<Data byte 2 
.equ CAN_RDL0R_DATA3_Pos, (24) 
.equ CAN_RDL0R_DATA3_Msk, (0xFF << CAN_RDL0R_DATA3_Pos) //!< 0xFF000000 
.equ CAN_RDL0R_DATA3, CAN_RDL0R_DATA3_Msk //!<Data byte 3 
//******************  Bit definition for CAN_RDH0R register  *****************
.equ CAN_RDH0R_DATA4_Pos, (0) 
.equ CAN_RDH0R_DATA4_Msk, (0xFF << CAN_RDH0R_DATA4_Pos) //!< 0x000000FF 
.equ CAN_RDH0R_DATA4, CAN_RDH0R_DATA4_Msk //!<Data byte 4 
.equ CAN_RDH0R_DATA5_Pos, (8) 
.equ CAN_RDH0R_DATA5_Msk, (0xFF << CAN_RDH0R_DATA5_Pos) //!< 0x0000FF00 
.equ CAN_RDH0R_DATA5, CAN_RDH0R_DATA5_Msk //!<Data byte 5 
.equ CAN_RDH0R_DATA6_Pos, (16) 
.equ CAN_RDH0R_DATA6_Msk, (0xFF << CAN_RDH0R_DATA6_Pos) //!< 0x00FF0000 
.equ CAN_RDH0R_DATA6, CAN_RDH0R_DATA6_Msk //!<Data byte 6 
.equ CAN_RDH0R_DATA7_Pos, (24) 
.equ CAN_RDH0R_DATA7_Msk, (0xFF << CAN_RDH0R_DATA7_Pos) //!< 0xFF000000 
.equ CAN_RDH0R_DATA7, CAN_RDH0R_DATA7_Msk //!<Data byte 7 
//******************  Bit definition for CAN_RI1R register  ******************
.equ CAN_RI1R_RTR_Pos, (1) 
.equ CAN_RI1R_RTR_Msk, (0x1 << CAN_RI1R_RTR_Pos) //!< 0x00000002 
.equ CAN_RI1R_RTR, CAN_RI1R_RTR_Msk //!<Remote Transmission Request 
.equ CAN_RI1R_IDE_Pos, (2) 
.equ CAN_RI1R_IDE_Msk, (0x1 << CAN_RI1R_IDE_Pos) //!< 0x00000004 
.equ CAN_RI1R_IDE, CAN_RI1R_IDE_Msk //!<Identifier Extension 
.equ CAN_RI1R_EXID_Pos, (3) 
.equ CAN_RI1R_EXID_Msk, (0x3FFFF << CAN_RI1R_EXID_Pos) //!< 0x001FFFF8 
.equ CAN_RI1R_EXID, CAN_RI1R_EXID_Msk //!<Extended identifier 
.equ CAN_RI1R_STID_Pos, (21) 
.equ CAN_RI1R_STID_Msk, (0x7FF << CAN_RI1R_STID_Pos) //!< 0xFFE00000 
.equ CAN_RI1R_STID, CAN_RI1R_STID_Msk //!<Standard Identifier or Extended Identifier 
//******************  Bit definition for CAN_RDT1R register  *****************
.equ CAN_RDT1R_DLC_Pos, (0) 
.equ CAN_RDT1R_DLC_Msk, (0xF << CAN_RDT1R_DLC_Pos) //!< 0x0000000F 
.equ CAN_RDT1R_DLC, CAN_RDT1R_DLC_Msk //!<Data Length Code 
.equ CAN_RDT1R_FMI_Pos, (8) 
.equ CAN_RDT1R_FMI_Msk, (0xFF << CAN_RDT1R_FMI_Pos) //!< 0x0000FF00 
.equ CAN_RDT1R_FMI, CAN_RDT1R_FMI_Msk //!<Filter Match Index 
.equ CAN_RDT1R_TIME_Pos, (16) 
.equ CAN_RDT1R_TIME_Msk, (0xFFFF << CAN_RDT1R_TIME_Pos) //!< 0xFFFF0000 
.equ CAN_RDT1R_TIME, CAN_RDT1R_TIME_Msk //!<Message Time Stamp 
//******************  Bit definition for CAN_RDL1R register  *****************
.equ CAN_RDL1R_DATA0_Pos, (0) 
.equ CAN_RDL1R_DATA0_Msk, (0xFF << CAN_RDL1R_DATA0_Pos) //!< 0x000000FF 
.equ CAN_RDL1R_DATA0, CAN_RDL1R_DATA0_Msk //!<Data byte 0 
.equ CAN_RDL1R_DATA1_Pos, (8) 
.equ CAN_RDL1R_DATA1_Msk, (0xFF << CAN_RDL1R_DATA1_Pos) //!< 0x0000FF00 
.equ CAN_RDL1R_DATA1, CAN_RDL1R_DATA1_Msk //!<Data byte 1 
.equ CAN_RDL1R_DATA2_Pos, (16) 
.equ CAN_RDL1R_DATA2_Msk, (0xFF << CAN_RDL1R_DATA2_Pos) //!< 0x00FF0000 
.equ CAN_RDL1R_DATA2, CAN_RDL1R_DATA2_Msk //!<Data byte 2 
.equ CAN_RDL1R_DATA3_Pos, (24) 
.equ CAN_RDL1R_DATA3_Msk, (0xFF << CAN_RDL1R_DATA3_Pos) //!< 0xFF000000 
.equ CAN_RDL1R_DATA3, CAN_RDL1R_DATA3_Msk //!<Data byte 3 
//******************  Bit definition for CAN_RDH1R register  *****************
.equ CAN_RDH1R_DATA4_Pos, (0) 
.equ CAN_RDH1R_DATA4_Msk, (0xFF << CAN_RDH1R_DATA4_Pos) //!< 0x000000FF 
.equ CAN_RDH1R_DATA4, CAN_RDH1R_DATA4_Msk //!<Data byte 4 
.equ CAN_RDH1R_DATA5_Pos, (8) 
.equ CAN_RDH1R_DATA5_Msk, (0xFF << CAN_RDH1R_DATA5_Pos) //!< 0x0000FF00 
.equ CAN_RDH1R_DATA5, CAN_RDH1R_DATA5_Msk //!<Data byte 5 
.equ CAN_RDH1R_DATA6_Pos, (16) 
.equ CAN_RDH1R_DATA6_Msk, (0xFF << CAN_RDH1R_DATA6_Pos) //!< 0x00FF0000 
.equ CAN_RDH1R_DATA6, CAN_RDH1R_DATA6_Msk //!<Data byte 6 
.equ CAN_RDH1R_DATA7_Pos, (24) 
.equ CAN_RDH1R_DATA7_Msk, (0xFF << CAN_RDH1R_DATA7_Pos) //!< 0xFF000000 
.equ CAN_RDH1R_DATA7, CAN_RDH1R_DATA7_Msk //!<Data byte 7 
//!<CAN filter registers
//******************  Bit definition for CAN_FMR register  *******************
.equ CAN_FMR_FINIT_Pos, (0) 
.equ CAN_FMR_FINIT_Msk, (0x1 << CAN_FMR_FINIT_Pos) //!< 0x00000001 
.equ CAN_FMR_FINIT, CAN_FMR_FINIT_Msk //!<Filter Init Mode 
//******************  Bit definition for CAN_FM1R register  ******************
.equ CAN_FM1R_FBM_Pos, (0) 
.equ CAN_FM1R_FBM_Msk, (0x3FFF << CAN_FM1R_FBM_Pos) //!< 0x00003FFF 
.equ CAN_FM1R_FBM, CAN_FM1R_FBM_Msk //!<Filter Mode 
.equ CAN_FM1R_FBM0_Pos, (0) 
.equ CAN_FM1R_FBM0_Msk, (0x1 << CAN_FM1R_FBM0_Pos) //!< 0x00000001 
.equ CAN_FM1R_FBM0, CAN_FM1R_FBM0_Msk //!<Filter Init Mode bit 0 
.equ CAN_FM1R_FBM1_Pos, (1) 
.equ CAN_FM1R_FBM1_Msk, (0x1 << CAN_FM1R_FBM1_Pos) //!< 0x00000002 
.equ CAN_FM1R_FBM1, CAN_FM1R_FBM1_Msk //!<Filter Init Mode bit 1 
.equ CAN_FM1R_FBM2_Pos, (2) 
.equ CAN_FM1R_FBM2_Msk, (0x1 << CAN_FM1R_FBM2_Pos) //!< 0x00000004 
.equ CAN_FM1R_FBM2, CAN_FM1R_FBM2_Msk //!<Filter Init Mode bit 2 
.equ CAN_FM1R_FBM3_Pos, (3) 
.equ CAN_FM1R_FBM3_Msk, (0x1 << CAN_FM1R_FBM3_Pos) //!< 0x00000008 
.equ CAN_FM1R_FBM3, CAN_FM1R_FBM3_Msk //!<Filter Init Mode bit 3 
.equ CAN_FM1R_FBM4_Pos, (4) 
.equ CAN_FM1R_FBM4_Msk, (0x1 << CAN_FM1R_FBM4_Pos) //!< 0x00000010 
.equ CAN_FM1R_FBM4, CAN_FM1R_FBM4_Msk //!<Filter Init Mode bit 4 
.equ CAN_FM1R_FBM5_Pos, (5) 
.equ CAN_FM1R_FBM5_Msk, (0x1 << CAN_FM1R_FBM5_Pos) //!< 0x00000020 
.equ CAN_FM1R_FBM5, CAN_FM1R_FBM5_Msk //!<Filter Init Mode bit 5 
.equ CAN_FM1R_FBM6_Pos, (6) 
.equ CAN_FM1R_FBM6_Msk, (0x1 << CAN_FM1R_FBM6_Pos) //!< 0x00000040 
.equ CAN_FM1R_FBM6, CAN_FM1R_FBM6_Msk //!<Filter Init Mode bit 6 
.equ CAN_FM1R_FBM7_Pos, (7) 
.equ CAN_FM1R_FBM7_Msk, (0x1 << CAN_FM1R_FBM7_Pos) //!< 0x00000080 
.equ CAN_FM1R_FBM7, CAN_FM1R_FBM7_Msk //!<Filter Init Mode bit 7 
.equ CAN_FM1R_FBM8_Pos, (8) 
.equ CAN_FM1R_FBM8_Msk, (0x1 << CAN_FM1R_FBM8_Pos) //!< 0x00000100 
.equ CAN_FM1R_FBM8, CAN_FM1R_FBM8_Msk //!<Filter Init Mode bit 8 
.equ CAN_FM1R_FBM9_Pos, (9) 
.equ CAN_FM1R_FBM9_Msk, (0x1 << CAN_FM1R_FBM9_Pos) //!< 0x00000200 
.equ CAN_FM1R_FBM9, CAN_FM1R_FBM9_Msk //!<Filter Init Mode bit 9 
.equ CAN_FM1R_FBM10_Pos, (10) 
.equ CAN_FM1R_FBM10_Msk, (0x1 << CAN_FM1R_FBM10_Pos) //!< 0x00000400 
.equ CAN_FM1R_FBM10, CAN_FM1R_FBM10_Msk //!<Filter Init Mode bit 10 
.equ CAN_FM1R_FBM11_Pos, (11) 
.equ CAN_FM1R_FBM11_Msk, (0x1 << CAN_FM1R_FBM11_Pos) //!< 0x00000800 
.equ CAN_FM1R_FBM11, CAN_FM1R_FBM11_Msk //!<Filter Init Mode bit 11 
.equ CAN_FM1R_FBM12_Pos, (12) 
.equ CAN_FM1R_FBM12_Msk, (0x1 << CAN_FM1R_FBM12_Pos) //!< 0x00001000 
.equ CAN_FM1R_FBM12, CAN_FM1R_FBM12_Msk //!<Filter Init Mode bit 12 
.equ CAN_FM1R_FBM13_Pos, (13) 
.equ CAN_FM1R_FBM13_Msk, (0x1 << CAN_FM1R_FBM13_Pos) //!< 0x00002000 
.equ CAN_FM1R_FBM13, CAN_FM1R_FBM13_Msk //!<Filter Init Mode bit 13 
//******************  Bit definition for CAN_FS1R register  ******************
.equ CAN_FS1R_FSC_Pos, (0) 
.equ CAN_FS1R_FSC_Msk, (0x3FFF << CAN_FS1R_FSC_Pos) //!< 0x00003FFF 
.equ CAN_FS1R_FSC, CAN_FS1R_FSC_Msk //!<Filter Scale Configuration 
.equ CAN_FS1R_FSC0_Pos, (0) 
.equ CAN_FS1R_FSC0_Msk, (0x1 << CAN_FS1R_FSC0_Pos) //!< 0x00000001 
.equ CAN_FS1R_FSC0, CAN_FS1R_FSC0_Msk //!<Filter Scale Configuration bit 0 
.equ CAN_FS1R_FSC1_Pos, (1) 
.equ CAN_FS1R_FSC1_Msk, (0x1 << CAN_FS1R_FSC1_Pos) //!< 0x00000002 
.equ CAN_FS1R_FSC1, CAN_FS1R_FSC1_Msk //!<Filter Scale Configuration bit 1 
.equ CAN_FS1R_FSC2_Pos, (2) 
.equ CAN_FS1R_FSC2_Msk, (0x1 << CAN_FS1R_FSC2_Pos) //!< 0x00000004 
.equ CAN_FS1R_FSC2, CAN_FS1R_FSC2_Msk //!<Filter Scale Configuration bit 2 
.equ CAN_FS1R_FSC3_Pos, (3) 
.equ CAN_FS1R_FSC3_Msk, (0x1 << CAN_FS1R_FSC3_Pos) //!< 0x00000008 
.equ CAN_FS1R_FSC3, CAN_FS1R_FSC3_Msk //!<Filter Scale Configuration bit 3 
.equ CAN_FS1R_FSC4_Pos, (4) 
.equ CAN_FS1R_FSC4_Msk, (0x1 << CAN_FS1R_FSC4_Pos) //!< 0x00000010 
.equ CAN_FS1R_FSC4, CAN_FS1R_FSC4_Msk //!<Filter Scale Configuration bit 4 
.equ CAN_FS1R_FSC5_Pos, (5) 
.equ CAN_FS1R_FSC5_Msk, (0x1 << CAN_FS1R_FSC5_Pos) //!< 0x00000020 
.equ CAN_FS1R_FSC5, CAN_FS1R_FSC5_Msk //!<Filter Scale Configuration bit 5 
.equ CAN_FS1R_FSC6_Pos, (6) 
.equ CAN_FS1R_FSC6_Msk, (0x1 << CAN_FS1R_FSC6_Pos) //!< 0x00000040 
.equ CAN_FS1R_FSC6, CAN_FS1R_FSC6_Msk //!<Filter Scale Configuration bit 6 
.equ CAN_FS1R_FSC7_Pos, (7) 
.equ CAN_FS1R_FSC7_Msk, (0x1 << CAN_FS1R_FSC7_Pos) //!< 0x00000080 
.equ CAN_FS1R_FSC7, CAN_FS1R_FSC7_Msk //!<Filter Scale Configuration bit 7 
.equ CAN_FS1R_FSC8_Pos, (8) 
.equ CAN_FS1R_FSC8_Msk, (0x1 << CAN_FS1R_FSC8_Pos) //!< 0x00000100 
.equ CAN_FS1R_FSC8, CAN_FS1R_FSC8_Msk //!<Filter Scale Configuration bit 8 
.equ CAN_FS1R_FSC9_Pos, (9) 
.equ CAN_FS1R_FSC9_Msk, (0x1 << CAN_FS1R_FSC9_Pos) //!< 0x00000200 
.equ CAN_FS1R_FSC9, CAN_FS1R_FSC9_Msk //!<Filter Scale Configuration bit 9 
.equ CAN_FS1R_FSC10_Pos, (10) 
.equ CAN_FS1R_FSC10_Msk, (0x1 << CAN_FS1R_FSC10_Pos) //!< 0x00000400 
.equ CAN_FS1R_FSC10, CAN_FS1R_FSC10_Msk //!<Filter Scale Configuration bit 10 
.equ CAN_FS1R_FSC11_Pos, (11) 
.equ CAN_FS1R_FSC11_Msk, (0x1 << CAN_FS1R_FSC11_Pos) //!< 0x00000800 
.equ CAN_FS1R_FSC11, CAN_FS1R_FSC11_Msk //!<Filter Scale Configuration bit 11 
.equ CAN_FS1R_FSC12_Pos, (12) 
.equ CAN_FS1R_FSC12_Msk, (0x1 << CAN_FS1R_FSC12_Pos) //!< 0x00001000 
.equ CAN_FS1R_FSC12, CAN_FS1R_FSC12_Msk //!<Filter Scale Configuration bit 12 
.equ CAN_FS1R_FSC13_Pos, (13) 
.equ CAN_FS1R_FSC13_Msk, (0x1 << CAN_FS1R_FSC13_Pos) //!< 0x00002000 
.equ CAN_FS1R_FSC13, CAN_FS1R_FSC13_Msk //!<Filter Scale Configuration bit 13 
//*****************  Bit definition for CAN_FFA1R register  ******************
.equ CAN_FFA1R_FFA_Pos, (0) 
.equ CAN_FFA1R_FFA_Msk, (0x3FFF << CAN_FFA1R_FFA_Pos) //!< 0x00003FFF 
.equ CAN_FFA1R_FFA, CAN_FFA1R_FFA_Msk //!<Filter FIFO Assignment 
.equ CAN_FFA1R_FFA0_Pos, (0) 
.equ CAN_FFA1R_FFA0_Msk, (0x1 << CAN_FFA1R_FFA0_Pos) //!< 0x00000001 
.equ CAN_FFA1R_FFA0, CAN_FFA1R_FFA0_Msk //!<Filter FIFO Assignment for Filter 0 
.equ CAN_FFA1R_FFA1_Pos, (1) 
.equ CAN_FFA1R_FFA1_Msk, (0x1 << CAN_FFA1R_FFA1_Pos) //!< 0x00000002 
.equ CAN_FFA1R_FFA1, CAN_FFA1R_FFA1_Msk //!<Filter FIFO Assignment for Filter 1 
.equ CAN_FFA1R_FFA2_Pos, (2) 
.equ CAN_FFA1R_FFA2_Msk, (0x1 << CAN_FFA1R_FFA2_Pos) //!< 0x00000004 
.equ CAN_FFA1R_FFA2, CAN_FFA1R_FFA2_Msk //!<Filter FIFO Assignment for Filter 2 
.equ CAN_FFA1R_FFA3_Pos, (3) 
.equ CAN_FFA1R_FFA3_Msk, (0x1 << CAN_FFA1R_FFA3_Pos) //!< 0x00000008 
.equ CAN_FFA1R_FFA3, CAN_FFA1R_FFA3_Msk //!<Filter FIFO Assignment for Filter 3 
.equ CAN_FFA1R_FFA4_Pos, (4) 
.equ CAN_FFA1R_FFA4_Msk, (0x1 << CAN_FFA1R_FFA4_Pos) //!< 0x00000010 
.equ CAN_FFA1R_FFA4, CAN_FFA1R_FFA4_Msk //!<Filter FIFO Assignment for Filter 4 
.equ CAN_FFA1R_FFA5_Pos, (5) 
.equ CAN_FFA1R_FFA5_Msk, (0x1 << CAN_FFA1R_FFA5_Pos) //!< 0x00000020 
.equ CAN_FFA1R_FFA5, CAN_FFA1R_FFA5_Msk //!<Filter FIFO Assignment for Filter 5 
.equ CAN_FFA1R_FFA6_Pos, (6) 
.equ CAN_FFA1R_FFA6_Msk, (0x1 << CAN_FFA1R_FFA6_Pos) //!< 0x00000040 
.equ CAN_FFA1R_FFA6, CAN_FFA1R_FFA6_Msk //!<Filter FIFO Assignment for Filter 6 
.equ CAN_FFA1R_FFA7_Pos, (7) 
.equ CAN_FFA1R_FFA7_Msk, (0x1 << CAN_FFA1R_FFA7_Pos) //!< 0x00000080 
.equ CAN_FFA1R_FFA7, CAN_FFA1R_FFA7_Msk //!<Filter FIFO Assignment for Filter 7 
.equ CAN_FFA1R_FFA8_Pos, (8) 
.equ CAN_FFA1R_FFA8_Msk, (0x1 << CAN_FFA1R_FFA8_Pos) //!< 0x00000100 
.equ CAN_FFA1R_FFA8, CAN_FFA1R_FFA8_Msk //!<Filter FIFO Assignment for Filter 8 
.equ CAN_FFA1R_FFA9_Pos, (9) 
.equ CAN_FFA1R_FFA9_Msk, (0x1 << CAN_FFA1R_FFA9_Pos) //!< 0x00000200 
.equ CAN_FFA1R_FFA9, CAN_FFA1R_FFA9_Msk //!<Filter FIFO Assignment for Filter 9 
.equ CAN_FFA1R_FFA10_Pos, (10) 
.equ CAN_FFA1R_FFA10_Msk, (0x1 << CAN_FFA1R_FFA10_Pos) //!< 0x00000400 
.equ CAN_FFA1R_FFA10, CAN_FFA1R_FFA10_Msk //!<Filter FIFO Assignment for Filter 10 
.equ CAN_FFA1R_FFA11_Pos, (11) 
.equ CAN_FFA1R_FFA11_Msk, (0x1 << CAN_FFA1R_FFA11_Pos) //!< 0x00000800 
.equ CAN_FFA1R_FFA11, CAN_FFA1R_FFA11_Msk //!<Filter FIFO Assignment for Filter 11 
.equ CAN_FFA1R_FFA12_Pos, (12) 
.equ CAN_FFA1R_FFA12_Msk, (0x1 << CAN_FFA1R_FFA12_Pos) //!< 0x00001000 
.equ CAN_FFA1R_FFA12, CAN_FFA1R_FFA12_Msk //!<Filter FIFO Assignment for Filter 12 
.equ CAN_FFA1R_FFA13_Pos, (13) 
.equ CAN_FFA1R_FFA13_Msk, (0x1 << CAN_FFA1R_FFA13_Pos) //!< 0x00002000 
.equ CAN_FFA1R_FFA13, CAN_FFA1R_FFA13_Msk //!<Filter FIFO Assignment for Filter 13 
//******************  Bit definition for CAN_FA1R register  ******************
.equ CAN_FA1R_FACT_Pos, (0) 
.equ CAN_FA1R_FACT_Msk, (0x3FFF << CAN_FA1R_FACT_Pos) //!< 0x00003FFF 
.equ CAN_FA1R_FACT, CAN_FA1R_FACT_Msk //!<Filter Active 
.equ CAN_FA1R_FACT0_Pos, (0) 
.equ CAN_FA1R_FACT0_Msk, (0x1 << CAN_FA1R_FACT0_Pos) //!< 0x00000001 
.equ CAN_FA1R_FACT0, CAN_FA1R_FACT0_Msk //!<Filter 0 Active 
.equ CAN_FA1R_FACT1_Pos, (1) 
.equ CAN_FA1R_FACT1_Msk, (0x1 << CAN_FA1R_FACT1_Pos) //!< 0x00000002 
.equ CAN_FA1R_FACT1, CAN_FA1R_FACT1_Msk //!<Filter 1 Active 
.equ CAN_FA1R_FACT2_Pos, (2) 
.equ CAN_FA1R_FACT2_Msk, (0x1 << CAN_FA1R_FACT2_Pos) //!< 0x00000004 
.equ CAN_FA1R_FACT2, CAN_FA1R_FACT2_Msk //!<Filter 2 Active 
.equ CAN_FA1R_FACT3_Pos, (3) 
.equ CAN_FA1R_FACT3_Msk, (0x1 << CAN_FA1R_FACT3_Pos) //!< 0x00000008 
.equ CAN_FA1R_FACT3, CAN_FA1R_FACT3_Msk //!<Filter 3 Active 
.equ CAN_FA1R_FACT4_Pos, (4) 
.equ CAN_FA1R_FACT4_Msk, (0x1 << CAN_FA1R_FACT4_Pos) //!< 0x00000010 
.equ CAN_FA1R_FACT4, CAN_FA1R_FACT4_Msk //!<Filter 4 Active 
.equ CAN_FA1R_FACT5_Pos, (5) 
.equ CAN_FA1R_FACT5_Msk, (0x1 << CAN_FA1R_FACT5_Pos) //!< 0x00000020 
.equ CAN_FA1R_FACT5, CAN_FA1R_FACT5_Msk //!<Filter 5 Active 
.equ CAN_FA1R_FACT6_Pos, (6) 
.equ CAN_FA1R_FACT6_Msk, (0x1 << CAN_FA1R_FACT6_Pos) //!< 0x00000040 
.equ CAN_FA1R_FACT6, CAN_FA1R_FACT6_Msk //!<Filter 6 Active 
.equ CAN_FA1R_FACT7_Pos, (7) 
.equ CAN_FA1R_FACT7_Msk, (0x1 << CAN_FA1R_FACT7_Pos) //!< 0x00000080 
.equ CAN_FA1R_FACT7, CAN_FA1R_FACT7_Msk //!<Filter 7 Active 
.equ CAN_FA1R_FACT8_Pos, (8) 
.equ CAN_FA1R_FACT8_Msk, (0x1 << CAN_FA1R_FACT8_Pos) //!< 0x00000100 
.equ CAN_FA1R_FACT8, CAN_FA1R_FACT8_Msk //!<Filter 8 Active 
.equ CAN_FA1R_FACT9_Pos, (9) 
.equ CAN_FA1R_FACT9_Msk, (0x1 << CAN_FA1R_FACT9_Pos) //!< 0x00000200 
.equ CAN_FA1R_FACT9, CAN_FA1R_FACT9_Msk //!<Filter 9 Active 
.equ CAN_FA1R_FACT10_Pos, (10) 
.equ CAN_FA1R_FACT10_Msk, (0x1 << CAN_FA1R_FACT10_Pos) //!< 0x00000400 
.equ CAN_FA1R_FACT10, CAN_FA1R_FACT10_Msk //!<Filter 10 Active 
.equ CAN_FA1R_FACT11_Pos, (11) 
.equ CAN_FA1R_FACT11_Msk, (0x1 << CAN_FA1R_FACT11_Pos) //!< 0x00000800 
.equ CAN_FA1R_FACT11, CAN_FA1R_FACT11_Msk //!<Filter 11 Active 
.equ CAN_FA1R_FACT12_Pos, (12) 
.equ CAN_FA1R_FACT12_Msk, (0x1 << CAN_FA1R_FACT12_Pos) //!< 0x00001000 
.equ CAN_FA1R_FACT12, CAN_FA1R_FACT12_Msk //!<Filter 12 Active 
.equ CAN_FA1R_FACT13_Pos, (13) 
.equ CAN_FA1R_FACT13_Msk, (0x1 << CAN_FA1R_FACT13_Pos) //!< 0x00002000 
.equ CAN_FA1R_FACT13, CAN_FA1R_FACT13_Msk //!<Filter 13 Active 
//******************  Bit definition for CAN_F0R1 register  ******************
.equ CAN_F0R1_FB0_Pos, (0) 
.equ CAN_F0R1_FB0_Msk, (0x1 << CAN_F0R1_FB0_Pos) //!< 0x00000001 
.equ CAN_F0R1_FB0, CAN_F0R1_FB0_Msk //!<Filter bit 0 
.equ CAN_F0R1_FB1_Pos, (1) 
.equ CAN_F0R1_FB1_Msk, (0x1 << CAN_F0R1_FB1_Pos) //!< 0x00000002 
.equ CAN_F0R1_FB1, CAN_F0R1_FB1_Msk //!<Filter bit 1 
.equ CAN_F0R1_FB2_Pos, (2) 
.equ CAN_F0R1_FB2_Msk, (0x1 << CAN_F0R1_FB2_Pos) //!< 0x00000004 
.equ CAN_F0R1_FB2, CAN_F0R1_FB2_Msk //!<Filter bit 2 
.equ CAN_F0R1_FB3_Pos, (3) 
.equ CAN_F0R1_FB3_Msk, (0x1 << CAN_F0R1_FB3_Pos) //!< 0x00000008 
.equ CAN_F0R1_FB3, CAN_F0R1_FB3_Msk //!<Filter bit 3 
.equ CAN_F0R1_FB4_Pos, (4) 
.equ CAN_F0R1_FB4_Msk, (0x1 << CAN_F0R1_FB4_Pos) //!< 0x00000010 
.equ CAN_F0R1_FB4, CAN_F0R1_FB4_Msk //!<Filter bit 4 
.equ CAN_F0R1_FB5_Pos, (5) 
.equ CAN_F0R1_FB5_Msk, (0x1 << CAN_F0R1_FB5_Pos) //!< 0x00000020 
.equ CAN_F0R1_FB5, CAN_F0R1_FB5_Msk //!<Filter bit 5 
.equ CAN_F0R1_FB6_Pos, (6) 
.equ CAN_F0R1_FB6_Msk, (0x1 << CAN_F0R1_FB6_Pos) //!< 0x00000040 
.equ CAN_F0R1_FB6, CAN_F0R1_FB6_Msk //!<Filter bit 6 
.equ CAN_F0R1_FB7_Pos, (7) 
.equ CAN_F0R1_FB7_Msk, (0x1 << CAN_F0R1_FB7_Pos) //!< 0x00000080 
.equ CAN_F0R1_FB7, CAN_F0R1_FB7_Msk //!<Filter bit 7 
.equ CAN_F0R1_FB8_Pos, (8) 
.equ CAN_F0R1_FB8_Msk, (0x1 << CAN_F0R1_FB8_Pos) //!< 0x00000100 
.equ CAN_F0R1_FB8, CAN_F0R1_FB8_Msk //!<Filter bit 8 
.equ CAN_F0R1_FB9_Pos, (9) 
.equ CAN_F0R1_FB9_Msk, (0x1 << CAN_F0R1_FB9_Pos) //!< 0x00000200 
.equ CAN_F0R1_FB9, CAN_F0R1_FB9_Msk //!<Filter bit 9 
.equ CAN_F0R1_FB10_Pos, (10) 
.equ CAN_F0R1_FB10_Msk, (0x1 << CAN_F0R1_FB10_Pos) //!< 0x00000400 
.equ CAN_F0R1_FB10, CAN_F0R1_FB10_Msk //!<Filter bit 10 
.equ CAN_F0R1_FB11_Pos, (11) 
.equ CAN_F0R1_FB11_Msk, (0x1 << CAN_F0R1_FB11_Pos) //!< 0x00000800 
.equ CAN_F0R1_FB11, CAN_F0R1_FB11_Msk //!<Filter bit 11 
.equ CAN_F0R1_FB12_Pos, (12) 
.equ CAN_F0R1_FB12_Msk, (0x1 << CAN_F0R1_FB12_Pos) //!< 0x00001000 
.equ CAN_F0R1_FB12, CAN_F0R1_FB12_Msk //!<Filter bit 12 
.equ CAN_F0R1_FB13_Pos, (13) 
.equ CAN_F0R1_FB13_Msk, (0x1 << CAN_F0R1_FB13_Pos) //!< 0x00002000 
.equ CAN_F0R1_FB13, CAN_F0R1_FB13_Msk //!<Filter bit 13 
.equ CAN_F0R1_FB14_Pos, (14) 
.equ CAN_F0R1_FB14_Msk, (0x1 << CAN_F0R1_FB14_Pos) //!< 0x00004000 
.equ CAN_F0R1_FB14, CAN_F0R1_FB14_Msk //!<Filter bit 14 
.equ CAN_F0R1_FB15_Pos, (15) 
.equ CAN_F0R1_FB15_Msk, (0x1 << CAN_F0R1_FB15_Pos) //!< 0x00008000 
.equ CAN_F0R1_FB15, CAN_F0R1_FB15_Msk //!<Filter bit 15 
.equ CAN_F0R1_FB16_Pos, (16) 
.equ CAN_F0R1_FB16_Msk, (0x1 << CAN_F0R1_FB16_Pos) //!< 0x00010000 
.equ CAN_F0R1_FB16, CAN_F0R1_FB16_Msk //!<Filter bit 16 
.equ CAN_F0R1_FB17_Pos, (17) 
.equ CAN_F0R1_FB17_Msk, (0x1 << CAN_F0R1_FB17_Pos) //!< 0x00020000 
.equ CAN_F0R1_FB17, CAN_F0R1_FB17_Msk //!<Filter bit 17 
.equ CAN_F0R1_FB18_Pos, (18) 
.equ CAN_F0R1_FB18_Msk, (0x1 << CAN_F0R1_FB18_Pos) //!< 0x00040000 
.equ CAN_F0R1_FB18, CAN_F0R1_FB18_Msk //!<Filter bit 18 
.equ CAN_F0R1_FB19_Pos, (19) 
.equ CAN_F0R1_FB19_Msk, (0x1 << CAN_F0R1_FB19_Pos) //!< 0x00080000 
.equ CAN_F0R1_FB19, CAN_F0R1_FB19_Msk //!<Filter bit 19 
.equ CAN_F0R1_FB20_Pos, (20) 
.equ CAN_F0R1_FB20_Msk, (0x1 << CAN_F0R1_FB20_Pos) //!< 0x00100000 
.equ CAN_F0R1_FB20, CAN_F0R1_FB20_Msk //!<Filter bit 20 
.equ CAN_F0R1_FB21_Pos, (21) 
.equ CAN_F0R1_FB21_Msk, (0x1 << CAN_F0R1_FB21_Pos) //!< 0x00200000 
.equ CAN_F0R1_FB21, CAN_F0R1_FB21_Msk //!<Filter bit 21 
.equ CAN_F0R1_FB22_Pos, (22) 
.equ CAN_F0R1_FB22_Msk, (0x1 << CAN_F0R1_FB22_Pos) //!< 0x00400000 
.equ CAN_F0R1_FB22, CAN_F0R1_FB22_Msk //!<Filter bit 22 
.equ CAN_F0R1_FB23_Pos, (23) 
.equ CAN_F0R1_FB23_Msk, (0x1 << CAN_F0R1_FB23_Pos) //!< 0x00800000 
.equ CAN_F0R1_FB23, CAN_F0R1_FB23_Msk //!<Filter bit 23 
.equ CAN_F0R1_FB24_Pos, (24) 
.equ CAN_F0R1_FB24_Msk, (0x1 << CAN_F0R1_FB24_Pos) //!< 0x01000000 
.equ CAN_F0R1_FB24, CAN_F0R1_FB24_Msk //!<Filter bit 24 
.equ CAN_F0R1_FB25_Pos, (25) 
.equ CAN_F0R1_FB25_Msk, (0x1 << CAN_F0R1_FB25_Pos) //!< 0x02000000 
.equ CAN_F0R1_FB25, CAN_F0R1_FB25_Msk //!<Filter bit 25 
.equ CAN_F0R1_FB26_Pos, (26) 
.equ CAN_F0R1_FB26_Msk, (0x1 << CAN_F0R1_FB26_Pos) //!< 0x04000000 
.equ CAN_F0R1_FB26, CAN_F0R1_FB26_Msk //!<Filter bit 26 
.equ CAN_F0R1_FB27_Pos, (27) 
.equ CAN_F0R1_FB27_Msk, (0x1 << CAN_F0R1_FB27_Pos) //!< 0x08000000 
.equ CAN_F0R1_FB27, CAN_F0R1_FB27_Msk //!<Filter bit 27 
.equ CAN_F0R1_FB28_Pos, (28) 
.equ CAN_F0R1_FB28_Msk, (0x1 << CAN_F0R1_FB28_Pos) //!< 0x10000000 
.equ CAN_F0R1_FB28, CAN_F0R1_FB28_Msk //!<Filter bit 28 
.equ CAN_F0R1_FB29_Pos, (29) 
.equ CAN_F0R1_FB29_Msk, (0x1 << CAN_F0R1_FB29_Pos) //!< 0x20000000 
.equ CAN_F0R1_FB29, CAN_F0R1_FB29_Msk //!<Filter bit 29 
.equ CAN_F0R1_FB30_Pos, (30) 
.equ CAN_F0R1_FB30_Msk, (0x1 << CAN_F0R1_FB30_Pos) //!< 0x40000000 
.equ CAN_F0R1_FB30, CAN_F0R1_FB30_Msk //!<Filter bit 30 
.equ CAN_F0R1_FB31_Pos, (31) 
.equ CAN_F0R1_FB31_Msk, (0x1 << CAN_F0R1_FB31_Pos) //!< 0x80000000 
.equ CAN_F0R1_FB31, CAN_F0R1_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F1R1 register  ******************
.equ CAN_F1R1_FB0_Pos, (0) 
.equ CAN_F1R1_FB0_Msk, (0x1 << CAN_F1R1_FB0_Pos) //!< 0x00000001 
.equ CAN_F1R1_FB0, CAN_F1R1_FB0_Msk //!<Filter bit 0 
.equ CAN_F1R1_FB1_Pos, (1) 
.equ CAN_F1R1_FB1_Msk, (0x1 << CAN_F1R1_FB1_Pos) //!< 0x00000002 
.equ CAN_F1R1_FB1, CAN_F1R1_FB1_Msk //!<Filter bit 1 
.equ CAN_F1R1_FB2_Pos, (2) 
.equ CAN_F1R1_FB2_Msk, (0x1 << CAN_F1R1_FB2_Pos) //!< 0x00000004 
.equ CAN_F1R1_FB2, CAN_F1R1_FB2_Msk //!<Filter bit 2 
.equ CAN_F1R1_FB3_Pos, (3) 
.equ CAN_F1R1_FB3_Msk, (0x1 << CAN_F1R1_FB3_Pos) //!< 0x00000008 
.equ CAN_F1R1_FB3, CAN_F1R1_FB3_Msk //!<Filter bit 3 
.equ CAN_F1R1_FB4_Pos, (4) 
.equ CAN_F1R1_FB4_Msk, (0x1 << CAN_F1R1_FB4_Pos) //!< 0x00000010 
.equ CAN_F1R1_FB4, CAN_F1R1_FB4_Msk //!<Filter bit 4 
.equ CAN_F1R1_FB5_Pos, (5) 
.equ CAN_F1R1_FB5_Msk, (0x1 << CAN_F1R1_FB5_Pos) //!< 0x00000020 
.equ CAN_F1R1_FB5, CAN_F1R1_FB5_Msk //!<Filter bit 5 
.equ CAN_F1R1_FB6_Pos, (6) 
.equ CAN_F1R1_FB6_Msk, (0x1 << CAN_F1R1_FB6_Pos) //!< 0x00000040 
.equ CAN_F1R1_FB6, CAN_F1R1_FB6_Msk //!<Filter bit 6 
.equ CAN_F1R1_FB7_Pos, (7) 
.equ CAN_F1R1_FB7_Msk, (0x1 << CAN_F1R1_FB7_Pos) //!< 0x00000080 
.equ CAN_F1R1_FB7, CAN_F1R1_FB7_Msk //!<Filter bit 7 
.equ CAN_F1R1_FB8_Pos, (8) 
.equ CAN_F1R1_FB8_Msk, (0x1 << CAN_F1R1_FB8_Pos) //!< 0x00000100 
.equ CAN_F1R1_FB8, CAN_F1R1_FB8_Msk //!<Filter bit 8 
.equ CAN_F1R1_FB9_Pos, (9) 
.equ CAN_F1R1_FB9_Msk, (0x1 << CAN_F1R1_FB9_Pos) //!< 0x00000200 
.equ CAN_F1R1_FB9, CAN_F1R1_FB9_Msk //!<Filter bit 9 
.equ CAN_F1R1_FB10_Pos, (10) 
.equ CAN_F1R1_FB10_Msk, (0x1 << CAN_F1R1_FB10_Pos) //!< 0x00000400 
.equ CAN_F1R1_FB10, CAN_F1R1_FB10_Msk //!<Filter bit 10 
.equ CAN_F1R1_FB11_Pos, (11) 
.equ CAN_F1R1_FB11_Msk, (0x1 << CAN_F1R1_FB11_Pos) //!< 0x00000800 
.equ CAN_F1R1_FB11, CAN_F1R1_FB11_Msk //!<Filter bit 11 
.equ CAN_F1R1_FB12_Pos, (12) 
.equ CAN_F1R1_FB12_Msk, (0x1 << CAN_F1R1_FB12_Pos) //!< 0x00001000 
.equ CAN_F1R1_FB12, CAN_F1R1_FB12_Msk //!<Filter bit 12 
.equ CAN_F1R1_FB13_Pos, (13) 
.equ CAN_F1R1_FB13_Msk, (0x1 << CAN_F1R1_FB13_Pos) //!< 0x00002000 
.equ CAN_F1R1_FB13, CAN_F1R1_FB13_Msk //!<Filter bit 13 
.equ CAN_F1R1_FB14_Pos, (14) 
.equ CAN_F1R1_FB14_Msk, (0x1 << CAN_F1R1_FB14_Pos) //!< 0x00004000 
.equ CAN_F1R1_FB14, CAN_F1R1_FB14_Msk //!<Filter bit 14 
.equ CAN_F1R1_FB15_Pos, (15) 
.equ CAN_F1R1_FB15_Msk, (0x1 << CAN_F1R1_FB15_Pos) //!< 0x00008000 
.equ CAN_F1R1_FB15, CAN_F1R1_FB15_Msk //!<Filter bit 15 
.equ CAN_F1R1_FB16_Pos, (16) 
.equ CAN_F1R1_FB16_Msk, (0x1 << CAN_F1R1_FB16_Pos) //!< 0x00010000 
.equ CAN_F1R1_FB16, CAN_F1R1_FB16_Msk //!<Filter bit 16 
.equ CAN_F1R1_FB17_Pos, (17) 
.equ CAN_F1R1_FB17_Msk, (0x1 << CAN_F1R1_FB17_Pos) //!< 0x00020000 
.equ CAN_F1R1_FB17, CAN_F1R1_FB17_Msk //!<Filter bit 17 
.equ CAN_F1R1_FB18_Pos, (18) 
.equ CAN_F1R1_FB18_Msk, (0x1 << CAN_F1R1_FB18_Pos) //!< 0x00040000 
.equ CAN_F1R1_FB18, CAN_F1R1_FB18_Msk //!<Filter bit 18 
.equ CAN_F1R1_FB19_Pos, (19) 
.equ CAN_F1R1_FB19_Msk, (0x1 << CAN_F1R1_FB19_Pos) //!< 0x00080000 
.equ CAN_F1R1_FB19, CAN_F1R1_FB19_Msk //!<Filter bit 19 
.equ CAN_F1R1_FB20_Pos, (20) 
.equ CAN_F1R1_FB20_Msk, (0x1 << CAN_F1R1_FB20_Pos) //!< 0x00100000 
.equ CAN_F1R1_FB20, CAN_F1R1_FB20_Msk //!<Filter bit 20 
.equ CAN_F1R1_FB21_Pos, (21) 
.equ CAN_F1R1_FB21_Msk, (0x1 << CAN_F1R1_FB21_Pos) //!< 0x00200000 
.equ CAN_F1R1_FB21, CAN_F1R1_FB21_Msk //!<Filter bit 21 
.equ CAN_F1R1_FB22_Pos, (22) 
.equ CAN_F1R1_FB22_Msk, (0x1 << CAN_F1R1_FB22_Pos) //!< 0x00400000 
.equ CAN_F1R1_FB22, CAN_F1R1_FB22_Msk //!<Filter bit 22 
.equ CAN_F1R1_FB23_Pos, (23) 
.equ CAN_F1R1_FB23_Msk, (0x1 << CAN_F1R1_FB23_Pos) //!< 0x00800000 
.equ CAN_F1R1_FB23, CAN_F1R1_FB23_Msk //!<Filter bit 23 
.equ CAN_F1R1_FB24_Pos, (24) 
.equ CAN_F1R1_FB24_Msk, (0x1 << CAN_F1R1_FB24_Pos) //!< 0x01000000 
.equ CAN_F1R1_FB24, CAN_F1R1_FB24_Msk //!<Filter bit 24 
.equ CAN_F1R1_FB25_Pos, (25) 
.equ CAN_F1R1_FB25_Msk, (0x1 << CAN_F1R1_FB25_Pos) //!< 0x02000000 
.equ CAN_F1R1_FB25, CAN_F1R1_FB25_Msk //!<Filter bit 25 
.equ CAN_F1R1_FB26_Pos, (26) 
.equ CAN_F1R1_FB26_Msk, (0x1 << CAN_F1R1_FB26_Pos) //!< 0x04000000 
.equ CAN_F1R1_FB26, CAN_F1R1_FB26_Msk //!<Filter bit 26 
.equ CAN_F1R1_FB27_Pos, (27) 
.equ CAN_F1R1_FB27_Msk, (0x1 << CAN_F1R1_FB27_Pos) //!< 0x08000000 
.equ CAN_F1R1_FB27, CAN_F1R1_FB27_Msk //!<Filter bit 27 
.equ CAN_F1R1_FB28_Pos, (28) 
.equ CAN_F1R1_FB28_Msk, (0x1 << CAN_F1R1_FB28_Pos) //!< 0x10000000 
.equ CAN_F1R1_FB28, CAN_F1R1_FB28_Msk //!<Filter bit 28 
.equ CAN_F1R1_FB29_Pos, (29) 
.equ CAN_F1R1_FB29_Msk, (0x1 << CAN_F1R1_FB29_Pos) //!< 0x20000000 
.equ CAN_F1R1_FB29, CAN_F1R1_FB29_Msk //!<Filter bit 29 
.equ CAN_F1R1_FB30_Pos, (30) 
.equ CAN_F1R1_FB30_Msk, (0x1 << CAN_F1R1_FB30_Pos) //!< 0x40000000 
.equ CAN_F1R1_FB30, CAN_F1R1_FB30_Msk //!<Filter bit 30 
.equ CAN_F1R1_FB31_Pos, (31) 
.equ CAN_F1R1_FB31_Msk, (0x1 << CAN_F1R1_FB31_Pos) //!< 0x80000000 
.equ CAN_F1R1_FB31, CAN_F1R1_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F2R1 register  ******************
.equ CAN_F2R1_FB0_Pos, (0) 
.equ CAN_F2R1_FB0_Msk, (0x1 << CAN_F2R1_FB0_Pos) //!< 0x00000001 
.equ CAN_F2R1_FB0, CAN_F2R1_FB0_Msk //!<Filter bit 0 
.equ CAN_F2R1_FB1_Pos, (1) 
.equ CAN_F2R1_FB1_Msk, (0x1 << CAN_F2R1_FB1_Pos) //!< 0x00000002 
.equ CAN_F2R1_FB1, CAN_F2R1_FB1_Msk //!<Filter bit 1 
.equ CAN_F2R1_FB2_Pos, (2) 
.equ CAN_F2R1_FB2_Msk, (0x1 << CAN_F2R1_FB2_Pos) //!< 0x00000004 
.equ CAN_F2R1_FB2, CAN_F2R1_FB2_Msk //!<Filter bit 2 
.equ CAN_F2R1_FB3_Pos, (3) 
.equ CAN_F2R1_FB3_Msk, (0x1 << CAN_F2R1_FB3_Pos) //!< 0x00000008 
.equ CAN_F2R1_FB3, CAN_F2R1_FB3_Msk //!<Filter bit 3 
.equ CAN_F2R1_FB4_Pos, (4) 
.equ CAN_F2R1_FB4_Msk, (0x1 << CAN_F2R1_FB4_Pos) //!< 0x00000010 
.equ CAN_F2R1_FB4, CAN_F2R1_FB4_Msk //!<Filter bit 4 
.equ CAN_F2R1_FB5_Pos, (5) 
.equ CAN_F2R1_FB5_Msk, (0x1 << CAN_F2R1_FB5_Pos) //!< 0x00000020 
.equ CAN_F2R1_FB5, CAN_F2R1_FB5_Msk //!<Filter bit 5 
.equ CAN_F2R1_FB6_Pos, (6) 
.equ CAN_F2R1_FB6_Msk, (0x1 << CAN_F2R1_FB6_Pos) //!< 0x00000040 
.equ CAN_F2R1_FB6, CAN_F2R1_FB6_Msk //!<Filter bit 6 
.equ CAN_F2R1_FB7_Pos, (7) 
.equ CAN_F2R1_FB7_Msk, (0x1 << CAN_F2R1_FB7_Pos) //!< 0x00000080 
.equ CAN_F2R1_FB7, CAN_F2R1_FB7_Msk //!<Filter bit 7 
.equ CAN_F2R1_FB8_Pos, (8) 
.equ CAN_F2R1_FB8_Msk, (0x1 << CAN_F2R1_FB8_Pos) //!< 0x00000100 
.equ CAN_F2R1_FB8, CAN_F2R1_FB8_Msk //!<Filter bit 8 
.equ CAN_F2R1_FB9_Pos, (9) 
.equ CAN_F2R1_FB9_Msk, (0x1 << CAN_F2R1_FB9_Pos) //!< 0x00000200 
.equ CAN_F2R1_FB9, CAN_F2R1_FB9_Msk //!<Filter bit 9 
.equ CAN_F2R1_FB10_Pos, (10) 
.equ CAN_F2R1_FB10_Msk, (0x1 << CAN_F2R1_FB10_Pos) //!< 0x00000400 
.equ CAN_F2R1_FB10, CAN_F2R1_FB10_Msk //!<Filter bit 10 
.equ CAN_F2R1_FB11_Pos, (11) 
.equ CAN_F2R1_FB11_Msk, (0x1 << CAN_F2R1_FB11_Pos) //!< 0x00000800 
.equ CAN_F2R1_FB11, CAN_F2R1_FB11_Msk //!<Filter bit 11 
.equ CAN_F2R1_FB12_Pos, (12) 
.equ CAN_F2R1_FB12_Msk, (0x1 << CAN_F2R1_FB12_Pos) //!< 0x00001000 
.equ CAN_F2R1_FB12, CAN_F2R1_FB12_Msk //!<Filter bit 12 
.equ CAN_F2R1_FB13_Pos, (13) 
.equ CAN_F2R1_FB13_Msk, (0x1 << CAN_F2R1_FB13_Pos) //!< 0x00002000 
.equ CAN_F2R1_FB13, CAN_F2R1_FB13_Msk //!<Filter bit 13 
.equ CAN_F2R1_FB14_Pos, (14) 
.equ CAN_F2R1_FB14_Msk, (0x1 << CAN_F2R1_FB14_Pos) //!< 0x00004000 
.equ CAN_F2R1_FB14, CAN_F2R1_FB14_Msk //!<Filter bit 14 
.equ CAN_F2R1_FB15_Pos, (15) 
.equ CAN_F2R1_FB15_Msk, (0x1 << CAN_F2R1_FB15_Pos) //!< 0x00008000 
.equ CAN_F2R1_FB15, CAN_F2R1_FB15_Msk //!<Filter bit 15 
.equ CAN_F2R1_FB16_Pos, (16) 
.equ CAN_F2R1_FB16_Msk, (0x1 << CAN_F2R1_FB16_Pos) //!< 0x00010000 
.equ CAN_F2R1_FB16, CAN_F2R1_FB16_Msk //!<Filter bit 16 
.equ CAN_F2R1_FB17_Pos, (17) 
.equ CAN_F2R1_FB17_Msk, (0x1 << CAN_F2R1_FB17_Pos) //!< 0x00020000 
.equ CAN_F2R1_FB17, CAN_F2R1_FB17_Msk //!<Filter bit 17 
.equ CAN_F2R1_FB18_Pos, (18) 
.equ CAN_F2R1_FB18_Msk, (0x1 << CAN_F2R1_FB18_Pos) //!< 0x00040000 
.equ CAN_F2R1_FB18, CAN_F2R1_FB18_Msk //!<Filter bit 18 
.equ CAN_F2R1_FB19_Pos, (19) 
.equ CAN_F2R1_FB19_Msk, (0x1 << CAN_F2R1_FB19_Pos) //!< 0x00080000 
.equ CAN_F2R1_FB19, CAN_F2R1_FB19_Msk //!<Filter bit 19 
.equ CAN_F2R1_FB20_Pos, (20) 
.equ CAN_F2R1_FB20_Msk, (0x1 << CAN_F2R1_FB20_Pos) //!< 0x00100000 
.equ CAN_F2R1_FB20, CAN_F2R1_FB20_Msk //!<Filter bit 20 
.equ CAN_F2R1_FB21_Pos, (21) 
.equ CAN_F2R1_FB21_Msk, (0x1 << CAN_F2R1_FB21_Pos) //!< 0x00200000 
.equ CAN_F2R1_FB21, CAN_F2R1_FB21_Msk //!<Filter bit 21 
.equ CAN_F2R1_FB22_Pos, (22) 
.equ CAN_F2R1_FB22_Msk, (0x1 << CAN_F2R1_FB22_Pos) //!< 0x00400000 
.equ CAN_F2R1_FB22, CAN_F2R1_FB22_Msk //!<Filter bit 22 
.equ CAN_F2R1_FB23_Pos, (23) 
.equ CAN_F2R1_FB23_Msk, (0x1 << CAN_F2R1_FB23_Pos) //!< 0x00800000 
.equ CAN_F2R1_FB23, CAN_F2R1_FB23_Msk //!<Filter bit 23 
.equ CAN_F2R1_FB24_Pos, (24) 
.equ CAN_F2R1_FB24_Msk, (0x1 << CAN_F2R1_FB24_Pos) //!< 0x01000000 
.equ CAN_F2R1_FB24, CAN_F2R1_FB24_Msk //!<Filter bit 24 
.equ CAN_F2R1_FB25_Pos, (25) 
.equ CAN_F2R1_FB25_Msk, (0x1 << CAN_F2R1_FB25_Pos) //!< 0x02000000 
.equ CAN_F2R1_FB25, CAN_F2R1_FB25_Msk //!<Filter bit 25 
.equ CAN_F2R1_FB26_Pos, (26) 
.equ CAN_F2R1_FB26_Msk, (0x1 << CAN_F2R1_FB26_Pos) //!< 0x04000000 
.equ CAN_F2R1_FB26, CAN_F2R1_FB26_Msk //!<Filter bit 26 
.equ CAN_F2R1_FB27_Pos, (27) 
.equ CAN_F2R1_FB27_Msk, (0x1 << CAN_F2R1_FB27_Pos) //!< 0x08000000 
.equ CAN_F2R1_FB27, CAN_F2R1_FB27_Msk //!<Filter bit 27 
.equ CAN_F2R1_FB28_Pos, (28) 
.equ CAN_F2R1_FB28_Msk, (0x1 << CAN_F2R1_FB28_Pos) //!< 0x10000000 
.equ CAN_F2R1_FB28, CAN_F2R1_FB28_Msk //!<Filter bit 28 
.equ CAN_F2R1_FB29_Pos, (29) 
.equ CAN_F2R1_FB29_Msk, (0x1 << CAN_F2R1_FB29_Pos) //!< 0x20000000 
.equ CAN_F2R1_FB29, CAN_F2R1_FB29_Msk //!<Filter bit 29 
.equ CAN_F2R1_FB30_Pos, (30) 
.equ CAN_F2R1_FB30_Msk, (0x1 << CAN_F2R1_FB30_Pos) //!< 0x40000000 
.equ CAN_F2R1_FB30, CAN_F2R1_FB30_Msk //!<Filter bit 30 
.equ CAN_F2R1_FB31_Pos, (31) 
.equ CAN_F2R1_FB31_Msk, (0x1 << CAN_F2R1_FB31_Pos) //!< 0x80000000 
.equ CAN_F2R1_FB31, CAN_F2R1_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F3R1 register  ******************
.equ CAN_F3R1_FB0_Pos, (0) 
.equ CAN_F3R1_FB0_Msk, (0x1 << CAN_F3R1_FB0_Pos) //!< 0x00000001 
.equ CAN_F3R1_FB0, CAN_F3R1_FB0_Msk //!<Filter bit 0 
.equ CAN_F3R1_FB1_Pos, (1) 
.equ CAN_F3R1_FB1_Msk, (0x1 << CAN_F3R1_FB1_Pos) //!< 0x00000002 
.equ CAN_F3R1_FB1, CAN_F3R1_FB1_Msk //!<Filter bit 1 
.equ CAN_F3R1_FB2_Pos, (2) 
.equ CAN_F3R1_FB2_Msk, (0x1 << CAN_F3R1_FB2_Pos) //!< 0x00000004 
.equ CAN_F3R1_FB2, CAN_F3R1_FB2_Msk //!<Filter bit 2 
.equ CAN_F3R1_FB3_Pos, (3) 
.equ CAN_F3R1_FB3_Msk, (0x1 << CAN_F3R1_FB3_Pos) //!< 0x00000008 
.equ CAN_F3R1_FB3, CAN_F3R1_FB3_Msk //!<Filter bit 3 
.equ CAN_F3R1_FB4_Pos, (4) 
.equ CAN_F3R1_FB4_Msk, (0x1 << CAN_F3R1_FB4_Pos) //!< 0x00000010 
.equ CAN_F3R1_FB4, CAN_F3R1_FB4_Msk //!<Filter bit 4 
.equ CAN_F3R1_FB5_Pos, (5) 
.equ CAN_F3R1_FB5_Msk, (0x1 << CAN_F3R1_FB5_Pos) //!< 0x00000020 
.equ CAN_F3R1_FB5, CAN_F3R1_FB5_Msk //!<Filter bit 5 
.equ CAN_F3R1_FB6_Pos, (6) 
.equ CAN_F3R1_FB6_Msk, (0x1 << CAN_F3R1_FB6_Pos) //!< 0x00000040 
.equ CAN_F3R1_FB6, CAN_F3R1_FB6_Msk //!<Filter bit 6 
.equ CAN_F3R1_FB7_Pos, (7) 
.equ CAN_F3R1_FB7_Msk, (0x1 << CAN_F3R1_FB7_Pos) //!< 0x00000080 
.equ CAN_F3R1_FB7, CAN_F3R1_FB7_Msk //!<Filter bit 7 
.equ CAN_F3R1_FB8_Pos, (8) 
.equ CAN_F3R1_FB8_Msk, (0x1 << CAN_F3R1_FB8_Pos) //!< 0x00000100 
.equ CAN_F3R1_FB8, CAN_F3R1_FB8_Msk //!<Filter bit 8 
.equ CAN_F3R1_FB9_Pos, (9) 
.equ CAN_F3R1_FB9_Msk, (0x1 << CAN_F3R1_FB9_Pos) //!< 0x00000200 
.equ CAN_F3R1_FB9, CAN_F3R1_FB9_Msk //!<Filter bit 9 
.equ CAN_F3R1_FB10_Pos, (10) 
.equ CAN_F3R1_FB10_Msk, (0x1 << CAN_F3R1_FB10_Pos) //!< 0x00000400 
.equ CAN_F3R1_FB10, CAN_F3R1_FB10_Msk //!<Filter bit 10 
.equ CAN_F3R1_FB11_Pos, (11) 
.equ CAN_F3R1_FB11_Msk, (0x1 << CAN_F3R1_FB11_Pos) //!< 0x00000800 
.equ CAN_F3R1_FB11, CAN_F3R1_FB11_Msk //!<Filter bit 11 
.equ CAN_F3R1_FB12_Pos, (12) 
.equ CAN_F3R1_FB12_Msk, (0x1 << CAN_F3R1_FB12_Pos) //!< 0x00001000 
.equ CAN_F3R1_FB12, CAN_F3R1_FB12_Msk //!<Filter bit 12 
.equ CAN_F3R1_FB13_Pos, (13) 
.equ CAN_F3R1_FB13_Msk, (0x1 << CAN_F3R1_FB13_Pos) //!< 0x00002000 
.equ CAN_F3R1_FB13, CAN_F3R1_FB13_Msk //!<Filter bit 13 
.equ CAN_F3R1_FB14_Pos, (14) 
.equ CAN_F3R1_FB14_Msk, (0x1 << CAN_F3R1_FB14_Pos) //!< 0x00004000 
.equ CAN_F3R1_FB14, CAN_F3R1_FB14_Msk //!<Filter bit 14 
.equ CAN_F3R1_FB15_Pos, (15) 
.equ CAN_F3R1_FB15_Msk, (0x1 << CAN_F3R1_FB15_Pos) //!< 0x00008000 
.equ CAN_F3R1_FB15, CAN_F3R1_FB15_Msk //!<Filter bit 15 
.equ CAN_F3R1_FB16_Pos, (16) 
.equ CAN_F3R1_FB16_Msk, (0x1 << CAN_F3R1_FB16_Pos) //!< 0x00010000 
.equ CAN_F3R1_FB16, CAN_F3R1_FB16_Msk //!<Filter bit 16 
.equ CAN_F3R1_FB17_Pos, (17) 
.equ CAN_F3R1_FB17_Msk, (0x1 << CAN_F3R1_FB17_Pos) //!< 0x00020000 
.equ CAN_F3R1_FB17, CAN_F3R1_FB17_Msk //!<Filter bit 17 
.equ CAN_F3R1_FB18_Pos, (18) 
.equ CAN_F3R1_FB18_Msk, (0x1 << CAN_F3R1_FB18_Pos) //!< 0x00040000 
.equ CAN_F3R1_FB18, CAN_F3R1_FB18_Msk //!<Filter bit 18 
.equ CAN_F3R1_FB19_Pos, (19) 
.equ CAN_F3R1_FB19_Msk, (0x1 << CAN_F3R1_FB19_Pos) //!< 0x00080000 
.equ CAN_F3R1_FB19, CAN_F3R1_FB19_Msk //!<Filter bit 19 
.equ CAN_F3R1_FB20_Pos, (20) 
.equ CAN_F3R1_FB20_Msk, (0x1 << CAN_F3R1_FB20_Pos) //!< 0x00100000 
.equ CAN_F3R1_FB20, CAN_F3R1_FB20_Msk //!<Filter bit 20 
.equ CAN_F3R1_FB21_Pos, (21) 
.equ CAN_F3R1_FB21_Msk, (0x1 << CAN_F3R1_FB21_Pos) //!< 0x00200000 
.equ CAN_F3R1_FB21, CAN_F3R1_FB21_Msk //!<Filter bit 21 
.equ CAN_F3R1_FB22_Pos, (22) 
.equ CAN_F3R1_FB22_Msk, (0x1 << CAN_F3R1_FB22_Pos) //!< 0x00400000 
.equ CAN_F3R1_FB22, CAN_F3R1_FB22_Msk //!<Filter bit 22 
.equ CAN_F3R1_FB23_Pos, (23) 
.equ CAN_F3R1_FB23_Msk, (0x1 << CAN_F3R1_FB23_Pos) //!< 0x00800000 
.equ CAN_F3R1_FB23, CAN_F3R1_FB23_Msk //!<Filter bit 23 
.equ CAN_F3R1_FB24_Pos, (24) 
.equ CAN_F3R1_FB24_Msk, (0x1 << CAN_F3R1_FB24_Pos) //!< 0x01000000 
.equ CAN_F3R1_FB24, CAN_F3R1_FB24_Msk //!<Filter bit 24 
.equ CAN_F3R1_FB25_Pos, (25) 
.equ CAN_F3R1_FB25_Msk, (0x1 << CAN_F3R1_FB25_Pos) //!< 0x02000000 
.equ CAN_F3R1_FB25, CAN_F3R1_FB25_Msk //!<Filter bit 25 
.equ CAN_F3R1_FB26_Pos, (26) 
.equ CAN_F3R1_FB26_Msk, (0x1 << CAN_F3R1_FB26_Pos) //!< 0x04000000 
.equ CAN_F3R1_FB26, CAN_F3R1_FB26_Msk //!<Filter bit 26 
.equ CAN_F3R1_FB27_Pos, (27) 
.equ CAN_F3R1_FB27_Msk, (0x1 << CAN_F3R1_FB27_Pos) //!< 0x08000000 
.equ CAN_F3R1_FB27, CAN_F3R1_FB27_Msk //!<Filter bit 27 
.equ CAN_F3R1_FB28_Pos, (28) 
.equ CAN_F3R1_FB28_Msk, (0x1 << CAN_F3R1_FB28_Pos) //!< 0x10000000 
.equ CAN_F3R1_FB28, CAN_F3R1_FB28_Msk //!<Filter bit 28 
.equ CAN_F3R1_FB29_Pos, (29) 
.equ CAN_F3R1_FB29_Msk, (0x1 << CAN_F3R1_FB29_Pos) //!< 0x20000000 
.equ CAN_F3R1_FB29, CAN_F3R1_FB29_Msk //!<Filter bit 29 
.equ CAN_F3R1_FB30_Pos, (30) 
.equ CAN_F3R1_FB30_Msk, (0x1 << CAN_F3R1_FB30_Pos) //!< 0x40000000 
.equ CAN_F3R1_FB30, CAN_F3R1_FB30_Msk //!<Filter bit 30 
.equ CAN_F3R1_FB31_Pos, (31) 
.equ CAN_F3R1_FB31_Msk, (0x1 << CAN_F3R1_FB31_Pos) //!< 0x80000000 
.equ CAN_F3R1_FB31, CAN_F3R1_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F4R1 register  ******************
.equ CAN_F4R1_FB0_Pos, (0) 
.equ CAN_F4R1_FB0_Msk, (0x1 << CAN_F4R1_FB0_Pos) //!< 0x00000001 
.equ CAN_F4R1_FB0, CAN_F4R1_FB0_Msk //!<Filter bit 0 
.equ CAN_F4R1_FB1_Pos, (1) 
.equ CAN_F4R1_FB1_Msk, (0x1 << CAN_F4R1_FB1_Pos) //!< 0x00000002 
.equ CAN_F4R1_FB1, CAN_F4R1_FB1_Msk //!<Filter bit 1 
.equ CAN_F4R1_FB2_Pos, (2) 
.equ CAN_F4R1_FB2_Msk, (0x1 << CAN_F4R1_FB2_Pos) //!< 0x00000004 
.equ CAN_F4R1_FB2, CAN_F4R1_FB2_Msk //!<Filter bit 2 
.equ CAN_F4R1_FB3_Pos, (3) 
.equ CAN_F4R1_FB3_Msk, (0x1 << CAN_F4R1_FB3_Pos) //!< 0x00000008 
.equ CAN_F4R1_FB3, CAN_F4R1_FB3_Msk //!<Filter bit 3 
.equ CAN_F4R1_FB4_Pos, (4) 
.equ CAN_F4R1_FB4_Msk, (0x1 << CAN_F4R1_FB4_Pos) //!< 0x00000010 
.equ CAN_F4R1_FB4, CAN_F4R1_FB4_Msk //!<Filter bit 4 
.equ CAN_F4R1_FB5_Pos, (5) 
.equ CAN_F4R1_FB5_Msk, (0x1 << CAN_F4R1_FB5_Pos) //!< 0x00000020 
.equ CAN_F4R1_FB5, CAN_F4R1_FB5_Msk //!<Filter bit 5 
.equ CAN_F4R1_FB6_Pos, (6) 
.equ CAN_F4R1_FB6_Msk, (0x1 << CAN_F4R1_FB6_Pos) //!< 0x00000040 
.equ CAN_F4R1_FB6, CAN_F4R1_FB6_Msk //!<Filter bit 6 
.equ CAN_F4R1_FB7_Pos, (7) 
.equ CAN_F4R1_FB7_Msk, (0x1 << CAN_F4R1_FB7_Pos) //!< 0x00000080 
.equ CAN_F4R1_FB7, CAN_F4R1_FB7_Msk //!<Filter bit 7 
.equ CAN_F4R1_FB8_Pos, (8) 
.equ CAN_F4R1_FB8_Msk, (0x1 << CAN_F4R1_FB8_Pos) //!< 0x00000100 
.equ CAN_F4R1_FB8, CAN_F4R1_FB8_Msk //!<Filter bit 8 
.equ CAN_F4R1_FB9_Pos, (9) 
.equ CAN_F4R1_FB9_Msk, (0x1 << CAN_F4R1_FB9_Pos) //!< 0x00000200 
.equ CAN_F4R1_FB9, CAN_F4R1_FB9_Msk //!<Filter bit 9 
.equ CAN_F4R1_FB10_Pos, (10) 
.equ CAN_F4R1_FB10_Msk, (0x1 << CAN_F4R1_FB10_Pos) //!< 0x00000400 
.equ CAN_F4R1_FB10, CAN_F4R1_FB10_Msk //!<Filter bit 10 
.equ CAN_F4R1_FB11_Pos, (11) 
.equ CAN_F4R1_FB11_Msk, (0x1 << CAN_F4R1_FB11_Pos) //!< 0x00000800 
.equ CAN_F4R1_FB11, CAN_F4R1_FB11_Msk //!<Filter bit 11 
.equ CAN_F4R1_FB12_Pos, (12) 
.equ CAN_F4R1_FB12_Msk, (0x1 << CAN_F4R1_FB12_Pos) //!< 0x00001000 
.equ CAN_F4R1_FB12, CAN_F4R1_FB12_Msk //!<Filter bit 12 
.equ CAN_F4R1_FB13_Pos, (13) 
.equ CAN_F4R1_FB13_Msk, (0x1 << CAN_F4R1_FB13_Pos) //!< 0x00002000 
.equ CAN_F4R1_FB13, CAN_F4R1_FB13_Msk //!<Filter bit 13 
.equ CAN_F4R1_FB14_Pos, (14) 
.equ CAN_F4R1_FB14_Msk, (0x1 << CAN_F4R1_FB14_Pos) //!< 0x00004000 
.equ CAN_F4R1_FB14, CAN_F4R1_FB14_Msk //!<Filter bit 14 
.equ CAN_F4R1_FB15_Pos, (15) 
.equ CAN_F4R1_FB15_Msk, (0x1 << CAN_F4R1_FB15_Pos) //!< 0x00008000 
.equ CAN_F4R1_FB15, CAN_F4R1_FB15_Msk //!<Filter bit 15 
.equ CAN_F4R1_FB16_Pos, (16) 
.equ CAN_F4R1_FB16_Msk, (0x1 << CAN_F4R1_FB16_Pos) //!< 0x00010000 
.equ CAN_F4R1_FB16, CAN_F4R1_FB16_Msk //!<Filter bit 16 
.equ CAN_F4R1_FB17_Pos, (17) 
.equ CAN_F4R1_FB17_Msk, (0x1 << CAN_F4R1_FB17_Pos) //!< 0x00020000 
.equ CAN_F4R1_FB17, CAN_F4R1_FB17_Msk //!<Filter bit 17 
.equ CAN_F4R1_FB18_Pos, (18) 
.equ CAN_F4R1_FB18_Msk, (0x1 << CAN_F4R1_FB18_Pos) //!< 0x00040000 
.equ CAN_F4R1_FB18, CAN_F4R1_FB18_Msk //!<Filter bit 18 
.equ CAN_F4R1_FB19_Pos, (19) 
.equ CAN_F4R1_FB19_Msk, (0x1 << CAN_F4R1_FB19_Pos) //!< 0x00080000 
.equ CAN_F4R1_FB19, CAN_F4R1_FB19_Msk //!<Filter bit 19 
.equ CAN_F4R1_FB20_Pos, (20) 
.equ CAN_F4R1_FB20_Msk, (0x1 << CAN_F4R1_FB20_Pos) //!< 0x00100000 
.equ CAN_F4R1_FB20, CAN_F4R1_FB20_Msk //!<Filter bit 20 
.equ CAN_F4R1_FB21_Pos, (21) 
.equ CAN_F4R1_FB21_Msk, (0x1 << CAN_F4R1_FB21_Pos) //!< 0x00200000 
.equ CAN_F4R1_FB21, CAN_F4R1_FB21_Msk //!<Filter bit 21 
.equ CAN_F4R1_FB22_Pos, (22) 
.equ CAN_F4R1_FB22_Msk, (0x1 << CAN_F4R1_FB22_Pos) //!< 0x00400000 
.equ CAN_F4R1_FB22, CAN_F4R1_FB22_Msk //!<Filter bit 22 
.equ CAN_F4R1_FB23_Pos, (23) 
.equ CAN_F4R1_FB23_Msk, (0x1 << CAN_F4R1_FB23_Pos) //!< 0x00800000 
.equ CAN_F4R1_FB23, CAN_F4R1_FB23_Msk //!<Filter bit 23 
.equ CAN_F4R1_FB24_Pos, (24) 
.equ CAN_F4R1_FB24_Msk, (0x1 << CAN_F4R1_FB24_Pos) //!< 0x01000000 
.equ CAN_F4R1_FB24, CAN_F4R1_FB24_Msk //!<Filter bit 24 
.equ CAN_F4R1_FB25_Pos, (25) 
.equ CAN_F4R1_FB25_Msk, (0x1 << CAN_F4R1_FB25_Pos) //!< 0x02000000 
.equ CAN_F4R1_FB25, CAN_F4R1_FB25_Msk //!<Filter bit 25 
.equ CAN_F4R1_FB26_Pos, (26) 
.equ CAN_F4R1_FB26_Msk, (0x1 << CAN_F4R1_FB26_Pos) //!< 0x04000000 
.equ CAN_F4R1_FB26, CAN_F4R1_FB26_Msk //!<Filter bit 26 
.equ CAN_F4R1_FB27_Pos, (27) 
.equ CAN_F4R1_FB27_Msk, (0x1 << CAN_F4R1_FB27_Pos) //!< 0x08000000 
.equ CAN_F4R1_FB27, CAN_F4R1_FB27_Msk //!<Filter bit 27 
.equ CAN_F4R1_FB28_Pos, (28) 
.equ CAN_F4R1_FB28_Msk, (0x1 << CAN_F4R1_FB28_Pos) //!< 0x10000000 
.equ CAN_F4R1_FB28, CAN_F4R1_FB28_Msk //!<Filter bit 28 
.equ CAN_F4R1_FB29_Pos, (29) 
.equ CAN_F4R1_FB29_Msk, (0x1 << CAN_F4R1_FB29_Pos) //!< 0x20000000 
.equ CAN_F4R1_FB29, CAN_F4R1_FB29_Msk //!<Filter bit 29 
.equ CAN_F4R1_FB30_Pos, (30) 
.equ CAN_F4R1_FB30_Msk, (0x1 << CAN_F4R1_FB30_Pos) //!< 0x40000000 
.equ CAN_F4R1_FB30, CAN_F4R1_FB30_Msk //!<Filter bit 30 
.equ CAN_F4R1_FB31_Pos, (31) 
.equ CAN_F4R1_FB31_Msk, (0x1 << CAN_F4R1_FB31_Pos) //!< 0x80000000 
.equ CAN_F4R1_FB31, CAN_F4R1_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F5R1 register  ******************
.equ CAN_F5R1_FB0_Pos, (0) 
.equ CAN_F5R1_FB0_Msk, (0x1 << CAN_F5R1_FB0_Pos) //!< 0x00000001 
.equ CAN_F5R1_FB0, CAN_F5R1_FB0_Msk //!<Filter bit 0 
.equ CAN_F5R1_FB1_Pos, (1) 
.equ CAN_F5R1_FB1_Msk, (0x1 << CAN_F5R1_FB1_Pos) //!< 0x00000002 
.equ CAN_F5R1_FB1, CAN_F5R1_FB1_Msk //!<Filter bit 1 
.equ CAN_F5R1_FB2_Pos, (2) 
.equ CAN_F5R1_FB2_Msk, (0x1 << CAN_F5R1_FB2_Pos) //!< 0x00000004 
.equ CAN_F5R1_FB2, CAN_F5R1_FB2_Msk //!<Filter bit 2 
.equ CAN_F5R1_FB3_Pos, (3) 
.equ CAN_F5R1_FB3_Msk, (0x1 << CAN_F5R1_FB3_Pos) //!< 0x00000008 
.equ CAN_F5R1_FB3, CAN_F5R1_FB3_Msk //!<Filter bit 3 
.equ CAN_F5R1_FB4_Pos, (4) 
.equ CAN_F5R1_FB4_Msk, (0x1 << CAN_F5R1_FB4_Pos) //!< 0x00000010 
.equ CAN_F5R1_FB4, CAN_F5R1_FB4_Msk //!<Filter bit 4 
.equ CAN_F5R1_FB5_Pos, (5) 
.equ CAN_F5R1_FB5_Msk, (0x1 << CAN_F5R1_FB5_Pos) //!< 0x00000020 
.equ CAN_F5R1_FB5, CAN_F5R1_FB5_Msk //!<Filter bit 5 
.equ CAN_F5R1_FB6_Pos, (6) 
.equ CAN_F5R1_FB6_Msk, (0x1 << CAN_F5R1_FB6_Pos) //!< 0x00000040 
.equ CAN_F5R1_FB6, CAN_F5R1_FB6_Msk //!<Filter bit 6 
.equ CAN_F5R1_FB7_Pos, (7) 
.equ CAN_F5R1_FB7_Msk, (0x1 << CAN_F5R1_FB7_Pos) //!< 0x00000080 
.equ CAN_F5R1_FB7, CAN_F5R1_FB7_Msk //!<Filter bit 7 
.equ CAN_F5R1_FB8_Pos, (8) 
.equ CAN_F5R1_FB8_Msk, (0x1 << CAN_F5R1_FB8_Pos) //!< 0x00000100 
.equ CAN_F5R1_FB8, CAN_F5R1_FB8_Msk //!<Filter bit 8 
.equ CAN_F5R1_FB9_Pos, (9) 
.equ CAN_F5R1_FB9_Msk, (0x1 << CAN_F5R1_FB9_Pos) //!< 0x00000200 
.equ CAN_F5R1_FB9, CAN_F5R1_FB9_Msk //!<Filter bit 9 
.equ CAN_F5R1_FB10_Pos, (10) 
.equ CAN_F5R1_FB10_Msk, (0x1 << CAN_F5R1_FB10_Pos) //!< 0x00000400 
.equ CAN_F5R1_FB10, CAN_F5R1_FB10_Msk //!<Filter bit 10 
.equ CAN_F5R1_FB11_Pos, (11) 
.equ CAN_F5R1_FB11_Msk, (0x1 << CAN_F5R1_FB11_Pos) //!< 0x00000800 
.equ CAN_F5R1_FB11, CAN_F5R1_FB11_Msk //!<Filter bit 11 
.equ CAN_F5R1_FB12_Pos, (12) 
.equ CAN_F5R1_FB12_Msk, (0x1 << CAN_F5R1_FB12_Pos) //!< 0x00001000 
.equ CAN_F5R1_FB12, CAN_F5R1_FB12_Msk //!<Filter bit 12 
.equ CAN_F5R1_FB13_Pos, (13) 
.equ CAN_F5R1_FB13_Msk, (0x1 << CAN_F5R1_FB13_Pos) //!< 0x00002000 
.equ CAN_F5R1_FB13, CAN_F5R1_FB13_Msk //!<Filter bit 13 
.equ CAN_F5R1_FB14_Pos, (14) 
.equ CAN_F5R1_FB14_Msk, (0x1 << CAN_F5R1_FB14_Pos) //!< 0x00004000 
.equ CAN_F5R1_FB14, CAN_F5R1_FB14_Msk //!<Filter bit 14 
.equ CAN_F5R1_FB15_Pos, (15) 
.equ CAN_F5R1_FB15_Msk, (0x1 << CAN_F5R1_FB15_Pos) //!< 0x00008000 
.equ CAN_F5R1_FB15, CAN_F5R1_FB15_Msk //!<Filter bit 15 
.equ CAN_F5R1_FB16_Pos, (16) 
.equ CAN_F5R1_FB16_Msk, (0x1 << CAN_F5R1_FB16_Pos) //!< 0x00010000 
.equ CAN_F5R1_FB16, CAN_F5R1_FB16_Msk //!<Filter bit 16 
.equ CAN_F5R1_FB17_Pos, (17) 
.equ CAN_F5R1_FB17_Msk, (0x1 << CAN_F5R1_FB17_Pos) //!< 0x00020000 
.equ CAN_F5R1_FB17, CAN_F5R1_FB17_Msk //!<Filter bit 17 
.equ CAN_F5R1_FB18_Pos, (18) 
.equ CAN_F5R1_FB18_Msk, (0x1 << CAN_F5R1_FB18_Pos) //!< 0x00040000 
.equ CAN_F5R1_FB18, CAN_F5R1_FB18_Msk //!<Filter bit 18 
.equ CAN_F5R1_FB19_Pos, (19) 
.equ CAN_F5R1_FB19_Msk, (0x1 << CAN_F5R1_FB19_Pos) //!< 0x00080000 
.equ CAN_F5R1_FB19, CAN_F5R1_FB19_Msk //!<Filter bit 19 
.equ CAN_F5R1_FB20_Pos, (20) 
.equ CAN_F5R1_FB20_Msk, (0x1 << CAN_F5R1_FB20_Pos) //!< 0x00100000 
.equ CAN_F5R1_FB20, CAN_F5R1_FB20_Msk //!<Filter bit 20 
.equ CAN_F5R1_FB21_Pos, (21) 
.equ CAN_F5R1_FB21_Msk, (0x1 << CAN_F5R1_FB21_Pos) //!< 0x00200000 
.equ CAN_F5R1_FB21, CAN_F5R1_FB21_Msk //!<Filter bit 21 
.equ CAN_F5R1_FB22_Pos, (22) 
.equ CAN_F5R1_FB22_Msk, (0x1 << CAN_F5R1_FB22_Pos) //!< 0x00400000 
.equ CAN_F5R1_FB22, CAN_F5R1_FB22_Msk //!<Filter bit 22 
.equ CAN_F5R1_FB23_Pos, (23) 
.equ CAN_F5R1_FB23_Msk, (0x1 << CAN_F5R1_FB23_Pos) //!< 0x00800000 
.equ CAN_F5R1_FB23, CAN_F5R1_FB23_Msk //!<Filter bit 23 
.equ CAN_F5R1_FB24_Pos, (24) 
.equ CAN_F5R1_FB24_Msk, (0x1 << CAN_F5R1_FB24_Pos) //!< 0x01000000 
.equ CAN_F5R1_FB24, CAN_F5R1_FB24_Msk //!<Filter bit 24 
.equ CAN_F5R1_FB25_Pos, (25) 
.equ CAN_F5R1_FB25_Msk, (0x1 << CAN_F5R1_FB25_Pos) //!< 0x02000000 
.equ CAN_F5R1_FB25, CAN_F5R1_FB25_Msk //!<Filter bit 25 
.equ CAN_F5R1_FB26_Pos, (26) 
.equ CAN_F5R1_FB26_Msk, (0x1 << CAN_F5R1_FB26_Pos) //!< 0x04000000 
.equ CAN_F5R1_FB26, CAN_F5R1_FB26_Msk //!<Filter bit 26 
.equ CAN_F5R1_FB27_Pos, (27) 
.equ CAN_F5R1_FB27_Msk, (0x1 << CAN_F5R1_FB27_Pos) //!< 0x08000000 
.equ CAN_F5R1_FB27, CAN_F5R1_FB27_Msk //!<Filter bit 27 
.equ CAN_F5R1_FB28_Pos, (28) 
.equ CAN_F5R1_FB28_Msk, (0x1 << CAN_F5R1_FB28_Pos) //!< 0x10000000 
.equ CAN_F5R1_FB28, CAN_F5R1_FB28_Msk //!<Filter bit 28 
.equ CAN_F5R1_FB29_Pos, (29) 
.equ CAN_F5R1_FB29_Msk, (0x1 << CAN_F5R1_FB29_Pos) //!< 0x20000000 
.equ CAN_F5R1_FB29, CAN_F5R1_FB29_Msk //!<Filter bit 29 
.equ CAN_F5R1_FB30_Pos, (30) 
.equ CAN_F5R1_FB30_Msk, (0x1 << CAN_F5R1_FB30_Pos) //!< 0x40000000 
.equ CAN_F5R1_FB30, CAN_F5R1_FB30_Msk //!<Filter bit 30 
.equ CAN_F5R1_FB31_Pos, (31) 
.equ CAN_F5R1_FB31_Msk, (0x1 << CAN_F5R1_FB31_Pos) //!< 0x80000000 
.equ CAN_F5R1_FB31, CAN_F5R1_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F6R1 register  ******************
.equ CAN_F6R1_FB0_Pos, (0) 
.equ CAN_F6R1_FB0_Msk, (0x1 << CAN_F6R1_FB0_Pos) //!< 0x00000001 
.equ CAN_F6R1_FB0, CAN_F6R1_FB0_Msk //!<Filter bit 0 
.equ CAN_F6R1_FB1_Pos, (1) 
.equ CAN_F6R1_FB1_Msk, (0x1 << CAN_F6R1_FB1_Pos) //!< 0x00000002 
.equ CAN_F6R1_FB1, CAN_F6R1_FB1_Msk //!<Filter bit 1 
.equ CAN_F6R1_FB2_Pos, (2) 
.equ CAN_F6R1_FB2_Msk, (0x1 << CAN_F6R1_FB2_Pos) //!< 0x00000004 
.equ CAN_F6R1_FB2, CAN_F6R1_FB2_Msk //!<Filter bit 2 
.equ CAN_F6R1_FB3_Pos, (3) 
.equ CAN_F6R1_FB3_Msk, (0x1 << CAN_F6R1_FB3_Pos) //!< 0x00000008 
.equ CAN_F6R1_FB3, CAN_F6R1_FB3_Msk //!<Filter bit 3 
.equ CAN_F6R1_FB4_Pos, (4) 
.equ CAN_F6R1_FB4_Msk, (0x1 << CAN_F6R1_FB4_Pos) //!< 0x00000010 
.equ CAN_F6R1_FB4, CAN_F6R1_FB4_Msk //!<Filter bit 4 
.equ CAN_F6R1_FB5_Pos, (5) 
.equ CAN_F6R1_FB5_Msk, (0x1 << CAN_F6R1_FB5_Pos) //!< 0x00000020 
.equ CAN_F6R1_FB5, CAN_F6R1_FB5_Msk //!<Filter bit 5 
.equ CAN_F6R1_FB6_Pos, (6) 
.equ CAN_F6R1_FB6_Msk, (0x1 << CAN_F6R1_FB6_Pos) //!< 0x00000040 
.equ CAN_F6R1_FB6, CAN_F6R1_FB6_Msk //!<Filter bit 6 
.equ CAN_F6R1_FB7_Pos, (7) 
.equ CAN_F6R1_FB7_Msk, (0x1 << CAN_F6R1_FB7_Pos) //!< 0x00000080 
.equ CAN_F6R1_FB7, CAN_F6R1_FB7_Msk //!<Filter bit 7 
.equ CAN_F6R1_FB8_Pos, (8) 
.equ CAN_F6R1_FB8_Msk, (0x1 << CAN_F6R1_FB8_Pos) //!< 0x00000100 
.equ CAN_F6R1_FB8, CAN_F6R1_FB8_Msk //!<Filter bit 8 
.equ CAN_F6R1_FB9_Pos, (9) 
.equ CAN_F6R1_FB9_Msk, (0x1 << CAN_F6R1_FB9_Pos) //!< 0x00000200 
.equ CAN_F6R1_FB9, CAN_F6R1_FB9_Msk //!<Filter bit 9 
.equ CAN_F6R1_FB10_Pos, (10) 
.equ CAN_F6R1_FB10_Msk, (0x1 << CAN_F6R1_FB10_Pos) //!< 0x00000400 
.equ CAN_F6R1_FB10, CAN_F6R1_FB10_Msk //!<Filter bit 10 
.equ CAN_F6R1_FB11_Pos, (11) 
.equ CAN_F6R1_FB11_Msk, (0x1 << CAN_F6R1_FB11_Pos) //!< 0x00000800 
.equ CAN_F6R1_FB11, CAN_F6R1_FB11_Msk //!<Filter bit 11 
.equ CAN_F6R1_FB12_Pos, (12) 
.equ CAN_F6R1_FB12_Msk, (0x1 << CAN_F6R1_FB12_Pos) //!< 0x00001000 
.equ CAN_F6R1_FB12, CAN_F6R1_FB12_Msk //!<Filter bit 12 
.equ CAN_F6R1_FB13_Pos, (13) 
.equ CAN_F6R1_FB13_Msk, (0x1 << CAN_F6R1_FB13_Pos) //!< 0x00002000 
.equ CAN_F6R1_FB13, CAN_F6R1_FB13_Msk //!<Filter bit 13 
.equ CAN_F6R1_FB14_Pos, (14) 
.equ CAN_F6R1_FB14_Msk, (0x1 << CAN_F6R1_FB14_Pos) //!< 0x00004000 
.equ CAN_F6R1_FB14, CAN_F6R1_FB14_Msk //!<Filter bit 14 
.equ CAN_F6R1_FB15_Pos, (15) 
.equ CAN_F6R1_FB15_Msk, (0x1 << CAN_F6R1_FB15_Pos) //!< 0x00008000 
.equ CAN_F6R1_FB15, CAN_F6R1_FB15_Msk //!<Filter bit 15 
.equ CAN_F6R1_FB16_Pos, (16) 
.equ CAN_F6R1_FB16_Msk, (0x1 << CAN_F6R1_FB16_Pos) //!< 0x00010000 
.equ CAN_F6R1_FB16, CAN_F6R1_FB16_Msk //!<Filter bit 16 
.equ CAN_F6R1_FB17_Pos, (17) 
.equ CAN_F6R1_FB17_Msk, (0x1 << CAN_F6R1_FB17_Pos) //!< 0x00020000 
.equ CAN_F6R1_FB17, CAN_F6R1_FB17_Msk //!<Filter bit 17 
.equ CAN_F6R1_FB18_Pos, (18) 
.equ CAN_F6R1_FB18_Msk, (0x1 << CAN_F6R1_FB18_Pos) //!< 0x00040000 
.equ CAN_F6R1_FB18, CAN_F6R1_FB18_Msk //!<Filter bit 18 
.equ CAN_F6R1_FB19_Pos, (19) 
.equ CAN_F6R1_FB19_Msk, (0x1 << CAN_F6R1_FB19_Pos) //!< 0x00080000 
.equ CAN_F6R1_FB19, CAN_F6R1_FB19_Msk //!<Filter bit 19 
.equ CAN_F6R1_FB20_Pos, (20) 
.equ CAN_F6R1_FB20_Msk, (0x1 << CAN_F6R1_FB20_Pos) //!< 0x00100000 
.equ CAN_F6R1_FB20, CAN_F6R1_FB20_Msk //!<Filter bit 20 
.equ CAN_F6R1_FB21_Pos, (21) 
.equ CAN_F6R1_FB21_Msk, (0x1 << CAN_F6R1_FB21_Pos) //!< 0x00200000 
.equ CAN_F6R1_FB21, CAN_F6R1_FB21_Msk //!<Filter bit 21 
.equ CAN_F6R1_FB22_Pos, (22) 
.equ CAN_F6R1_FB22_Msk, (0x1 << CAN_F6R1_FB22_Pos) //!< 0x00400000 
.equ CAN_F6R1_FB22, CAN_F6R1_FB22_Msk //!<Filter bit 22 
.equ CAN_F6R1_FB23_Pos, (23) 
.equ CAN_F6R1_FB23_Msk, (0x1 << CAN_F6R1_FB23_Pos) //!< 0x00800000 
.equ CAN_F6R1_FB23, CAN_F6R1_FB23_Msk //!<Filter bit 23 
.equ CAN_F6R1_FB24_Pos, (24) 
.equ CAN_F6R1_FB24_Msk, (0x1 << CAN_F6R1_FB24_Pos) //!< 0x01000000 
.equ CAN_F6R1_FB24, CAN_F6R1_FB24_Msk //!<Filter bit 24 
.equ CAN_F6R1_FB25_Pos, (25) 
.equ CAN_F6R1_FB25_Msk, (0x1 << CAN_F6R1_FB25_Pos) //!< 0x02000000 
.equ CAN_F6R1_FB25, CAN_F6R1_FB25_Msk //!<Filter bit 25 
.equ CAN_F6R1_FB26_Pos, (26) 
.equ CAN_F6R1_FB26_Msk, (0x1 << CAN_F6R1_FB26_Pos) //!< 0x04000000 
.equ CAN_F6R1_FB26, CAN_F6R1_FB26_Msk //!<Filter bit 26 
.equ CAN_F6R1_FB27_Pos, (27) 
.equ CAN_F6R1_FB27_Msk, (0x1 << CAN_F6R1_FB27_Pos) //!< 0x08000000 
.equ CAN_F6R1_FB27, CAN_F6R1_FB27_Msk //!<Filter bit 27 
.equ CAN_F6R1_FB28_Pos, (28) 
.equ CAN_F6R1_FB28_Msk, (0x1 << CAN_F6R1_FB28_Pos) //!< 0x10000000 
.equ CAN_F6R1_FB28, CAN_F6R1_FB28_Msk //!<Filter bit 28 
.equ CAN_F6R1_FB29_Pos, (29) 
.equ CAN_F6R1_FB29_Msk, (0x1 << CAN_F6R1_FB29_Pos) //!< 0x20000000 
.equ CAN_F6R1_FB29, CAN_F6R1_FB29_Msk //!<Filter bit 29 
.equ CAN_F6R1_FB30_Pos, (30) 
.equ CAN_F6R1_FB30_Msk, (0x1 << CAN_F6R1_FB30_Pos) //!< 0x40000000 
.equ CAN_F6R1_FB30, CAN_F6R1_FB30_Msk //!<Filter bit 30 
.equ CAN_F6R1_FB31_Pos, (31) 
.equ CAN_F6R1_FB31_Msk, (0x1 << CAN_F6R1_FB31_Pos) //!< 0x80000000 
.equ CAN_F6R1_FB31, CAN_F6R1_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F7R1 register  ******************
.equ CAN_F7R1_FB0_Pos, (0) 
.equ CAN_F7R1_FB0_Msk, (0x1 << CAN_F7R1_FB0_Pos) //!< 0x00000001 
.equ CAN_F7R1_FB0, CAN_F7R1_FB0_Msk //!<Filter bit 0 
.equ CAN_F7R1_FB1_Pos, (1) 
.equ CAN_F7R1_FB1_Msk, (0x1 << CAN_F7R1_FB1_Pos) //!< 0x00000002 
.equ CAN_F7R1_FB1, CAN_F7R1_FB1_Msk //!<Filter bit 1 
.equ CAN_F7R1_FB2_Pos, (2) 
.equ CAN_F7R1_FB2_Msk, (0x1 << CAN_F7R1_FB2_Pos) //!< 0x00000004 
.equ CAN_F7R1_FB2, CAN_F7R1_FB2_Msk //!<Filter bit 2 
.equ CAN_F7R1_FB3_Pos, (3) 
.equ CAN_F7R1_FB3_Msk, (0x1 << CAN_F7R1_FB3_Pos) //!< 0x00000008 
.equ CAN_F7R1_FB3, CAN_F7R1_FB3_Msk //!<Filter bit 3 
.equ CAN_F7R1_FB4_Pos, (4) 
.equ CAN_F7R1_FB4_Msk, (0x1 << CAN_F7R1_FB4_Pos) //!< 0x00000010 
.equ CAN_F7R1_FB4, CAN_F7R1_FB4_Msk //!<Filter bit 4 
.equ CAN_F7R1_FB5_Pos, (5) 
.equ CAN_F7R1_FB5_Msk, (0x1 << CAN_F7R1_FB5_Pos) //!< 0x00000020 
.equ CAN_F7R1_FB5, CAN_F7R1_FB5_Msk //!<Filter bit 5 
.equ CAN_F7R1_FB6_Pos, (6) 
.equ CAN_F7R1_FB6_Msk, (0x1 << CAN_F7R1_FB6_Pos) //!< 0x00000040 
.equ CAN_F7R1_FB6, CAN_F7R1_FB6_Msk //!<Filter bit 6 
.equ CAN_F7R1_FB7_Pos, (7) 
.equ CAN_F7R1_FB7_Msk, (0x1 << CAN_F7R1_FB7_Pos) //!< 0x00000080 
.equ CAN_F7R1_FB7, CAN_F7R1_FB7_Msk //!<Filter bit 7 
.equ CAN_F7R1_FB8_Pos, (8) 
.equ CAN_F7R1_FB8_Msk, (0x1 << CAN_F7R1_FB8_Pos) //!< 0x00000100 
.equ CAN_F7R1_FB8, CAN_F7R1_FB8_Msk //!<Filter bit 8 
.equ CAN_F7R1_FB9_Pos, (9) 
.equ CAN_F7R1_FB9_Msk, (0x1 << CAN_F7R1_FB9_Pos) //!< 0x00000200 
.equ CAN_F7R1_FB9, CAN_F7R1_FB9_Msk //!<Filter bit 9 
.equ CAN_F7R1_FB10_Pos, (10) 
.equ CAN_F7R1_FB10_Msk, (0x1 << CAN_F7R1_FB10_Pos) //!< 0x00000400 
.equ CAN_F7R1_FB10, CAN_F7R1_FB10_Msk //!<Filter bit 10 
.equ CAN_F7R1_FB11_Pos, (11) 
.equ CAN_F7R1_FB11_Msk, (0x1 << CAN_F7R1_FB11_Pos) //!< 0x00000800 
.equ CAN_F7R1_FB11, CAN_F7R1_FB11_Msk //!<Filter bit 11 
.equ CAN_F7R1_FB12_Pos, (12) 
.equ CAN_F7R1_FB12_Msk, (0x1 << CAN_F7R1_FB12_Pos) //!< 0x00001000 
.equ CAN_F7R1_FB12, CAN_F7R1_FB12_Msk //!<Filter bit 12 
.equ CAN_F7R1_FB13_Pos, (13) 
.equ CAN_F7R1_FB13_Msk, (0x1 << CAN_F7R1_FB13_Pos) //!< 0x00002000 
.equ CAN_F7R1_FB13, CAN_F7R1_FB13_Msk //!<Filter bit 13 
.equ CAN_F7R1_FB14_Pos, (14) 
.equ CAN_F7R1_FB14_Msk, (0x1 << CAN_F7R1_FB14_Pos) //!< 0x00004000 
.equ CAN_F7R1_FB14, CAN_F7R1_FB14_Msk //!<Filter bit 14 
.equ CAN_F7R1_FB15_Pos, (15) 
.equ CAN_F7R1_FB15_Msk, (0x1 << CAN_F7R1_FB15_Pos) //!< 0x00008000 
.equ CAN_F7R1_FB15, CAN_F7R1_FB15_Msk //!<Filter bit 15 
.equ CAN_F7R1_FB16_Pos, (16) 
.equ CAN_F7R1_FB16_Msk, (0x1 << CAN_F7R1_FB16_Pos) //!< 0x00010000 
.equ CAN_F7R1_FB16, CAN_F7R1_FB16_Msk //!<Filter bit 16 
.equ CAN_F7R1_FB17_Pos, (17) 
.equ CAN_F7R1_FB17_Msk, (0x1 << CAN_F7R1_FB17_Pos) //!< 0x00020000 
.equ CAN_F7R1_FB17, CAN_F7R1_FB17_Msk //!<Filter bit 17 
.equ CAN_F7R1_FB18_Pos, (18) 
.equ CAN_F7R1_FB18_Msk, (0x1 << CAN_F7R1_FB18_Pos) //!< 0x00040000 
.equ CAN_F7R1_FB18, CAN_F7R1_FB18_Msk //!<Filter bit 18 
.equ CAN_F7R1_FB19_Pos, (19) 
.equ CAN_F7R1_FB19_Msk, (0x1 << CAN_F7R1_FB19_Pos) //!< 0x00080000 
.equ CAN_F7R1_FB19, CAN_F7R1_FB19_Msk //!<Filter bit 19 
.equ CAN_F7R1_FB20_Pos, (20) 
.equ CAN_F7R1_FB20_Msk, (0x1 << CAN_F7R1_FB20_Pos) //!< 0x00100000 
.equ CAN_F7R1_FB20, CAN_F7R1_FB20_Msk //!<Filter bit 20 
.equ CAN_F7R1_FB21_Pos, (21) 
.equ CAN_F7R1_FB21_Msk, (0x1 << CAN_F7R1_FB21_Pos) //!< 0x00200000 
.equ CAN_F7R1_FB21, CAN_F7R1_FB21_Msk //!<Filter bit 21 
.equ CAN_F7R1_FB22_Pos, (22) 
.equ CAN_F7R1_FB22_Msk, (0x1 << CAN_F7R1_FB22_Pos) //!< 0x00400000 
.equ CAN_F7R1_FB22, CAN_F7R1_FB22_Msk //!<Filter bit 22 
.equ CAN_F7R1_FB23_Pos, (23) 
.equ CAN_F7R1_FB23_Msk, (0x1 << CAN_F7R1_FB23_Pos) //!< 0x00800000 
.equ CAN_F7R1_FB23, CAN_F7R1_FB23_Msk //!<Filter bit 23 
.equ CAN_F7R1_FB24_Pos, (24) 
.equ CAN_F7R1_FB24_Msk, (0x1 << CAN_F7R1_FB24_Pos) //!< 0x01000000 
.equ CAN_F7R1_FB24, CAN_F7R1_FB24_Msk //!<Filter bit 24 
.equ CAN_F7R1_FB25_Pos, (25) 
.equ CAN_F7R1_FB25_Msk, (0x1 << CAN_F7R1_FB25_Pos) //!< 0x02000000 
.equ CAN_F7R1_FB25, CAN_F7R1_FB25_Msk //!<Filter bit 25 
.equ CAN_F7R1_FB26_Pos, (26) 
.equ CAN_F7R1_FB26_Msk, (0x1 << CAN_F7R1_FB26_Pos) //!< 0x04000000 
.equ CAN_F7R1_FB26, CAN_F7R1_FB26_Msk //!<Filter bit 26 
.equ CAN_F7R1_FB27_Pos, (27) 
.equ CAN_F7R1_FB27_Msk, (0x1 << CAN_F7R1_FB27_Pos) //!< 0x08000000 
.equ CAN_F7R1_FB27, CAN_F7R1_FB27_Msk //!<Filter bit 27 
.equ CAN_F7R1_FB28_Pos, (28) 
.equ CAN_F7R1_FB28_Msk, (0x1 << CAN_F7R1_FB28_Pos) //!< 0x10000000 
.equ CAN_F7R1_FB28, CAN_F7R1_FB28_Msk //!<Filter bit 28 
.equ CAN_F7R1_FB29_Pos, (29) 
.equ CAN_F7R1_FB29_Msk, (0x1 << CAN_F7R1_FB29_Pos) //!< 0x20000000 
.equ CAN_F7R1_FB29, CAN_F7R1_FB29_Msk //!<Filter bit 29 
.equ CAN_F7R1_FB30_Pos, (30) 
.equ CAN_F7R1_FB30_Msk, (0x1 << CAN_F7R1_FB30_Pos) //!< 0x40000000 
.equ CAN_F7R1_FB30, CAN_F7R1_FB30_Msk //!<Filter bit 30 
.equ CAN_F7R1_FB31_Pos, (31) 
.equ CAN_F7R1_FB31_Msk, (0x1 << CAN_F7R1_FB31_Pos) //!< 0x80000000 
.equ CAN_F7R1_FB31, CAN_F7R1_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F8R1 register  ******************
.equ CAN_F8R1_FB0_Pos, (0) 
.equ CAN_F8R1_FB0_Msk, (0x1 << CAN_F8R1_FB0_Pos) //!< 0x00000001 
.equ CAN_F8R1_FB0, CAN_F8R1_FB0_Msk //!<Filter bit 0 
.equ CAN_F8R1_FB1_Pos, (1) 
.equ CAN_F8R1_FB1_Msk, (0x1 << CAN_F8R1_FB1_Pos) //!< 0x00000002 
.equ CAN_F8R1_FB1, CAN_F8R1_FB1_Msk //!<Filter bit 1 
.equ CAN_F8R1_FB2_Pos, (2) 
.equ CAN_F8R1_FB2_Msk, (0x1 << CAN_F8R1_FB2_Pos) //!< 0x00000004 
.equ CAN_F8R1_FB2, CAN_F8R1_FB2_Msk //!<Filter bit 2 
.equ CAN_F8R1_FB3_Pos, (3) 
.equ CAN_F8R1_FB3_Msk, (0x1 << CAN_F8R1_FB3_Pos) //!< 0x00000008 
.equ CAN_F8R1_FB3, CAN_F8R1_FB3_Msk //!<Filter bit 3 
.equ CAN_F8R1_FB4_Pos, (4) 
.equ CAN_F8R1_FB4_Msk, (0x1 << CAN_F8R1_FB4_Pos) //!< 0x00000010 
.equ CAN_F8R1_FB4, CAN_F8R1_FB4_Msk //!<Filter bit 4 
.equ CAN_F8R1_FB5_Pos, (5) 
.equ CAN_F8R1_FB5_Msk, (0x1 << CAN_F8R1_FB5_Pos) //!< 0x00000020 
.equ CAN_F8R1_FB5, CAN_F8R1_FB5_Msk //!<Filter bit 5 
.equ CAN_F8R1_FB6_Pos, (6) 
.equ CAN_F8R1_FB6_Msk, (0x1 << CAN_F8R1_FB6_Pos) //!< 0x00000040 
.equ CAN_F8R1_FB6, CAN_F8R1_FB6_Msk //!<Filter bit 6 
.equ CAN_F8R1_FB7_Pos, (7) 
.equ CAN_F8R1_FB7_Msk, (0x1 << CAN_F8R1_FB7_Pos) //!< 0x00000080 
.equ CAN_F8R1_FB7, CAN_F8R1_FB7_Msk //!<Filter bit 7 
.equ CAN_F8R1_FB8_Pos, (8) 
.equ CAN_F8R1_FB8_Msk, (0x1 << CAN_F8R1_FB8_Pos) //!< 0x00000100 
.equ CAN_F8R1_FB8, CAN_F8R1_FB8_Msk //!<Filter bit 8 
.equ CAN_F8R1_FB9_Pos, (9) 
.equ CAN_F8R1_FB9_Msk, (0x1 << CAN_F8R1_FB9_Pos) //!< 0x00000200 
.equ CAN_F8R1_FB9, CAN_F8R1_FB9_Msk //!<Filter bit 9 
.equ CAN_F8R1_FB10_Pos, (10) 
.equ CAN_F8R1_FB10_Msk, (0x1 << CAN_F8R1_FB10_Pos) //!< 0x00000400 
.equ CAN_F8R1_FB10, CAN_F8R1_FB10_Msk //!<Filter bit 10 
.equ CAN_F8R1_FB11_Pos, (11) 
.equ CAN_F8R1_FB11_Msk, (0x1 << CAN_F8R1_FB11_Pos) //!< 0x00000800 
.equ CAN_F8R1_FB11, CAN_F8R1_FB11_Msk //!<Filter bit 11 
.equ CAN_F8R1_FB12_Pos, (12) 
.equ CAN_F8R1_FB12_Msk, (0x1 << CAN_F8R1_FB12_Pos) //!< 0x00001000 
.equ CAN_F8R1_FB12, CAN_F8R1_FB12_Msk //!<Filter bit 12 
.equ CAN_F8R1_FB13_Pos, (13) 
.equ CAN_F8R1_FB13_Msk, (0x1 << CAN_F8R1_FB13_Pos) //!< 0x00002000 
.equ CAN_F8R1_FB13, CAN_F8R1_FB13_Msk //!<Filter bit 13 
.equ CAN_F8R1_FB14_Pos, (14) 
.equ CAN_F8R1_FB14_Msk, (0x1 << CAN_F8R1_FB14_Pos) //!< 0x00004000 
.equ CAN_F8R1_FB14, CAN_F8R1_FB14_Msk //!<Filter bit 14 
.equ CAN_F8R1_FB15_Pos, (15) 
.equ CAN_F8R1_FB15_Msk, (0x1 << CAN_F8R1_FB15_Pos) //!< 0x00008000 
.equ CAN_F8R1_FB15, CAN_F8R1_FB15_Msk //!<Filter bit 15 
.equ CAN_F8R1_FB16_Pos, (16) 
.equ CAN_F8R1_FB16_Msk, (0x1 << CAN_F8R1_FB16_Pos) //!< 0x00010000 
.equ CAN_F8R1_FB16, CAN_F8R1_FB16_Msk //!<Filter bit 16 
.equ CAN_F8R1_FB17_Pos, (17) 
.equ CAN_F8R1_FB17_Msk, (0x1 << CAN_F8R1_FB17_Pos) //!< 0x00020000 
.equ CAN_F8R1_FB17, CAN_F8R1_FB17_Msk //!<Filter bit 17 
.equ CAN_F8R1_FB18_Pos, (18) 
.equ CAN_F8R1_FB18_Msk, (0x1 << CAN_F8R1_FB18_Pos) //!< 0x00040000 
.equ CAN_F8R1_FB18, CAN_F8R1_FB18_Msk //!<Filter bit 18 
.equ CAN_F8R1_FB19_Pos, (19) 
.equ CAN_F8R1_FB19_Msk, (0x1 << CAN_F8R1_FB19_Pos) //!< 0x00080000 
.equ CAN_F8R1_FB19, CAN_F8R1_FB19_Msk //!<Filter bit 19 
.equ CAN_F8R1_FB20_Pos, (20) 
.equ CAN_F8R1_FB20_Msk, (0x1 << CAN_F8R1_FB20_Pos) //!< 0x00100000 
.equ CAN_F8R1_FB20, CAN_F8R1_FB20_Msk //!<Filter bit 20 
.equ CAN_F8R1_FB21_Pos, (21) 
.equ CAN_F8R1_FB21_Msk, (0x1 << CAN_F8R1_FB21_Pos) //!< 0x00200000 
.equ CAN_F8R1_FB21, CAN_F8R1_FB21_Msk //!<Filter bit 21 
.equ CAN_F8R1_FB22_Pos, (22) 
.equ CAN_F8R1_FB22_Msk, (0x1 << CAN_F8R1_FB22_Pos) //!< 0x00400000 
.equ CAN_F8R1_FB22, CAN_F8R1_FB22_Msk //!<Filter bit 22 
.equ CAN_F8R1_FB23_Pos, (23) 
.equ CAN_F8R1_FB23_Msk, (0x1 << CAN_F8R1_FB23_Pos) //!< 0x00800000 
.equ CAN_F8R1_FB23, CAN_F8R1_FB23_Msk //!<Filter bit 23 
.equ CAN_F8R1_FB24_Pos, (24) 
.equ CAN_F8R1_FB24_Msk, (0x1 << CAN_F8R1_FB24_Pos) //!< 0x01000000 
.equ CAN_F8R1_FB24, CAN_F8R1_FB24_Msk //!<Filter bit 24 
.equ CAN_F8R1_FB25_Pos, (25) 
.equ CAN_F8R1_FB25_Msk, (0x1 << CAN_F8R1_FB25_Pos) //!< 0x02000000 
.equ CAN_F8R1_FB25, CAN_F8R1_FB25_Msk //!<Filter bit 25 
.equ CAN_F8R1_FB26_Pos, (26) 
.equ CAN_F8R1_FB26_Msk, (0x1 << CAN_F8R1_FB26_Pos) //!< 0x04000000 
.equ CAN_F8R1_FB26, CAN_F8R1_FB26_Msk //!<Filter bit 26 
.equ CAN_F8R1_FB27_Pos, (27) 
.equ CAN_F8R1_FB27_Msk, (0x1 << CAN_F8R1_FB27_Pos) //!< 0x08000000 
.equ CAN_F8R1_FB27, CAN_F8R1_FB27_Msk //!<Filter bit 27 
.equ CAN_F8R1_FB28_Pos, (28) 
.equ CAN_F8R1_FB28_Msk, (0x1 << CAN_F8R1_FB28_Pos) //!< 0x10000000 
.equ CAN_F8R1_FB28, CAN_F8R1_FB28_Msk //!<Filter bit 28 
.equ CAN_F8R1_FB29_Pos, (29) 
.equ CAN_F8R1_FB29_Msk, (0x1 << CAN_F8R1_FB29_Pos) //!< 0x20000000 
.equ CAN_F8R1_FB29, CAN_F8R1_FB29_Msk //!<Filter bit 29 
.equ CAN_F8R1_FB30_Pos, (30) 
.equ CAN_F8R1_FB30_Msk, (0x1 << CAN_F8R1_FB30_Pos) //!< 0x40000000 
.equ CAN_F8R1_FB30, CAN_F8R1_FB30_Msk //!<Filter bit 30 
.equ CAN_F8R1_FB31_Pos, (31) 
.equ CAN_F8R1_FB31_Msk, (0x1 << CAN_F8R1_FB31_Pos) //!< 0x80000000 
.equ CAN_F8R1_FB31, CAN_F8R1_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F9R1 register  ******************
.equ CAN_F9R1_FB0_Pos, (0) 
.equ CAN_F9R1_FB0_Msk, (0x1 << CAN_F9R1_FB0_Pos) //!< 0x00000001 
.equ CAN_F9R1_FB0, CAN_F9R1_FB0_Msk //!<Filter bit 0 
.equ CAN_F9R1_FB1_Pos, (1) 
.equ CAN_F9R1_FB1_Msk, (0x1 << CAN_F9R1_FB1_Pos) //!< 0x00000002 
.equ CAN_F9R1_FB1, CAN_F9R1_FB1_Msk //!<Filter bit 1 
.equ CAN_F9R1_FB2_Pos, (2) 
.equ CAN_F9R1_FB2_Msk, (0x1 << CAN_F9R1_FB2_Pos) //!< 0x00000004 
.equ CAN_F9R1_FB2, CAN_F9R1_FB2_Msk //!<Filter bit 2 
.equ CAN_F9R1_FB3_Pos, (3) 
.equ CAN_F9R1_FB3_Msk, (0x1 << CAN_F9R1_FB3_Pos) //!< 0x00000008 
.equ CAN_F9R1_FB3, CAN_F9R1_FB3_Msk //!<Filter bit 3 
.equ CAN_F9R1_FB4_Pos, (4) 
.equ CAN_F9R1_FB4_Msk, (0x1 << CAN_F9R1_FB4_Pos) //!< 0x00000010 
.equ CAN_F9R1_FB4, CAN_F9R1_FB4_Msk //!<Filter bit 4 
.equ CAN_F9R1_FB5_Pos, (5) 
.equ CAN_F9R1_FB5_Msk, (0x1 << CAN_F9R1_FB5_Pos) //!< 0x00000020 
.equ CAN_F9R1_FB5, CAN_F9R1_FB5_Msk //!<Filter bit 5 
.equ CAN_F9R1_FB6_Pos, (6) 
.equ CAN_F9R1_FB6_Msk, (0x1 << CAN_F9R1_FB6_Pos) //!< 0x00000040 
.equ CAN_F9R1_FB6, CAN_F9R1_FB6_Msk //!<Filter bit 6 
.equ CAN_F9R1_FB7_Pos, (7) 
.equ CAN_F9R1_FB7_Msk, (0x1 << CAN_F9R1_FB7_Pos) //!< 0x00000080 
.equ CAN_F9R1_FB7, CAN_F9R1_FB7_Msk //!<Filter bit 7 
.equ CAN_F9R1_FB8_Pos, (8) 
.equ CAN_F9R1_FB8_Msk, (0x1 << CAN_F9R1_FB8_Pos) //!< 0x00000100 
.equ CAN_F9R1_FB8, CAN_F9R1_FB8_Msk //!<Filter bit 8 
.equ CAN_F9R1_FB9_Pos, (9) 
.equ CAN_F9R1_FB9_Msk, (0x1 << CAN_F9R1_FB9_Pos) //!< 0x00000200 
.equ CAN_F9R1_FB9, CAN_F9R1_FB9_Msk //!<Filter bit 9 
.equ CAN_F9R1_FB10_Pos, (10) 
.equ CAN_F9R1_FB10_Msk, (0x1 << CAN_F9R1_FB10_Pos) //!< 0x00000400 
.equ CAN_F9R1_FB10, CAN_F9R1_FB10_Msk //!<Filter bit 10 
.equ CAN_F9R1_FB11_Pos, (11) 
.equ CAN_F9R1_FB11_Msk, (0x1 << CAN_F9R1_FB11_Pos) //!< 0x00000800 
.equ CAN_F9R1_FB11, CAN_F9R1_FB11_Msk //!<Filter bit 11 
.equ CAN_F9R1_FB12_Pos, (12) 
.equ CAN_F9R1_FB12_Msk, (0x1 << CAN_F9R1_FB12_Pos) //!< 0x00001000 
.equ CAN_F9R1_FB12, CAN_F9R1_FB12_Msk //!<Filter bit 12 
.equ CAN_F9R1_FB13_Pos, (13) 
.equ CAN_F9R1_FB13_Msk, (0x1 << CAN_F9R1_FB13_Pos) //!< 0x00002000 
.equ CAN_F9R1_FB13, CAN_F9R1_FB13_Msk //!<Filter bit 13 
.equ CAN_F9R1_FB14_Pos, (14) 
.equ CAN_F9R1_FB14_Msk, (0x1 << CAN_F9R1_FB14_Pos) //!< 0x00004000 
.equ CAN_F9R1_FB14, CAN_F9R1_FB14_Msk //!<Filter bit 14 
.equ CAN_F9R1_FB15_Pos, (15) 
.equ CAN_F9R1_FB15_Msk, (0x1 << CAN_F9R1_FB15_Pos) //!< 0x00008000 
.equ CAN_F9R1_FB15, CAN_F9R1_FB15_Msk //!<Filter bit 15 
.equ CAN_F9R1_FB16_Pos, (16) 
.equ CAN_F9R1_FB16_Msk, (0x1 << CAN_F9R1_FB16_Pos) //!< 0x00010000 
.equ CAN_F9R1_FB16, CAN_F9R1_FB16_Msk //!<Filter bit 16 
.equ CAN_F9R1_FB17_Pos, (17) 
.equ CAN_F9R1_FB17_Msk, (0x1 << CAN_F9R1_FB17_Pos) //!< 0x00020000 
.equ CAN_F9R1_FB17, CAN_F9R1_FB17_Msk //!<Filter bit 17 
.equ CAN_F9R1_FB18_Pos, (18) 
.equ CAN_F9R1_FB18_Msk, (0x1 << CAN_F9R1_FB18_Pos) //!< 0x00040000 
.equ CAN_F9R1_FB18, CAN_F9R1_FB18_Msk //!<Filter bit 18 
.equ CAN_F9R1_FB19_Pos, (19) 
.equ CAN_F9R1_FB19_Msk, (0x1 << CAN_F9R1_FB19_Pos) //!< 0x00080000 
.equ CAN_F9R1_FB19, CAN_F9R1_FB19_Msk //!<Filter bit 19 
.equ CAN_F9R1_FB20_Pos, (20) 
.equ CAN_F9R1_FB20_Msk, (0x1 << CAN_F9R1_FB20_Pos) //!< 0x00100000 
.equ CAN_F9R1_FB20, CAN_F9R1_FB20_Msk //!<Filter bit 20 
.equ CAN_F9R1_FB21_Pos, (21) 
.equ CAN_F9R1_FB21_Msk, (0x1 << CAN_F9R1_FB21_Pos) //!< 0x00200000 
.equ CAN_F9R1_FB21, CAN_F9R1_FB21_Msk //!<Filter bit 21 
.equ CAN_F9R1_FB22_Pos, (22) 
.equ CAN_F9R1_FB22_Msk, (0x1 << CAN_F9R1_FB22_Pos) //!< 0x00400000 
.equ CAN_F9R1_FB22, CAN_F9R1_FB22_Msk //!<Filter bit 22 
.equ CAN_F9R1_FB23_Pos, (23) 
.equ CAN_F9R1_FB23_Msk, (0x1 << CAN_F9R1_FB23_Pos) //!< 0x00800000 
.equ CAN_F9R1_FB23, CAN_F9R1_FB23_Msk //!<Filter bit 23 
.equ CAN_F9R1_FB24_Pos, (24) 
.equ CAN_F9R1_FB24_Msk, (0x1 << CAN_F9R1_FB24_Pos) //!< 0x01000000 
.equ CAN_F9R1_FB24, CAN_F9R1_FB24_Msk //!<Filter bit 24 
.equ CAN_F9R1_FB25_Pos, (25) 
.equ CAN_F9R1_FB25_Msk, (0x1 << CAN_F9R1_FB25_Pos) //!< 0x02000000 
.equ CAN_F9R1_FB25, CAN_F9R1_FB25_Msk //!<Filter bit 25 
.equ CAN_F9R1_FB26_Pos, (26) 
.equ CAN_F9R1_FB26_Msk, (0x1 << CAN_F9R1_FB26_Pos) //!< 0x04000000 
.equ CAN_F9R1_FB26, CAN_F9R1_FB26_Msk //!<Filter bit 26 
.equ CAN_F9R1_FB27_Pos, (27) 
.equ CAN_F9R1_FB27_Msk, (0x1 << CAN_F9R1_FB27_Pos) //!< 0x08000000 
.equ CAN_F9R1_FB27, CAN_F9R1_FB27_Msk //!<Filter bit 27 
.equ CAN_F9R1_FB28_Pos, (28) 
.equ CAN_F9R1_FB28_Msk, (0x1 << CAN_F9R1_FB28_Pos) //!< 0x10000000 
.equ CAN_F9R1_FB28, CAN_F9R1_FB28_Msk //!<Filter bit 28 
.equ CAN_F9R1_FB29_Pos, (29) 
.equ CAN_F9R1_FB29_Msk, (0x1 << CAN_F9R1_FB29_Pos) //!< 0x20000000 
.equ CAN_F9R1_FB29, CAN_F9R1_FB29_Msk //!<Filter bit 29 
.equ CAN_F9R1_FB30_Pos, (30) 
.equ CAN_F9R1_FB30_Msk, (0x1 << CAN_F9R1_FB30_Pos) //!< 0x40000000 
.equ CAN_F9R1_FB30, CAN_F9R1_FB30_Msk //!<Filter bit 30 
.equ CAN_F9R1_FB31_Pos, (31) 
.equ CAN_F9R1_FB31_Msk, (0x1 << CAN_F9R1_FB31_Pos) //!< 0x80000000 
.equ CAN_F9R1_FB31, CAN_F9R1_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F10R1 register  *****************
.equ CAN_F10R1_FB0_Pos, (0) 
.equ CAN_F10R1_FB0_Msk, (0x1 << CAN_F10R1_FB0_Pos) //!< 0x00000001 
.equ CAN_F10R1_FB0, CAN_F10R1_FB0_Msk //!<Filter bit 0 
.equ CAN_F10R1_FB1_Pos, (1) 
.equ CAN_F10R1_FB1_Msk, (0x1 << CAN_F10R1_FB1_Pos) //!< 0x00000002 
.equ CAN_F10R1_FB1, CAN_F10R1_FB1_Msk //!<Filter bit 1 
.equ CAN_F10R1_FB2_Pos, (2) 
.equ CAN_F10R1_FB2_Msk, (0x1 << CAN_F10R1_FB2_Pos) //!< 0x00000004 
.equ CAN_F10R1_FB2, CAN_F10R1_FB2_Msk //!<Filter bit 2 
.equ CAN_F10R1_FB3_Pos, (3) 
.equ CAN_F10R1_FB3_Msk, (0x1 << CAN_F10R1_FB3_Pos) //!< 0x00000008 
.equ CAN_F10R1_FB3, CAN_F10R1_FB3_Msk //!<Filter bit 3 
.equ CAN_F10R1_FB4_Pos, (4) 
.equ CAN_F10R1_FB4_Msk, (0x1 << CAN_F10R1_FB4_Pos) //!< 0x00000010 
.equ CAN_F10R1_FB4, CAN_F10R1_FB4_Msk //!<Filter bit 4 
.equ CAN_F10R1_FB5_Pos, (5) 
.equ CAN_F10R1_FB5_Msk, (0x1 << CAN_F10R1_FB5_Pos) //!< 0x00000020 
.equ CAN_F10R1_FB5, CAN_F10R1_FB5_Msk //!<Filter bit 5 
.equ CAN_F10R1_FB6_Pos, (6) 
.equ CAN_F10R1_FB6_Msk, (0x1 << CAN_F10R1_FB6_Pos) //!< 0x00000040 
.equ CAN_F10R1_FB6, CAN_F10R1_FB6_Msk //!<Filter bit 6 
.equ CAN_F10R1_FB7_Pos, (7) 
.equ CAN_F10R1_FB7_Msk, (0x1 << CAN_F10R1_FB7_Pos) //!< 0x00000080 
.equ CAN_F10R1_FB7, CAN_F10R1_FB7_Msk //!<Filter bit 7 
.equ CAN_F10R1_FB8_Pos, (8) 
.equ CAN_F10R1_FB8_Msk, (0x1 << CAN_F10R1_FB8_Pos) //!< 0x00000100 
.equ CAN_F10R1_FB8, CAN_F10R1_FB8_Msk //!<Filter bit 8 
.equ CAN_F10R1_FB9_Pos, (9) 
.equ CAN_F10R1_FB9_Msk, (0x1 << CAN_F10R1_FB9_Pos) //!< 0x00000200 
.equ CAN_F10R1_FB9, CAN_F10R1_FB9_Msk //!<Filter bit 9 
.equ CAN_F10R1_FB10_Pos, (10) 
.equ CAN_F10R1_FB10_Msk, (0x1 << CAN_F10R1_FB10_Pos) //!< 0x00000400 
.equ CAN_F10R1_FB10, CAN_F10R1_FB10_Msk //!<Filter bit 10 
.equ CAN_F10R1_FB11_Pos, (11) 
.equ CAN_F10R1_FB11_Msk, (0x1 << CAN_F10R1_FB11_Pos) //!< 0x00000800 
.equ CAN_F10R1_FB11, CAN_F10R1_FB11_Msk //!<Filter bit 11 
.equ CAN_F10R1_FB12_Pos, (12) 
.equ CAN_F10R1_FB12_Msk, (0x1 << CAN_F10R1_FB12_Pos) //!< 0x00001000 
.equ CAN_F10R1_FB12, CAN_F10R1_FB12_Msk //!<Filter bit 12 
.equ CAN_F10R1_FB13_Pos, (13) 
.equ CAN_F10R1_FB13_Msk, (0x1 << CAN_F10R1_FB13_Pos) //!< 0x00002000 
.equ CAN_F10R1_FB13, CAN_F10R1_FB13_Msk //!<Filter bit 13 
.equ CAN_F10R1_FB14_Pos, (14) 
.equ CAN_F10R1_FB14_Msk, (0x1 << CAN_F10R1_FB14_Pos) //!< 0x00004000 
.equ CAN_F10R1_FB14, CAN_F10R1_FB14_Msk //!<Filter bit 14 
.equ CAN_F10R1_FB15_Pos, (15) 
.equ CAN_F10R1_FB15_Msk, (0x1 << CAN_F10R1_FB15_Pos) //!< 0x00008000 
.equ CAN_F10R1_FB15, CAN_F10R1_FB15_Msk //!<Filter bit 15 
.equ CAN_F10R1_FB16_Pos, (16) 
.equ CAN_F10R1_FB16_Msk, (0x1 << CAN_F10R1_FB16_Pos) //!< 0x00010000 
.equ CAN_F10R1_FB16, CAN_F10R1_FB16_Msk //!<Filter bit 16 
.equ CAN_F10R1_FB17_Pos, (17) 
.equ CAN_F10R1_FB17_Msk, (0x1 << CAN_F10R1_FB17_Pos) //!< 0x00020000 
.equ CAN_F10R1_FB17, CAN_F10R1_FB17_Msk //!<Filter bit 17 
.equ CAN_F10R1_FB18_Pos, (18) 
.equ CAN_F10R1_FB18_Msk, (0x1 << CAN_F10R1_FB18_Pos) //!< 0x00040000 
.equ CAN_F10R1_FB18, CAN_F10R1_FB18_Msk //!<Filter bit 18 
.equ CAN_F10R1_FB19_Pos, (19) 
.equ CAN_F10R1_FB19_Msk, (0x1 << CAN_F10R1_FB19_Pos) //!< 0x00080000 
.equ CAN_F10R1_FB19, CAN_F10R1_FB19_Msk //!<Filter bit 19 
.equ CAN_F10R1_FB20_Pos, (20) 
.equ CAN_F10R1_FB20_Msk, (0x1 << CAN_F10R1_FB20_Pos) //!< 0x00100000 
.equ CAN_F10R1_FB20, CAN_F10R1_FB20_Msk //!<Filter bit 20 
.equ CAN_F10R1_FB21_Pos, (21) 
.equ CAN_F10R1_FB21_Msk, (0x1 << CAN_F10R1_FB21_Pos) //!< 0x00200000 
.equ CAN_F10R1_FB21, CAN_F10R1_FB21_Msk //!<Filter bit 21 
.equ CAN_F10R1_FB22_Pos, (22) 
.equ CAN_F10R1_FB22_Msk, (0x1 << CAN_F10R1_FB22_Pos) //!< 0x00400000 
.equ CAN_F10R1_FB22, CAN_F10R1_FB22_Msk //!<Filter bit 22 
.equ CAN_F10R1_FB23_Pos, (23) 
.equ CAN_F10R1_FB23_Msk, (0x1 << CAN_F10R1_FB23_Pos) //!< 0x00800000 
.equ CAN_F10R1_FB23, CAN_F10R1_FB23_Msk //!<Filter bit 23 
.equ CAN_F10R1_FB24_Pos, (24) 
.equ CAN_F10R1_FB24_Msk, (0x1 << CAN_F10R1_FB24_Pos) //!< 0x01000000 
.equ CAN_F10R1_FB24, CAN_F10R1_FB24_Msk //!<Filter bit 24 
.equ CAN_F10R1_FB25_Pos, (25) 
.equ CAN_F10R1_FB25_Msk, (0x1 << CAN_F10R1_FB25_Pos) //!< 0x02000000 
.equ CAN_F10R1_FB25, CAN_F10R1_FB25_Msk //!<Filter bit 25 
.equ CAN_F10R1_FB26_Pos, (26) 
.equ CAN_F10R1_FB26_Msk, (0x1 << CAN_F10R1_FB26_Pos) //!< 0x04000000 
.equ CAN_F10R1_FB26, CAN_F10R1_FB26_Msk //!<Filter bit 26 
.equ CAN_F10R1_FB27_Pos, (27) 
.equ CAN_F10R1_FB27_Msk, (0x1 << CAN_F10R1_FB27_Pos) //!< 0x08000000 
.equ CAN_F10R1_FB27, CAN_F10R1_FB27_Msk //!<Filter bit 27 
.equ CAN_F10R1_FB28_Pos, (28) 
.equ CAN_F10R1_FB28_Msk, (0x1 << CAN_F10R1_FB28_Pos) //!< 0x10000000 
.equ CAN_F10R1_FB28, CAN_F10R1_FB28_Msk //!<Filter bit 28 
.equ CAN_F10R1_FB29_Pos, (29) 
.equ CAN_F10R1_FB29_Msk, (0x1 << CAN_F10R1_FB29_Pos) //!< 0x20000000 
.equ CAN_F10R1_FB29, CAN_F10R1_FB29_Msk //!<Filter bit 29 
.equ CAN_F10R1_FB30_Pos, (30) 
.equ CAN_F10R1_FB30_Msk, (0x1 << CAN_F10R1_FB30_Pos) //!< 0x40000000 
.equ CAN_F10R1_FB30, CAN_F10R1_FB30_Msk //!<Filter bit 30 
.equ CAN_F10R1_FB31_Pos, (31) 
.equ CAN_F10R1_FB31_Msk, (0x1 << CAN_F10R1_FB31_Pos) //!< 0x80000000 
.equ CAN_F10R1_FB31, CAN_F10R1_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F11R1 register  *****************
.equ CAN_F11R1_FB0_Pos, (0) 
.equ CAN_F11R1_FB0_Msk, (0x1 << CAN_F11R1_FB0_Pos) //!< 0x00000001 
.equ CAN_F11R1_FB0, CAN_F11R1_FB0_Msk //!<Filter bit 0 
.equ CAN_F11R1_FB1_Pos, (1) 
.equ CAN_F11R1_FB1_Msk, (0x1 << CAN_F11R1_FB1_Pos) //!< 0x00000002 
.equ CAN_F11R1_FB1, CAN_F11R1_FB1_Msk //!<Filter bit 1 
.equ CAN_F11R1_FB2_Pos, (2) 
.equ CAN_F11R1_FB2_Msk, (0x1 << CAN_F11R1_FB2_Pos) //!< 0x00000004 
.equ CAN_F11R1_FB2, CAN_F11R1_FB2_Msk //!<Filter bit 2 
.equ CAN_F11R1_FB3_Pos, (3) 
.equ CAN_F11R1_FB3_Msk, (0x1 << CAN_F11R1_FB3_Pos) //!< 0x00000008 
.equ CAN_F11R1_FB3, CAN_F11R1_FB3_Msk //!<Filter bit 3 
.equ CAN_F11R1_FB4_Pos, (4) 
.equ CAN_F11R1_FB4_Msk, (0x1 << CAN_F11R1_FB4_Pos) //!< 0x00000010 
.equ CAN_F11R1_FB4, CAN_F11R1_FB4_Msk //!<Filter bit 4 
.equ CAN_F11R1_FB5_Pos, (5) 
.equ CAN_F11R1_FB5_Msk, (0x1 << CAN_F11R1_FB5_Pos) //!< 0x00000020 
.equ CAN_F11R1_FB5, CAN_F11R1_FB5_Msk //!<Filter bit 5 
.equ CAN_F11R1_FB6_Pos, (6) 
.equ CAN_F11R1_FB6_Msk, (0x1 << CAN_F11R1_FB6_Pos) //!< 0x00000040 
.equ CAN_F11R1_FB6, CAN_F11R1_FB6_Msk //!<Filter bit 6 
.equ CAN_F11R1_FB7_Pos, (7) 
.equ CAN_F11R1_FB7_Msk, (0x1 << CAN_F11R1_FB7_Pos) //!< 0x00000080 
.equ CAN_F11R1_FB7, CAN_F11R1_FB7_Msk //!<Filter bit 7 
.equ CAN_F11R1_FB8_Pos, (8) 
.equ CAN_F11R1_FB8_Msk, (0x1 << CAN_F11R1_FB8_Pos) //!< 0x00000100 
.equ CAN_F11R1_FB8, CAN_F11R1_FB8_Msk //!<Filter bit 8 
.equ CAN_F11R1_FB9_Pos, (9) 
.equ CAN_F11R1_FB9_Msk, (0x1 << CAN_F11R1_FB9_Pos) //!< 0x00000200 
.equ CAN_F11R1_FB9, CAN_F11R1_FB9_Msk //!<Filter bit 9 
.equ CAN_F11R1_FB10_Pos, (10) 
.equ CAN_F11R1_FB10_Msk, (0x1 << CAN_F11R1_FB10_Pos) //!< 0x00000400 
.equ CAN_F11R1_FB10, CAN_F11R1_FB10_Msk //!<Filter bit 10 
.equ CAN_F11R1_FB11_Pos, (11) 
.equ CAN_F11R1_FB11_Msk, (0x1 << CAN_F11R1_FB11_Pos) //!< 0x00000800 
.equ CAN_F11R1_FB11, CAN_F11R1_FB11_Msk //!<Filter bit 11 
.equ CAN_F11R1_FB12_Pos, (12) 
.equ CAN_F11R1_FB12_Msk, (0x1 << CAN_F11R1_FB12_Pos) //!< 0x00001000 
.equ CAN_F11R1_FB12, CAN_F11R1_FB12_Msk //!<Filter bit 12 
.equ CAN_F11R1_FB13_Pos, (13) 
.equ CAN_F11R1_FB13_Msk, (0x1 << CAN_F11R1_FB13_Pos) //!< 0x00002000 
.equ CAN_F11R1_FB13, CAN_F11R1_FB13_Msk //!<Filter bit 13 
.equ CAN_F11R1_FB14_Pos, (14) 
.equ CAN_F11R1_FB14_Msk, (0x1 << CAN_F11R1_FB14_Pos) //!< 0x00004000 
.equ CAN_F11R1_FB14, CAN_F11R1_FB14_Msk //!<Filter bit 14 
.equ CAN_F11R1_FB15_Pos, (15) 
.equ CAN_F11R1_FB15_Msk, (0x1 << CAN_F11R1_FB15_Pos) //!< 0x00008000 
.equ CAN_F11R1_FB15, CAN_F11R1_FB15_Msk //!<Filter bit 15 
.equ CAN_F11R1_FB16_Pos, (16) 
.equ CAN_F11R1_FB16_Msk, (0x1 << CAN_F11R1_FB16_Pos) //!< 0x00010000 
.equ CAN_F11R1_FB16, CAN_F11R1_FB16_Msk //!<Filter bit 16 
.equ CAN_F11R1_FB17_Pos, (17) 
.equ CAN_F11R1_FB17_Msk, (0x1 << CAN_F11R1_FB17_Pos) //!< 0x00020000 
.equ CAN_F11R1_FB17, CAN_F11R1_FB17_Msk //!<Filter bit 17 
.equ CAN_F11R1_FB18_Pos, (18) 
.equ CAN_F11R1_FB18_Msk, (0x1 << CAN_F11R1_FB18_Pos) //!< 0x00040000 
.equ CAN_F11R1_FB18, CAN_F11R1_FB18_Msk //!<Filter bit 18 
.equ CAN_F11R1_FB19_Pos, (19) 
.equ CAN_F11R1_FB19_Msk, (0x1 << CAN_F11R1_FB19_Pos) //!< 0x00080000 
.equ CAN_F11R1_FB19, CAN_F11R1_FB19_Msk //!<Filter bit 19 
.equ CAN_F11R1_FB20_Pos, (20) 
.equ CAN_F11R1_FB20_Msk, (0x1 << CAN_F11R1_FB20_Pos) //!< 0x00100000 
.equ CAN_F11R1_FB20, CAN_F11R1_FB20_Msk //!<Filter bit 20 
.equ CAN_F11R1_FB21_Pos, (21) 
.equ CAN_F11R1_FB21_Msk, (0x1 << CAN_F11R1_FB21_Pos) //!< 0x00200000 
.equ CAN_F11R1_FB21, CAN_F11R1_FB21_Msk //!<Filter bit 21 
.equ CAN_F11R1_FB22_Pos, (22) 
.equ CAN_F11R1_FB22_Msk, (0x1 << CAN_F11R1_FB22_Pos) //!< 0x00400000 
.equ CAN_F11R1_FB22, CAN_F11R1_FB22_Msk //!<Filter bit 22 
.equ CAN_F11R1_FB23_Pos, (23) 
.equ CAN_F11R1_FB23_Msk, (0x1 << CAN_F11R1_FB23_Pos) //!< 0x00800000 
.equ CAN_F11R1_FB23, CAN_F11R1_FB23_Msk //!<Filter bit 23 
.equ CAN_F11R1_FB24_Pos, (24) 
.equ CAN_F11R1_FB24_Msk, (0x1 << CAN_F11R1_FB24_Pos) //!< 0x01000000 
.equ CAN_F11R1_FB24, CAN_F11R1_FB24_Msk //!<Filter bit 24 
.equ CAN_F11R1_FB25_Pos, (25) 
.equ CAN_F11R1_FB25_Msk, (0x1 << CAN_F11R1_FB25_Pos) //!< 0x02000000 
.equ CAN_F11R1_FB25, CAN_F11R1_FB25_Msk //!<Filter bit 25 
.equ CAN_F11R1_FB26_Pos, (26) 
.equ CAN_F11R1_FB26_Msk, (0x1 << CAN_F11R1_FB26_Pos) //!< 0x04000000 
.equ CAN_F11R1_FB26, CAN_F11R1_FB26_Msk //!<Filter bit 26 
.equ CAN_F11R1_FB27_Pos, (27) 
.equ CAN_F11R1_FB27_Msk, (0x1 << CAN_F11R1_FB27_Pos) //!< 0x08000000 
.equ CAN_F11R1_FB27, CAN_F11R1_FB27_Msk //!<Filter bit 27 
.equ CAN_F11R1_FB28_Pos, (28) 
.equ CAN_F11R1_FB28_Msk, (0x1 << CAN_F11R1_FB28_Pos) //!< 0x10000000 
.equ CAN_F11R1_FB28, CAN_F11R1_FB28_Msk //!<Filter bit 28 
.equ CAN_F11R1_FB29_Pos, (29) 
.equ CAN_F11R1_FB29_Msk, (0x1 << CAN_F11R1_FB29_Pos) //!< 0x20000000 
.equ CAN_F11R1_FB29, CAN_F11R1_FB29_Msk //!<Filter bit 29 
.equ CAN_F11R1_FB30_Pos, (30) 
.equ CAN_F11R1_FB30_Msk, (0x1 << CAN_F11R1_FB30_Pos) //!< 0x40000000 
.equ CAN_F11R1_FB30, CAN_F11R1_FB30_Msk //!<Filter bit 30 
.equ CAN_F11R1_FB31_Pos, (31) 
.equ CAN_F11R1_FB31_Msk, (0x1 << CAN_F11R1_FB31_Pos) //!< 0x80000000 
.equ CAN_F11R1_FB31, CAN_F11R1_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F12R1 register  *****************
.equ CAN_F12R1_FB0_Pos, (0) 
.equ CAN_F12R1_FB0_Msk, (0x1 << CAN_F12R1_FB0_Pos) //!< 0x00000001 
.equ CAN_F12R1_FB0, CAN_F12R1_FB0_Msk //!<Filter bit 0 
.equ CAN_F12R1_FB1_Pos, (1) 
.equ CAN_F12R1_FB1_Msk, (0x1 << CAN_F12R1_FB1_Pos) //!< 0x00000002 
.equ CAN_F12R1_FB1, CAN_F12R1_FB1_Msk //!<Filter bit 1 
.equ CAN_F12R1_FB2_Pos, (2) 
.equ CAN_F12R1_FB2_Msk, (0x1 << CAN_F12R1_FB2_Pos) //!< 0x00000004 
.equ CAN_F12R1_FB2, CAN_F12R1_FB2_Msk //!<Filter bit 2 
.equ CAN_F12R1_FB3_Pos, (3) 
.equ CAN_F12R1_FB3_Msk, (0x1 << CAN_F12R1_FB3_Pos) //!< 0x00000008 
.equ CAN_F12R1_FB3, CAN_F12R1_FB3_Msk //!<Filter bit 3 
.equ CAN_F12R1_FB4_Pos, (4) 
.equ CAN_F12R1_FB4_Msk, (0x1 << CAN_F12R1_FB4_Pos) //!< 0x00000010 
.equ CAN_F12R1_FB4, CAN_F12R1_FB4_Msk //!<Filter bit 4 
.equ CAN_F12R1_FB5_Pos, (5) 
.equ CAN_F12R1_FB5_Msk, (0x1 << CAN_F12R1_FB5_Pos) //!< 0x00000020 
.equ CAN_F12R1_FB5, CAN_F12R1_FB5_Msk //!<Filter bit 5 
.equ CAN_F12R1_FB6_Pos, (6) 
.equ CAN_F12R1_FB6_Msk, (0x1 << CAN_F12R1_FB6_Pos) //!< 0x00000040 
.equ CAN_F12R1_FB6, CAN_F12R1_FB6_Msk //!<Filter bit 6 
.equ CAN_F12R1_FB7_Pos, (7) 
.equ CAN_F12R1_FB7_Msk, (0x1 << CAN_F12R1_FB7_Pos) //!< 0x00000080 
.equ CAN_F12R1_FB7, CAN_F12R1_FB7_Msk //!<Filter bit 7 
.equ CAN_F12R1_FB8_Pos, (8) 
.equ CAN_F12R1_FB8_Msk, (0x1 << CAN_F12R1_FB8_Pos) //!< 0x00000100 
.equ CAN_F12R1_FB8, CAN_F12R1_FB8_Msk //!<Filter bit 8 
.equ CAN_F12R1_FB9_Pos, (9) 
.equ CAN_F12R1_FB9_Msk, (0x1 << CAN_F12R1_FB9_Pos) //!< 0x00000200 
.equ CAN_F12R1_FB9, CAN_F12R1_FB9_Msk //!<Filter bit 9 
.equ CAN_F12R1_FB10_Pos, (10) 
.equ CAN_F12R1_FB10_Msk, (0x1 << CAN_F12R1_FB10_Pos) //!< 0x00000400 
.equ CAN_F12R1_FB10, CAN_F12R1_FB10_Msk //!<Filter bit 10 
.equ CAN_F12R1_FB11_Pos, (11) 
.equ CAN_F12R1_FB11_Msk, (0x1 << CAN_F12R1_FB11_Pos) //!< 0x00000800 
.equ CAN_F12R1_FB11, CAN_F12R1_FB11_Msk //!<Filter bit 11 
.equ CAN_F12R1_FB12_Pos, (12) 
.equ CAN_F12R1_FB12_Msk, (0x1 << CAN_F12R1_FB12_Pos) //!< 0x00001000 
.equ CAN_F12R1_FB12, CAN_F12R1_FB12_Msk //!<Filter bit 12 
.equ CAN_F12R1_FB13_Pos, (13) 
.equ CAN_F12R1_FB13_Msk, (0x1 << CAN_F12R1_FB13_Pos) //!< 0x00002000 
.equ CAN_F12R1_FB13, CAN_F12R1_FB13_Msk //!<Filter bit 13 
.equ CAN_F12R1_FB14_Pos, (14) 
.equ CAN_F12R1_FB14_Msk, (0x1 << CAN_F12R1_FB14_Pos) //!< 0x00004000 
.equ CAN_F12R1_FB14, CAN_F12R1_FB14_Msk //!<Filter bit 14 
.equ CAN_F12R1_FB15_Pos, (15) 
.equ CAN_F12R1_FB15_Msk, (0x1 << CAN_F12R1_FB15_Pos) //!< 0x00008000 
.equ CAN_F12R1_FB15, CAN_F12R1_FB15_Msk //!<Filter bit 15 
.equ CAN_F12R1_FB16_Pos, (16) 
.equ CAN_F12R1_FB16_Msk, (0x1 << CAN_F12R1_FB16_Pos) //!< 0x00010000 
.equ CAN_F12R1_FB16, CAN_F12R1_FB16_Msk //!<Filter bit 16 
.equ CAN_F12R1_FB17_Pos, (17) 
.equ CAN_F12R1_FB17_Msk, (0x1 << CAN_F12R1_FB17_Pos) //!< 0x00020000 
.equ CAN_F12R1_FB17, CAN_F12R1_FB17_Msk //!<Filter bit 17 
.equ CAN_F12R1_FB18_Pos, (18) 
.equ CAN_F12R1_FB18_Msk, (0x1 << CAN_F12R1_FB18_Pos) //!< 0x00040000 
.equ CAN_F12R1_FB18, CAN_F12R1_FB18_Msk //!<Filter bit 18 
.equ CAN_F12R1_FB19_Pos, (19) 
.equ CAN_F12R1_FB19_Msk, (0x1 << CAN_F12R1_FB19_Pos) //!< 0x00080000 
.equ CAN_F12R1_FB19, CAN_F12R1_FB19_Msk //!<Filter bit 19 
.equ CAN_F12R1_FB20_Pos, (20) 
.equ CAN_F12R1_FB20_Msk, (0x1 << CAN_F12R1_FB20_Pos) //!< 0x00100000 
.equ CAN_F12R1_FB20, CAN_F12R1_FB20_Msk //!<Filter bit 20 
.equ CAN_F12R1_FB21_Pos, (21) 
.equ CAN_F12R1_FB21_Msk, (0x1 << CAN_F12R1_FB21_Pos) //!< 0x00200000 
.equ CAN_F12R1_FB21, CAN_F12R1_FB21_Msk //!<Filter bit 21 
.equ CAN_F12R1_FB22_Pos, (22) 
.equ CAN_F12R1_FB22_Msk, (0x1 << CAN_F12R1_FB22_Pos) //!< 0x00400000 
.equ CAN_F12R1_FB22, CAN_F12R1_FB22_Msk //!<Filter bit 22 
.equ CAN_F12R1_FB23_Pos, (23) 
.equ CAN_F12R1_FB23_Msk, (0x1 << CAN_F12R1_FB23_Pos) //!< 0x00800000 
.equ CAN_F12R1_FB23, CAN_F12R1_FB23_Msk //!<Filter bit 23 
.equ CAN_F12R1_FB24_Pos, (24) 
.equ CAN_F12R1_FB24_Msk, (0x1 << CAN_F12R1_FB24_Pos) //!< 0x01000000 
.equ CAN_F12R1_FB24, CAN_F12R1_FB24_Msk //!<Filter bit 24 
.equ CAN_F12R1_FB25_Pos, (25) 
.equ CAN_F12R1_FB25_Msk, (0x1 << CAN_F12R1_FB25_Pos) //!< 0x02000000 
.equ CAN_F12R1_FB25, CAN_F12R1_FB25_Msk //!<Filter bit 25 
.equ CAN_F12R1_FB26_Pos, (26) 
.equ CAN_F12R1_FB26_Msk, (0x1 << CAN_F12R1_FB26_Pos) //!< 0x04000000 
.equ CAN_F12R1_FB26, CAN_F12R1_FB26_Msk //!<Filter bit 26 
.equ CAN_F12R1_FB27_Pos, (27) 
.equ CAN_F12R1_FB27_Msk, (0x1 << CAN_F12R1_FB27_Pos) //!< 0x08000000 
.equ CAN_F12R1_FB27, CAN_F12R1_FB27_Msk //!<Filter bit 27 
.equ CAN_F12R1_FB28_Pos, (28) 
.equ CAN_F12R1_FB28_Msk, (0x1 << CAN_F12R1_FB28_Pos) //!< 0x10000000 
.equ CAN_F12R1_FB28, CAN_F12R1_FB28_Msk //!<Filter bit 28 
.equ CAN_F12R1_FB29_Pos, (29) 
.equ CAN_F12R1_FB29_Msk, (0x1 << CAN_F12R1_FB29_Pos) //!< 0x20000000 
.equ CAN_F12R1_FB29, CAN_F12R1_FB29_Msk //!<Filter bit 29 
.equ CAN_F12R1_FB30_Pos, (30) 
.equ CAN_F12R1_FB30_Msk, (0x1 << CAN_F12R1_FB30_Pos) //!< 0x40000000 
.equ CAN_F12R1_FB30, CAN_F12R1_FB30_Msk //!<Filter bit 30 
.equ CAN_F12R1_FB31_Pos, (31) 
.equ CAN_F12R1_FB31_Msk, (0x1 << CAN_F12R1_FB31_Pos) //!< 0x80000000 
.equ CAN_F12R1_FB31, CAN_F12R1_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F13R1 register  *****************
.equ CAN_F13R1_FB0_Pos, (0) 
.equ CAN_F13R1_FB0_Msk, (0x1 << CAN_F13R1_FB0_Pos) //!< 0x00000001 
.equ CAN_F13R1_FB0, CAN_F13R1_FB0_Msk //!<Filter bit 0 
.equ CAN_F13R1_FB1_Pos, (1) 
.equ CAN_F13R1_FB1_Msk, (0x1 << CAN_F13R1_FB1_Pos) //!< 0x00000002 
.equ CAN_F13R1_FB1, CAN_F13R1_FB1_Msk //!<Filter bit 1 
.equ CAN_F13R1_FB2_Pos, (2) 
.equ CAN_F13R1_FB2_Msk, (0x1 << CAN_F13R1_FB2_Pos) //!< 0x00000004 
.equ CAN_F13R1_FB2, CAN_F13R1_FB2_Msk //!<Filter bit 2 
.equ CAN_F13R1_FB3_Pos, (3) 
.equ CAN_F13R1_FB3_Msk, (0x1 << CAN_F13R1_FB3_Pos) //!< 0x00000008 
.equ CAN_F13R1_FB3, CAN_F13R1_FB3_Msk //!<Filter bit 3 
.equ CAN_F13R1_FB4_Pos, (4) 
.equ CAN_F13R1_FB4_Msk, (0x1 << CAN_F13R1_FB4_Pos) //!< 0x00000010 
.equ CAN_F13R1_FB4, CAN_F13R1_FB4_Msk //!<Filter bit 4 
.equ CAN_F13R1_FB5_Pos, (5) 
.equ CAN_F13R1_FB5_Msk, (0x1 << CAN_F13R1_FB5_Pos) //!< 0x00000020 
.equ CAN_F13R1_FB5, CAN_F13R1_FB5_Msk //!<Filter bit 5 
.equ CAN_F13R1_FB6_Pos, (6) 
.equ CAN_F13R1_FB6_Msk, (0x1 << CAN_F13R1_FB6_Pos) //!< 0x00000040 
.equ CAN_F13R1_FB6, CAN_F13R1_FB6_Msk //!<Filter bit 6 
.equ CAN_F13R1_FB7_Pos, (7) 
.equ CAN_F13R1_FB7_Msk, (0x1 << CAN_F13R1_FB7_Pos) //!< 0x00000080 
.equ CAN_F13R1_FB7, CAN_F13R1_FB7_Msk //!<Filter bit 7 
.equ CAN_F13R1_FB8_Pos, (8) 
.equ CAN_F13R1_FB8_Msk, (0x1 << CAN_F13R1_FB8_Pos) //!< 0x00000100 
.equ CAN_F13R1_FB8, CAN_F13R1_FB8_Msk //!<Filter bit 8 
.equ CAN_F13R1_FB9_Pos, (9) 
.equ CAN_F13R1_FB9_Msk, (0x1 << CAN_F13R1_FB9_Pos) //!< 0x00000200 
.equ CAN_F13R1_FB9, CAN_F13R1_FB9_Msk //!<Filter bit 9 
.equ CAN_F13R1_FB10_Pos, (10) 
.equ CAN_F13R1_FB10_Msk, (0x1 << CAN_F13R1_FB10_Pos) //!< 0x00000400 
.equ CAN_F13R1_FB10, CAN_F13R1_FB10_Msk //!<Filter bit 10 
.equ CAN_F13R1_FB11_Pos, (11) 
.equ CAN_F13R1_FB11_Msk, (0x1 << CAN_F13R1_FB11_Pos) //!< 0x00000800 
.equ CAN_F13R1_FB11, CAN_F13R1_FB11_Msk //!<Filter bit 11 
.equ CAN_F13R1_FB12_Pos, (12) 
.equ CAN_F13R1_FB12_Msk, (0x1 << CAN_F13R1_FB12_Pos) //!< 0x00001000 
.equ CAN_F13R1_FB12, CAN_F13R1_FB12_Msk //!<Filter bit 12 
.equ CAN_F13R1_FB13_Pos, (13) 
.equ CAN_F13R1_FB13_Msk, (0x1 << CAN_F13R1_FB13_Pos) //!< 0x00002000 
.equ CAN_F13R1_FB13, CAN_F13R1_FB13_Msk //!<Filter bit 13 
.equ CAN_F13R1_FB14_Pos, (14) 
.equ CAN_F13R1_FB14_Msk, (0x1 << CAN_F13R1_FB14_Pos) //!< 0x00004000 
.equ CAN_F13R1_FB14, CAN_F13R1_FB14_Msk //!<Filter bit 14 
.equ CAN_F13R1_FB15_Pos, (15) 
.equ CAN_F13R1_FB15_Msk, (0x1 << CAN_F13R1_FB15_Pos) //!< 0x00008000 
.equ CAN_F13R1_FB15, CAN_F13R1_FB15_Msk //!<Filter bit 15 
.equ CAN_F13R1_FB16_Pos, (16) 
.equ CAN_F13R1_FB16_Msk, (0x1 << CAN_F13R1_FB16_Pos) //!< 0x00010000 
.equ CAN_F13R1_FB16, CAN_F13R1_FB16_Msk //!<Filter bit 16 
.equ CAN_F13R1_FB17_Pos, (17) 
.equ CAN_F13R1_FB17_Msk, (0x1 << CAN_F13R1_FB17_Pos) //!< 0x00020000 
.equ CAN_F13R1_FB17, CAN_F13R1_FB17_Msk //!<Filter bit 17 
.equ CAN_F13R1_FB18_Pos, (18) 
.equ CAN_F13R1_FB18_Msk, (0x1 << CAN_F13R1_FB18_Pos) //!< 0x00040000 
.equ CAN_F13R1_FB18, CAN_F13R1_FB18_Msk //!<Filter bit 18 
.equ CAN_F13R1_FB19_Pos, (19) 
.equ CAN_F13R1_FB19_Msk, (0x1 << CAN_F13R1_FB19_Pos) //!< 0x00080000 
.equ CAN_F13R1_FB19, CAN_F13R1_FB19_Msk //!<Filter bit 19 
.equ CAN_F13R1_FB20_Pos, (20) 
.equ CAN_F13R1_FB20_Msk, (0x1 << CAN_F13R1_FB20_Pos) //!< 0x00100000 
.equ CAN_F13R1_FB20, CAN_F13R1_FB20_Msk //!<Filter bit 20 
.equ CAN_F13R1_FB21_Pos, (21) 
.equ CAN_F13R1_FB21_Msk, (0x1 << CAN_F13R1_FB21_Pos) //!< 0x00200000 
.equ CAN_F13R1_FB21, CAN_F13R1_FB21_Msk //!<Filter bit 21 
.equ CAN_F13R1_FB22_Pos, (22) 
.equ CAN_F13R1_FB22_Msk, (0x1 << CAN_F13R1_FB22_Pos) //!< 0x00400000 
.equ CAN_F13R1_FB22, CAN_F13R1_FB22_Msk //!<Filter bit 22 
.equ CAN_F13R1_FB23_Pos, (23) 
.equ CAN_F13R1_FB23_Msk, (0x1 << CAN_F13R1_FB23_Pos) //!< 0x00800000 
.equ CAN_F13R1_FB23, CAN_F13R1_FB23_Msk //!<Filter bit 23 
.equ CAN_F13R1_FB24_Pos, (24) 
.equ CAN_F13R1_FB24_Msk, (0x1 << CAN_F13R1_FB24_Pos) //!< 0x01000000 
.equ CAN_F13R1_FB24, CAN_F13R1_FB24_Msk //!<Filter bit 24 
.equ CAN_F13R1_FB25_Pos, (25) 
.equ CAN_F13R1_FB25_Msk, (0x1 << CAN_F13R1_FB25_Pos) //!< 0x02000000 
.equ CAN_F13R1_FB25, CAN_F13R1_FB25_Msk //!<Filter bit 25 
.equ CAN_F13R1_FB26_Pos, (26) 
.equ CAN_F13R1_FB26_Msk, (0x1 << CAN_F13R1_FB26_Pos) //!< 0x04000000 
.equ CAN_F13R1_FB26, CAN_F13R1_FB26_Msk //!<Filter bit 26 
.equ CAN_F13R1_FB27_Pos, (27) 
.equ CAN_F13R1_FB27_Msk, (0x1 << CAN_F13R1_FB27_Pos) //!< 0x08000000 
.equ CAN_F13R1_FB27, CAN_F13R1_FB27_Msk //!<Filter bit 27 
.equ CAN_F13R1_FB28_Pos, (28) 
.equ CAN_F13R1_FB28_Msk, (0x1 << CAN_F13R1_FB28_Pos) //!< 0x10000000 
.equ CAN_F13R1_FB28, CAN_F13R1_FB28_Msk //!<Filter bit 28 
.equ CAN_F13R1_FB29_Pos, (29) 
.equ CAN_F13R1_FB29_Msk, (0x1 << CAN_F13R1_FB29_Pos) //!< 0x20000000 
.equ CAN_F13R1_FB29, CAN_F13R1_FB29_Msk //!<Filter bit 29 
.equ CAN_F13R1_FB30_Pos, (30) 
.equ CAN_F13R1_FB30_Msk, (0x1 << CAN_F13R1_FB30_Pos) //!< 0x40000000 
.equ CAN_F13R1_FB30, CAN_F13R1_FB30_Msk //!<Filter bit 30 
.equ CAN_F13R1_FB31_Pos, (31) 
.equ CAN_F13R1_FB31_Msk, (0x1 << CAN_F13R1_FB31_Pos) //!< 0x80000000 
.equ CAN_F13R1_FB31, CAN_F13R1_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F0R2 register  ******************
.equ CAN_F0R2_FB0_Pos, (0) 
.equ CAN_F0R2_FB0_Msk, (0x1 << CAN_F0R2_FB0_Pos) //!< 0x00000001 
.equ CAN_F0R2_FB0, CAN_F0R2_FB0_Msk //!<Filter bit 0 
.equ CAN_F0R2_FB1_Pos, (1) 
.equ CAN_F0R2_FB1_Msk, (0x1 << CAN_F0R2_FB1_Pos) //!< 0x00000002 
.equ CAN_F0R2_FB1, CAN_F0R2_FB1_Msk //!<Filter bit 1 
.equ CAN_F0R2_FB2_Pos, (2) 
.equ CAN_F0R2_FB2_Msk, (0x1 << CAN_F0R2_FB2_Pos) //!< 0x00000004 
.equ CAN_F0R2_FB2, CAN_F0R2_FB2_Msk //!<Filter bit 2 
.equ CAN_F0R2_FB3_Pos, (3) 
.equ CAN_F0R2_FB3_Msk, (0x1 << CAN_F0R2_FB3_Pos) //!< 0x00000008 
.equ CAN_F0R2_FB3, CAN_F0R2_FB3_Msk //!<Filter bit 3 
.equ CAN_F0R2_FB4_Pos, (4) 
.equ CAN_F0R2_FB4_Msk, (0x1 << CAN_F0R2_FB4_Pos) //!< 0x00000010 
.equ CAN_F0R2_FB4, CAN_F0R2_FB4_Msk //!<Filter bit 4 
.equ CAN_F0R2_FB5_Pos, (5) 
.equ CAN_F0R2_FB5_Msk, (0x1 << CAN_F0R2_FB5_Pos) //!< 0x00000020 
.equ CAN_F0R2_FB5, CAN_F0R2_FB5_Msk //!<Filter bit 5 
.equ CAN_F0R2_FB6_Pos, (6) 
.equ CAN_F0R2_FB6_Msk, (0x1 << CAN_F0R2_FB6_Pos) //!< 0x00000040 
.equ CAN_F0R2_FB6, CAN_F0R2_FB6_Msk //!<Filter bit 6 
.equ CAN_F0R2_FB7_Pos, (7) 
.equ CAN_F0R2_FB7_Msk, (0x1 << CAN_F0R2_FB7_Pos) //!< 0x00000080 
.equ CAN_F0R2_FB7, CAN_F0R2_FB7_Msk //!<Filter bit 7 
.equ CAN_F0R2_FB8_Pos, (8) 
.equ CAN_F0R2_FB8_Msk, (0x1 << CAN_F0R2_FB8_Pos) //!< 0x00000100 
.equ CAN_F0R2_FB8, CAN_F0R2_FB8_Msk //!<Filter bit 8 
.equ CAN_F0R2_FB9_Pos, (9) 
.equ CAN_F0R2_FB9_Msk, (0x1 << CAN_F0R2_FB9_Pos) //!< 0x00000200 
.equ CAN_F0R2_FB9, CAN_F0R2_FB9_Msk //!<Filter bit 9 
.equ CAN_F0R2_FB10_Pos, (10) 
.equ CAN_F0R2_FB10_Msk, (0x1 << CAN_F0R2_FB10_Pos) //!< 0x00000400 
.equ CAN_F0R2_FB10, CAN_F0R2_FB10_Msk //!<Filter bit 10 
.equ CAN_F0R2_FB11_Pos, (11) 
.equ CAN_F0R2_FB11_Msk, (0x1 << CAN_F0R2_FB11_Pos) //!< 0x00000800 
.equ CAN_F0R2_FB11, CAN_F0R2_FB11_Msk //!<Filter bit 11 
.equ CAN_F0R2_FB12_Pos, (12) 
.equ CAN_F0R2_FB12_Msk, (0x1 << CAN_F0R2_FB12_Pos) //!< 0x00001000 
.equ CAN_F0R2_FB12, CAN_F0R2_FB12_Msk //!<Filter bit 12 
.equ CAN_F0R2_FB13_Pos, (13) 
.equ CAN_F0R2_FB13_Msk, (0x1 << CAN_F0R2_FB13_Pos) //!< 0x00002000 
.equ CAN_F0R2_FB13, CAN_F0R2_FB13_Msk //!<Filter bit 13 
.equ CAN_F0R2_FB14_Pos, (14) 
.equ CAN_F0R2_FB14_Msk, (0x1 << CAN_F0R2_FB14_Pos) //!< 0x00004000 
.equ CAN_F0R2_FB14, CAN_F0R2_FB14_Msk //!<Filter bit 14 
.equ CAN_F0R2_FB15_Pos, (15) 
.equ CAN_F0R2_FB15_Msk, (0x1 << CAN_F0R2_FB15_Pos) //!< 0x00008000 
.equ CAN_F0R2_FB15, CAN_F0R2_FB15_Msk //!<Filter bit 15 
.equ CAN_F0R2_FB16_Pos, (16) 
.equ CAN_F0R2_FB16_Msk, (0x1 << CAN_F0R2_FB16_Pos) //!< 0x00010000 
.equ CAN_F0R2_FB16, CAN_F0R2_FB16_Msk //!<Filter bit 16 
.equ CAN_F0R2_FB17_Pos, (17) 
.equ CAN_F0R2_FB17_Msk, (0x1 << CAN_F0R2_FB17_Pos) //!< 0x00020000 
.equ CAN_F0R2_FB17, CAN_F0R2_FB17_Msk //!<Filter bit 17 
.equ CAN_F0R2_FB18_Pos, (18) 
.equ CAN_F0R2_FB18_Msk, (0x1 << CAN_F0R2_FB18_Pos) //!< 0x00040000 
.equ CAN_F0R2_FB18, CAN_F0R2_FB18_Msk //!<Filter bit 18 
.equ CAN_F0R2_FB19_Pos, (19) 
.equ CAN_F0R2_FB19_Msk, (0x1 << CAN_F0R2_FB19_Pos) //!< 0x00080000 
.equ CAN_F0R2_FB19, CAN_F0R2_FB19_Msk //!<Filter bit 19 
.equ CAN_F0R2_FB20_Pos, (20) 
.equ CAN_F0R2_FB20_Msk, (0x1 << CAN_F0R2_FB20_Pos) //!< 0x00100000 
.equ CAN_F0R2_FB20, CAN_F0R2_FB20_Msk //!<Filter bit 20 
.equ CAN_F0R2_FB21_Pos, (21) 
.equ CAN_F0R2_FB21_Msk, (0x1 << CAN_F0R2_FB21_Pos) //!< 0x00200000 
.equ CAN_F0R2_FB21, CAN_F0R2_FB21_Msk //!<Filter bit 21 
.equ CAN_F0R2_FB22_Pos, (22) 
.equ CAN_F0R2_FB22_Msk, (0x1 << CAN_F0R2_FB22_Pos) //!< 0x00400000 
.equ CAN_F0R2_FB22, CAN_F0R2_FB22_Msk //!<Filter bit 22 
.equ CAN_F0R2_FB23_Pos, (23) 
.equ CAN_F0R2_FB23_Msk, (0x1 << CAN_F0R2_FB23_Pos) //!< 0x00800000 
.equ CAN_F0R2_FB23, CAN_F0R2_FB23_Msk //!<Filter bit 23 
.equ CAN_F0R2_FB24_Pos, (24) 
.equ CAN_F0R2_FB24_Msk, (0x1 << CAN_F0R2_FB24_Pos) //!< 0x01000000 
.equ CAN_F0R2_FB24, CAN_F0R2_FB24_Msk //!<Filter bit 24 
.equ CAN_F0R2_FB25_Pos, (25) 
.equ CAN_F0R2_FB25_Msk, (0x1 << CAN_F0R2_FB25_Pos) //!< 0x02000000 
.equ CAN_F0R2_FB25, CAN_F0R2_FB25_Msk //!<Filter bit 25 
.equ CAN_F0R2_FB26_Pos, (26) 
.equ CAN_F0R2_FB26_Msk, (0x1 << CAN_F0R2_FB26_Pos) //!< 0x04000000 
.equ CAN_F0R2_FB26, CAN_F0R2_FB26_Msk //!<Filter bit 26 
.equ CAN_F0R2_FB27_Pos, (27) 
.equ CAN_F0R2_FB27_Msk, (0x1 << CAN_F0R2_FB27_Pos) //!< 0x08000000 
.equ CAN_F0R2_FB27, CAN_F0R2_FB27_Msk //!<Filter bit 27 
.equ CAN_F0R2_FB28_Pos, (28) 
.equ CAN_F0R2_FB28_Msk, (0x1 << CAN_F0R2_FB28_Pos) //!< 0x10000000 
.equ CAN_F0R2_FB28, CAN_F0R2_FB28_Msk //!<Filter bit 28 
.equ CAN_F0R2_FB29_Pos, (29) 
.equ CAN_F0R2_FB29_Msk, (0x1 << CAN_F0R2_FB29_Pos) //!< 0x20000000 
.equ CAN_F0R2_FB29, CAN_F0R2_FB29_Msk //!<Filter bit 29 
.equ CAN_F0R2_FB30_Pos, (30) 
.equ CAN_F0R2_FB30_Msk, (0x1 << CAN_F0R2_FB30_Pos) //!< 0x40000000 
.equ CAN_F0R2_FB30, CAN_F0R2_FB30_Msk //!<Filter bit 30 
.equ CAN_F0R2_FB31_Pos, (31) 
.equ CAN_F0R2_FB31_Msk, (0x1 << CAN_F0R2_FB31_Pos) //!< 0x80000000 
.equ CAN_F0R2_FB31, CAN_F0R2_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F1R2 register  ******************
.equ CAN_F1R2_FB0_Pos, (0) 
.equ CAN_F1R2_FB0_Msk, (0x1 << CAN_F1R2_FB0_Pos) //!< 0x00000001 
.equ CAN_F1R2_FB0, CAN_F1R2_FB0_Msk //!<Filter bit 0 
.equ CAN_F1R2_FB1_Pos, (1) 
.equ CAN_F1R2_FB1_Msk, (0x1 << CAN_F1R2_FB1_Pos) //!< 0x00000002 
.equ CAN_F1R2_FB1, CAN_F1R2_FB1_Msk //!<Filter bit 1 
.equ CAN_F1R2_FB2_Pos, (2) 
.equ CAN_F1R2_FB2_Msk, (0x1 << CAN_F1R2_FB2_Pos) //!< 0x00000004 
.equ CAN_F1R2_FB2, CAN_F1R2_FB2_Msk //!<Filter bit 2 
.equ CAN_F1R2_FB3_Pos, (3) 
.equ CAN_F1R2_FB3_Msk, (0x1 << CAN_F1R2_FB3_Pos) //!< 0x00000008 
.equ CAN_F1R2_FB3, CAN_F1R2_FB3_Msk //!<Filter bit 3 
.equ CAN_F1R2_FB4_Pos, (4) 
.equ CAN_F1R2_FB4_Msk, (0x1 << CAN_F1R2_FB4_Pos) //!< 0x00000010 
.equ CAN_F1R2_FB4, CAN_F1R2_FB4_Msk //!<Filter bit 4 
.equ CAN_F1R2_FB5_Pos, (5) 
.equ CAN_F1R2_FB5_Msk, (0x1 << CAN_F1R2_FB5_Pos) //!< 0x00000020 
.equ CAN_F1R2_FB5, CAN_F1R2_FB5_Msk //!<Filter bit 5 
.equ CAN_F1R2_FB6_Pos, (6) 
.equ CAN_F1R2_FB6_Msk, (0x1 << CAN_F1R2_FB6_Pos) //!< 0x00000040 
.equ CAN_F1R2_FB6, CAN_F1R2_FB6_Msk //!<Filter bit 6 
.equ CAN_F1R2_FB7_Pos, (7) 
.equ CAN_F1R2_FB7_Msk, (0x1 << CAN_F1R2_FB7_Pos) //!< 0x00000080 
.equ CAN_F1R2_FB7, CAN_F1R2_FB7_Msk //!<Filter bit 7 
.equ CAN_F1R2_FB8_Pos, (8) 
.equ CAN_F1R2_FB8_Msk, (0x1 << CAN_F1R2_FB8_Pos) //!< 0x00000100 
.equ CAN_F1R2_FB8, CAN_F1R2_FB8_Msk //!<Filter bit 8 
.equ CAN_F1R2_FB9_Pos, (9) 
.equ CAN_F1R2_FB9_Msk, (0x1 << CAN_F1R2_FB9_Pos) //!< 0x00000200 
.equ CAN_F1R2_FB9, CAN_F1R2_FB9_Msk //!<Filter bit 9 
.equ CAN_F1R2_FB10_Pos, (10) 
.equ CAN_F1R2_FB10_Msk, (0x1 << CAN_F1R2_FB10_Pos) //!< 0x00000400 
.equ CAN_F1R2_FB10, CAN_F1R2_FB10_Msk //!<Filter bit 10 
.equ CAN_F1R2_FB11_Pos, (11) 
.equ CAN_F1R2_FB11_Msk, (0x1 << CAN_F1R2_FB11_Pos) //!< 0x00000800 
.equ CAN_F1R2_FB11, CAN_F1R2_FB11_Msk //!<Filter bit 11 
.equ CAN_F1R2_FB12_Pos, (12) 
.equ CAN_F1R2_FB12_Msk, (0x1 << CAN_F1R2_FB12_Pos) //!< 0x00001000 
.equ CAN_F1R2_FB12, CAN_F1R2_FB12_Msk //!<Filter bit 12 
.equ CAN_F1R2_FB13_Pos, (13) 
.equ CAN_F1R2_FB13_Msk, (0x1 << CAN_F1R2_FB13_Pos) //!< 0x00002000 
.equ CAN_F1R2_FB13, CAN_F1R2_FB13_Msk //!<Filter bit 13 
.equ CAN_F1R2_FB14_Pos, (14) 
.equ CAN_F1R2_FB14_Msk, (0x1 << CAN_F1R2_FB14_Pos) //!< 0x00004000 
.equ CAN_F1R2_FB14, CAN_F1R2_FB14_Msk //!<Filter bit 14 
.equ CAN_F1R2_FB15_Pos, (15) 
.equ CAN_F1R2_FB15_Msk, (0x1 << CAN_F1R2_FB15_Pos) //!< 0x00008000 
.equ CAN_F1R2_FB15, CAN_F1R2_FB15_Msk //!<Filter bit 15 
.equ CAN_F1R2_FB16_Pos, (16) 
.equ CAN_F1R2_FB16_Msk, (0x1 << CAN_F1R2_FB16_Pos) //!< 0x00010000 
.equ CAN_F1R2_FB16, CAN_F1R2_FB16_Msk //!<Filter bit 16 
.equ CAN_F1R2_FB17_Pos, (17) 
.equ CAN_F1R2_FB17_Msk, (0x1 << CAN_F1R2_FB17_Pos) //!< 0x00020000 
.equ CAN_F1R2_FB17, CAN_F1R2_FB17_Msk //!<Filter bit 17 
.equ CAN_F1R2_FB18_Pos, (18) 
.equ CAN_F1R2_FB18_Msk, (0x1 << CAN_F1R2_FB18_Pos) //!< 0x00040000 
.equ CAN_F1R2_FB18, CAN_F1R2_FB18_Msk //!<Filter bit 18 
.equ CAN_F1R2_FB19_Pos, (19) 
.equ CAN_F1R2_FB19_Msk, (0x1 << CAN_F1R2_FB19_Pos) //!< 0x00080000 
.equ CAN_F1R2_FB19, CAN_F1R2_FB19_Msk //!<Filter bit 19 
.equ CAN_F1R2_FB20_Pos, (20) 
.equ CAN_F1R2_FB20_Msk, (0x1 << CAN_F1R2_FB20_Pos) //!< 0x00100000 
.equ CAN_F1R2_FB20, CAN_F1R2_FB20_Msk //!<Filter bit 20 
.equ CAN_F1R2_FB21_Pos, (21) 
.equ CAN_F1R2_FB21_Msk, (0x1 << CAN_F1R2_FB21_Pos) //!< 0x00200000 
.equ CAN_F1R2_FB21, CAN_F1R2_FB21_Msk //!<Filter bit 21 
.equ CAN_F1R2_FB22_Pos, (22) 
.equ CAN_F1R2_FB22_Msk, (0x1 << CAN_F1R2_FB22_Pos) //!< 0x00400000 
.equ CAN_F1R2_FB22, CAN_F1R2_FB22_Msk //!<Filter bit 22 
.equ CAN_F1R2_FB23_Pos, (23) 
.equ CAN_F1R2_FB23_Msk, (0x1 << CAN_F1R2_FB23_Pos) //!< 0x00800000 
.equ CAN_F1R2_FB23, CAN_F1R2_FB23_Msk //!<Filter bit 23 
.equ CAN_F1R2_FB24_Pos, (24) 
.equ CAN_F1R2_FB24_Msk, (0x1 << CAN_F1R2_FB24_Pos) //!< 0x01000000 
.equ CAN_F1R2_FB24, CAN_F1R2_FB24_Msk //!<Filter bit 24 
.equ CAN_F1R2_FB25_Pos, (25) 
.equ CAN_F1R2_FB25_Msk, (0x1 << CAN_F1R2_FB25_Pos) //!< 0x02000000 
.equ CAN_F1R2_FB25, CAN_F1R2_FB25_Msk //!<Filter bit 25 
.equ CAN_F1R2_FB26_Pos, (26) 
.equ CAN_F1R2_FB26_Msk, (0x1 << CAN_F1R2_FB26_Pos) //!< 0x04000000 
.equ CAN_F1R2_FB26, CAN_F1R2_FB26_Msk //!<Filter bit 26 
.equ CAN_F1R2_FB27_Pos, (27) 
.equ CAN_F1R2_FB27_Msk, (0x1 << CAN_F1R2_FB27_Pos) //!< 0x08000000 
.equ CAN_F1R2_FB27, CAN_F1R2_FB27_Msk //!<Filter bit 27 
.equ CAN_F1R2_FB28_Pos, (28) 
.equ CAN_F1R2_FB28_Msk, (0x1 << CAN_F1R2_FB28_Pos) //!< 0x10000000 
.equ CAN_F1R2_FB28, CAN_F1R2_FB28_Msk //!<Filter bit 28 
.equ CAN_F1R2_FB29_Pos, (29) 
.equ CAN_F1R2_FB29_Msk, (0x1 << CAN_F1R2_FB29_Pos) //!< 0x20000000 
.equ CAN_F1R2_FB29, CAN_F1R2_FB29_Msk //!<Filter bit 29 
.equ CAN_F1R2_FB30_Pos, (30) 
.equ CAN_F1R2_FB30_Msk, (0x1 << CAN_F1R2_FB30_Pos) //!< 0x40000000 
.equ CAN_F1R2_FB30, CAN_F1R2_FB30_Msk //!<Filter bit 30 
.equ CAN_F1R2_FB31_Pos, (31) 
.equ CAN_F1R2_FB31_Msk, (0x1 << CAN_F1R2_FB31_Pos) //!< 0x80000000 
.equ CAN_F1R2_FB31, CAN_F1R2_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F2R2 register  ******************
.equ CAN_F2R2_FB0_Pos, (0) 
.equ CAN_F2R2_FB0_Msk, (0x1 << CAN_F2R2_FB0_Pos) //!< 0x00000001 
.equ CAN_F2R2_FB0, CAN_F2R2_FB0_Msk //!<Filter bit 0 
.equ CAN_F2R2_FB1_Pos, (1) 
.equ CAN_F2R2_FB1_Msk, (0x1 << CAN_F2R2_FB1_Pos) //!< 0x00000002 
.equ CAN_F2R2_FB1, CAN_F2R2_FB1_Msk //!<Filter bit 1 
.equ CAN_F2R2_FB2_Pos, (2) 
.equ CAN_F2R2_FB2_Msk, (0x1 << CAN_F2R2_FB2_Pos) //!< 0x00000004 
.equ CAN_F2R2_FB2, CAN_F2R2_FB2_Msk //!<Filter bit 2 
.equ CAN_F2R2_FB3_Pos, (3) 
.equ CAN_F2R2_FB3_Msk, (0x1 << CAN_F2R2_FB3_Pos) //!< 0x00000008 
.equ CAN_F2R2_FB3, CAN_F2R2_FB3_Msk //!<Filter bit 3 
.equ CAN_F2R2_FB4_Pos, (4) 
.equ CAN_F2R2_FB4_Msk, (0x1 << CAN_F2R2_FB4_Pos) //!< 0x00000010 
.equ CAN_F2R2_FB4, CAN_F2R2_FB4_Msk //!<Filter bit 4 
.equ CAN_F2R2_FB5_Pos, (5) 
.equ CAN_F2R2_FB5_Msk, (0x1 << CAN_F2R2_FB5_Pos) //!< 0x00000020 
.equ CAN_F2R2_FB5, CAN_F2R2_FB5_Msk //!<Filter bit 5 
.equ CAN_F2R2_FB6_Pos, (6) 
.equ CAN_F2R2_FB6_Msk, (0x1 << CAN_F2R2_FB6_Pos) //!< 0x00000040 
.equ CAN_F2R2_FB6, CAN_F2R2_FB6_Msk //!<Filter bit 6 
.equ CAN_F2R2_FB7_Pos, (7) 
.equ CAN_F2R2_FB7_Msk, (0x1 << CAN_F2R2_FB7_Pos) //!< 0x00000080 
.equ CAN_F2R2_FB7, CAN_F2R2_FB7_Msk //!<Filter bit 7 
.equ CAN_F2R2_FB8_Pos, (8) 
.equ CAN_F2R2_FB8_Msk, (0x1 << CAN_F2R2_FB8_Pos) //!< 0x00000100 
.equ CAN_F2R2_FB8, CAN_F2R2_FB8_Msk //!<Filter bit 8 
.equ CAN_F2R2_FB9_Pos, (9) 
.equ CAN_F2R2_FB9_Msk, (0x1 << CAN_F2R2_FB9_Pos) //!< 0x00000200 
.equ CAN_F2R2_FB9, CAN_F2R2_FB9_Msk //!<Filter bit 9 
.equ CAN_F2R2_FB10_Pos, (10) 
.equ CAN_F2R2_FB10_Msk, (0x1 << CAN_F2R2_FB10_Pos) //!< 0x00000400 
.equ CAN_F2R2_FB10, CAN_F2R2_FB10_Msk //!<Filter bit 10 
.equ CAN_F2R2_FB11_Pos, (11) 
.equ CAN_F2R2_FB11_Msk, (0x1 << CAN_F2R2_FB11_Pos) //!< 0x00000800 
.equ CAN_F2R2_FB11, CAN_F2R2_FB11_Msk //!<Filter bit 11 
.equ CAN_F2R2_FB12_Pos, (12) 
.equ CAN_F2R2_FB12_Msk, (0x1 << CAN_F2R2_FB12_Pos) //!< 0x00001000 
.equ CAN_F2R2_FB12, CAN_F2R2_FB12_Msk //!<Filter bit 12 
.equ CAN_F2R2_FB13_Pos, (13) 
.equ CAN_F2R2_FB13_Msk, (0x1 << CAN_F2R2_FB13_Pos) //!< 0x00002000 
.equ CAN_F2R2_FB13, CAN_F2R2_FB13_Msk //!<Filter bit 13 
.equ CAN_F2R2_FB14_Pos, (14) 
.equ CAN_F2R2_FB14_Msk, (0x1 << CAN_F2R2_FB14_Pos) //!< 0x00004000 
.equ CAN_F2R2_FB14, CAN_F2R2_FB14_Msk //!<Filter bit 14 
.equ CAN_F2R2_FB15_Pos, (15) 
.equ CAN_F2R2_FB15_Msk, (0x1 << CAN_F2R2_FB15_Pos) //!< 0x00008000 
.equ CAN_F2R2_FB15, CAN_F2R2_FB15_Msk //!<Filter bit 15 
.equ CAN_F2R2_FB16_Pos, (16) 
.equ CAN_F2R2_FB16_Msk, (0x1 << CAN_F2R2_FB16_Pos) //!< 0x00010000 
.equ CAN_F2R2_FB16, CAN_F2R2_FB16_Msk //!<Filter bit 16 
.equ CAN_F2R2_FB17_Pos, (17) 
.equ CAN_F2R2_FB17_Msk, (0x1 << CAN_F2R2_FB17_Pos) //!< 0x00020000 
.equ CAN_F2R2_FB17, CAN_F2R2_FB17_Msk //!<Filter bit 17 
.equ CAN_F2R2_FB18_Pos, (18) 
.equ CAN_F2R2_FB18_Msk, (0x1 << CAN_F2R2_FB18_Pos) //!< 0x00040000 
.equ CAN_F2R2_FB18, CAN_F2R2_FB18_Msk //!<Filter bit 18 
.equ CAN_F2R2_FB19_Pos, (19) 
.equ CAN_F2R2_FB19_Msk, (0x1 << CAN_F2R2_FB19_Pos) //!< 0x00080000 
.equ CAN_F2R2_FB19, CAN_F2R2_FB19_Msk //!<Filter bit 19 
.equ CAN_F2R2_FB20_Pos, (20) 
.equ CAN_F2R2_FB20_Msk, (0x1 << CAN_F2R2_FB20_Pos) //!< 0x00100000 
.equ CAN_F2R2_FB20, CAN_F2R2_FB20_Msk //!<Filter bit 20 
.equ CAN_F2R2_FB21_Pos, (21) 
.equ CAN_F2R2_FB21_Msk, (0x1 << CAN_F2R2_FB21_Pos) //!< 0x00200000 
.equ CAN_F2R2_FB21, CAN_F2R2_FB21_Msk //!<Filter bit 21 
.equ CAN_F2R2_FB22_Pos, (22) 
.equ CAN_F2R2_FB22_Msk, (0x1 << CAN_F2R2_FB22_Pos) //!< 0x00400000 
.equ CAN_F2R2_FB22, CAN_F2R2_FB22_Msk //!<Filter bit 22 
.equ CAN_F2R2_FB23_Pos, (23) 
.equ CAN_F2R2_FB23_Msk, (0x1 << CAN_F2R2_FB23_Pos) //!< 0x00800000 
.equ CAN_F2R2_FB23, CAN_F2R2_FB23_Msk //!<Filter bit 23 
.equ CAN_F2R2_FB24_Pos, (24) 
.equ CAN_F2R2_FB24_Msk, (0x1 << CAN_F2R2_FB24_Pos) //!< 0x01000000 
.equ CAN_F2R2_FB24, CAN_F2R2_FB24_Msk //!<Filter bit 24 
.equ CAN_F2R2_FB25_Pos, (25) 
.equ CAN_F2R2_FB25_Msk, (0x1 << CAN_F2R2_FB25_Pos) //!< 0x02000000 
.equ CAN_F2R2_FB25, CAN_F2R2_FB25_Msk //!<Filter bit 25 
.equ CAN_F2R2_FB26_Pos, (26) 
.equ CAN_F2R2_FB26_Msk, (0x1 << CAN_F2R2_FB26_Pos) //!< 0x04000000 
.equ CAN_F2R2_FB26, CAN_F2R2_FB26_Msk //!<Filter bit 26 
.equ CAN_F2R2_FB27_Pos, (27) 
.equ CAN_F2R2_FB27_Msk, (0x1 << CAN_F2R2_FB27_Pos) //!< 0x08000000 
.equ CAN_F2R2_FB27, CAN_F2R2_FB27_Msk //!<Filter bit 27 
.equ CAN_F2R2_FB28_Pos, (28) 
.equ CAN_F2R2_FB28_Msk, (0x1 << CAN_F2R2_FB28_Pos) //!< 0x10000000 
.equ CAN_F2R2_FB28, CAN_F2R2_FB28_Msk //!<Filter bit 28 
.equ CAN_F2R2_FB29_Pos, (29) 
.equ CAN_F2R2_FB29_Msk, (0x1 << CAN_F2R2_FB29_Pos) //!< 0x20000000 
.equ CAN_F2R2_FB29, CAN_F2R2_FB29_Msk //!<Filter bit 29 
.equ CAN_F2R2_FB30_Pos, (30) 
.equ CAN_F2R2_FB30_Msk, (0x1 << CAN_F2R2_FB30_Pos) //!< 0x40000000 
.equ CAN_F2R2_FB30, CAN_F2R2_FB30_Msk //!<Filter bit 30 
.equ CAN_F2R2_FB31_Pos, (31) 
.equ CAN_F2R2_FB31_Msk, (0x1 << CAN_F2R2_FB31_Pos) //!< 0x80000000 
.equ CAN_F2R2_FB31, CAN_F2R2_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F3R2 register  ******************
.equ CAN_F3R2_FB0_Pos, (0) 
.equ CAN_F3R2_FB0_Msk, (0x1 << CAN_F3R2_FB0_Pos) //!< 0x00000001 
.equ CAN_F3R2_FB0, CAN_F3R2_FB0_Msk //!<Filter bit 0 
.equ CAN_F3R2_FB1_Pos, (1) 
.equ CAN_F3R2_FB1_Msk, (0x1 << CAN_F3R2_FB1_Pos) //!< 0x00000002 
.equ CAN_F3R2_FB1, CAN_F3R2_FB1_Msk //!<Filter bit 1 
.equ CAN_F3R2_FB2_Pos, (2) 
.equ CAN_F3R2_FB2_Msk, (0x1 << CAN_F3R2_FB2_Pos) //!< 0x00000004 
.equ CAN_F3R2_FB2, CAN_F3R2_FB2_Msk //!<Filter bit 2 
.equ CAN_F3R2_FB3_Pos, (3) 
.equ CAN_F3R2_FB3_Msk, (0x1 << CAN_F3R2_FB3_Pos) //!< 0x00000008 
.equ CAN_F3R2_FB3, CAN_F3R2_FB3_Msk //!<Filter bit 3 
.equ CAN_F3R2_FB4_Pos, (4) 
.equ CAN_F3R2_FB4_Msk, (0x1 << CAN_F3R2_FB4_Pos) //!< 0x00000010 
.equ CAN_F3R2_FB4, CAN_F3R2_FB4_Msk //!<Filter bit 4 
.equ CAN_F3R2_FB5_Pos, (5) 
.equ CAN_F3R2_FB5_Msk, (0x1 << CAN_F3R2_FB5_Pos) //!< 0x00000020 
.equ CAN_F3R2_FB5, CAN_F3R2_FB5_Msk //!<Filter bit 5 
.equ CAN_F3R2_FB6_Pos, (6) 
.equ CAN_F3R2_FB6_Msk, (0x1 << CAN_F3R2_FB6_Pos) //!< 0x00000040 
.equ CAN_F3R2_FB6, CAN_F3R2_FB6_Msk //!<Filter bit 6 
.equ CAN_F3R2_FB7_Pos, (7) 
.equ CAN_F3R2_FB7_Msk, (0x1 << CAN_F3R2_FB7_Pos) //!< 0x00000080 
.equ CAN_F3R2_FB7, CAN_F3R2_FB7_Msk //!<Filter bit 7 
.equ CAN_F3R2_FB8_Pos, (8) 
.equ CAN_F3R2_FB8_Msk, (0x1 << CAN_F3R2_FB8_Pos) //!< 0x00000100 
.equ CAN_F3R2_FB8, CAN_F3R2_FB8_Msk //!<Filter bit 8 
.equ CAN_F3R2_FB9_Pos, (9) 
.equ CAN_F3R2_FB9_Msk, (0x1 << CAN_F3R2_FB9_Pos) //!< 0x00000200 
.equ CAN_F3R2_FB9, CAN_F3R2_FB9_Msk //!<Filter bit 9 
.equ CAN_F3R2_FB10_Pos, (10) 
.equ CAN_F3R2_FB10_Msk, (0x1 << CAN_F3R2_FB10_Pos) //!< 0x00000400 
.equ CAN_F3R2_FB10, CAN_F3R2_FB10_Msk //!<Filter bit 10 
.equ CAN_F3R2_FB11_Pos, (11) 
.equ CAN_F3R2_FB11_Msk, (0x1 << CAN_F3R2_FB11_Pos) //!< 0x00000800 
.equ CAN_F3R2_FB11, CAN_F3R2_FB11_Msk //!<Filter bit 11 
.equ CAN_F3R2_FB12_Pos, (12) 
.equ CAN_F3R2_FB12_Msk, (0x1 << CAN_F3R2_FB12_Pos) //!< 0x00001000 
.equ CAN_F3R2_FB12, CAN_F3R2_FB12_Msk //!<Filter bit 12 
.equ CAN_F3R2_FB13_Pos, (13) 
.equ CAN_F3R2_FB13_Msk, (0x1 << CAN_F3R2_FB13_Pos) //!< 0x00002000 
.equ CAN_F3R2_FB13, CAN_F3R2_FB13_Msk //!<Filter bit 13 
.equ CAN_F3R2_FB14_Pos, (14) 
.equ CAN_F3R2_FB14_Msk, (0x1 << CAN_F3R2_FB14_Pos) //!< 0x00004000 
.equ CAN_F3R2_FB14, CAN_F3R2_FB14_Msk //!<Filter bit 14 
.equ CAN_F3R2_FB15_Pos, (15) 
.equ CAN_F3R2_FB15_Msk, (0x1 << CAN_F3R2_FB15_Pos) //!< 0x00008000 
.equ CAN_F3R2_FB15, CAN_F3R2_FB15_Msk //!<Filter bit 15 
.equ CAN_F3R2_FB16_Pos, (16) 
.equ CAN_F3R2_FB16_Msk, (0x1 << CAN_F3R2_FB16_Pos) //!< 0x00010000 
.equ CAN_F3R2_FB16, CAN_F3R2_FB16_Msk //!<Filter bit 16 
.equ CAN_F3R2_FB17_Pos, (17) 
.equ CAN_F3R2_FB17_Msk, (0x1 << CAN_F3R2_FB17_Pos) //!< 0x00020000 
.equ CAN_F3R2_FB17, CAN_F3R2_FB17_Msk //!<Filter bit 17 
.equ CAN_F3R2_FB18_Pos, (18) 
.equ CAN_F3R2_FB18_Msk, (0x1 << CAN_F3R2_FB18_Pos) //!< 0x00040000 
.equ CAN_F3R2_FB18, CAN_F3R2_FB18_Msk //!<Filter bit 18 
.equ CAN_F3R2_FB19_Pos, (19) 
.equ CAN_F3R2_FB19_Msk, (0x1 << CAN_F3R2_FB19_Pos) //!< 0x00080000 
.equ CAN_F3R2_FB19, CAN_F3R2_FB19_Msk //!<Filter bit 19 
.equ CAN_F3R2_FB20_Pos, (20) 
.equ CAN_F3R2_FB20_Msk, (0x1 << CAN_F3R2_FB20_Pos) //!< 0x00100000 
.equ CAN_F3R2_FB20, CAN_F3R2_FB20_Msk //!<Filter bit 20 
.equ CAN_F3R2_FB21_Pos, (21) 
.equ CAN_F3R2_FB21_Msk, (0x1 << CAN_F3R2_FB21_Pos) //!< 0x00200000 
.equ CAN_F3R2_FB21, CAN_F3R2_FB21_Msk //!<Filter bit 21 
.equ CAN_F3R2_FB22_Pos, (22) 
.equ CAN_F3R2_FB22_Msk, (0x1 << CAN_F3R2_FB22_Pos) //!< 0x00400000 
.equ CAN_F3R2_FB22, CAN_F3R2_FB22_Msk //!<Filter bit 22 
.equ CAN_F3R2_FB23_Pos, (23) 
.equ CAN_F3R2_FB23_Msk, (0x1 << CAN_F3R2_FB23_Pos) //!< 0x00800000 
.equ CAN_F3R2_FB23, CAN_F3R2_FB23_Msk //!<Filter bit 23 
.equ CAN_F3R2_FB24_Pos, (24) 
.equ CAN_F3R2_FB24_Msk, (0x1 << CAN_F3R2_FB24_Pos) //!< 0x01000000 
.equ CAN_F3R2_FB24, CAN_F3R2_FB24_Msk //!<Filter bit 24 
.equ CAN_F3R2_FB25_Pos, (25) 
.equ CAN_F3R2_FB25_Msk, (0x1 << CAN_F3R2_FB25_Pos) //!< 0x02000000 
.equ CAN_F3R2_FB25, CAN_F3R2_FB25_Msk //!<Filter bit 25 
.equ CAN_F3R2_FB26_Pos, (26) 
.equ CAN_F3R2_FB26_Msk, (0x1 << CAN_F3R2_FB26_Pos) //!< 0x04000000 
.equ CAN_F3R2_FB26, CAN_F3R2_FB26_Msk //!<Filter bit 26 
.equ CAN_F3R2_FB27_Pos, (27) 
.equ CAN_F3R2_FB27_Msk, (0x1 << CAN_F3R2_FB27_Pos) //!< 0x08000000 
.equ CAN_F3R2_FB27, CAN_F3R2_FB27_Msk //!<Filter bit 27 
.equ CAN_F3R2_FB28_Pos, (28) 
.equ CAN_F3R2_FB28_Msk, (0x1 << CAN_F3R2_FB28_Pos) //!< 0x10000000 
.equ CAN_F3R2_FB28, CAN_F3R2_FB28_Msk //!<Filter bit 28 
.equ CAN_F3R2_FB29_Pos, (29) 
.equ CAN_F3R2_FB29_Msk, (0x1 << CAN_F3R2_FB29_Pos) //!< 0x20000000 
.equ CAN_F3R2_FB29, CAN_F3R2_FB29_Msk //!<Filter bit 29 
.equ CAN_F3R2_FB30_Pos, (30) 
.equ CAN_F3R2_FB30_Msk, (0x1 << CAN_F3R2_FB30_Pos) //!< 0x40000000 
.equ CAN_F3R2_FB30, CAN_F3R2_FB30_Msk //!<Filter bit 30 
.equ CAN_F3R2_FB31_Pos, (31) 
.equ CAN_F3R2_FB31_Msk, (0x1 << CAN_F3R2_FB31_Pos) //!< 0x80000000 
.equ CAN_F3R2_FB31, CAN_F3R2_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F4R2 register  ******************
.equ CAN_F4R2_FB0_Pos, (0) 
.equ CAN_F4R2_FB0_Msk, (0x1 << CAN_F4R2_FB0_Pos) //!< 0x00000001 
.equ CAN_F4R2_FB0, CAN_F4R2_FB0_Msk //!<Filter bit 0 
.equ CAN_F4R2_FB1_Pos, (1) 
.equ CAN_F4R2_FB1_Msk, (0x1 << CAN_F4R2_FB1_Pos) //!< 0x00000002 
.equ CAN_F4R2_FB1, CAN_F4R2_FB1_Msk //!<Filter bit 1 
.equ CAN_F4R2_FB2_Pos, (2) 
.equ CAN_F4R2_FB2_Msk, (0x1 << CAN_F4R2_FB2_Pos) //!< 0x00000004 
.equ CAN_F4R2_FB2, CAN_F4R2_FB2_Msk //!<Filter bit 2 
.equ CAN_F4R2_FB3_Pos, (3) 
.equ CAN_F4R2_FB3_Msk, (0x1 << CAN_F4R2_FB3_Pos) //!< 0x00000008 
.equ CAN_F4R2_FB3, CAN_F4R2_FB3_Msk //!<Filter bit 3 
.equ CAN_F4R2_FB4_Pos, (4) 
.equ CAN_F4R2_FB4_Msk, (0x1 << CAN_F4R2_FB4_Pos) //!< 0x00000010 
.equ CAN_F4R2_FB4, CAN_F4R2_FB4_Msk //!<Filter bit 4 
.equ CAN_F4R2_FB5_Pos, (5) 
.equ CAN_F4R2_FB5_Msk, (0x1 << CAN_F4R2_FB5_Pos) //!< 0x00000020 
.equ CAN_F4R2_FB5, CAN_F4R2_FB5_Msk //!<Filter bit 5 
.equ CAN_F4R2_FB6_Pos, (6) 
.equ CAN_F4R2_FB6_Msk, (0x1 << CAN_F4R2_FB6_Pos) //!< 0x00000040 
.equ CAN_F4R2_FB6, CAN_F4R2_FB6_Msk //!<Filter bit 6 
.equ CAN_F4R2_FB7_Pos, (7) 
.equ CAN_F4R2_FB7_Msk, (0x1 << CAN_F4R2_FB7_Pos) //!< 0x00000080 
.equ CAN_F4R2_FB7, CAN_F4R2_FB7_Msk //!<Filter bit 7 
.equ CAN_F4R2_FB8_Pos, (8) 
.equ CAN_F4R2_FB8_Msk, (0x1 << CAN_F4R2_FB8_Pos) //!< 0x00000100 
.equ CAN_F4R2_FB8, CAN_F4R2_FB8_Msk //!<Filter bit 8 
.equ CAN_F4R2_FB9_Pos, (9) 
.equ CAN_F4R2_FB9_Msk, (0x1 << CAN_F4R2_FB9_Pos) //!< 0x00000200 
.equ CAN_F4R2_FB9, CAN_F4R2_FB9_Msk //!<Filter bit 9 
.equ CAN_F4R2_FB10_Pos, (10) 
.equ CAN_F4R2_FB10_Msk, (0x1 << CAN_F4R2_FB10_Pos) //!< 0x00000400 
.equ CAN_F4R2_FB10, CAN_F4R2_FB10_Msk //!<Filter bit 10 
.equ CAN_F4R2_FB11_Pos, (11) 
.equ CAN_F4R2_FB11_Msk, (0x1 << CAN_F4R2_FB11_Pos) //!< 0x00000800 
.equ CAN_F4R2_FB11, CAN_F4R2_FB11_Msk //!<Filter bit 11 
.equ CAN_F4R2_FB12_Pos, (12) 
.equ CAN_F4R2_FB12_Msk, (0x1 << CAN_F4R2_FB12_Pos) //!< 0x00001000 
.equ CAN_F4R2_FB12, CAN_F4R2_FB12_Msk //!<Filter bit 12 
.equ CAN_F4R2_FB13_Pos, (13) 
.equ CAN_F4R2_FB13_Msk, (0x1 << CAN_F4R2_FB13_Pos) //!< 0x00002000 
.equ CAN_F4R2_FB13, CAN_F4R2_FB13_Msk //!<Filter bit 13 
.equ CAN_F4R2_FB14_Pos, (14) 
.equ CAN_F4R2_FB14_Msk, (0x1 << CAN_F4R2_FB14_Pos) //!< 0x00004000 
.equ CAN_F4R2_FB14, CAN_F4R2_FB14_Msk //!<Filter bit 14 
.equ CAN_F4R2_FB15_Pos, (15) 
.equ CAN_F4R2_FB15_Msk, (0x1 << CAN_F4R2_FB15_Pos) //!< 0x00008000 
.equ CAN_F4R2_FB15, CAN_F4R2_FB15_Msk //!<Filter bit 15 
.equ CAN_F4R2_FB16_Pos, (16) 
.equ CAN_F4R2_FB16_Msk, (0x1 << CAN_F4R2_FB16_Pos) //!< 0x00010000 
.equ CAN_F4R2_FB16, CAN_F4R2_FB16_Msk //!<Filter bit 16 
.equ CAN_F4R2_FB17_Pos, (17) 
.equ CAN_F4R2_FB17_Msk, (0x1 << CAN_F4R2_FB17_Pos) //!< 0x00020000 
.equ CAN_F4R2_FB17, CAN_F4R2_FB17_Msk //!<Filter bit 17 
.equ CAN_F4R2_FB18_Pos, (18) 
.equ CAN_F4R2_FB18_Msk, (0x1 << CAN_F4R2_FB18_Pos) //!< 0x00040000 
.equ CAN_F4R2_FB18, CAN_F4R2_FB18_Msk //!<Filter bit 18 
.equ CAN_F4R2_FB19_Pos, (19) 
.equ CAN_F4R2_FB19_Msk, (0x1 << CAN_F4R2_FB19_Pos) //!< 0x00080000 
.equ CAN_F4R2_FB19, CAN_F4R2_FB19_Msk //!<Filter bit 19 
.equ CAN_F4R2_FB20_Pos, (20) 
.equ CAN_F4R2_FB20_Msk, (0x1 << CAN_F4R2_FB20_Pos) //!< 0x00100000 
.equ CAN_F4R2_FB20, CAN_F4R2_FB20_Msk //!<Filter bit 20 
.equ CAN_F4R2_FB21_Pos, (21) 
.equ CAN_F4R2_FB21_Msk, (0x1 << CAN_F4R2_FB21_Pos) //!< 0x00200000 
.equ CAN_F4R2_FB21, CAN_F4R2_FB21_Msk //!<Filter bit 21 
.equ CAN_F4R2_FB22_Pos, (22) 
.equ CAN_F4R2_FB22_Msk, (0x1 << CAN_F4R2_FB22_Pos) //!< 0x00400000 
.equ CAN_F4R2_FB22, CAN_F4R2_FB22_Msk //!<Filter bit 22 
.equ CAN_F4R2_FB23_Pos, (23) 
.equ CAN_F4R2_FB23_Msk, (0x1 << CAN_F4R2_FB23_Pos) //!< 0x00800000 
.equ CAN_F4R2_FB23, CAN_F4R2_FB23_Msk //!<Filter bit 23 
.equ CAN_F4R2_FB24_Pos, (24) 
.equ CAN_F4R2_FB24_Msk, (0x1 << CAN_F4R2_FB24_Pos) //!< 0x01000000 
.equ CAN_F4R2_FB24, CAN_F4R2_FB24_Msk //!<Filter bit 24 
.equ CAN_F4R2_FB25_Pos, (25) 
.equ CAN_F4R2_FB25_Msk, (0x1 << CAN_F4R2_FB25_Pos) //!< 0x02000000 
.equ CAN_F4R2_FB25, CAN_F4R2_FB25_Msk //!<Filter bit 25 
.equ CAN_F4R2_FB26_Pos, (26) 
.equ CAN_F4R2_FB26_Msk, (0x1 << CAN_F4R2_FB26_Pos) //!< 0x04000000 
.equ CAN_F4R2_FB26, CAN_F4R2_FB26_Msk //!<Filter bit 26 
.equ CAN_F4R2_FB27_Pos, (27) 
.equ CAN_F4R2_FB27_Msk, (0x1 << CAN_F4R2_FB27_Pos) //!< 0x08000000 
.equ CAN_F4R2_FB27, CAN_F4R2_FB27_Msk //!<Filter bit 27 
.equ CAN_F4R2_FB28_Pos, (28) 
.equ CAN_F4R2_FB28_Msk, (0x1 << CAN_F4R2_FB28_Pos) //!< 0x10000000 
.equ CAN_F4R2_FB28, CAN_F4R2_FB28_Msk //!<Filter bit 28 
.equ CAN_F4R2_FB29_Pos, (29) 
.equ CAN_F4R2_FB29_Msk, (0x1 << CAN_F4R2_FB29_Pos) //!< 0x20000000 
.equ CAN_F4R2_FB29, CAN_F4R2_FB29_Msk //!<Filter bit 29 
.equ CAN_F4R2_FB30_Pos, (30) 
.equ CAN_F4R2_FB30_Msk, (0x1 << CAN_F4R2_FB30_Pos) //!< 0x40000000 
.equ CAN_F4R2_FB30, CAN_F4R2_FB30_Msk //!<Filter bit 30 
.equ CAN_F4R2_FB31_Pos, (31) 
.equ CAN_F4R2_FB31_Msk, (0x1 << CAN_F4R2_FB31_Pos) //!< 0x80000000 
.equ CAN_F4R2_FB31, CAN_F4R2_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F5R2 register  ******************
.equ CAN_F5R2_FB0_Pos, (0) 
.equ CAN_F5R2_FB0_Msk, (0x1 << CAN_F5R2_FB0_Pos) //!< 0x00000001 
.equ CAN_F5R2_FB0, CAN_F5R2_FB0_Msk //!<Filter bit 0 
.equ CAN_F5R2_FB1_Pos, (1) 
.equ CAN_F5R2_FB1_Msk, (0x1 << CAN_F5R2_FB1_Pos) //!< 0x00000002 
.equ CAN_F5R2_FB1, CAN_F5R2_FB1_Msk //!<Filter bit 1 
.equ CAN_F5R2_FB2_Pos, (2) 
.equ CAN_F5R2_FB2_Msk, (0x1 << CAN_F5R2_FB2_Pos) //!< 0x00000004 
.equ CAN_F5R2_FB2, CAN_F5R2_FB2_Msk //!<Filter bit 2 
.equ CAN_F5R2_FB3_Pos, (3) 
.equ CAN_F5R2_FB3_Msk, (0x1 << CAN_F5R2_FB3_Pos) //!< 0x00000008 
.equ CAN_F5R2_FB3, CAN_F5R2_FB3_Msk //!<Filter bit 3 
.equ CAN_F5R2_FB4_Pos, (4) 
.equ CAN_F5R2_FB4_Msk, (0x1 << CAN_F5R2_FB4_Pos) //!< 0x00000010 
.equ CAN_F5R2_FB4, CAN_F5R2_FB4_Msk //!<Filter bit 4 
.equ CAN_F5R2_FB5_Pos, (5) 
.equ CAN_F5R2_FB5_Msk, (0x1 << CAN_F5R2_FB5_Pos) //!< 0x00000020 
.equ CAN_F5R2_FB5, CAN_F5R2_FB5_Msk //!<Filter bit 5 
.equ CAN_F5R2_FB6_Pos, (6) 
.equ CAN_F5R2_FB6_Msk, (0x1 << CAN_F5R2_FB6_Pos) //!< 0x00000040 
.equ CAN_F5R2_FB6, CAN_F5R2_FB6_Msk //!<Filter bit 6 
.equ CAN_F5R2_FB7_Pos, (7) 
.equ CAN_F5R2_FB7_Msk, (0x1 << CAN_F5R2_FB7_Pos) //!< 0x00000080 
.equ CAN_F5R2_FB7, CAN_F5R2_FB7_Msk //!<Filter bit 7 
.equ CAN_F5R2_FB8_Pos, (8) 
.equ CAN_F5R2_FB8_Msk, (0x1 << CAN_F5R2_FB8_Pos) //!< 0x00000100 
.equ CAN_F5R2_FB8, CAN_F5R2_FB8_Msk //!<Filter bit 8 
.equ CAN_F5R2_FB9_Pos, (9) 
.equ CAN_F5R2_FB9_Msk, (0x1 << CAN_F5R2_FB9_Pos) //!< 0x00000200 
.equ CAN_F5R2_FB9, CAN_F5R2_FB9_Msk //!<Filter bit 9 
.equ CAN_F5R2_FB10_Pos, (10) 
.equ CAN_F5R2_FB10_Msk, (0x1 << CAN_F5R2_FB10_Pos) //!< 0x00000400 
.equ CAN_F5R2_FB10, CAN_F5R2_FB10_Msk //!<Filter bit 10 
.equ CAN_F5R2_FB11_Pos, (11) 
.equ CAN_F5R2_FB11_Msk, (0x1 << CAN_F5R2_FB11_Pos) //!< 0x00000800 
.equ CAN_F5R2_FB11, CAN_F5R2_FB11_Msk //!<Filter bit 11 
.equ CAN_F5R2_FB12_Pos, (12) 
.equ CAN_F5R2_FB12_Msk, (0x1 << CAN_F5R2_FB12_Pos) //!< 0x00001000 
.equ CAN_F5R2_FB12, CAN_F5R2_FB12_Msk //!<Filter bit 12 
.equ CAN_F5R2_FB13_Pos, (13) 
.equ CAN_F5R2_FB13_Msk, (0x1 << CAN_F5R2_FB13_Pos) //!< 0x00002000 
.equ CAN_F5R2_FB13, CAN_F5R2_FB13_Msk //!<Filter bit 13 
.equ CAN_F5R2_FB14_Pos, (14) 
.equ CAN_F5R2_FB14_Msk, (0x1 << CAN_F5R2_FB14_Pos) //!< 0x00004000 
.equ CAN_F5R2_FB14, CAN_F5R2_FB14_Msk //!<Filter bit 14 
.equ CAN_F5R2_FB15_Pos, (15) 
.equ CAN_F5R2_FB15_Msk, (0x1 << CAN_F5R2_FB15_Pos) //!< 0x00008000 
.equ CAN_F5R2_FB15, CAN_F5R2_FB15_Msk //!<Filter bit 15 
.equ CAN_F5R2_FB16_Pos, (16) 
.equ CAN_F5R2_FB16_Msk, (0x1 << CAN_F5R2_FB16_Pos) //!< 0x00010000 
.equ CAN_F5R2_FB16, CAN_F5R2_FB16_Msk //!<Filter bit 16 
.equ CAN_F5R2_FB17_Pos, (17) 
.equ CAN_F5R2_FB17_Msk, (0x1 << CAN_F5R2_FB17_Pos) //!< 0x00020000 
.equ CAN_F5R2_FB17, CAN_F5R2_FB17_Msk //!<Filter bit 17 
.equ CAN_F5R2_FB18_Pos, (18) 
.equ CAN_F5R2_FB18_Msk, (0x1 << CAN_F5R2_FB18_Pos) //!< 0x00040000 
.equ CAN_F5R2_FB18, CAN_F5R2_FB18_Msk //!<Filter bit 18 
.equ CAN_F5R2_FB19_Pos, (19) 
.equ CAN_F5R2_FB19_Msk, (0x1 << CAN_F5R2_FB19_Pos) //!< 0x00080000 
.equ CAN_F5R2_FB19, CAN_F5R2_FB19_Msk //!<Filter bit 19 
.equ CAN_F5R2_FB20_Pos, (20) 
.equ CAN_F5R2_FB20_Msk, (0x1 << CAN_F5R2_FB20_Pos) //!< 0x00100000 
.equ CAN_F5R2_FB20, CAN_F5R2_FB20_Msk //!<Filter bit 20 
.equ CAN_F5R2_FB21_Pos, (21) 
.equ CAN_F5R2_FB21_Msk, (0x1 << CAN_F5R2_FB21_Pos) //!< 0x00200000 
.equ CAN_F5R2_FB21, CAN_F5R2_FB21_Msk //!<Filter bit 21 
.equ CAN_F5R2_FB22_Pos, (22) 
.equ CAN_F5R2_FB22_Msk, (0x1 << CAN_F5R2_FB22_Pos) //!< 0x00400000 
.equ CAN_F5R2_FB22, CAN_F5R2_FB22_Msk //!<Filter bit 22 
.equ CAN_F5R2_FB23_Pos, (23) 
.equ CAN_F5R2_FB23_Msk, (0x1 << CAN_F5R2_FB23_Pos) //!< 0x00800000 
.equ CAN_F5R2_FB23, CAN_F5R2_FB23_Msk //!<Filter bit 23 
.equ CAN_F5R2_FB24_Pos, (24) 
.equ CAN_F5R2_FB24_Msk, (0x1 << CAN_F5R2_FB24_Pos) //!< 0x01000000 
.equ CAN_F5R2_FB24, CAN_F5R2_FB24_Msk //!<Filter bit 24 
.equ CAN_F5R2_FB25_Pos, (25) 
.equ CAN_F5R2_FB25_Msk, (0x1 << CAN_F5R2_FB25_Pos) //!< 0x02000000 
.equ CAN_F5R2_FB25, CAN_F5R2_FB25_Msk //!<Filter bit 25 
.equ CAN_F5R2_FB26_Pos, (26) 
.equ CAN_F5R2_FB26_Msk, (0x1 << CAN_F5R2_FB26_Pos) //!< 0x04000000 
.equ CAN_F5R2_FB26, CAN_F5R2_FB26_Msk //!<Filter bit 26 
.equ CAN_F5R2_FB27_Pos, (27) 
.equ CAN_F5R2_FB27_Msk, (0x1 << CAN_F5R2_FB27_Pos) //!< 0x08000000 
.equ CAN_F5R2_FB27, CAN_F5R2_FB27_Msk //!<Filter bit 27 
.equ CAN_F5R2_FB28_Pos, (28) 
.equ CAN_F5R2_FB28_Msk, (0x1 << CAN_F5R2_FB28_Pos) //!< 0x10000000 
.equ CAN_F5R2_FB28, CAN_F5R2_FB28_Msk //!<Filter bit 28 
.equ CAN_F5R2_FB29_Pos, (29) 
.equ CAN_F5R2_FB29_Msk, (0x1 << CAN_F5R2_FB29_Pos) //!< 0x20000000 
.equ CAN_F5R2_FB29, CAN_F5R2_FB29_Msk //!<Filter bit 29 
.equ CAN_F5R2_FB30_Pos, (30) 
.equ CAN_F5R2_FB30_Msk, (0x1 << CAN_F5R2_FB30_Pos) //!< 0x40000000 
.equ CAN_F5R2_FB30, CAN_F5R2_FB30_Msk //!<Filter bit 30 
.equ CAN_F5R2_FB31_Pos, (31) 
.equ CAN_F5R2_FB31_Msk, (0x1 << CAN_F5R2_FB31_Pos) //!< 0x80000000 
.equ CAN_F5R2_FB31, CAN_F5R2_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F6R2 register  ******************
.equ CAN_F6R2_FB0_Pos, (0) 
.equ CAN_F6R2_FB0_Msk, (0x1 << CAN_F6R2_FB0_Pos) //!< 0x00000001 
.equ CAN_F6R2_FB0, CAN_F6R2_FB0_Msk //!<Filter bit 0 
.equ CAN_F6R2_FB1_Pos, (1) 
.equ CAN_F6R2_FB1_Msk, (0x1 << CAN_F6R2_FB1_Pos) //!< 0x00000002 
.equ CAN_F6R2_FB1, CAN_F6R2_FB1_Msk //!<Filter bit 1 
.equ CAN_F6R2_FB2_Pos, (2) 
.equ CAN_F6R2_FB2_Msk, (0x1 << CAN_F6R2_FB2_Pos) //!< 0x00000004 
.equ CAN_F6R2_FB2, CAN_F6R2_FB2_Msk //!<Filter bit 2 
.equ CAN_F6R2_FB3_Pos, (3) 
.equ CAN_F6R2_FB3_Msk, (0x1 << CAN_F6R2_FB3_Pos) //!< 0x00000008 
.equ CAN_F6R2_FB3, CAN_F6R2_FB3_Msk //!<Filter bit 3 
.equ CAN_F6R2_FB4_Pos, (4) 
.equ CAN_F6R2_FB4_Msk, (0x1 << CAN_F6R2_FB4_Pos) //!< 0x00000010 
.equ CAN_F6R2_FB4, CAN_F6R2_FB4_Msk //!<Filter bit 4 
.equ CAN_F6R2_FB5_Pos, (5) 
.equ CAN_F6R2_FB5_Msk, (0x1 << CAN_F6R2_FB5_Pos) //!< 0x00000020 
.equ CAN_F6R2_FB5, CAN_F6R2_FB5_Msk //!<Filter bit 5 
.equ CAN_F6R2_FB6_Pos, (6) 
.equ CAN_F6R2_FB6_Msk, (0x1 << CAN_F6R2_FB6_Pos) //!< 0x00000040 
.equ CAN_F6R2_FB6, CAN_F6R2_FB6_Msk //!<Filter bit 6 
.equ CAN_F6R2_FB7_Pos, (7) 
.equ CAN_F6R2_FB7_Msk, (0x1 << CAN_F6R2_FB7_Pos) //!< 0x00000080 
.equ CAN_F6R2_FB7, CAN_F6R2_FB7_Msk //!<Filter bit 7 
.equ CAN_F6R2_FB8_Pos, (8) 
.equ CAN_F6R2_FB8_Msk, (0x1 << CAN_F6R2_FB8_Pos) //!< 0x00000100 
.equ CAN_F6R2_FB8, CAN_F6R2_FB8_Msk //!<Filter bit 8 
.equ CAN_F6R2_FB9_Pos, (9) 
.equ CAN_F6R2_FB9_Msk, (0x1 << CAN_F6R2_FB9_Pos) //!< 0x00000200 
.equ CAN_F6R2_FB9, CAN_F6R2_FB9_Msk //!<Filter bit 9 
.equ CAN_F6R2_FB10_Pos, (10) 
.equ CAN_F6R2_FB10_Msk, (0x1 << CAN_F6R2_FB10_Pos) //!< 0x00000400 
.equ CAN_F6R2_FB10, CAN_F6R2_FB10_Msk //!<Filter bit 10 
.equ CAN_F6R2_FB11_Pos, (11) 
.equ CAN_F6R2_FB11_Msk, (0x1 << CAN_F6R2_FB11_Pos) //!< 0x00000800 
.equ CAN_F6R2_FB11, CAN_F6R2_FB11_Msk //!<Filter bit 11 
.equ CAN_F6R2_FB12_Pos, (12) 
.equ CAN_F6R2_FB12_Msk, (0x1 << CAN_F6R2_FB12_Pos) //!< 0x00001000 
.equ CAN_F6R2_FB12, CAN_F6R2_FB12_Msk //!<Filter bit 12 
.equ CAN_F6R2_FB13_Pos, (13) 
.equ CAN_F6R2_FB13_Msk, (0x1 << CAN_F6R2_FB13_Pos) //!< 0x00002000 
.equ CAN_F6R2_FB13, CAN_F6R2_FB13_Msk //!<Filter bit 13 
.equ CAN_F6R2_FB14_Pos, (14) 
.equ CAN_F6R2_FB14_Msk, (0x1 << CAN_F6R2_FB14_Pos) //!< 0x00004000 
.equ CAN_F6R2_FB14, CAN_F6R2_FB14_Msk //!<Filter bit 14 
.equ CAN_F6R2_FB15_Pos, (15) 
.equ CAN_F6R2_FB15_Msk, (0x1 << CAN_F6R2_FB15_Pos) //!< 0x00008000 
.equ CAN_F6R2_FB15, CAN_F6R2_FB15_Msk //!<Filter bit 15 
.equ CAN_F6R2_FB16_Pos, (16) 
.equ CAN_F6R2_FB16_Msk, (0x1 << CAN_F6R2_FB16_Pos) //!< 0x00010000 
.equ CAN_F6R2_FB16, CAN_F6R2_FB16_Msk //!<Filter bit 16 
.equ CAN_F6R2_FB17_Pos, (17) 
.equ CAN_F6R2_FB17_Msk, (0x1 << CAN_F6R2_FB17_Pos) //!< 0x00020000 
.equ CAN_F6R2_FB17, CAN_F6R2_FB17_Msk //!<Filter bit 17 
.equ CAN_F6R2_FB18_Pos, (18) 
.equ CAN_F6R2_FB18_Msk, (0x1 << CAN_F6R2_FB18_Pos) //!< 0x00040000 
.equ CAN_F6R2_FB18, CAN_F6R2_FB18_Msk //!<Filter bit 18 
.equ CAN_F6R2_FB19_Pos, (19) 
.equ CAN_F6R2_FB19_Msk, (0x1 << CAN_F6R2_FB19_Pos) //!< 0x00080000 
.equ CAN_F6R2_FB19, CAN_F6R2_FB19_Msk //!<Filter bit 19 
.equ CAN_F6R2_FB20_Pos, (20) 
.equ CAN_F6R2_FB20_Msk, (0x1 << CAN_F6R2_FB20_Pos) //!< 0x00100000 
.equ CAN_F6R2_FB20, CAN_F6R2_FB20_Msk //!<Filter bit 20 
.equ CAN_F6R2_FB21_Pos, (21) 
.equ CAN_F6R2_FB21_Msk, (0x1 << CAN_F6R2_FB21_Pos) //!< 0x00200000 
.equ CAN_F6R2_FB21, CAN_F6R2_FB21_Msk //!<Filter bit 21 
.equ CAN_F6R2_FB22_Pos, (22) 
.equ CAN_F6R2_FB22_Msk, (0x1 << CAN_F6R2_FB22_Pos) //!< 0x00400000 
.equ CAN_F6R2_FB22, CAN_F6R2_FB22_Msk //!<Filter bit 22 
.equ CAN_F6R2_FB23_Pos, (23) 
.equ CAN_F6R2_FB23_Msk, (0x1 << CAN_F6R2_FB23_Pos) //!< 0x00800000 
.equ CAN_F6R2_FB23, CAN_F6R2_FB23_Msk //!<Filter bit 23 
.equ CAN_F6R2_FB24_Pos, (24) 
.equ CAN_F6R2_FB24_Msk, (0x1 << CAN_F6R2_FB24_Pos) //!< 0x01000000 
.equ CAN_F6R2_FB24, CAN_F6R2_FB24_Msk //!<Filter bit 24 
.equ CAN_F6R2_FB25_Pos, (25) 
.equ CAN_F6R2_FB25_Msk, (0x1 << CAN_F6R2_FB25_Pos) //!< 0x02000000 
.equ CAN_F6R2_FB25, CAN_F6R2_FB25_Msk //!<Filter bit 25 
.equ CAN_F6R2_FB26_Pos, (26) 
.equ CAN_F6R2_FB26_Msk, (0x1 << CAN_F6R2_FB26_Pos) //!< 0x04000000 
.equ CAN_F6R2_FB26, CAN_F6R2_FB26_Msk //!<Filter bit 26 
.equ CAN_F6R2_FB27_Pos, (27) 
.equ CAN_F6R2_FB27_Msk, (0x1 << CAN_F6R2_FB27_Pos) //!< 0x08000000 
.equ CAN_F6R2_FB27, CAN_F6R2_FB27_Msk //!<Filter bit 27 
.equ CAN_F6R2_FB28_Pos, (28) 
.equ CAN_F6R2_FB28_Msk, (0x1 << CAN_F6R2_FB28_Pos) //!< 0x10000000 
.equ CAN_F6R2_FB28, CAN_F6R2_FB28_Msk //!<Filter bit 28 
.equ CAN_F6R2_FB29_Pos, (29) 
.equ CAN_F6R2_FB29_Msk, (0x1 << CAN_F6R2_FB29_Pos) //!< 0x20000000 
.equ CAN_F6R2_FB29, CAN_F6R2_FB29_Msk //!<Filter bit 29 
.equ CAN_F6R2_FB30_Pos, (30) 
.equ CAN_F6R2_FB30_Msk, (0x1 << CAN_F6R2_FB30_Pos) //!< 0x40000000 
.equ CAN_F6R2_FB30, CAN_F6R2_FB30_Msk //!<Filter bit 30 
.equ CAN_F6R2_FB31_Pos, (31) 
.equ CAN_F6R2_FB31_Msk, (0x1 << CAN_F6R2_FB31_Pos) //!< 0x80000000 
.equ CAN_F6R2_FB31, CAN_F6R2_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F7R2 register  ******************
.equ CAN_F7R2_FB0_Pos, (0) 
.equ CAN_F7R2_FB0_Msk, (0x1 << CAN_F7R2_FB0_Pos) //!< 0x00000001 
.equ CAN_F7R2_FB0, CAN_F7R2_FB0_Msk //!<Filter bit 0 
.equ CAN_F7R2_FB1_Pos, (1) 
.equ CAN_F7R2_FB1_Msk, (0x1 << CAN_F7R2_FB1_Pos) //!< 0x00000002 
.equ CAN_F7R2_FB1, CAN_F7R2_FB1_Msk //!<Filter bit 1 
.equ CAN_F7R2_FB2_Pos, (2) 
.equ CAN_F7R2_FB2_Msk, (0x1 << CAN_F7R2_FB2_Pos) //!< 0x00000004 
.equ CAN_F7R2_FB2, CAN_F7R2_FB2_Msk //!<Filter bit 2 
.equ CAN_F7R2_FB3_Pos, (3) 
.equ CAN_F7R2_FB3_Msk, (0x1 << CAN_F7R2_FB3_Pos) //!< 0x00000008 
.equ CAN_F7R2_FB3, CAN_F7R2_FB3_Msk //!<Filter bit 3 
.equ CAN_F7R2_FB4_Pos, (4) 
.equ CAN_F7R2_FB4_Msk, (0x1 << CAN_F7R2_FB4_Pos) //!< 0x00000010 
.equ CAN_F7R2_FB4, CAN_F7R2_FB4_Msk //!<Filter bit 4 
.equ CAN_F7R2_FB5_Pos, (5) 
.equ CAN_F7R2_FB5_Msk, (0x1 << CAN_F7R2_FB5_Pos) //!< 0x00000020 
.equ CAN_F7R2_FB5, CAN_F7R2_FB5_Msk //!<Filter bit 5 
.equ CAN_F7R2_FB6_Pos, (6) 
.equ CAN_F7R2_FB6_Msk, (0x1 << CAN_F7R2_FB6_Pos) //!< 0x00000040 
.equ CAN_F7R2_FB6, CAN_F7R2_FB6_Msk //!<Filter bit 6 
.equ CAN_F7R2_FB7_Pos, (7) 
.equ CAN_F7R2_FB7_Msk, (0x1 << CAN_F7R2_FB7_Pos) //!< 0x00000080 
.equ CAN_F7R2_FB7, CAN_F7R2_FB7_Msk //!<Filter bit 7 
.equ CAN_F7R2_FB8_Pos, (8) 
.equ CAN_F7R2_FB8_Msk, (0x1 << CAN_F7R2_FB8_Pos) //!< 0x00000100 
.equ CAN_F7R2_FB8, CAN_F7R2_FB8_Msk //!<Filter bit 8 
.equ CAN_F7R2_FB9_Pos, (9) 
.equ CAN_F7R2_FB9_Msk, (0x1 << CAN_F7R2_FB9_Pos) //!< 0x00000200 
.equ CAN_F7R2_FB9, CAN_F7R2_FB9_Msk //!<Filter bit 9 
.equ CAN_F7R2_FB10_Pos, (10) 
.equ CAN_F7R2_FB10_Msk, (0x1 << CAN_F7R2_FB10_Pos) //!< 0x00000400 
.equ CAN_F7R2_FB10, CAN_F7R2_FB10_Msk //!<Filter bit 10 
.equ CAN_F7R2_FB11_Pos, (11) 
.equ CAN_F7R2_FB11_Msk, (0x1 << CAN_F7R2_FB11_Pos) //!< 0x00000800 
.equ CAN_F7R2_FB11, CAN_F7R2_FB11_Msk //!<Filter bit 11 
.equ CAN_F7R2_FB12_Pos, (12) 
.equ CAN_F7R2_FB12_Msk, (0x1 << CAN_F7R2_FB12_Pos) //!< 0x00001000 
.equ CAN_F7R2_FB12, CAN_F7R2_FB12_Msk //!<Filter bit 12 
.equ CAN_F7R2_FB13_Pos, (13) 
.equ CAN_F7R2_FB13_Msk, (0x1 << CAN_F7R2_FB13_Pos) //!< 0x00002000 
.equ CAN_F7R2_FB13, CAN_F7R2_FB13_Msk //!<Filter bit 13 
.equ CAN_F7R2_FB14_Pos, (14) 
.equ CAN_F7R2_FB14_Msk, (0x1 << CAN_F7R2_FB14_Pos) //!< 0x00004000 
.equ CAN_F7R2_FB14, CAN_F7R2_FB14_Msk //!<Filter bit 14 
.equ CAN_F7R2_FB15_Pos, (15) 
.equ CAN_F7R2_FB15_Msk, (0x1 << CAN_F7R2_FB15_Pos) //!< 0x00008000 
.equ CAN_F7R2_FB15, CAN_F7R2_FB15_Msk //!<Filter bit 15 
.equ CAN_F7R2_FB16_Pos, (16) 
.equ CAN_F7R2_FB16_Msk, (0x1 << CAN_F7R2_FB16_Pos) //!< 0x00010000 
.equ CAN_F7R2_FB16, CAN_F7R2_FB16_Msk //!<Filter bit 16 
.equ CAN_F7R2_FB17_Pos, (17) 
.equ CAN_F7R2_FB17_Msk, (0x1 << CAN_F7R2_FB17_Pos) //!< 0x00020000 
.equ CAN_F7R2_FB17, CAN_F7R2_FB17_Msk //!<Filter bit 17 
.equ CAN_F7R2_FB18_Pos, (18) 
.equ CAN_F7R2_FB18_Msk, (0x1 << CAN_F7R2_FB18_Pos) //!< 0x00040000 
.equ CAN_F7R2_FB18, CAN_F7R2_FB18_Msk //!<Filter bit 18 
.equ CAN_F7R2_FB19_Pos, (19) 
.equ CAN_F7R2_FB19_Msk, (0x1 << CAN_F7R2_FB19_Pos) //!< 0x00080000 
.equ CAN_F7R2_FB19, CAN_F7R2_FB19_Msk //!<Filter bit 19 
.equ CAN_F7R2_FB20_Pos, (20) 
.equ CAN_F7R2_FB20_Msk, (0x1 << CAN_F7R2_FB20_Pos) //!< 0x00100000 
.equ CAN_F7R2_FB20, CAN_F7R2_FB20_Msk //!<Filter bit 20 
.equ CAN_F7R2_FB21_Pos, (21) 
.equ CAN_F7R2_FB21_Msk, (0x1 << CAN_F7R2_FB21_Pos) //!< 0x00200000 
.equ CAN_F7R2_FB21, CAN_F7R2_FB21_Msk //!<Filter bit 21 
.equ CAN_F7R2_FB22_Pos, (22) 
.equ CAN_F7R2_FB22_Msk, (0x1 << CAN_F7R2_FB22_Pos) //!< 0x00400000 
.equ CAN_F7R2_FB22, CAN_F7R2_FB22_Msk //!<Filter bit 22 
.equ CAN_F7R2_FB23_Pos, (23) 
.equ CAN_F7R2_FB23_Msk, (0x1 << CAN_F7R2_FB23_Pos) //!< 0x00800000 
.equ CAN_F7R2_FB23, CAN_F7R2_FB23_Msk //!<Filter bit 23 
.equ CAN_F7R2_FB24_Pos, (24) 
.equ CAN_F7R2_FB24_Msk, (0x1 << CAN_F7R2_FB24_Pos) //!< 0x01000000 
.equ CAN_F7R2_FB24, CAN_F7R2_FB24_Msk //!<Filter bit 24 
.equ CAN_F7R2_FB25_Pos, (25) 
.equ CAN_F7R2_FB25_Msk, (0x1 << CAN_F7R2_FB25_Pos) //!< 0x02000000 
.equ CAN_F7R2_FB25, CAN_F7R2_FB25_Msk //!<Filter bit 25 
.equ CAN_F7R2_FB26_Pos, (26) 
.equ CAN_F7R2_FB26_Msk, (0x1 << CAN_F7R2_FB26_Pos) //!< 0x04000000 
.equ CAN_F7R2_FB26, CAN_F7R2_FB26_Msk //!<Filter bit 26 
.equ CAN_F7R2_FB27_Pos, (27) 
.equ CAN_F7R2_FB27_Msk, (0x1 << CAN_F7R2_FB27_Pos) //!< 0x08000000 
.equ CAN_F7R2_FB27, CAN_F7R2_FB27_Msk //!<Filter bit 27 
.equ CAN_F7R2_FB28_Pos, (28) 
.equ CAN_F7R2_FB28_Msk, (0x1 << CAN_F7R2_FB28_Pos) //!< 0x10000000 
.equ CAN_F7R2_FB28, CAN_F7R2_FB28_Msk //!<Filter bit 28 
.equ CAN_F7R2_FB29_Pos, (29) 
.equ CAN_F7R2_FB29_Msk, (0x1 << CAN_F7R2_FB29_Pos) //!< 0x20000000 
.equ CAN_F7R2_FB29, CAN_F7R2_FB29_Msk //!<Filter bit 29 
.equ CAN_F7R2_FB30_Pos, (30) 
.equ CAN_F7R2_FB30_Msk, (0x1 << CAN_F7R2_FB30_Pos) //!< 0x40000000 
.equ CAN_F7R2_FB30, CAN_F7R2_FB30_Msk //!<Filter bit 30 
.equ CAN_F7R2_FB31_Pos, (31) 
.equ CAN_F7R2_FB31_Msk, (0x1 << CAN_F7R2_FB31_Pos) //!< 0x80000000 
.equ CAN_F7R2_FB31, CAN_F7R2_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F8R2 register  ******************
.equ CAN_F8R2_FB0_Pos, (0) 
.equ CAN_F8R2_FB0_Msk, (0x1 << CAN_F8R2_FB0_Pos) //!< 0x00000001 
.equ CAN_F8R2_FB0, CAN_F8R2_FB0_Msk //!<Filter bit 0 
.equ CAN_F8R2_FB1_Pos, (1) 
.equ CAN_F8R2_FB1_Msk, (0x1 << CAN_F8R2_FB1_Pos) //!< 0x00000002 
.equ CAN_F8R2_FB1, CAN_F8R2_FB1_Msk //!<Filter bit 1 
.equ CAN_F8R2_FB2_Pos, (2) 
.equ CAN_F8R2_FB2_Msk, (0x1 << CAN_F8R2_FB2_Pos) //!< 0x00000004 
.equ CAN_F8R2_FB2, CAN_F8R2_FB2_Msk //!<Filter bit 2 
.equ CAN_F8R2_FB3_Pos, (3) 
.equ CAN_F8R2_FB3_Msk, (0x1 << CAN_F8R2_FB3_Pos) //!< 0x00000008 
.equ CAN_F8R2_FB3, CAN_F8R2_FB3_Msk //!<Filter bit 3 
.equ CAN_F8R2_FB4_Pos, (4) 
.equ CAN_F8R2_FB4_Msk, (0x1 << CAN_F8R2_FB4_Pos) //!< 0x00000010 
.equ CAN_F8R2_FB4, CAN_F8R2_FB4_Msk //!<Filter bit 4 
.equ CAN_F8R2_FB5_Pos, (5) 
.equ CAN_F8R2_FB5_Msk, (0x1 << CAN_F8R2_FB5_Pos) //!< 0x00000020 
.equ CAN_F8R2_FB5, CAN_F8R2_FB5_Msk //!<Filter bit 5 
.equ CAN_F8R2_FB6_Pos, (6) 
.equ CAN_F8R2_FB6_Msk, (0x1 << CAN_F8R2_FB6_Pos) //!< 0x00000040 
.equ CAN_F8R2_FB6, CAN_F8R2_FB6_Msk //!<Filter bit 6 
.equ CAN_F8R2_FB7_Pos, (7) 
.equ CAN_F8R2_FB7_Msk, (0x1 << CAN_F8R2_FB7_Pos) //!< 0x00000080 
.equ CAN_F8R2_FB7, CAN_F8R2_FB7_Msk //!<Filter bit 7 
.equ CAN_F8R2_FB8_Pos, (8) 
.equ CAN_F8R2_FB8_Msk, (0x1 << CAN_F8R2_FB8_Pos) //!< 0x00000100 
.equ CAN_F8R2_FB8, CAN_F8R2_FB8_Msk //!<Filter bit 8 
.equ CAN_F8R2_FB9_Pos, (9) 
.equ CAN_F8R2_FB9_Msk, (0x1 << CAN_F8R2_FB9_Pos) //!< 0x00000200 
.equ CAN_F8R2_FB9, CAN_F8R2_FB9_Msk //!<Filter bit 9 
.equ CAN_F8R2_FB10_Pos, (10) 
.equ CAN_F8R2_FB10_Msk, (0x1 << CAN_F8R2_FB10_Pos) //!< 0x00000400 
.equ CAN_F8R2_FB10, CAN_F8R2_FB10_Msk //!<Filter bit 10 
.equ CAN_F8R2_FB11_Pos, (11) 
.equ CAN_F8R2_FB11_Msk, (0x1 << CAN_F8R2_FB11_Pos) //!< 0x00000800 
.equ CAN_F8R2_FB11, CAN_F8R2_FB11_Msk //!<Filter bit 11 
.equ CAN_F8R2_FB12_Pos, (12) 
.equ CAN_F8R2_FB12_Msk, (0x1 << CAN_F8R2_FB12_Pos) //!< 0x00001000 
.equ CAN_F8R2_FB12, CAN_F8R2_FB12_Msk //!<Filter bit 12 
.equ CAN_F8R2_FB13_Pos, (13) 
.equ CAN_F8R2_FB13_Msk, (0x1 << CAN_F8R2_FB13_Pos) //!< 0x00002000 
.equ CAN_F8R2_FB13, CAN_F8R2_FB13_Msk //!<Filter bit 13 
.equ CAN_F8R2_FB14_Pos, (14) 
.equ CAN_F8R2_FB14_Msk, (0x1 << CAN_F8R2_FB14_Pos) //!< 0x00004000 
.equ CAN_F8R2_FB14, CAN_F8R2_FB14_Msk //!<Filter bit 14 
.equ CAN_F8R2_FB15_Pos, (15) 
.equ CAN_F8R2_FB15_Msk, (0x1 << CAN_F8R2_FB15_Pos) //!< 0x00008000 
.equ CAN_F8R2_FB15, CAN_F8R2_FB15_Msk //!<Filter bit 15 
.equ CAN_F8R2_FB16_Pos, (16) 
.equ CAN_F8R2_FB16_Msk, (0x1 << CAN_F8R2_FB16_Pos) //!< 0x00010000 
.equ CAN_F8R2_FB16, CAN_F8R2_FB16_Msk //!<Filter bit 16 
.equ CAN_F8R2_FB17_Pos, (17) 
.equ CAN_F8R2_FB17_Msk, (0x1 << CAN_F8R2_FB17_Pos) //!< 0x00020000 
.equ CAN_F8R2_FB17, CAN_F8R2_FB17_Msk //!<Filter bit 17 
.equ CAN_F8R2_FB18_Pos, (18) 
.equ CAN_F8R2_FB18_Msk, (0x1 << CAN_F8R2_FB18_Pos) //!< 0x00040000 
.equ CAN_F8R2_FB18, CAN_F8R2_FB18_Msk //!<Filter bit 18 
.equ CAN_F8R2_FB19_Pos, (19) 
.equ CAN_F8R2_FB19_Msk, (0x1 << CAN_F8R2_FB19_Pos) //!< 0x00080000 
.equ CAN_F8R2_FB19, CAN_F8R2_FB19_Msk //!<Filter bit 19 
.equ CAN_F8R2_FB20_Pos, (20) 
.equ CAN_F8R2_FB20_Msk, (0x1 << CAN_F8R2_FB20_Pos) //!< 0x00100000 
.equ CAN_F8R2_FB20, CAN_F8R2_FB20_Msk //!<Filter bit 20 
.equ CAN_F8R2_FB21_Pos, (21) 
.equ CAN_F8R2_FB21_Msk, (0x1 << CAN_F8R2_FB21_Pos) //!< 0x00200000 
.equ CAN_F8R2_FB21, CAN_F8R2_FB21_Msk //!<Filter bit 21 
.equ CAN_F8R2_FB22_Pos, (22) 
.equ CAN_F8R2_FB22_Msk, (0x1 << CAN_F8R2_FB22_Pos) //!< 0x00400000 
.equ CAN_F8R2_FB22, CAN_F8R2_FB22_Msk //!<Filter bit 22 
.equ CAN_F8R2_FB23_Pos, (23) 
.equ CAN_F8R2_FB23_Msk, (0x1 << CAN_F8R2_FB23_Pos) //!< 0x00800000 
.equ CAN_F8R2_FB23, CAN_F8R2_FB23_Msk //!<Filter bit 23 
.equ CAN_F8R2_FB24_Pos, (24) 
.equ CAN_F8R2_FB24_Msk, (0x1 << CAN_F8R2_FB24_Pos) //!< 0x01000000 
.equ CAN_F8R2_FB24, CAN_F8R2_FB24_Msk //!<Filter bit 24 
.equ CAN_F8R2_FB25_Pos, (25) 
.equ CAN_F8R2_FB25_Msk, (0x1 << CAN_F8R2_FB25_Pos) //!< 0x02000000 
.equ CAN_F8R2_FB25, CAN_F8R2_FB25_Msk //!<Filter bit 25 
.equ CAN_F8R2_FB26_Pos, (26) 
.equ CAN_F8R2_FB26_Msk, (0x1 << CAN_F8R2_FB26_Pos) //!< 0x04000000 
.equ CAN_F8R2_FB26, CAN_F8R2_FB26_Msk //!<Filter bit 26 
.equ CAN_F8R2_FB27_Pos, (27) 
.equ CAN_F8R2_FB27_Msk, (0x1 << CAN_F8R2_FB27_Pos) //!< 0x08000000 
.equ CAN_F8R2_FB27, CAN_F8R2_FB27_Msk //!<Filter bit 27 
.equ CAN_F8R2_FB28_Pos, (28) 
.equ CAN_F8R2_FB28_Msk, (0x1 << CAN_F8R2_FB28_Pos) //!< 0x10000000 
.equ CAN_F8R2_FB28, CAN_F8R2_FB28_Msk //!<Filter bit 28 
.equ CAN_F8R2_FB29_Pos, (29) 
.equ CAN_F8R2_FB29_Msk, (0x1 << CAN_F8R2_FB29_Pos) //!< 0x20000000 
.equ CAN_F8R2_FB29, CAN_F8R2_FB29_Msk //!<Filter bit 29 
.equ CAN_F8R2_FB30_Pos, (30) 
.equ CAN_F8R2_FB30_Msk, (0x1 << CAN_F8R2_FB30_Pos) //!< 0x40000000 
.equ CAN_F8R2_FB30, CAN_F8R2_FB30_Msk //!<Filter bit 30 
.equ CAN_F8R2_FB31_Pos, (31) 
.equ CAN_F8R2_FB31_Msk, (0x1 << CAN_F8R2_FB31_Pos) //!< 0x80000000 
.equ CAN_F8R2_FB31, CAN_F8R2_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F9R2 register  ******************
.equ CAN_F9R2_FB0_Pos, (0) 
.equ CAN_F9R2_FB0_Msk, (0x1 << CAN_F9R2_FB0_Pos) //!< 0x00000001 
.equ CAN_F9R2_FB0, CAN_F9R2_FB0_Msk //!<Filter bit 0 
.equ CAN_F9R2_FB1_Pos, (1) 
.equ CAN_F9R2_FB1_Msk, (0x1 << CAN_F9R2_FB1_Pos) //!< 0x00000002 
.equ CAN_F9R2_FB1, CAN_F9R2_FB1_Msk //!<Filter bit 1 
.equ CAN_F9R2_FB2_Pos, (2) 
.equ CAN_F9R2_FB2_Msk, (0x1 << CAN_F9R2_FB2_Pos) //!< 0x00000004 
.equ CAN_F9R2_FB2, CAN_F9R2_FB2_Msk //!<Filter bit 2 
.equ CAN_F9R2_FB3_Pos, (3) 
.equ CAN_F9R2_FB3_Msk, (0x1 << CAN_F9R2_FB3_Pos) //!< 0x00000008 
.equ CAN_F9R2_FB3, CAN_F9R2_FB3_Msk //!<Filter bit 3 
.equ CAN_F9R2_FB4_Pos, (4) 
.equ CAN_F9R2_FB4_Msk, (0x1 << CAN_F9R2_FB4_Pos) //!< 0x00000010 
.equ CAN_F9R2_FB4, CAN_F9R2_FB4_Msk //!<Filter bit 4 
.equ CAN_F9R2_FB5_Pos, (5) 
.equ CAN_F9R2_FB5_Msk, (0x1 << CAN_F9R2_FB5_Pos) //!< 0x00000020 
.equ CAN_F9R2_FB5, CAN_F9R2_FB5_Msk //!<Filter bit 5 
.equ CAN_F9R2_FB6_Pos, (6) 
.equ CAN_F9R2_FB6_Msk, (0x1 << CAN_F9R2_FB6_Pos) //!< 0x00000040 
.equ CAN_F9R2_FB6, CAN_F9R2_FB6_Msk //!<Filter bit 6 
.equ CAN_F9R2_FB7_Pos, (7) 
.equ CAN_F9R2_FB7_Msk, (0x1 << CAN_F9R2_FB7_Pos) //!< 0x00000080 
.equ CAN_F9R2_FB7, CAN_F9R2_FB7_Msk //!<Filter bit 7 
.equ CAN_F9R2_FB8_Pos, (8) 
.equ CAN_F9R2_FB8_Msk, (0x1 << CAN_F9R2_FB8_Pos) //!< 0x00000100 
.equ CAN_F9R2_FB8, CAN_F9R2_FB8_Msk //!<Filter bit 8 
.equ CAN_F9R2_FB9_Pos, (9) 
.equ CAN_F9R2_FB9_Msk, (0x1 << CAN_F9R2_FB9_Pos) //!< 0x00000200 
.equ CAN_F9R2_FB9, CAN_F9R2_FB9_Msk //!<Filter bit 9 
.equ CAN_F9R2_FB10_Pos, (10) 
.equ CAN_F9R2_FB10_Msk, (0x1 << CAN_F9R2_FB10_Pos) //!< 0x00000400 
.equ CAN_F9R2_FB10, CAN_F9R2_FB10_Msk //!<Filter bit 10 
.equ CAN_F9R2_FB11_Pos, (11) 
.equ CAN_F9R2_FB11_Msk, (0x1 << CAN_F9R2_FB11_Pos) //!< 0x00000800 
.equ CAN_F9R2_FB11, CAN_F9R2_FB11_Msk //!<Filter bit 11 
.equ CAN_F9R2_FB12_Pos, (12) 
.equ CAN_F9R2_FB12_Msk, (0x1 << CAN_F9R2_FB12_Pos) //!< 0x00001000 
.equ CAN_F9R2_FB12, CAN_F9R2_FB12_Msk //!<Filter bit 12 
.equ CAN_F9R2_FB13_Pos, (13) 
.equ CAN_F9R2_FB13_Msk, (0x1 << CAN_F9R2_FB13_Pos) //!< 0x00002000 
.equ CAN_F9R2_FB13, CAN_F9R2_FB13_Msk //!<Filter bit 13 
.equ CAN_F9R2_FB14_Pos, (14) 
.equ CAN_F9R2_FB14_Msk, (0x1 << CAN_F9R2_FB14_Pos) //!< 0x00004000 
.equ CAN_F9R2_FB14, CAN_F9R2_FB14_Msk //!<Filter bit 14 
.equ CAN_F9R2_FB15_Pos, (15) 
.equ CAN_F9R2_FB15_Msk, (0x1 << CAN_F9R2_FB15_Pos) //!< 0x00008000 
.equ CAN_F9R2_FB15, CAN_F9R2_FB15_Msk //!<Filter bit 15 
.equ CAN_F9R2_FB16_Pos, (16) 
.equ CAN_F9R2_FB16_Msk, (0x1 << CAN_F9R2_FB16_Pos) //!< 0x00010000 
.equ CAN_F9R2_FB16, CAN_F9R2_FB16_Msk //!<Filter bit 16 
.equ CAN_F9R2_FB17_Pos, (17) 
.equ CAN_F9R2_FB17_Msk, (0x1 << CAN_F9R2_FB17_Pos) //!< 0x00020000 
.equ CAN_F9R2_FB17, CAN_F9R2_FB17_Msk //!<Filter bit 17 
.equ CAN_F9R2_FB18_Pos, (18) 
.equ CAN_F9R2_FB18_Msk, (0x1 << CAN_F9R2_FB18_Pos) //!< 0x00040000 
.equ CAN_F9R2_FB18, CAN_F9R2_FB18_Msk //!<Filter bit 18 
.equ CAN_F9R2_FB19_Pos, (19) 
.equ CAN_F9R2_FB19_Msk, (0x1 << CAN_F9R2_FB19_Pos) //!< 0x00080000 
.equ CAN_F9R2_FB19, CAN_F9R2_FB19_Msk //!<Filter bit 19 
.equ CAN_F9R2_FB20_Pos, (20) 
.equ CAN_F9R2_FB20_Msk, (0x1 << CAN_F9R2_FB20_Pos) //!< 0x00100000 
.equ CAN_F9R2_FB20, CAN_F9R2_FB20_Msk //!<Filter bit 20 
.equ CAN_F9R2_FB21_Pos, (21) 
.equ CAN_F9R2_FB21_Msk, (0x1 << CAN_F9R2_FB21_Pos) //!< 0x00200000 
.equ CAN_F9R2_FB21, CAN_F9R2_FB21_Msk //!<Filter bit 21 
.equ CAN_F9R2_FB22_Pos, (22) 
.equ CAN_F9R2_FB22_Msk, (0x1 << CAN_F9R2_FB22_Pos) //!< 0x00400000 
.equ CAN_F9R2_FB22, CAN_F9R2_FB22_Msk //!<Filter bit 22 
.equ CAN_F9R2_FB23_Pos, (23) 
.equ CAN_F9R2_FB23_Msk, (0x1 << CAN_F9R2_FB23_Pos) //!< 0x00800000 
.equ CAN_F9R2_FB23, CAN_F9R2_FB23_Msk //!<Filter bit 23 
.equ CAN_F9R2_FB24_Pos, (24) 
.equ CAN_F9R2_FB24_Msk, (0x1 << CAN_F9R2_FB24_Pos) //!< 0x01000000 
.equ CAN_F9R2_FB24, CAN_F9R2_FB24_Msk //!<Filter bit 24 
.equ CAN_F9R2_FB25_Pos, (25) 
.equ CAN_F9R2_FB25_Msk, (0x1 << CAN_F9R2_FB25_Pos) //!< 0x02000000 
.equ CAN_F9R2_FB25, CAN_F9R2_FB25_Msk //!<Filter bit 25 
.equ CAN_F9R2_FB26_Pos, (26) 
.equ CAN_F9R2_FB26_Msk, (0x1 << CAN_F9R2_FB26_Pos) //!< 0x04000000 
.equ CAN_F9R2_FB26, CAN_F9R2_FB26_Msk //!<Filter bit 26 
.equ CAN_F9R2_FB27_Pos, (27) 
.equ CAN_F9R2_FB27_Msk, (0x1 << CAN_F9R2_FB27_Pos) //!< 0x08000000 
.equ CAN_F9R2_FB27, CAN_F9R2_FB27_Msk //!<Filter bit 27 
.equ CAN_F9R2_FB28_Pos, (28) 
.equ CAN_F9R2_FB28_Msk, (0x1 << CAN_F9R2_FB28_Pos) //!< 0x10000000 
.equ CAN_F9R2_FB28, CAN_F9R2_FB28_Msk //!<Filter bit 28 
.equ CAN_F9R2_FB29_Pos, (29) 
.equ CAN_F9R2_FB29_Msk, (0x1 << CAN_F9R2_FB29_Pos) //!< 0x20000000 
.equ CAN_F9R2_FB29, CAN_F9R2_FB29_Msk //!<Filter bit 29 
.equ CAN_F9R2_FB30_Pos, (30) 
.equ CAN_F9R2_FB30_Msk, (0x1 << CAN_F9R2_FB30_Pos) //!< 0x40000000 
.equ CAN_F9R2_FB30, CAN_F9R2_FB30_Msk //!<Filter bit 30 
.equ CAN_F9R2_FB31_Pos, (31) 
.equ CAN_F9R2_FB31_Msk, (0x1 << CAN_F9R2_FB31_Pos) //!< 0x80000000 
.equ CAN_F9R2_FB31, CAN_F9R2_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F10R2 register  *****************
.equ CAN_F10R2_FB0_Pos, (0) 
.equ CAN_F10R2_FB0_Msk, (0x1 << CAN_F10R2_FB0_Pos) //!< 0x00000001 
.equ CAN_F10R2_FB0, CAN_F10R2_FB0_Msk //!<Filter bit 0 
.equ CAN_F10R2_FB1_Pos, (1) 
.equ CAN_F10R2_FB1_Msk, (0x1 << CAN_F10R2_FB1_Pos) //!< 0x00000002 
.equ CAN_F10R2_FB1, CAN_F10R2_FB1_Msk //!<Filter bit 1 
.equ CAN_F10R2_FB2_Pos, (2) 
.equ CAN_F10R2_FB2_Msk, (0x1 << CAN_F10R2_FB2_Pos) //!< 0x00000004 
.equ CAN_F10R2_FB2, CAN_F10R2_FB2_Msk //!<Filter bit 2 
.equ CAN_F10R2_FB3_Pos, (3) 
.equ CAN_F10R2_FB3_Msk, (0x1 << CAN_F10R2_FB3_Pos) //!< 0x00000008 
.equ CAN_F10R2_FB3, CAN_F10R2_FB3_Msk //!<Filter bit 3 
.equ CAN_F10R2_FB4_Pos, (4) 
.equ CAN_F10R2_FB4_Msk, (0x1 << CAN_F10R2_FB4_Pos) //!< 0x00000010 
.equ CAN_F10R2_FB4, CAN_F10R2_FB4_Msk //!<Filter bit 4 
.equ CAN_F10R2_FB5_Pos, (5) 
.equ CAN_F10R2_FB5_Msk, (0x1 << CAN_F10R2_FB5_Pos) //!< 0x00000020 
.equ CAN_F10R2_FB5, CAN_F10R2_FB5_Msk //!<Filter bit 5 
.equ CAN_F10R2_FB6_Pos, (6) 
.equ CAN_F10R2_FB6_Msk, (0x1 << CAN_F10R2_FB6_Pos) //!< 0x00000040 
.equ CAN_F10R2_FB6, CAN_F10R2_FB6_Msk //!<Filter bit 6 
.equ CAN_F10R2_FB7_Pos, (7) 
.equ CAN_F10R2_FB7_Msk, (0x1 << CAN_F10R2_FB7_Pos) //!< 0x00000080 
.equ CAN_F10R2_FB7, CAN_F10R2_FB7_Msk //!<Filter bit 7 
.equ CAN_F10R2_FB8_Pos, (8) 
.equ CAN_F10R2_FB8_Msk, (0x1 << CAN_F10R2_FB8_Pos) //!< 0x00000100 
.equ CAN_F10R2_FB8, CAN_F10R2_FB8_Msk //!<Filter bit 8 
.equ CAN_F10R2_FB9_Pos, (9) 
.equ CAN_F10R2_FB9_Msk, (0x1 << CAN_F10R2_FB9_Pos) //!< 0x00000200 
.equ CAN_F10R2_FB9, CAN_F10R2_FB9_Msk //!<Filter bit 9 
.equ CAN_F10R2_FB10_Pos, (10) 
.equ CAN_F10R2_FB10_Msk, (0x1 << CAN_F10R2_FB10_Pos) //!< 0x00000400 
.equ CAN_F10R2_FB10, CAN_F10R2_FB10_Msk //!<Filter bit 10 
.equ CAN_F10R2_FB11_Pos, (11) 
.equ CAN_F10R2_FB11_Msk, (0x1 << CAN_F10R2_FB11_Pos) //!< 0x00000800 
.equ CAN_F10R2_FB11, CAN_F10R2_FB11_Msk //!<Filter bit 11 
.equ CAN_F10R2_FB12_Pos, (12) 
.equ CAN_F10R2_FB12_Msk, (0x1 << CAN_F10R2_FB12_Pos) //!< 0x00001000 
.equ CAN_F10R2_FB12, CAN_F10R2_FB12_Msk //!<Filter bit 12 
.equ CAN_F10R2_FB13_Pos, (13) 
.equ CAN_F10R2_FB13_Msk, (0x1 << CAN_F10R2_FB13_Pos) //!< 0x00002000 
.equ CAN_F10R2_FB13, CAN_F10R2_FB13_Msk //!<Filter bit 13 
.equ CAN_F10R2_FB14_Pos, (14) 
.equ CAN_F10R2_FB14_Msk, (0x1 << CAN_F10R2_FB14_Pos) //!< 0x00004000 
.equ CAN_F10R2_FB14, CAN_F10R2_FB14_Msk //!<Filter bit 14 
.equ CAN_F10R2_FB15_Pos, (15) 
.equ CAN_F10R2_FB15_Msk, (0x1 << CAN_F10R2_FB15_Pos) //!< 0x00008000 
.equ CAN_F10R2_FB15, CAN_F10R2_FB15_Msk //!<Filter bit 15 
.equ CAN_F10R2_FB16_Pos, (16) 
.equ CAN_F10R2_FB16_Msk, (0x1 << CAN_F10R2_FB16_Pos) //!< 0x00010000 
.equ CAN_F10R2_FB16, CAN_F10R2_FB16_Msk //!<Filter bit 16 
.equ CAN_F10R2_FB17_Pos, (17) 
.equ CAN_F10R2_FB17_Msk, (0x1 << CAN_F10R2_FB17_Pos) //!< 0x00020000 
.equ CAN_F10R2_FB17, CAN_F10R2_FB17_Msk //!<Filter bit 17 
.equ CAN_F10R2_FB18_Pos, (18) 
.equ CAN_F10R2_FB18_Msk, (0x1 << CAN_F10R2_FB18_Pos) //!< 0x00040000 
.equ CAN_F10R2_FB18, CAN_F10R2_FB18_Msk //!<Filter bit 18 
.equ CAN_F10R2_FB19_Pos, (19) 
.equ CAN_F10R2_FB19_Msk, (0x1 << CAN_F10R2_FB19_Pos) //!< 0x00080000 
.equ CAN_F10R2_FB19, CAN_F10R2_FB19_Msk //!<Filter bit 19 
.equ CAN_F10R2_FB20_Pos, (20) 
.equ CAN_F10R2_FB20_Msk, (0x1 << CAN_F10R2_FB20_Pos) //!< 0x00100000 
.equ CAN_F10R2_FB20, CAN_F10R2_FB20_Msk //!<Filter bit 20 
.equ CAN_F10R2_FB21_Pos, (21) 
.equ CAN_F10R2_FB21_Msk, (0x1 << CAN_F10R2_FB21_Pos) //!< 0x00200000 
.equ CAN_F10R2_FB21, CAN_F10R2_FB21_Msk //!<Filter bit 21 
.equ CAN_F10R2_FB22_Pos, (22) 
.equ CAN_F10R2_FB22_Msk, (0x1 << CAN_F10R2_FB22_Pos) //!< 0x00400000 
.equ CAN_F10R2_FB22, CAN_F10R2_FB22_Msk //!<Filter bit 22 
.equ CAN_F10R2_FB23_Pos, (23) 
.equ CAN_F10R2_FB23_Msk, (0x1 << CAN_F10R2_FB23_Pos) //!< 0x00800000 
.equ CAN_F10R2_FB23, CAN_F10R2_FB23_Msk //!<Filter bit 23 
.equ CAN_F10R2_FB24_Pos, (24) 
.equ CAN_F10R2_FB24_Msk, (0x1 << CAN_F10R2_FB24_Pos) //!< 0x01000000 
.equ CAN_F10R2_FB24, CAN_F10R2_FB24_Msk //!<Filter bit 24 
.equ CAN_F10R2_FB25_Pos, (25) 
.equ CAN_F10R2_FB25_Msk, (0x1 << CAN_F10R2_FB25_Pos) //!< 0x02000000 
.equ CAN_F10R2_FB25, CAN_F10R2_FB25_Msk //!<Filter bit 25 
.equ CAN_F10R2_FB26_Pos, (26) 
.equ CAN_F10R2_FB26_Msk, (0x1 << CAN_F10R2_FB26_Pos) //!< 0x04000000 
.equ CAN_F10R2_FB26, CAN_F10R2_FB26_Msk //!<Filter bit 26 
.equ CAN_F10R2_FB27_Pos, (27) 
.equ CAN_F10R2_FB27_Msk, (0x1 << CAN_F10R2_FB27_Pos) //!< 0x08000000 
.equ CAN_F10R2_FB27, CAN_F10R2_FB27_Msk //!<Filter bit 27 
.equ CAN_F10R2_FB28_Pos, (28) 
.equ CAN_F10R2_FB28_Msk, (0x1 << CAN_F10R2_FB28_Pos) //!< 0x10000000 
.equ CAN_F10R2_FB28, CAN_F10R2_FB28_Msk //!<Filter bit 28 
.equ CAN_F10R2_FB29_Pos, (29) 
.equ CAN_F10R2_FB29_Msk, (0x1 << CAN_F10R2_FB29_Pos) //!< 0x20000000 
.equ CAN_F10R2_FB29, CAN_F10R2_FB29_Msk //!<Filter bit 29 
.equ CAN_F10R2_FB30_Pos, (30) 
.equ CAN_F10R2_FB30_Msk, (0x1 << CAN_F10R2_FB30_Pos) //!< 0x40000000 
.equ CAN_F10R2_FB30, CAN_F10R2_FB30_Msk //!<Filter bit 30 
.equ CAN_F10R2_FB31_Pos, (31) 
.equ CAN_F10R2_FB31_Msk, (0x1 << CAN_F10R2_FB31_Pos) //!< 0x80000000 
.equ CAN_F10R2_FB31, CAN_F10R2_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F11R2 register  *****************
.equ CAN_F11R2_FB0_Pos, (0) 
.equ CAN_F11R2_FB0_Msk, (0x1 << CAN_F11R2_FB0_Pos) //!< 0x00000001 
.equ CAN_F11R2_FB0, CAN_F11R2_FB0_Msk //!<Filter bit 0 
.equ CAN_F11R2_FB1_Pos, (1) 
.equ CAN_F11R2_FB1_Msk, (0x1 << CAN_F11R2_FB1_Pos) //!< 0x00000002 
.equ CAN_F11R2_FB1, CAN_F11R2_FB1_Msk //!<Filter bit 1 
.equ CAN_F11R2_FB2_Pos, (2) 
.equ CAN_F11R2_FB2_Msk, (0x1 << CAN_F11R2_FB2_Pos) //!< 0x00000004 
.equ CAN_F11R2_FB2, CAN_F11R2_FB2_Msk //!<Filter bit 2 
.equ CAN_F11R2_FB3_Pos, (3) 
.equ CAN_F11R2_FB3_Msk, (0x1 << CAN_F11R2_FB3_Pos) //!< 0x00000008 
.equ CAN_F11R2_FB3, CAN_F11R2_FB3_Msk //!<Filter bit 3 
.equ CAN_F11R2_FB4_Pos, (4) 
.equ CAN_F11R2_FB4_Msk, (0x1 << CAN_F11R2_FB4_Pos) //!< 0x00000010 
.equ CAN_F11R2_FB4, CAN_F11R2_FB4_Msk //!<Filter bit 4 
.equ CAN_F11R2_FB5_Pos, (5) 
.equ CAN_F11R2_FB5_Msk, (0x1 << CAN_F11R2_FB5_Pos) //!< 0x00000020 
.equ CAN_F11R2_FB5, CAN_F11R2_FB5_Msk //!<Filter bit 5 
.equ CAN_F11R2_FB6_Pos, (6) 
.equ CAN_F11R2_FB6_Msk, (0x1 << CAN_F11R2_FB6_Pos) //!< 0x00000040 
.equ CAN_F11R2_FB6, CAN_F11R2_FB6_Msk //!<Filter bit 6 
.equ CAN_F11R2_FB7_Pos, (7) 
.equ CAN_F11R2_FB7_Msk, (0x1 << CAN_F11R2_FB7_Pos) //!< 0x00000080 
.equ CAN_F11R2_FB7, CAN_F11R2_FB7_Msk //!<Filter bit 7 
.equ CAN_F11R2_FB8_Pos, (8) 
.equ CAN_F11R2_FB8_Msk, (0x1 << CAN_F11R2_FB8_Pos) //!< 0x00000100 
.equ CAN_F11R2_FB8, CAN_F11R2_FB8_Msk //!<Filter bit 8 
.equ CAN_F11R2_FB9_Pos, (9) 
.equ CAN_F11R2_FB9_Msk, (0x1 << CAN_F11R2_FB9_Pos) //!< 0x00000200 
.equ CAN_F11R2_FB9, CAN_F11R2_FB9_Msk //!<Filter bit 9 
.equ CAN_F11R2_FB10_Pos, (10) 
.equ CAN_F11R2_FB10_Msk, (0x1 << CAN_F11R2_FB10_Pos) //!< 0x00000400 
.equ CAN_F11R2_FB10, CAN_F11R2_FB10_Msk //!<Filter bit 10 
.equ CAN_F11R2_FB11_Pos, (11) 
.equ CAN_F11R2_FB11_Msk, (0x1 << CAN_F11R2_FB11_Pos) //!< 0x00000800 
.equ CAN_F11R2_FB11, CAN_F11R2_FB11_Msk //!<Filter bit 11 
.equ CAN_F11R2_FB12_Pos, (12) 
.equ CAN_F11R2_FB12_Msk, (0x1 << CAN_F11R2_FB12_Pos) //!< 0x00001000 
.equ CAN_F11R2_FB12, CAN_F11R2_FB12_Msk //!<Filter bit 12 
.equ CAN_F11R2_FB13_Pos, (13) 
.equ CAN_F11R2_FB13_Msk, (0x1 << CAN_F11R2_FB13_Pos) //!< 0x00002000 
.equ CAN_F11R2_FB13, CAN_F11R2_FB13_Msk //!<Filter bit 13 
.equ CAN_F11R2_FB14_Pos, (14) 
.equ CAN_F11R2_FB14_Msk, (0x1 << CAN_F11R2_FB14_Pos) //!< 0x00004000 
.equ CAN_F11R2_FB14, CAN_F11R2_FB14_Msk //!<Filter bit 14 
.equ CAN_F11R2_FB15_Pos, (15) 
.equ CAN_F11R2_FB15_Msk, (0x1 << CAN_F11R2_FB15_Pos) //!< 0x00008000 
.equ CAN_F11R2_FB15, CAN_F11R2_FB15_Msk //!<Filter bit 15 
.equ CAN_F11R2_FB16_Pos, (16) 
.equ CAN_F11R2_FB16_Msk, (0x1 << CAN_F11R2_FB16_Pos) //!< 0x00010000 
.equ CAN_F11R2_FB16, CAN_F11R2_FB16_Msk //!<Filter bit 16 
.equ CAN_F11R2_FB17_Pos, (17) 
.equ CAN_F11R2_FB17_Msk, (0x1 << CAN_F11R2_FB17_Pos) //!< 0x00020000 
.equ CAN_F11R2_FB17, CAN_F11R2_FB17_Msk //!<Filter bit 17 
.equ CAN_F11R2_FB18_Pos, (18) 
.equ CAN_F11R2_FB18_Msk, (0x1 << CAN_F11R2_FB18_Pos) //!< 0x00040000 
.equ CAN_F11R2_FB18, CAN_F11R2_FB18_Msk //!<Filter bit 18 
.equ CAN_F11R2_FB19_Pos, (19) 
.equ CAN_F11R2_FB19_Msk, (0x1 << CAN_F11R2_FB19_Pos) //!< 0x00080000 
.equ CAN_F11R2_FB19, CAN_F11R2_FB19_Msk //!<Filter bit 19 
.equ CAN_F11R2_FB20_Pos, (20) 
.equ CAN_F11R2_FB20_Msk, (0x1 << CAN_F11R2_FB20_Pos) //!< 0x00100000 
.equ CAN_F11R2_FB20, CAN_F11R2_FB20_Msk //!<Filter bit 20 
.equ CAN_F11R2_FB21_Pos, (21) 
.equ CAN_F11R2_FB21_Msk, (0x1 << CAN_F11R2_FB21_Pos) //!< 0x00200000 
.equ CAN_F11R2_FB21, CAN_F11R2_FB21_Msk //!<Filter bit 21 
.equ CAN_F11R2_FB22_Pos, (22) 
.equ CAN_F11R2_FB22_Msk, (0x1 << CAN_F11R2_FB22_Pos) //!< 0x00400000 
.equ CAN_F11R2_FB22, CAN_F11R2_FB22_Msk //!<Filter bit 22 
.equ CAN_F11R2_FB23_Pos, (23) 
.equ CAN_F11R2_FB23_Msk, (0x1 << CAN_F11R2_FB23_Pos) //!< 0x00800000 
.equ CAN_F11R2_FB23, CAN_F11R2_FB23_Msk //!<Filter bit 23 
.equ CAN_F11R2_FB24_Pos, (24) 
.equ CAN_F11R2_FB24_Msk, (0x1 << CAN_F11R2_FB24_Pos) //!< 0x01000000 
.equ CAN_F11R2_FB24, CAN_F11R2_FB24_Msk //!<Filter bit 24 
.equ CAN_F11R2_FB25_Pos, (25) 
.equ CAN_F11R2_FB25_Msk, (0x1 << CAN_F11R2_FB25_Pos) //!< 0x02000000 
.equ CAN_F11R2_FB25, CAN_F11R2_FB25_Msk //!<Filter bit 25 
.equ CAN_F11R2_FB26_Pos, (26) 
.equ CAN_F11R2_FB26_Msk, (0x1 << CAN_F11R2_FB26_Pos) //!< 0x04000000 
.equ CAN_F11R2_FB26, CAN_F11R2_FB26_Msk //!<Filter bit 26 
.equ CAN_F11R2_FB27_Pos, (27) 
.equ CAN_F11R2_FB27_Msk, (0x1 << CAN_F11R2_FB27_Pos) //!< 0x08000000 
.equ CAN_F11R2_FB27, CAN_F11R2_FB27_Msk //!<Filter bit 27 
.equ CAN_F11R2_FB28_Pos, (28) 
.equ CAN_F11R2_FB28_Msk, (0x1 << CAN_F11R2_FB28_Pos) //!< 0x10000000 
.equ CAN_F11R2_FB28, CAN_F11R2_FB28_Msk //!<Filter bit 28 
.equ CAN_F11R2_FB29_Pos, (29) 
.equ CAN_F11R2_FB29_Msk, (0x1 << CAN_F11R2_FB29_Pos) //!< 0x20000000 
.equ CAN_F11R2_FB29, CAN_F11R2_FB29_Msk //!<Filter bit 29 
.equ CAN_F11R2_FB30_Pos, (30) 
.equ CAN_F11R2_FB30_Msk, (0x1 << CAN_F11R2_FB30_Pos) //!< 0x40000000 
.equ CAN_F11R2_FB30, CAN_F11R2_FB30_Msk //!<Filter bit 30 
.equ CAN_F11R2_FB31_Pos, (31) 
.equ CAN_F11R2_FB31_Msk, (0x1 << CAN_F11R2_FB31_Pos) //!< 0x80000000 
.equ CAN_F11R2_FB31, CAN_F11R2_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F12R2 register  *****************
.equ CAN_F12R2_FB0_Pos, (0) 
.equ CAN_F12R2_FB0_Msk, (0x1 << CAN_F12R2_FB0_Pos) //!< 0x00000001 
.equ CAN_F12R2_FB0, CAN_F12R2_FB0_Msk //!<Filter bit 0 
.equ CAN_F12R2_FB1_Pos, (1) 
.equ CAN_F12R2_FB1_Msk, (0x1 << CAN_F12R2_FB1_Pos) //!< 0x00000002 
.equ CAN_F12R2_FB1, CAN_F12R2_FB1_Msk //!<Filter bit 1 
.equ CAN_F12R2_FB2_Pos, (2) 
.equ CAN_F12R2_FB2_Msk, (0x1 << CAN_F12R2_FB2_Pos) //!< 0x00000004 
.equ CAN_F12R2_FB2, CAN_F12R2_FB2_Msk //!<Filter bit 2 
.equ CAN_F12R2_FB3_Pos, (3) 
.equ CAN_F12R2_FB3_Msk, (0x1 << CAN_F12R2_FB3_Pos) //!< 0x00000008 
.equ CAN_F12R2_FB3, CAN_F12R2_FB3_Msk //!<Filter bit 3 
.equ CAN_F12R2_FB4_Pos, (4) 
.equ CAN_F12R2_FB4_Msk, (0x1 << CAN_F12R2_FB4_Pos) //!< 0x00000010 
.equ CAN_F12R2_FB4, CAN_F12R2_FB4_Msk //!<Filter bit 4 
.equ CAN_F12R2_FB5_Pos, (5) 
.equ CAN_F12R2_FB5_Msk, (0x1 << CAN_F12R2_FB5_Pos) //!< 0x00000020 
.equ CAN_F12R2_FB5, CAN_F12R2_FB5_Msk //!<Filter bit 5 
.equ CAN_F12R2_FB6_Pos, (6) 
.equ CAN_F12R2_FB6_Msk, (0x1 << CAN_F12R2_FB6_Pos) //!< 0x00000040 
.equ CAN_F12R2_FB6, CAN_F12R2_FB6_Msk //!<Filter bit 6 
.equ CAN_F12R2_FB7_Pos, (7) 
.equ CAN_F12R2_FB7_Msk, (0x1 << CAN_F12R2_FB7_Pos) //!< 0x00000080 
.equ CAN_F12R2_FB7, CAN_F12R2_FB7_Msk //!<Filter bit 7 
.equ CAN_F12R2_FB8_Pos, (8) 
.equ CAN_F12R2_FB8_Msk, (0x1 << CAN_F12R2_FB8_Pos) //!< 0x00000100 
.equ CAN_F12R2_FB8, CAN_F12R2_FB8_Msk //!<Filter bit 8 
.equ CAN_F12R2_FB9_Pos, (9) 
.equ CAN_F12R2_FB9_Msk, (0x1 << CAN_F12R2_FB9_Pos) //!< 0x00000200 
.equ CAN_F12R2_FB9, CAN_F12R2_FB9_Msk //!<Filter bit 9 
.equ CAN_F12R2_FB10_Pos, (10) 
.equ CAN_F12R2_FB10_Msk, (0x1 << CAN_F12R2_FB10_Pos) //!< 0x00000400 
.equ CAN_F12R2_FB10, CAN_F12R2_FB10_Msk //!<Filter bit 10 
.equ CAN_F12R2_FB11_Pos, (11) 
.equ CAN_F12R2_FB11_Msk, (0x1 << CAN_F12R2_FB11_Pos) //!< 0x00000800 
.equ CAN_F12R2_FB11, CAN_F12R2_FB11_Msk //!<Filter bit 11 
.equ CAN_F12R2_FB12_Pos, (12) 
.equ CAN_F12R2_FB12_Msk, (0x1 << CAN_F12R2_FB12_Pos) //!< 0x00001000 
.equ CAN_F12R2_FB12, CAN_F12R2_FB12_Msk //!<Filter bit 12 
.equ CAN_F12R2_FB13_Pos, (13) 
.equ CAN_F12R2_FB13_Msk, (0x1 << CAN_F12R2_FB13_Pos) //!< 0x00002000 
.equ CAN_F12R2_FB13, CAN_F12R2_FB13_Msk //!<Filter bit 13 
.equ CAN_F12R2_FB14_Pos, (14) 
.equ CAN_F12R2_FB14_Msk, (0x1 << CAN_F12R2_FB14_Pos) //!< 0x00004000 
.equ CAN_F12R2_FB14, CAN_F12R2_FB14_Msk //!<Filter bit 14 
.equ CAN_F12R2_FB15_Pos, (15) 
.equ CAN_F12R2_FB15_Msk, (0x1 << CAN_F12R2_FB15_Pos) //!< 0x00008000 
.equ CAN_F12R2_FB15, CAN_F12R2_FB15_Msk //!<Filter bit 15 
.equ CAN_F12R2_FB16_Pos, (16) 
.equ CAN_F12R2_FB16_Msk, (0x1 << CAN_F12R2_FB16_Pos) //!< 0x00010000 
.equ CAN_F12R2_FB16, CAN_F12R2_FB16_Msk //!<Filter bit 16 
.equ CAN_F12R2_FB17_Pos, (17) 
.equ CAN_F12R2_FB17_Msk, (0x1 << CAN_F12R2_FB17_Pos) //!< 0x00020000 
.equ CAN_F12R2_FB17, CAN_F12R2_FB17_Msk //!<Filter bit 17 
.equ CAN_F12R2_FB18_Pos, (18) 
.equ CAN_F12R2_FB18_Msk, (0x1 << CAN_F12R2_FB18_Pos) //!< 0x00040000 
.equ CAN_F12R2_FB18, CAN_F12R2_FB18_Msk //!<Filter bit 18 
.equ CAN_F12R2_FB19_Pos, (19) 
.equ CAN_F12R2_FB19_Msk, (0x1 << CAN_F12R2_FB19_Pos) //!< 0x00080000 
.equ CAN_F12R2_FB19, CAN_F12R2_FB19_Msk //!<Filter bit 19 
.equ CAN_F12R2_FB20_Pos, (20) 
.equ CAN_F12R2_FB20_Msk, (0x1 << CAN_F12R2_FB20_Pos) //!< 0x00100000 
.equ CAN_F12R2_FB20, CAN_F12R2_FB20_Msk //!<Filter bit 20 
.equ CAN_F12R2_FB21_Pos, (21) 
.equ CAN_F12R2_FB21_Msk, (0x1 << CAN_F12R2_FB21_Pos) //!< 0x00200000 
.equ CAN_F12R2_FB21, CAN_F12R2_FB21_Msk //!<Filter bit 21 
.equ CAN_F12R2_FB22_Pos, (22) 
.equ CAN_F12R2_FB22_Msk, (0x1 << CAN_F12R2_FB22_Pos) //!< 0x00400000 
.equ CAN_F12R2_FB22, CAN_F12R2_FB22_Msk //!<Filter bit 22 
.equ CAN_F12R2_FB23_Pos, (23) 
.equ CAN_F12R2_FB23_Msk, (0x1 << CAN_F12R2_FB23_Pos) //!< 0x00800000 
.equ CAN_F12R2_FB23, CAN_F12R2_FB23_Msk //!<Filter bit 23 
.equ CAN_F12R2_FB24_Pos, (24) 
.equ CAN_F12R2_FB24_Msk, (0x1 << CAN_F12R2_FB24_Pos) //!< 0x01000000 
.equ CAN_F12R2_FB24, CAN_F12R2_FB24_Msk //!<Filter bit 24 
.equ CAN_F12R2_FB25_Pos, (25) 
.equ CAN_F12R2_FB25_Msk, (0x1 << CAN_F12R2_FB25_Pos) //!< 0x02000000 
.equ CAN_F12R2_FB25, CAN_F12R2_FB25_Msk //!<Filter bit 25 
.equ CAN_F12R2_FB26_Pos, (26) 
.equ CAN_F12R2_FB26_Msk, (0x1 << CAN_F12R2_FB26_Pos) //!< 0x04000000 
.equ CAN_F12R2_FB26, CAN_F12R2_FB26_Msk //!<Filter bit 26 
.equ CAN_F12R2_FB27_Pos, (27) 
.equ CAN_F12R2_FB27_Msk, (0x1 << CAN_F12R2_FB27_Pos) //!< 0x08000000 
.equ CAN_F12R2_FB27, CAN_F12R2_FB27_Msk //!<Filter bit 27 
.equ CAN_F12R2_FB28_Pos, (28) 
.equ CAN_F12R2_FB28_Msk, (0x1 << CAN_F12R2_FB28_Pos) //!< 0x10000000 
.equ CAN_F12R2_FB28, CAN_F12R2_FB28_Msk //!<Filter bit 28 
.equ CAN_F12R2_FB29_Pos, (29) 
.equ CAN_F12R2_FB29_Msk, (0x1 << CAN_F12R2_FB29_Pos) //!< 0x20000000 
.equ CAN_F12R2_FB29, CAN_F12R2_FB29_Msk //!<Filter bit 29 
.equ CAN_F12R2_FB30_Pos, (30) 
.equ CAN_F12R2_FB30_Msk, (0x1 << CAN_F12R2_FB30_Pos) //!< 0x40000000 
.equ CAN_F12R2_FB30, CAN_F12R2_FB30_Msk //!<Filter bit 30 
.equ CAN_F12R2_FB31_Pos, (31) 
.equ CAN_F12R2_FB31_Msk, (0x1 << CAN_F12R2_FB31_Pos) //!< 0x80000000 
.equ CAN_F12R2_FB31, CAN_F12R2_FB31_Msk //!<Filter bit 31 
//******************  Bit definition for CAN_F13R2 register  *****************
.equ CAN_F13R2_FB0_Pos, (0) 
.equ CAN_F13R2_FB0_Msk, (0x1 << CAN_F13R2_FB0_Pos) //!< 0x00000001 
.equ CAN_F13R2_FB0, CAN_F13R2_FB0_Msk //!<Filter bit 0 
.equ CAN_F13R2_FB1_Pos, (1) 
.equ CAN_F13R2_FB1_Msk, (0x1 << CAN_F13R2_FB1_Pos) //!< 0x00000002 
.equ CAN_F13R2_FB1, CAN_F13R2_FB1_Msk //!<Filter bit 1 
.equ CAN_F13R2_FB2_Pos, (2) 
.equ CAN_F13R2_FB2_Msk, (0x1 << CAN_F13R2_FB2_Pos) //!< 0x00000004 
.equ CAN_F13R2_FB2, CAN_F13R2_FB2_Msk //!<Filter bit 2 
.equ CAN_F13R2_FB3_Pos, (3) 
.equ CAN_F13R2_FB3_Msk, (0x1 << CAN_F13R2_FB3_Pos) //!< 0x00000008 
.equ CAN_F13R2_FB3, CAN_F13R2_FB3_Msk //!<Filter bit 3 
.equ CAN_F13R2_FB4_Pos, (4) 
.equ CAN_F13R2_FB4_Msk, (0x1 << CAN_F13R2_FB4_Pos) //!< 0x00000010 
.equ CAN_F13R2_FB4, CAN_F13R2_FB4_Msk //!<Filter bit 4 
.equ CAN_F13R2_FB5_Pos, (5) 
.equ CAN_F13R2_FB5_Msk, (0x1 << CAN_F13R2_FB5_Pos) //!< 0x00000020 
.equ CAN_F13R2_FB5, CAN_F13R2_FB5_Msk //!<Filter bit 5 
.equ CAN_F13R2_FB6_Pos, (6) 
.equ CAN_F13R2_FB6_Msk, (0x1 << CAN_F13R2_FB6_Pos) //!< 0x00000040 
.equ CAN_F13R2_FB6, CAN_F13R2_FB6_Msk //!<Filter bit 6 
.equ CAN_F13R2_FB7_Pos, (7) 
.equ CAN_F13R2_FB7_Msk, (0x1 << CAN_F13R2_FB7_Pos) //!< 0x00000080 
.equ CAN_F13R2_FB7, CAN_F13R2_FB7_Msk //!<Filter bit 7 
.equ CAN_F13R2_FB8_Pos, (8) 
.equ CAN_F13R2_FB8_Msk, (0x1 << CAN_F13R2_FB8_Pos) //!< 0x00000100 
.equ CAN_F13R2_FB8, CAN_F13R2_FB8_Msk //!<Filter bit 8 
.equ CAN_F13R2_FB9_Pos, (9) 
.equ CAN_F13R2_FB9_Msk, (0x1 << CAN_F13R2_FB9_Pos) //!< 0x00000200 
.equ CAN_F13R2_FB9, CAN_F13R2_FB9_Msk //!<Filter bit 9 
.equ CAN_F13R2_FB10_Pos, (10) 
.equ CAN_F13R2_FB10_Msk, (0x1 << CAN_F13R2_FB10_Pos) //!< 0x00000400 
.equ CAN_F13R2_FB10, CAN_F13R2_FB10_Msk //!<Filter bit 10 
.equ CAN_F13R2_FB11_Pos, (11) 
.equ CAN_F13R2_FB11_Msk, (0x1 << CAN_F13R2_FB11_Pos) //!< 0x00000800 
.equ CAN_F13R2_FB11, CAN_F13R2_FB11_Msk //!<Filter bit 11 
.equ CAN_F13R2_FB12_Pos, (12) 
.equ CAN_F13R2_FB12_Msk, (0x1 << CAN_F13R2_FB12_Pos) //!< 0x00001000 
.equ CAN_F13R2_FB12, CAN_F13R2_FB12_Msk //!<Filter bit 12 
.equ CAN_F13R2_FB13_Pos, (13) 
.equ CAN_F13R2_FB13_Msk, (0x1 << CAN_F13R2_FB13_Pos) //!< 0x00002000 
.equ CAN_F13R2_FB13, CAN_F13R2_FB13_Msk //!<Filter bit 13 
.equ CAN_F13R2_FB14_Pos, (14) 
.equ CAN_F13R2_FB14_Msk, (0x1 << CAN_F13R2_FB14_Pos) //!< 0x00004000 
.equ CAN_F13R2_FB14, CAN_F13R2_FB14_Msk //!<Filter bit 14 
.equ CAN_F13R2_FB15_Pos, (15) 
.equ CAN_F13R2_FB15_Msk, (0x1 << CAN_F13R2_FB15_Pos) //!< 0x00008000 
.equ CAN_F13R2_FB15, CAN_F13R2_FB15_Msk //!<Filter bit 15 
.equ CAN_F13R2_FB16_Pos, (16) 
.equ CAN_F13R2_FB16_Msk, (0x1 << CAN_F13R2_FB16_Pos) //!< 0x00010000 
.equ CAN_F13R2_FB16, CAN_F13R2_FB16_Msk //!<Filter bit 16 
.equ CAN_F13R2_FB17_Pos, (17) 
.equ CAN_F13R2_FB17_Msk, (0x1 << CAN_F13R2_FB17_Pos) //!< 0x00020000 
.equ CAN_F13R2_FB17, CAN_F13R2_FB17_Msk //!<Filter bit 17 
.equ CAN_F13R2_FB18_Pos, (18) 
.equ CAN_F13R2_FB18_Msk, (0x1 << CAN_F13R2_FB18_Pos) //!< 0x00040000 
.equ CAN_F13R2_FB18, CAN_F13R2_FB18_Msk //!<Filter bit 18 
.equ CAN_F13R2_FB19_Pos, (19) 
.equ CAN_F13R2_FB19_Msk, (0x1 << CAN_F13R2_FB19_Pos) //!< 0x00080000 
.equ CAN_F13R2_FB19, CAN_F13R2_FB19_Msk //!<Filter bit 19 
.equ CAN_F13R2_FB20_Pos, (20) 
.equ CAN_F13R2_FB20_Msk, (0x1 << CAN_F13R2_FB20_Pos) //!< 0x00100000 
.equ CAN_F13R2_FB20, CAN_F13R2_FB20_Msk //!<Filter bit 20 
.equ CAN_F13R2_FB21_Pos, (21) 
.equ CAN_F13R2_FB21_Msk, (0x1 << CAN_F13R2_FB21_Pos) //!< 0x00200000 
.equ CAN_F13R2_FB21, CAN_F13R2_FB21_Msk //!<Filter bit 21 
.equ CAN_F13R2_FB22_Pos, (22) 
.equ CAN_F13R2_FB22_Msk, (0x1 << CAN_F13R2_FB22_Pos) //!< 0x00400000 
.equ CAN_F13R2_FB22, CAN_F13R2_FB22_Msk //!<Filter bit 22 
.equ CAN_F13R2_FB23_Pos, (23) 
.equ CAN_F13R2_FB23_Msk, (0x1 << CAN_F13R2_FB23_Pos) //!< 0x00800000 
.equ CAN_F13R2_FB23, CAN_F13R2_FB23_Msk //!<Filter bit 23 
.equ CAN_F13R2_FB24_Pos, (24) 
.equ CAN_F13R2_FB24_Msk, (0x1 << CAN_F13R2_FB24_Pos) //!< 0x01000000 
.equ CAN_F13R2_FB24, CAN_F13R2_FB24_Msk //!<Filter bit 24 
.equ CAN_F13R2_FB25_Pos, (25) 
.equ CAN_F13R2_FB25_Msk, (0x1 << CAN_F13R2_FB25_Pos) //!< 0x02000000 
.equ CAN_F13R2_FB25, CAN_F13R2_FB25_Msk //!<Filter bit 25 
.equ CAN_F13R2_FB26_Pos, (26) 
.equ CAN_F13R2_FB26_Msk, (0x1 << CAN_F13R2_FB26_Pos) //!< 0x04000000 
.equ CAN_F13R2_FB26, CAN_F13R2_FB26_Msk //!<Filter bit 26 
.equ CAN_F13R2_FB27_Pos, (27) 
.equ CAN_F13R2_FB27_Msk, (0x1 << CAN_F13R2_FB27_Pos) //!< 0x08000000 
.equ CAN_F13R2_FB27, CAN_F13R2_FB27_Msk //!<Filter bit 27 
.equ CAN_F13R2_FB28_Pos, (28) 
.equ CAN_F13R2_FB28_Msk, (0x1 << CAN_F13R2_FB28_Pos) //!< 0x10000000 
.equ CAN_F13R2_FB28, CAN_F13R2_FB28_Msk //!<Filter bit 28 
.equ CAN_F13R2_FB29_Pos, (29) 
.equ CAN_F13R2_FB29_Msk, (0x1 << CAN_F13R2_FB29_Pos) //!< 0x20000000 
.equ CAN_F13R2_FB29, CAN_F13R2_FB29_Msk //!<Filter bit 29 
.equ CAN_F13R2_FB30_Pos, (30) 
.equ CAN_F13R2_FB30_Msk, (0x1 << CAN_F13R2_FB30_Pos) //!< 0x40000000 
.equ CAN_F13R2_FB30, CAN_F13R2_FB30_Msk //!<Filter bit 30 
.equ CAN_F13R2_FB31_Pos, (31) 
.equ CAN_F13R2_FB31_Msk, (0x1 << CAN_F13R2_FB31_Pos) //!< 0x80000000 
.equ CAN_F13R2_FB31, CAN_F13R2_FB31_Msk //!<Filter bit 31 
//****************************************************************************
//
//                     CRC calculation unit (CRC)
//
//****************************************************************************
//******************  Bit definition for CRC_DR register  ********************
.equ CRC_DR_DR_Pos, (0) 
.equ CRC_DR_DR_Msk, (0xFFFFFFFF << CRC_DR_DR_Pos) //!< 0xFFFFFFFF 
.equ CRC_DR_DR, CRC_DR_DR_Msk //!< Data register bits 
//******************  Bit definition for CRC_IDR register  *******************
.equ CRC_IDR_IDR, (0xFF) //!< General-purpose 8-bit data register bits 
//*******************  Bit definition for CRC_CR register  *******************
.equ CRC_CR_RESET_Pos, (0) 
.equ CRC_CR_RESET_Msk, (0x1 << CRC_CR_RESET_Pos) //!< 0x00000001 
.equ CRC_CR_RESET, CRC_CR_RESET_Msk //!< RESET the CRC computation unit bit 
.equ CRC_CR_POLYSIZE_Pos, (3) 
.equ CRC_CR_POLYSIZE_Msk, (0x3 << CRC_CR_POLYSIZE_Pos) //!< 0x00000018 
.equ CRC_CR_POLYSIZE, CRC_CR_POLYSIZE_Msk //!< Polynomial size bits 
.equ CRC_CR_POLYSIZE_0, (0x1 << CRC_CR_POLYSIZE_Pos) //!< 0x00000008 
.equ CRC_CR_POLYSIZE_1, (0x2 << CRC_CR_POLYSIZE_Pos) //!< 0x00000010 
.equ CRC_CR_REV_IN_Pos, (5) 
.equ CRC_CR_REV_IN_Msk, (0x3 << CRC_CR_REV_IN_Pos) //!< 0x00000060 
.equ CRC_CR_REV_IN, CRC_CR_REV_IN_Msk //!< REV_IN Reverse Input Data bits 
.equ CRC_CR_REV_IN_0, (0x1 << CRC_CR_REV_IN_Pos) //!< 0x00000020 
.equ CRC_CR_REV_IN_1, (0x2 << CRC_CR_REV_IN_Pos) //!< 0x00000040 
.equ CRC_CR_REV_OUT_Pos, (7) 
.equ CRC_CR_REV_OUT_Msk, (0x1 << CRC_CR_REV_OUT_Pos) //!< 0x00000080 
.equ CRC_CR_REV_OUT, CRC_CR_REV_OUT_Msk //!< REV_OUT Reverse Output Data bits 
//******************  Bit definition for CRC_INIT register  ******************
.equ CRC_INIT_INIT_Pos, (0) 
.equ CRC_INIT_INIT_Msk, (0xFFFFFFFF << CRC_INIT_INIT_Pos) //!< 0xFFFFFFFF 
.equ CRC_INIT_INIT, CRC_INIT_INIT_Msk //!< Initial CRC value bits 
//******************  Bit definition for CRC_POL register  *******************
.equ CRC_POL_POL_Pos, (0) 
.equ CRC_POL_POL_Msk, (0xFFFFFFFF << CRC_POL_POL_Pos) //!< 0xFFFFFFFF 
.equ CRC_POL_POL, CRC_POL_POL_Msk //!< Coefficients of the polynomial 
//****************************************************************************
//
//                 Digital to Analog Converter (DAC)
//
//****************************************************************************
//*******************  Bit definition for DAC_CR register  *******************
.equ DAC_CR_EN1_Pos, (0) 
.equ DAC_CR_EN1_Msk, (0x1 << DAC_CR_EN1_Pos) //!< 0x00000001 
.equ DAC_CR_EN1, DAC_CR_EN1_Msk //!< DAC channel1 enable 
.equ DAC_CR_BOFF1_Pos, (1) 
.equ DAC_CR_BOFF1_Msk, (0x1 << DAC_CR_BOFF1_Pos) //!< 0x00000002 
.equ DAC_CR_BOFF1, DAC_CR_BOFF1_Msk //!< DAC channel1 output buffer disable 
.equ DAC_CR_TEN1_Pos, (2) 
.equ DAC_CR_TEN1_Msk, (0x1 << DAC_CR_TEN1_Pos) //!< 0x00000004 
.equ DAC_CR_TEN1, DAC_CR_TEN1_Msk //!< DAC channel1 Trigger enable 
.equ DAC_CR_TSEL1_Pos, (3) 
.equ DAC_CR_TSEL1_Msk, (0x7 << DAC_CR_TSEL1_Pos) //!< 0x00000038 
.equ DAC_CR_TSEL1, DAC_CR_TSEL1_Msk //!< TSEL1[2:0] (DAC channel1 Trigger selection) 
.equ DAC_CR_TSEL1_0, (0x1 << DAC_CR_TSEL1_Pos) //!< 0x00000008 
.equ DAC_CR_TSEL1_1, (0x2 << DAC_CR_TSEL1_Pos) //!< 0x00000010 
.equ DAC_CR_TSEL1_2, (0x4 << DAC_CR_TSEL1_Pos) //!< 0x00000020 
.equ DAC_CR_WAVE1_Pos, (6) 
.equ DAC_CR_WAVE1_Msk, (0x3 << DAC_CR_WAVE1_Pos) //!< 0x000000C0 
.equ DAC_CR_WAVE1, DAC_CR_WAVE1_Msk //!< WAVE1[1:0] (DAC channel1 noise/triangle wave generation enable) 
.equ DAC_CR_WAVE1_0, (0x1 << DAC_CR_WAVE1_Pos) //!< 0x00000040 
.equ DAC_CR_WAVE1_1, (0x2 << DAC_CR_WAVE1_Pos) //!< 0x00000080 
.equ DAC_CR_MAMP1_Pos, (8) 
.equ DAC_CR_MAMP1_Msk, (0xF << DAC_CR_MAMP1_Pos) //!< 0x00000F00 
.equ DAC_CR_MAMP1, DAC_CR_MAMP1_Msk //!< MAMP1[3:0] (DAC channel1 Mask/Amplitude selector) 
.equ DAC_CR_MAMP1_0, (0x1 << DAC_CR_MAMP1_Pos) //!< 0x00000100 
.equ DAC_CR_MAMP1_1, (0x2 << DAC_CR_MAMP1_Pos) //!< 0x00000200 
.equ DAC_CR_MAMP1_2, (0x4 << DAC_CR_MAMP1_Pos) //!< 0x00000400 
.equ DAC_CR_MAMP1_3, (0x8 << DAC_CR_MAMP1_Pos) //!< 0x00000800 
.equ DAC_CR_DMAEN1_Pos, (12) 
.equ DAC_CR_DMAEN1_Msk, (0x1 << DAC_CR_DMAEN1_Pos) //!< 0x00001000 
.equ DAC_CR_DMAEN1, DAC_CR_DMAEN1_Msk //!< DAC channel1 DMA enable 
.equ DAC_CR_DMAUDRIE1_Pos, (13) 
.equ DAC_CR_DMAUDRIE1_Msk, (0x1 << DAC_CR_DMAUDRIE1_Pos) //!< 0x00002000 
.equ DAC_CR_DMAUDRIE1, DAC_CR_DMAUDRIE1_Msk //!< DAC channel1 DMA underrun IT enable 
.equ DAC_CR_EN2_Pos, (16) 
.equ DAC_CR_EN2_Msk, (0x1 << DAC_CR_EN2_Pos) //!< 0x00010000 
.equ DAC_CR_EN2, DAC_CR_EN2_Msk //!< DAC channel2 enable 
.equ DAC_CR_BOFF2_Pos, (17) 
.equ DAC_CR_BOFF2_Msk, (0x1 << DAC_CR_BOFF2_Pos) //!< 0x00020000 
.equ DAC_CR_BOFF2, DAC_CR_BOFF2_Msk //!< DAC channel2 output buffer disable 
.equ DAC_CR_TEN2_Pos, (18) 
.equ DAC_CR_TEN2_Msk, (0x1 << DAC_CR_TEN2_Pos) //!< 0x00040000 
.equ DAC_CR_TEN2, DAC_CR_TEN2_Msk //!< DAC channel2 Trigger enable 
.equ DAC_CR_TSEL2_Pos, (19) 
.equ DAC_CR_TSEL2_Msk, (0x7 << DAC_CR_TSEL2_Pos) //!< 0x00380000 
.equ DAC_CR_TSEL2, DAC_CR_TSEL2_Msk //!< TSEL2[2:0] (DAC channel2 Trigger selection) 
.equ DAC_CR_TSEL2_0, (0x1 << DAC_CR_TSEL2_Pos) //!< 0x00080000 
.equ DAC_CR_TSEL2_1, (0x2 << DAC_CR_TSEL2_Pos) //!< 0x00100000 
.equ DAC_CR_TSEL2_2, (0x4 << DAC_CR_TSEL2_Pos) //!< 0x00200000 
.equ DAC_CR_WAVE2_Pos, (22) 
.equ DAC_CR_WAVE2_Msk, (0x3 << DAC_CR_WAVE2_Pos) //!< 0x00C00000 
.equ DAC_CR_WAVE2, DAC_CR_WAVE2_Msk //!< WAVE2[1:0] (DAC channel2 noise/triangle wave generation enable) 
.equ DAC_CR_WAVE2_0, (0x1 << DAC_CR_WAVE2_Pos) //!< 0x00400000 
.equ DAC_CR_WAVE2_1, (0x2 << DAC_CR_WAVE2_Pos) //!< 0x00800000 
.equ DAC_CR_MAMP2_Pos, (24) 
.equ DAC_CR_MAMP2_Msk, (0xF << DAC_CR_MAMP2_Pos) //!< 0x0F000000 
.equ DAC_CR_MAMP2, DAC_CR_MAMP2_Msk //!< MAMP2[3:0] (DAC channel2 Mask/Amplitude selector) 
.equ DAC_CR_MAMP2_0, (0x1 << DAC_CR_MAMP2_Pos) //!< 0x01000000 
.equ DAC_CR_MAMP2_1, (0x2 << DAC_CR_MAMP2_Pos) //!< 0x02000000 
.equ DAC_CR_MAMP2_2, (0x4 << DAC_CR_MAMP2_Pos) //!< 0x04000000 
.equ DAC_CR_MAMP2_3, (0x8 << DAC_CR_MAMP2_Pos) //!< 0x08000000 
.equ DAC_CR_DMAEN2_Pos, (28) 
.equ DAC_CR_DMAEN2_Msk, (0x1 << DAC_CR_DMAEN2_Pos) //!< 0x10000000 
.equ DAC_CR_DMAEN2, DAC_CR_DMAEN2_Msk //!< DAC channel2 DMA enabled 
.equ DAC_CR_DMAUDRIE2_Pos, (29) 
.equ DAC_CR_DMAUDRIE2_Msk, (0x1 << DAC_CR_DMAUDRIE2_Pos) //!< 0x20000000 
.equ DAC_CR_DMAUDRIE2, DAC_CR_DMAUDRIE2_Msk //!< DAC channel2 DMA underrun IT enable 
//****************  Bit definition for DAC_SWTRIGR register  *****************
.equ DAC_SWTRIGR_SWTRIG1_Pos, (0) 
.equ DAC_SWTRIGR_SWTRIG1_Msk, (0x1 << DAC_SWTRIGR_SWTRIG1_Pos) //!< 0x00000001 
.equ DAC_SWTRIGR_SWTRIG1, DAC_SWTRIGR_SWTRIG1_Msk //!< DAC channel1 software trigger 
.equ DAC_SWTRIGR_SWTRIG2_Pos, (1) 
.equ DAC_SWTRIGR_SWTRIG2_Msk, (0x1 << DAC_SWTRIGR_SWTRIG2_Pos) //!< 0x00000002 
.equ DAC_SWTRIGR_SWTRIG2, DAC_SWTRIGR_SWTRIG2_Msk //!< DAC channel2 software trigger 
//****************  Bit definition for DAC_DHR12R1 register  *****************
.equ DAC_DHR12R1_DACC1DHR_Pos, (0) 
.equ DAC_DHR12R1_DACC1DHR_Msk, (0xFFF << DAC_DHR12R1_DACC1DHR_Pos) //!< 0x00000FFF 
.equ DAC_DHR12R1_DACC1DHR, DAC_DHR12R1_DACC1DHR_Msk //!< DAC channel1 12-bit Right aligned data 
//****************  Bit definition for DAC_DHR12L1 register  *****************
.equ DAC_DHR12L1_DACC1DHR_Pos, (4) 
.equ DAC_DHR12L1_DACC1DHR_Msk, (0xFFF << DAC_DHR12L1_DACC1DHR_Pos) //!< 0x0000FFF0 
.equ DAC_DHR12L1_DACC1DHR, DAC_DHR12L1_DACC1DHR_Msk //!< DAC channel1 12-bit Left aligned data 
//*****************  Bit definition for DAC_DHR8R1 register  *****************
.equ DAC_DHR8R1_DACC1DHR_Pos, (0) 
.equ DAC_DHR8R1_DACC1DHR_Msk, (0xFF << DAC_DHR8R1_DACC1DHR_Pos) //!< 0x000000FF 
.equ DAC_DHR8R1_DACC1DHR, DAC_DHR8R1_DACC1DHR_Msk //!< DAC channel1 8-bit Right aligned data 
//****************  Bit definition for DAC_DHR12R2 register  *****************
.equ DAC_DHR12R2_DACC2DHR_Pos, (0) 
.equ DAC_DHR12R2_DACC2DHR_Msk, (0xFFF << DAC_DHR12R2_DACC2DHR_Pos) //!< 0x00000FFF 
.equ DAC_DHR12R2_DACC2DHR, DAC_DHR12R2_DACC2DHR_Msk //!< DAC channel2 12-bit Right aligned data 
//****************  Bit definition for DAC_DHR12L2 register  *****************
.equ DAC_DHR12L2_DACC2DHR_Pos, (4) 
.equ DAC_DHR12L2_DACC2DHR_Msk, (0xFFF << DAC_DHR12L2_DACC2DHR_Pos) //!< 0x0000FFF0 
.equ DAC_DHR12L2_DACC2DHR, DAC_DHR12L2_DACC2DHR_Msk //!< DAC channel2 12-bit Left aligned data 
//*****************  Bit definition for DAC_DHR8R2 register  *****************
.equ DAC_DHR8R2_DACC2DHR_Pos, (0) 
.equ DAC_DHR8R2_DACC2DHR_Msk, (0xFF << DAC_DHR8R2_DACC2DHR_Pos) //!< 0x000000FF 
.equ DAC_DHR8R2_DACC2DHR, DAC_DHR8R2_DACC2DHR_Msk //!< DAC channel2 8-bit Right aligned data 
//****************  Bit definition for DAC_DHR12RD register  *****************
.equ DAC_DHR12RD_DACC1DHR_Pos, (0) 
.equ DAC_DHR12RD_DACC1DHR_Msk, (0xFFF << DAC_DHR12RD_DACC1DHR_Pos) //!< 0x00000FFF 
.equ DAC_DHR12RD_DACC1DHR, DAC_DHR12RD_DACC1DHR_Msk //!< DAC channel1 12-bit Right aligned data 
.equ DAC_DHR12RD_DACC2DHR_Pos, (16) 
.equ DAC_DHR12RD_DACC2DHR_Msk, (0xFFF << DAC_DHR12RD_DACC2DHR_Pos) //!< 0x0FFF0000 
.equ DAC_DHR12RD_DACC2DHR, DAC_DHR12RD_DACC2DHR_Msk //!< DAC channel2 12-bit Right aligned data 
//****************  Bit definition for DAC_DHR12LD register  *****************
.equ DAC_DHR12LD_DACC1DHR_Pos, (4) 
.equ DAC_DHR12LD_DACC1DHR_Msk, (0xFFF << DAC_DHR12LD_DACC1DHR_Pos) //!< 0x0000FFF0 
.equ DAC_DHR12LD_DACC1DHR, DAC_DHR12LD_DACC1DHR_Msk //!< DAC channel1 12-bit Left aligned data 
.equ DAC_DHR12LD_DACC2DHR_Pos, (20) 
.equ DAC_DHR12LD_DACC2DHR_Msk, (0xFFF << DAC_DHR12LD_DACC2DHR_Pos) //!< 0xFFF00000 
.equ DAC_DHR12LD_DACC2DHR, DAC_DHR12LD_DACC2DHR_Msk //!< DAC channel2 12-bit Left aligned data 
//*****************  Bit definition for DAC_DHR8RD register  *****************
.equ DAC_DHR8RD_DACC1DHR_Pos, (0) 
.equ DAC_DHR8RD_DACC1DHR_Msk, (0xFF << DAC_DHR8RD_DACC1DHR_Pos) //!< 0x000000FF 
.equ DAC_DHR8RD_DACC1DHR, DAC_DHR8RD_DACC1DHR_Msk //!< DAC channel1 8-bit Right aligned data 
.equ DAC_DHR8RD_DACC2DHR_Pos, (8) 
.equ DAC_DHR8RD_DACC2DHR_Msk, (0xFF << DAC_DHR8RD_DACC2DHR_Pos) //!< 0x0000FF00 
.equ DAC_DHR8RD_DACC2DHR, DAC_DHR8RD_DACC2DHR_Msk //!< DAC channel2 8-bit Right aligned data 
//******************  Bit definition for DAC_DOR1 register  ******************
.equ DAC_DOR1_DACC1DOR_Pos, (0) 
.equ DAC_DOR1_DACC1DOR_Msk, (0xFFF << DAC_DOR1_DACC1DOR_Pos) //!< 0x00000FFF 
.equ DAC_DOR1_DACC1DOR, DAC_DOR1_DACC1DOR_Msk //!< DAC channel1 data output 
//******************  Bit definition for DAC_DOR2 register  ******************
.equ DAC_DOR2_DACC2DOR_Pos, (0) 
.equ DAC_DOR2_DACC2DOR_Msk, (0xFFF << DAC_DOR2_DACC2DOR_Pos) //!< 0x00000FFF 
.equ DAC_DOR2_DACC2DOR, DAC_DOR2_DACC2DOR_Msk //!< DAC channel2 data output 
//*******************  Bit definition for DAC_SR register  *******************
.equ DAC_SR_DMAUDR1_Pos, (13) 
.equ DAC_SR_DMAUDR1_Msk, (0x1 << DAC_SR_DMAUDR1_Pos) //!< 0x00002000 
.equ DAC_SR_DMAUDR1, DAC_SR_DMAUDR1_Msk //!< DAC channel1 DMA underrun flag 
.equ DAC_SR_DMAUDR2_Pos, (29) 
.equ DAC_SR_DMAUDR2_Msk, (0x1 << DAC_SR_DMAUDR2_Pos) //!< 0x20000000 
.equ DAC_SR_DMAUDR2, DAC_SR_DMAUDR2_Msk //!< DAC channel2 DMA underrun flag 
//****************************************************************************
//
//                                 Debug MCU (DBGMCU)
//
//****************************************************************************
//*******************  Bit definition for DBGMCU_IDCODE register  ************
.equ DBGMCU_IDCODE_DEV_ID_Pos, (0) 
.equ DBGMCU_IDCODE_DEV_ID_Msk, (0xFFF << DBGMCU_IDCODE_DEV_ID_Pos) //!< 0x00000FFF 
.equ DBGMCU_IDCODE_DEV_ID, DBGMCU_IDCODE_DEV_ID_Msk 
.equ DBGMCU_IDCODE_REV_ID_Pos, (16) 
.equ DBGMCU_IDCODE_REV_ID_Msk, (0xFFFF << DBGMCU_IDCODE_REV_ID_Pos) //!< 0xFFFF0000 
.equ DBGMCU_IDCODE_REV_ID, DBGMCU_IDCODE_REV_ID_Msk 
//*******************  Bit definition for DBGMCU_CR register  ****************
.equ DBGMCU_CR_DBG_SLEEP_Pos, (0) 
.equ DBGMCU_CR_DBG_SLEEP_Msk, (0x1 << DBGMCU_CR_DBG_SLEEP_Pos) //!< 0x00000001 
.equ DBGMCU_CR_DBG_SLEEP, DBGMCU_CR_DBG_SLEEP_Msk 
.equ DBGMCU_CR_DBG_STOP_Pos, (1) 
.equ DBGMCU_CR_DBG_STOP_Msk, (0x1 << DBGMCU_CR_DBG_STOP_Pos) //!< 0x00000002 
.equ DBGMCU_CR_DBG_STOP, DBGMCU_CR_DBG_STOP_Msk 
.equ DBGMCU_CR_DBG_STANDBY_Pos, (2) 
.equ DBGMCU_CR_DBG_STANDBY_Msk, (0x1 << DBGMCU_CR_DBG_STANDBY_Pos) //!< 0x00000004 
.equ DBGMCU_CR_DBG_STANDBY, DBGMCU_CR_DBG_STANDBY_Msk 
.equ DBGMCU_CR_TRACE_IOEN_Pos, (5) 
.equ DBGMCU_CR_TRACE_IOEN_Msk, (0x1 << DBGMCU_CR_TRACE_IOEN_Pos) //!< 0x00000020 
.equ DBGMCU_CR_TRACE_IOEN, DBGMCU_CR_TRACE_IOEN_Msk 
.equ DBGMCU_CR_TRACE_MODE_Pos, (6) 
.equ DBGMCU_CR_TRACE_MODE_Msk, (0x3 << DBGMCU_CR_TRACE_MODE_Pos) //!< 0x000000C0 
.equ DBGMCU_CR_TRACE_MODE, DBGMCU_CR_TRACE_MODE_Msk 
.equ DBGMCU_CR_TRACE_MODE_0, (0x1 << DBGMCU_CR_TRACE_MODE_Pos) //!< 0x00000040 
.equ DBGMCU_CR_TRACE_MODE_1, (0x2 << DBGMCU_CR_TRACE_MODE_Pos) //!< 0x00000080 
//*******************  Bit definition for DBGMCU_APB1_FZ register  ***********
.equ DBGMCU_APB1_FZ_DBG_TIM2_STOP_Pos, (0) 
.equ DBGMCU_APB1_FZ_DBG_TIM2_STOP_Msk, (0x1 << DBGMCU_APB1_FZ_DBG_TIM2_STOP_Pos) //!< 0x00000001 
.equ DBGMCU_APB1_FZ_DBG_TIM2_STOP, DBGMCU_APB1_FZ_DBG_TIM2_STOP_Msk 
.equ DBGMCU_APB1_FZ_DBG_TIM3_STOP_Pos, (1) 
.equ DBGMCU_APB1_FZ_DBG_TIM3_STOP_Msk, (0x1 << DBGMCU_APB1_FZ_DBG_TIM3_STOP_Pos) //!< 0x00000002 
.equ DBGMCU_APB1_FZ_DBG_TIM3_STOP, DBGMCU_APB1_FZ_DBG_TIM3_STOP_Msk 
.equ DBGMCU_APB1_FZ_DBG_TIM4_STOP_Pos, (2) 
.equ DBGMCU_APB1_FZ_DBG_TIM4_STOP_Msk, (0x1 << DBGMCU_APB1_FZ_DBG_TIM4_STOP_Pos) //!< 0x00000004 
.equ DBGMCU_APB1_FZ_DBG_TIM4_STOP, DBGMCU_APB1_FZ_DBG_TIM4_STOP_Msk 
.equ DBGMCU_APB1_FZ_DBG_TIM6_STOP_Pos, (4) 
.equ DBGMCU_APB1_FZ_DBG_TIM6_STOP_Msk, (0x1 << DBGMCU_APB1_FZ_DBG_TIM6_STOP_Pos) //!< 0x00000010 
.equ DBGMCU_APB1_FZ_DBG_TIM6_STOP, DBGMCU_APB1_FZ_DBG_TIM6_STOP_Msk 
.equ DBGMCU_APB1_FZ_DBG_TIM7_STOP_Pos, (5) 
.equ DBGMCU_APB1_FZ_DBG_TIM7_STOP_Msk, (0x1 << DBGMCU_APB1_FZ_DBG_TIM7_STOP_Pos) //!< 0x00000020 
.equ DBGMCU_APB1_FZ_DBG_TIM7_STOP, DBGMCU_APB1_FZ_DBG_TIM7_STOP_Msk 
.equ DBGMCU_APB1_FZ_DBG_RTC_STOP_Pos, (10) 
.equ DBGMCU_APB1_FZ_DBG_RTC_STOP_Msk, (0x1 << DBGMCU_APB1_FZ_DBG_RTC_STOP_Pos) //!< 0x00000400 
.equ DBGMCU_APB1_FZ_DBG_RTC_STOP, DBGMCU_APB1_FZ_DBG_RTC_STOP_Msk 
.equ DBGMCU_APB1_FZ_DBG_WWDG_STOP_Pos, (11) 
.equ DBGMCU_APB1_FZ_DBG_WWDG_STOP_Msk, (0x1 << DBGMCU_APB1_FZ_DBG_WWDG_STOP_Pos) //!< 0x00000800 
.equ DBGMCU_APB1_FZ_DBG_WWDG_STOP, DBGMCU_APB1_FZ_DBG_WWDG_STOP_Msk 
.equ DBGMCU_APB1_FZ_DBG_IWDG_STOP_Pos, (12) 
.equ DBGMCU_APB1_FZ_DBG_IWDG_STOP_Msk, (0x1 << DBGMCU_APB1_FZ_DBG_IWDG_STOP_Pos) //!< 0x00001000 
.equ DBGMCU_APB1_FZ_DBG_IWDG_STOP, DBGMCU_APB1_FZ_DBG_IWDG_STOP_Msk 
.equ DBGMCU_APB1_FZ_DBG_I2C1_SMBUS_TIMEOUT_Pos, (21) 
.equ DBGMCU_APB1_FZ_DBG_I2C1_SMBUS_TIMEOUT_Msk, (0x1 << DBGMCU_APB1_FZ_DBG_I2C1_SMBUS_TIMEOUT_Pos) //!< 0x00200000 
.equ DBGMCU_APB1_FZ_DBG_I2C1_SMBUS_TIMEOUT, DBGMCU_APB1_FZ_DBG_I2C1_SMBUS_TIMEOUT_Msk 
.equ DBGMCU_APB1_FZ_DBG_I2C2_SMBUS_TIMEOUT_Pos, (22) 
.equ DBGMCU_APB1_FZ_DBG_I2C2_SMBUS_TIMEOUT_Msk, (0x1 << DBGMCU_APB1_FZ_DBG_I2C2_SMBUS_TIMEOUT_Pos) //!< 0x00400000 
.equ DBGMCU_APB1_FZ_DBG_I2C2_SMBUS_TIMEOUT, DBGMCU_APB1_FZ_DBG_I2C2_SMBUS_TIMEOUT_Msk 
.equ DBGMCU_APB1_FZ_DBG_I2C3_SMBUS_TIMEOUT_Pos, (30) 
.equ DBGMCU_APB1_FZ_DBG_I2C3_SMBUS_TIMEOUT_Msk, (0x1 << DBGMCU_APB1_FZ_DBG_I2C3_SMBUS_TIMEOUT_Pos) //!< 0x40000000 
.equ DBGMCU_APB1_FZ_DBG_I2C3_SMBUS_TIMEOUT, DBGMCU_APB1_FZ_DBG_I2C3_SMBUS_TIMEOUT_Msk 
.equ DBGMCU_APB1_FZ_DBG_CAN_STOP_Pos, (25) 
.equ DBGMCU_APB1_FZ_DBG_CAN_STOP_Msk, (0x1 << DBGMCU_APB1_FZ_DBG_CAN_STOP_Pos) //!< 0x02000000 
.equ DBGMCU_APB1_FZ_DBG_CAN_STOP, DBGMCU_APB1_FZ_DBG_CAN_STOP_Msk 
//*******************  Bit definition for DBGMCU_APB2_FZ register  ***********
.equ DBGMCU_APB2_FZ_DBG_TIM1_STOP_Pos, (0) 
.equ DBGMCU_APB2_FZ_DBG_TIM1_STOP_Msk, (0x1 << DBGMCU_APB2_FZ_DBG_TIM1_STOP_Pos) //!< 0x00000001 
.equ DBGMCU_APB2_FZ_DBG_TIM1_STOP, DBGMCU_APB2_FZ_DBG_TIM1_STOP_Msk 
.equ DBGMCU_APB2_FZ_DBG_TIM8_STOP_Pos, (1) 
.equ DBGMCU_APB2_FZ_DBG_TIM8_STOP_Msk, (0x1 << DBGMCU_APB2_FZ_DBG_TIM8_STOP_Pos) //!< 0x00000002 
.equ DBGMCU_APB2_FZ_DBG_TIM8_STOP, DBGMCU_APB2_FZ_DBG_TIM8_STOP_Msk 
.equ DBGMCU_APB2_FZ_DBG_TIM15_STOP_Pos, (2) 
.equ DBGMCU_APB2_FZ_DBG_TIM15_STOP_Msk, (0x1 << DBGMCU_APB2_FZ_DBG_TIM15_STOP_Pos) //!< 0x00000004 
.equ DBGMCU_APB2_FZ_DBG_TIM15_STOP, DBGMCU_APB2_FZ_DBG_TIM15_STOP_Msk 
.equ DBGMCU_APB2_FZ_DBG_TIM16_STOP_Pos, (3) 
.equ DBGMCU_APB2_FZ_DBG_TIM16_STOP_Msk, (0x1 << DBGMCU_APB2_FZ_DBG_TIM16_STOP_Pos) //!< 0x00000008 
.equ DBGMCU_APB2_FZ_DBG_TIM16_STOP, DBGMCU_APB2_FZ_DBG_TIM16_STOP_Msk 
.equ DBGMCU_APB2_FZ_DBG_TIM17_STOP_Pos, (4) 
.equ DBGMCU_APB2_FZ_DBG_TIM17_STOP_Msk, (0x1 << DBGMCU_APB2_FZ_DBG_TIM17_STOP_Pos) //!< 0x00000010 
.equ DBGMCU_APB2_FZ_DBG_TIM17_STOP, DBGMCU_APB2_FZ_DBG_TIM17_STOP_Msk 
.equ DBGMCU_APB2_FZ_DBG_TIM20_STOP_Pos, (5) 
.equ DBGMCU_APB2_FZ_DBG_TIM20_STOP_Msk, (0x1 << DBGMCU_APB2_FZ_DBG_TIM20_STOP_Pos) //!< 0x00000020 
.equ DBGMCU_APB2_FZ_DBG_TIM20_STOP, DBGMCU_APB2_FZ_DBG_TIM20_STOP_Msk 
//****************************************************************************
//
//                             DMA Controller (DMA)
//
//****************************************************************************
//******************  Bit definition for DMA_ISR register  *******************
.equ DMA_ISR_GIF1_Pos, (0) 
.equ DMA_ISR_GIF1_Msk, (0x1 << DMA_ISR_GIF1_Pos) //!< 0x00000001 
.equ DMA_ISR_GIF1, DMA_ISR_GIF1_Msk //!< Channel 1 Global interrupt flag 
.equ DMA_ISR_TCIF1_Pos, (1) 
.equ DMA_ISR_TCIF1_Msk, (0x1 << DMA_ISR_TCIF1_Pos) //!< 0x00000002 
.equ DMA_ISR_TCIF1, DMA_ISR_TCIF1_Msk //!< Channel 1 Transfer Complete flag 
.equ DMA_ISR_HTIF1_Pos, (2) 
.equ DMA_ISR_HTIF1_Msk, (0x1 << DMA_ISR_HTIF1_Pos) //!< 0x00000004 
.equ DMA_ISR_HTIF1, DMA_ISR_HTIF1_Msk //!< Channel 1 Half Transfer flag 
.equ DMA_ISR_TEIF1_Pos, (3) 
.equ DMA_ISR_TEIF1_Msk, (0x1 << DMA_ISR_TEIF1_Pos) //!< 0x00000008 
.equ DMA_ISR_TEIF1, DMA_ISR_TEIF1_Msk //!< Channel 1 Transfer Error flag 
.equ DMA_ISR_GIF2_Pos, (4) 
.equ DMA_ISR_GIF2_Msk, (0x1 << DMA_ISR_GIF2_Pos) //!< 0x00000010 
.equ DMA_ISR_GIF2, DMA_ISR_GIF2_Msk //!< Channel 2 Global interrupt flag 
.equ DMA_ISR_TCIF2_Pos, (5) 
.equ DMA_ISR_TCIF2_Msk, (0x1 << DMA_ISR_TCIF2_Pos) //!< 0x00000020 
.equ DMA_ISR_TCIF2, DMA_ISR_TCIF2_Msk //!< Channel 2 Transfer Complete flag 
.equ DMA_ISR_HTIF2_Pos, (6) 
.equ DMA_ISR_HTIF2_Msk, (0x1 << DMA_ISR_HTIF2_Pos) //!< 0x00000040 
.equ DMA_ISR_HTIF2, DMA_ISR_HTIF2_Msk //!< Channel 2 Half Transfer flag 
.equ DMA_ISR_TEIF2_Pos, (7) 
.equ DMA_ISR_TEIF2_Msk, (0x1 << DMA_ISR_TEIF2_Pos) //!< 0x00000080 
.equ DMA_ISR_TEIF2, DMA_ISR_TEIF2_Msk //!< Channel 2 Transfer Error flag 
.equ DMA_ISR_GIF3_Pos, (8) 
.equ DMA_ISR_GIF3_Msk, (0x1 << DMA_ISR_GIF3_Pos) //!< 0x00000100 
.equ DMA_ISR_GIF3, DMA_ISR_GIF3_Msk //!< Channel 3 Global interrupt flag 
.equ DMA_ISR_TCIF3_Pos, (9) 
.equ DMA_ISR_TCIF3_Msk, (0x1 << DMA_ISR_TCIF3_Pos) //!< 0x00000200 
.equ DMA_ISR_TCIF3, DMA_ISR_TCIF3_Msk //!< Channel 3 Transfer Complete flag 
.equ DMA_ISR_HTIF3_Pos, (10) 
.equ DMA_ISR_HTIF3_Msk, (0x1 << DMA_ISR_HTIF3_Pos) //!< 0x00000400 
.equ DMA_ISR_HTIF3, DMA_ISR_HTIF3_Msk //!< Channel 3 Half Transfer flag 
.equ DMA_ISR_TEIF3_Pos, (11) 
.equ DMA_ISR_TEIF3_Msk, (0x1 << DMA_ISR_TEIF3_Pos) //!< 0x00000800 
.equ DMA_ISR_TEIF3, DMA_ISR_TEIF3_Msk //!< Channel 3 Transfer Error flag 
.equ DMA_ISR_GIF4_Pos, (12) 
.equ DMA_ISR_GIF4_Msk, (0x1 << DMA_ISR_GIF4_Pos) //!< 0x00001000 
.equ DMA_ISR_GIF4, DMA_ISR_GIF4_Msk //!< Channel 4 Global interrupt flag 
.equ DMA_ISR_TCIF4_Pos, (13) 
.equ DMA_ISR_TCIF4_Msk, (0x1 << DMA_ISR_TCIF4_Pos) //!< 0x00002000 
.equ DMA_ISR_TCIF4, DMA_ISR_TCIF4_Msk //!< Channel 4 Transfer Complete flag 
.equ DMA_ISR_HTIF4_Pos, (14) 
.equ DMA_ISR_HTIF4_Msk, (0x1 << DMA_ISR_HTIF4_Pos) //!< 0x00004000 
.equ DMA_ISR_HTIF4, DMA_ISR_HTIF4_Msk //!< Channel 4 Half Transfer flag 
.equ DMA_ISR_TEIF4_Pos, (15) 
.equ DMA_ISR_TEIF4_Msk, (0x1 << DMA_ISR_TEIF4_Pos) //!< 0x00008000 
.equ DMA_ISR_TEIF4, DMA_ISR_TEIF4_Msk //!< Channel 4 Transfer Error flag 
.equ DMA_ISR_GIF5_Pos, (16) 
.equ DMA_ISR_GIF5_Msk, (0x1 << DMA_ISR_GIF5_Pos) //!< 0x00010000 
.equ DMA_ISR_GIF5, DMA_ISR_GIF5_Msk //!< Channel 5 Global interrupt flag 
.equ DMA_ISR_TCIF5_Pos, (17) 
.equ DMA_ISR_TCIF5_Msk, (0x1 << DMA_ISR_TCIF5_Pos) //!< 0x00020000 
.equ DMA_ISR_TCIF5, DMA_ISR_TCIF5_Msk //!< Channel 5 Transfer Complete flag 
.equ DMA_ISR_HTIF5_Pos, (18) 
.equ DMA_ISR_HTIF5_Msk, (0x1 << DMA_ISR_HTIF5_Pos) //!< 0x00040000 
.equ DMA_ISR_HTIF5, DMA_ISR_HTIF5_Msk //!< Channel 5 Half Transfer flag 
.equ DMA_ISR_TEIF5_Pos, (19) 
.equ DMA_ISR_TEIF5_Msk, (0x1 << DMA_ISR_TEIF5_Pos) //!< 0x00080000 
.equ DMA_ISR_TEIF5, DMA_ISR_TEIF5_Msk //!< Channel 5 Transfer Error flag 
.equ DMA_ISR_GIF6_Pos, (20) 
.equ DMA_ISR_GIF6_Msk, (0x1 << DMA_ISR_GIF6_Pos) //!< 0x00100000 
.equ DMA_ISR_GIF6, DMA_ISR_GIF6_Msk //!< Channel 6 Global interrupt flag 
.equ DMA_ISR_TCIF6_Pos, (21) 
.equ DMA_ISR_TCIF6_Msk, (0x1 << DMA_ISR_TCIF6_Pos) //!< 0x00200000 
.equ DMA_ISR_TCIF6, DMA_ISR_TCIF6_Msk //!< Channel 6 Transfer Complete flag 
.equ DMA_ISR_HTIF6_Pos, (22) 
.equ DMA_ISR_HTIF6_Msk, (0x1 << DMA_ISR_HTIF6_Pos) //!< 0x00400000 
.equ DMA_ISR_HTIF6, DMA_ISR_HTIF6_Msk //!< Channel 6 Half Transfer flag 
.equ DMA_ISR_TEIF6_Pos, (23) 
.equ DMA_ISR_TEIF6_Msk, (0x1 << DMA_ISR_TEIF6_Pos) //!< 0x00800000 
.equ DMA_ISR_TEIF6, DMA_ISR_TEIF6_Msk //!< Channel 6 Transfer Error flag 
.equ DMA_ISR_GIF7_Pos, (24) 
.equ DMA_ISR_GIF7_Msk, (0x1 << DMA_ISR_GIF7_Pos) //!< 0x01000000 
.equ DMA_ISR_GIF7, DMA_ISR_GIF7_Msk //!< Channel 7 Global interrupt flag 
.equ DMA_ISR_TCIF7_Pos, (25) 
.equ DMA_ISR_TCIF7_Msk, (0x1 << DMA_ISR_TCIF7_Pos) //!< 0x02000000 
.equ DMA_ISR_TCIF7, DMA_ISR_TCIF7_Msk //!< Channel 7 Transfer Complete flag 
.equ DMA_ISR_HTIF7_Pos, (26) 
.equ DMA_ISR_HTIF7_Msk, (0x1 << DMA_ISR_HTIF7_Pos) //!< 0x04000000 
.equ DMA_ISR_HTIF7, DMA_ISR_HTIF7_Msk //!< Channel 7 Half Transfer flag 
.equ DMA_ISR_TEIF7_Pos, (27) 
.equ DMA_ISR_TEIF7_Msk, (0x1 << DMA_ISR_TEIF7_Pos) //!< 0x08000000 
.equ DMA_ISR_TEIF7, DMA_ISR_TEIF7_Msk //!< Channel 7 Transfer Error flag 
//******************  Bit definition for DMA_IFCR register  ******************
.equ DMA_IFCR_CGIF1_Pos, (0) 
.equ DMA_IFCR_CGIF1_Msk, (0x1 << DMA_IFCR_CGIF1_Pos) //!< 0x00000001 
.equ DMA_IFCR_CGIF1, DMA_IFCR_CGIF1_Msk //!< Channel 1 Global interrupt clear 
.equ DMA_IFCR_CTCIF1_Pos, (1) 
.equ DMA_IFCR_CTCIF1_Msk, (0x1 << DMA_IFCR_CTCIF1_Pos) //!< 0x00000002 
.equ DMA_IFCR_CTCIF1, DMA_IFCR_CTCIF1_Msk //!< Channel 1 Transfer Complete clear 
.equ DMA_IFCR_CHTIF1_Pos, (2) 
.equ DMA_IFCR_CHTIF1_Msk, (0x1 << DMA_IFCR_CHTIF1_Pos) //!< 0x00000004 
.equ DMA_IFCR_CHTIF1, DMA_IFCR_CHTIF1_Msk //!< Channel 1 Half Transfer clear 
.equ DMA_IFCR_CTEIF1_Pos, (3) 
.equ DMA_IFCR_CTEIF1_Msk, (0x1 << DMA_IFCR_CTEIF1_Pos) //!< 0x00000008 
.equ DMA_IFCR_CTEIF1, DMA_IFCR_CTEIF1_Msk //!< Channel 1 Transfer Error clear 
.equ DMA_IFCR_CGIF2_Pos, (4) 
.equ DMA_IFCR_CGIF2_Msk, (0x1 << DMA_IFCR_CGIF2_Pos) //!< 0x00000010 
.equ DMA_IFCR_CGIF2, DMA_IFCR_CGIF2_Msk //!< Channel 2 Global interrupt clear 
.equ DMA_IFCR_CTCIF2_Pos, (5) 
.equ DMA_IFCR_CTCIF2_Msk, (0x1 << DMA_IFCR_CTCIF2_Pos) //!< 0x00000020 
.equ DMA_IFCR_CTCIF2, DMA_IFCR_CTCIF2_Msk //!< Channel 2 Transfer Complete clear 
.equ DMA_IFCR_CHTIF2_Pos, (6) 
.equ DMA_IFCR_CHTIF2_Msk, (0x1 << DMA_IFCR_CHTIF2_Pos) //!< 0x00000040 
.equ DMA_IFCR_CHTIF2, DMA_IFCR_CHTIF2_Msk //!< Channel 2 Half Transfer clear 
.equ DMA_IFCR_CTEIF2_Pos, (7) 
.equ DMA_IFCR_CTEIF2_Msk, (0x1 << DMA_IFCR_CTEIF2_Pos) //!< 0x00000080 
.equ DMA_IFCR_CTEIF2, DMA_IFCR_CTEIF2_Msk //!< Channel 2 Transfer Error clear 
.equ DMA_IFCR_CGIF3_Pos, (8) 
.equ DMA_IFCR_CGIF3_Msk, (0x1 << DMA_IFCR_CGIF3_Pos) //!< 0x00000100 
.equ DMA_IFCR_CGIF3, DMA_IFCR_CGIF3_Msk //!< Channel 3 Global interrupt clear 
.equ DMA_IFCR_CTCIF3_Pos, (9) 
.equ DMA_IFCR_CTCIF3_Msk, (0x1 << DMA_IFCR_CTCIF3_Pos) //!< 0x00000200 
.equ DMA_IFCR_CTCIF3, DMA_IFCR_CTCIF3_Msk //!< Channel 3 Transfer Complete clear 
.equ DMA_IFCR_CHTIF3_Pos, (10) 
.equ DMA_IFCR_CHTIF3_Msk, (0x1 << DMA_IFCR_CHTIF3_Pos) //!< 0x00000400 
.equ DMA_IFCR_CHTIF3, DMA_IFCR_CHTIF3_Msk //!< Channel 3 Half Transfer clear 
.equ DMA_IFCR_CTEIF3_Pos, (11) 
.equ DMA_IFCR_CTEIF3_Msk, (0x1 << DMA_IFCR_CTEIF3_Pos) //!< 0x00000800 
.equ DMA_IFCR_CTEIF3, DMA_IFCR_CTEIF3_Msk //!< Channel 3 Transfer Error clear 
.equ DMA_IFCR_CGIF4_Pos, (12) 
.equ DMA_IFCR_CGIF4_Msk, (0x1 << DMA_IFCR_CGIF4_Pos) //!< 0x00001000 
.equ DMA_IFCR_CGIF4, DMA_IFCR_CGIF4_Msk //!< Channel 4 Global interrupt clear 
.equ DMA_IFCR_CTCIF4_Pos, (13) 
.equ DMA_IFCR_CTCIF4_Msk, (0x1 << DMA_IFCR_CTCIF4_Pos) //!< 0x00002000 
.equ DMA_IFCR_CTCIF4, DMA_IFCR_CTCIF4_Msk //!< Channel 4 Transfer Complete clear 
.equ DMA_IFCR_CHTIF4_Pos, (14) 
.equ DMA_IFCR_CHTIF4_Msk, (0x1 << DMA_IFCR_CHTIF4_Pos) //!< 0x00004000 
.equ DMA_IFCR_CHTIF4, DMA_IFCR_CHTIF4_Msk //!< Channel 4 Half Transfer clear 
.equ DMA_IFCR_CTEIF4_Pos, (15) 
.equ DMA_IFCR_CTEIF4_Msk, (0x1 << DMA_IFCR_CTEIF4_Pos) //!< 0x00008000 
.equ DMA_IFCR_CTEIF4, DMA_IFCR_CTEIF4_Msk //!< Channel 4 Transfer Error clear 
.equ DMA_IFCR_CGIF5_Pos, (16) 
.equ DMA_IFCR_CGIF5_Msk, (0x1 << DMA_IFCR_CGIF5_Pos) //!< 0x00010000 
.equ DMA_IFCR_CGIF5, DMA_IFCR_CGIF5_Msk //!< Channel 5 Global interrupt clear 
.equ DMA_IFCR_CTCIF5_Pos, (17) 
.equ DMA_IFCR_CTCIF5_Msk, (0x1 << DMA_IFCR_CTCIF5_Pos) //!< 0x00020000 
.equ DMA_IFCR_CTCIF5, DMA_IFCR_CTCIF5_Msk //!< Channel 5 Transfer Complete clear 
.equ DMA_IFCR_CHTIF5_Pos, (18) 
.equ DMA_IFCR_CHTIF5_Msk, (0x1 << DMA_IFCR_CHTIF5_Pos) //!< 0x00040000 
.equ DMA_IFCR_CHTIF5, DMA_IFCR_CHTIF5_Msk //!< Channel 5 Half Transfer clear 
.equ DMA_IFCR_CTEIF5_Pos, (19) 
.equ DMA_IFCR_CTEIF5_Msk, (0x1 << DMA_IFCR_CTEIF5_Pos) //!< 0x00080000 
.equ DMA_IFCR_CTEIF5, DMA_IFCR_CTEIF5_Msk //!< Channel 5 Transfer Error clear 
.equ DMA_IFCR_CGIF6_Pos, (20) 
.equ DMA_IFCR_CGIF6_Msk, (0x1 << DMA_IFCR_CGIF6_Pos) //!< 0x00100000 
.equ DMA_IFCR_CGIF6, DMA_IFCR_CGIF6_Msk //!< Channel 6 Global interrupt clear 
.equ DMA_IFCR_CTCIF6_Pos, (21) 
.equ DMA_IFCR_CTCIF6_Msk, (0x1 << DMA_IFCR_CTCIF6_Pos) //!< 0x00200000 
.equ DMA_IFCR_CTCIF6, DMA_IFCR_CTCIF6_Msk //!< Channel 6 Transfer Complete clear 
.equ DMA_IFCR_CHTIF6_Pos, (22) 
.equ DMA_IFCR_CHTIF6_Msk, (0x1 << DMA_IFCR_CHTIF6_Pos) //!< 0x00400000 
.equ DMA_IFCR_CHTIF6, DMA_IFCR_CHTIF6_Msk //!< Channel 6 Half Transfer clear 
.equ DMA_IFCR_CTEIF6_Pos, (23) 
.equ DMA_IFCR_CTEIF6_Msk, (0x1 << DMA_IFCR_CTEIF6_Pos) //!< 0x00800000 
.equ DMA_IFCR_CTEIF6, DMA_IFCR_CTEIF6_Msk //!< Channel 6 Transfer Error clear 
.equ DMA_IFCR_CGIF7_Pos, (24) 
.equ DMA_IFCR_CGIF7_Msk, (0x1 << DMA_IFCR_CGIF7_Pos) //!< 0x01000000 
.equ DMA_IFCR_CGIF7, DMA_IFCR_CGIF7_Msk //!< Channel 7 Global interrupt clear 
.equ DMA_IFCR_CTCIF7_Pos, (25) 
.equ DMA_IFCR_CTCIF7_Msk, (0x1 << DMA_IFCR_CTCIF7_Pos) //!< 0x02000000 
.equ DMA_IFCR_CTCIF7, DMA_IFCR_CTCIF7_Msk //!< Channel 7 Transfer Complete clear 
.equ DMA_IFCR_CHTIF7_Pos, (26) 
.equ DMA_IFCR_CHTIF7_Msk, (0x1 << DMA_IFCR_CHTIF7_Pos) //!< 0x04000000 
.equ DMA_IFCR_CHTIF7, DMA_IFCR_CHTIF7_Msk //!< Channel 7 Half Transfer clear 
.equ DMA_IFCR_CTEIF7_Pos, (27) 
.equ DMA_IFCR_CTEIF7_Msk, (0x1 << DMA_IFCR_CTEIF7_Pos) //!< 0x08000000 
.equ DMA_IFCR_CTEIF7, DMA_IFCR_CTEIF7_Msk //!< Channel 7 Transfer Error clear 
//******************  Bit definition for DMA_CCR register  *******************
.equ DMA_CCR_EN_Pos, (0) 
.equ DMA_CCR_EN_Msk, (0x1 << DMA_CCR_EN_Pos) //!< 0x00000001 
.equ DMA_CCR_EN, DMA_CCR_EN_Msk //!< Channel enable 
.equ DMA_CCR_TCIE_Pos, (1) 
.equ DMA_CCR_TCIE_Msk, (0x1 << DMA_CCR_TCIE_Pos) //!< 0x00000002 
.equ DMA_CCR_TCIE, DMA_CCR_TCIE_Msk //!< Transfer complete interrupt enable 
.equ DMA_CCR_HTIE_Pos, (2) 
.equ DMA_CCR_HTIE_Msk, (0x1 << DMA_CCR_HTIE_Pos) //!< 0x00000004 
.equ DMA_CCR_HTIE, DMA_CCR_HTIE_Msk //!< Half Transfer interrupt enable 
.equ DMA_CCR_TEIE_Pos, (3) 
.equ DMA_CCR_TEIE_Msk, (0x1 << DMA_CCR_TEIE_Pos) //!< 0x00000008 
.equ DMA_CCR_TEIE, DMA_CCR_TEIE_Msk //!< Transfer error interrupt enable 
.equ DMA_CCR_DIR_Pos, (4) 
.equ DMA_CCR_DIR_Msk, (0x1 << DMA_CCR_DIR_Pos) //!< 0x00000010 
.equ DMA_CCR_DIR, DMA_CCR_DIR_Msk //!< Data transfer direction 
.equ DMA_CCR_CIRC_Pos, (5) 
.equ DMA_CCR_CIRC_Msk, (0x1 << DMA_CCR_CIRC_Pos) //!< 0x00000020 
.equ DMA_CCR_CIRC, DMA_CCR_CIRC_Msk //!< Circular mode 
.equ DMA_CCR_PINC_Pos, (6) 
.equ DMA_CCR_PINC_Msk, (0x1 << DMA_CCR_PINC_Pos) //!< 0x00000040 
.equ DMA_CCR_PINC, DMA_CCR_PINC_Msk //!< Peripheral increment mode 
.equ DMA_CCR_MINC_Pos, (7) 
.equ DMA_CCR_MINC_Msk, (0x1 << DMA_CCR_MINC_Pos) //!< 0x00000080 
.equ DMA_CCR_MINC, DMA_CCR_MINC_Msk //!< Memory increment mode 
.equ DMA_CCR_PSIZE_Pos, (8) 
.equ DMA_CCR_PSIZE_Msk, (0x3 << DMA_CCR_PSIZE_Pos) //!< 0x00000300 
.equ DMA_CCR_PSIZE, DMA_CCR_PSIZE_Msk //!< PSIZE[1:0] bits (Peripheral size) 
.equ DMA_CCR_PSIZE_0, (0x1 << DMA_CCR_PSIZE_Pos) //!< 0x00000100 
.equ DMA_CCR_PSIZE_1, (0x2 << DMA_CCR_PSIZE_Pos) //!< 0x00000200 
.equ DMA_CCR_MSIZE_Pos, (10) 
.equ DMA_CCR_MSIZE_Msk, (0x3 << DMA_CCR_MSIZE_Pos) //!< 0x00000C00 
.equ DMA_CCR_MSIZE, DMA_CCR_MSIZE_Msk //!< MSIZE[1:0] bits (Memory size) 
.equ DMA_CCR_MSIZE_0, (0x1 << DMA_CCR_MSIZE_Pos) //!< 0x00000400 
.equ DMA_CCR_MSIZE_1, (0x2 << DMA_CCR_MSIZE_Pos) //!< 0x00000800 
.equ DMA_CCR_PL_Pos, (12) 
.equ DMA_CCR_PL_Msk, (0x3 << DMA_CCR_PL_Pos) //!< 0x00003000 
.equ DMA_CCR_PL, DMA_CCR_PL_Msk //!< PL[1:0] bits(Channel Priority level) 
.equ DMA_CCR_PL_0, (0x1 << DMA_CCR_PL_Pos) //!< 0x00001000 
.equ DMA_CCR_PL_1, (0x2 << DMA_CCR_PL_Pos) //!< 0x00002000 
.equ DMA_CCR_MEM2MEM_Pos, (14) 
.equ DMA_CCR_MEM2MEM_Msk, (0x1 << DMA_CCR_MEM2MEM_Pos) //!< 0x00004000 
.equ DMA_CCR_MEM2MEM, DMA_CCR_MEM2MEM_Msk //!< Memory to memory mode 
//*****************  Bit definition for DMA_CNDTR register  ******************
.equ DMA_CNDTR_NDT_Pos, (0) 
.equ DMA_CNDTR_NDT_Msk, (0xFFFF << DMA_CNDTR_NDT_Pos) //!< 0x0000FFFF 
.equ DMA_CNDTR_NDT, DMA_CNDTR_NDT_Msk //!< Number of data to Transfer 
//*****************  Bit definition for DMA_CPAR register  *******************
.equ DMA_CPAR_PA_Pos, (0) 
.equ DMA_CPAR_PA_Msk, (0xFFFFFFFF << DMA_CPAR_PA_Pos) //!< 0xFFFFFFFF 
.equ DMA_CPAR_PA, DMA_CPAR_PA_Msk //!< Peripheral Address 
//*****************  Bit definition for DMA_CMAR register  *******************
.equ DMA_CMAR_MA_Pos, (0) 
.equ DMA_CMAR_MA_Msk, (0xFFFFFFFF << DMA_CMAR_MA_Pos) //!< 0xFFFFFFFF 
.equ DMA_CMAR_MA, DMA_CMAR_MA_Msk //!< Memory Address 
//****************************************************************************
//
//                    External Interrupt/Event Controller (EXTI)
//
//****************************************************************************
//******************  Bit definition for EXTI_IMR register  ******************
.equ EXTI_IMR_MR0_Pos, (0) 
.equ EXTI_IMR_MR0_Msk, (0x1 << EXTI_IMR_MR0_Pos) //!< 0x00000001 
.equ EXTI_IMR_MR0, EXTI_IMR_MR0_Msk //!< Interrupt Mask on line 0 
.equ EXTI_IMR_MR1_Pos, (1) 
.equ EXTI_IMR_MR1_Msk, (0x1 << EXTI_IMR_MR1_Pos) //!< 0x00000002 
.equ EXTI_IMR_MR1, EXTI_IMR_MR1_Msk //!< Interrupt Mask on line 1 
.equ EXTI_IMR_MR2_Pos, (2) 
.equ EXTI_IMR_MR2_Msk, (0x1 << EXTI_IMR_MR2_Pos) //!< 0x00000004 
.equ EXTI_IMR_MR2, EXTI_IMR_MR2_Msk //!< Interrupt Mask on line 2 
.equ EXTI_IMR_MR3_Pos, (3) 
.equ EXTI_IMR_MR3_Msk, (0x1 << EXTI_IMR_MR3_Pos) //!< 0x00000008 
.equ EXTI_IMR_MR3, EXTI_IMR_MR3_Msk //!< Interrupt Mask on line 3 
.equ EXTI_IMR_MR4_Pos, (4) 
.equ EXTI_IMR_MR4_Msk, (0x1 << EXTI_IMR_MR4_Pos) //!< 0x00000010 
.equ EXTI_IMR_MR4, EXTI_IMR_MR4_Msk //!< Interrupt Mask on line 4 
.equ EXTI_IMR_MR5_Pos, (5) 
.equ EXTI_IMR_MR5_Msk, (0x1 << EXTI_IMR_MR5_Pos) //!< 0x00000020 
.equ EXTI_IMR_MR5, EXTI_IMR_MR5_Msk //!< Interrupt Mask on line 5 
.equ EXTI_IMR_MR6_Pos, (6) 
.equ EXTI_IMR_MR6_Msk, (0x1 << EXTI_IMR_MR6_Pos) //!< 0x00000040 
.equ EXTI_IMR_MR6, EXTI_IMR_MR6_Msk //!< Interrupt Mask on line 6 
.equ EXTI_IMR_MR7_Pos, (7) 
.equ EXTI_IMR_MR7_Msk, (0x1 << EXTI_IMR_MR7_Pos) //!< 0x00000080 
.equ EXTI_IMR_MR7, EXTI_IMR_MR7_Msk //!< Interrupt Mask on line 7 
.equ EXTI_IMR_MR8_Pos, (8) 
.equ EXTI_IMR_MR8_Msk, (0x1 << EXTI_IMR_MR8_Pos) //!< 0x00000100 
.equ EXTI_IMR_MR8, EXTI_IMR_MR8_Msk //!< Interrupt Mask on line 8 
.equ EXTI_IMR_MR9_Pos, (9) 
.equ EXTI_IMR_MR9_Msk, (0x1 << EXTI_IMR_MR9_Pos) //!< 0x00000200 
.equ EXTI_IMR_MR9, EXTI_IMR_MR9_Msk //!< Interrupt Mask on line 9 
.equ EXTI_IMR_MR10_Pos, (10) 
.equ EXTI_IMR_MR10_Msk, (0x1 << EXTI_IMR_MR10_Pos) //!< 0x00000400 
.equ EXTI_IMR_MR10, EXTI_IMR_MR10_Msk //!< Interrupt Mask on line 10 
.equ EXTI_IMR_MR11_Pos, (11) 
.equ EXTI_IMR_MR11_Msk, (0x1 << EXTI_IMR_MR11_Pos) //!< 0x00000800 
.equ EXTI_IMR_MR11, EXTI_IMR_MR11_Msk //!< Interrupt Mask on line 11 
.equ EXTI_IMR_MR12_Pos, (12) 
.equ EXTI_IMR_MR12_Msk, (0x1 << EXTI_IMR_MR12_Pos) //!< 0x00001000 
.equ EXTI_IMR_MR12, EXTI_IMR_MR12_Msk //!< Interrupt Mask on line 12 
.equ EXTI_IMR_MR13_Pos, (13) 
.equ EXTI_IMR_MR13_Msk, (0x1 << EXTI_IMR_MR13_Pos) //!< 0x00002000 
.equ EXTI_IMR_MR13, EXTI_IMR_MR13_Msk //!< Interrupt Mask on line 13 
.equ EXTI_IMR_MR14_Pos, (14) 
.equ EXTI_IMR_MR14_Msk, (0x1 << EXTI_IMR_MR14_Pos) //!< 0x00004000 
.equ EXTI_IMR_MR14, EXTI_IMR_MR14_Msk //!< Interrupt Mask on line 14 
.equ EXTI_IMR_MR15_Pos, (15) 
.equ EXTI_IMR_MR15_Msk, (0x1 << EXTI_IMR_MR15_Pos) //!< 0x00008000 
.equ EXTI_IMR_MR15, EXTI_IMR_MR15_Msk //!< Interrupt Mask on line 15 
.equ EXTI_IMR_MR16_Pos, (16) 
.equ EXTI_IMR_MR16_Msk, (0x1 << EXTI_IMR_MR16_Pos) //!< 0x00010000 
.equ EXTI_IMR_MR16, EXTI_IMR_MR16_Msk //!< Interrupt Mask on line 16 
.equ EXTI_IMR_MR17_Pos, (17) 
.equ EXTI_IMR_MR17_Msk, (0x1 << EXTI_IMR_MR17_Pos) //!< 0x00020000 
.equ EXTI_IMR_MR17, EXTI_IMR_MR17_Msk //!< Interrupt Mask on line 17 
.equ EXTI_IMR_MR18_Pos, (18) 
.equ EXTI_IMR_MR18_Msk, (0x1 << EXTI_IMR_MR18_Pos) //!< 0x00040000 
.equ EXTI_IMR_MR18, EXTI_IMR_MR18_Msk //!< Interrupt Mask on line 18 
.equ EXTI_IMR_MR19_Pos, (19) 
.equ EXTI_IMR_MR19_Msk, (0x1 << EXTI_IMR_MR19_Pos) //!< 0x00080000 
.equ EXTI_IMR_MR19, EXTI_IMR_MR19_Msk //!< Interrupt Mask on line 19 
.equ EXTI_IMR_MR20_Pos, (20) 
.equ EXTI_IMR_MR20_Msk, (0x1 << EXTI_IMR_MR20_Pos) //!< 0x00100000 
.equ EXTI_IMR_MR20, EXTI_IMR_MR20_Msk //!< Interrupt Mask on line 20 
.equ EXTI_IMR_MR21_Pos, (21) 
.equ EXTI_IMR_MR21_Msk, (0x1 << EXTI_IMR_MR21_Pos) //!< 0x00200000 
.equ EXTI_IMR_MR21, EXTI_IMR_MR21_Msk //!< Interrupt Mask on line 21 
.equ EXTI_IMR_MR22_Pos, (22) 
.equ EXTI_IMR_MR22_Msk, (0x1 << EXTI_IMR_MR22_Pos) //!< 0x00400000 
.equ EXTI_IMR_MR22, EXTI_IMR_MR22_Msk //!< Interrupt Mask on line 22 
.equ EXTI_IMR_MR23_Pos, (23) 
.equ EXTI_IMR_MR23_Msk, (0x1 << EXTI_IMR_MR23_Pos) //!< 0x00800000 
.equ EXTI_IMR_MR23, EXTI_IMR_MR23_Msk //!< Interrupt Mask on line 23 
.equ EXTI_IMR_MR24_Pos, (24) 
.equ EXTI_IMR_MR24_Msk, (0x1 << EXTI_IMR_MR24_Pos) //!< 0x01000000 
.equ EXTI_IMR_MR24, EXTI_IMR_MR24_Msk //!< Interrupt Mask on line 24 
.equ EXTI_IMR_MR25_Pos, (25) 
.equ EXTI_IMR_MR25_Msk, (0x1 << EXTI_IMR_MR25_Pos) //!< 0x02000000 
.equ EXTI_IMR_MR25, EXTI_IMR_MR25_Msk //!< Interrupt Mask on line 25 
.equ EXTI_IMR_MR26_Pos, (26) 
.equ EXTI_IMR_MR26_Msk, (0x1 << EXTI_IMR_MR26_Pos) //!< 0x04000000 
.equ EXTI_IMR_MR26, EXTI_IMR_MR26_Msk //!< Interrupt Mask on line 26 
.equ EXTI_IMR_MR27_Pos, (27) 
.equ EXTI_IMR_MR27_Msk, (0x1 << EXTI_IMR_MR27_Pos) //!< 0x08000000 
.equ EXTI_IMR_MR27, EXTI_IMR_MR27_Msk //!< Interrupt Mask on line 27 
.equ EXTI_IMR_MR28_Pos, (28) 
.equ EXTI_IMR_MR28_Msk, (0x1 << EXTI_IMR_MR28_Pos) //!< 0x10000000 
.equ EXTI_IMR_MR28, EXTI_IMR_MR28_Msk //!< Interrupt Mask on line 28 
.equ EXTI_IMR_MR29_Pos, (29) 
.equ EXTI_IMR_MR29_Msk, (0x1 << EXTI_IMR_MR29_Pos) //!< 0x20000000 
.equ EXTI_IMR_MR29, EXTI_IMR_MR29_Msk //!< Interrupt Mask on line 29 
.equ EXTI_IMR_MR30_Pos, (30) 
.equ EXTI_IMR_MR30_Msk, (0x1 << EXTI_IMR_MR30_Pos) //!< 0x40000000 
.equ EXTI_IMR_MR30, EXTI_IMR_MR30_Msk //!< Interrupt Mask on line 30 
.equ EXTI_IMR_MR31_Pos, (31) 
.equ EXTI_IMR_MR31_Msk, (0x1 << EXTI_IMR_MR31_Pos) //!< 0x80000000 
.equ EXTI_IMR_MR31, EXTI_IMR_MR31_Msk //!< Interrupt Mask on line 31 
// References Defines
.equ EXTI_IMR_IM0, EXTI_IMR_MR0 
.equ EXTI_IMR_IM1, EXTI_IMR_MR1 
.equ EXTI_IMR_IM2, EXTI_IMR_MR2 
.equ EXTI_IMR_IM3, EXTI_IMR_MR3 
.equ EXTI_IMR_IM4, EXTI_IMR_MR4 
.equ EXTI_IMR_IM5, EXTI_IMR_MR5 
.equ EXTI_IMR_IM6, EXTI_IMR_MR6 
.equ EXTI_IMR_IM7, EXTI_IMR_MR7 
.equ EXTI_IMR_IM8, EXTI_IMR_MR8 
.equ EXTI_IMR_IM9, EXTI_IMR_MR9 
.equ EXTI_IMR_IM10, EXTI_IMR_MR10 
.equ EXTI_IMR_IM11, EXTI_IMR_MR11 
.equ EXTI_IMR_IM12, EXTI_IMR_MR12 
.equ EXTI_IMR_IM13, EXTI_IMR_MR13 
.equ EXTI_IMR_IM14, EXTI_IMR_MR14 
.equ EXTI_IMR_IM15, EXTI_IMR_MR15 
.equ EXTI_IMR_IM16, EXTI_IMR_MR16 
.equ EXTI_IMR_IM17, EXTI_IMR_MR17 
.equ EXTI_IMR_IM18, EXTI_IMR_MR18 
.equ EXTI_IMR_IM19, EXTI_IMR_MR19 
.equ EXTI_IMR_IM20, EXTI_IMR_MR20 
.equ EXTI_IMR_IM21, EXTI_IMR_MR21 
.equ EXTI_IMR_IM22, EXTI_IMR_MR22 
.equ EXTI_IMR_IM23, EXTI_IMR_MR23 
.equ EXTI_IMR_IM24, EXTI_IMR_MR24 
.equ EXTI_IMR_IM25, EXTI_IMR_MR25 
.equ EXTI_IMR_IM26, EXTI_IMR_MR26 
.equ EXTI_IMR_IM27, EXTI_IMR_MR27 
.equ EXTI_IMR_IM28, EXTI_IMR_MR28 
.equ EXTI_IMR_IM29, EXTI_IMR_MR29 
.equ EXTI_IMR_IM30, EXTI_IMR_MR30 
.equ EXTI_IMR_IM31, EXTI_IMR_MR31 
.equ EXTI_IMR_IM_Pos, (0) 
.equ EXTI_IMR_IM_Msk, (0xFFFFFFFF << EXTI_IMR_IM_Pos) //!< 0xFFFFFFFF 
.equ EXTI_IMR_IM, EXTI_IMR_IM_Msk //!< Interrupt Mask All 
//******************  Bit definition for EXTI_EMR register  ******************
.equ EXTI_EMR_MR0_Pos, (0) 
.equ EXTI_EMR_MR0_Msk, (0x1 << EXTI_EMR_MR0_Pos) //!< 0x00000001 
.equ EXTI_EMR_MR0, EXTI_EMR_MR0_Msk //!< Event Mask on line 0 
.equ EXTI_EMR_MR1_Pos, (1) 
.equ EXTI_EMR_MR1_Msk, (0x1 << EXTI_EMR_MR1_Pos) //!< 0x00000002 
.equ EXTI_EMR_MR1, EXTI_EMR_MR1_Msk //!< Event Mask on line 1 
.equ EXTI_EMR_MR2_Pos, (2) 
.equ EXTI_EMR_MR2_Msk, (0x1 << EXTI_EMR_MR2_Pos) //!< 0x00000004 
.equ EXTI_EMR_MR2, EXTI_EMR_MR2_Msk //!< Event Mask on line 2 
.equ EXTI_EMR_MR3_Pos, (3) 
.equ EXTI_EMR_MR3_Msk, (0x1 << EXTI_EMR_MR3_Pos) //!< 0x00000008 
.equ EXTI_EMR_MR3, EXTI_EMR_MR3_Msk //!< Event Mask on line 3 
.equ EXTI_EMR_MR4_Pos, (4) 
.equ EXTI_EMR_MR4_Msk, (0x1 << EXTI_EMR_MR4_Pos) //!< 0x00000010 
.equ EXTI_EMR_MR4, EXTI_EMR_MR4_Msk //!< Event Mask on line 4 
.equ EXTI_EMR_MR5_Pos, (5) 
.equ EXTI_EMR_MR5_Msk, (0x1 << EXTI_EMR_MR5_Pos) //!< 0x00000020 
.equ EXTI_EMR_MR5, EXTI_EMR_MR5_Msk //!< Event Mask on line 5 
.equ EXTI_EMR_MR6_Pos, (6) 
.equ EXTI_EMR_MR6_Msk, (0x1 << EXTI_EMR_MR6_Pos) //!< 0x00000040 
.equ EXTI_EMR_MR6, EXTI_EMR_MR6_Msk //!< Event Mask on line 6 
.equ EXTI_EMR_MR7_Pos, (7) 
.equ EXTI_EMR_MR7_Msk, (0x1 << EXTI_EMR_MR7_Pos) //!< 0x00000080 
.equ EXTI_EMR_MR7, EXTI_EMR_MR7_Msk //!< Event Mask on line 7 
.equ EXTI_EMR_MR8_Pos, (8) 
.equ EXTI_EMR_MR8_Msk, (0x1 << EXTI_EMR_MR8_Pos) //!< 0x00000100 
.equ EXTI_EMR_MR8, EXTI_EMR_MR8_Msk //!< Event Mask on line 8 
.equ EXTI_EMR_MR9_Pos, (9) 
.equ EXTI_EMR_MR9_Msk, (0x1 << EXTI_EMR_MR9_Pos) //!< 0x00000200 
.equ EXTI_EMR_MR9, EXTI_EMR_MR9_Msk //!< Event Mask on line 9 
.equ EXTI_EMR_MR10_Pos, (10) 
.equ EXTI_EMR_MR10_Msk, (0x1 << EXTI_EMR_MR10_Pos) //!< 0x00000400 
.equ EXTI_EMR_MR10, EXTI_EMR_MR10_Msk //!< Event Mask on line 10 
.equ EXTI_EMR_MR11_Pos, (11) 
.equ EXTI_EMR_MR11_Msk, (0x1 << EXTI_EMR_MR11_Pos) //!< 0x00000800 
.equ EXTI_EMR_MR11, EXTI_EMR_MR11_Msk //!< Event Mask on line 11 
.equ EXTI_EMR_MR12_Pos, (12) 
.equ EXTI_EMR_MR12_Msk, (0x1 << EXTI_EMR_MR12_Pos) //!< 0x00001000 
.equ EXTI_EMR_MR12, EXTI_EMR_MR12_Msk //!< Event Mask on line 12 
.equ EXTI_EMR_MR13_Pos, (13) 
.equ EXTI_EMR_MR13_Msk, (0x1 << EXTI_EMR_MR13_Pos) //!< 0x00002000 
.equ EXTI_EMR_MR13, EXTI_EMR_MR13_Msk //!< Event Mask on line 13 
.equ EXTI_EMR_MR14_Pos, (14) 
.equ EXTI_EMR_MR14_Msk, (0x1 << EXTI_EMR_MR14_Pos) //!< 0x00004000 
.equ EXTI_EMR_MR14, EXTI_EMR_MR14_Msk //!< Event Mask on line 14 
.equ EXTI_EMR_MR15_Pos, (15) 
.equ EXTI_EMR_MR15_Msk, (0x1 << EXTI_EMR_MR15_Pos) //!< 0x00008000 
.equ EXTI_EMR_MR15, EXTI_EMR_MR15_Msk //!< Event Mask on line 15 
.equ EXTI_EMR_MR16_Pos, (16) 
.equ EXTI_EMR_MR16_Msk, (0x1 << EXTI_EMR_MR16_Pos) //!< 0x00010000 
.equ EXTI_EMR_MR16, EXTI_EMR_MR16_Msk //!< Event Mask on line 16 
.equ EXTI_EMR_MR17_Pos, (17) 
.equ EXTI_EMR_MR17_Msk, (0x1 << EXTI_EMR_MR17_Pos) //!< 0x00020000 
.equ EXTI_EMR_MR17, EXTI_EMR_MR17_Msk //!< Event Mask on line 17 
.equ EXTI_EMR_MR18_Pos, (18) 
.equ EXTI_EMR_MR18_Msk, (0x1 << EXTI_EMR_MR18_Pos) //!< 0x00040000 
.equ EXTI_EMR_MR18, EXTI_EMR_MR18_Msk //!< Event Mask on line 18 
.equ EXTI_EMR_MR19_Pos, (19) 
.equ EXTI_EMR_MR19_Msk, (0x1 << EXTI_EMR_MR19_Pos) //!< 0x00080000 
.equ EXTI_EMR_MR19, EXTI_EMR_MR19_Msk //!< Event Mask on line 19 
.equ EXTI_EMR_MR20_Pos, (20) 
.equ EXTI_EMR_MR20_Msk, (0x1 << EXTI_EMR_MR20_Pos) //!< 0x00100000 
.equ EXTI_EMR_MR20, EXTI_EMR_MR20_Msk //!< Event Mask on line 20 
.equ EXTI_EMR_MR21_Pos, (21) 
.equ EXTI_EMR_MR21_Msk, (0x1 << EXTI_EMR_MR21_Pos) //!< 0x00200000 
.equ EXTI_EMR_MR21, EXTI_EMR_MR21_Msk //!< Event Mask on line 21 
.equ EXTI_EMR_MR22_Pos, (22) 
.equ EXTI_EMR_MR22_Msk, (0x1 << EXTI_EMR_MR22_Pos) //!< 0x00400000 
.equ EXTI_EMR_MR22, EXTI_EMR_MR22_Msk //!< Event Mask on line 22 
.equ EXTI_EMR_MR23_Pos, (23) 
.equ EXTI_EMR_MR23_Msk, (0x1 << EXTI_EMR_MR23_Pos) //!< 0x00800000 
.equ EXTI_EMR_MR23, EXTI_EMR_MR23_Msk //!< Event Mask on line 23 
.equ EXTI_EMR_MR24_Pos, (24) 
.equ EXTI_EMR_MR24_Msk, (0x1 << EXTI_EMR_MR24_Pos) //!< 0x01000000 
.equ EXTI_EMR_MR24, EXTI_EMR_MR24_Msk //!< Event Mask on line 24 
.equ EXTI_EMR_MR25_Pos, (25) 
.equ EXTI_EMR_MR25_Msk, (0x1 << EXTI_EMR_MR25_Pos) //!< 0x02000000 
.equ EXTI_EMR_MR25, EXTI_EMR_MR25_Msk //!< Event Mask on line 25 
.equ EXTI_EMR_MR26_Pos, (26) 
.equ EXTI_EMR_MR26_Msk, (0x1 << EXTI_EMR_MR26_Pos) //!< 0x04000000 
.equ EXTI_EMR_MR26, EXTI_EMR_MR26_Msk //!< Event Mask on line 26 
.equ EXTI_EMR_MR27_Pos, (27) 
.equ EXTI_EMR_MR27_Msk, (0x1 << EXTI_EMR_MR27_Pos) //!< 0x08000000 
.equ EXTI_EMR_MR27, EXTI_EMR_MR27_Msk //!< Event Mask on line 27 
.equ EXTI_EMR_MR28_Pos, (28) 
.equ EXTI_EMR_MR28_Msk, (0x1 << EXTI_EMR_MR28_Pos) //!< 0x10000000 
.equ EXTI_EMR_MR28, EXTI_EMR_MR28_Msk //!< Event Mask on line 28 
.equ EXTI_EMR_MR29_Pos, (29) 
.equ EXTI_EMR_MR29_Msk, (0x1 << EXTI_EMR_MR29_Pos) //!< 0x20000000 
.equ EXTI_EMR_MR29, EXTI_EMR_MR29_Msk //!< Event Mask on line 29 
.equ EXTI_EMR_MR30_Pos, (30) 
.equ EXTI_EMR_MR30_Msk, (0x1 << EXTI_EMR_MR30_Pos) //!< 0x40000000 
.equ EXTI_EMR_MR30, EXTI_EMR_MR30_Msk //!< Event Mask on line 30 
.equ EXTI_EMR_MR31_Pos, (31) 
.equ EXTI_EMR_MR31_Msk, (0x1 << EXTI_EMR_MR31_Pos) //!< 0x80000000 
.equ EXTI_EMR_MR31, EXTI_EMR_MR31_Msk //!< Event Mask on line 31 
// References Defines
.equ EXTI_EMR_EM0, EXTI_EMR_MR0 
.equ EXTI_EMR_EM1, EXTI_EMR_MR1 
.equ EXTI_EMR_EM2, EXTI_EMR_MR2 
.equ EXTI_EMR_EM3, EXTI_EMR_MR3 
.equ EXTI_EMR_EM4, EXTI_EMR_MR4 
.equ EXTI_EMR_EM5, EXTI_EMR_MR5 
.equ EXTI_EMR_EM6, EXTI_EMR_MR6 
.equ EXTI_EMR_EM7, EXTI_EMR_MR7 
.equ EXTI_EMR_EM8, EXTI_EMR_MR8 
.equ EXTI_EMR_EM9, EXTI_EMR_MR9 
.equ EXTI_EMR_EM10, EXTI_EMR_MR10 
.equ EXTI_EMR_EM11, EXTI_EMR_MR11 
.equ EXTI_EMR_EM12, EXTI_EMR_MR12 
.equ EXTI_EMR_EM13, EXTI_EMR_MR13 
.equ EXTI_EMR_EM14, EXTI_EMR_MR14 
.equ EXTI_EMR_EM15, EXTI_EMR_MR15 
.equ EXTI_EMR_EM16, EXTI_EMR_MR16 
.equ EXTI_EMR_EM17, EXTI_EMR_MR17 
.equ EXTI_EMR_EM18, EXTI_EMR_MR18 
.equ EXTI_EMR_EM19, EXTI_EMR_MR19 
.equ EXTI_EMR_EM20, EXTI_EMR_MR20 
.equ EXTI_EMR_EM21, EXTI_EMR_MR21 
.equ EXTI_EMR_EM22, EXTI_EMR_MR22 
.equ EXTI_EMR_EM23, EXTI_EMR_MR23 
.equ EXTI_EMR_EM24, EXTI_EMR_MR24 
.equ EXTI_EMR_EM25, EXTI_EMR_MR25 
.equ EXTI_EMR_EM26, EXTI_EMR_MR26 
.equ EXTI_EMR_EM27, EXTI_EMR_MR27 
.equ EXTI_EMR_EM28, EXTI_EMR_MR28 
.equ EXTI_EMR_EM29, EXTI_EMR_MR29 
.equ EXTI_EMR_EM30, EXTI_EMR_MR30 
.equ EXTI_EMR_EM31, EXTI_EMR_MR31 
//*****************  Bit definition for EXTI_RTSR register  ******************
.equ EXTI_RTSR_TR0_Pos, (0) 
.equ EXTI_RTSR_TR0_Msk, (0x1 << EXTI_RTSR_TR0_Pos) //!< 0x00000001 
.equ EXTI_RTSR_TR0, EXTI_RTSR_TR0_Msk //!< Rising trigger event configuration bit of line 0 
.equ EXTI_RTSR_TR1_Pos, (1) 
.equ EXTI_RTSR_TR1_Msk, (0x1 << EXTI_RTSR_TR1_Pos) //!< 0x00000002 
.equ EXTI_RTSR_TR1, EXTI_RTSR_TR1_Msk //!< Rising trigger event configuration bit of line 1 
.equ EXTI_RTSR_TR2_Pos, (2) 
.equ EXTI_RTSR_TR2_Msk, (0x1 << EXTI_RTSR_TR2_Pos) //!< 0x00000004 
.equ EXTI_RTSR_TR2, EXTI_RTSR_TR2_Msk //!< Rising trigger event configuration bit of line 2 
.equ EXTI_RTSR_TR3_Pos, (3) 
.equ EXTI_RTSR_TR3_Msk, (0x1 << EXTI_RTSR_TR3_Pos) //!< 0x00000008 
.equ EXTI_RTSR_TR3, EXTI_RTSR_TR3_Msk //!< Rising trigger event configuration bit of line 3 
.equ EXTI_RTSR_TR4_Pos, (4) 
.equ EXTI_RTSR_TR4_Msk, (0x1 << EXTI_RTSR_TR4_Pos) //!< 0x00000010 
.equ EXTI_RTSR_TR4, EXTI_RTSR_TR4_Msk //!< Rising trigger event configuration bit of line 4 
.equ EXTI_RTSR_TR5_Pos, (5) 
.equ EXTI_RTSR_TR5_Msk, (0x1 << EXTI_RTSR_TR5_Pos) //!< 0x00000020 
.equ EXTI_RTSR_TR5, EXTI_RTSR_TR5_Msk //!< Rising trigger event configuration bit of line 5 
.equ EXTI_RTSR_TR6_Pos, (6) 
.equ EXTI_RTSR_TR6_Msk, (0x1 << EXTI_RTSR_TR6_Pos) //!< 0x00000040 
.equ EXTI_RTSR_TR6, EXTI_RTSR_TR6_Msk //!< Rising trigger event configuration bit of line 6 
.equ EXTI_RTSR_TR7_Pos, (7) 
.equ EXTI_RTSR_TR7_Msk, (0x1 << EXTI_RTSR_TR7_Pos) //!< 0x00000080 
.equ EXTI_RTSR_TR7, EXTI_RTSR_TR7_Msk //!< Rising trigger event configuration bit of line 7 
.equ EXTI_RTSR_TR8_Pos, (8) 
.equ EXTI_RTSR_TR8_Msk, (0x1 << EXTI_RTSR_TR8_Pos) //!< 0x00000100 
.equ EXTI_RTSR_TR8, EXTI_RTSR_TR8_Msk //!< Rising trigger event configuration bit of line 8 
.equ EXTI_RTSR_TR9_Pos, (9) 
.equ EXTI_RTSR_TR9_Msk, (0x1 << EXTI_RTSR_TR9_Pos) //!< 0x00000200 
.equ EXTI_RTSR_TR9, EXTI_RTSR_TR9_Msk //!< Rising trigger event configuration bit of line 9 
.equ EXTI_RTSR_TR10_Pos, (10) 
.equ EXTI_RTSR_TR10_Msk, (0x1 << EXTI_RTSR_TR10_Pos) //!< 0x00000400 
.equ EXTI_RTSR_TR10, EXTI_RTSR_TR10_Msk //!< Rising trigger event configuration bit of line 10 
.equ EXTI_RTSR_TR11_Pos, (11) 
.equ EXTI_RTSR_TR11_Msk, (0x1 << EXTI_RTSR_TR11_Pos) //!< 0x00000800 
.equ EXTI_RTSR_TR11, EXTI_RTSR_TR11_Msk //!< Rising trigger event configuration bit of line 11 
.equ EXTI_RTSR_TR12_Pos, (12) 
.equ EXTI_RTSR_TR12_Msk, (0x1 << EXTI_RTSR_TR12_Pos) //!< 0x00001000 
.equ EXTI_RTSR_TR12, EXTI_RTSR_TR12_Msk //!< Rising trigger event configuration bit of line 12 
.equ EXTI_RTSR_TR13_Pos, (13) 
.equ EXTI_RTSR_TR13_Msk, (0x1 << EXTI_RTSR_TR13_Pos) //!< 0x00002000 
.equ EXTI_RTSR_TR13, EXTI_RTSR_TR13_Msk //!< Rising trigger event configuration bit of line 13 
.equ EXTI_RTSR_TR14_Pos, (14) 
.equ EXTI_RTSR_TR14_Msk, (0x1 << EXTI_RTSR_TR14_Pos) //!< 0x00004000 
.equ EXTI_RTSR_TR14, EXTI_RTSR_TR14_Msk //!< Rising trigger event configuration bit of line 14 
.equ EXTI_RTSR_TR15_Pos, (15) 
.equ EXTI_RTSR_TR15_Msk, (0x1 << EXTI_RTSR_TR15_Pos) //!< 0x00008000 
.equ EXTI_RTSR_TR15, EXTI_RTSR_TR15_Msk //!< Rising trigger event configuration bit of line 15 
.equ EXTI_RTSR_TR16_Pos, (16) 
.equ EXTI_RTSR_TR16_Msk, (0x1 << EXTI_RTSR_TR16_Pos) //!< 0x00010000 
.equ EXTI_RTSR_TR16, EXTI_RTSR_TR16_Msk //!< Rising trigger event configuration bit of line 16 
.equ EXTI_RTSR_TR17_Pos, (17) 
.equ EXTI_RTSR_TR17_Msk, (0x1 << EXTI_RTSR_TR17_Pos) //!< 0x00020000 
.equ EXTI_RTSR_TR17, EXTI_RTSR_TR17_Msk //!< Rising trigger event configuration bit of line 17 
.equ EXTI_RTSR_TR18_Pos, (18) 
.equ EXTI_RTSR_TR18_Msk, (0x1 << EXTI_RTSR_TR18_Pos) //!< 0x00040000 
.equ EXTI_RTSR_TR18, EXTI_RTSR_TR18_Msk //!< Rising trigger event configuration bit of line 18 
.equ EXTI_RTSR_TR19_Pos, (19) 
.equ EXTI_RTSR_TR19_Msk, (0x1 << EXTI_RTSR_TR19_Pos) //!< 0x00080000 
.equ EXTI_RTSR_TR19, EXTI_RTSR_TR19_Msk //!< Rising trigger event configuration bit of line 19 
.equ EXTI_RTSR_TR20_Pos, (20) 
.equ EXTI_RTSR_TR20_Msk, (0x1 << EXTI_RTSR_TR20_Pos) //!< 0x00100000 
.equ EXTI_RTSR_TR20, EXTI_RTSR_TR20_Msk //!< Rising trigger event configuration bit of line 20 
.equ EXTI_RTSR_TR21_Pos, (21) 
.equ EXTI_RTSR_TR21_Msk, (0x1 << EXTI_RTSR_TR21_Pos) //!< 0x00200000 
.equ EXTI_RTSR_TR21, EXTI_RTSR_TR21_Msk //!< Rising trigger event configuration bit of line 21 
.equ EXTI_RTSR_TR22_Pos, (22) 
.equ EXTI_RTSR_TR22_Msk, (0x1 << EXTI_RTSR_TR22_Pos) //!< 0x00400000 
.equ EXTI_RTSR_TR22, EXTI_RTSR_TR22_Msk //!< Rising trigger event configuration bit of line 22 
.equ EXTI_RTSR_TR29_Pos, (29) 
.equ EXTI_RTSR_TR29_Msk, (0x1 << EXTI_RTSR_TR29_Pos) //!< 0x20000000 
.equ EXTI_RTSR_TR29, EXTI_RTSR_TR29_Msk //!< Rising trigger event configuration bit of line 29 
.equ EXTI_RTSR_TR30_Pos, (30) 
.equ EXTI_RTSR_TR30_Msk, (0x1 << EXTI_RTSR_TR30_Pos) //!< 0x40000000 
.equ EXTI_RTSR_TR30, EXTI_RTSR_TR30_Msk //!< Rising trigger event configuration bit of line 30 
.equ EXTI_RTSR_TR31_Pos, (31) 
.equ EXTI_RTSR_TR31_Msk, (0x1 << EXTI_RTSR_TR31_Pos) //!< 0x80000000 
.equ EXTI_RTSR_TR31, EXTI_RTSR_TR31_Msk //!< Rising trigger event configuration bit of line 31 
//*****************  Bit definition for EXTI_FTSR register  ******************
.equ EXTI_FTSR_TR0_Pos, (0) 
.equ EXTI_FTSR_TR0_Msk, (0x1 << EXTI_FTSR_TR0_Pos) //!< 0x00000001 
.equ EXTI_FTSR_TR0, EXTI_FTSR_TR0_Msk //!< Falling trigger event configuration bit of line 0 
.equ EXTI_FTSR_TR1_Pos, (1) 
.equ EXTI_FTSR_TR1_Msk, (0x1 << EXTI_FTSR_TR1_Pos) //!< 0x00000002 
.equ EXTI_FTSR_TR1, EXTI_FTSR_TR1_Msk //!< Falling trigger event configuration bit of line 1 
.equ EXTI_FTSR_TR2_Pos, (2) 
.equ EXTI_FTSR_TR2_Msk, (0x1 << EXTI_FTSR_TR2_Pos) //!< 0x00000004 
.equ EXTI_FTSR_TR2, EXTI_FTSR_TR2_Msk //!< Falling trigger event configuration bit of line 2 
.equ EXTI_FTSR_TR3_Pos, (3) 
.equ EXTI_FTSR_TR3_Msk, (0x1 << EXTI_FTSR_TR3_Pos) //!< 0x00000008 
.equ EXTI_FTSR_TR3, EXTI_FTSR_TR3_Msk //!< Falling trigger event configuration bit of line 3 
.equ EXTI_FTSR_TR4_Pos, (4) 
.equ EXTI_FTSR_TR4_Msk, (0x1 << EXTI_FTSR_TR4_Pos) //!< 0x00000010 
.equ EXTI_FTSR_TR4, EXTI_FTSR_TR4_Msk //!< Falling trigger event configuration bit of line 4 
.equ EXTI_FTSR_TR5_Pos, (5) 
.equ EXTI_FTSR_TR5_Msk, (0x1 << EXTI_FTSR_TR5_Pos) //!< 0x00000020 
.equ EXTI_FTSR_TR5, EXTI_FTSR_TR5_Msk //!< Falling trigger event configuration bit of line 5 
.equ EXTI_FTSR_TR6_Pos, (6) 
.equ EXTI_FTSR_TR6_Msk, (0x1 << EXTI_FTSR_TR6_Pos) //!< 0x00000040 
.equ EXTI_FTSR_TR6, EXTI_FTSR_TR6_Msk //!< Falling trigger event configuration bit of line 6 
.equ EXTI_FTSR_TR7_Pos, (7) 
.equ EXTI_FTSR_TR7_Msk, (0x1 << EXTI_FTSR_TR7_Pos) //!< 0x00000080 
.equ EXTI_FTSR_TR7, EXTI_FTSR_TR7_Msk //!< Falling trigger event configuration bit of line 7 
.equ EXTI_FTSR_TR8_Pos, (8) 
.equ EXTI_FTSR_TR8_Msk, (0x1 << EXTI_FTSR_TR8_Pos) //!< 0x00000100 
.equ EXTI_FTSR_TR8, EXTI_FTSR_TR8_Msk //!< Falling trigger event configuration bit of line 8 
.equ EXTI_FTSR_TR9_Pos, (9) 
.equ EXTI_FTSR_TR9_Msk, (0x1 << EXTI_FTSR_TR9_Pos) //!< 0x00000200 
.equ EXTI_FTSR_TR9, EXTI_FTSR_TR9_Msk //!< Falling trigger event configuration bit of line 9 
.equ EXTI_FTSR_TR10_Pos, (10) 
.equ EXTI_FTSR_TR10_Msk, (0x1 << EXTI_FTSR_TR10_Pos) //!< 0x00000400 
.equ EXTI_FTSR_TR10, EXTI_FTSR_TR10_Msk //!< Falling trigger event configuration bit of line 10 
.equ EXTI_FTSR_TR11_Pos, (11) 
.equ EXTI_FTSR_TR11_Msk, (0x1 << EXTI_FTSR_TR11_Pos) //!< 0x00000800 
.equ EXTI_FTSR_TR11, EXTI_FTSR_TR11_Msk //!< Falling trigger event configuration bit of line 11 
.equ EXTI_FTSR_TR12_Pos, (12) 
.equ EXTI_FTSR_TR12_Msk, (0x1 << EXTI_FTSR_TR12_Pos) //!< 0x00001000 
.equ EXTI_FTSR_TR12, EXTI_FTSR_TR12_Msk //!< Falling trigger event configuration bit of line 12 
.equ EXTI_FTSR_TR13_Pos, (13) 
.equ EXTI_FTSR_TR13_Msk, (0x1 << EXTI_FTSR_TR13_Pos) //!< 0x00002000 
.equ EXTI_FTSR_TR13, EXTI_FTSR_TR13_Msk //!< Falling trigger event configuration bit of line 13 
.equ EXTI_FTSR_TR14_Pos, (14) 
.equ EXTI_FTSR_TR14_Msk, (0x1 << EXTI_FTSR_TR14_Pos) //!< 0x00004000 
.equ EXTI_FTSR_TR14, EXTI_FTSR_TR14_Msk //!< Falling trigger event configuration bit of line 14 
.equ EXTI_FTSR_TR15_Pos, (15) 
.equ EXTI_FTSR_TR15_Msk, (0x1 << EXTI_FTSR_TR15_Pos) //!< 0x00008000 
.equ EXTI_FTSR_TR15, EXTI_FTSR_TR15_Msk //!< Falling trigger event configuration bit of line 15 
.equ EXTI_FTSR_TR16_Pos, (16) 
.equ EXTI_FTSR_TR16_Msk, (0x1 << EXTI_FTSR_TR16_Pos) //!< 0x00010000 
.equ EXTI_FTSR_TR16, EXTI_FTSR_TR16_Msk //!< Falling trigger event configuration bit of line 16 
.equ EXTI_FTSR_TR17_Pos, (17) 
.equ EXTI_FTSR_TR17_Msk, (0x1 << EXTI_FTSR_TR17_Pos) //!< 0x00020000 
.equ EXTI_FTSR_TR17, EXTI_FTSR_TR17_Msk //!< Falling trigger event configuration bit of line 17 
.equ EXTI_FTSR_TR18_Pos, (18) 
.equ EXTI_FTSR_TR18_Msk, (0x1 << EXTI_FTSR_TR18_Pos) //!< 0x00040000 
.equ EXTI_FTSR_TR18, EXTI_FTSR_TR18_Msk //!< Falling trigger event configuration bit of line 18 
.equ EXTI_FTSR_TR19_Pos, (19) 
.equ EXTI_FTSR_TR19_Msk, (0x1 << EXTI_FTSR_TR19_Pos) //!< 0x00080000 
.equ EXTI_FTSR_TR19, EXTI_FTSR_TR19_Msk //!< Falling trigger event configuration bit of line 19 
.equ EXTI_FTSR_TR20_Pos, (20) 
.equ EXTI_FTSR_TR20_Msk, (0x1 << EXTI_FTSR_TR20_Pos) //!< 0x00100000 
.equ EXTI_FTSR_TR20, EXTI_FTSR_TR20_Msk //!< Falling trigger event configuration bit of line 20 
.equ EXTI_FTSR_TR21_Pos, (21) 
.equ EXTI_FTSR_TR21_Msk, (0x1 << EXTI_FTSR_TR21_Pos) //!< 0x00200000 
.equ EXTI_FTSR_TR21, EXTI_FTSR_TR21_Msk //!< Falling trigger event configuration bit of line 21 
.equ EXTI_FTSR_TR22_Pos, (22) 
.equ EXTI_FTSR_TR22_Msk, (0x1 << EXTI_FTSR_TR22_Pos) //!< 0x00400000 
.equ EXTI_FTSR_TR22, EXTI_FTSR_TR22_Msk //!< Falling trigger event configuration bit of line 22 
.equ EXTI_FTSR_TR29_Pos, (29) 
.equ EXTI_FTSR_TR29_Msk, (0x1 << EXTI_FTSR_TR29_Pos) //!< 0x20000000 
.equ EXTI_FTSR_TR29, EXTI_FTSR_TR29_Msk //!< Falling trigger event configuration bit of line 29 
.equ EXTI_FTSR_TR30_Pos, (30) 
.equ EXTI_FTSR_TR30_Msk, (0x1 << EXTI_FTSR_TR30_Pos) //!< 0x40000000 
.equ EXTI_FTSR_TR30, EXTI_FTSR_TR30_Msk //!< Falling trigger event configuration bit of line 30 
.equ EXTI_FTSR_TR31_Pos, (31) 
.equ EXTI_FTSR_TR31_Msk, (0x1 << EXTI_FTSR_TR31_Pos) //!< 0x80000000 
.equ EXTI_FTSR_TR31, EXTI_FTSR_TR31_Msk //!< Falling trigger event configuration bit of line 31 
//*****************  Bit definition for EXTI_SWIER register  *****************
.equ EXTI_SWIER_SWIER0_Pos, (0) 
.equ EXTI_SWIER_SWIER0_Msk, (0x1 << EXTI_SWIER_SWIER0_Pos) //!< 0x00000001 
.equ EXTI_SWIER_SWIER0, EXTI_SWIER_SWIER0_Msk //!< Software Interrupt on line 0 
.equ EXTI_SWIER_SWIER1_Pos, (1) 
.equ EXTI_SWIER_SWIER1_Msk, (0x1 << EXTI_SWIER_SWIER1_Pos) //!< 0x00000002 
.equ EXTI_SWIER_SWIER1, EXTI_SWIER_SWIER1_Msk //!< Software Interrupt on line 1 
.equ EXTI_SWIER_SWIER2_Pos, (2) 
.equ EXTI_SWIER_SWIER2_Msk, (0x1 << EXTI_SWIER_SWIER2_Pos) //!< 0x00000004 
.equ EXTI_SWIER_SWIER2, EXTI_SWIER_SWIER2_Msk //!< Software Interrupt on line 2 
.equ EXTI_SWIER_SWIER3_Pos, (3) 
.equ EXTI_SWIER_SWIER3_Msk, (0x1 << EXTI_SWIER_SWIER3_Pos) //!< 0x00000008 
.equ EXTI_SWIER_SWIER3, EXTI_SWIER_SWIER3_Msk //!< Software Interrupt on line 3 
.equ EXTI_SWIER_SWIER4_Pos, (4) 
.equ EXTI_SWIER_SWIER4_Msk, (0x1 << EXTI_SWIER_SWIER4_Pos) //!< 0x00000010 
.equ EXTI_SWIER_SWIER4, EXTI_SWIER_SWIER4_Msk //!< Software Interrupt on line 4 
.equ EXTI_SWIER_SWIER5_Pos, (5) 
.equ EXTI_SWIER_SWIER5_Msk, (0x1 << EXTI_SWIER_SWIER5_Pos) //!< 0x00000020 
.equ EXTI_SWIER_SWIER5, EXTI_SWIER_SWIER5_Msk //!< Software Interrupt on line 5 
.equ EXTI_SWIER_SWIER6_Pos, (6) 
.equ EXTI_SWIER_SWIER6_Msk, (0x1 << EXTI_SWIER_SWIER6_Pos) //!< 0x00000040 
.equ EXTI_SWIER_SWIER6, EXTI_SWIER_SWIER6_Msk //!< Software Interrupt on line 6 
.equ EXTI_SWIER_SWIER7_Pos, (7) 
.equ EXTI_SWIER_SWIER7_Msk, (0x1 << EXTI_SWIER_SWIER7_Pos) //!< 0x00000080 
.equ EXTI_SWIER_SWIER7, EXTI_SWIER_SWIER7_Msk //!< Software Interrupt on line 7 
.equ EXTI_SWIER_SWIER8_Pos, (8) 
.equ EXTI_SWIER_SWIER8_Msk, (0x1 << EXTI_SWIER_SWIER8_Pos) //!< 0x00000100 
.equ EXTI_SWIER_SWIER8, EXTI_SWIER_SWIER8_Msk //!< Software Interrupt on line 8 
.equ EXTI_SWIER_SWIER9_Pos, (9) 
.equ EXTI_SWIER_SWIER9_Msk, (0x1 << EXTI_SWIER_SWIER9_Pos) //!< 0x00000200 
.equ EXTI_SWIER_SWIER9, EXTI_SWIER_SWIER9_Msk //!< Software Interrupt on line 9 
.equ EXTI_SWIER_SWIER10_Pos, (10) 
.equ EXTI_SWIER_SWIER10_Msk, (0x1 << EXTI_SWIER_SWIER10_Pos) //!< 0x00000400 
.equ EXTI_SWIER_SWIER10, EXTI_SWIER_SWIER10_Msk //!< Software Interrupt on line 10 
.equ EXTI_SWIER_SWIER11_Pos, (11) 
.equ EXTI_SWIER_SWIER11_Msk, (0x1 << EXTI_SWIER_SWIER11_Pos) //!< 0x00000800 
.equ EXTI_SWIER_SWIER11, EXTI_SWIER_SWIER11_Msk //!< Software Interrupt on line 11 
.equ EXTI_SWIER_SWIER12_Pos, (12) 
.equ EXTI_SWIER_SWIER12_Msk, (0x1 << EXTI_SWIER_SWIER12_Pos) //!< 0x00001000 
.equ EXTI_SWIER_SWIER12, EXTI_SWIER_SWIER12_Msk //!< Software Interrupt on line 12 
.equ EXTI_SWIER_SWIER13_Pos, (13) 
.equ EXTI_SWIER_SWIER13_Msk, (0x1 << EXTI_SWIER_SWIER13_Pos) //!< 0x00002000 
.equ EXTI_SWIER_SWIER13, EXTI_SWIER_SWIER13_Msk //!< Software Interrupt on line 13 
.equ EXTI_SWIER_SWIER14_Pos, (14) 
.equ EXTI_SWIER_SWIER14_Msk, (0x1 << EXTI_SWIER_SWIER14_Pos) //!< 0x00004000 
.equ EXTI_SWIER_SWIER14, EXTI_SWIER_SWIER14_Msk //!< Software Interrupt on line 14 
.equ EXTI_SWIER_SWIER15_Pos, (15) 
.equ EXTI_SWIER_SWIER15_Msk, (0x1 << EXTI_SWIER_SWIER15_Pos) //!< 0x00008000 
.equ EXTI_SWIER_SWIER15, EXTI_SWIER_SWIER15_Msk //!< Software Interrupt on line 15 
.equ EXTI_SWIER_SWIER16_Pos, (16) 
.equ EXTI_SWIER_SWIER16_Msk, (0x1 << EXTI_SWIER_SWIER16_Pos) //!< 0x00010000 
.equ EXTI_SWIER_SWIER16, EXTI_SWIER_SWIER16_Msk //!< Software Interrupt on line 16 
.equ EXTI_SWIER_SWIER17_Pos, (17) 
.equ EXTI_SWIER_SWIER17_Msk, (0x1 << EXTI_SWIER_SWIER17_Pos) //!< 0x00020000 
.equ EXTI_SWIER_SWIER17, EXTI_SWIER_SWIER17_Msk //!< Software Interrupt on line 17 
.equ EXTI_SWIER_SWIER18_Pos, (18) 
.equ EXTI_SWIER_SWIER18_Msk, (0x1 << EXTI_SWIER_SWIER18_Pos) //!< 0x00040000 
.equ EXTI_SWIER_SWIER18, EXTI_SWIER_SWIER18_Msk //!< Software Interrupt on line 18 
.equ EXTI_SWIER_SWIER19_Pos, (19) 
.equ EXTI_SWIER_SWIER19_Msk, (0x1 << EXTI_SWIER_SWIER19_Pos) //!< 0x00080000 
.equ EXTI_SWIER_SWIER19, EXTI_SWIER_SWIER19_Msk //!< Software Interrupt on line 19 
.equ EXTI_SWIER_SWIER20_Pos, (20) 
.equ EXTI_SWIER_SWIER20_Msk, (0x1 << EXTI_SWIER_SWIER20_Pos) //!< 0x00100000 
.equ EXTI_SWIER_SWIER20, EXTI_SWIER_SWIER20_Msk //!< Software Interrupt on line 20 
.equ EXTI_SWIER_SWIER21_Pos, (21) 
.equ EXTI_SWIER_SWIER21_Msk, (0x1 << EXTI_SWIER_SWIER21_Pos) //!< 0x00200000 
.equ EXTI_SWIER_SWIER21, EXTI_SWIER_SWIER21_Msk //!< Software Interrupt on line 21 
.equ EXTI_SWIER_SWIER22_Pos, (22) 
.equ EXTI_SWIER_SWIER22_Msk, (0x1 << EXTI_SWIER_SWIER22_Pos) //!< 0x00400000 
.equ EXTI_SWIER_SWIER22, EXTI_SWIER_SWIER22_Msk //!< Software Interrupt on line 22 
.equ EXTI_SWIER_SWIER29_Pos, (29) 
.equ EXTI_SWIER_SWIER29_Msk, (0x1 << EXTI_SWIER_SWIER29_Pos) //!< 0x20000000 
.equ EXTI_SWIER_SWIER29, EXTI_SWIER_SWIER29_Msk //!< Software Interrupt on line 29 
.equ EXTI_SWIER_SWIER30_Pos, (30) 
.equ EXTI_SWIER_SWIER30_Msk, (0x1 << EXTI_SWIER_SWIER30_Pos) //!< 0x40000000 
.equ EXTI_SWIER_SWIER30, EXTI_SWIER_SWIER30_Msk //!< Software Interrupt on line 30 
.equ EXTI_SWIER_SWIER31_Pos, (31) 
.equ EXTI_SWIER_SWIER31_Msk, (0x1 << EXTI_SWIER_SWIER31_Pos) //!< 0x80000000 
.equ EXTI_SWIER_SWIER31, EXTI_SWIER_SWIER31_Msk //!< Software Interrupt on line 31 
//******************  Bit definition for EXTI_PR register  *******************
.equ EXTI_PR_PR0_Pos, (0) 
.equ EXTI_PR_PR0_Msk, (0x1 << EXTI_PR_PR0_Pos) //!< 0x00000001 
.equ EXTI_PR_PR0, EXTI_PR_PR0_Msk //!< Pending bit for line 0 
.equ EXTI_PR_PR1_Pos, (1) 
.equ EXTI_PR_PR1_Msk, (0x1 << EXTI_PR_PR1_Pos) //!< 0x00000002 
.equ EXTI_PR_PR1, EXTI_PR_PR1_Msk //!< Pending bit for line 1 
.equ EXTI_PR_PR2_Pos, (2) 
.equ EXTI_PR_PR2_Msk, (0x1 << EXTI_PR_PR2_Pos) //!< 0x00000004 
.equ EXTI_PR_PR2, EXTI_PR_PR2_Msk //!< Pending bit for line 2 
.equ EXTI_PR_PR3_Pos, (3) 
.equ EXTI_PR_PR3_Msk, (0x1 << EXTI_PR_PR3_Pos) //!< 0x00000008 
.equ EXTI_PR_PR3, EXTI_PR_PR3_Msk //!< Pending bit for line 3 
.equ EXTI_PR_PR4_Pos, (4) 
.equ EXTI_PR_PR4_Msk, (0x1 << EXTI_PR_PR4_Pos) //!< 0x00000010 
.equ EXTI_PR_PR4, EXTI_PR_PR4_Msk //!< Pending bit for line 4 
.equ EXTI_PR_PR5_Pos, (5) 
.equ EXTI_PR_PR5_Msk, (0x1 << EXTI_PR_PR5_Pos) //!< 0x00000020 
.equ EXTI_PR_PR5, EXTI_PR_PR5_Msk //!< Pending bit for line 5 
.equ EXTI_PR_PR6_Pos, (6) 
.equ EXTI_PR_PR6_Msk, (0x1 << EXTI_PR_PR6_Pos) //!< 0x00000040 
.equ EXTI_PR_PR6, EXTI_PR_PR6_Msk //!< Pending bit for line 6 
.equ EXTI_PR_PR7_Pos, (7) 
.equ EXTI_PR_PR7_Msk, (0x1 << EXTI_PR_PR7_Pos) //!< 0x00000080 
.equ EXTI_PR_PR7, EXTI_PR_PR7_Msk //!< Pending bit for line 7 
.equ EXTI_PR_PR8_Pos, (8) 
.equ EXTI_PR_PR8_Msk, (0x1 << EXTI_PR_PR8_Pos) //!< 0x00000100 
.equ EXTI_PR_PR8, EXTI_PR_PR8_Msk //!< Pending bit for line 8 
.equ EXTI_PR_PR9_Pos, (9) 
.equ EXTI_PR_PR9_Msk, (0x1 << EXTI_PR_PR9_Pos) //!< 0x00000200 
.equ EXTI_PR_PR9, EXTI_PR_PR9_Msk //!< Pending bit for line 9 
.equ EXTI_PR_PR10_Pos, (10) 
.equ EXTI_PR_PR10_Msk, (0x1 << EXTI_PR_PR10_Pos) //!< 0x00000400 
.equ EXTI_PR_PR10, EXTI_PR_PR10_Msk //!< Pending bit for line 10 
.equ EXTI_PR_PR11_Pos, (11) 
.equ EXTI_PR_PR11_Msk, (0x1 << EXTI_PR_PR11_Pos) //!< 0x00000800 
.equ EXTI_PR_PR11, EXTI_PR_PR11_Msk //!< Pending bit for line 11 
.equ EXTI_PR_PR12_Pos, (12) 
.equ EXTI_PR_PR12_Msk, (0x1 << EXTI_PR_PR12_Pos) //!< 0x00001000 
.equ EXTI_PR_PR12, EXTI_PR_PR12_Msk //!< Pending bit for line 12 
.equ EXTI_PR_PR13_Pos, (13) 
.equ EXTI_PR_PR13_Msk, (0x1 << EXTI_PR_PR13_Pos) //!< 0x00002000 
.equ EXTI_PR_PR13, EXTI_PR_PR13_Msk //!< Pending bit for line 13 
.equ EXTI_PR_PR14_Pos, (14) 
.equ EXTI_PR_PR14_Msk, (0x1 << EXTI_PR_PR14_Pos) //!< 0x00004000 
.equ EXTI_PR_PR14, EXTI_PR_PR14_Msk //!< Pending bit for line 14 
.equ EXTI_PR_PR15_Pos, (15) 
.equ EXTI_PR_PR15_Msk, (0x1 << EXTI_PR_PR15_Pos) //!< 0x00008000 
.equ EXTI_PR_PR15, EXTI_PR_PR15_Msk //!< Pending bit for line 15 
.equ EXTI_PR_PR16_Pos, (16) 
.equ EXTI_PR_PR16_Msk, (0x1 << EXTI_PR_PR16_Pos) //!< 0x00010000 
.equ EXTI_PR_PR16, EXTI_PR_PR16_Msk //!< Pending bit for line 16 
.equ EXTI_PR_PR17_Pos, (17) 
.equ EXTI_PR_PR17_Msk, (0x1 << EXTI_PR_PR17_Pos) //!< 0x00020000 
.equ EXTI_PR_PR17, EXTI_PR_PR17_Msk //!< Pending bit for line 17 
.equ EXTI_PR_PR18_Pos, (18) 
.equ EXTI_PR_PR18_Msk, (0x1 << EXTI_PR_PR18_Pos) //!< 0x00040000 
.equ EXTI_PR_PR18, EXTI_PR_PR18_Msk //!< Pending bit for line 18 
.equ EXTI_PR_PR19_Pos, (19) 
.equ EXTI_PR_PR19_Msk, (0x1 << EXTI_PR_PR19_Pos) //!< 0x00080000 
.equ EXTI_PR_PR19, EXTI_PR_PR19_Msk //!< Pending bit for line 19 
.equ EXTI_PR_PR20_Pos, (20) 
.equ EXTI_PR_PR20_Msk, (0x1 << EXTI_PR_PR20_Pos) //!< 0x00100000 
.equ EXTI_PR_PR20, EXTI_PR_PR20_Msk //!< Pending bit for line 20 
.equ EXTI_PR_PR21_Pos, (21) 
.equ EXTI_PR_PR21_Msk, (0x1 << EXTI_PR_PR21_Pos) //!< 0x00200000 
.equ EXTI_PR_PR21, EXTI_PR_PR21_Msk //!< Pending bit for line 21 
.equ EXTI_PR_PR22_Pos, (22) 
.equ EXTI_PR_PR22_Msk, (0x1 << EXTI_PR_PR22_Pos) //!< 0x00400000 
.equ EXTI_PR_PR22, EXTI_PR_PR22_Msk //!< Pending bit for line 22 
.equ EXTI_PR_PR29_Pos, (29) 
.equ EXTI_PR_PR29_Msk, (0x1 << EXTI_PR_PR29_Pos) //!< 0x20000000 
.equ EXTI_PR_PR29, EXTI_PR_PR29_Msk //!< Pending bit for line 29 
.equ EXTI_PR_PR30_Pos, (30) 
.equ EXTI_PR_PR30_Msk, (0x1 << EXTI_PR_PR30_Pos) //!< 0x40000000 
.equ EXTI_PR_PR30, EXTI_PR_PR30_Msk //!< Pending bit for line 30 
.equ EXTI_PR_PR31_Pos, (31) 
.equ EXTI_PR_PR31_Msk, (0x1 << EXTI_PR_PR31_Pos) //!< 0x80000000 
.equ EXTI_PR_PR31, EXTI_PR_PR31_Msk //!< Pending bit for line 31 
//******************  Bit definition for EXTI_IMR2 register  *****************
.equ EXTI_IMR2_MR32_Pos, (0) 
.equ EXTI_IMR2_MR32_Msk, (0x1 << EXTI_IMR2_MR32_Pos) //!< 0x00000001 
.equ EXTI_IMR2_MR32, EXTI_IMR2_MR32_Msk //!< Interrupt Mask on line 32 
.equ EXTI_IMR2_MR33_Pos, (1) 
.equ EXTI_IMR2_MR33_Msk, (0x1 << EXTI_IMR2_MR33_Pos) //!< 0x00000002 
.equ EXTI_IMR2_MR33, EXTI_IMR2_MR33_Msk //!< Interrupt Mask on line 33 
.equ EXTI_IMR2_MR34_Pos, (2) 
.equ EXTI_IMR2_MR34_Msk, (0x1 << EXTI_IMR2_MR34_Pos) //!< 0x00000004 
.equ EXTI_IMR2_MR34, EXTI_IMR2_MR34_Msk //!< Interrupt Mask on line 34 
.equ EXTI_IMR2_MR35_Pos, (3) 
.equ EXTI_IMR2_MR35_Msk, (0x1 << EXTI_IMR2_MR35_Pos) //!< 0x00000008 
.equ EXTI_IMR2_MR35, EXTI_IMR2_MR35_Msk //!< Interrupt Mask on line 35 
// References Defines
.equ EXTI_IMR2_IM32, EXTI_IMR2_MR32 
.equ EXTI_IMR2_IM33, EXTI_IMR2_MR33 
.equ EXTI_IMR2_IM34, EXTI_IMR2_MR34 
.equ EXTI_IMR2_IM35, EXTI_IMR2_MR35 
.equ EXTI_IMR2_IM_Pos, (0) 
.equ EXTI_IMR2_IM_Msk, (0xF << EXTI_IMR2_IM_Pos) //!< 0x0000000F 
.equ EXTI_IMR2_IM, EXTI_IMR2_IM_Msk 
//******************  Bit definition for EXTI_EMR2 ***************************
.equ EXTI_EMR2_MR32_Pos, (0) 
.equ EXTI_EMR2_MR32_Msk, (0x1 << EXTI_EMR2_MR32_Pos) //!< 0x00000001 
.equ EXTI_EMR2_MR32, EXTI_EMR2_MR32_Msk //!< Event Mask on line 32 
.equ EXTI_EMR2_MR33_Pos, (1) 
.equ EXTI_EMR2_MR33_Msk, (0x1 << EXTI_EMR2_MR33_Pos) //!< 0x00000002 
.equ EXTI_EMR2_MR33, EXTI_EMR2_MR33_Msk //!< Event Mask on line 33 
.equ EXTI_EMR2_MR34_Pos, (2) 
.equ EXTI_EMR2_MR34_Msk, (0x1 << EXTI_EMR2_MR34_Pos) //!< 0x00000004 
.equ EXTI_EMR2_MR34, EXTI_EMR2_MR34_Msk //!< Event Mask on line 34 
.equ EXTI_EMR2_MR35_Pos, (3) 
.equ EXTI_EMR2_MR35_Msk, (0x1 << EXTI_EMR2_MR35_Pos) //!< 0x00000008 
.equ EXTI_EMR2_MR35, EXTI_EMR2_MR35_Msk //!< Event Mask on line 34 
// References Defines
.equ EXTI_EMR2_EM32, EXTI_EMR2_MR32 
.equ EXTI_EMR2_EM33, EXTI_EMR2_MR33 
.equ EXTI_EMR2_EM34, EXTI_EMR2_MR34 
.equ EXTI_EMR2_EM35, EXTI_EMR2_MR35 
.equ EXTI_EMR2_EM_Pos, (0) 
.equ EXTI_EMR2_EM_Msk, (0xF << EXTI_EMR2_EM_Pos) //!< 0x0000000F 
.equ EXTI_EMR2_EM, EXTI_EMR2_EM_Msk 
//*****************  Bit definition for EXTI_RTSR2 register *******************
.equ EXTI_RTSR2_TR32_Pos, (0) 
.equ EXTI_RTSR2_TR32_Msk, (0x1 << EXTI_RTSR2_TR32_Pos) //!< 0x00000001 
.equ EXTI_RTSR2_TR32, EXTI_RTSR2_TR32_Msk //!< Rising trigger event configuration bit of line 32 
.equ EXTI_RTSR2_TR33_Pos, (1) 
.equ EXTI_RTSR2_TR33_Msk, (0x1 << EXTI_RTSR2_TR33_Pos) //!< 0x00000002 
.equ EXTI_RTSR2_TR33, EXTI_RTSR2_TR33_Msk //!< Rising trigger event configuration bit of line 33 
//*****************  Bit definition for EXTI_FTSR2 register  *****************
.equ EXTI_FTSR2_TR32_Pos, (0) 
.equ EXTI_FTSR2_TR32_Msk, (0x1 << EXTI_FTSR2_TR32_Pos) //!< 0x00000001 
.equ EXTI_FTSR2_TR32, EXTI_FTSR2_TR32_Msk //!< Falling trigger event configuration bit of line 32 
.equ EXTI_FTSR2_TR33_Pos, (1) 
.equ EXTI_FTSR2_TR33_Msk, (0x1 << EXTI_FTSR2_TR33_Pos) //!< 0x00000002 
.equ EXTI_FTSR2_TR33, EXTI_FTSR2_TR33_Msk //!< Falling trigger event configuration bit of line 33 
//*****************  Bit definition for EXTI_SWIER2 register  ****************
.equ EXTI_SWIER2_SWIER32_Pos, (0) 
.equ EXTI_SWIER2_SWIER32_Msk, (0x1 << EXTI_SWIER2_SWIER32_Pos) //!< 0x00000001 
.equ EXTI_SWIER2_SWIER32, EXTI_SWIER2_SWIER32_Msk //!< Software Interrupt on line 32 
.equ EXTI_SWIER2_SWIER33_Pos, (1) 
.equ EXTI_SWIER2_SWIER33_Msk, (0x1 << EXTI_SWIER2_SWIER33_Pos) //!< 0x00000002 
.equ EXTI_SWIER2_SWIER33, EXTI_SWIER2_SWIER33_Msk //!< Software Interrupt on line 33 
//******************  Bit definition for EXTI_PR2 register  ******************
.equ EXTI_PR2_PR32_Pos, (0) 
.equ EXTI_PR2_PR32_Msk, (0x1 << EXTI_PR2_PR32_Pos) //!< 0x00000001 
.equ EXTI_PR2_PR32, EXTI_PR2_PR32_Msk //!< Pending bit for line 32 
.equ EXTI_PR2_PR33_Pos, (1) 
.equ EXTI_PR2_PR33_Msk, (0x1 << EXTI_PR2_PR33_Pos) //!< 0x00000002 
.equ EXTI_PR2_PR33, EXTI_PR2_PR33_Msk //!< Pending bit for line 33 
//****************************************************************************
//
//                                    FLASH
//
//****************************************************************************
//******************  Bit definition for FLASH_ACR register  *****************
.equ FLASH_ACR_LATENCY_Pos, (0) 
.equ FLASH_ACR_LATENCY_Msk, (0x7 << FLASH_ACR_LATENCY_Pos) //!< 0x00000007 
.equ FLASH_ACR_LATENCY, FLASH_ACR_LATENCY_Msk //!< LATENCY[2:0] bits (Latency) 
.equ FLASH_ACR_LATENCY_0, (0x1 << FLASH_ACR_LATENCY_Pos) //!< 0x00000001 
.equ FLASH_ACR_LATENCY_1, (0x2 << FLASH_ACR_LATENCY_Pos) //!< 0x00000002 
.equ FLASH_ACR_LATENCY_2, (0x4 << FLASH_ACR_LATENCY_Pos) //!< 0x00000004 
.equ FLASH_ACR_HLFCYA_Pos, (3) 
.equ FLASH_ACR_HLFCYA_Msk, (0x1 << FLASH_ACR_HLFCYA_Pos) //!< 0x00000008 
.equ FLASH_ACR_HLFCYA, FLASH_ACR_HLFCYA_Msk //!< Flash Half Cycle Access Enable 
.equ FLASH_ACR_PRFTBE_Pos, (4) 
.equ FLASH_ACR_PRFTBE_Msk, (0x1 << FLASH_ACR_PRFTBE_Pos) //!< 0x00000010 
.equ FLASH_ACR_PRFTBE, FLASH_ACR_PRFTBE_Msk //!< Prefetch Buffer Enable 
.equ FLASH_ACR_PRFTBS_Pos, (5) 
.equ FLASH_ACR_PRFTBS_Msk, (0x1 << FLASH_ACR_PRFTBS_Pos) //!< 0x00000020 
.equ FLASH_ACR_PRFTBS, FLASH_ACR_PRFTBS_Msk //!< Prefetch Buffer Status 
//*****************  Bit definition for FLASH_KEYR register  *****************
.equ FLASH_KEYR_FKEYR_Pos, (0) 
.equ FLASH_KEYR_FKEYR_Msk, (0xFFFFFFFF << FLASH_KEYR_FKEYR_Pos) //!< 0xFFFFFFFF 
.equ FLASH_KEYR_FKEYR, FLASH_KEYR_FKEYR_Msk //!< FPEC Key 
.equ RDP_KEY_Pos, (0) 
.equ RDP_KEY_Msk, (0xA5 << RDP_KEY_Pos) //!< 0x000000A5 
.equ RDP_KEY, RDP_KEY_Msk //!< RDP Key 
.equ FLASH_KEY1_Pos, (0) 
.equ FLASH_KEY1_Msk, (0x45670123 << FLASH_KEY1_Pos) //!< 0x45670123 
.equ FLASH_KEY1, FLASH_KEY1_Msk //!< FPEC Key1 
.equ FLASH_KEY2_Pos, (0) 
.equ FLASH_KEY2_Msk, (0xCDEF89AB << FLASH_KEY2_Pos) //!< 0xCDEF89AB 
.equ FLASH_KEY2, FLASH_KEY2_Msk //!< FPEC Key2 
//****************  Bit definition for FLASH_OPTKEYR register  ***************
.equ FLASH_OPTKEYR_OPTKEYR_Pos, (0) 
.equ FLASH_OPTKEYR_OPTKEYR_Msk, (0xFFFFFFFF << FLASH_OPTKEYR_OPTKEYR_Pos) //!< 0xFFFFFFFF 
.equ FLASH_OPTKEYR_OPTKEYR, FLASH_OPTKEYR_OPTKEYR_Msk //!< Option Byte Key 
.equ FLASH_OPTKEY1, FLASH_KEY1 //!< Option Byte Key1 
.equ FLASH_OPTKEY2, FLASH_KEY2 //!< Option Byte Key2 
//*****************  Bit definition for FLASH_SR register  ******************
.equ FLASH_SR_BSY_Pos, (0) 
.equ FLASH_SR_BSY_Msk, (0x1 << FLASH_SR_BSY_Pos) //!< 0x00000001 
.equ FLASH_SR_BSY, FLASH_SR_BSY_Msk //!< Busy 
.equ FLASH_SR_PGERR_Pos, (2) 
.equ FLASH_SR_PGERR_Msk, (0x1 << FLASH_SR_PGERR_Pos) //!< 0x00000004 
.equ FLASH_SR_PGERR, FLASH_SR_PGERR_Msk //!< Programming Error 
.equ FLASH_SR_WRPERR_Pos, (4) 
.equ FLASH_SR_WRPERR_Msk, (0x1 << FLASH_SR_WRPERR_Pos) //!< 0x00000010 
.equ FLASH_SR_WRPERR, FLASH_SR_WRPERR_Msk //!< Write Protection Error 
.equ FLASH_SR_EOP_Pos, (5) 
.equ FLASH_SR_EOP_Msk, (0x1 << FLASH_SR_EOP_Pos) //!< 0x00000020 
.equ FLASH_SR_EOP, FLASH_SR_EOP_Msk //!< End of operation 
//******************  Bit definition for FLASH_CR register  ******************
.equ FLASH_CR_PG_Pos, (0) 
.equ FLASH_CR_PG_Msk, (0x1 << FLASH_CR_PG_Pos) //!< 0x00000001 
.equ FLASH_CR_PG, FLASH_CR_PG_Msk //!< Programming 
.equ FLASH_CR_PER_Pos, (1) 
.equ FLASH_CR_PER_Msk, (0x1 << FLASH_CR_PER_Pos) //!< 0x00000002 
.equ FLASH_CR_PER, FLASH_CR_PER_Msk //!< Page Erase 
.equ FLASH_CR_MER_Pos, (2) 
.equ FLASH_CR_MER_Msk, (0x1 << FLASH_CR_MER_Pos) //!< 0x00000004 
.equ FLASH_CR_MER, FLASH_CR_MER_Msk //!< Mass Erase 
.equ FLASH_CR_OPTPG_Pos, (4) 
.equ FLASH_CR_OPTPG_Msk, (0x1 << FLASH_CR_OPTPG_Pos) //!< 0x00000010 
.equ FLASH_CR_OPTPG, FLASH_CR_OPTPG_Msk //!< Option Byte Programming 
.equ FLASH_CR_OPTER_Pos, (5) 
.equ FLASH_CR_OPTER_Msk, (0x1 << FLASH_CR_OPTER_Pos) //!< 0x00000020 
.equ FLASH_CR_OPTER, FLASH_CR_OPTER_Msk //!< Option Byte Erase 
.equ FLASH_CR_STRT_Pos, (6) 
.equ FLASH_CR_STRT_Msk, (0x1 << FLASH_CR_STRT_Pos) //!< 0x00000040 
.equ FLASH_CR_STRT, FLASH_CR_STRT_Msk //!< Start 
.equ FLASH_CR_LOCK_Pos, (7) 
.equ FLASH_CR_LOCK_Msk, (0x1 << FLASH_CR_LOCK_Pos) //!< 0x00000080 
.equ FLASH_CR_LOCK, FLASH_CR_LOCK_Msk //!< Lock 
.equ FLASH_CR_OPTWRE_Pos, (9) 
.equ FLASH_CR_OPTWRE_Msk, (0x1 << FLASH_CR_OPTWRE_Pos) //!< 0x00000200 
.equ FLASH_CR_OPTWRE, FLASH_CR_OPTWRE_Msk //!< Option Bytes Write Enable 
.equ FLASH_CR_ERRIE_Pos, (10) 
.equ FLASH_CR_ERRIE_Msk, (0x1 << FLASH_CR_ERRIE_Pos) //!< 0x00000400 
.equ FLASH_CR_ERRIE, FLASH_CR_ERRIE_Msk //!< Error Interrupt Enable 
.equ FLASH_CR_EOPIE_Pos, (12) 
.equ FLASH_CR_EOPIE_Msk, (0x1 << FLASH_CR_EOPIE_Pos) //!< 0x00001000 
.equ FLASH_CR_EOPIE, FLASH_CR_EOPIE_Msk //!< End of operation interrupt enable 
.equ FLASH_CR_OBL_LAUNCH_Pos, (13) 
.equ FLASH_CR_OBL_LAUNCH_Msk, (0x1 << FLASH_CR_OBL_LAUNCH_Pos) //!< 0x00002000 
.equ FLASH_CR_OBL_LAUNCH, FLASH_CR_OBL_LAUNCH_Msk //!< OptionBytes Loader Launch 
//******************  Bit definition for FLASH_AR register  ******************
.equ FLASH_AR_FAR_Pos, (0) 
.equ FLASH_AR_FAR_Msk, (0xFFFFFFFF << FLASH_AR_FAR_Pos) //!< 0xFFFFFFFF 
.equ FLASH_AR_FAR, FLASH_AR_FAR_Msk //!< Flash Address 
//*****************  Bit definition for FLASH_OBR register  ******************
.equ FLASH_OBR_OPTERR_Pos, (0) 
.equ FLASH_OBR_OPTERR_Msk, (0x1 << FLASH_OBR_OPTERR_Pos) //!< 0x00000001 
.equ FLASH_OBR_OPTERR, FLASH_OBR_OPTERR_Msk //!< Option Byte Error 
.equ FLASH_OBR_RDPRT_Pos, (1) 
.equ FLASH_OBR_RDPRT_Msk, (0x3 << FLASH_OBR_RDPRT_Pos) //!< 0x00000006 
.equ FLASH_OBR_RDPRT, FLASH_OBR_RDPRT_Msk //!< Read protection 
.equ FLASH_OBR_RDPRT_1, (0x1 << FLASH_OBR_RDPRT_Pos) //!< 0x00000002 
.equ FLASH_OBR_RDPRT_2, (0x3 << FLASH_OBR_RDPRT_Pos) //!< 0x00000006 
.equ FLASH_OBR_USER_Pos, (8) 
.equ FLASH_OBR_USER_Msk, (0x77 << FLASH_OBR_USER_Pos) //!< 0x00007700 
.equ FLASH_OBR_USER, FLASH_OBR_USER_Msk //!< User Option Bytes 
.equ FLASH_OBR_IWDG_SW_Pos, (8) 
.equ FLASH_OBR_IWDG_SW_Msk, (0x1 << FLASH_OBR_IWDG_SW_Pos) //!< 0x00000100 
.equ FLASH_OBR_IWDG_SW, FLASH_OBR_IWDG_SW_Msk //!< IWDG SW 
.equ FLASH_OBR_nRST_STOP_Pos, (9) 
.equ FLASH_OBR_nRST_STOP_Msk, (0x1 << FLASH_OBR_nRST_STOP_Pos) //!< 0x00000200 
.equ FLASH_OBR_nRST_STOP, FLASH_OBR_nRST_STOP_Msk //!< nRST_STOP 
.equ FLASH_OBR_nRST_STDBY_Pos, (10) 
.equ FLASH_OBR_nRST_STDBY_Msk, (0x1 << FLASH_OBR_nRST_STDBY_Pos) //!< 0x00000400 
.equ FLASH_OBR_nRST_STDBY, FLASH_OBR_nRST_STDBY_Msk //!< nRST_STDBY 
.equ FLASH_OBR_nBOOT1_Pos, (12) 
.equ FLASH_OBR_nBOOT1_Msk, (0x1 << FLASH_OBR_nBOOT1_Pos) //!< 0x00001000 
.equ FLASH_OBR_nBOOT1, FLASH_OBR_nBOOT1_Msk //!< nBOOT1 
.equ FLASH_OBR_VDDA_MONITOR_Pos, (13) 
.equ FLASH_OBR_VDDA_MONITOR_Msk, (0x1 << FLASH_OBR_VDDA_MONITOR_Pos) //!< 0x00002000 
.equ FLASH_OBR_VDDA_MONITOR, FLASH_OBR_VDDA_MONITOR_Msk //!< VDDA_MONITOR 
.equ FLASH_OBR_SRAM_PE_Pos, (14) 
.equ FLASH_OBR_SRAM_PE_Msk, (0x1 << FLASH_OBR_SRAM_PE_Pos) //!< 0x00004000 
.equ FLASH_OBR_SRAM_PE, FLASH_OBR_SRAM_PE_Msk //!< SRAM_PE 
.equ FLASH_OBR_DATA0_Pos, (16) 
.equ FLASH_OBR_DATA0_Msk, (0xFF << FLASH_OBR_DATA0_Pos) //!< 0x00FF0000 
.equ FLASH_OBR_DATA0, FLASH_OBR_DATA0_Msk //!< Data0 
.equ FLASH_OBR_DATA1_Pos, (24) 
.equ FLASH_OBR_DATA1_Msk, (0xFF << FLASH_OBR_DATA1_Pos) //!< 0xFF000000 
.equ FLASH_OBR_DATA1, FLASH_OBR_DATA1_Msk //!< Data1 
// Legacy defines
.equ FLASH_OBR_WDG_SW, FLASH_OBR_IWDG_SW 
//*****************  Bit definition for FLASH_WRPR register  *****************
.equ FLASH_WRPR_WRP_Pos, (0) 
.equ FLASH_WRPR_WRP_Msk, (0xFFFFFFFF << FLASH_WRPR_WRP_Pos) //!< 0xFFFFFFFF 
.equ FLASH_WRPR_WRP, FLASH_WRPR_WRP_Msk //!< Write Protect 
//----------------------------------------------------------------------------
//*****************  Bit definition for OB_RDP register  *********************
.equ OB_RDP_RDP_Pos, (0) 
.equ OB_RDP_RDP_Msk, (0xFF << OB_RDP_RDP_Pos) //!< 0x000000FF 
.equ OB_RDP_RDP, OB_RDP_RDP_Msk //!< Read protection option byte 
.equ OB_RDP_nRDP_Pos, (8) 
.equ OB_RDP_nRDP_Msk, (0xFF << OB_RDP_nRDP_Pos) //!< 0x0000FF00 
.equ OB_RDP_nRDP, OB_RDP_nRDP_Msk //!< Read protection complemented option byte 
//*****************  Bit definition for OB_USER register  ********************
.equ OB_USER_USER_Pos, (16) 
.equ OB_USER_USER_Msk, (0xFF << OB_USER_USER_Pos) //!< 0x00FF0000 
.equ OB_USER_USER, OB_USER_USER_Msk //!< User option byte 
.equ OB_USER_nUSER_Pos, (24) 
.equ OB_USER_nUSER_Msk, (0xFF << OB_USER_nUSER_Pos) //!< 0xFF000000 
.equ OB_USER_nUSER, OB_USER_nUSER_Msk //!< User complemented option byte 
//*****************  Bit definition for FLASH_WRP0 register  *****************
.equ OB_WRP0_WRP0_Pos, (0) 
.equ OB_WRP0_WRP0_Msk, (0xFF << OB_WRP0_WRP0_Pos) //!< 0x000000FF 
.equ OB_WRP0_WRP0, OB_WRP0_WRP0_Msk //!< Flash memory write protection option bytes 
.equ OB_WRP0_nWRP0_Pos, (8) 
.equ OB_WRP0_nWRP0_Msk, (0xFF << OB_WRP0_nWRP0_Pos) //!< 0x0000FF00 
.equ OB_WRP0_nWRP0, OB_WRP0_nWRP0_Msk //!< Flash memory write protection complemented option bytes 
//*****************  Bit definition for FLASH_WRP1 register  *****************
.equ OB_WRP1_WRP1_Pos, (16) 
.equ OB_WRP1_WRP1_Msk, (0xFF << OB_WRP1_WRP1_Pos) //!< 0x00FF0000 
.equ OB_WRP1_WRP1, OB_WRP1_WRP1_Msk //!< Flash memory write protection option bytes 
.equ OB_WRP1_nWRP1_Pos, (24) 
.equ OB_WRP1_nWRP1_Msk, (0xFF << OB_WRP1_nWRP1_Pos) //!< 0xFF000000 
.equ OB_WRP1_nWRP1, OB_WRP1_nWRP1_Msk //!< Flash memory write protection complemented option bytes 
//*****************  Bit definition for FLASH_WRP2 register  *****************
.equ OB_WRP2_WRP2_Pos, (0) 
.equ OB_WRP2_WRP2_Msk, (0xFF << OB_WRP2_WRP2_Pos) //!< 0x000000FF 
.equ OB_WRP2_WRP2, OB_WRP2_WRP2_Msk //!< Flash memory write protection option bytes 
.equ OB_WRP2_nWRP2_Pos, (8) 
.equ OB_WRP2_nWRP2_Msk, (0xFF << OB_WRP2_nWRP2_Pos) //!< 0x0000FF00 
.equ OB_WRP2_nWRP2, OB_WRP2_nWRP2_Msk //!< Flash memory write protection complemented option bytes 
//*****************  Bit definition for FLASH_WRP3 register  *****************
.equ OB_WRP3_WRP3_Pos, (16) 
.equ OB_WRP3_WRP3_Msk, (0xFF << OB_WRP3_WRP3_Pos) //!< 0x00FF0000 
.equ OB_WRP3_WRP3, OB_WRP3_WRP3_Msk //!< Flash memory write protection option bytes 
.equ OB_WRP3_nWRP3_Pos, (24) 
.equ OB_WRP3_nWRP3_Msk, (0xFF << OB_WRP3_nWRP3_Pos) //!< 0xFF000000 
.equ OB_WRP3_nWRP3, OB_WRP3_nWRP3_Msk //!< Flash memory write protection complemented option bytes 
//****************************************************************************
//
//                          Flexible Memory Controller
//
//****************************************************************************
//*****************  Bit definition for FMC_BCRx register  ******************
.equ FMC_BCRx_MBKEN_Pos, (0) 
.equ FMC_BCRx_MBKEN_Msk, (0x1 << FMC_BCRx_MBKEN_Pos) //!< 0x00000001 
.equ FMC_BCRx_MBKEN, FMC_BCRx_MBKEN_Msk //!<Memory bank enable bit 
.equ FMC_BCRx_MUXEN_Pos, (1) 
.equ FMC_BCRx_MUXEN_Msk, (0x1 << FMC_BCRx_MUXEN_Pos) //!< 0x00000002 
.equ FMC_BCRx_MUXEN, FMC_BCRx_MUXEN_Msk //!<Address/data multiplexing enable bit 
.equ FMC_BCRx_MTYP_Pos, (2) 
.equ FMC_BCRx_MTYP_Msk, (0x3 << FMC_BCRx_MTYP_Pos) //!< 0x0000000C 
.equ FMC_BCRx_MTYP, FMC_BCRx_MTYP_Msk //!<MTYP[1:0] bits (Memory type) 
.equ FMC_BCRx_MTYP_0, (0x1 << FMC_BCRx_MTYP_Pos) //!< 0x00000004 
.equ FMC_BCRx_MTYP_1, (0x2 << FMC_BCRx_MTYP_Pos) //!< 0x00000008 
.equ FMC_BCRx_MWID_Pos, (4) 
.equ FMC_BCRx_MWID_Msk, (0x3 << FMC_BCRx_MWID_Pos) //!< 0x00000030 
.equ FMC_BCRx_MWID, FMC_BCRx_MWID_Msk //!<MWID[1:0] bits (Memory data bus width) 
.equ FMC_BCRx_MWID_0, (0x1 << FMC_BCRx_MWID_Pos) //!< 0x00000010 
.equ FMC_BCRx_MWID_1, (0x2 << FMC_BCRx_MWID_Pos) //!< 0x00000020 
.equ FMC_BCRx_FACCEN_Pos, (6) 
.equ FMC_BCRx_FACCEN_Msk, (0x1 << FMC_BCRx_FACCEN_Pos) //!< 0x00000040 
.equ FMC_BCRx_FACCEN, FMC_BCRx_FACCEN_Msk //!<Flash access enable 
.equ FMC_BCRx_BURSTEN_Pos, (8) 
.equ FMC_BCRx_BURSTEN_Msk, (0x1 << FMC_BCRx_BURSTEN_Pos) //!< 0x00000100 
.equ FMC_BCRx_BURSTEN, FMC_BCRx_BURSTEN_Msk //!<Burst enable bit 
.equ FMC_BCRx_WAITPOL_Pos, (9) 
.equ FMC_BCRx_WAITPOL_Msk, (0x1 << FMC_BCRx_WAITPOL_Pos) //!< 0x00000200 
.equ FMC_BCRx_WAITPOL, FMC_BCRx_WAITPOL_Msk //!<Wait signal polarity bit 
.equ FMC_BCRx_WRAPMOD_Pos, (10) 
.equ FMC_BCRx_WRAPMOD_Msk, (0x1 << FMC_BCRx_WRAPMOD_Pos) //!< 0x00000400 
.equ FMC_BCRx_WRAPMOD, FMC_BCRx_WRAPMOD_Msk //!<Wrapped burst mode support 
.equ FMC_BCRx_WAITCFG_Pos, (11) 
.equ FMC_BCRx_WAITCFG_Msk, (0x1 << FMC_BCRx_WAITCFG_Pos) //!< 0x00000800 
.equ FMC_BCRx_WAITCFG, FMC_BCRx_WAITCFG_Msk //!<Wait timing configuration 
.equ FMC_BCRx_WREN_Pos, (12) 
.equ FMC_BCRx_WREN_Msk, (0x1 << FMC_BCRx_WREN_Pos) //!< 0x00001000 
.equ FMC_BCRx_WREN, FMC_BCRx_WREN_Msk //!<Write enable bit 
.equ FMC_BCRx_WAITEN_Pos, (13) 
.equ FMC_BCRx_WAITEN_Msk, (0x1 << FMC_BCRx_WAITEN_Pos) //!< 0x00002000 
.equ FMC_BCRx_WAITEN, FMC_BCRx_WAITEN_Msk //!<Wait enable bit 
.equ FMC_BCRx_EXTMOD_Pos, (14) 
.equ FMC_BCRx_EXTMOD_Msk, (0x1 << FMC_BCRx_EXTMOD_Pos) //!< 0x00004000 
.equ FMC_BCRx_EXTMOD, FMC_BCRx_EXTMOD_Msk //!<Extended mode enable 
.equ FMC_BCRx_ASYNCWAIT_Pos, (15) 
.equ FMC_BCRx_ASYNCWAIT_Msk, (0x1 << FMC_BCRx_ASYNCWAIT_Pos) //!< 0x00008000 
.equ FMC_BCRx_ASYNCWAIT, FMC_BCRx_ASYNCWAIT_Msk //!<Asynchronous wait 
.equ FMC_BCRx_CBURSTRW_Pos, (19) 
.equ FMC_BCRx_CBURSTRW_Msk, (0x1 << FMC_BCRx_CBURSTRW_Pos) //!< 0x00080000 
.equ FMC_BCRx_CBURSTRW, FMC_BCRx_CBURSTRW_Msk //!<Write burst enable 
//*****************  Bit definition for FMC_BCR1 register  ******************
.equ FMC_BCR1_MBKEN_Pos, (0) 
.equ FMC_BCR1_MBKEN_Msk, (0x1 << FMC_BCR1_MBKEN_Pos) //!< 0x00000001 
.equ FMC_BCR1_MBKEN, FMC_BCR1_MBKEN_Msk //!<Memory bank enable bit 
.equ FMC_BCR1_MUXEN_Pos, (1) 
.equ FMC_BCR1_MUXEN_Msk, (0x1 << FMC_BCR1_MUXEN_Pos) //!< 0x00000002 
.equ FMC_BCR1_MUXEN, FMC_BCR1_MUXEN_Msk //!<Address/data multiplexing enable bit 
.equ FMC_BCR1_MTYP_Pos, (2) 
.equ FMC_BCR1_MTYP_Msk, (0x3 << FMC_BCR1_MTYP_Pos) //!< 0x0000000C 
.equ FMC_BCR1_MTYP, FMC_BCR1_MTYP_Msk //!<MTYP[1:0] bits (Memory type) 
.equ FMC_BCR1_MTYP_0, (0x1 << FMC_BCR1_MTYP_Pos) //!< 0x00000004 
.equ FMC_BCR1_MTYP_1, (0x2 << FMC_BCR1_MTYP_Pos) //!< 0x00000008 
.equ FMC_BCR1_MWID_Pos, (4) 
.equ FMC_BCR1_MWID_Msk, (0x3 << FMC_BCR1_MWID_Pos) //!< 0x00000030 
.equ FMC_BCR1_MWID, FMC_BCR1_MWID_Msk //!<MWID[1:0] bits (Memory data bus width) 
.equ FMC_BCR1_MWID_0, (0x1 << FMC_BCR1_MWID_Pos) //!< 0x00000010 
.equ FMC_BCR1_MWID_1, (0x2 << FMC_BCR1_MWID_Pos) //!< 0x00000020 
.equ FMC_BCR1_FACCEN_Pos, (6) 
.equ FMC_BCR1_FACCEN_Msk, (0x1 << FMC_BCR1_FACCEN_Pos) //!< 0x00000040 
.equ FMC_BCR1_FACCEN, FMC_BCR1_FACCEN_Msk //!<Flash access enable 
.equ FMC_BCR1_BURSTEN_Pos, (8) 
.equ FMC_BCR1_BURSTEN_Msk, (0x1 << FMC_BCR1_BURSTEN_Pos) //!< 0x00000100 
.equ FMC_BCR1_BURSTEN, FMC_BCR1_BURSTEN_Msk //!<Burst enable bit 
.equ FMC_BCR1_WAITPOL_Pos, (9) 
.equ FMC_BCR1_WAITPOL_Msk, (0x1 << FMC_BCR1_WAITPOL_Pos) //!< 0x00000200 
.equ FMC_BCR1_WAITPOL, FMC_BCR1_WAITPOL_Msk //!<Wait signal polarity bit 
.equ FMC_BCR1_WRAPMOD_Pos, (10) 
.equ FMC_BCR1_WRAPMOD_Msk, (0x1 << FMC_BCR1_WRAPMOD_Pos) //!< 0x00000400 
.equ FMC_BCR1_WRAPMOD, FMC_BCR1_WRAPMOD_Msk //!<Wrapped burst mode support 
.equ FMC_BCR1_WAITCFG_Pos, (11) 
.equ FMC_BCR1_WAITCFG_Msk, (0x1 << FMC_BCR1_WAITCFG_Pos) //!< 0x00000800 
.equ FMC_BCR1_WAITCFG, FMC_BCR1_WAITCFG_Msk //!<Wait timing configuration 
.equ FMC_BCR1_WREN_Pos, (12) 
.equ FMC_BCR1_WREN_Msk, (0x1 << FMC_BCR1_WREN_Pos) //!< 0x00001000 
.equ FMC_BCR1_WREN, FMC_BCR1_WREN_Msk //!<Write enable bit 
.equ FMC_BCR1_WAITEN_Pos, (13) 
.equ FMC_BCR1_WAITEN_Msk, (0x1 << FMC_BCR1_WAITEN_Pos) //!< 0x00002000 
.equ FMC_BCR1_WAITEN, FMC_BCR1_WAITEN_Msk //!<Wait enable bit 
.equ FMC_BCR1_EXTMOD_Pos, (14) 
.equ FMC_BCR1_EXTMOD_Msk, (0x1 << FMC_BCR1_EXTMOD_Pos) //!< 0x00004000 
.equ FMC_BCR1_EXTMOD, FMC_BCR1_EXTMOD_Msk //!<Extended mode enable 
.equ FMC_BCR1_ASYNCWAIT_Pos, (15) 
.equ FMC_BCR1_ASYNCWAIT_Msk, (0x1 << FMC_BCR1_ASYNCWAIT_Pos) //!< 0x00008000 
.equ FMC_BCR1_ASYNCWAIT, FMC_BCR1_ASYNCWAIT_Msk //!<Asynchronous wait 
.equ FMC_BCR1_CBURSTRW_Pos, (19) 
.equ FMC_BCR1_CBURSTRW_Msk, (0x1 << FMC_BCR1_CBURSTRW_Pos) //!< 0x00080000 
.equ FMC_BCR1_CBURSTRW, FMC_BCR1_CBURSTRW_Msk //!<Write burst enable 
.equ FMC_BCR1_CCLKEN_Pos, (20) 
.equ FMC_BCR1_CCLKEN_Msk, (0x1 << FMC_BCR1_CCLKEN_Pos) //!< 0x00100000 
.equ FMC_BCR1_CCLKEN, FMC_BCR1_CCLKEN_Msk //!<Continous clock enable 
//*****************  Bit definition for FMC_BCR2 register  ******************
.equ FMC_BCR2_MBKEN_Pos, (0) 
.equ FMC_BCR2_MBKEN_Msk, (0x1 << FMC_BCR2_MBKEN_Pos) //!< 0x00000001 
.equ FMC_BCR2_MBKEN, FMC_BCR2_MBKEN_Msk //!<Memory bank enable bit 
.equ FMC_BCR2_MUXEN_Pos, (1) 
.equ FMC_BCR2_MUXEN_Msk, (0x1 << FMC_BCR2_MUXEN_Pos) //!< 0x00000002 
.equ FMC_BCR2_MUXEN, FMC_BCR2_MUXEN_Msk //!<Address/data multiplexing enable bit 
.equ FMC_BCR2_MTYP_Pos, (2) 
.equ FMC_BCR2_MTYP_Msk, (0x3 << FMC_BCR2_MTYP_Pos) //!< 0x0000000C 
.equ FMC_BCR2_MTYP, FMC_BCR2_MTYP_Msk //!<MTYP[1:0] bits (Memory type) 
.equ FMC_BCR2_MTYP_0, (0x1 << FMC_BCR2_MTYP_Pos) //!< 0x00000004 
.equ FMC_BCR2_MTYP_1, (0x2 << FMC_BCR2_MTYP_Pos) //!< 0x00000008 
.equ FMC_BCR2_MWID_Pos, (4) 
.equ FMC_BCR2_MWID_Msk, (0x3 << FMC_BCR2_MWID_Pos) //!< 0x00000030 
.equ FMC_BCR2_MWID, FMC_BCR2_MWID_Msk //!<MWID[1:0] bits (Memory data bus width) 
.equ FMC_BCR2_MWID_0, (0x1 << FMC_BCR2_MWID_Pos) //!< 0x00000010 
.equ FMC_BCR2_MWID_1, (0x2 << FMC_BCR2_MWID_Pos) //!< 0x00000020 
.equ FMC_BCR2_FACCEN_Pos, (6) 
.equ FMC_BCR2_FACCEN_Msk, (0x1 << FMC_BCR2_FACCEN_Pos) //!< 0x00000040 
.equ FMC_BCR2_FACCEN, FMC_BCR2_FACCEN_Msk //!<Flash access enable 
.equ FMC_BCR2_BURSTEN_Pos, (8) 
.equ FMC_BCR2_BURSTEN_Msk, (0x1 << FMC_BCR2_BURSTEN_Pos) //!< 0x00000100 
.equ FMC_BCR2_BURSTEN, FMC_BCR2_BURSTEN_Msk //!<Burst enable bit 
.equ FMC_BCR2_WAITPOL_Pos, (9) 
.equ FMC_BCR2_WAITPOL_Msk, (0x1 << FMC_BCR2_WAITPOL_Pos) //!< 0x00000200 
.equ FMC_BCR2_WAITPOL, FMC_BCR2_WAITPOL_Msk //!<Wait signal polarity bit 
.equ FMC_BCR2_WRAPMOD_Pos, (10) 
.equ FMC_BCR2_WRAPMOD_Msk, (0x1 << FMC_BCR2_WRAPMOD_Pos) //!< 0x00000400 
.equ FMC_BCR2_WRAPMOD, FMC_BCR2_WRAPMOD_Msk //!<Wrapped burst mode support 
.equ FMC_BCR2_WAITCFG_Pos, (11) 
.equ FMC_BCR2_WAITCFG_Msk, (0x1 << FMC_BCR2_WAITCFG_Pos) //!< 0x00000800 
.equ FMC_BCR2_WAITCFG, FMC_BCR2_WAITCFG_Msk //!<Wait timing configuration 
.equ FMC_BCR2_WREN_Pos, (12) 
.equ FMC_BCR2_WREN_Msk, (0x1 << FMC_BCR2_WREN_Pos) //!< 0x00001000 
.equ FMC_BCR2_WREN, FMC_BCR2_WREN_Msk //!<Write enable bit 
.equ FMC_BCR2_WAITEN_Pos, (13) 
.equ FMC_BCR2_WAITEN_Msk, (0x1 << FMC_BCR2_WAITEN_Pos) //!< 0x00002000 
.equ FMC_BCR2_WAITEN, FMC_BCR2_WAITEN_Msk //!<Wait enable bit 
.equ FMC_BCR2_EXTMOD_Pos, (14) 
.equ FMC_BCR2_EXTMOD_Msk, (0x1 << FMC_BCR2_EXTMOD_Pos) //!< 0x00004000 
.equ FMC_BCR2_EXTMOD, FMC_BCR2_EXTMOD_Msk //!<Extended mode enable 
.equ FMC_BCR2_ASYNCWAIT_Pos, (15) 
.equ FMC_BCR2_ASYNCWAIT_Msk, (0x1 << FMC_BCR2_ASYNCWAIT_Pos) //!< 0x00008000 
.equ FMC_BCR2_ASYNCWAIT, FMC_BCR2_ASYNCWAIT_Msk //!<Asynchronous wait 
.equ FMC_BCR2_CBURSTRW_Pos, (19) 
.equ FMC_BCR2_CBURSTRW_Msk, (0x1 << FMC_BCR2_CBURSTRW_Pos) //!< 0x00080000 
.equ FMC_BCR2_CBURSTRW, FMC_BCR2_CBURSTRW_Msk //!<Write burst enable 
//*****************  Bit definition for FMC_BCR3 register  ******************
.equ FMC_BCR3_MBKEN_Pos, (0) 
.equ FMC_BCR3_MBKEN_Msk, (0x1 << FMC_BCR3_MBKEN_Pos) //!< 0x00000001 
.equ FMC_BCR3_MBKEN, FMC_BCR3_MBKEN_Msk //!<Memory bank enable bit 
.equ FMC_BCR3_MUXEN_Pos, (1) 
.equ FMC_BCR3_MUXEN_Msk, (0x1 << FMC_BCR3_MUXEN_Pos) //!< 0x00000002 
.equ FMC_BCR3_MUXEN, FMC_BCR3_MUXEN_Msk //!<Address/data multiplexing enable bit 
.equ FMC_BCR3_MTYP_Pos, (2) 
.equ FMC_BCR3_MTYP_Msk, (0x3 << FMC_BCR3_MTYP_Pos) //!< 0x0000000C 
.equ FMC_BCR3_MTYP, FMC_BCR3_MTYP_Msk //!<MTYP[1:0] bits (Memory type) 
.equ FMC_BCR3_MTYP_0, (0x1 << FMC_BCR3_MTYP_Pos) //!< 0x00000004 
.equ FMC_BCR3_MTYP_1, (0x2 << FMC_BCR3_MTYP_Pos) //!< 0x00000008 
.equ FMC_BCR3_MWID_Pos, (4) 
.equ FMC_BCR3_MWID_Msk, (0x3 << FMC_BCR3_MWID_Pos) //!< 0x00000030 
.equ FMC_BCR3_MWID, FMC_BCR3_MWID_Msk //!<MWID[1:0] bits (Memory data bus width) 
.equ FMC_BCR3_MWID_0, (0x1 << FMC_BCR3_MWID_Pos) //!< 0x00000010 
.equ FMC_BCR3_MWID_1, (0x2 << FMC_BCR3_MWID_Pos) //!< 0x00000020 
.equ FMC_BCR3_FACCEN_Pos, (6) 
.equ FMC_BCR3_FACCEN_Msk, (0x1 << FMC_BCR3_FACCEN_Pos) //!< 0x00000040 
.equ FMC_BCR3_FACCEN, FMC_BCR3_FACCEN_Msk //!<Flash access enable 
.equ FMC_BCR3_BURSTEN_Pos, (8) 
.equ FMC_BCR3_BURSTEN_Msk, (0x1 << FMC_BCR3_BURSTEN_Pos) //!< 0x00000100 
.equ FMC_BCR3_BURSTEN, FMC_BCR3_BURSTEN_Msk //!<Burst enable bit 
.equ FMC_BCR3_WAITPOL_Pos, (9) 
.equ FMC_BCR3_WAITPOL_Msk, (0x1 << FMC_BCR3_WAITPOL_Pos) //!< 0x00000200 
.equ FMC_BCR3_WAITPOL, FMC_BCR3_WAITPOL_Msk //!<Wait signal polarity bit 
.equ FMC_BCR3_WRAPMOD_Pos, (10) 
.equ FMC_BCR3_WRAPMOD_Msk, (0x1 << FMC_BCR3_WRAPMOD_Pos) //!< 0x00000400 
.equ FMC_BCR3_WRAPMOD, FMC_BCR3_WRAPMOD_Msk //!<Wrapped burst mode support 
.equ FMC_BCR3_WAITCFG_Pos, (11) 
.equ FMC_BCR3_WAITCFG_Msk, (0x1 << FMC_BCR3_WAITCFG_Pos) //!< 0x00000800 
.equ FMC_BCR3_WAITCFG, FMC_BCR3_WAITCFG_Msk //!<Wait timing configuration 
.equ FMC_BCR3_WREN_Pos, (12) 
.equ FMC_BCR3_WREN_Msk, (0x1 << FMC_BCR3_WREN_Pos) //!< 0x00001000 
.equ FMC_BCR3_WREN, FMC_BCR3_WREN_Msk //!<Write enable bit 
.equ FMC_BCR3_WAITEN_Pos, (13) 
.equ FMC_BCR3_WAITEN_Msk, (0x1 << FMC_BCR3_WAITEN_Pos) //!< 0x00002000 
.equ FMC_BCR3_WAITEN, FMC_BCR3_WAITEN_Msk //!<Wait enable bit 
.equ FMC_BCR3_EXTMOD_Pos, (14) 
.equ FMC_BCR3_EXTMOD_Msk, (0x1 << FMC_BCR3_EXTMOD_Pos) //!< 0x00004000 
.equ FMC_BCR3_EXTMOD, FMC_BCR3_EXTMOD_Msk //!<Extended mode enable 
.equ FMC_BCR3_ASYNCWAIT_Pos, (15) 
.equ FMC_BCR3_ASYNCWAIT_Msk, (0x1 << FMC_BCR3_ASYNCWAIT_Pos) //!< 0x00008000 
.equ FMC_BCR3_ASYNCWAIT, FMC_BCR3_ASYNCWAIT_Msk //!<Asynchronous wait 
.equ FMC_BCR3_CBURSTRW_Pos, (19) 
.equ FMC_BCR3_CBURSTRW_Msk, (0x1 << FMC_BCR3_CBURSTRW_Pos) //!< 0x00080000 
.equ FMC_BCR3_CBURSTRW, FMC_BCR3_CBURSTRW_Msk //!<Write burst enable 
//*****************  Bit definition for FMC_BCR4 register  ******************
.equ FMC_BCR4_MBKEN_Pos, (0) 
.equ FMC_BCR4_MBKEN_Msk, (0x1 << FMC_BCR4_MBKEN_Pos) //!< 0x00000001 
.equ FMC_BCR4_MBKEN, FMC_BCR4_MBKEN_Msk //!<Memory bank enable bit 
.equ FMC_BCR4_MUXEN_Pos, (1) 
.equ FMC_BCR4_MUXEN_Msk, (0x1 << FMC_BCR4_MUXEN_Pos) //!< 0x00000002 
.equ FMC_BCR4_MUXEN, FMC_BCR4_MUXEN_Msk //!<Address/data multiplexing enable bit 
.equ FMC_BCR4_MTYP_Pos, (2) 
.equ FMC_BCR4_MTYP_Msk, (0x3 << FMC_BCR4_MTYP_Pos) //!< 0x0000000C 
.equ FMC_BCR4_MTYP, FMC_BCR4_MTYP_Msk //!<MTYP[1:0] bits (Memory type) 
.equ FMC_BCR4_MTYP_0, (0x1 << FMC_BCR4_MTYP_Pos) //!< 0x00000004 
.equ FMC_BCR4_MTYP_1, (0x2 << FMC_BCR4_MTYP_Pos) //!< 0x00000008 
.equ FMC_BCR4_MWID_Pos, (4) 
.equ FMC_BCR4_MWID_Msk, (0x3 << FMC_BCR4_MWID_Pos) //!< 0x00000030 
.equ FMC_BCR4_MWID, FMC_BCR4_MWID_Msk //!<MWID[1:0] bits (Memory data bus width) 
.equ FMC_BCR4_MWID_0, (0x1 << FMC_BCR4_MWID_Pos) //!< 0x00000010 
.equ FMC_BCR4_MWID_1, (0x2 << FMC_BCR4_MWID_Pos) //!< 0x00000020 
.equ FMC_BCR4_FACCEN_Pos, (6) 
.equ FMC_BCR4_FACCEN_Msk, (0x1 << FMC_BCR4_FACCEN_Pos) //!< 0x00000040 
.equ FMC_BCR4_FACCEN, FMC_BCR4_FACCEN_Msk //!<Flash access enable 
.equ FMC_BCR4_BURSTEN_Pos, (8) 
.equ FMC_BCR4_BURSTEN_Msk, (0x1 << FMC_BCR4_BURSTEN_Pos) //!< 0x00000100 
.equ FMC_BCR4_BURSTEN, FMC_BCR4_BURSTEN_Msk //!<Burst enable bit 
.equ FMC_BCR4_WAITPOL_Pos, (9) 
.equ FMC_BCR4_WAITPOL_Msk, (0x1 << FMC_BCR4_WAITPOL_Pos) //!< 0x00000200 
.equ FMC_BCR4_WAITPOL, FMC_BCR4_WAITPOL_Msk //!<Wait signal polarity bit 
.equ FMC_BCR4_WRAPMOD_Pos, (10) 
.equ FMC_BCR4_WRAPMOD_Msk, (0x1 << FMC_BCR4_WRAPMOD_Pos) //!< 0x00000400 
.equ FMC_BCR4_WRAPMOD, FMC_BCR4_WRAPMOD_Msk //!<Wrapped burst mode support 
.equ FMC_BCR4_WAITCFG_Pos, (11) 
.equ FMC_BCR4_WAITCFG_Msk, (0x1 << FMC_BCR4_WAITCFG_Pos) //!< 0x00000800 
.equ FMC_BCR4_WAITCFG, FMC_BCR4_WAITCFG_Msk //!<Wait timing configuration 
.equ FMC_BCR4_WREN_Pos, (12) 
.equ FMC_BCR4_WREN_Msk, (0x1 << FMC_BCR4_WREN_Pos) //!< 0x00001000 
.equ FMC_BCR4_WREN, FMC_BCR4_WREN_Msk //!<Write enable bit 
.equ FMC_BCR4_WAITEN_Pos, (13) 
.equ FMC_BCR4_WAITEN_Msk, (0x1 << FMC_BCR4_WAITEN_Pos) //!< 0x00002000 
.equ FMC_BCR4_WAITEN, FMC_BCR4_WAITEN_Msk //!<Wait enable bit 
.equ FMC_BCR4_EXTMOD_Pos, (14) 
.equ FMC_BCR4_EXTMOD_Msk, (0x1 << FMC_BCR4_EXTMOD_Pos) //!< 0x00004000 
.equ FMC_BCR4_EXTMOD, FMC_BCR4_EXTMOD_Msk //!<Extended mode enable 
.equ FMC_BCR4_ASYNCWAIT_Pos, (15) 
.equ FMC_BCR4_ASYNCWAIT_Msk, (0x1 << FMC_BCR4_ASYNCWAIT_Pos) //!< 0x00008000 
.equ FMC_BCR4_ASYNCWAIT, FMC_BCR4_ASYNCWAIT_Msk //!<Asynchronous wait 
.equ FMC_BCR4_CBURSTRW_Pos, (19) 
.equ FMC_BCR4_CBURSTRW_Msk, (0x1 << FMC_BCR4_CBURSTRW_Pos) //!< 0x00080000 
.equ FMC_BCR4_CBURSTRW, FMC_BCR4_CBURSTRW_Msk //!<Write burst enable 
//*****************  Bit definition for FMC_BTRx register  *****************
.equ FMC_BTRx_ADDSET_Pos, (0) 
.equ FMC_BTRx_ADDSET_Msk, (0xF << FMC_BTRx_ADDSET_Pos) //!< 0x0000000F 
.equ FMC_BTRx_ADDSET, FMC_BTRx_ADDSET_Msk //!<ADDSET[3:0] bits (Address setup phase duration) 
.equ FMC_BTRx_ADDSET_0, (0x1 << FMC_BTRx_ADDSET_Pos) //!< 0x00000001 
.equ FMC_BTRx_ADDSET_1, (0x2 << FMC_BTRx_ADDSET_Pos) //!< 0x00000002 
.equ FMC_BTRx_ADDSET_2, (0x4 << FMC_BTRx_ADDSET_Pos) //!< 0x00000004 
.equ FMC_BTR_ADDSET_3, (0x00000008) //!<Bit 3 
.equ FMC_BTRx_ADDHLD_Pos, (4) 
.equ FMC_BTRx_ADDHLD_Msk, (0xF << FMC_BTRx_ADDHLD_Pos) //!< 0x000000F0 
.equ FMC_BTRx_ADDHLD, FMC_BTRx_ADDHLD_Msk //!<ADDHLD[3:0] bits (Address-hold phase duration) 
.equ FMC_BTRx_ADDHLD_0, (0x1 << FMC_BTRx_ADDHLD_Pos) //!< 0x00000010 
.equ FMC_BTRx_ADDHLD_1, (0x2 << FMC_BTRx_ADDHLD_Pos) //!< 0x00000020 
.equ FMC_BTRx_ADDHLD_2, (0x4 << FMC_BTRx_ADDHLD_Pos) //!< 0x00000040 
.equ FMC_BTRx_ADDHLD_3, (0x8 << FMC_BTRx_ADDHLD_Pos) //!< 0x00000080 
.equ FMC_BTRx_DATAST_Pos, (8) 
.equ FMC_BTRx_DATAST_Msk, (0xFF << FMC_BTRx_DATAST_Pos) //!< 0x0000FF00 
.equ FMC_BTRx_DATAST, FMC_BTRx_DATAST_Msk //!<DATAST [3:0] bits (Data-phase duration) 
.equ FMC_BTR_DATAST_0, (0x00000100) //!<Bit 0 
.equ FMC_BTRx_DATAST_1, (0x00000200) //!<Bit 1 
.equ FMC_BTRx_DATAST_2, (0x00000400) //!<Bit 2 
.equ FMC_BTRx_DATAST_3, (0x00000800) //!<Bit 3 
.equ FMC_BTRx_DATAST_4, (0x00001000) //!<Bit 4 
.equ FMC_BTRx_DATAST_5, (0x00002000) //!<Bit 5 
.equ FMC_BTRx_DATAST_6, (0x00004000) //!<Bit 6 
.equ FMC_BTRx_DATAST_7, (0x00008000) //!<Bit 7 
.equ FMC_BTRx_BUSTURN_Pos, (16) 
.equ FMC_BTRx_BUSTURN_Msk, (0xF << FMC_BTRx_BUSTURN_Pos) //!< 0x000F0000 
.equ FMC_BTRx_BUSTURN, FMC_BTRx_BUSTURN_Msk //!<BUSTURN[3:0] bits (Bus turnaround phase duration) 
.equ FMC_BTRx_BUSTURN_0, (0x1 << FMC_BTRx_BUSTURN_Pos) //!< 0x00010000 
.equ FMC_BTRx_BUSTURN_1, (0x2 << FMC_BTRx_BUSTURN_Pos) //!< 0x00020000 
.equ FMC_BTRx_BUSTURN_2, (0x4 << FMC_BTRx_BUSTURN_Pos) //!< 0x00040000 
.equ FMC_BTRx_BUSTURN_3, (0x8 << FMC_BTRx_BUSTURN_Pos) //!< 0x00080000 
.equ FMC_BTRx_CLKDIV_Pos, (20) 
.equ FMC_BTRx_CLKDIV_Msk, (0xF << FMC_BTRx_CLKDIV_Pos) //!< 0x00F00000 
.equ FMC_BTRx_CLKDIV, FMC_BTRx_CLKDIV_Msk //!<CLKDIV[3:0] bits (Clock divide ratio) 
.equ FMC_BTRx_CLKDIV_0, (0x1 << FMC_BTRx_CLKDIV_Pos) //!< 0x00100000 
.equ FMC_BTRx_CLKDIV_1, (0x2 << FMC_BTRx_CLKDIV_Pos) //!< 0x00200000 
.equ FMC_BTRx_CLKDIV_2, (0x4 << FMC_BTRx_CLKDIV_Pos) //!< 0x00400000 
.equ FMC_BTRx_CLKDIV_3, (0x8 << FMC_BTRx_CLKDIV_Pos) //!< 0x00800000 
.equ FMC_BTRx_DATLAT_Pos, (24) 
.equ FMC_BTRx_DATLAT_Msk, (0xF << FMC_BTRx_DATLAT_Pos) //!< 0x0F000000 
.equ FMC_BTRx_DATLAT, FMC_BTRx_DATLAT_Msk //!<DATLA[3:0] bits (Data latency) 
.equ FMC_BTRx_DATLAT_0, (0x1 << FMC_BTRx_DATLAT_Pos) //!< 0x01000000 
.equ FMC_BTRx_DATLAT_1, (0x2 << FMC_BTRx_DATLAT_Pos) //!< 0x02000000 
.equ FMC_BTRx_DATLAT_2, (0x4 << FMC_BTRx_DATLAT_Pos) //!< 0x04000000 
.equ FMC_BTRx_DATLAT_3, (0x8 << FMC_BTRx_DATLAT_Pos) //!< 0x08000000 
.equ FMC_BTRx_ACCMOD_Pos, (28) 
.equ FMC_BTRx_ACCMOD_Msk, (0x3 << FMC_BTRx_ACCMOD_Pos) //!< 0x30000000 
.equ FMC_BTRx_ACCMOD, FMC_BTRx_ACCMOD_Msk //!<ACCMOD[1:0] bits (Access mode) 
.equ FMC_BTRx_ACCMOD_0, (0x1 << FMC_BTRx_ACCMOD_Pos) //!< 0x10000000 
.equ FMC_BTRx_ACCMOD_1, (0x2 << FMC_BTRx_ACCMOD_Pos) //!< 0x20000000 
//*****************  Bit definition for FMC_BTR1 register  *****************
.equ FMC_BTR1_ADDSET_Pos, (0) 
.equ FMC_BTR1_ADDSET_Msk, (0xF << FMC_BTR1_ADDSET_Pos) //!< 0x0000000F 
.equ FMC_BTR1_ADDSET, FMC_BTR1_ADDSET_Msk //!<ADDSET[3:0] bits (Address setup phase duration) 
.equ FMC_BTR1_ADDSET_0, (0x1 << FMC_BTR1_ADDSET_Pos) //!< 0x00000001 
.equ FMC_BTR1_ADDSET_1, (0x2 << FMC_BTR1_ADDSET_Pos) //!< 0x00000002 
.equ FMC_BTR1_ADDSET_2, (0x4 << FMC_BTR1_ADDSET_Pos) //!< 0x00000004 
.equ FMC_BTR1_ADDSET_3, (0x8 << FMC_BTR1_ADDSET_Pos) //!< 0x00000008 
.equ FMC_BTR1_ADDHLD_Pos, (4) 
.equ FMC_BTR1_ADDHLD_Msk, (0xF << FMC_BTR1_ADDHLD_Pos) //!< 0x000000F0 
.equ FMC_BTR1_ADDHLD, FMC_BTR1_ADDHLD_Msk //!<ADDHLD[3:0] bits (Address-hold phase duration) 
.equ FMC_BTR1_ADDHLD_0, (0x1 << FMC_BTR1_ADDHLD_Pos) //!< 0x00000010 
.equ FMC_BTR1_ADDHLD_1, (0x2 << FMC_BTR1_ADDHLD_Pos) //!< 0x00000020 
.equ FMC_BTR1_ADDHLD_2, (0x4 << FMC_BTR1_ADDHLD_Pos) //!< 0x00000040 
.equ FMC_BTR1_ADDHLD_3, (0x8 << FMC_BTR1_ADDHLD_Pos) //!< 0x00000080 
.equ FMC_BTR1_DATAST_Pos, (8) 
.equ FMC_BTR1_DATAST_Msk, (0xFF << FMC_BTR1_DATAST_Pos) //!< 0x0000FF00 
.equ FMC_BTR1_DATAST, FMC_BTR1_DATAST_Msk //!<DATAST [3:0] bits (Data-phase duration) 
.equ FMC_BTR1_DATAST_0, (0x01 << FMC_BTR1_DATAST_Pos) //!< 0x00000100 
.equ FMC_BTR1_DATAST_1, (0x02 << FMC_BTR1_DATAST_Pos) //!< 0x00000200 
.equ FMC_BTR1_DATAST_2, (0x04 << FMC_BTR1_DATAST_Pos) //!< 0x00000400 
.equ FMC_BTR1_DATAST_3, (0x08 << FMC_BTR1_DATAST_Pos) //!< 0x00000800 
.equ FMC_BTR1_DATAST_4, (0x10 << FMC_BTR1_DATAST_Pos) //!< 0x00001000 
.equ FMC_BTR1_DATAST_5, (0x20 << FMC_BTR1_DATAST_Pos) //!< 0x00002000 
.equ FMC_BTR1_DATAST_6, (0x40 << FMC_BTR1_DATAST_Pos) //!< 0x00004000 
.equ FMC_BTR1_DATAST_7, (0x80 << FMC_BTR1_DATAST_Pos) //!< 0x00008000 
.equ FMC_BTR1_BUSTURN_Pos, (16) 
.equ FMC_BTR1_BUSTURN_Msk, (0xF << FMC_BTR1_BUSTURN_Pos) //!< 0x000F0000 
.equ FMC_BTR1_BUSTURN, FMC_BTR1_BUSTURN_Msk //!<BUSTURN[3:0] bits (Bus turnaround phase duration) 
.equ FMC_BTR1_BUSTURN_0, (0x1 << FMC_BTR1_BUSTURN_Pos) //!< 0x00010000 
.equ FMC_BTR1_BUSTURN_1, (0x2 << FMC_BTR1_BUSTURN_Pos) //!< 0x00020000 
.equ FMC_BTR1_BUSTURN_2, (0x4 << FMC_BTR1_BUSTURN_Pos) //!< 0x00040000 
.equ FMC_BTR1_BUSTURN_3, (0x8 << FMC_BTR1_BUSTURN_Pos) //!< 0x00080000 
.equ FMC_BTR1_CLKDIV_Pos, (20) 
.equ FMC_BTR1_CLKDIV_Msk, (0xF << FMC_BTR1_CLKDIV_Pos) //!< 0x00F00000 
.equ FMC_BTR1_CLKDIV, FMC_BTR1_CLKDIV_Msk //!<CLKDIV[3:0] bits (Clock divide ratio) 
.equ FMC_BTR1_CLKDIV_0, (0x1 << FMC_BTR1_CLKDIV_Pos) //!< 0x00100000 
.equ FMC_BTR1_CLKDIV_1, (0x2 << FMC_BTR1_CLKDIV_Pos) //!< 0x00200000 
.equ FMC_BTR1_CLKDIV_2, (0x4 << FMC_BTR1_CLKDIV_Pos) //!< 0x00400000 
.equ FMC_BTR1_CLKDIV_3, (0x8 << FMC_BTR1_CLKDIV_Pos) //!< 0x00800000 
.equ FMC_BTR1_DATLAT_Pos, (24) 
.equ FMC_BTR1_DATLAT_Msk, (0xF << FMC_BTR1_DATLAT_Pos) //!< 0x0F000000 
.equ FMC_BTR1_DATLAT, FMC_BTR1_DATLAT_Msk //!<DATLA[3:0] bits (Data latency) 
.equ FMC_BTR1_DATLAT_0, (0x1 << FMC_BTR1_DATLAT_Pos) //!< 0x01000000 
.equ FMC_BTR1_DATLAT_1, (0x2 << FMC_BTR1_DATLAT_Pos) //!< 0x02000000 
.equ FMC_BTR1_DATLAT_2, (0x4 << FMC_BTR1_DATLAT_Pos) //!< 0x04000000 
.equ FMC_BTR1_DATLAT_3, (0x8 << FMC_BTR1_DATLAT_Pos) //!< 0x08000000 
.equ FMC_BTR1_ACCMOD_Pos, (28) 
.equ FMC_BTR1_ACCMOD_Msk, (0x3 << FMC_BTR1_ACCMOD_Pos) //!< 0x30000000 
.equ FMC_BTR1_ACCMOD, FMC_BTR1_ACCMOD_Msk //!<ACCMOD[1:0] bits (Access mode) 
.equ FMC_BTR1_ACCMOD_0, (0x1 << FMC_BTR1_ACCMOD_Pos) //!< 0x10000000 
.equ FMC_BTR1_ACCMOD_1, (0x2 << FMC_BTR1_ACCMOD_Pos) //!< 0x20000000 
//*****************  Bit definition for FMC_BTR2 register  ******************
.equ FMC_BTR2_ADDSET_Pos, (0) 
.equ FMC_BTR2_ADDSET_Msk, (0xF << FMC_BTR2_ADDSET_Pos) //!< 0x0000000F 
.equ FMC_BTR2_ADDSET, FMC_BTR2_ADDSET_Msk //!<ADDSET[3:0] bits (Address setup phase duration) 
.equ FMC_BTR2_ADDSET_0, (0x1 << FMC_BTR2_ADDSET_Pos) //!< 0x00000001 
.equ FMC_BTR2_ADDSET_1, (0x2 << FMC_BTR2_ADDSET_Pos) //!< 0x00000002 
.equ FMC_BTR2_ADDSET_2, (0x4 << FMC_BTR2_ADDSET_Pos) //!< 0x00000004 
.equ FMC_BTR2_ADDSET_3, (0x8 << FMC_BTR2_ADDSET_Pos) //!< 0x00000008 
.equ FMC_BTR2_ADDHLD_Pos, (4) 
.equ FMC_BTR2_ADDHLD_Msk, (0xF << FMC_BTR2_ADDHLD_Pos) //!< 0x000000F0 
.equ FMC_BTR2_ADDHLD, FMC_BTR2_ADDHLD_Msk //!<ADDHLD[3:0] bits (Address-hold phase duration) 
.equ FMC_BTR2_ADDHLD_0, (0x1 << FMC_BTR2_ADDHLD_Pos) //!< 0x00000010 
.equ FMC_BTR2_ADDHLD_1, (0x2 << FMC_BTR2_ADDHLD_Pos) //!< 0x00000020 
.equ FMC_BTR2_ADDHLD_2, (0x4 << FMC_BTR2_ADDHLD_Pos) //!< 0x00000040 
.equ FMC_BTR2_ADDHLD_3, (0x8 << FMC_BTR2_ADDHLD_Pos) //!< 0x00000080 
.equ FMC_BTR2_DATAST_Pos, (8) 
.equ FMC_BTR2_DATAST_Msk, (0xFF << FMC_BTR2_DATAST_Pos) //!< 0x0000FF00 
.equ FMC_BTR2_DATAST, FMC_BTR2_DATAST_Msk //!<DATAST [3:0] bits (Data-phase duration) 
.equ FMC_BTR2_DATAST_0, (0x01 << FMC_BTR2_DATAST_Pos) //!< 0x00000100 
.equ FMC_BTR2_DATAST_1, (0x02 << FMC_BTR2_DATAST_Pos) //!< 0x00000200 
.equ FMC_BTR2_DATAST_2, (0x04 << FMC_BTR2_DATAST_Pos) //!< 0x00000400 
.equ FMC_BTR2_DATAST_3, (0x08 << FMC_BTR2_DATAST_Pos) //!< 0x00000800 
.equ FMC_BTR2_DATAST_4, (0x10 << FMC_BTR2_DATAST_Pos) //!< 0x00001000 
.equ FMC_BTR2_DATAST_5, (0x20 << FMC_BTR2_DATAST_Pos) //!< 0x00002000 
.equ FMC_BTR2_DATAST_6, (0x40 << FMC_BTR2_DATAST_Pos) //!< 0x00004000 
.equ FMC_BTR2_DATAST_7, (0x80 << FMC_BTR2_DATAST_Pos) //!< 0x00008000 
.equ FMC_BTR2_BUSTURN_Pos, (16) 
.equ FMC_BTR2_BUSTURN_Msk, (0xF << FMC_BTR2_BUSTURN_Pos) //!< 0x000F0000 
.equ FMC_BTR2_BUSTURN, FMC_BTR2_BUSTURN_Msk //!<BUSTURN[3:0] bits (Bus turnaround phase duration) 
.equ FMC_BTR2_BUSTURN_0, (0x1 << FMC_BTR2_BUSTURN_Pos) //!< 0x00010000 
.equ FMC_BTR2_BUSTURN_1, (0x2 << FMC_BTR2_BUSTURN_Pos) //!< 0x00020000 
.equ FMC_BTR2_BUSTURN_2, (0x4 << FMC_BTR2_BUSTURN_Pos) //!< 0x00040000 
.equ FMC_BTR2_BUSTURN_3, (0x8 << FMC_BTR2_BUSTURN_Pos) //!< 0x00080000 
.equ FMC_BTR2_CLKDIV_Pos, (20) 
.equ FMC_BTR2_CLKDIV_Msk, (0xF << FMC_BTR2_CLKDIV_Pos) //!< 0x00F00000 
.equ FMC_BTR2_CLKDIV, FMC_BTR2_CLKDIV_Msk //!<CLKDIV[3:0] bits (Clock divide ratio) 
.equ FMC_BTR2_CLKDIV_0, (0x1 << FMC_BTR2_CLKDIV_Pos) //!< 0x00100000 
.equ FMC_BTR2_CLKDIV_1, (0x2 << FMC_BTR2_CLKDIV_Pos) //!< 0x00200000 
.equ FMC_BTR2_CLKDIV_2, (0x4 << FMC_BTR2_CLKDIV_Pos) //!< 0x00400000 
.equ FMC_BTR2_CLKDIV_3, (0x8 << FMC_BTR2_CLKDIV_Pos) //!< 0x00800000 
.equ FMC_BTR2_DATLAT_Pos, (24) 
.equ FMC_BTR2_DATLAT_Msk, (0xF << FMC_BTR2_DATLAT_Pos) //!< 0x0F000000 
.equ FMC_BTR2_DATLAT, FMC_BTR2_DATLAT_Msk //!<DATLA[3:0] bits (Data latency) 
.equ FMC_BTR2_DATLAT_0, (0x1 << FMC_BTR2_DATLAT_Pos) //!< 0x01000000 
.equ FMC_BTR2_DATLAT_1, (0x2 << FMC_BTR2_DATLAT_Pos) //!< 0x02000000 
.equ FMC_BTR2_DATLAT_2, (0x4 << FMC_BTR2_DATLAT_Pos) //!< 0x04000000 
.equ FMC_BTR2_DATLAT_3, (0x8 << FMC_BTR2_DATLAT_Pos) //!< 0x08000000 
.equ FMC_BTR2_ACCMOD_Pos, (28) 
.equ FMC_BTR2_ACCMOD_Msk, (0x3 << FMC_BTR2_ACCMOD_Pos) //!< 0x30000000 
.equ FMC_BTR2_ACCMOD, FMC_BTR2_ACCMOD_Msk //!<ACCMOD[1:0] bits (Access mode) 
.equ FMC_BTR2_ACCMOD_0, (0x1 << FMC_BTR2_ACCMOD_Pos) //!< 0x10000000 
.equ FMC_BTR2_ACCMOD_1, (0x2 << FMC_BTR2_ACCMOD_Pos) //!< 0x20000000 
//******************  Bit definition for FMC_BTR3 register  ******************
.equ FMC_BTR3_ADDSET_Pos, (0) 
.equ FMC_BTR3_ADDSET_Msk, (0xF << FMC_BTR3_ADDSET_Pos) //!< 0x0000000F 
.equ FMC_BTR3_ADDSET, FMC_BTR3_ADDSET_Msk //!<ADDSET[3:0] bits (Address setup phase duration) 
.equ FMC_BTR3_ADDSET_0, (0x1 << FMC_BTR3_ADDSET_Pos) //!< 0x00000001 
.equ FMC_BTR3_ADDSET_1, (0x2 << FMC_BTR3_ADDSET_Pos) //!< 0x00000002 
.equ FMC_BTR3_ADDSET_2, (0x4 << FMC_BTR3_ADDSET_Pos) //!< 0x00000004 
.equ FMC_BTR3_ADDSET_3, (0x8 << FMC_BTR3_ADDSET_Pos) //!< 0x00000008 
.equ FMC_BTR3_ADDHLD_Pos, (4) 
.equ FMC_BTR3_ADDHLD_Msk, (0xF << FMC_BTR3_ADDHLD_Pos) //!< 0x000000F0 
.equ FMC_BTR3_ADDHLD, FMC_BTR3_ADDHLD_Msk //!<ADDHLD[3:0] bits (Address-hold phase duration) 
.equ FMC_BTR3_ADDHLD_0, (0x1 << FMC_BTR3_ADDHLD_Pos) //!< 0x00000010 
.equ FMC_BTR3_ADDHLD_1, (0x2 << FMC_BTR3_ADDHLD_Pos) //!< 0x00000020 
.equ FMC_BTR3_ADDHLD_2, (0x4 << FMC_BTR3_ADDHLD_Pos) //!< 0x00000040 
.equ FMC_BTR3_ADDHLD_3, (0x8 << FMC_BTR3_ADDHLD_Pos) //!< 0x00000080 
.equ FMC_BTR3_DATAST_Pos, (8) 
.equ FMC_BTR3_DATAST_Msk, (0xFF << FMC_BTR3_DATAST_Pos) //!< 0x0000FF00 
.equ FMC_BTR3_DATAST, FMC_BTR3_DATAST_Msk //!<DATAST [3:0] bits (Data-phase duration) 
.equ FMC_BTR3_DATAST_0, (0x01 << FMC_BTR3_DATAST_Pos) //!< 0x00000100 
.equ FMC_BTR3_DATAST_1, (0x02 << FMC_BTR3_DATAST_Pos) //!< 0x00000200 
.equ FMC_BTR3_DATAST_2, (0x04 << FMC_BTR3_DATAST_Pos) //!< 0x00000400 
.equ FMC_BTR3_DATAST_3, (0x08 << FMC_BTR3_DATAST_Pos) //!< 0x00000800 
.equ FMC_BTR3_DATAST_4, (0x10 << FMC_BTR3_DATAST_Pos) //!< 0x00001000 
.equ FMC_BTR3_DATAST_5, (0x20 << FMC_BTR3_DATAST_Pos) //!< 0x00002000 
.equ FMC_BTR3_DATAST_6, (0x40 << FMC_BTR3_DATAST_Pos) //!< 0x00004000 
.equ FMC_BTR3_DATAST_7, (0x80 << FMC_BTR3_DATAST_Pos) //!< 0x00008000 
.equ FMC_BTR3_BUSTURN_Pos, (16) 
.equ FMC_BTR3_BUSTURN_Msk, (0xF << FMC_BTR3_BUSTURN_Pos) //!< 0x000F0000 
.equ FMC_BTR3_BUSTURN, FMC_BTR3_BUSTURN_Msk //!<BUSTURN[3:0] bits (Bus turnaround phase duration) 
.equ FMC_BTR3_BUSTURN_0, (0x1 << FMC_BTR3_BUSTURN_Pos) //!< 0x00010000 
.equ FMC_BTR3_BUSTURN_1, (0x2 << FMC_BTR3_BUSTURN_Pos) //!< 0x00020000 
.equ FMC_BTR3_BUSTURN_2, (0x4 << FMC_BTR3_BUSTURN_Pos) //!< 0x00040000 
.equ FMC_BTR3_BUSTURN_3, (0x8 << FMC_BTR3_BUSTURN_Pos) //!< 0x00080000 
.equ FMC_BTR3_CLKDIV_Pos, (20) 
.equ FMC_BTR3_CLKDIV_Msk, (0xF << FMC_BTR3_CLKDIV_Pos) //!< 0x00F00000 
.equ FMC_BTR3_CLKDIV, FMC_BTR3_CLKDIV_Msk //!<CLKDIV[3:0] bits (Clock divide ratio) 
.equ FMC_BTR3_CLKDIV_0, (0x1 << FMC_BTR3_CLKDIV_Pos) //!< 0x00100000 
.equ FMC_BTR3_CLKDIV_1, (0x2 << FMC_BTR3_CLKDIV_Pos) //!< 0x00200000 
.equ FMC_BTR3_CLKDIV_2, (0x4 << FMC_BTR3_CLKDIV_Pos) //!< 0x00400000 
.equ FMC_BTR3_CLKDIV_3, (0x8 << FMC_BTR3_CLKDIV_Pos) //!< 0x00800000 
.equ FMC_BTR3_DATLAT_Pos, (24) 
.equ FMC_BTR3_DATLAT_Msk, (0xF << FMC_BTR3_DATLAT_Pos) //!< 0x0F000000 
.equ FMC_BTR3_DATLAT, FMC_BTR3_DATLAT_Msk //!<DATLA[3:0] bits (Data latency) 
.equ FMC_BTR3_DATLAT_0, (0x1 << FMC_BTR3_DATLAT_Pos) //!< 0x01000000 
.equ FMC_BTR3_DATLAT_1, (0x2 << FMC_BTR3_DATLAT_Pos) //!< 0x02000000 
.equ FMC_BTR3_DATLAT_2, (0x4 << FMC_BTR3_DATLAT_Pos) //!< 0x04000000 
.equ FMC_BTR3_DATLAT_3, (0x8 << FMC_BTR3_DATLAT_Pos) //!< 0x08000000 
.equ FMC_BTR3_ACCMOD_Pos, (28) 
.equ FMC_BTR3_ACCMOD_Msk, (0x3 << FMC_BTR3_ACCMOD_Pos) //!< 0x30000000 
.equ FMC_BTR3_ACCMOD, FMC_BTR3_ACCMOD_Msk //!<ACCMOD[1:0] bits (Access mode) 
.equ FMC_BTR3_ACCMOD_0, (0x1 << FMC_BTR3_ACCMOD_Pos) //!< 0x10000000 
.equ FMC_BTR3_ACCMOD_1, (0x2 << FMC_BTR3_ACCMOD_Pos) //!< 0x20000000 
//*****************  Bit definition for FMC_BTR4 register  ******************
.equ FMC_BTR4_ADDSET_Pos, (0) 
.equ FMC_BTR4_ADDSET_Msk, (0xF << FMC_BTR4_ADDSET_Pos) //!< 0x0000000F 
.equ FMC_BTR4_ADDSET, FMC_BTR4_ADDSET_Msk //!<ADDSET[3:0] bits (Address setup phase duration) 
.equ FMC_BTR4_ADDSET_0, (0x1 << FMC_BTR4_ADDSET_Pos) //!< 0x00000001 
.equ FMC_BTR4_ADDSET_1, (0x2 << FMC_BTR4_ADDSET_Pos) //!< 0x00000002 
.equ FMC_BTR4_ADDSET_2, (0x4 << FMC_BTR4_ADDSET_Pos) //!< 0x00000004 
.equ FMC_BTR4_ADDSET_3, (0x8 << FMC_BTR4_ADDSET_Pos) //!< 0x00000008 
.equ FMC_BTR4_ADDHLD_Pos, (4) 
.equ FMC_BTR4_ADDHLD_Msk, (0xF << FMC_BTR4_ADDHLD_Pos) //!< 0x000000F0 
.equ FMC_BTR4_ADDHLD, FMC_BTR4_ADDHLD_Msk //!<ADDHLD[3:0] bits (Address-hold phase duration) 
.equ FMC_BTR4_ADDHLD_0, (0x1 << FMC_BTR4_ADDHLD_Pos) //!< 0x00000010 
.equ FMC_BTR4_ADDHLD_1, (0x2 << FMC_BTR4_ADDHLD_Pos) //!< 0x00000020 
.equ FMC_BTR4_ADDHLD_2, (0x4 << FMC_BTR4_ADDHLD_Pos) //!< 0x00000040 
.equ FMC_BTR4_ADDHLD_3, (0x8 << FMC_BTR4_ADDHLD_Pos) //!< 0x00000080 
.equ FMC_BTR4_DATAST_Pos, (8) 
.equ FMC_BTR4_DATAST_Msk, (0xFF << FMC_BTR4_DATAST_Pos) //!< 0x0000FF00 
.equ FMC_BTR4_DATAST, FMC_BTR4_DATAST_Msk //!<DATAST [3:0] bits (Data-phase duration) 
.equ FMC_BTR4_DATAST_0, (0x01 << FMC_BTR4_DATAST_Pos) //!< 0x00000100 
.equ FMC_BTR4_DATAST_1, (0x02 << FMC_BTR4_DATAST_Pos) //!< 0x00000200 
.equ FMC_BTR4_DATAST_2, (0x04 << FMC_BTR4_DATAST_Pos) //!< 0x00000400 
.equ FMC_BTR4_DATAST_3, (0x08 << FMC_BTR4_DATAST_Pos) //!< 0x00000800 
.equ FMC_BTR4_DATAST_4, (0x10 << FMC_BTR4_DATAST_Pos) //!< 0x00001000 
.equ FMC_BTR4_DATAST_5, (0x20 << FMC_BTR4_DATAST_Pos) //!< 0x00002000 
.equ FMC_BTR4_DATAST_6, (0x40 << FMC_BTR4_DATAST_Pos) //!< 0x00004000 
.equ FMC_BTR4_DATAST_7, (0x80 << FMC_BTR4_DATAST_Pos) //!< 0x00008000 
.equ FMC_BTR4_BUSTURN_Pos, (16) 
.equ FMC_BTR4_BUSTURN_Msk, (0xF << FMC_BTR4_BUSTURN_Pos) //!< 0x000F0000 
.equ FMC_BTR4_BUSTURN, FMC_BTR4_BUSTURN_Msk //!<BUSTURN[3:0] bits (Bus turnaround phase duration) 
.equ FMC_BTR4_BUSTURN_0, (0x1 << FMC_BTR4_BUSTURN_Pos) //!< 0x00010000 
.equ FMC_BTR4_BUSTURN_1, (0x2 << FMC_BTR4_BUSTURN_Pos) //!< 0x00020000 
.equ FMC_BTR4_BUSTURN_2, (0x4 << FMC_BTR4_BUSTURN_Pos) //!< 0x00040000 
.equ FMC_BTR4_BUSTURN_3, (0x8 << FMC_BTR4_BUSTURN_Pos) //!< 0x00080000 
.equ FMC_BTR4_CLKDIV_Pos, (20) 
.equ FMC_BTR4_CLKDIV_Msk, (0xF << FMC_BTR4_CLKDIV_Pos) //!< 0x00F00000 
.equ FMC_BTR4_CLKDIV, FMC_BTR4_CLKDIV_Msk //!<CLKDIV[3:0] bits (Clock divide ratio) 
.equ FMC_BTR4_CLKDIV_0, (0x1 << FMC_BTR4_CLKDIV_Pos) //!< 0x00100000 
.equ FMC_BTR4_CLKDIV_1, (0x2 << FMC_BTR4_CLKDIV_Pos) //!< 0x00200000 
.equ FMC_BTR4_CLKDIV_2, (0x4 << FMC_BTR4_CLKDIV_Pos) //!< 0x00400000 
.equ FMC_BTR4_CLKDIV_3, (0x8 << FMC_BTR4_CLKDIV_Pos) //!< 0x00800000 
.equ FMC_BTR4_DATLAT_Pos, (24) 
.equ FMC_BTR4_DATLAT_Msk, (0xF << FMC_BTR4_DATLAT_Pos) //!< 0x0F000000 
.equ FMC_BTR4_DATLAT, FMC_BTR4_DATLAT_Msk //!<DATLA[3:0] bits (Data latency) 
.equ FMC_BTR4_DATLAT_0, (0x1 << FMC_BTR4_DATLAT_Pos) //!< 0x01000000 
.equ FMC_BTR4_DATLAT_1, (0x2 << FMC_BTR4_DATLAT_Pos) //!< 0x02000000 
.equ FMC_BTR4_DATLAT_2, (0x4 << FMC_BTR4_DATLAT_Pos) //!< 0x04000000 
.equ FMC_BTR4_DATLAT_3, (0x8 << FMC_BTR4_DATLAT_Pos) //!< 0x08000000 
.equ FMC_BTR4_ACCMOD_Pos, (28) 
.equ FMC_BTR4_ACCMOD_Msk, (0x3 << FMC_BTR4_ACCMOD_Pos) //!< 0x30000000 
.equ FMC_BTR4_ACCMOD, FMC_BTR4_ACCMOD_Msk //!<ACCMOD[1:0] bits (Access mode) 
.equ FMC_BTR4_ACCMOD_0, (0x1 << FMC_BTR4_ACCMOD_Pos) //!< 0x10000000 
.equ FMC_BTR4_ACCMOD_1, (0x2 << FMC_BTR4_ACCMOD_Pos) //!< 0x20000000 
//*****************  Bit definition for FMC_BWTRx register  *****************
.equ FMC_BWTRx_ADDSET_Pos, (0) 
.equ FMC_BWTRx_ADDSET_Msk, (0xF << FMC_BWTRx_ADDSET_Pos) //!< 0x0000000F 
.equ FMC_BWTRx_ADDSET, FMC_BWTRx_ADDSET_Msk //!<ADDSET[3:0] bits (Address setup phase duration) 
.equ FMC_BWTRx_ADDSET_0, (0x1 << FMC_BWTRx_ADDSET_Pos) //!< 0x00000001 
.equ FMC_BWTRx_ADDSET_1, (0x2 << FMC_BWTRx_ADDSET_Pos) //!< 0x00000002 
.equ FMC_BWTRx_ADDSET_2, (0x4 << FMC_BWTRx_ADDSET_Pos) //!< 0x00000004 
.equ FMC_BWTRx_ADDSET_3, (0x8 << FMC_BWTRx_ADDSET_Pos) //!< 0x00000008 
.equ FMC_BWTRx_ADDHLD_Pos, (4) 
.equ FMC_BWTRx_ADDHLD_Msk, (0xF << FMC_BWTRx_ADDHLD_Pos) //!< 0x000000F0 
.equ FMC_BWTRx_ADDHLD, FMC_BWTRx_ADDHLD_Msk //!<ADDHLD[3:0] bits (Address-hold phase duration) 
.equ FMC_BWTRx_ADDHLD_0, (0x1 << FMC_BWTRx_ADDHLD_Pos) //!< 0x00000010 
.equ FMC_BWTRx_ADDHLD_1, (0x2 << FMC_BWTRx_ADDHLD_Pos) //!< 0x00000020 
.equ FMC_BWTRx_ADDHLD_2, (0x4 << FMC_BWTRx_ADDHLD_Pos) //!< 0x00000040 
.equ FMC_BWTRx_ADDHLD_3, (0x8 << FMC_BWTRx_ADDHLD_Pos) //!< 0x00000080 
.equ FMC_BWTRx_DATAST_Pos, (8) 
.equ FMC_BWTRx_DATAST_Msk, (0xFF << FMC_BWTRx_DATAST_Pos) //!< 0x0000FF00 
.equ FMC_BWTRx_DATAST, FMC_BWTRx_DATAST_Msk //!<DATAST [3:0] bits (Data-phase duration) 
.equ FMC_BWTRx_DATAST_0, (0x01 << FMC_BWTRx_DATAST_Pos) //!< 0x00000100 
.equ FMC_BWTRx_DATAST_1, (0x02 << FMC_BWTRx_DATAST_Pos) //!< 0x00000200 
.equ FMC_BWTRx_DATAST_2, (0x04 << FMC_BWTRx_DATAST_Pos) //!< 0x00000400 
.equ FMC_BWTRx_DATAST_3, (0x08 << FMC_BWTRx_DATAST_Pos) //!< 0x00000800 
.equ FMC_BWTRx_DATAST_4, (0x10 << FMC_BWTRx_DATAST_Pos) //!< 0x00001000 
.equ FMC_BWTRx_DATAST_5, (0x20 << FMC_BWTRx_DATAST_Pos) //!< 0x00002000 
.equ FMC_BWTRx_DATAST_6, (0x40 << FMC_BWTRx_DATAST_Pos) //!< 0x00004000 
.equ FMC_BWTRx_DATAST_7, (0x80 << FMC_BWTRx_DATAST_Pos) //!< 0x00008000 
.equ FMC_BWTRx_ACCMOD_Pos, (28) 
.equ FMC_BWTRx_ACCMOD_Msk, (0x3 << FMC_BWTRx_ACCMOD_Pos) //!< 0x30000000 
.equ FMC_BWTRx_ACCMOD, FMC_BWTRx_ACCMOD_Msk //!<ACCMOD[1:0] bits (Access mode) 
.equ FMC_BWTRx_ACCMOD_0, (0x1 << FMC_BWTRx_ACCMOD_Pos) //!< 0x10000000 
.equ FMC_BWTRx_ACCMOD_1, (0x2 << FMC_BWTRx_ACCMOD_Pos) //!< 0x20000000 
// Old Bit definition for FMC_BWTRx register maintained for legacy purpose
.equ FMC_BWTRx_ADDSETx, FMC_BWTRx_ADDSET 
.equ FMC_BWTRx_ADDSETx_0, FMC_BWTRx_ADDSET_0 
.equ FMC_BWTRx_ADDSETx_1, FMC_BWTRx_ADDSET_1 
.equ FMC_BWTRx_ADDSETx_2, FMC_BWTRx_ADDSET_2 
.equ FMC_BWTRx_ADDSETx_3, FMC_BWTRx_ADDSET_3 
.equ FMC_BWTRx_ADDHLDx, FMC_BWTRx_ADDHLD 
.equ FMC_BWTRx_ADDHLDx_0, FMC_BWTRx_ADDHLD_0 
.equ FMC_BWTRx_ADDHLDx_1, FMC_BWTRx_ADDHLD_1 
.equ FMC_BWTRx_ADDHLDx_2, FMC_BWTRx_ADDHLD_2 
.equ FMC_BWTRx_ADDHLDx_3, FMC_BWTRx_ADDHLD_3 
.equ FMC_BWTRx_DATASTx, FMC_BWTRx_DATAST 
.equ FMC_BWTRx_DATASTx_0, FMC_BWTRx_DATAST_0 
.equ FMC_BWTRx_DATASTx_1, FMC_BWTRx_DATAST_1 
.equ FMC_BWTRx_DATASTx_2, FMC_BWTRx_DATAST_2 
.equ FMC_BWTRx_DATASTx_3, FMC_BWTRx_DATAST_3 
.equ FMC_BWTRx_DATASTx_4, FMC_BWTRx_DATAST_4 
.equ FMC_BWTRx_DATASTx_5, FMC_BWTRx_DATAST_5 
.equ FMC_BWTRx_DATASTx_6, FMC_BWTRx_DATAST_6 
.equ FMC_BWTRx_DATASTx_7, FMC_BWTRx_DATAST_7 
.equ FMC_BWTRx_ACCMODx, FMC_BWTRx_ACCMOD 
.equ FMC_BWTRx_ACCMODx_0, FMC_BWTRx_ACCMOD_0 
.equ FMC_BWTRx_ACCMODx_1, FMC_BWTRx_ACCMOD_1 
//*****************  Bit definition for FMC_BWTR1 register  *****************
.equ FMC_BWTR1_ADDSET_Pos, (0) 
.equ FMC_BWTR1_ADDSET_Msk, (0xF << FMC_BWTR1_ADDSET_Pos) //!< 0x0000000F 
.equ FMC_BWTR1_ADDSET, FMC_BWTR1_ADDSET_Msk //!<ADDSET[3:0] bits (Address setup phase duration) 
.equ FMC_BWTR1_ADDSET_0, (0x1 << FMC_BWTR1_ADDSET_Pos) //!< 0x00000001 
.equ FMC_BWTR1_ADDSET_1, (0x2 << FMC_BWTR1_ADDSET_Pos) //!< 0x00000002 
.equ FMC_BWTR1_ADDSET_2, (0x4 << FMC_BWTR1_ADDSET_Pos) //!< 0x00000004 
.equ FMC_BWTR1_ADDSET_3, (0x8 << FMC_BWTR1_ADDSET_Pos) //!< 0x00000008 
.equ FMC_BWTR1_ADDHLD_Pos, (4) 
.equ FMC_BWTR1_ADDHLD_Msk, (0xF << FMC_BWTR1_ADDHLD_Pos) //!< 0x000000F0 
.equ FMC_BWTR1_ADDHLD, FMC_BWTR1_ADDHLD_Msk //!<ADDHLD[3:0] bits (Address-hold phase duration) 
.equ FMC_BWTR1_ADDHLD_0, (0x1 << FMC_BWTR1_ADDHLD_Pos) //!< 0x00000010 
.equ FMC_BWTR1_ADDHLD_1, (0x2 << FMC_BWTR1_ADDHLD_Pos) //!< 0x00000020 
.equ FMC_BWTR1_ADDHLD_2, (0x4 << FMC_BWTR1_ADDHLD_Pos) //!< 0x00000040 
.equ FMC_BWTR1_ADDHLD_3, (0x8 << FMC_BWTR1_ADDHLD_Pos) //!< 0x00000080 
.equ FMC_BWTR1_DATAST_Pos, (8) 
.equ FMC_BWTR1_DATAST_Msk, (0xFF << FMC_BWTR1_DATAST_Pos) //!< 0x0000FF00 
.equ FMC_BWTR1_DATAST, FMC_BWTR1_DATAST_Msk //!<DATAST [3:0] bits (Data-phase duration) 
.equ FMC_BWTR1_DATAST_0, (0x01 << FMC_BWTR1_DATAST_Pos) //!< 0x00000100 
.equ FMC_BWTR1_DATAST_1, (0x02 << FMC_BWTR1_DATAST_Pos) //!< 0x00000200 
.equ FMC_BWTR1_DATAST_2, (0x04 << FMC_BWTR1_DATAST_Pos) //!< 0x00000400 
.equ FMC_BWTR1_DATAST_3, (0x08 << FMC_BWTR1_DATAST_Pos) //!< 0x00000800 
.equ FMC_BWTR1_DATAST_4, (0x10 << FMC_BWTR1_DATAST_Pos) //!< 0x00001000 
.equ FMC_BWTR1_DATAST_5, (0x20 << FMC_BWTR1_DATAST_Pos) //!< 0x00002000 
.equ FMC_BWTR1_DATAST_6, (0x40 << FMC_BWTR1_DATAST_Pos) //!< 0x00004000 
.equ FMC_BWTR1_DATAST_7, (0x80 << FMC_BWTR1_DATAST_Pos) //!< 0x00008000 
.equ FMC_BWTR1_CLKDIV_Pos, (20) 
.equ FMC_BWTR1_CLKDIV_Msk, (0xF << FMC_BWTR1_CLKDIV_Pos) //!< 0x00F00000 
.equ FMC_BWTR1_CLKDIV, FMC_BWTR1_CLKDIV_Msk //!<CLKDIV[3:0] bits (Clock divide ratio) 
.equ FMC_BWTR1_CLKDIV_0, (0x1 << FMC_BWTR1_CLKDIV_Pos) //!< 0x00100000 
.equ FMC_BWTR1_CLKDIV_1, (0x2 << FMC_BWTR1_CLKDIV_Pos) //!< 0x00200000 
.equ FMC_BWTR1_CLKDIV_2, (0x4 << FMC_BWTR1_CLKDIV_Pos) //!< 0x00400000 
.equ FMC_BWTR1_CLKDIV_3, (0x8 << FMC_BWTR1_CLKDIV_Pos) //!< 0x00800000 
.equ FMC_BWTR1_DATLAT_Pos, (24) 
.equ FMC_BWTR1_DATLAT_Msk, (0xF << FMC_BWTR1_DATLAT_Pos) //!< 0x0F000000 
.equ FMC_BWTR1_DATLAT, FMC_BWTR1_DATLAT_Msk //!<DATLA[3:0] bits (Data latency) 
.equ FMC_BWTR1_DATLAT_0, (0x1 << FMC_BWTR1_DATLAT_Pos) //!< 0x01000000 
.equ FMC_BWTR1_DATLAT_1, (0x2 << FMC_BWTR1_DATLAT_Pos) //!< 0x02000000 
.equ FMC_BWTR1_DATLAT_2, (0x4 << FMC_BWTR1_DATLAT_Pos) //!< 0x04000000 
.equ FMC_BWTR1_DATLAT_3, (0x8 << FMC_BWTR1_DATLAT_Pos) //!< 0x08000000 
.equ FMC_BWTR1_ACCMOD_Pos, (28) 
.equ FMC_BWTR1_ACCMOD_Msk, (0x3 << FMC_BWTR1_ACCMOD_Pos) //!< 0x30000000 
.equ FMC_BWTR1_ACCMOD, FMC_BWTR1_ACCMOD_Msk //!<ACCMOD[1:0] bits (Access mode) 
.equ FMC_BWTR1_ACCMOD_0, (0x1 << FMC_BWTR1_ACCMOD_Pos) //!< 0x10000000 
.equ FMC_BWTR1_ACCMOD_1, (0x2 << FMC_BWTR1_ACCMOD_Pos) //!< 0x20000000 
//*****************  Bit definition for FMC_BWTR2 register  *****************
.equ FMC_BWTR2_ADDSET_Pos, (0) 
.equ FMC_BWTR2_ADDSET_Msk, (0xF << FMC_BWTR2_ADDSET_Pos) //!< 0x0000000F 
.equ FMC_BWTR2_ADDSET, FMC_BWTR2_ADDSET_Msk //!<ADDSET[3:0] bits (Address setup phase duration) 
.equ FMC_BWTR2_ADDSET_0, (0x1 << FMC_BWTR2_ADDSET_Pos) //!< 0x00000001 
.equ FMC_BWTR2_ADDSET_1, (0x2 << FMC_BWTR2_ADDSET_Pos) //!< 0x00000002 
.equ FMC_BWTR2_ADDSET_2, (0x4 << FMC_BWTR2_ADDSET_Pos) //!< 0x00000004 
.equ FMC_BWTR2_ADDSET_3, (0x8 << FMC_BWTR2_ADDSET_Pos) //!< 0x00000008 
.equ FMC_BWTR2_ADDHLD_Pos, (4) 
.equ FMC_BWTR2_ADDHLD_Msk, (0xF << FMC_BWTR2_ADDHLD_Pos) //!< 0x000000F0 
.equ FMC_BWTR2_ADDHLD, FMC_BWTR2_ADDHLD_Msk //!<ADDHLD[3:0] bits (Address-hold phase duration) 
.equ FMC_BWTR2_ADDHLD_0, (0x1 << FMC_BWTR2_ADDHLD_Pos) //!< 0x00000010 
.equ FMC_BWTR2_ADDHLD_1, (0x2 << FMC_BWTR2_ADDHLD_Pos) //!< 0x00000020 
.equ FMC_BWTR2_ADDHLD_2, (0x4 << FMC_BWTR2_ADDHLD_Pos) //!< 0x00000040 
.equ FMC_BWTR2_ADDHLD_3, (0x8 << FMC_BWTR2_ADDHLD_Pos) //!< 0x00000080 
.equ FMC_BWTR2_DATAST_Pos, (8) 
.equ FMC_BWTR2_DATAST_Msk, (0xFF << FMC_BWTR2_DATAST_Pos) //!< 0x0000FF00 
.equ FMC_BWTR2_DATAST, FMC_BWTR2_DATAST_Msk //!<DATAST [3:0] bits (Data-phase duration) 
.equ FMC_BWTR2_DATAST_0, (0x01 << FMC_BWTR2_DATAST_Pos) //!< 0x00000100 
.equ FMC_BWTR2_DATAST_1, (0x02 << FMC_BWTR2_DATAST_Pos) //!< 0x00000200 
.equ FMC_BWTR2_DATAST_2, (0x04 << FMC_BWTR2_DATAST_Pos) //!< 0x00000400 
.equ FMC_BWTR2_DATAST_3, (0x08 << FMC_BWTR2_DATAST_Pos) //!< 0x00000800 
.equ FMC_BWTR2_DATAST_4, (0x10 << FMC_BWTR2_DATAST_Pos) //!< 0x00001000 
.equ FMC_BWTR2_DATAST_5, (0x20 << FMC_BWTR2_DATAST_Pos) //!< 0x00002000 
.equ FMC_BWTR2_DATAST_6, (0x40 << FMC_BWTR2_DATAST_Pos) //!< 0x00004000 
.equ FMC_BWTR2_DATAST_7, (0x80 << FMC_BWTR2_DATAST_Pos) //!< 0x00008000 
.equ FMC_BWTR2_CLKDIV_Pos, (20) 
.equ FMC_BWTR2_CLKDIV_Msk, (0xF << FMC_BWTR2_CLKDIV_Pos) //!< 0x00F00000 
.equ FMC_BWTR2_CLKDIV, FMC_BWTR2_CLKDIV_Msk //!<CLKDIV[3:0] bits (Clock divide ratio) 
.equ FMC_BWTR2_CLKDIV_0, (0x1 << FMC_BWTR2_CLKDIV_Pos) //!< 0x00100000 
.equ FMC_BWTR2_CLKDIV_1, (0x2 << FMC_BWTR2_CLKDIV_Pos) //!< 0x00200000 
.equ FMC_BWTR2_CLKDIV_2, (0x4 << FMC_BWTR2_CLKDIV_Pos) //!< 0x00400000 
.equ FMC_BWTR2_CLKDIV_3, (0x8 << FMC_BWTR2_CLKDIV_Pos) //!< 0x00800000 
.equ FMC_BWTR2_DATLAT_Pos, (24) 
.equ FMC_BWTR2_DATLAT_Msk, (0xF << FMC_BWTR2_DATLAT_Pos) //!< 0x0F000000 
.equ FMC_BWTR2_DATLAT, FMC_BWTR2_DATLAT_Msk //!<DATLA[3:0] bits (Data latency) 
.equ FMC_BWTR2_DATLAT_0, (0x1 << FMC_BWTR2_DATLAT_Pos) //!< 0x01000000 
.equ FMC_BWTR2_DATLAT_1, (0x2 << FMC_BWTR2_DATLAT_Pos) //!< 0x02000000 
.equ FMC_BWTR2_DATLAT_2, (0x4 << FMC_BWTR2_DATLAT_Pos) //!< 0x04000000 
.equ FMC_BWTR2_DATLAT_3, (0x8 << FMC_BWTR2_DATLAT_Pos) //!< 0x08000000 
.equ FMC_BWTR2_ACCMOD_Pos, (28) 
.equ FMC_BWTR2_ACCMOD_Msk, (0x3 << FMC_BWTR2_ACCMOD_Pos) //!< 0x30000000 
.equ FMC_BWTR2_ACCMOD, FMC_BWTR2_ACCMOD_Msk //!<ACCMOD[1:0] bits (Access mode) 
.equ FMC_BWTR2_ACCMOD_0, (0x1 << FMC_BWTR2_ACCMOD_Pos) //!< 0x10000000 
.equ FMC_BWTR2_ACCMOD_1, (0x2 << FMC_BWTR2_ACCMOD_Pos) //!< 0x20000000 
//*****************  Bit definition for FMC_BWTR3 register  *****************
.equ FMC_BWTR3_ADDSET_Pos, (0) 
.equ FMC_BWTR3_ADDSET_Msk, (0xF << FMC_BWTR3_ADDSET_Pos) //!< 0x0000000F 
.equ FMC_BWTR3_ADDSET, FMC_BWTR3_ADDSET_Msk //!<ADDSET[3:0] bits (Address setup phase duration) 
.equ FMC_BWTR3_ADDSET_0, (0x1 << FMC_BWTR3_ADDSET_Pos) //!< 0x00000001 
.equ FMC_BWTR3_ADDSET_1, (0x2 << FMC_BWTR3_ADDSET_Pos) //!< 0x00000002 
.equ FMC_BWTR3_ADDSET_2, (0x4 << FMC_BWTR3_ADDSET_Pos) //!< 0x00000004 
.equ FMC_BWTR3_ADDSET_3, (0x8 << FMC_BWTR3_ADDSET_Pos) //!< 0x00000008 
.equ FMC_BWTR3_ADDHLD_Pos, (4) 
.equ FMC_BWTR3_ADDHLD_Msk, (0xF << FMC_BWTR3_ADDHLD_Pos) //!< 0x000000F0 
.equ FMC_BWTR3_ADDHLD, FMC_BWTR3_ADDHLD_Msk //!<ADDHLD[3:0] bits (Address-hold phase duration) 
.equ FMC_BWTR3_ADDHLD_0, (0x1 << FMC_BWTR3_ADDHLD_Pos) //!< 0x00000010 
.equ FMC_BWTR3_ADDHLD_1, (0x2 << FMC_BWTR3_ADDHLD_Pos) //!< 0x00000020 
.equ FMC_BWTR3_ADDHLD_2, (0x4 << FMC_BWTR3_ADDHLD_Pos) //!< 0x00000040 
.equ FMC_BWTR3_ADDHLD_3, (0x8 << FMC_BWTR3_ADDHLD_Pos) //!< 0x00000080 
.equ FMC_BWTR3_DATAST_Pos, (8) 
.equ FMC_BWTR3_DATAST_Msk, (0xFF << FMC_BWTR3_DATAST_Pos) //!< 0x0000FF00 
.equ FMC_BWTR3_DATAST, FMC_BWTR3_DATAST_Msk //!<DATAST [3:0] bits (Data-phase duration) 
.equ FMC_BWTR3_DATAST_0, (0x01 << FMC_BWTR3_DATAST_Pos) //!< 0x00000100 
.equ FMC_BWTR3_DATAST_1, (0x02 << FMC_BWTR3_DATAST_Pos) //!< 0x00000200 
.equ FMC_BWTR3_DATAST_2, (0x04 << FMC_BWTR3_DATAST_Pos) //!< 0x00000400 
.equ FMC_BWTR3_DATAST_3, (0x08 << FMC_BWTR3_DATAST_Pos) //!< 0x00000800 
.equ FMC_BWTR3_DATAST_4, (0x10 << FMC_BWTR3_DATAST_Pos) //!< 0x00001000 
.equ FMC_BWTR3_DATAST_5, (0x20 << FMC_BWTR3_DATAST_Pos) //!< 0x00002000 
.equ FMC_BWTR3_DATAST_6, (0x40 << FMC_BWTR3_DATAST_Pos) //!< 0x00004000 
.equ FMC_BWTR3_DATAST_7, (0x80 << FMC_BWTR3_DATAST_Pos) //!< 0x00008000 
.equ FMC_BWTR3_CLKDIV_Pos, (20) 
.equ FMC_BWTR3_CLKDIV_Msk, (0xF << FMC_BWTR3_CLKDIV_Pos) //!< 0x00F00000 
.equ FMC_BWTR3_CLKDIV, FMC_BWTR3_CLKDIV_Msk //!<CLKDIV[3:0] bits (Clock divide ratio) 
.equ FMC_BWTR3_CLKDIV_0, (0x1 << FMC_BWTR3_CLKDIV_Pos) //!< 0x00100000 
.equ FMC_BWTR3_CLKDIV_1, (0x2 << FMC_BWTR3_CLKDIV_Pos) //!< 0x00200000 
.equ FMC_BWTR3_CLKDIV_2, (0x4 << FMC_BWTR3_CLKDIV_Pos) //!< 0x00400000 
.equ FMC_BWTR3_CLKDIV_3, (0x8 << FMC_BWTR3_CLKDIV_Pos) //!< 0x00800000 
.equ FMC_BWTR3_DATLAT_Pos, (24) 
.equ FMC_BWTR3_DATLAT_Msk, (0xF << FMC_BWTR3_DATLAT_Pos) //!< 0x0F000000 
.equ FMC_BWTR3_DATLAT, FMC_BWTR3_DATLAT_Msk //!<DATLA[3:0] bits (Data latency) 
.equ FMC_BWTR3_DATLAT_0, (0x1 << FMC_BWTR3_DATLAT_Pos) //!< 0x01000000 
.equ FMC_BWTR3_DATLAT_1, (0x2 << FMC_BWTR3_DATLAT_Pos) //!< 0x02000000 
.equ FMC_BWTR3_DATLAT_2, (0x4 << FMC_BWTR3_DATLAT_Pos) //!< 0x04000000 
.equ FMC_BWTR3_DATLAT_3, (0x8 << FMC_BWTR3_DATLAT_Pos) //!< 0x08000000 
.equ FMC_BWTR3_ACCMOD_Pos, (28) 
.equ FMC_BWTR3_ACCMOD_Msk, (0x3 << FMC_BWTR3_ACCMOD_Pos) //!< 0x30000000 
.equ FMC_BWTR3_ACCMOD, FMC_BWTR3_ACCMOD_Msk //!<ACCMOD[1:0] bits (Access mode) 
.equ FMC_BWTR3_ACCMOD_0, (0x1 << FMC_BWTR3_ACCMOD_Pos) //!< 0x10000000 
.equ FMC_BWTR3_ACCMOD_1, (0x2 << FMC_BWTR3_ACCMOD_Pos) //!< 0x20000000 
//*****************  Bit definition for FMC_BWTR4 register  *****************
.equ FMC_BWTR4_ADDSET_Pos, (0) 
.equ FMC_BWTR4_ADDSET_Msk, (0xF << FMC_BWTR4_ADDSET_Pos) //!< 0x0000000F 
.equ FMC_BWTR4_ADDSET, FMC_BWTR4_ADDSET_Msk //!<ADDSET[3:0] bits (Address setup phase duration) 
.equ FMC_BWTR4_ADDSET_0, (0x1 << FMC_BWTR4_ADDSET_Pos) //!< 0x00000001 
.equ FMC_BWTR4_ADDSET_1, (0x2 << FMC_BWTR4_ADDSET_Pos) //!< 0x00000002 
.equ FMC_BWTR4_ADDSET_2, (0x4 << FMC_BWTR4_ADDSET_Pos) //!< 0x00000004 
.equ FMC_BWTR4_ADDSET_3, (0x8 << FMC_BWTR4_ADDSET_Pos) //!< 0x00000008 
.equ FMC_BWTR4_ADDHLD_Pos, (4) 
.equ FMC_BWTR4_ADDHLD_Msk, (0xF << FMC_BWTR4_ADDHLD_Pos) //!< 0x000000F0 
.equ FMC_BWTR4_ADDHLD, FMC_BWTR4_ADDHLD_Msk //!<ADDHLD[3:0] bits (Address-hold phase duration) 
.equ FMC_BWTR4_ADDHLD_0, (0x1 << FMC_BWTR4_ADDHLD_Pos) //!< 0x00000010 
.equ FMC_BWTR4_ADDHLD_1, (0x2 << FMC_BWTR4_ADDHLD_Pos) //!< 0x00000020 
.equ FMC_BWTR4_ADDHLD_2, (0x4 << FMC_BWTR4_ADDHLD_Pos) //!< 0x00000040 
.equ FMC_BWTR4_ADDHLD_3, (0x8 << FMC_BWTR4_ADDHLD_Pos) //!< 0x00000080 
.equ FMC_BWTR4_DATAST_Pos, (8) 
.equ FMC_BWTR4_DATAST_Msk, (0xFF << FMC_BWTR4_DATAST_Pos) //!< 0x0000FF00 
.equ FMC_BWTR4_DATAST, FMC_BWTR4_DATAST_Msk //!<DATAST [3:0] bits (Data-phase duration) 
.equ FMC_BWTR4_DATAST_0, (0x01 << FMC_BWTR4_DATAST_Pos) //!< 0x00000100 
.equ FMC_BWTR4_DATAST_1, (0x02 << FMC_BWTR4_DATAST_Pos) //!< 0x00000200 
.equ FMC_BWTR4_DATAST_2, (0x04 << FMC_BWTR4_DATAST_Pos) //!< 0x00000400 
.equ FMC_BWTR4_DATAST_3, (0x08 << FMC_BWTR4_DATAST_Pos) //!< 0x00000800 
.equ FMC_BWTR4_DATAST_4, (0x10 << FMC_BWTR4_DATAST_Pos) //!< 0x00001000 
.equ FMC_BWTR4_DATAST_5, (0x20 << FMC_BWTR4_DATAST_Pos) //!< 0x00002000 
.equ FMC_BWTR4_DATAST_6, (0x40 << FMC_BWTR4_DATAST_Pos) //!< 0x00004000 
.equ FMC_BWTR4_DATAST_7, (0x80 << FMC_BWTR4_DATAST_Pos) //!< 0x00008000 
.equ FMC_BWTR4_CLKDIV_Pos, (20) 
.equ FMC_BWTR4_CLKDIV_Msk, (0xF << FMC_BWTR4_CLKDIV_Pos) //!< 0x00F00000 
.equ FMC_BWTR4_CLKDIV, FMC_BWTR4_CLKDIV_Msk //!<CLKDIV[3:0] bits (Clock divide ratio) 
.equ FMC_BWTR4_CLKDIV_0, (0x1 << FMC_BWTR4_CLKDIV_Pos) //!< 0x00100000 
.equ FMC_BWTR4_CLKDIV_1, (0x2 << FMC_BWTR4_CLKDIV_Pos) //!< 0x00200000 
.equ FMC_BWTR4_CLKDIV_2, (0x4 << FMC_BWTR4_CLKDIV_Pos) //!< 0x00400000 
.equ FMC_BWTR4_CLKDIV_3, (0x8 << FMC_BWTR4_CLKDIV_Pos) //!< 0x00800000 
.equ FMC_BWTR4_DATLAT_Pos, (24) 
.equ FMC_BWTR4_DATLAT_Msk, (0xF << FMC_BWTR4_DATLAT_Pos) //!< 0x0F000000 
.equ FMC_BWTR4_DATLAT, FMC_BWTR4_DATLAT_Msk //!<DATLA[3:0] bits (Data latency) 
.equ FMC_BWTR4_DATLAT_0, (0x1 << FMC_BWTR4_DATLAT_Pos) //!< 0x01000000 
.equ FMC_BWTR4_DATLAT_1, (0x2 << FMC_BWTR4_DATLAT_Pos) //!< 0x02000000 
.equ FMC_BWTR4_DATLAT_2, (0x4 << FMC_BWTR4_DATLAT_Pos) //!< 0x04000000 
.equ FMC_BWTR4_DATLAT_3, (0x8 << FMC_BWTR4_DATLAT_Pos) //!< 0x08000000 
.equ FMC_BWTR4_ACCMOD_Pos, (28) 
.equ FMC_BWTR4_ACCMOD_Msk, (0x3 << FMC_BWTR4_ACCMOD_Pos) //!< 0x30000000 
.equ FMC_BWTR4_ACCMOD, FMC_BWTR4_ACCMOD_Msk //!<ACCMOD[1:0] bits (Access mode) 
.equ FMC_BWTR4_ACCMOD_0, (0x1 << FMC_BWTR4_ACCMOD_Pos) //!< 0x10000000 
.equ FMC_BWTR4_ACCMOD_1, (0x2 << FMC_BWTR4_ACCMOD_Pos) //!< 0x20000000 
//*****************  Bit definition for FMC_PCRx register  ******************
.equ FMC_PCRx_PWAITEN_Pos, (1) 
.equ FMC_PCRx_PWAITEN_Msk, (0x1 << FMC_PCRx_PWAITEN_Pos) //!< 0x00000002 
.equ FMC_PCRx_PWAITEN, FMC_PCRx_PWAITEN_Msk //!<Wait feature enable bit 
.equ FMC_PCRx_PBKEN_Pos, (2) 
.equ FMC_PCRx_PBKEN_Msk, (0x1 << FMC_PCRx_PBKEN_Pos) //!< 0x00000004 
.equ FMC_PCRx_PBKEN, FMC_PCRx_PBKEN_Msk //!<PC Card/NAND Flash memory bank enable bit 
.equ FMC_PCRx_PTYP_Pos, (3) 
.equ FMC_PCRx_PTYP_Msk, (0x1 << FMC_PCRx_PTYP_Pos) //!< 0x00000008 
.equ FMC_PCRx_PTYP, FMC_PCRx_PTYP_Msk //!<Memory type 
.equ FMC_PCRx_PWID_Pos, (4) 
.equ FMC_PCRx_PWID_Msk, (0x3 << FMC_PCRx_PWID_Pos) //!< 0x00000030 
.equ FMC_PCRx_PWID, FMC_PCRx_PWID_Msk //!<PWID[1:0] bits (NAND Flash databus width) 
.equ FMC_PCRx_PWID_0, (0x1 << FMC_PCRx_PWID_Pos) //!< 0x00000010 
.equ FMC_PCRx_PWID_1, (0x2 << FMC_PCRx_PWID_Pos) //!< 0x00000020 
.equ FMC_PCRx_ECCEN_Pos, (6) 
.equ FMC_PCRx_ECCEN_Msk, (0x1 << FMC_PCRx_ECCEN_Pos) //!< 0x00000040 
.equ FMC_PCRx_ECCEN, FMC_PCRx_ECCEN_Msk //!<ECC computation logic enable bit 
.equ FMC_PCRx_TCLR_Pos, (9) 
.equ FMC_PCRx_TCLR_Msk, (0xF << FMC_PCRx_TCLR_Pos) //!< 0x00001E00 
.equ FMC_PCRx_TCLR, FMC_PCRx_TCLR_Msk //!<TCLR[3:0] bits (CLE to RE delay) 
.equ FMC_PCRx_TCLR_0, (0x1 << FMC_PCRx_TCLR_Pos) //!< 0x00000200 
.equ FMC_PCRx_TCLR_1, (0x2 << FMC_PCRx_TCLR_Pos) //!< 0x00000400 
.equ FMC_PCRx_TCLR_2, (0x4 << FMC_PCRx_TCLR_Pos) //!< 0x00000800 
.equ FMC_PCRx_TCLR_3, (0x8 << FMC_PCRx_TCLR_Pos) //!< 0x00001000 
.equ FMC_PCRx_TAR_Pos, (13) 
.equ FMC_PCRx_TAR_Msk, (0xF << FMC_PCRx_TAR_Pos) //!< 0x0001E000 
.equ FMC_PCRx_TAR, FMC_PCRx_TAR_Msk //!<TAR[3:0] bits (ALE to RE delay) 
.equ FMC_PCRx_TAR_0, (0x1 << FMC_PCRx_TAR_Pos) //!< 0x00002000 
.equ FMC_PCRx_TAR_1, (0x2 << FMC_PCRx_TAR_Pos) //!< 0x00004000 
.equ FMC_PCRx_TAR_2, (0x4 << FMC_PCRx_TAR_Pos) //!< 0x00008000 
.equ FMC_PCRx_TAR_3, (0x8 << FMC_PCRx_TAR_Pos) //!< 0x00010000 
.equ FMC_PCRx_ECCPS_Pos, (17) 
.equ FMC_PCRx_ECCPS_Msk, (0x7 << FMC_PCRx_ECCPS_Pos) //!< 0x000E0000 
.equ FMC_PCRx_ECCPS, FMC_PCRx_ECCPS_Msk //!<ECCPS[1:0] bits (ECC page size) 
.equ FMC_PCRx_ECCPS_0, (0x1 << FMC_PCRx_ECCPS_Pos) //!< 0x00020000 
.equ FMC_PCRx_ECCPS_1, (0x2 << FMC_PCRx_ECCPS_Pos) //!< 0x00040000 
.equ FMC_PCRx_ECCPS_2, (0x4 << FMC_PCRx_ECCPS_Pos) //!< 0x00080000 
//*****************  Bit definition for FMC_PCR2 register  ******************
.equ FMC_PCR2_PWAITEN_Pos, (1) 
.equ FMC_PCR2_PWAITEN_Msk, (0x1 << FMC_PCR2_PWAITEN_Pos) //!< 0x00000002 
.equ FMC_PCR2_PWAITEN, FMC_PCR2_PWAITEN_Msk //!<Wait feature enable bit 
.equ FMC_PCR2_PBKEN_Pos, (2) 
.equ FMC_PCR2_PBKEN_Msk, (0x1 << FMC_PCR2_PBKEN_Pos) //!< 0x00000004 
.equ FMC_PCR2_PBKEN, FMC_PCR2_PBKEN_Msk //!<PC Card/NAND Flash memory bank enable bit 
.equ FMC_PCR2_PTYP_Pos, (3) 
.equ FMC_PCR2_PTYP_Msk, (0x1 << FMC_PCR2_PTYP_Pos) //!< 0x00000008 
.equ FMC_PCR2_PTYP, FMC_PCR2_PTYP_Msk //!<Memory type 
.equ FMC_PCR2_PWID_Pos, (4) 
.equ FMC_PCR2_PWID_Msk, (0x3 << FMC_PCR2_PWID_Pos) //!< 0x00000030 
.equ FMC_PCR2_PWID, FMC_PCR2_PWID_Msk //!<PWID[1:0] bits (NAND Flash databus width) 
.equ FMC_PCR2_PWID_0, (0x1 << FMC_PCR2_PWID_Pos) //!< 0x00000010 
.equ FMC_PCR2_PWID_1, (0x2 << FMC_PCR2_PWID_Pos) //!< 0x00000020 
.equ FMC_PCR2_ECCEN_Pos, (6) 
.equ FMC_PCR2_ECCEN_Msk, (0x1 << FMC_PCR2_ECCEN_Pos) //!< 0x00000040 
.equ FMC_PCR2_ECCEN, FMC_PCR2_ECCEN_Msk //!<ECC computation logic enable bit 
.equ FMC_PCR2_TCLR_Pos, (9) 
.equ FMC_PCR2_TCLR_Msk, (0xF << FMC_PCR2_TCLR_Pos) //!< 0x00001E00 
.equ FMC_PCR2_TCLR, FMC_PCR2_TCLR_Msk //!<TCLR[3:0] bits (CLE to RE delay) 
.equ FMC_PCR2_TCLR_0, (0x1 << FMC_PCR2_TCLR_Pos) //!< 0x00000200 
.equ FMC_PCR2_TCLR_1, (0x2 << FMC_PCR2_TCLR_Pos) //!< 0x00000400 
.equ FMC_PCR2_TCLR_2, (0x4 << FMC_PCR2_TCLR_Pos) //!< 0x00000800 
.equ FMC_PCR2_TCLR_3, (0x8 << FMC_PCR2_TCLR_Pos) //!< 0x00001000 
.equ FMC_PCR2_TAR_Pos, (13) 
.equ FMC_PCR2_TAR_Msk, (0xF << FMC_PCR2_TAR_Pos) //!< 0x0001E000 
.equ FMC_PCR2_TAR, FMC_PCR2_TAR_Msk //!<TAR[3:0] bits (ALE to RE delay) 
.equ FMC_PCR2_TAR_0, (0x1 << FMC_PCR2_TAR_Pos) //!< 0x00002000 
.equ FMC_PCR2_TAR_1, (0x2 << FMC_PCR2_TAR_Pos) //!< 0x00004000 
.equ FMC_PCR2_TAR_2, (0x4 << FMC_PCR2_TAR_Pos) //!< 0x00008000 
.equ FMC_PCR2_TAR_3, (0x8 << FMC_PCR2_TAR_Pos) //!< 0x00010000 
.equ FMC_PCR2_ECCPS_Pos, (17) 
.equ FMC_PCR2_ECCPS_Msk, (0x7 << FMC_PCR2_ECCPS_Pos) //!< 0x000E0000 
.equ FMC_PCR2_ECCPS, FMC_PCR2_ECCPS_Msk //!<ECCPS[1:0] bits (ECC page size) 
.equ FMC_PCR2_ECCPS_0, (0x1 << FMC_PCR2_ECCPS_Pos) //!< 0x00020000 
.equ FMC_PCR2_ECCPS_1, (0x2 << FMC_PCR2_ECCPS_Pos) //!< 0x00040000 
.equ FMC_PCR2_ECCPS_2, (0x4 << FMC_PCR2_ECCPS_Pos) //!< 0x00080000 
//*****************  Bit definition for FMC_PCR3 register  ******************
.equ FMC_PCR3_PWAITEN_Pos, (1) 
.equ FMC_PCR3_PWAITEN_Msk, (0x1 << FMC_PCR3_PWAITEN_Pos) //!< 0x00000002 
.equ FMC_PCR3_PWAITEN, FMC_PCR3_PWAITEN_Msk //!<Wait feature enable bit 
.equ FMC_PCR3_PBKEN_Pos, (2) 
.equ FMC_PCR3_PBKEN_Msk, (0x1 << FMC_PCR3_PBKEN_Pos) //!< 0x00000004 
.equ FMC_PCR3_PBKEN, FMC_PCR3_PBKEN_Msk //!<PC Card/NAND Flash memory bank enable bit 
.equ FMC_PCR3_PTYP_Pos, (3) 
.equ FMC_PCR3_PTYP_Msk, (0x1 << FMC_PCR3_PTYP_Pos) //!< 0x00000008 
.equ FMC_PCR3_PTYP, FMC_PCR3_PTYP_Msk //!<Memory type 
.equ FMC_PCR3_PWID_Pos, (4) 
.equ FMC_PCR3_PWID_Msk, (0x3 << FMC_PCR3_PWID_Pos) //!< 0x00000030 
.equ FMC_PCR3_PWID, FMC_PCR3_PWID_Msk //!<PWID[1:0] bits (NAND Flash databus width) 
.equ FMC_PCR3_PWID_0, (0x1 << FMC_PCR3_PWID_Pos) //!< 0x00000010 
.equ FMC_PCR3_PWID_1, (0x2 << FMC_PCR3_PWID_Pos) //!< 0x00000020 
.equ FMC_PCR3_ECCEN_Pos, (6) 
.equ FMC_PCR3_ECCEN_Msk, (0x1 << FMC_PCR3_ECCEN_Pos) //!< 0x00000040 
.equ FMC_PCR3_ECCEN, FMC_PCR3_ECCEN_Msk //!<ECC computation logic enable bit 
.equ FMC_PCR3_TCLR_Pos, (9) 
.equ FMC_PCR3_TCLR_Msk, (0xF << FMC_PCR3_TCLR_Pos) //!< 0x00001E00 
.equ FMC_PCR3_TCLR, FMC_PCR3_TCLR_Msk //!<TCLR[3:0] bits (CLE to RE delay) 
.equ FMC_PCR3_TCLR_0, (0x1 << FMC_PCR3_TCLR_Pos) //!< 0x00000200 
.equ FMC_PCR3_TCLR_1, (0x2 << FMC_PCR3_TCLR_Pos) //!< 0x00000400 
.equ FMC_PCR3_TCLR_2, (0x4 << FMC_PCR3_TCLR_Pos) //!< 0x00000800 
.equ FMC_PCR3_TCLR_3, (0x8 << FMC_PCR3_TCLR_Pos) //!< 0x00001000 
.equ FMC_PCR3_TAR_Pos, (13) 
.equ FMC_PCR3_TAR_Msk, (0xF << FMC_PCR3_TAR_Pos) //!< 0x0001E000 
.equ FMC_PCR3_TAR, FMC_PCR3_TAR_Msk //!<TAR[3:0] bits (ALE to RE delay) 
.equ FMC_PCR3_TAR_0, (0x1 << FMC_PCR3_TAR_Pos) //!< 0x00002000 
.equ FMC_PCR3_TAR_1, (0x2 << FMC_PCR3_TAR_Pos) //!< 0x00004000 
.equ FMC_PCR3_TAR_2, (0x4 << FMC_PCR3_TAR_Pos) //!< 0x00008000 
.equ FMC_PCR3_TAR_3, (0x8 << FMC_PCR3_TAR_Pos) //!< 0x00010000 
.equ FMC_PCR3_ECCPS_Pos, (17) 
.equ FMC_PCR3_ECCPS_Msk, (0x7 << FMC_PCR3_ECCPS_Pos) //!< 0x000E0000 
.equ FMC_PCR3_ECCPS, FMC_PCR3_ECCPS_Msk //!<ECCPS[2:0] bits (ECC page size) 
.equ FMC_PCR3_ECCPS_0, (0x1 << FMC_PCR3_ECCPS_Pos) //!< 0x00020000 
.equ FMC_PCR3_ECCPS_1, (0x2 << FMC_PCR3_ECCPS_Pos) //!< 0x00040000 
.equ FMC_PCR3_ECCPS_2, (0x4 << FMC_PCR3_ECCPS_Pos) //!< 0x00080000 
//*****************  Bit definition for FMC_PCR4 register  ******************
.equ FMC_PCR4_PWAITEN_Pos, (1) 
.equ FMC_PCR4_PWAITEN_Msk, (0x1 << FMC_PCR4_PWAITEN_Pos) //!< 0x00000002 
.equ FMC_PCR4_PWAITEN, FMC_PCR4_PWAITEN_Msk //!<Wait feature enable bit 
.equ FMC_PCR4_PBKEN_Pos, (2) 
.equ FMC_PCR4_PBKEN_Msk, (0x1 << FMC_PCR4_PBKEN_Pos) //!< 0x00000004 
.equ FMC_PCR4_PBKEN, FMC_PCR4_PBKEN_Msk //!<PC Card/NAND Flash memory bank enable bit 
.equ FMC_PCR4_PTYP_Pos, (3) 
.equ FMC_PCR4_PTYP_Msk, (0x1 << FMC_PCR4_PTYP_Pos) //!< 0x00000008 
.equ FMC_PCR4_PTYP, FMC_PCR4_PTYP_Msk //!<Memory type 
.equ FMC_PCR4_PWID_Pos, (4) 
.equ FMC_PCR4_PWID_Msk, (0x3 << FMC_PCR4_PWID_Pos) //!< 0x00000030 
.equ FMC_PCR4_PWID, FMC_PCR4_PWID_Msk //!<PWID[1:0] bits (NAND Flash databus width) 
.equ FMC_PCR4_PWID_0, (0x1 << FMC_PCR4_PWID_Pos) //!< 0x00000010 
.equ FMC_PCR4_PWID_1, (0x2 << FMC_PCR4_PWID_Pos) //!< 0x00000020 
.equ FMC_PCR4_ECCEN_Pos, (6) 
.equ FMC_PCR4_ECCEN_Msk, (0x1 << FMC_PCR4_ECCEN_Pos) //!< 0x00000040 
.equ FMC_PCR4_ECCEN, FMC_PCR4_ECCEN_Msk //!<ECC computation logic enable bit 
.equ FMC_PCR4_TCLR_Pos, (9) 
.equ FMC_PCR4_TCLR_Msk, (0xF << FMC_PCR4_TCLR_Pos) //!< 0x00001E00 
.equ FMC_PCR4_TCLR, FMC_PCR4_TCLR_Msk //!<TCLR[3:0] bits (CLE to RE delay) 
.equ FMC_PCR4_TCLR_0, (0x1 << FMC_PCR4_TCLR_Pos) //!< 0x00000200 
.equ FMC_PCR4_TCLR_1, (0x2 << FMC_PCR4_TCLR_Pos) //!< 0x00000400 
.equ FMC_PCR4_TCLR_2, (0x4 << FMC_PCR4_TCLR_Pos) //!< 0x00000800 
.equ FMC_PCR4_TCLR_3, (0x8 << FMC_PCR4_TCLR_Pos) //!< 0x00001000 
.equ FMC_PCR4_TAR_Pos, (13) 
.equ FMC_PCR4_TAR_Msk, (0xF << FMC_PCR4_TAR_Pos) //!< 0x0001E000 
.equ FMC_PCR4_TAR, FMC_PCR4_TAR_Msk //!<TAR[3:0] bits (ALE to RE delay) 
.equ FMC_PCR4_TAR_0, (0x1 << FMC_PCR4_TAR_Pos) //!< 0x00002000 
.equ FMC_PCR4_TAR_1, (0x2 << FMC_PCR4_TAR_Pos) //!< 0x00004000 
.equ FMC_PCR4_TAR_2, (0x4 << FMC_PCR4_TAR_Pos) //!< 0x00008000 
.equ FMC_PCR4_TAR_3, (0x8 << FMC_PCR4_TAR_Pos) //!< 0x00010000 
.equ FMC_PCR4_ECCPS_Pos, (17) 
.equ FMC_PCR4_ECCPS_Msk, (0x7 << FMC_PCR4_ECCPS_Pos) //!< 0x000E0000 
.equ FMC_PCR4_ECCPS, FMC_PCR4_ECCPS_Msk //!<ECCPS[2:0] bits (ECC page size) 
.equ FMC_PCR4_ECCPS_0, (0x1 << FMC_PCR4_ECCPS_Pos) //!< 0x00020000 
.equ FMC_PCR4_ECCPS_1, (0x2 << FMC_PCR4_ECCPS_Pos) //!< 0x00040000 
.equ FMC_PCR4_ECCPS_2, (0x4 << FMC_PCR4_ECCPS_Pos) //!< 0x00080000 
//******************  Bit definition for FMC_SRx register  ******************
.equ FMC_SRx_IRS_Pos, (0) 
.equ FMC_SRx_IRS_Msk, (0x1 << FMC_SRx_IRS_Pos) //!< 0x00000001 
.equ FMC_SRx_IRS, FMC_SRx_IRS_Msk //!<Interrupt Rising Edge status 
.equ FMC_SRx_ILS_Pos, (1) 
.equ FMC_SRx_ILS_Msk, (0x1 << FMC_SRx_ILS_Pos) //!< 0x00000002 
.equ FMC_SRx_ILS, FMC_SRx_ILS_Msk //!<Interrupt Level status 
.equ FMC_SRx_IFS_Pos, (2) 
.equ FMC_SRx_IFS_Msk, (0x1 << FMC_SRx_IFS_Pos) //!< 0x00000004 
.equ FMC_SRx_IFS, FMC_SRx_IFS_Msk //!<Interrupt Falling Edge status 
.equ FMC_SRx_IREN_Pos, (3) 
.equ FMC_SRx_IREN_Msk, (0x1 << FMC_SRx_IREN_Pos) //!< 0x00000008 
.equ FMC_SRx_IREN, FMC_SRx_IREN_Msk //!<Interrupt Rising Edge detection Enable bit 
.equ FMC_SRx_ILEN_Pos, (4) 
.equ FMC_SRx_ILEN_Msk, (0x1 << FMC_SRx_ILEN_Pos) //!< 0x00000010 
.equ FMC_SRx_ILEN, FMC_SRx_ILEN_Msk //!<Interrupt Level detection Enable bit 
.equ FMC_SRx_IFEN_Pos, (5) 
.equ FMC_SRx_IFEN_Msk, (0x1 << FMC_SRx_IFEN_Pos) //!< 0x00000020 
.equ FMC_SRx_IFEN, FMC_SRx_IFEN_Msk //!<Interrupt Falling Edge detection Enable bit 
.equ FMC_SRx_FEMPT_Pos, (6) 
.equ FMC_SRx_FEMPT_Msk, (0x1 << FMC_SRx_FEMPT_Pos) //!< 0x00000040 
.equ FMC_SRx_FEMPT, FMC_SRx_FEMPT_Msk //!<FIFO empty 
//******************  Bit definition for FMC_SR2 register  ******************
.equ FMC_SR2_IRS_Pos, (0) 
.equ FMC_SR2_IRS_Msk, (0x1 << FMC_SR2_IRS_Pos) //!< 0x00000001 
.equ FMC_SR2_IRS, FMC_SR2_IRS_Msk //!<Interrupt Rising Edge status 
.equ FMC_SR2_ILS_Pos, (1) 
.equ FMC_SR2_ILS_Msk, (0x1 << FMC_SR2_ILS_Pos) //!< 0x00000002 
.equ FMC_SR2_ILS, FMC_SR2_ILS_Msk //!<Interrupt Level status 
.equ FMC_SR2_IFS_Pos, (2) 
.equ FMC_SR2_IFS_Msk, (0x1 << FMC_SR2_IFS_Pos) //!< 0x00000004 
.equ FMC_SR2_IFS, FMC_SR2_IFS_Msk //!<Interrupt Falling Edge status 
.equ FMC_SR2_IREN_Pos, (3) 
.equ FMC_SR2_IREN_Msk, (0x1 << FMC_SR2_IREN_Pos) //!< 0x00000008 
.equ FMC_SR2_IREN, FMC_SR2_IREN_Msk //!<Interrupt Rising Edge detection Enable bit 
.equ FMC_SR2_ILEN_Pos, (4) 
.equ FMC_SR2_ILEN_Msk, (0x1 << FMC_SR2_ILEN_Pos) //!< 0x00000010 
.equ FMC_SR2_ILEN, FMC_SR2_ILEN_Msk //!<Interrupt Level detection Enable bit 
.equ FMC_SR2_IFEN_Pos, (5) 
.equ FMC_SR2_IFEN_Msk, (0x1 << FMC_SR2_IFEN_Pos) //!< 0x00000020 
.equ FMC_SR2_IFEN, FMC_SR2_IFEN_Msk //!<Interrupt Falling Edge detection Enable bit 
.equ FMC_SR2_FEMPT_Pos, (6) 
.equ FMC_SR2_FEMPT_Msk, (0x1 << FMC_SR2_FEMPT_Pos) //!< 0x00000040 
.equ FMC_SR2_FEMPT, FMC_SR2_FEMPT_Msk //!<FIFO empty 
//******************  Bit definition for FMC_SR3 register  ******************
.equ FMC_SR3_IRS_Pos, (0) 
.equ FMC_SR3_IRS_Msk, (0x1 << FMC_SR3_IRS_Pos) //!< 0x00000001 
.equ FMC_SR3_IRS, FMC_SR3_IRS_Msk //!<Interrupt Rising Edge status 
.equ FMC_SR3_ILS_Pos, (1) 
.equ FMC_SR3_ILS_Msk, (0x1 << FMC_SR3_ILS_Pos) //!< 0x00000002 
.equ FMC_SR3_ILS, FMC_SR3_ILS_Msk //!<Interrupt Level status 
.equ FMC_SR3_IFS_Pos, (2) 
.equ FMC_SR3_IFS_Msk, (0x1 << FMC_SR3_IFS_Pos) //!< 0x00000004 
.equ FMC_SR3_IFS, FMC_SR3_IFS_Msk //!<Interrupt Falling Edge status 
.equ FMC_SR3_IREN_Pos, (3) 
.equ FMC_SR3_IREN_Msk, (0x1 << FMC_SR3_IREN_Pos) //!< 0x00000008 
.equ FMC_SR3_IREN, FMC_SR3_IREN_Msk //!<Interrupt Rising Edge detection Enable bit 
.equ FMC_SR3_ILEN_Pos, (4) 
.equ FMC_SR3_ILEN_Msk, (0x1 << FMC_SR3_ILEN_Pos) //!< 0x00000010 
.equ FMC_SR3_ILEN, FMC_SR3_ILEN_Msk //!<Interrupt Level detection Enable bit 
.equ FMC_SR3_IFEN_Pos, (5) 
.equ FMC_SR3_IFEN_Msk, (0x1 << FMC_SR3_IFEN_Pos) //!< 0x00000020 
.equ FMC_SR3_IFEN, FMC_SR3_IFEN_Msk //!<Interrupt Falling Edge detection Enable bit 
.equ FMC_SR3_FEMPT_Pos, (6) 
.equ FMC_SR3_FEMPT_Msk, (0x1 << FMC_SR3_FEMPT_Pos) //!< 0x00000040 
.equ FMC_SR3_FEMPT, FMC_SR3_FEMPT_Msk //!<FIFO empty 
//******************  Bit definition for FMC_SR4 register  ******************
.equ FMC_SR4_IRS_Pos, (0) 
.equ FMC_SR4_IRS_Msk, (0x1 << FMC_SR4_IRS_Pos) //!< 0x00000001 
.equ FMC_SR4_IRS, FMC_SR4_IRS_Msk //!<Interrupt Rising Edge status 
.equ FMC_SR4_ILS_Pos, (1) 
.equ FMC_SR4_ILS_Msk, (0x1 << FMC_SR4_ILS_Pos) //!< 0x00000002 
.equ FMC_SR4_ILS, FMC_SR4_ILS_Msk //!<Interrupt Level status 
.equ FMC_SR4_IFS_Pos, (2) 
.equ FMC_SR4_IFS_Msk, (0x1 << FMC_SR4_IFS_Pos) //!< 0x00000004 
.equ FMC_SR4_IFS, FMC_SR4_IFS_Msk //!<Interrupt Falling Edge status 
.equ FMC_SR4_IREN_Pos, (3) 
.equ FMC_SR4_IREN_Msk, (0x1 << FMC_SR4_IREN_Pos) //!< 0x00000008 
.equ FMC_SR4_IREN, FMC_SR4_IREN_Msk //!<Interrupt Rising Edge detection Enable bit 
.equ FMC_SR4_ILEN_Pos, (4) 
.equ FMC_SR4_ILEN_Msk, (0x1 << FMC_SR4_ILEN_Pos) //!< 0x00000010 
.equ FMC_SR4_ILEN, FMC_SR4_ILEN_Msk //!<Interrupt Level detection Enable bit 
.equ FMC_SR4_IFEN_Pos, (5) 
.equ FMC_SR4_IFEN_Msk, (0x1 << FMC_SR4_IFEN_Pos) //!< 0x00000020 
.equ FMC_SR4_IFEN, FMC_SR4_IFEN_Msk //!<Interrupt Falling Edge detection Enable bit 
.equ FMC_SR4_FEMPT_Pos, (6) 
.equ FMC_SR4_FEMPT_Msk, (0x1 << FMC_SR4_FEMPT_Pos) //!< 0x00000040 
.equ FMC_SR4_FEMPT, FMC_SR4_FEMPT_Msk //!<FIFO empty 
//*****************  Bit definition for FMC_PMEMx register  *****************
.equ FMC_PMEMx_MEMSETx_Pos, (0) 
.equ FMC_PMEMx_MEMSETx_Msk, (0xFF << FMC_PMEMx_MEMSETx_Pos) //!< 0x000000FF 
.equ FMC_PMEMx_MEMSETx, FMC_PMEMx_MEMSETx_Msk //!<MEMSETx[7:0] bits (Common memory x setup time) 
.equ FMC_PMEMx_MEMSETx_0, (0x01 << FMC_PMEMx_MEMSETx_Pos) //!< 0x00000001 
.equ FMC_PMEMx_MEMSETx_1, (0x02 << FMC_PMEMx_MEMSETx_Pos) //!< 0x00000002 
.equ FMC_PMEMx_MEMSETx_2, (0x04 << FMC_PMEMx_MEMSETx_Pos) //!< 0x00000004 
.equ FMC_PMEMx_MEMSETx_3, (0x08 << FMC_PMEMx_MEMSETx_Pos) //!< 0x00000008 
.equ FMC_PMEMx_MEMSETx_4, (0x10 << FMC_PMEMx_MEMSETx_Pos) //!< 0x00000010 
.equ FMC_PMEMx_MEMSETx_5, (0x20 << FMC_PMEMx_MEMSETx_Pos) //!< 0x00000020 
.equ FMC_PMEMx_MEMSETx_6, (0x40 << FMC_PMEMx_MEMSETx_Pos) //!< 0x00000040 
.equ FMC_PMEMx_MEMSETx_7, (0x80 << FMC_PMEMx_MEMSETx_Pos) //!< 0x00000080 
.equ FMC_PMEMx_MEMWAITx_Pos, (8) 
.equ FMC_PMEMx_MEMWAITx_Msk, (0xFF << FMC_PMEMx_MEMWAITx_Pos) //!< 0x0000FF00 
.equ FMC_PMEMx_MEMWAITx, FMC_PMEMx_MEMWAITx_Msk //!<MEMWAITx[7:0] bits (Common memory x wait time) 
.equ FMC_PMEMx_MEMWAITx_0, (0x01 << FMC_PMEMx_MEMWAITx_Pos) //!< 0x00000100 
.equ FMC_PMEMx_MEMWAITx_1, (0x02 << FMC_PMEMx_MEMWAITx_Pos) //!< 0x00000200 
.equ FMC_PMEMx_MEMWAITx_2, (0x04 << FMC_PMEMx_MEMWAITx_Pos) //!< 0x00000400 
.equ FMC_PMEMx_MEMWAITx_3, (0x08 << FMC_PMEMx_MEMWAITx_Pos) //!< 0x00000800 
.equ FMC_PMEMx_MEMWAITx_4, (0x10 << FMC_PMEMx_MEMWAITx_Pos) //!< 0x00001000 
.equ FMC_PMEMx_MEMWAITx_5, (0x20 << FMC_PMEMx_MEMWAITx_Pos) //!< 0x00002000 
.equ FMC_PMEMx_MEMWAITx_6, (0x40 << FMC_PMEMx_MEMWAITx_Pos) //!< 0x00004000 
.equ FMC_PMEMx_MEMWAITx_7, (0x80 << FMC_PMEMx_MEMWAITx_Pos) //!< 0x00008000 
.equ FMC_PMEMx_MEMHOLDx_Pos, (16) 
.equ FMC_PMEMx_MEMHOLDx_Msk, (0xFF << FMC_PMEMx_MEMHOLDx_Pos) //!< 0x00FF0000 
.equ FMC_PMEMx_MEMHOLDx, FMC_PMEMx_MEMHOLDx_Msk //!<MEMHOLDx[7:0] bits (Common memory x hold time) 
.equ FMC_PMEMx_MEMHOLDx_0, (0x01 << FMC_PMEMx_MEMHOLDx_Pos) //!< 0x00010000 
.equ FMC_PMEMx_MEMHOLDx_1, (0x02 << FMC_PMEMx_MEMHOLDx_Pos) //!< 0x00020000 
.equ FMC_PMEMx_MEMHOLDx_2, (0x04 << FMC_PMEMx_MEMHOLDx_Pos) //!< 0x00040000 
.equ FMC_PMEMx_MEMHOLDx_3, (0x08 << FMC_PMEMx_MEMHOLDx_Pos) //!< 0x00080000 
.equ FMC_PMEMx_MEMHOLDx_4, (0x10 << FMC_PMEMx_MEMHOLDx_Pos) //!< 0x00100000 
.equ FMC_PMEMx_MEMHOLDx_5, (0x20 << FMC_PMEMx_MEMHOLDx_Pos) //!< 0x00200000 
.equ FMC_PMEMx_MEMHOLDx_6, (0x40 << FMC_PMEMx_MEMHOLDx_Pos) //!< 0x00400000 
.equ FMC_PMEMx_MEMHOLDx_7, (0x80 << FMC_PMEMx_MEMHOLDx_Pos) //!< 0x00800000 
.equ FMC_PMEMx_MEMHIZx_Pos, (24) 
.equ FMC_PMEMx_MEMHIZx_Msk, (0xFF << FMC_PMEMx_MEMHIZx_Pos) //!< 0xFF000000 
.equ FMC_PMEMx_MEMHIZx, FMC_PMEMx_MEMHIZx_Msk //!<MEMHIZx[7:0] bits (Common memory x databus HiZ time) 
.equ FMC_PMEMx_MEMHIZx_0, (0x01 << FMC_PMEMx_MEMHIZx_Pos) //!< 0x01000000 
.equ FMC_PMEMx_MEMHIZx_1, (0x02 << FMC_PMEMx_MEMHIZx_Pos) //!< 0x02000000 
.equ FMC_PMEMx_MEMHIZx_2, (0x04 << FMC_PMEMx_MEMHIZx_Pos) //!< 0x04000000 
.equ FMC_PMEMx_MEMHIZx_3, (0x08 << FMC_PMEMx_MEMHIZx_Pos) //!< 0x08000000 
.equ FMC_PMEMx_MEMHIZx_4, (0x10 << FMC_PMEMx_MEMHIZx_Pos) //!< 0x10000000 
.equ FMC_PMEMx_MEMHIZx_5, (0x20 << FMC_PMEMx_MEMHIZx_Pos) //!< 0x20000000 
.equ FMC_PMEMx_MEMHIZx_6, (0x40 << FMC_PMEMx_MEMHIZx_Pos) //!< 0x40000000 
.equ FMC_PMEMx_MEMHIZx_7, (0x80 << FMC_PMEMx_MEMHIZx_Pos) //!< 0x80000000 
//*****************  Bit definition for FMC_PMEM2 register  *****************
.equ FMC_PMEM2_MEMSET2_Pos, (0) 
.equ FMC_PMEM2_MEMSET2_Msk, (0xFF << FMC_PMEM2_MEMSET2_Pos) //!< 0x000000FF 
.equ FMC_PMEM2_MEMSET2, FMC_PMEM2_MEMSET2_Msk //!<MEMSET2[7:0] bits (Common memory 2 setup time) 
.equ FMC_PMEM2_MEMSET2_0, (0x01 << FMC_PMEM2_MEMSET2_Pos) //!< 0x00000001 
.equ FMC_PMEM2_MEMSET2_1, (0x02 << FMC_PMEM2_MEMSET2_Pos) //!< 0x00000002 
.equ FMC_PMEM2_MEMSET2_2, (0x04 << FMC_PMEM2_MEMSET2_Pos) //!< 0x00000004 
.equ FMC_PMEM2_MEMSET2_3, (0x08 << FMC_PMEM2_MEMSET2_Pos) //!< 0x00000008 
.equ FMC_PMEM2_MEMSET2_4, (0x10 << FMC_PMEM2_MEMSET2_Pos) //!< 0x00000010 
.equ FMC_PMEM2_MEMSET2_5, (0x20 << FMC_PMEM2_MEMSET2_Pos) //!< 0x00000020 
.equ FMC_PMEM2_MEMSET2_6, (0x40 << FMC_PMEM2_MEMSET2_Pos) //!< 0x00000040 
.equ FMC_PMEM2_MEMSET2_7, (0x80 << FMC_PMEM2_MEMSET2_Pos) //!< 0x00000080 
.equ FMC_PMEM2_MEMWAIT2_Pos, (8) 
.equ FMC_PMEM2_MEMWAIT2_Msk, (0xFF << FMC_PMEM2_MEMWAIT2_Pos) //!< 0x0000FF00 
.equ FMC_PMEM2_MEMWAIT2, FMC_PMEM2_MEMWAIT2_Msk //!<MEMWAIT2[7:0] bits (Common memory 2 wait time) 
.equ FMC_PMEM2_MEMWAIT2_0, (0x01 << FMC_PMEM2_MEMWAIT2_Pos) //!< 0x00000100 
.equ FMC_PMEM2_MEMWAIT2_1, (0x02 << FMC_PMEM2_MEMWAIT2_Pos) //!< 0x00000200 
.equ FMC_PMEM2_MEMWAIT2_2, (0x04 << FMC_PMEM2_MEMWAIT2_Pos) //!< 0x00000400 
.equ FMC_PMEM2_MEMWAIT2_3, (0x08 << FMC_PMEM2_MEMWAIT2_Pos) //!< 0x00000800 
.equ FMC_PMEM2_MEMWAIT2_4, (0x10 << FMC_PMEM2_MEMWAIT2_Pos) //!< 0x00001000 
.equ FMC_PMEM2_MEMWAIT2_5, (0x20 << FMC_PMEM2_MEMWAIT2_Pos) //!< 0x00002000 
.equ FMC_PMEM2_MEMWAIT2_6, (0x40 << FMC_PMEM2_MEMWAIT2_Pos) //!< 0x00004000 
.equ FMC_PMEM2_MEMWAIT2_7, (0x80 << FMC_PMEM2_MEMWAIT2_Pos) //!< 0x00008000 
.equ FMC_PMEM2_MEMHOLD2_Pos, (16) 
.equ FMC_PMEM2_MEMHOLD2_Msk, (0xFF << FMC_PMEM2_MEMHOLD2_Pos) //!< 0x00FF0000 
.equ FMC_PMEM2_MEMHOLD2, FMC_PMEM2_MEMHOLD2_Msk //!<MEMHOLD2[7:0] bits (Common memory 2 hold time) 
.equ FMC_PMEM2_MEMHOLD2_0, (0x01 << FMC_PMEM2_MEMHOLD2_Pos) //!< 0x00010000 
.equ FMC_PMEM2_MEMHOLD2_1, (0x02 << FMC_PMEM2_MEMHOLD2_Pos) //!< 0x00020000 
.equ FMC_PMEM2_MEMHOLD2_2, (0x04 << FMC_PMEM2_MEMHOLD2_Pos) //!< 0x00040000 
.equ FMC_PMEM2_MEMHOLD2_3, (0x08 << FMC_PMEM2_MEMHOLD2_Pos) //!< 0x00080000 
.equ FMC_PMEM2_MEMHOLD2_4, (0x10 << FMC_PMEM2_MEMHOLD2_Pos) //!< 0x00100000 
.equ FMC_PMEM2_MEMHOLD2_5, (0x20 << FMC_PMEM2_MEMHOLD2_Pos) //!< 0x00200000 
.equ FMC_PMEM2_MEMHOLD2_6, (0x40 << FMC_PMEM2_MEMHOLD2_Pos) //!< 0x00400000 
.equ FMC_PMEM2_MEMHOLD2_7, (0x80 << FMC_PMEM2_MEMHOLD2_Pos) //!< 0x00800000 
.equ FMC_PMEM2_MEMHIZ2_Pos, (24) 
.equ FMC_PMEM2_MEMHIZ2_Msk, (0xFF << FMC_PMEM2_MEMHIZ2_Pos) //!< 0xFF000000 
.equ FMC_PMEM2_MEMHIZ2, FMC_PMEM2_MEMHIZ2_Msk //!<MEMHIZ2[7:0] bits (Common memory 2 databus HiZ time) 
.equ FMC_PMEM2_MEMHIZ2_0, (0x01 << FMC_PMEM2_MEMHIZ2_Pos) //!< 0x01000000 
.equ FMC_PMEM2_MEMHIZ2_1, (0x02 << FMC_PMEM2_MEMHIZ2_Pos) //!< 0x02000000 
.equ FMC_PMEM2_MEMHIZ2_2, (0x04 << FMC_PMEM2_MEMHIZ2_Pos) //!< 0x04000000 
.equ FMC_PMEM2_MEMHIZ2_3, (0x08 << FMC_PMEM2_MEMHIZ2_Pos) //!< 0x08000000 
.equ FMC_PMEM2_MEMHIZ2_4, (0x10 << FMC_PMEM2_MEMHIZ2_Pos) //!< 0x10000000 
.equ FMC_PMEM2_MEMHIZ2_5, (0x20 << FMC_PMEM2_MEMHIZ2_Pos) //!< 0x20000000 
.equ FMC_PMEM2_MEMHIZ2_6, (0x40 << FMC_PMEM2_MEMHIZ2_Pos) //!< 0x40000000 
.equ FMC_PMEM2_MEMHIZ2_7, (0x80 << FMC_PMEM2_MEMHIZ2_Pos) //!< 0x80000000 
//*****************  Bit definition for FMC_PMEM3 register  *****************
.equ FMC_PMEM3_MEMSET3_Pos, (0) 
.equ FMC_PMEM3_MEMSET3_Msk, (0xFF << FMC_PMEM3_MEMSET3_Pos) //!< 0x000000FF 
.equ FMC_PMEM3_MEMSET3, FMC_PMEM3_MEMSET3_Msk //!<MEMSET3[7:0] bits (Common memory 3 setup time) 
.equ FMC_PMEM3_MEMSET3_0, (0x01 << FMC_PMEM3_MEMSET3_Pos) //!< 0x00000001 
.equ FMC_PMEM3_MEMSET3_1, (0x02 << FMC_PMEM3_MEMSET3_Pos) //!< 0x00000002 
.equ FMC_PMEM3_MEMSET3_2, (0x04 << FMC_PMEM3_MEMSET3_Pos) //!< 0x00000004 
.equ FMC_PMEM3_MEMSET3_3, (0x08 << FMC_PMEM3_MEMSET3_Pos) //!< 0x00000008 
.equ FMC_PMEM3_MEMSET3_4, (0x10 << FMC_PMEM3_MEMSET3_Pos) //!< 0x00000010 
.equ FMC_PMEM3_MEMSET3_5, (0x20 << FMC_PMEM3_MEMSET3_Pos) //!< 0x00000020 
.equ FMC_PMEM3_MEMSET3_6, (0x40 << FMC_PMEM3_MEMSET3_Pos) //!< 0x00000040 
.equ FMC_PMEM3_MEMSET3_7, (0x80 << FMC_PMEM3_MEMSET3_Pos) //!< 0x00000080 
.equ FMC_PMEM3_MEMWAIT3_Pos, (8) 
.equ FMC_PMEM3_MEMWAIT3_Msk, (0xFF << FMC_PMEM3_MEMWAIT3_Pos) //!< 0x0000FF00 
.equ FMC_PMEM3_MEMWAIT3, FMC_PMEM3_MEMWAIT3_Msk //!<MEMWAIT3[7:0] bits (Common memory 3 wait time) 
.equ FMC_PMEM3_MEMWAIT3_0, (0x01 << FMC_PMEM3_MEMWAIT3_Pos) //!< 0x00000100 
.equ FMC_PMEM3_MEMWAIT3_1, (0x02 << FMC_PMEM3_MEMWAIT3_Pos) //!< 0x00000200 
.equ FMC_PMEM3_MEMWAIT3_2, (0x04 << FMC_PMEM3_MEMWAIT3_Pos) //!< 0x00000400 
.equ FMC_PMEM3_MEMWAIT3_3, (0x08 << FMC_PMEM3_MEMWAIT3_Pos) //!< 0x00000800 
.equ FMC_PMEM3_MEMWAIT3_4, (0x10 << FMC_PMEM3_MEMWAIT3_Pos) //!< 0x00001000 
.equ FMC_PMEM3_MEMWAIT3_5, (0x20 << FMC_PMEM3_MEMWAIT3_Pos) //!< 0x00002000 
.equ FMC_PMEM3_MEMWAIT3_6, (0x40 << FMC_PMEM3_MEMWAIT3_Pos) //!< 0x00004000 
.equ FMC_PMEM3_MEMWAIT3_7, (0x80 << FMC_PMEM3_MEMWAIT3_Pos) //!< 0x00008000 
.equ FMC_PMEM3_MEMHOLD3_Pos, (16) 
.equ FMC_PMEM3_MEMHOLD3_Msk, (0xFF << FMC_PMEM3_MEMHOLD3_Pos) //!< 0x00FF0000 
.equ FMC_PMEM3_MEMHOLD3, FMC_PMEM3_MEMHOLD3_Msk //!<MEMHOLD3[7:0] bits (Common memory 3 hold time) 
.equ FMC_PMEM3_MEMHOLD3_0, (0x01 << FMC_PMEM3_MEMHOLD3_Pos) //!< 0x00010000 
.equ FMC_PMEM3_MEMHOLD3_1, (0x02 << FMC_PMEM3_MEMHOLD3_Pos) //!< 0x00020000 
.equ FMC_PMEM3_MEMHOLD3_2, (0x04 << FMC_PMEM3_MEMHOLD3_Pos) //!< 0x00040000 
.equ FMC_PMEM3_MEMHOLD3_3, (0x08 << FMC_PMEM3_MEMHOLD3_Pos) //!< 0x00080000 
.equ FMC_PMEM3_MEMHOLD3_4, (0x10 << FMC_PMEM3_MEMHOLD3_Pos) //!< 0x00100000 
.equ FMC_PMEM3_MEMHOLD3_5, (0x20 << FMC_PMEM3_MEMHOLD3_Pos) //!< 0x00200000 
.equ FMC_PMEM3_MEMHOLD3_6, (0x40 << FMC_PMEM3_MEMHOLD3_Pos) //!< 0x00400000 
.equ FMC_PMEM3_MEMHOLD3_7, (0x80 << FMC_PMEM3_MEMHOLD3_Pos) //!< 0x00800000 
.equ FMC_PMEM3_MEMHIZ3_Pos, (24) 
.equ FMC_PMEM3_MEMHIZ3_Msk, (0xFF << FMC_PMEM3_MEMHIZ3_Pos) //!< 0xFF000000 
.equ FMC_PMEM3_MEMHIZ3, FMC_PMEM3_MEMHIZ3_Msk //!<MEMHIZ3[7:0] bits (Common memory 3 databus HiZ time) 
.equ FMC_PMEM3_MEMHIZ3_0, (0x01 << FMC_PMEM3_MEMHIZ3_Pos) //!< 0x01000000 
.equ FMC_PMEM3_MEMHIZ3_1, (0x02 << FMC_PMEM3_MEMHIZ3_Pos) //!< 0x02000000 
.equ FMC_PMEM3_MEMHIZ3_2, (0x04 << FMC_PMEM3_MEMHIZ3_Pos) //!< 0x04000000 
.equ FMC_PMEM3_MEMHIZ3_3, (0x08 << FMC_PMEM3_MEMHIZ3_Pos) //!< 0x08000000 
.equ FMC_PMEM3_MEMHIZ3_4, (0x10 << FMC_PMEM3_MEMHIZ3_Pos) //!< 0x10000000 
.equ FMC_PMEM3_MEMHIZ3_5, (0x20 << FMC_PMEM3_MEMHIZ3_Pos) //!< 0x20000000 
.equ FMC_PMEM3_MEMHIZ3_6, (0x40 << FMC_PMEM3_MEMHIZ3_Pos) //!< 0x40000000 
.equ FMC_PMEM3_MEMHIZ3_7, (0x80 << FMC_PMEM3_MEMHIZ3_Pos) //!< 0x80000000 
//*****************  Bit definition for FMC_PMEM4 register  *****************
.equ FMC_PMEM4_MEMSET4_Pos, (0) 
.equ FMC_PMEM4_MEMSET4_Msk, (0xFF << FMC_PMEM4_MEMSET4_Pos) //!< 0x000000FF 
.equ FMC_PMEM4_MEMSET4, FMC_PMEM4_MEMSET4_Msk //!<MEMSET4[7:0] bits (Common memory 4 setup time) 
.equ FMC_PMEM4_MEMSET4_0, (0x01 << FMC_PMEM4_MEMSET4_Pos) //!< 0x00000001 
.equ FMC_PMEM4_MEMSET4_1, (0x02 << FMC_PMEM4_MEMSET4_Pos) //!< 0x00000002 
.equ FMC_PMEM4_MEMSET4_2, (0x04 << FMC_PMEM4_MEMSET4_Pos) //!< 0x00000004 
.equ FMC_PMEM4_MEMSET4_3, (0x08 << FMC_PMEM4_MEMSET4_Pos) //!< 0x00000008 
.equ FMC_PMEM4_MEMSET4_4, (0x10 << FMC_PMEM4_MEMSET4_Pos) //!< 0x00000010 
.equ FMC_PMEM4_MEMSET4_5, (0x20 << FMC_PMEM4_MEMSET4_Pos) //!< 0x00000020 
.equ FMC_PMEM4_MEMSET4_6, (0x40 << FMC_PMEM4_MEMSET4_Pos) //!< 0x00000040 
.equ FMC_PMEM4_MEMSET4_7, (0x80 << FMC_PMEM4_MEMSET4_Pos) //!< 0x00000080 
.equ FMC_PMEM4_MEMWAIT4_Pos, (8) 
.equ FMC_PMEM4_MEMWAIT4_Msk, (0xFF << FMC_PMEM4_MEMWAIT4_Pos) //!< 0x0000FF00 
.equ FMC_PMEM4_MEMWAIT4, FMC_PMEM4_MEMWAIT4_Msk //!<MEMWAIT4[7:0] bits (Common memory 4 wait time) 
.equ FMC_PMEM4_MEMWAIT4_0, (0x01 << FMC_PMEM4_MEMWAIT4_Pos) //!< 0x00000100 
.equ FMC_PMEM4_MEMWAIT4_1, (0x02 << FMC_PMEM4_MEMWAIT4_Pos) //!< 0x00000200 
.equ FMC_PMEM4_MEMWAIT4_2, (0x04 << FMC_PMEM4_MEMWAIT4_Pos) //!< 0x00000400 
.equ FMC_PMEM4_MEMWAIT4_3, (0x08 << FMC_PMEM4_MEMWAIT4_Pos) //!< 0x00000800 
.equ FMC_PMEM4_MEMWAIT4_4, (0x10 << FMC_PMEM4_MEMWAIT4_Pos) //!< 0x00001000 
.equ FMC_PMEM4_MEMWAIT4_5, (0x20 << FMC_PMEM4_MEMWAIT4_Pos) //!< 0x00002000 
.equ FMC_PMEM4_MEMWAIT4_6, (0x40 << FMC_PMEM4_MEMWAIT4_Pos) //!< 0x00004000 
.equ FMC_PMEM4_MEMWAIT4_7, (0x80 << FMC_PMEM4_MEMWAIT4_Pos) //!< 0x00008000 
.equ FMC_PMEM4_MEMHOLD4_Pos, (16) 
.equ FMC_PMEM4_MEMHOLD4_Msk, (0xFF << FMC_PMEM4_MEMHOLD4_Pos) //!< 0x00FF0000 
.equ FMC_PMEM4_MEMHOLD4, FMC_PMEM4_MEMHOLD4_Msk //!<MEMHOLD4[7:0] bits (Common memory 4 hold time) 
.equ FMC_PMEM4_MEMHOLD4_0, (0x01 << FMC_PMEM4_MEMHOLD4_Pos) //!< 0x00010000 
.equ FMC_PMEM4_MEMHOLD4_1, (0x02 << FMC_PMEM4_MEMHOLD4_Pos) //!< 0x00020000 
.equ FMC_PMEM4_MEMHOLD4_2, (0x04 << FMC_PMEM4_MEMHOLD4_Pos) //!< 0x00040000 
.equ FMC_PMEM4_MEMHOLD4_3, (0x08 << FMC_PMEM4_MEMHOLD4_Pos) //!< 0x00080000 
.equ FMC_PMEM4_MEMHOLD4_4, (0x10 << FMC_PMEM4_MEMHOLD4_Pos) //!< 0x00100000 
.equ FMC_PMEM4_MEMHOLD4_5, (0x20 << FMC_PMEM4_MEMHOLD4_Pos) //!< 0x00200000 
.equ FMC_PMEM4_MEMHOLD4_6, (0x40 << FMC_PMEM4_MEMHOLD4_Pos) //!< 0x00400000 
.equ FMC_PMEM4_MEMHOLD4_7, (0x80 << FMC_PMEM4_MEMHOLD4_Pos) //!< 0x00800000 
.equ FMC_PMEM4_MEMHIZ4_Pos, (24) 
.equ FMC_PMEM4_MEMHIZ4_Msk, (0xFF << FMC_PMEM4_MEMHIZ4_Pos) //!< 0xFF000000 
.equ FMC_PMEM4_MEMHIZ4, FMC_PMEM4_MEMHIZ4_Msk //!<MEMHIZ4[7:0] bits (Common memory 4 databus HiZ time) 
.equ FMC_PMEM4_MEMHIZ4_0, (0x01 << FMC_PMEM4_MEMHIZ4_Pos) //!< 0x01000000 
.equ FMC_PMEM4_MEMHIZ4_1, (0x02 << FMC_PMEM4_MEMHIZ4_Pos) //!< 0x02000000 
.equ FMC_PMEM4_MEMHIZ4_2, (0x04 << FMC_PMEM4_MEMHIZ4_Pos) //!< 0x04000000 
.equ FMC_PMEM4_MEMHIZ4_3, (0x08 << FMC_PMEM4_MEMHIZ4_Pos) //!< 0x08000000 
.equ FMC_PMEM4_MEMHIZ4_4, (0x10 << FMC_PMEM4_MEMHIZ4_Pos) //!< 0x10000000 
.equ FMC_PMEM4_MEMHIZ4_5, (0x20 << FMC_PMEM4_MEMHIZ4_Pos) //!< 0x20000000 
.equ FMC_PMEM4_MEMHIZ4_6, (0x40 << FMC_PMEM4_MEMHIZ4_Pos) //!< 0x40000000 
.equ FMC_PMEM4_MEMHIZ4_7, (0x80 << FMC_PMEM4_MEMHIZ4_Pos) //!< 0x80000000 
//*****************  Bit definition for FMC_PATTx register  *****************
.equ FMC_PATTx_ATTSETx_Pos, (0) 
.equ FMC_PATTx_ATTSETx_Msk, (0xFF << FMC_PATTx_ATTSETx_Pos) //!< 0x000000FF 
.equ FMC_PATTx_ATTSETx, FMC_PATTx_ATTSETx_Msk //!<ATTSETx[7:0] bits (Attribute memory x setup time) 
.equ FMC_PATTx_ATTSETx_0, (0x01 << FMC_PATTx_ATTSETx_Pos) //!< 0x00000001 
.equ FMC_PATTx_ATTSETx_1, (0x02 << FMC_PATTx_ATTSETx_Pos) //!< 0x00000002 
.equ FMC_PATTx_ATTSETx_2, (0x04 << FMC_PATTx_ATTSETx_Pos) //!< 0x00000004 
.equ FMC_PATTx_ATTSETx_3, (0x08 << FMC_PATTx_ATTSETx_Pos) //!< 0x00000008 
.equ FMC_PATTx_ATTSETx_4, (0x10 << FMC_PATTx_ATTSETx_Pos) //!< 0x00000010 
.equ FMC_PATTx_ATTSETx_5, (0x20 << FMC_PATTx_ATTSETx_Pos) //!< 0x00000020 
.equ FMC_PATTx_ATTSETx_6, (0x40 << FMC_PATTx_ATTSETx_Pos) //!< 0x00000040 
.equ FMC_PATTx_ATTSETx_7, (0x80 << FMC_PATTx_ATTSETx_Pos) //!< 0x00000080 
.equ FMC_PATTx_ATTWAITx_Pos, (8) 
.equ FMC_PATTx_ATTWAITx_Msk, (0xFF << FMC_PATTx_ATTWAITx_Pos) //!< 0x0000FF00 
.equ FMC_PATTx_ATTWAITx, FMC_PATTx_ATTWAITx_Msk //!<ATTWAITx[7:0] bits (Attribute memory x wait time) 
.equ FMC_PATTx_ATTWAITx_0, (0x01 << FMC_PATTx_ATTWAITx_Pos) //!< 0x00000100 
.equ FMC_PATTx_ATTWAITx_1, (0x02 << FMC_PATTx_ATTWAITx_Pos) //!< 0x00000200 
.equ FMC_PATTx_ATTWAITx_2, (0x04 << FMC_PATTx_ATTWAITx_Pos) //!< 0x00000400 
.equ FMC_PATTx_ATTWAITx_3, (0x08 << FMC_PATTx_ATTWAITx_Pos) //!< 0x00000800 
.equ FMC_PATTx_ATTWAITx_4, (0x10 << FMC_PATTx_ATTWAITx_Pos) //!< 0x00001000 
.equ FMC_PATTx_ATTWAITx_5, (0x20 << FMC_PATTx_ATTWAITx_Pos) //!< 0x00002000 
.equ FMC_PATTx_ATTWAITx_6, (0x40 << FMC_PATTx_ATTWAITx_Pos) //!< 0x00004000 
.equ FMC_PATTx_ATTWAITx_7, (0x80 << FMC_PATTx_ATTWAITx_Pos) //!< 0x00008000 
.equ FMC_PATTx_ATTHOLDx_Pos, (16) 
.equ FMC_PATTx_ATTHOLDx_Msk, (0xFF << FMC_PATTx_ATTHOLDx_Pos) //!< 0x00FF0000 
.equ FMC_PATTx_ATTHOLDx, FMC_PATTx_ATTHOLDx_Msk //!<ATTHOLDx[7:0] bits (Attribute memory x hold time) 
.equ FMC_PATTx_ATTHOLDx_0, (0x01 << FMC_PATTx_ATTHOLDx_Pos) //!< 0x00010000 
.equ FMC_PATTx_ATTHOLDx_1, (0x02 << FMC_PATTx_ATTHOLDx_Pos) //!< 0x00020000 
.equ FMC_PATTx_ATTHOLDx_2, (0x04 << FMC_PATTx_ATTHOLDx_Pos) //!< 0x00040000 
.equ FMC_PATTx_ATTHOLDx_3, (0x08 << FMC_PATTx_ATTHOLDx_Pos) //!< 0x00080000 
.equ FMC_PATTx_ATTHOLDx_4, (0x10 << FMC_PATTx_ATTHOLDx_Pos) //!< 0x00100000 
.equ FMC_PATTx_ATTHOLDx_5, (0x20 << FMC_PATTx_ATTHOLDx_Pos) //!< 0x00200000 
.equ FMC_PATTx_ATTHOLDx_6, (0x40 << FMC_PATTx_ATTHOLDx_Pos) //!< 0x00400000 
.equ FMC_PATTx_ATTHOLDx_7, (0x80 << FMC_PATTx_ATTHOLDx_Pos) //!< 0x00800000 
.equ FMC_PATTx_ATTHIZx_Pos, (24) 
.equ FMC_PATTx_ATTHIZx_Msk, (0xFF << FMC_PATTx_ATTHIZx_Pos) //!< 0xFF000000 
.equ FMC_PATTx_ATTHIZx, FMC_PATTx_ATTHIZx_Msk //!<ATTHIZx[7:0] bits (Attribute memory x databus HiZ time) 
.equ FMC_PATTx_ATTHIZx_0, (0x01 << FMC_PATTx_ATTHIZx_Pos) //!< 0x01000000 
.equ FMC_PATTx_ATTHIZx_1, (0x02 << FMC_PATTx_ATTHIZx_Pos) //!< 0x02000000 
.equ FMC_PATTx_ATTHIZx_2, (0x04 << FMC_PATTx_ATTHIZx_Pos) //!< 0x04000000 
.equ FMC_PATTx_ATTHIZx_3, (0x08 << FMC_PATTx_ATTHIZx_Pos) //!< 0x08000000 
.equ FMC_PATTx_ATTHIZx_4, (0x10 << FMC_PATTx_ATTHIZx_Pos) //!< 0x10000000 
.equ FMC_PATTx_ATTHIZx_5, (0x20 << FMC_PATTx_ATTHIZx_Pos) //!< 0x20000000 
.equ FMC_PATTx_ATTHIZx_6, (0x40 << FMC_PATTx_ATTHIZx_Pos) //!< 0x40000000 
.equ FMC_PATTx_ATTHIZx_7, (0x80 << FMC_PATTx_ATTHIZx_Pos) //!< 0x80000000 
//*****************  Bit definition for FMC_PATT2 register  *****************
.equ FMC_PATT2_ATTSET2_Pos, (0) 
.equ FMC_PATT2_ATTSET2_Msk, (0xFF << FMC_PATT2_ATTSET2_Pos) //!< 0x000000FF 
.equ FMC_PATT2_ATTSET2, FMC_PATT2_ATTSET2_Msk //!<ATTSET2[7:0] bits (Attribute memory 2 setup time) 
.equ FMC_PATT2_ATTSET2_0, (0x01 << FMC_PATT2_ATTSET2_Pos) //!< 0x00000001 
.equ FMC_PATT2_ATTSET2_1, (0x02 << FMC_PATT2_ATTSET2_Pos) //!< 0x00000002 
.equ FMC_PATT2_ATTSET2_2, (0x04 << FMC_PATT2_ATTSET2_Pos) //!< 0x00000004 
.equ FMC_PATT2_ATTSET2_3, (0x08 << FMC_PATT2_ATTSET2_Pos) //!< 0x00000008 
.equ FMC_PATT2_ATTSET2_4, (0x10 << FMC_PATT2_ATTSET2_Pos) //!< 0x00000010 
.equ FMC_PATT2_ATTSET2_5, (0x20 << FMC_PATT2_ATTSET2_Pos) //!< 0x00000020 
.equ FMC_PATT2_ATTSET2_6, (0x40 << FMC_PATT2_ATTSET2_Pos) //!< 0x00000040 
.equ FMC_PATT2_ATTSET2_7, (0x80 << FMC_PATT2_ATTSET2_Pos) //!< 0x00000080 
.equ FMC_PATT2_ATTWAIT2_Pos, (8) 
.equ FMC_PATT2_ATTWAIT2_Msk, (0xFF << FMC_PATT2_ATTWAIT2_Pos) //!< 0x0000FF00 
.equ FMC_PATT2_ATTWAIT2, FMC_PATT2_ATTWAIT2_Msk //!<ATTWAIT2[7:0] bits (Attribute memory 2 wait time) 
.equ FMC_PATT2_ATTWAIT2_0, (0x01 << FMC_PATT2_ATTWAIT2_Pos) //!< 0x00000100 
.equ FMC_PATT2_ATTWAIT2_1, (0x02 << FMC_PATT2_ATTWAIT2_Pos) //!< 0x00000200 
.equ FMC_PATT2_ATTWAIT2_2, (0x04 << FMC_PATT2_ATTWAIT2_Pos) //!< 0x00000400 
.equ FMC_PATT2_ATTWAIT2_3, (0x08 << FMC_PATT2_ATTWAIT2_Pos) //!< 0x00000800 
.equ FMC_PATT2_ATTWAIT2_4, (0x10 << FMC_PATT2_ATTWAIT2_Pos) //!< 0x00001000 
.equ FMC_PATT2_ATTWAIT2_5, (0x20 << FMC_PATT2_ATTWAIT2_Pos) //!< 0x00002000 
.equ FMC_PATT2_ATTWAIT2_6, (0x40 << FMC_PATT2_ATTWAIT2_Pos) //!< 0x00004000 
.equ FMC_PATT2_ATTWAIT2_7, (0x80 << FMC_PATT2_ATTWAIT2_Pos) //!< 0x00008000 
.equ FMC_PATT2_ATTHOLD2_Pos, (16) 
.equ FMC_PATT2_ATTHOLD2_Msk, (0xFF << FMC_PATT2_ATTHOLD2_Pos) //!< 0x00FF0000 
.equ FMC_PATT2_ATTHOLD2, FMC_PATT2_ATTHOLD2_Msk //!<ATTHOLD2[7:0] bits (Attribute memory 2 hold time) 
.equ FMC_PATT2_ATTHOLD2_0, (0x01 << FMC_PATT2_ATTHOLD2_Pos) //!< 0x00010000 
.equ FMC_PATT2_ATTHOLD2_1, (0x02 << FMC_PATT2_ATTHOLD2_Pos) //!< 0x00020000 
.equ FMC_PATT2_ATTHOLD2_2, (0x04 << FMC_PATT2_ATTHOLD2_Pos) //!< 0x00040000 
.equ FMC_PATT2_ATTHOLD2_3, (0x08 << FMC_PATT2_ATTHOLD2_Pos) //!< 0x00080000 
.equ FMC_PATT2_ATTHOLD2_4, (0x10 << FMC_PATT2_ATTHOLD2_Pos) //!< 0x00100000 
.equ FMC_PATT2_ATTHOLD2_5, (0x20 << FMC_PATT2_ATTHOLD2_Pos) //!< 0x00200000 
.equ FMC_PATT2_ATTHOLD2_6, (0x40 << FMC_PATT2_ATTHOLD2_Pos) //!< 0x00400000 
.equ FMC_PATT2_ATTHOLD2_7, (0x80 << FMC_PATT2_ATTHOLD2_Pos) //!< 0x00800000 
.equ FMC_PATT2_ATTHIZ2_Pos, (24) 
.equ FMC_PATT2_ATTHIZ2_Msk, (0xFF << FMC_PATT2_ATTHIZ2_Pos) //!< 0xFF000000 
.equ FMC_PATT2_ATTHIZ2, FMC_PATT2_ATTHIZ2_Msk //!<ATTHIZ2[7:0] bits (Attribute memory 2 databus HiZ time) 
.equ FMC_PATT2_ATTHIZ2_0, (0x01 << FMC_PATT2_ATTHIZ2_Pos) //!< 0x01000000 
.equ FMC_PATT2_ATTHIZ2_1, (0x02 << FMC_PATT2_ATTHIZ2_Pos) //!< 0x02000000 
.equ FMC_PATT2_ATTHIZ2_2, (0x04 << FMC_PATT2_ATTHIZ2_Pos) //!< 0x04000000 
.equ FMC_PATT2_ATTHIZ2_3, (0x08 << FMC_PATT2_ATTHIZ2_Pos) //!< 0x08000000 
.equ FMC_PATT2_ATTHIZ2_4, (0x10 << FMC_PATT2_ATTHIZ2_Pos) //!< 0x10000000 
.equ FMC_PATT2_ATTHIZ2_5, (0x20 << FMC_PATT2_ATTHIZ2_Pos) //!< 0x20000000 
.equ FMC_PATT2_ATTHIZ2_6, (0x40 << FMC_PATT2_ATTHIZ2_Pos) //!< 0x40000000 
.equ FMC_PATT2_ATTHIZ2_7, (0x80 << FMC_PATT2_ATTHIZ2_Pos) //!< 0x80000000 
//*****************  Bit definition for FMC_PATT3 register  *****************
.equ FMC_PATT3_ATTSET3_Pos, (0) 
.equ FMC_PATT3_ATTSET3_Msk, (0xFF << FMC_PATT3_ATTSET3_Pos) //!< 0x000000FF 
.equ FMC_PATT3_ATTSET3, FMC_PATT3_ATTSET3_Msk //!<ATTSET3[7:0] bits (Attribute memory 3 setup time) 
.equ FMC_PATT3_ATTSET3_0, (0x01 << FMC_PATT3_ATTSET3_Pos) //!< 0x00000001 
.equ FMC_PATT3_ATTSET3_1, (0x02 << FMC_PATT3_ATTSET3_Pos) //!< 0x00000002 
.equ FMC_PATT3_ATTSET3_2, (0x04 << FMC_PATT3_ATTSET3_Pos) //!< 0x00000004 
.equ FMC_PATT3_ATTSET3_3, (0x08 << FMC_PATT3_ATTSET3_Pos) //!< 0x00000008 
.equ FMC_PATT3_ATTSET3_4, (0x10 << FMC_PATT3_ATTSET3_Pos) //!< 0x00000010 
.equ FMC_PATT3_ATTSET3_5, (0x20 << FMC_PATT3_ATTSET3_Pos) //!< 0x00000020 
.equ FMC_PATT3_ATTSET3_6, (0x40 << FMC_PATT3_ATTSET3_Pos) //!< 0x00000040 
.equ FMC_PATT3_ATTSET3_7, (0x80 << FMC_PATT3_ATTSET3_Pos) //!< 0x00000080 
.equ FMC_PATT3_ATTWAIT3_Pos, (8) 
.equ FMC_PATT3_ATTWAIT3_Msk, (0xFF << FMC_PATT3_ATTWAIT3_Pos) //!< 0x0000FF00 
.equ FMC_PATT3_ATTWAIT3, FMC_PATT3_ATTWAIT3_Msk //!<ATTWAIT3[7:0] bits (Attribute memory 3 wait time) 
.equ FMC_PATT3_ATTWAIT3_0, (0x01 << FMC_PATT3_ATTWAIT3_Pos) //!< 0x00000100 
.equ FMC_PATT3_ATTWAIT3_1, (0x02 << FMC_PATT3_ATTWAIT3_Pos) //!< 0x00000200 
.equ FMC_PATT3_ATTWAIT3_2, (0x04 << FMC_PATT3_ATTWAIT3_Pos) //!< 0x00000400 
.equ FMC_PATT3_ATTWAIT3_3, (0x08 << FMC_PATT3_ATTWAIT3_Pos) //!< 0x00000800 
.equ FMC_PATT3_ATTWAIT3_4, (0x10 << FMC_PATT3_ATTWAIT3_Pos) //!< 0x00001000 
.equ FMC_PATT3_ATTWAIT3_5, (0x20 << FMC_PATT3_ATTWAIT3_Pos) //!< 0x00002000 
.equ FMC_PATT3_ATTWAIT3_6, (0x40 << FMC_PATT3_ATTWAIT3_Pos) //!< 0x00004000 
.equ FMC_PATT3_ATTWAIT3_7, (0x80 << FMC_PATT3_ATTWAIT3_Pos) //!< 0x00008000 
.equ FMC_PATT3_ATTHOLD3_Pos, (16) 
.equ FMC_PATT3_ATTHOLD3_Msk, (0xFF << FMC_PATT3_ATTHOLD3_Pos) //!< 0x00FF0000 
.equ FMC_PATT3_ATTHOLD3, FMC_PATT3_ATTHOLD3_Msk //!<ATTHOLD3[7:0] bits (Attribute memory 3 hold time) 
.equ FMC_PATT3_ATTHOLD3_0, (0x01 << FMC_PATT3_ATTHOLD3_Pos) //!< 0x00010000 
.equ FMC_PATT3_ATTHOLD3_1, (0x02 << FMC_PATT3_ATTHOLD3_Pos) //!< 0x00020000 
.equ FMC_PATT3_ATTHOLD3_2, (0x04 << FMC_PATT3_ATTHOLD3_Pos) //!< 0x00040000 
.equ FMC_PATT3_ATTHOLD3_3, (0x08 << FMC_PATT3_ATTHOLD3_Pos) //!< 0x00080000 
.equ FMC_PATT3_ATTHOLD3_4, (0x10 << FMC_PATT3_ATTHOLD3_Pos) //!< 0x00100000 
.equ FMC_PATT3_ATTHOLD3_5, (0x20 << FMC_PATT3_ATTHOLD3_Pos) //!< 0x00200000 
.equ FMC_PATT3_ATTHOLD3_6, (0x40 << FMC_PATT3_ATTHOLD3_Pos) //!< 0x00400000 
.equ FMC_PATT3_ATTHOLD3_7, (0x80 << FMC_PATT3_ATTHOLD3_Pos) //!< 0x00800000 
.equ FMC_PATT3_ATTHIZ3_Pos, (24) 
.equ FMC_PATT3_ATTHIZ3_Msk, (0xFF << FMC_PATT3_ATTHIZ3_Pos) //!< 0xFF000000 
.equ FMC_PATT3_ATTHIZ3, FMC_PATT3_ATTHIZ3_Msk //!<ATTHIZ3[7:0] bits (Attribute memory 3 databus HiZ time) 
.equ FMC_PATT3_ATTHIZ3_0, (0x01 << FMC_PATT3_ATTHIZ3_Pos) //!< 0x01000000 
.equ FMC_PATT3_ATTHIZ3_1, (0x02 << FMC_PATT3_ATTHIZ3_Pos) //!< 0x02000000 
.equ FMC_PATT3_ATTHIZ3_2, (0x04 << FMC_PATT3_ATTHIZ3_Pos) //!< 0x04000000 
.equ FMC_PATT3_ATTHIZ3_3, (0x08 << FMC_PATT3_ATTHIZ3_Pos) //!< 0x08000000 
.equ FMC_PATT3_ATTHIZ3_4, (0x10 << FMC_PATT3_ATTHIZ3_Pos) //!< 0x10000000 
.equ FMC_PATT3_ATTHIZ3_5, (0x20 << FMC_PATT3_ATTHIZ3_Pos) //!< 0x20000000 
.equ FMC_PATT3_ATTHIZ3_6, (0x40 << FMC_PATT3_ATTHIZ3_Pos) //!< 0x40000000 
.equ FMC_PATT3_ATTHIZ3_7, (0x80 << FMC_PATT3_ATTHIZ3_Pos) //!< 0x80000000 
//*****************  Bit definition for FMC_PATT4 register  *****************
.equ FMC_PATT4_ATTSET4_Pos, (0) 
.equ FMC_PATT4_ATTSET4_Msk, (0xFF << FMC_PATT4_ATTSET4_Pos) //!< 0x000000FF 
.equ FMC_PATT4_ATTSET4, FMC_PATT4_ATTSET4_Msk //!<ATTSET4[7:0] bits (Attribute memory 4 setup time) 
.equ FMC_PATT4_ATTSET4_0, (0x01 << FMC_PATT4_ATTSET4_Pos) //!< 0x00000001 
.equ FMC_PATT4_ATTSET4_1, (0x02 << FMC_PATT4_ATTSET4_Pos) //!< 0x00000002 
.equ FMC_PATT4_ATTSET4_2, (0x04 << FMC_PATT4_ATTSET4_Pos) //!< 0x00000004 
.equ FMC_PATT4_ATTSET4_3, (0x08 << FMC_PATT4_ATTSET4_Pos) //!< 0x00000008 
.equ FMC_PATT4_ATTSET4_4, (0x10 << FMC_PATT4_ATTSET4_Pos) //!< 0x00000010 
.equ FMC_PATT4_ATTSET4_5, (0x20 << FMC_PATT4_ATTSET4_Pos) //!< 0x00000020 
.equ FMC_PATT4_ATTSET4_6, (0x40 << FMC_PATT4_ATTSET4_Pos) //!< 0x00000040 
.equ FMC_PATT4_ATTSET4_7, (0x80 << FMC_PATT4_ATTSET4_Pos) //!< 0x00000080 
.equ FMC_PATT4_ATTWAIT4_Pos, (8) 
.equ FMC_PATT4_ATTWAIT4_Msk, (0xFF << FMC_PATT4_ATTWAIT4_Pos) //!< 0x0000FF00 
.equ FMC_PATT4_ATTWAIT4, FMC_PATT4_ATTWAIT4_Msk //!<ATTWAIT4[7:0] bits (Attribute memory 4 wait time) 
.equ FMC_PATT4_ATTWAIT4_0, (0x01 << FMC_PATT4_ATTWAIT4_Pos) //!< 0x00000100 
.equ FMC_PATT4_ATTWAIT4_1, (0x02 << FMC_PATT4_ATTWAIT4_Pos) //!< 0x00000200 
.equ FMC_PATT4_ATTWAIT4_2, (0x04 << FMC_PATT4_ATTWAIT4_Pos) //!< 0x00000400 
.equ FMC_PATT4_ATTWAIT4_3, (0x08 << FMC_PATT4_ATTWAIT4_Pos) //!< 0x00000800 
.equ FMC_PATT4_ATTWAIT4_4, (0x10 << FMC_PATT4_ATTWAIT4_Pos) //!< 0x00001000 
.equ FMC_PATT4_ATTWAIT4_5, (0x20 << FMC_PATT4_ATTWAIT4_Pos) //!< 0x00002000 
.equ FMC_PATT4_ATTWAIT4_6, (0x40 << FMC_PATT4_ATTWAIT4_Pos) //!< 0x00004000 
.equ FMC_PATT4_ATTWAIT4_7, (0x80 << FMC_PATT4_ATTWAIT4_Pos) //!< 0x00008000 
.equ FMC_PATT4_ATTHOLD4_Pos, (16) 
.equ FMC_PATT4_ATTHOLD4_Msk, (0xFF << FMC_PATT4_ATTHOLD4_Pos) //!< 0x00FF0000 
.equ FMC_PATT4_ATTHOLD4, FMC_PATT4_ATTHOLD4_Msk //!<ATTHOLD4[7:0] bits (Attribute memory 4 hold time) 
.equ FMC_PATT4_ATTHOLD4_0, (0x01 << FMC_PATT4_ATTHOLD4_Pos) //!< 0x00010000 
.equ FMC_PATT4_ATTHOLD4_1, (0x02 << FMC_PATT4_ATTHOLD4_Pos) //!< 0x00020000 
.equ FMC_PATT4_ATTHOLD4_2, (0x04 << FMC_PATT4_ATTHOLD4_Pos) //!< 0x00040000 
.equ FMC_PATT4_ATTHOLD4_3, (0x08 << FMC_PATT4_ATTHOLD4_Pos) //!< 0x00080000 
.equ FMC_PATT4_ATTHOLD4_4, (0x10 << FMC_PATT4_ATTHOLD4_Pos) //!< 0x00100000 
.equ FMC_PATT4_ATTHOLD4_5, (0x20 << FMC_PATT4_ATTHOLD4_Pos) //!< 0x00200000 
.equ FMC_PATT4_ATTHOLD4_6, (0x40 << FMC_PATT4_ATTHOLD4_Pos) //!< 0x00400000 
.equ FMC_PATT4_ATTHOLD4_7, (0x80 << FMC_PATT4_ATTHOLD4_Pos) //!< 0x00800000 
.equ FMC_PATT4_ATTHIZ4_Pos, (24) 
.equ FMC_PATT4_ATTHIZ4_Msk, (0xFF << FMC_PATT4_ATTHIZ4_Pos) //!< 0xFF000000 
.equ FMC_PATT4_ATTHIZ4, FMC_PATT4_ATTHIZ4_Msk //!<ATTHIZ4[7:0] bits (Attribute memory 4 databus HiZ time) 
.equ FMC_PATT4_ATTHIZ4_0, (0x01 << FMC_PATT4_ATTHIZ4_Pos) //!< 0x01000000 
.equ FMC_PATT4_ATTHIZ4_1, (0x02 << FMC_PATT4_ATTHIZ4_Pos) //!< 0x02000000 
.equ FMC_PATT4_ATTHIZ4_2, (0x04 << FMC_PATT4_ATTHIZ4_Pos) //!< 0x04000000 
.equ FMC_PATT4_ATTHIZ4_3, (0x08 << FMC_PATT4_ATTHIZ4_Pos) //!< 0x08000000 
.equ FMC_PATT4_ATTHIZ4_4, (0x10 << FMC_PATT4_ATTHIZ4_Pos) //!< 0x10000000 
.equ FMC_PATT4_ATTHIZ4_5, (0x20 << FMC_PATT4_ATTHIZ4_Pos) //!< 0x20000000 
.equ FMC_PATT4_ATTHIZ4_6, (0x40 << FMC_PATT4_ATTHIZ4_Pos) //!< 0x40000000 
.equ FMC_PATT4_ATTHIZ4_7, (0x80 << FMC_PATT4_ATTHIZ4_Pos) //!< 0x80000000 
//*****************  Bit definition for FMC_PIO4 register  ******************
.equ FMC_PIO4_IOSET4_Pos, (0) 
.equ FMC_PIO4_IOSET4_Msk, (0xFF << FMC_PIO4_IOSET4_Pos) //!< 0x000000FF 
.equ FMC_PIO4_IOSET4, FMC_PIO4_IOSET4_Msk //!<IOSET4[7:0] bits (I/O 4 setup time) 
.equ FMC_PIO4_IOSET4_0, (0x01 << FMC_PIO4_IOSET4_Pos) //!< 0x00000001 
.equ FMC_PIO4_IOSET4_1, (0x02 << FMC_PIO4_IOSET4_Pos) //!< 0x00000002 
.equ FMC_PIO4_IOSET4_2, (0x04 << FMC_PIO4_IOSET4_Pos) //!< 0x00000004 
.equ FMC_PIO4_IOSET4_3, (0x08 << FMC_PIO4_IOSET4_Pos) //!< 0x00000008 
.equ FMC_PIO4_IOSET4_4, (0x10 << FMC_PIO4_IOSET4_Pos) //!< 0x00000010 
.equ FMC_PIO4_IOSET4_5, (0x20 << FMC_PIO4_IOSET4_Pos) //!< 0x00000020 
.equ FMC_PIO4_IOSET4_6, (0x40 << FMC_PIO4_IOSET4_Pos) //!< 0x00000040 
.equ FMC_PIO4_IOSET4_7, (0x80 << FMC_PIO4_IOSET4_Pos) //!< 0x00000080 
.equ FMC_PIO4_IOWAIT4_Pos, (8) 
.equ FMC_PIO4_IOWAIT4_Msk, (0xFF << FMC_PIO4_IOWAIT4_Pos) //!< 0x0000FF00 
.equ FMC_PIO4_IOWAIT4, FMC_PIO4_IOWAIT4_Msk //!<IOWAIT4[7:0] bits (I/O 4 wait time) 
.equ FMC_PIO4_IOWAIT4_0, (0x01 << FMC_PIO4_IOWAIT4_Pos) //!< 0x00000100 
.equ FMC_PIO4_IOWAIT4_1, (0x02 << FMC_PIO4_IOWAIT4_Pos) //!< 0x00000200 
.equ FMC_PIO4_IOWAIT4_2, (0x04 << FMC_PIO4_IOWAIT4_Pos) //!< 0x00000400 
.equ FMC_PIO4_IOWAIT4_3, (0x08 << FMC_PIO4_IOWAIT4_Pos) //!< 0x00000800 
.equ FMC_PIO4_IOWAIT4_4, (0x10 << FMC_PIO4_IOWAIT4_Pos) //!< 0x00001000 
.equ FMC_PIO4_IOWAIT4_5, (0x20 << FMC_PIO4_IOWAIT4_Pos) //!< 0x00002000 
.equ FMC_PIO4_IOWAIT4_6, (0x40 << FMC_PIO4_IOWAIT4_Pos) //!< 0x00004000 
.equ FMC_PIO4_IOWAIT4_7, (0x80 << FMC_PIO4_IOWAIT4_Pos) //!< 0x00008000 
.equ FMC_PIO4_IOHOLD4_Pos, (16) 
.equ FMC_PIO4_IOHOLD4_Msk, (0xFF << FMC_PIO4_IOHOLD4_Pos) //!< 0x00FF0000 
.equ FMC_PIO4_IOHOLD4, FMC_PIO4_IOHOLD4_Msk //!<IOHOLD4[7:0] bits (I/O 4 hold time) 
.equ FMC_PIO4_IOHOLD4_0, (0x01 << FMC_PIO4_IOHOLD4_Pos) //!< 0x00010000 
.equ FMC_PIO4_IOHOLD4_1, (0x02 << FMC_PIO4_IOHOLD4_Pos) //!< 0x00020000 
.equ FMC_PIO4_IOHOLD4_2, (0x04 << FMC_PIO4_IOHOLD4_Pos) //!< 0x00040000 
.equ FMC_PIO4_IOHOLD4_3, (0x08 << FMC_PIO4_IOHOLD4_Pos) //!< 0x00080000 
.equ FMC_PIO4_IOHOLD4_4, (0x10 << FMC_PIO4_IOHOLD4_Pos) //!< 0x00100000 
.equ FMC_PIO4_IOHOLD4_5, (0x20 << FMC_PIO4_IOHOLD4_Pos) //!< 0x00200000 
.equ FMC_PIO4_IOHOLD4_6, (0x40 << FMC_PIO4_IOHOLD4_Pos) //!< 0x00400000 
.equ FMC_PIO4_IOHOLD4_7, (0x80 << FMC_PIO4_IOHOLD4_Pos) //!< 0x00800000 
.equ FMC_PIO4_IOHIZ4_Pos, (24) 
.equ FMC_PIO4_IOHIZ4_Msk, (0xFF << FMC_PIO4_IOHIZ4_Pos) //!< 0xFF000000 
.equ FMC_PIO4_IOHIZ4, FMC_PIO4_IOHIZ4_Msk //!<IOHIZ4[7:0] bits (I/O 4 databus HiZ time) 
.equ FMC_PIO4_IOHIZ4_0, (0x01 << FMC_PIO4_IOHIZ4_Pos) //!< 0x01000000 
.equ FMC_PIO4_IOHIZ4_1, (0x02 << FMC_PIO4_IOHIZ4_Pos) //!< 0x02000000 
.equ FMC_PIO4_IOHIZ4_2, (0x04 << FMC_PIO4_IOHIZ4_Pos) //!< 0x04000000 
.equ FMC_PIO4_IOHIZ4_3, (0x08 << FMC_PIO4_IOHIZ4_Pos) //!< 0x08000000 
.equ FMC_PIO4_IOHIZ4_4, (0x10 << FMC_PIO4_IOHIZ4_Pos) //!< 0x10000000 
.equ FMC_PIO4_IOHIZ4_5, (0x20 << FMC_PIO4_IOHIZ4_Pos) //!< 0x20000000 
.equ FMC_PIO4_IOHIZ4_6, (0x40 << FMC_PIO4_IOHIZ4_Pos) //!< 0x40000000 
.equ FMC_PIO4_IOHIZ4_7, (0x80 << FMC_PIO4_IOHIZ4_Pos) //!< 0x80000000 
//*****************  Bit definition for FMC_ECCR2 register  *****************
.equ FMC_ECCR2_ECC2_Pos, (0) 
.equ FMC_ECCR2_ECC2_Msk, (0xFFFFFFFF << FMC_ECCR2_ECC2_Pos) //!< 0xFFFFFFFF 
.equ FMC_ECCR2_ECC2, FMC_ECCR2_ECC2_Msk //!<ECC result 
//*****************  Bit definition for FMC_ECCR3 register  *****************
.equ FMC_ECCR3_ECC3_Pos, (0) 
.equ FMC_ECCR3_ECC3_Msk, (0xFFFFFFFF << FMC_ECCR3_ECC3_Pos) //!< 0xFFFFFFFF 
.equ FMC_ECCR3_ECC3, FMC_ECCR3_ECC3_Msk //!<ECC result 
//****************************************************************************
//
//                            General Purpose I/O (GPIO)
//
//****************************************************************************
//******************  Bit definition for GPIO_MODER register  ****************
.equ GPIO_MODER_MODER0_Pos, (0) 
.equ GPIO_MODER_MODER0_Msk, (0x3 << GPIO_MODER_MODER0_Pos) //!< 0x00000003 
.equ GPIO_MODER_MODER0, GPIO_MODER_MODER0_Msk 
.equ GPIO_MODER_MODER0_0, (0x1 << GPIO_MODER_MODER0_Pos) //!< 0x00000001 
.equ GPIO_MODER_MODER0_1, (0x2 << GPIO_MODER_MODER0_Pos) //!< 0x00000002 
.equ GPIO_MODER_MODER1_Pos, (2) 
.equ GPIO_MODER_MODER1_Msk, (0x3 << GPIO_MODER_MODER1_Pos) //!< 0x0000000C 
.equ GPIO_MODER_MODER1, GPIO_MODER_MODER1_Msk 
.equ GPIO_MODER_MODER1_0, (0x1 << GPIO_MODER_MODER1_Pos) //!< 0x00000004 
.equ GPIO_MODER_MODER1_1, (0x2 << GPIO_MODER_MODER1_Pos) //!< 0x00000008 
.equ GPIO_MODER_MODER2_Pos, (4) 
.equ GPIO_MODER_MODER2_Msk, (0x3 << GPIO_MODER_MODER2_Pos) //!< 0x00000030 
.equ GPIO_MODER_MODER2, GPIO_MODER_MODER2_Msk 
.equ GPIO_MODER_MODER2_0, (0x1 << GPIO_MODER_MODER2_Pos) //!< 0x00000010 
.equ GPIO_MODER_MODER2_1, (0x2 << GPIO_MODER_MODER2_Pos) //!< 0x00000020 
.equ GPIO_MODER_MODER3_Pos, (6) 
.equ GPIO_MODER_MODER3_Msk, (0x3 << GPIO_MODER_MODER3_Pos) //!< 0x000000C0 
.equ GPIO_MODER_MODER3, GPIO_MODER_MODER3_Msk 
.equ GPIO_MODER_MODER3_0, (0x1 << GPIO_MODER_MODER3_Pos) //!< 0x00000040 
.equ GPIO_MODER_MODER3_1, (0x2 << GPIO_MODER_MODER3_Pos) //!< 0x00000080 
.equ GPIO_MODER_MODER4_Pos, (8) 
.equ GPIO_MODER_MODER4_Msk, (0x3 << GPIO_MODER_MODER4_Pos) //!< 0x00000300 
.equ GPIO_MODER_MODER4, GPIO_MODER_MODER4_Msk 
.equ GPIO_MODER_MODER4_0, (0x1 << GPIO_MODER_MODER4_Pos) //!< 0x00000100 
.equ GPIO_MODER_MODER4_1, (0x2 << GPIO_MODER_MODER4_Pos) //!< 0x00000200 
.equ GPIO_MODER_MODER5_Pos, (10) 
.equ GPIO_MODER_MODER5_Msk, (0x3 << GPIO_MODER_MODER5_Pos) //!< 0x00000C00 
.equ GPIO_MODER_MODER5, GPIO_MODER_MODER5_Msk 
.equ GPIO_MODER_MODER5_0, (0x1 << GPIO_MODER_MODER5_Pos) //!< 0x00000400 
.equ GPIO_MODER_MODER5_1, (0x2 << GPIO_MODER_MODER5_Pos) //!< 0x00000800 
.equ GPIO_MODER_MODER6_Pos, (12) 
.equ GPIO_MODER_MODER6_Msk, (0x3 << GPIO_MODER_MODER6_Pos) //!< 0x00003000 
.equ GPIO_MODER_MODER6, GPIO_MODER_MODER6_Msk 
.equ GPIO_MODER_MODER6_0, (0x1 << GPIO_MODER_MODER6_Pos) //!< 0x00001000 
.equ GPIO_MODER_MODER6_1, (0x2 << GPIO_MODER_MODER6_Pos) //!< 0x00002000 
.equ GPIO_MODER_MODER7_Pos, (14) 
.equ GPIO_MODER_MODER7_Msk, (0x3 << GPIO_MODER_MODER7_Pos) //!< 0x0000C000 
.equ GPIO_MODER_MODER7, GPIO_MODER_MODER7_Msk 
.equ GPIO_MODER_MODER7_0, (0x1 << GPIO_MODER_MODER7_Pos) //!< 0x00004000 
.equ GPIO_MODER_MODER7_1, (0x2 << GPIO_MODER_MODER7_Pos) //!< 0x00008000 
.equ GPIO_MODER_MODER8_Pos, (16) 
.equ GPIO_MODER_MODER8_Msk, (0x3 << GPIO_MODER_MODER8_Pos) //!< 0x00030000 
.equ GPIO_MODER_MODER8, GPIO_MODER_MODER8_Msk 
.equ GPIO_MODER_MODER8_0, (0x1 << GPIO_MODER_MODER8_Pos) //!< 0x00010000 
.equ GPIO_MODER_MODER8_1, (0x2 << GPIO_MODER_MODER8_Pos) //!< 0x00020000 
.equ GPIO_MODER_MODER9_Pos, (18) 
.equ GPIO_MODER_MODER9_Msk, (0x3 << GPIO_MODER_MODER9_Pos) //!< 0x000C0000 
.equ GPIO_MODER_MODER9, GPIO_MODER_MODER9_Msk 
.equ GPIO_MODER_MODER9_0, (0x1 << GPIO_MODER_MODER9_Pos) //!< 0x00040000 
.equ GPIO_MODER_MODER9_1, (0x2 << GPIO_MODER_MODER9_Pos) //!< 0x00080000 
.equ GPIO_MODER_MODER10_Pos, (20) 
.equ GPIO_MODER_MODER10_Msk, (0x3 << GPIO_MODER_MODER10_Pos) //!< 0x00300000 
.equ GPIO_MODER_MODER10, GPIO_MODER_MODER10_Msk 
.equ GPIO_MODER_MODER10_0, (0x1 << GPIO_MODER_MODER10_Pos) //!< 0x00100000 
.equ GPIO_MODER_MODER10_1, (0x2 << GPIO_MODER_MODER10_Pos) //!< 0x00200000 
.equ GPIO_MODER_MODER11_Pos, (22) 
.equ GPIO_MODER_MODER11_Msk, (0x3 << GPIO_MODER_MODER11_Pos) //!< 0x00C00000 
.equ GPIO_MODER_MODER11, GPIO_MODER_MODER11_Msk 
.equ GPIO_MODER_MODER11_0, (0x1 << GPIO_MODER_MODER11_Pos) //!< 0x00400000 
.equ GPIO_MODER_MODER11_1, (0x2 << GPIO_MODER_MODER11_Pos) //!< 0x00800000 
.equ GPIO_MODER_MODER12_Pos, (24) 
.equ GPIO_MODER_MODER12_Msk, (0x3 << GPIO_MODER_MODER12_Pos) //!< 0x03000000 
.equ GPIO_MODER_MODER12, GPIO_MODER_MODER12_Msk 
.equ GPIO_MODER_MODER12_0, (0x1 << GPIO_MODER_MODER12_Pos) //!< 0x01000000 
.equ GPIO_MODER_MODER12_1, (0x2 << GPIO_MODER_MODER12_Pos) //!< 0x02000000 
.equ GPIO_MODER_MODER13_Pos, (26) 
.equ GPIO_MODER_MODER13_Msk, (0x3 << GPIO_MODER_MODER13_Pos) //!< 0x0C000000 
.equ GPIO_MODER_MODER13, GPIO_MODER_MODER13_Msk 
.equ GPIO_MODER_MODER13_0, (0x1 << GPIO_MODER_MODER13_Pos) //!< 0x04000000 
.equ GPIO_MODER_MODER13_1, (0x2 << GPIO_MODER_MODER13_Pos) //!< 0x08000000 
.equ GPIO_MODER_MODER14_Pos, (28) 
.equ GPIO_MODER_MODER14_Msk, (0x3 << GPIO_MODER_MODER14_Pos) //!< 0x30000000 
.equ GPIO_MODER_MODER14, GPIO_MODER_MODER14_Msk 
.equ GPIO_MODER_MODER14_0, (0x1 << GPIO_MODER_MODER14_Pos) //!< 0x10000000 
.equ GPIO_MODER_MODER14_1, (0x2 << GPIO_MODER_MODER14_Pos) //!< 0x20000000 
.equ GPIO_MODER_MODER15_Pos, (30) 
.equ GPIO_MODER_MODER15_Msk, (0x3 << GPIO_MODER_MODER15_Pos) //!< 0xC0000000 
.equ GPIO_MODER_MODER15, GPIO_MODER_MODER15_Msk 
.equ GPIO_MODER_MODER15_0, (0x1 << GPIO_MODER_MODER15_Pos) //!< 0x40000000 
.equ GPIO_MODER_MODER15_1, (0x2 << GPIO_MODER_MODER15_Pos) //!< 0x80000000 
//*****************  Bit definition for GPIO_OTYPER register  ****************
.equ GPIO_OTYPER_OT_0, (0x00000001) 
.equ GPIO_OTYPER_OT_1, (0x00000002) 
.equ GPIO_OTYPER_OT_2, (0x00000004) 
.equ GPIO_OTYPER_OT_3, (0x00000008) 
.equ GPIO_OTYPER_OT_4, (0x00000010) 
.equ GPIO_OTYPER_OT_5, (0x00000020) 
.equ GPIO_OTYPER_OT_6, (0x00000040) 
.equ GPIO_OTYPER_OT_7, (0x00000080) 
.equ GPIO_OTYPER_OT_8, (0x00000100) 
.equ GPIO_OTYPER_OT_9, (0x00000200) 
.equ GPIO_OTYPER_OT_10, (0x00000400) 
.equ GPIO_OTYPER_OT_11, (0x00000800) 
.equ GPIO_OTYPER_OT_12, (0x00001000) 
.equ GPIO_OTYPER_OT_13, (0x00002000) 
.equ GPIO_OTYPER_OT_14, (0x00004000) 
.equ GPIO_OTYPER_OT_15, (0x00008000) 
//***************  Bit definition for GPIO_OSPEEDR register  *****************
.equ GPIO_OSPEEDER_OSPEEDR0_Pos, (0) 
.equ GPIO_OSPEEDER_OSPEEDR0_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR0_Pos) //!< 0x00000003 
.equ GPIO_OSPEEDER_OSPEEDR0, GPIO_OSPEEDER_OSPEEDR0_Msk 
.equ GPIO_OSPEEDER_OSPEEDR0_0, (0x1 << GPIO_OSPEEDER_OSPEEDR0_Pos) //!< 0x00000001 
.equ GPIO_OSPEEDER_OSPEEDR0_1, (0x2 << GPIO_OSPEEDER_OSPEEDR0_Pos) //!< 0x00000002 
.equ GPIO_OSPEEDER_OSPEEDR1_Pos, (2) 
.equ GPIO_OSPEEDER_OSPEEDR1_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR1_Pos) //!< 0x0000000C 
.equ GPIO_OSPEEDER_OSPEEDR1, GPIO_OSPEEDER_OSPEEDR1_Msk 
.equ GPIO_OSPEEDER_OSPEEDR1_0, (0x1 << GPIO_OSPEEDER_OSPEEDR1_Pos) //!< 0x00000004 
.equ GPIO_OSPEEDER_OSPEEDR1_1, (0x2 << GPIO_OSPEEDER_OSPEEDR1_Pos) //!< 0x00000008 
.equ GPIO_OSPEEDER_OSPEEDR2_Pos, (4) 
.equ GPIO_OSPEEDER_OSPEEDR2_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR2_Pos) //!< 0x00000030 
.equ GPIO_OSPEEDER_OSPEEDR2, GPIO_OSPEEDER_OSPEEDR2_Msk 
.equ GPIO_OSPEEDER_OSPEEDR2_0, (0x1 << GPIO_OSPEEDER_OSPEEDR2_Pos) //!< 0x00000010 
.equ GPIO_OSPEEDER_OSPEEDR2_1, (0x2 << GPIO_OSPEEDER_OSPEEDR2_Pos) //!< 0x00000020 
.equ GPIO_OSPEEDER_OSPEEDR3_Pos, (6) 
.equ GPIO_OSPEEDER_OSPEEDR3_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR3_Pos) //!< 0x000000C0 
.equ GPIO_OSPEEDER_OSPEEDR3, GPIO_OSPEEDER_OSPEEDR3_Msk 
.equ GPIO_OSPEEDER_OSPEEDR3_0, (0x1 << GPIO_OSPEEDER_OSPEEDR3_Pos) //!< 0x00000040 
.equ GPIO_OSPEEDER_OSPEEDR3_1, (0x2 << GPIO_OSPEEDER_OSPEEDR3_Pos) //!< 0x00000080 
.equ GPIO_OSPEEDER_OSPEEDR4_Pos, (8) 
.equ GPIO_OSPEEDER_OSPEEDR4_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR4_Pos) //!< 0x00000300 
.equ GPIO_OSPEEDER_OSPEEDR4, GPIO_OSPEEDER_OSPEEDR4_Msk 
.equ GPIO_OSPEEDER_OSPEEDR4_0, (0x1 << GPIO_OSPEEDER_OSPEEDR4_Pos) //!< 0x00000100 
.equ GPIO_OSPEEDER_OSPEEDR4_1, (0x2 << GPIO_OSPEEDER_OSPEEDR4_Pos) //!< 0x00000200 
.equ GPIO_OSPEEDER_OSPEEDR5_Pos, (10) 
.equ GPIO_OSPEEDER_OSPEEDR5_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR5_Pos) //!< 0x00000C00 
.equ GPIO_OSPEEDER_OSPEEDR5, GPIO_OSPEEDER_OSPEEDR5_Msk 
.equ GPIO_OSPEEDER_OSPEEDR5_0, (0x1 << GPIO_OSPEEDER_OSPEEDR5_Pos) //!< 0x00000400 
.equ GPIO_OSPEEDER_OSPEEDR5_1, (0x2 << GPIO_OSPEEDER_OSPEEDR5_Pos) //!< 0x00000800 
.equ GPIO_OSPEEDER_OSPEEDR6_Pos, (12) 
.equ GPIO_OSPEEDER_OSPEEDR6_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR6_Pos) //!< 0x00003000 
.equ GPIO_OSPEEDER_OSPEEDR6, GPIO_OSPEEDER_OSPEEDR6_Msk 
.equ GPIO_OSPEEDER_OSPEEDR6_0, (0x1 << GPIO_OSPEEDER_OSPEEDR6_Pos) //!< 0x00001000 
.equ GPIO_OSPEEDER_OSPEEDR6_1, (0x2 << GPIO_OSPEEDER_OSPEEDR6_Pos) //!< 0x00002000 
.equ GPIO_OSPEEDER_OSPEEDR7_Pos, (14) 
.equ GPIO_OSPEEDER_OSPEEDR7_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR7_Pos) //!< 0x0000C000 
.equ GPIO_OSPEEDER_OSPEEDR7, GPIO_OSPEEDER_OSPEEDR7_Msk 
.equ GPIO_OSPEEDER_OSPEEDR7_0, (0x1 << GPIO_OSPEEDER_OSPEEDR7_Pos) //!< 0x00004000 
.equ GPIO_OSPEEDER_OSPEEDR7_1, (0x2 << GPIO_OSPEEDER_OSPEEDR7_Pos) //!< 0x00008000 
.equ GPIO_OSPEEDER_OSPEEDR8_Pos, (16) 
.equ GPIO_OSPEEDER_OSPEEDR8_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR8_Pos) //!< 0x00030000 
.equ GPIO_OSPEEDER_OSPEEDR8, GPIO_OSPEEDER_OSPEEDR8_Msk 
.equ GPIO_OSPEEDER_OSPEEDR8_0, (0x1 << GPIO_OSPEEDER_OSPEEDR8_Pos) //!< 0x00010000 
.equ GPIO_OSPEEDER_OSPEEDR8_1, (0x2 << GPIO_OSPEEDER_OSPEEDR8_Pos) //!< 0x00020000 
.equ GPIO_OSPEEDER_OSPEEDR9_Pos, (18) 
.equ GPIO_OSPEEDER_OSPEEDR9_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR9_Pos) //!< 0x000C0000 
.equ GPIO_OSPEEDER_OSPEEDR9, GPIO_OSPEEDER_OSPEEDR9_Msk 
.equ GPIO_OSPEEDER_OSPEEDR9_0, (0x1 << GPIO_OSPEEDER_OSPEEDR9_Pos) //!< 0x00040000 
.equ GPIO_OSPEEDER_OSPEEDR9_1, (0x2 << GPIO_OSPEEDER_OSPEEDR9_Pos) //!< 0x00080000 
.equ GPIO_OSPEEDER_OSPEEDR10_Pos, (20) 
.equ GPIO_OSPEEDER_OSPEEDR10_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR10_Pos) //!< 0x00300000 
.equ GPIO_OSPEEDER_OSPEEDR10, GPIO_OSPEEDER_OSPEEDR10_Msk 
.equ GPIO_OSPEEDER_OSPEEDR10_0, (0x1 << GPIO_OSPEEDER_OSPEEDR10_Pos) //!< 0x00100000 
.equ GPIO_OSPEEDER_OSPEEDR10_1, (0x2 << GPIO_OSPEEDER_OSPEEDR10_Pos) //!< 0x00200000 
.equ GPIO_OSPEEDER_OSPEEDR11_Pos, (22) 
.equ GPIO_OSPEEDER_OSPEEDR11_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR11_Pos) //!< 0x00C00000 
.equ GPIO_OSPEEDER_OSPEEDR11, GPIO_OSPEEDER_OSPEEDR11_Msk 
.equ GPIO_OSPEEDER_OSPEEDR11_0, (0x1 << GPIO_OSPEEDER_OSPEEDR11_Pos) //!< 0x00400000 
.equ GPIO_OSPEEDER_OSPEEDR11_1, (0x2 << GPIO_OSPEEDER_OSPEEDR11_Pos) //!< 0x00800000 
.equ GPIO_OSPEEDER_OSPEEDR12_Pos, (24) 
.equ GPIO_OSPEEDER_OSPEEDR12_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR12_Pos) //!< 0x03000000 
.equ GPIO_OSPEEDER_OSPEEDR12, GPIO_OSPEEDER_OSPEEDR12_Msk 
.equ GPIO_OSPEEDER_OSPEEDR12_0, (0x1 << GPIO_OSPEEDER_OSPEEDR12_Pos) //!< 0x01000000 
.equ GPIO_OSPEEDER_OSPEEDR12_1, (0x2 << GPIO_OSPEEDER_OSPEEDR12_Pos) //!< 0x02000000 
.equ GPIO_OSPEEDER_OSPEEDR13_Pos, (26) 
.equ GPIO_OSPEEDER_OSPEEDR13_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR13_Pos) //!< 0x0C000000 
.equ GPIO_OSPEEDER_OSPEEDR13, GPIO_OSPEEDER_OSPEEDR13_Msk 
.equ GPIO_OSPEEDER_OSPEEDR13_0, (0x1 << GPIO_OSPEEDER_OSPEEDR13_Pos) //!< 0x04000000 
.equ GPIO_OSPEEDER_OSPEEDR13_1, (0x2 << GPIO_OSPEEDER_OSPEEDR13_Pos) //!< 0x08000000 
.equ GPIO_OSPEEDER_OSPEEDR14_Pos, (28) 
.equ GPIO_OSPEEDER_OSPEEDR14_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR14_Pos) //!< 0x30000000 
.equ GPIO_OSPEEDER_OSPEEDR14, GPIO_OSPEEDER_OSPEEDR14_Msk 
.equ GPIO_OSPEEDER_OSPEEDR14_0, (0x1 << GPIO_OSPEEDER_OSPEEDR14_Pos) //!< 0x10000000 
.equ GPIO_OSPEEDER_OSPEEDR14_1, (0x2 << GPIO_OSPEEDER_OSPEEDR14_Pos) //!< 0x20000000 
.equ GPIO_OSPEEDER_OSPEEDR15_Pos, (30) 
.equ GPIO_OSPEEDER_OSPEEDR15_Msk, (0x3 << GPIO_OSPEEDER_OSPEEDR15_Pos) //!< 0xC0000000 
.equ GPIO_OSPEEDER_OSPEEDR15, GPIO_OSPEEDER_OSPEEDR15_Msk 
.equ GPIO_OSPEEDER_OSPEEDR15_0, (0x1 << GPIO_OSPEEDER_OSPEEDR15_Pos) //!< 0x40000000 
.equ GPIO_OSPEEDER_OSPEEDR15_1, (0x2 << GPIO_OSPEEDER_OSPEEDR15_Pos) //!< 0x80000000 
//******************  Bit definition for GPIO_PUPDR register *****************
.equ GPIO_PUPDR_PUPDR0_Pos, (0) 
.equ GPIO_PUPDR_PUPDR0_Msk, (0x3 << GPIO_PUPDR_PUPDR0_Pos) //!< 0x00000003 
.equ GPIO_PUPDR_PUPDR0, GPIO_PUPDR_PUPDR0_Msk 
.equ GPIO_PUPDR_PUPDR0_0, (0x1 << GPIO_PUPDR_PUPDR0_Pos) //!< 0x00000001 
.equ GPIO_PUPDR_PUPDR0_1, (0x2 << GPIO_PUPDR_PUPDR0_Pos) //!< 0x00000002 
.equ GPIO_PUPDR_PUPDR1_Pos, (2) 
.equ GPIO_PUPDR_PUPDR1_Msk, (0x3 << GPIO_PUPDR_PUPDR1_Pos) //!< 0x0000000C 
.equ GPIO_PUPDR_PUPDR1, GPIO_PUPDR_PUPDR1_Msk 
.equ GPIO_PUPDR_PUPDR1_0, (0x1 << GPIO_PUPDR_PUPDR1_Pos) //!< 0x00000004 
.equ GPIO_PUPDR_PUPDR1_1, (0x2 << GPIO_PUPDR_PUPDR1_Pos) //!< 0x00000008 
.equ GPIO_PUPDR_PUPDR2_Pos, (4) 
.equ GPIO_PUPDR_PUPDR2_Msk, (0x3 << GPIO_PUPDR_PUPDR2_Pos) //!< 0x00000030 
.equ GPIO_PUPDR_PUPDR2, GPIO_PUPDR_PUPDR2_Msk 
.equ GPIO_PUPDR_PUPDR2_0, (0x1 << GPIO_PUPDR_PUPDR2_Pos) //!< 0x00000010 
.equ GPIO_PUPDR_PUPDR2_1, (0x2 << GPIO_PUPDR_PUPDR2_Pos) //!< 0x00000020 
.equ GPIO_PUPDR_PUPDR3_Pos, (6) 
.equ GPIO_PUPDR_PUPDR3_Msk, (0x3 << GPIO_PUPDR_PUPDR3_Pos) //!< 0x000000C0 
.equ GPIO_PUPDR_PUPDR3, GPIO_PUPDR_PUPDR3_Msk 
.equ GPIO_PUPDR_PUPDR3_0, (0x1 << GPIO_PUPDR_PUPDR3_Pos) //!< 0x00000040 
.equ GPIO_PUPDR_PUPDR3_1, (0x2 << GPIO_PUPDR_PUPDR3_Pos) //!< 0x00000080 
.equ GPIO_PUPDR_PUPDR4_Pos, (8) 
.equ GPIO_PUPDR_PUPDR4_Msk, (0x3 << GPIO_PUPDR_PUPDR4_Pos) //!< 0x00000300 
.equ GPIO_PUPDR_PUPDR4, GPIO_PUPDR_PUPDR4_Msk 
.equ GPIO_PUPDR_PUPDR4_0, (0x1 << GPIO_PUPDR_PUPDR4_Pos) //!< 0x00000100 
.equ GPIO_PUPDR_PUPDR4_1, (0x2 << GPIO_PUPDR_PUPDR4_Pos) //!< 0x00000200 
.equ GPIO_PUPDR_PUPDR5_Pos, (10) 
.equ GPIO_PUPDR_PUPDR5_Msk, (0x3 << GPIO_PUPDR_PUPDR5_Pos) //!< 0x00000C00 
.equ GPIO_PUPDR_PUPDR5, GPIO_PUPDR_PUPDR5_Msk 
.equ GPIO_PUPDR_PUPDR5_0, (0x1 << GPIO_PUPDR_PUPDR5_Pos) //!< 0x00000400 
.equ GPIO_PUPDR_PUPDR5_1, (0x2 << GPIO_PUPDR_PUPDR5_Pos) //!< 0x00000800 
.equ GPIO_PUPDR_PUPDR6_Pos, (12) 
.equ GPIO_PUPDR_PUPDR6_Msk, (0x3 << GPIO_PUPDR_PUPDR6_Pos) //!< 0x00003000 
.equ GPIO_PUPDR_PUPDR6, GPIO_PUPDR_PUPDR6_Msk 
.equ GPIO_PUPDR_PUPDR6_0, (0x1 << GPIO_PUPDR_PUPDR6_Pos) //!< 0x00001000 
.equ GPIO_PUPDR_PUPDR6_1, (0x2 << GPIO_PUPDR_PUPDR6_Pos) //!< 0x00002000 
.equ GPIO_PUPDR_PUPDR7_Pos, (14) 
.equ GPIO_PUPDR_PUPDR7_Msk, (0x3 << GPIO_PUPDR_PUPDR7_Pos) //!< 0x0000C000 
.equ GPIO_PUPDR_PUPDR7, GPIO_PUPDR_PUPDR7_Msk 
.equ GPIO_PUPDR_PUPDR7_0, (0x1 << GPIO_PUPDR_PUPDR7_Pos) //!< 0x00004000 
.equ GPIO_PUPDR_PUPDR7_1, (0x2 << GPIO_PUPDR_PUPDR7_Pos) //!< 0x00008000 
.equ GPIO_PUPDR_PUPDR8_Pos, (16) 
.equ GPIO_PUPDR_PUPDR8_Msk, (0x3 << GPIO_PUPDR_PUPDR8_Pos) //!< 0x00030000 
.equ GPIO_PUPDR_PUPDR8, GPIO_PUPDR_PUPDR8_Msk 
.equ GPIO_PUPDR_PUPDR8_0, (0x1 << GPIO_PUPDR_PUPDR8_Pos) //!< 0x00010000 
.equ GPIO_PUPDR_PUPDR8_1, (0x2 << GPIO_PUPDR_PUPDR8_Pos) //!< 0x00020000 
.equ GPIO_PUPDR_PUPDR9_Pos, (18) 
.equ GPIO_PUPDR_PUPDR9_Msk, (0x3 << GPIO_PUPDR_PUPDR9_Pos) //!< 0x000C0000 
.equ GPIO_PUPDR_PUPDR9, GPIO_PUPDR_PUPDR9_Msk 
.equ GPIO_PUPDR_PUPDR9_0, (0x1 << GPIO_PUPDR_PUPDR9_Pos) //!< 0x00040000 
.equ GPIO_PUPDR_PUPDR9_1, (0x2 << GPIO_PUPDR_PUPDR9_Pos) //!< 0x00080000 
.equ GPIO_PUPDR_PUPDR10_Pos, (20) 
.equ GPIO_PUPDR_PUPDR10_Msk, (0x3 << GPIO_PUPDR_PUPDR10_Pos) //!< 0x00300000 
.equ GPIO_PUPDR_PUPDR10, GPIO_PUPDR_PUPDR10_Msk 
.equ GPIO_PUPDR_PUPDR10_0, (0x1 << GPIO_PUPDR_PUPDR10_Pos) //!< 0x00100000 
.equ GPIO_PUPDR_PUPDR10_1, (0x2 << GPIO_PUPDR_PUPDR10_Pos) //!< 0x00200000 
.equ GPIO_PUPDR_PUPDR11_Pos, (22) 
.equ GPIO_PUPDR_PUPDR11_Msk, (0x3 << GPIO_PUPDR_PUPDR11_Pos) //!< 0x00C00000 
.equ GPIO_PUPDR_PUPDR11, GPIO_PUPDR_PUPDR11_Msk 
.equ GPIO_PUPDR_PUPDR11_0, (0x1 << GPIO_PUPDR_PUPDR11_Pos) //!< 0x00400000 
.equ GPIO_PUPDR_PUPDR11_1, (0x2 << GPIO_PUPDR_PUPDR11_Pos) //!< 0x00800000 
.equ GPIO_PUPDR_PUPDR12_Pos, (24) 
.equ GPIO_PUPDR_PUPDR12_Msk, (0x3 << GPIO_PUPDR_PUPDR12_Pos) //!< 0x03000000 
.equ GPIO_PUPDR_PUPDR12, GPIO_PUPDR_PUPDR12_Msk 
.equ GPIO_PUPDR_PUPDR12_0, (0x1 << GPIO_PUPDR_PUPDR12_Pos) //!< 0x01000000 
.equ GPIO_PUPDR_PUPDR12_1, (0x2 << GPIO_PUPDR_PUPDR12_Pos) //!< 0x02000000 
.equ GPIO_PUPDR_PUPDR13_Pos, (26) 
.equ GPIO_PUPDR_PUPDR13_Msk, (0x3 << GPIO_PUPDR_PUPDR13_Pos) //!< 0x0C000000 
.equ GPIO_PUPDR_PUPDR13, GPIO_PUPDR_PUPDR13_Msk 
.equ GPIO_PUPDR_PUPDR13_0, (0x1 << GPIO_PUPDR_PUPDR13_Pos) //!< 0x04000000 
.equ GPIO_PUPDR_PUPDR13_1, (0x2 << GPIO_PUPDR_PUPDR13_Pos) //!< 0x08000000 
.equ GPIO_PUPDR_PUPDR14_Pos, (28) 
.equ GPIO_PUPDR_PUPDR14_Msk, (0x3 << GPIO_PUPDR_PUPDR14_Pos) //!< 0x30000000 
.equ GPIO_PUPDR_PUPDR14, GPIO_PUPDR_PUPDR14_Msk 
.equ GPIO_PUPDR_PUPDR14_0, (0x1 << GPIO_PUPDR_PUPDR14_Pos) //!< 0x10000000 
.equ GPIO_PUPDR_PUPDR14_1, (0x2 << GPIO_PUPDR_PUPDR14_Pos) //!< 0x20000000 
.equ GPIO_PUPDR_PUPDR15_Pos, (30) 
.equ GPIO_PUPDR_PUPDR15_Msk, (0x3 << GPIO_PUPDR_PUPDR15_Pos) //!< 0xC0000000 
.equ GPIO_PUPDR_PUPDR15, GPIO_PUPDR_PUPDR15_Msk 
.equ GPIO_PUPDR_PUPDR15_0, (0x1 << GPIO_PUPDR_PUPDR15_Pos) //!< 0x40000000 
.equ GPIO_PUPDR_PUPDR15_1, (0x2 << GPIO_PUPDR_PUPDR15_Pos) //!< 0x80000000 
//******************  Bit definition for GPIO_IDR register  ******************
.equ GPIO_IDR_0, (0x00000001) 
.equ GPIO_IDR_1, (0x00000002) 
.equ GPIO_IDR_2, (0x00000004) 
.equ GPIO_IDR_3, (0x00000008) 
.equ GPIO_IDR_4, (0x00000010) 
.equ GPIO_IDR_5, (0x00000020) 
.equ GPIO_IDR_6, (0x00000040) 
.equ GPIO_IDR_7, (0x00000080) 
.equ GPIO_IDR_8, (0x00000100) 
.equ GPIO_IDR_9, (0x00000200) 
.equ GPIO_IDR_10, (0x00000400) 
.equ GPIO_IDR_11, (0x00000800) 
.equ GPIO_IDR_12, (0x00001000) 
.equ GPIO_IDR_13, (0x00002000) 
.equ GPIO_IDR_14, (0x00004000) 
.equ GPIO_IDR_15, (0x00008000) 
//*****************  Bit definition for GPIO_ODR register  *******************
.equ GPIO_ODR_0, (0x00000001) 
.equ GPIO_ODR_1, (0x00000002) 
.equ GPIO_ODR_2, (0x00000004) 
.equ GPIO_ODR_3, (0x00000008) 
.equ GPIO_ODR_4, (0x00000010) 
.equ GPIO_ODR_5, (0x00000020) 
.equ GPIO_ODR_6, (0x00000040) 
.equ GPIO_ODR_7, (0x00000080) 
.equ GPIO_ODR_8, (0x00000100) 
.equ GPIO_ODR_9, (0x00000200) 
.equ GPIO_ODR_10, (0x00000400) 
.equ GPIO_ODR_11, (0x00000800) 
.equ GPIO_ODR_12, (0x00001000) 
.equ GPIO_ODR_13, (0x00002000) 
.equ GPIO_ODR_14, (0x00004000) 
.equ GPIO_ODR_15, (0x00008000) 
//***************** Bit definition for GPIO_BSRR register  *******************
.equ GPIO_BSRR_BS_0, (0x00000001) 
.equ GPIO_BSRR_BS_1, (0x00000002) 
.equ GPIO_BSRR_BS_2, (0x00000004) 
.equ GPIO_BSRR_BS_3, (0x00000008) 
.equ GPIO_BSRR_BS_4, (0x00000010) 
.equ GPIO_BSRR_BS_5, (0x00000020) 
.equ GPIO_BSRR_BS_6, (0x00000040) 
.equ GPIO_BSRR_BS_7, (0x00000080) 
.equ GPIO_BSRR_BS_8, (0x00000100) 
.equ GPIO_BSRR_BS_9, (0x00000200) 
.equ GPIO_BSRR_BS_10, (0x00000400) 
.equ GPIO_BSRR_BS_11, (0x00000800) 
.equ GPIO_BSRR_BS_12, (0x00001000) 
.equ GPIO_BSRR_BS_13, (0x00002000) 
.equ GPIO_BSRR_BS_14, (0x00004000) 
.equ GPIO_BSRR_BS_15, (0x00008000) 
.equ GPIO_BSRR_BR_0, (0x00010000) 
.equ GPIO_BSRR_BR_1, (0x00020000) 
.equ GPIO_BSRR_BR_2, (0x00040000) 
.equ GPIO_BSRR_BR_3, (0x00080000) 
.equ GPIO_BSRR_BR_4, (0x00100000) 
.equ GPIO_BSRR_BR_5, (0x00200000) 
.equ GPIO_BSRR_BR_6, (0x00400000) 
.equ GPIO_BSRR_BR_7, (0x00800000) 
.equ GPIO_BSRR_BR_8, (0x01000000) 
.equ GPIO_BSRR_BR_9, (0x02000000) 
.equ GPIO_BSRR_BR_10, (0x04000000) 
.equ GPIO_BSRR_BR_11, (0x08000000) 
.equ GPIO_BSRR_BR_12, (0x10000000) 
.equ GPIO_BSRR_BR_13, (0x20000000) 
.equ GPIO_BSRR_BR_14, (0x40000000) 
.equ GPIO_BSRR_BR_15, (0x80000000) 
//***************** Bit definition for GPIO_LCKR register  *******************
.equ GPIO_LCKR_LCK0_Pos, (0) 
.equ GPIO_LCKR_LCK0_Msk, (0x1 << GPIO_LCKR_LCK0_Pos) //!< 0x00000001 
.equ GPIO_LCKR_LCK0, GPIO_LCKR_LCK0_Msk 
.equ GPIO_LCKR_LCK1_Pos, (1) 
.equ GPIO_LCKR_LCK1_Msk, (0x1 << GPIO_LCKR_LCK1_Pos) //!< 0x00000002 
.equ GPIO_LCKR_LCK1, GPIO_LCKR_LCK1_Msk 
.equ GPIO_LCKR_LCK2_Pos, (2) 
.equ GPIO_LCKR_LCK2_Msk, (0x1 << GPIO_LCKR_LCK2_Pos) //!< 0x00000004 
.equ GPIO_LCKR_LCK2, GPIO_LCKR_LCK2_Msk 
.equ GPIO_LCKR_LCK3_Pos, (3) 
.equ GPIO_LCKR_LCK3_Msk, (0x1 << GPIO_LCKR_LCK3_Pos) //!< 0x00000008 
.equ GPIO_LCKR_LCK3, GPIO_LCKR_LCK3_Msk 
.equ GPIO_LCKR_LCK4_Pos, (4) 
.equ GPIO_LCKR_LCK4_Msk, (0x1 << GPIO_LCKR_LCK4_Pos) //!< 0x00000010 
.equ GPIO_LCKR_LCK4, GPIO_LCKR_LCK4_Msk 
.equ GPIO_LCKR_LCK5_Pos, (5) 
.equ GPIO_LCKR_LCK5_Msk, (0x1 << GPIO_LCKR_LCK5_Pos) //!< 0x00000020 
.equ GPIO_LCKR_LCK5, GPIO_LCKR_LCK5_Msk 
.equ GPIO_LCKR_LCK6_Pos, (6) 
.equ GPIO_LCKR_LCK6_Msk, (0x1 << GPIO_LCKR_LCK6_Pos) //!< 0x00000040 
.equ GPIO_LCKR_LCK6, GPIO_LCKR_LCK6_Msk 
.equ GPIO_LCKR_LCK7_Pos, (7) 
.equ GPIO_LCKR_LCK7_Msk, (0x1 << GPIO_LCKR_LCK7_Pos) //!< 0x00000080 
.equ GPIO_LCKR_LCK7, GPIO_LCKR_LCK7_Msk 
.equ GPIO_LCKR_LCK8_Pos, (8) 
.equ GPIO_LCKR_LCK8_Msk, (0x1 << GPIO_LCKR_LCK8_Pos) //!< 0x00000100 
.equ GPIO_LCKR_LCK8, GPIO_LCKR_LCK8_Msk 
.equ GPIO_LCKR_LCK9_Pos, (9) 
.equ GPIO_LCKR_LCK9_Msk, (0x1 << GPIO_LCKR_LCK9_Pos) //!< 0x00000200 
.equ GPIO_LCKR_LCK9, GPIO_LCKR_LCK9_Msk 
.equ GPIO_LCKR_LCK10_Pos, (10) 
.equ GPIO_LCKR_LCK10_Msk, (0x1 << GPIO_LCKR_LCK10_Pos) //!< 0x00000400 
.equ GPIO_LCKR_LCK10, GPIO_LCKR_LCK10_Msk 
.equ GPIO_LCKR_LCK11_Pos, (11) 
.equ GPIO_LCKR_LCK11_Msk, (0x1 << GPIO_LCKR_LCK11_Pos) //!< 0x00000800 
.equ GPIO_LCKR_LCK11, GPIO_LCKR_LCK11_Msk 
.equ GPIO_LCKR_LCK12_Pos, (12) 
.equ GPIO_LCKR_LCK12_Msk, (0x1 << GPIO_LCKR_LCK12_Pos) //!< 0x00001000 
.equ GPIO_LCKR_LCK12, GPIO_LCKR_LCK12_Msk 
.equ GPIO_LCKR_LCK13_Pos, (13) 
.equ GPIO_LCKR_LCK13_Msk, (0x1 << GPIO_LCKR_LCK13_Pos) //!< 0x00002000 
.equ GPIO_LCKR_LCK13, GPIO_LCKR_LCK13_Msk 
.equ GPIO_LCKR_LCK14_Pos, (14) 
.equ GPIO_LCKR_LCK14_Msk, (0x1 << GPIO_LCKR_LCK14_Pos) //!< 0x00004000 
.equ GPIO_LCKR_LCK14, GPIO_LCKR_LCK14_Msk 
.equ GPIO_LCKR_LCK15_Pos, (15) 
.equ GPIO_LCKR_LCK15_Msk, (0x1 << GPIO_LCKR_LCK15_Pos) //!< 0x00008000 
.equ GPIO_LCKR_LCK15, GPIO_LCKR_LCK15_Msk 
.equ GPIO_LCKR_LCKK_Pos, (16) 
.equ GPIO_LCKR_LCKK_Msk, (0x1 << GPIO_LCKR_LCKK_Pos) //!< 0x00010000 
.equ GPIO_LCKR_LCKK, GPIO_LCKR_LCKK_Msk 
//***************** Bit definition for GPIO_AFRL register  *******************
.equ GPIO_AFRL_AFRL0_Pos, (0) 
.equ GPIO_AFRL_AFRL0_Msk, (0xF << GPIO_AFRL_AFRL0_Pos) //!< 0x0000000F 
.equ GPIO_AFRL_AFRL0, GPIO_AFRL_AFRL0_Msk 
.equ GPIO_AFRL_AFRL1_Pos, (4) 
.equ GPIO_AFRL_AFRL1_Msk, (0xF << GPIO_AFRL_AFRL1_Pos) //!< 0x000000F0 
.equ GPIO_AFRL_AFRL1, GPIO_AFRL_AFRL1_Msk 
.equ GPIO_AFRL_AFRL2_Pos, (8) 
.equ GPIO_AFRL_AFRL2_Msk, (0xF << GPIO_AFRL_AFRL2_Pos) //!< 0x00000F00 
.equ GPIO_AFRL_AFRL2, GPIO_AFRL_AFRL2_Msk 
.equ GPIO_AFRL_AFRL3_Pos, (12) 
.equ GPIO_AFRL_AFRL3_Msk, (0xF << GPIO_AFRL_AFRL3_Pos) //!< 0x0000F000 
.equ GPIO_AFRL_AFRL3, GPIO_AFRL_AFRL3_Msk 
.equ GPIO_AFRL_AFRL4_Pos, (16) 
.equ GPIO_AFRL_AFRL4_Msk, (0xF << GPIO_AFRL_AFRL4_Pos) //!< 0x000F0000 
.equ GPIO_AFRL_AFRL4, GPIO_AFRL_AFRL4_Msk 
.equ GPIO_AFRL_AFRL5_Pos, (20) 
.equ GPIO_AFRL_AFRL5_Msk, (0xF << GPIO_AFRL_AFRL5_Pos) //!< 0x00F00000 
.equ GPIO_AFRL_AFRL5, GPIO_AFRL_AFRL5_Msk 
.equ GPIO_AFRL_AFRL6_Pos, (24) 
.equ GPIO_AFRL_AFRL6_Msk, (0xF << GPIO_AFRL_AFRL6_Pos) //!< 0x0F000000 
.equ GPIO_AFRL_AFRL6, GPIO_AFRL_AFRL6_Msk 
.equ GPIO_AFRL_AFRL7_Pos, (28) 
.equ GPIO_AFRL_AFRL7_Msk, (0xF << GPIO_AFRL_AFRL7_Pos) //!< 0xF0000000 
.equ GPIO_AFRL_AFRL7, GPIO_AFRL_AFRL7_Msk 
//***************** Bit definition for GPIO_AFRH register  *******************
.equ GPIO_AFRH_AFRH0_Pos, (0) 
.equ GPIO_AFRH_AFRH0_Msk, (0xF << GPIO_AFRH_AFRH0_Pos) //!< 0x0000000F 
.equ GPIO_AFRH_AFRH0, GPIO_AFRH_AFRH0_Msk 
.equ GPIO_AFRH_AFRH1_Pos, (4) 
.equ GPIO_AFRH_AFRH1_Msk, (0xF << GPIO_AFRH_AFRH1_Pos) //!< 0x000000F0 
.equ GPIO_AFRH_AFRH1, GPIO_AFRH_AFRH1_Msk 
.equ GPIO_AFRH_AFRH2_Pos, (8) 
.equ GPIO_AFRH_AFRH2_Msk, (0xF << GPIO_AFRH_AFRH2_Pos) //!< 0x00000F00 
.equ GPIO_AFRH_AFRH2, GPIO_AFRH_AFRH2_Msk 
.equ GPIO_AFRH_AFRH3_Pos, (12) 
.equ GPIO_AFRH_AFRH3_Msk, (0xF << GPIO_AFRH_AFRH3_Pos) //!< 0x0000F000 
.equ GPIO_AFRH_AFRH3, GPIO_AFRH_AFRH3_Msk 
.equ GPIO_AFRH_AFRH4_Pos, (16) 
.equ GPIO_AFRH_AFRH4_Msk, (0xF << GPIO_AFRH_AFRH4_Pos) //!< 0x000F0000 
.equ GPIO_AFRH_AFRH4, GPIO_AFRH_AFRH4_Msk 
.equ GPIO_AFRH_AFRH5_Pos, (20) 
.equ GPIO_AFRH_AFRH5_Msk, (0xF << GPIO_AFRH_AFRH5_Pos) //!< 0x00F00000 
.equ GPIO_AFRH_AFRH5, GPIO_AFRH_AFRH5_Msk 
.equ GPIO_AFRH_AFRH6_Pos, (24) 
.equ GPIO_AFRH_AFRH6_Msk, (0xF << GPIO_AFRH_AFRH6_Pos) //!< 0x0F000000 
.equ GPIO_AFRH_AFRH6, GPIO_AFRH_AFRH6_Msk 
.equ GPIO_AFRH_AFRH7_Pos, (28) 
.equ GPIO_AFRH_AFRH7_Msk, (0xF << GPIO_AFRH_AFRH7_Pos) //!< 0xF0000000 
.equ GPIO_AFRH_AFRH7, GPIO_AFRH_AFRH7_Msk 
//***************** Bit definition for GPIO_BRR register  ********************
.equ GPIO_BRR_BR_0, (0x00000001) 
.equ GPIO_BRR_BR_1, (0x00000002) 
.equ GPIO_BRR_BR_2, (0x00000004) 
.equ GPIO_BRR_BR_3, (0x00000008) 
.equ GPIO_BRR_BR_4, (0x00000010) 
.equ GPIO_BRR_BR_5, (0x00000020) 
.equ GPIO_BRR_BR_6, (0x00000040) 
.equ GPIO_BRR_BR_7, (0x00000080) 
.equ GPIO_BRR_BR_8, (0x00000100) 
.equ GPIO_BRR_BR_9, (0x00000200) 
.equ GPIO_BRR_BR_10, (0x00000400) 
.equ GPIO_BRR_BR_11, (0x00000800) 
.equ GPIO_BRR_BR_12, (0x00001000) 
.equ GPIO_BRR_BR_13, (0x00002000) 
.equ GPIO_BRR_BR_14, (0x00004000) 
.equ GPIO_BRR_BR_15, (0x00008000) 
//****************************************************************************
//
//                      Inter-integrated Circuit Interface (I2C)
//
//****************************************************************************
//******************  Bit definition for I2C_CR1 register  ******************
.equ I2C_CR1_PE_Pos, (0) 
.equ I2C_CR1_PE_Msk, (0x1 << I2C_CR1_PE_Pos) //!< 0x00000001 
.equ I2C_CR1_PE, I2C_CR1_PE_Msk //!< Peripheral enable 
.equ I2C_CR1_TXIE_Pos, (1) 
.equ I2C_CR1_TXIE_Msk, (0x1 << I2C_CR1_TXIE_Pos) //!< 0x00000002 
.equ I2C_CR1_TXIE, I2C_CR1_TXIE_Msk //!< TX interrupt enable 
.equ I2C_CR1_RXIE_Pos, (2) 
.equ I2C_CR1_RXIE_Msk, (0x1 << I2C_CR1_RXIE_Pos) //!< 0x00000004 
.equ I2C_CR1_RXIE, I2C_CR1_RXIE_Msk //!< RX interrupt enable 
.equ I2C_CR1_ADDRIE_Pos, (3) 
.equ I2C_CR1_ADDRIE_Msk, (0x1 << I2C_CR1_ADDRIE_Pos) //!< 0x00000008 
.equ I2C_CR1_ADDRIE, I2C_CR1_ADDRIE_Msk //!< Address match interrupt enable 
.equ I2C_CR1_NACKIE_Pos, (4) 
.equ I2C_CR1_NACKIE_Msk, (0x1 << I2C_CR1_NACKIE_Pos) //!< 0x00000010 
.equ I2C_CR1_NACKIE, I2C_CR1_NACKIE_Msk //!< NACK received interrupt enable 
.equ I2C_CR1_STOPIE_Pos, (5) 
.equ I2C_CR1_STOPIE_Msk, (0x1 << I2C_CR1_STOPIE_Pos) //!< 0x00000020 
.equ I2C_CR1_STOPIE, I2C_CR1_STOPIE_Msk //!< STOP detection interrupt enable 
.equ I2C_CR1_TCIE_Pos, (6) 
.equ I2C_CR1_TCIE_Msk, (0x1 << I2C_CR1_TCIE_Pos) //!< 0x00000040 
.equ I2C_CR1_TCIE, I2C_CR1_TCIE_Msk //!< Transfer complete interrupt enable 
.equ I2C_CR1_ERRIE_Pos, (7) 
.equ I2C_CR1_ERRIE_Msk, (0x1 << I2C_CR1_ERRIE_Pos) //!< 0x00000080 
.equ I2C_CR1_ERRIE, I2C_CR1_ERRIE_Msk //!< Errors interrupt enable 
.equ I2C_CR1_DNF_Pos, (8) 
.equ I2C_CR1_DNF_Msk, (0xF << I2C_CR1_DNF_Pos) //!< 0x00000F00 
.equ I2C_CR1_DNF, I2C_CR1_DNF_Msk //!< Digital noise filter 
.equ I2C_CR1_ANFOFF_Pos, (12) 
.equ I2C_CR1_ANFOFF_Msk, (0x1 << I2C_CR1_ANFOFF_Pos) //!< 0x00001000 
.equ I2C_CR1_ANFOFF, I2C_CR1_ANFOFF_Msk //!< Analog noise filter OFF 
.equ I2C_CR1_SWRST_Pos, (13) 
.equ I2C_CR1_SWRST_Msk, (0x1 << I2C_CR1_SWRST_Pos) //!< 0x00002000 
.equ I2C_CR1_SWRST, I2C_CR1_SWRST_Msk //!< Software reset 
.equ I2C_CR1_TXDMAEN_Pos, (14) 
.equ I2C_CR1_TXDMAEN_Msk, (0x1 << I2C_CR1_TXDMAEN_Pos) //!< 0x00004000 
.equ I2C_CR1_TXDMAEN, I2C_CR1_TXDMAEN_Msk //!< DMA transmission requests enable 
.equ I2C_CR1_RXDMAEN_Pos, (15) 
.equ I2C_CR1_RXDMAEN_Msk, (0x1 << I2C_CR1_RXDMAEN_Pos) //!< 0x00008000 
.equ I2C_CR1_RXDMAEN, I2C_CR1_RXDMAEN_Msk //!< DMA reception requests enable 
.equ I2C_CR1_SBC_Pos, (16) 
.equ I2C_CR1_SBC_Msk, (0x1 << I2C_CR1_SBC_Pos) //!< 0x00010000 
.equ I2C_CR1_SBC, I2C_CR1_SBC_Msk //!< Slave byte control 
.equ I2C_CR1_NOSTRETCH_Pos, (17) 
.equ I2C_CR1_NOSTRETCH_Msk, (0x1 << I2C_CR1_NOSTRETCH_Pos) //!< 0x00020000 
.equ I2C_CR1_NOSTRETCH, I2C_CR1_NOSTRETCH_Msk //!< Clock stretching disable 
.equ I2C_CR1_WUPEN_Pos, (18) 
.equ I2C_CR1_WUPEN_Msk, (0x1 << I2C_CR1_WUPEN_Pos) //!< 0x00040000 
.equ I2C_CR1_WUPEN, I2C_CR1_WUPEN_Msk //!< Wakeup from STOP enable 
.equ I2C_CR1_GCEN_Pos, (19) 
.equ I2C_CR1_GCEN_Msk, (0x1 << I2C_CR1_GCEN_Pos) //!< 0x00080000 
.equ I2C_CR1_GCEN, I2C_CR1_GCEN_Msk //!< General call enable 
.equ I2C_CR1_SMBHEN_Pos, (20) 
.equ I2C_CR1_SMBHEN_Msk, (0x1 << I2C_CR1_SMBHEN_Pos) //!< 0x00100000 
.equ I2C_CR1_SMBHEN, I2C_CR1_SMBHEN_Msk //!< SMBus host address enable 
.equ I2C_CR1_SMBDEN_Pos, (21) 
.equ I2C_CR1_SMBDEN_Msk, (0x1 << I2C_CR1_SMBDEN_Pos) //!< 0x00200000 
.equ I2C_CR1_SMBDEN, I2C_CR1_SMBDEN_Msk //!< SMBus device default address enable 
.equ I2C_CR1_ALERTEN_Pos, (22) 
.equ I2C_CR1_ALERTEN_Msk, (0x1 << I2C_CR1_ALERTEN_Pos) //!< 0x00400000 
.equ I2C_CR1_ALERTEN, I2C_CR1_ALERTEN_Msk //!< SMBus alert enable 
.equ I2C_CR1_PECEN_Pos, (23) 
.equ I2C_CR1_PECEN_Msk, (0x1 << I2C_CR1_PECEN_Pos) //!< 0x00800000 
.equ I2C_CR1_PECEN, I2C_CR1_PECEN_Msk //!< PEC enable 
// Legacy defines
.equ I2C_CR1_DFN, I2C_CR1_DNF 
//*****************  Bit definition for I2C_CR2 register  *******************
.equ I2C_CR2_SADD_Pos, (0) 
.equ I2C_CR2_SADD_Msk, (0x3FF << I2C_CR2_SADD_Pos) //!< 0x000003FF 
.equ I2C_CR2_SADD, I2C_CR2_SADD_Msk //!< Slave address (master mode) 
.equ I2C_CR2_RD_WRN_Pos, (10) 
.equ I2C_CR2_RD_WRN_Msk, (0x1 << I2C_CR2_RD_WRN_Pos) //!< 0x00000400 
.equ I2C_CR2_RD_WRN, I2C_CR2_RD_WRN_Msk //!< Transfer direction (master mode) 
.equ I2C_CR2_ADD10_Pos, (11) 
.equ I2C_CR2_ADD10_Msk, (0x1 << I2C_CR2_ADD10_Pos) //!< 0x00000800 
.equ I2C_CR2_ADD10, I2C_CR2_ADD10_Msk //!< 10-bit addressing mode (master mode) 
.equ I2C_CR2_HEAD10R_Pos, (12) 
.equ I2C_CR2_HEAD10R_Msk, (0x1 << I2C_CR2_HEAD10R_Pos) //!< 0x00001000 
.equ I2C_CR2_HEAD10R, I2C_CR2_HEAD10R_Msk //!< 10-bit address header only read direction (master mode) 
.equ I2C_CR2_START_Pos, (13) 
.equ I2C_CR2_START_Msk, (0x1 << I2C_CR2_START_Pos) //!< 0x00002000 
.equ I2C_CR2_START, I2C_CR2_START_Msk //!< START generation 
.equ I2C_CR2_STOP_Pos, (14) 
.equ I2C_CR2_STOP_Msk, (0x1 << I2C_CR2_STOP_Pos) //!< 0x00004000 
.equ I2C_CR2_STOP, I2C_CR2_STOP_Msk //!< STOP generation (master mode) 
.equ I2C_CR2_NACK_Pos, (15) 
.equ I2C_CR2_NACK_Msk, (0x1 << I2C_CR2_NACK_Pos) //!< 0x00008000 
.equ I2C_CR2_NACK, I2C_CR2_NACK_Msk //!< NACK generation (slave mode) 
.equ I2C_CR2_NBYTES_Pos, (16) 
.equ I2C_CR2_NBYTES_Msk, (0xFF << I2C_CR2_NBYTES_Pos) //!< 0x00FF0000 
.equ I2C_CR2_NBYTES, I2C_CR2_NBYTES_Msk //!< Number of bytes 
.equ I2C_CR2_RELOAD_Pos, (24) 
.equ I2C_CR2_RELOAD_Msk, (0x1 << I2C_CR2_RELOAD_Pos) //!< 0x01000000 
.equ I2C_CR2_RELOAD, I2C_CR2_RELOAD_Msk //!< NBYTES reload mode 
.equ I2C_CR2_AUTOEND_Pos, (25) 
.equ I2C_CR2_AUTOEND_Msk, (0x1 << I2C_CR2_AUTOEND_Pos) //!< 0x02000000 
.equ I2C_CR2_AUTOEND, I2C_CR2_AUTOEND_Msk //!< Automatic end mode (master mode) 
.equ I2C_CR2_PECBYTE_Pos, (26) 
.equ I2C_CR2_PECBYTE_Msk, (0x1 << I2C_CR2_PECBYTE_Pos) //!< 0x04000000 
.equ I2C_CR2_PECBYTE, I2C_CR2_PECBYTE_Msk //!< Packet error checking byte 
//******************  Bit definition for I2C_OAR1 register  *****************
.equ I2C_OAR1_OA1_Pos, (0) 
.equ I2C_OAR1_OA1_Msk, (0x3FF << I2C_OAR1_OA1_Pos) //!< 0x000003FF 
.equ I2C_OAR1_OA1, I2C_OAR1_OA1_Msk //!< Interface own address 1 
.equ I2C_OAR1_OA1MODE_Pos, (10) 
.equ I2C_OAR1_OA1MODE_Msk, (0x1 << I2C_OAR1_OA1MODE_Pos) //!< 0x00000400 
.equ I2C_OAR1_OA1MODE, I2C_OAR1_OA1MODE_Msk //!< Own address 1 10-bit mode 
.equ I2C_OAR1_OA1EN_Pos, (15) 
.equ I2C_OAR1_OA1EN_Msk, (0x1 << I2C_OAR1_OA1EN_Pos) //!< 0x00008000 
.equ I2C_OAR1_OA1EN, I2C_OAR1_OA1EN_Msk //!< Own address 1 enable 
//******************  Bit definition for I2C_OAR2 register  ******************
.equ I2C_OAR2_OA2_Pos, (1) 
.equ I2C_OAR2_OA2_Msk, (0x7F << I2C_OAR2_OA2_Pos) //!< 0x000000FE 
.equ I2C_OAR2_OA2, I2C_OAR2_OA2_Msk //!< Interface own address 2 
.equ I2C_OAR2_OA2MSK_Pos, (8) 
.equ I2C_OAR2_OA2MSK_Msk, (0x7 << I2C_OAR2_OA2MSK_Pos) //!< 0x00000700 
.equ I2C_OAR2_OA2MSK, I2C_OAR2_OA2MSK_Msk //!< Own address 2 masks 
.equ I2C_OAR2_OA2NOMASK, (0x00000000) //!< No mask 
.equ I2C_OAR2_OA2MASK01_Pos, (8) 
.equ I2C_OAR2_OA2MASK01_Msk, (0x1 << I2C_OAR2_OA2MASK01_Pos) //!< 0x00000100 
.equ I2C_OAR2_OA2MASK01, I2C_OAR2_OA2MASK01_Msk //!< OA2[1] is masked, Only OA2[7:2] are compared 
.equ I2C_OAR2_OA2MASK02_Pos, (9) 
.equ I2C_OAR2_OA2MASK02_Msk, (0x1 << I2C_OAR2_OA2MASK02_Pos) //!< 0x00000200 
.equ I2C_OAR2_OA2MASK02, I2C_OAR2_OA2MASK02_Msk //!< OA2[2:1] is masked, Only OA2[7:3] are compared 
.equ I2C_OAR2_OA2MASK03_Pos, (8) 
.equ I2C_OAR2_OA2MASK03_Msk, (0x3 << I2C_OAR2_OA2MASK03_Pos) //!< 0x00000300 
.equ I2C_OAR2_OA2MASK03, I2C_OAR2_OA2MASK03_Msk //!< OA2[3:1] is masked, Only OA2[7:4] are compared 
.equ I2C_OAR2_OA2MASK04_Pos, (10) 
.equ I2C_OAR2_OA2MASK04_Msk, (0x1 << I2C_OAR2_OA2MASK04_Pos) //!< 0x00000400 
.equ I2C_OAR2_OA2MASK04, I2C_OAR2_OA2MASK04_Msk //!< OA2[4:1] is masked, Only OA2[7:5] are compared 
.equ I2C_OAR2_OA2MASK05_Pos, (8) 
.equ I2C_OAR2_OA2MASK05_Msk, (0x5 << I2C_OAR2_OA2MASK05_Pos) //!< 0x00000500 
.equ I2C_OAR2_OA2MASK05, I2C_OAR2_OA2MASK05_Msk //!< OA2[5:1] is masked, Only OA2[7:6] are compared 
.equ I2C_OAR2_OA2MASK06_Pos, (9) 
.equ I2C_OAR2_OA2MASK06_Msk, (0x3 << I2C_OAR2_OA2MASK06_Pos) //!< 0x00000600 
.equ I2C_OAR2_OA2MASK06, I2C_OAR2_OA2MASK06_Msk //!< OA2[6:1] is masked, Only OA2[7] are compared 
.equ I2C_OAR2_OA2MASK07_Pos, (8) 
.equ I2C_OAR2_OA2MASK07_Msk, (0x7 << I2C_OAR2_OA2MASK07_Pos) //!< 0x00000700 
.equ I2C_OAR2_OA2MASK07, I2C_OAR2_OA2MASK07_Msk //!< OA2[7:1] is masked, No comparison is done 
.equ I2C_OAR2_OA2EN_Pos, (15) 
.equ I2C_OAR2_OA2EN_Msk, (0x1 << I2C_OAR2_OA2EN_Pos) //!< 0x00008000 
.equ I2C_OAR2_OA2EN, I2C_OAR2_OA2EN_Msk //!< Own address 2 enable 
//******************  Bit definition for I2C_TIMINGR register ****************
.equ I2C_TIMINGR_SCLL_Pos, (0) 
.equ I2C_TIMINGR_SCLL_Msk, (0xFF << I2C_TIMINGR_SCLL_Pos) //!< 0x000000FF 
.equ I2C_TIMINGR_SCLL, I2C_TIMINGR_SCLL_Msk //!< SCL low period (master mode) 
.equ I2C_TIMINGR_SCLH_Pos, (8) 
.equ I2C_TIMINGR_SCLH_Msk, (0xFF << I2C_TIMINGR_SCLH_Pos) //!< 0x0000FF00 
.equ I2C_TIMINGR_SCLH, I2C_TIMINGR_SCLH_Msk //!< SCL high period (master mode) 
.equ I2C_TIMINGR_SDADEL_Pos, (16) 
.equ I2C_TIMINGR_SDADEL_Msk, (0xF << I2C_TIMINGR_SDADEL_Pos) //!< 0x000F0000 
.equ I2C_TIMINGR_SDADEL, I2C_TIMINGR_SDADEL_Msk //!< Data hold time 
.equ I2C_TIMINGR_SCLDEL_Pos, (20) 
.equ I2C_TIMINGR_SCLDEL_Msk, (0xF << I2C_TIMINGR_SCLDEL_Pos) //!< 0x00F00000 
.equ I2C_TIMINGR_SCLDEL, I2C_TIMINGR_SCLDEL_Msk //!< Data setup time 
.equ I2C_TIMINGR_PRESC_Pos, (28) 
.equ I2C_TIMINGR_PRESC_Msk, (0xF << I2C_TIMINGR_PRESC_Pos) //!< 0xF0000000 
.equ I2C_TIMINGR_PRESC, I2C_TIMINGR_PRESC_Msk //!< Timings prescaler 
//****************** Bit definition for I2C_TIMEOUTR register ****************
.equ I2C_TIMEOUTR_TIMEOUTA_Pos, (0) 
.equ I2C_TIMEOUTR_TIMEOUTA_Msk, (0xFFF << I2C_TIMEOUTR_TIMEOUTA_Pos) //!< 0x00000FFF 
.equ I2C_TIMEOUTR_TIMEOUTA, I2C_TIMEOUTR_TIMEOUTA_Msk //!< Bus timeout A 
.equ I2C_TIMEOUTR_TIDLE_Pos, (12) 
.equ I2C_TIMEOUTR_TIDLE_Msk, (0x1 << I2C_TIMEOUTR_TIDLE_Pos) //!< 0x00001000 
.equ I2C_TIMEOUTR_TIDLE, I2C_TIMEOUTR_TIDLE_Msk //!< Idle clock timeout detection 
.equ I2C_TIMEOUTR_TIMOUTEN_Pos, (15) 
.equ I2C_TIMEOUTR_TIMOUTEN_Msk, (0x1 << I2C_TIMEOUTR_TIMOUTEN_Pos) //!< 0x00008000 
.equ I2C_TIMEOUTR_TIMOUTEN, I2C_TIMEOUTR_TIMOUTEN_Msk //!< Clock timeout enable 
.equ I2C_TIMEOUTR_TIMEOUTB_Pos, (16) 
.equ I2C_TIMEOUTR_TIMEOUTB_Msk, (0xFFF << I2C_TIMEOUTR_TIMEOUTB_Pos) //!< 0x0FFF0000 
.equ I2C_TIMEOUTR_TIMEOUTB, I2C_TIMEOUTR_TIMEOUTB_Msk //!< Bus timeout B 
.equ I2C_TIMEOUTR_TEXTEN_Pos, (31) 
.equ I2C_TIMEOUTR_TEXTEN_Msk, (0x1 << I2C_TIMEOUTR_TEXTEN_Pos) //!< 0x80000000 
.equ I2C_TIMEOUTR_TEXTEN, I2C_TIMEOUTR_TEXTEN_Msk //!< Extended clock timeout enable 
//*****************  Bit definition for I2C_ISR register  ********************
.equ I2C_ISR_TXE_Pos, (0) 
.equ I2C_ISR_TXE_Msk, (0x1 << I2C_ISR_TXE_Pos) //!< 0x00000001 
.equ I2C_ISR_TXE, I2C_ISR_TXE_Msk //!< Transmit data register empty 
.equ I2C_ISR_TXIS_Pos, (1) 
.equ I2C_ISR_TXIS_Msk, (0x1 << I2C_ISR_TXIS_Pos) //!< 0x00000002 
.equ I2C_ISR_TXIS, I2C_ISR_TXIS_Msk //!< Transmit interrupt status 
.equ I2C_ISR_RXNE_Pos, (2) 
.equ I2C_ISR_RXNE_Msk, (0x1 << I2C_ISR_RXNE_Pos) //!< 0x00000004 
.equ I2C_ISR_RXNE, I2C_ISR_RXNE_Msk //!< Receive data register not empty 
.equ I2C_ISR_ADDR_Pos, (3) 
.equ I2C_ISR_ADDR_Msk, (0x1 << I2C_ISR_ADDR_Pos) //!< 0x00000008 
.equ I2C_ISR_ADDR, I2C_ISR_ADDR_Msk //!< Address matched (slave mode) 
.equ I2C_ISR_NACKF_Pos, (4) 
.equ I2C_ISR_NACKF_Msk, (0x1 << I2C_ISR_NACKF_Pos) //!< 0x00000010 
.equ I2C_ISR_NACKF, I2C_ISR_NACKF_Msk //!< NACK received flag 
.equ I2C_ISR_STOPF_Pos, (5) 
.equ I2C_ISR_STOPF_Msk, (0x1 << I2C_ISR_STOPF_Pos) //!< 0x00000020 
.equ I2C_ISR_STOPF, I2C_ISR_STOPF_Msk //!< STOP detection flag 
.equ I2C_ISR_TC_Pos, (6) 
.equ I2C_ISR_TC_Msk, (0x1 << I2C_ISR_TC_Pos) //!< 0x00000040 
.equ I2C_ISR_TC, I2C_ISR_TC_Msk //!< Transfer complete (master mode) 
.equ I2C_ISR_TCR_Pos, (7) 
.equ I2C_ISR_TCR_Msk, (0x1 << I2C_ISR_TCR_Pos) //!< 0x00000080 
.equ I2C_ISR_TCR, I2C_ISR_TCR_Msk //!< Transfer complete reload 
.equ I2C_ISR_BERR_Pos, (8) 
.equ I2C_ISR_BERR_Msk, (0x1 << I2C_ISR_BERR_Pos) //!< 0x00000100 
.equ I2C_ISR_BERR, I2C_ISR_BERR_Msk //!< Bus error 
.equ I2C_ISR_ARLO_Pos, (9) 
.equ I2C_ISR_ARLO_Msk, (0x1 << I2C_ISR_ARLO_Pos) //!< 0x00000200 
.equ I2C_ISR_ARLO, I2C_ISR_ARLO_Msk //!< Arbitration lost 
.equ I2C_ISR_OVR_Pos, (10) 
.equ I2C_ISR_OVR_Msk, (0x1 << I2C_ISR_OVR_Pos) //!< 0x00000400 
.equ I2C_ISR_OVR, I2C_ISR_OVR_Msk //!< Overrun/Underrun 
.equ I2C_ISR_PECERR_Pos, (11) 
.equ I2C_ISR_PECERR_Msk, (0x1 << I2C_ISR_PECERR_Pos) //!< 0x00000800 
.equ I2C_ISR_PECERR, I2C_ISR_PECERR_Msk //!< PEC error in reception 
.equ I2C_ISR_TIMEOUT_Pos, (12) 
.equ I2C_ISR_TIMEOUT_Msk, (0x1 << I2C_ISR_TIMEOUT_Pos) //!< 0x00001000 
.equ I2C_ISR_TIMEOUT, I2C_ISR_TIMEOUT_Msk //!< Timeout or Tlow detection flag 
.equ I2C_ISR_ALERT_Pos, (13) 
.equ I2C_ISR_ALERT_Msk, (0x1 << I2C_ISR_ALERT_Pos) //!< 0x00002000 
.equ I2C_ISR_ALERT, I2C_ISR_ALERT_Msk //!< SMBus alert 
.equ I2C_ISR_BUSY_Pos, (15) 
.equ I2C_ISR_BUSY_Msk, (0x1 << I2C_ISR_BUSY_Pos) //!< 0x00008000 
.equ I2C_ISR_BUSY, I2C_ISR_BUSY_Msk //!< Bus busy 
.equ I2C_ISR_DIR_Pos, (16) 
.equ I2C_ISR_DIR_Msk, (0x1 << I2C_ISR_DIR_Pos) //!< 0x00010000 
.equ I2C_ISR_DIR, I2C_ISR_DIR_Msk //!< Transfer direction (slave mode) 
.equ I2C_ISR_ADDCODE_Pos, (17) 
.equ I2C_ISR_ADDCODE_Msk, (0x7F << I2C_ISR_ADDCODE_Pos) //!< 0x00FE0000 
.equ I2C_ISR_ADDCODE, I2C_ISR_ADDCODE_Msk //!< Address match code (slave mode) 
//*****************  Bit definition for I2C_ICR register  ********************
.equ I2C_ICR_ADDRCF_Pos, (3) 
.equ I2C_ICR_ADDRCF_Msk, (0x1 << I2C_ICR_ADDRCF_Pos) //!< 0x00000008 
.equ I2C_ICR_ADDRCF, I2C_ICR_ADDRCF_Msk //!< Address matched clear flag 
.equ I2C_ICR_NACKCF_Pos, (4) 
.equ I2C_ICR_NACKCF_Msk, (0x1 << I2C_ICR_NACKCF_Pos) //!< 0x00000010 
.equ I2C_ICR_NACKCF, I2C_ICR_NACKCF_Msk //!< NACK clear flag 
.equ I2C_ICR_STOPCF_Pos, (5) 
.equ I2C_ICR_STOPCF_Msk, (0x1 << I2C_ICR_STOPCF_Pos) //!< 0x00000020 
.equ I2C_ICR_STOPCF, I2C_ICR_STOPCF_Msk //!< STOP detection clear flag 
.equ I2C_ICR_BERRCF_Pos, (8) 
.equ I2C_ICR_BERRCF_Msk, (0x1 << I2C_ICR_BERRCF_Pos) //!< 0x00000100 
.equ I2C_ICR_BERRCF, I2C_ICR_BERRCF_Msk //!< Bus error clear flag 
.equ I2C_ICR_ARLOCF_Pos, (9) 
.equ I2C_ICR_ARLOCF_Msk, (0x1 << I2C_ICR_ARLOCF_Pos) //!< 0x00000200 
.equ I2C_ICR_ARLOCF, I2C_ICR_ARLOCF_Msk //!< Arbitration lost clear flag 
.equ I2C_ICR_OVRCF_Pos, (10) 
.equ I2C_ICR_OVRCF_Msk, (0x1 << I2C_ICR_OVRCF_Pos) //!< 0x00000400 
.equ I2C_ICR_OVRCF, I2C_ICR_OVRCF_Msk //!< Overrun/Underrun clear flag 
.equ I2C_ICR_PECCF_Pos, (11) 
.equ I2C_ICR_PECCF_Msk, (0x1 << I2C_ICR_PECCF_Pos) //!< 0x00000800 
.equ I2C_ICR_PECCF, I2C_ICR_PECCF_Msk //!< PAC error clear flag 
.equ I2C_ICR_TIMOUTCF_Pos, (12) 
.equ I2C_ICR_TIMOUTCF_Msk, (0x1 << I2C_ICR_TIMOUTCF_Pos) //!< 0x00001000 
.equ I2C_ICR_TIMOUTCF, I2C_ICR_TIMOUTCF_Msk //!< Timeout clear flag 
.equ I2C_ICR_ALERTCF_Pos, (13) 
.equ I2C_ICR_ALERTCF_Msk, (0x1 << I2C_ICR_ALERTCF_Pos) //!< 0x00002000 
.equ I2C_ICR_ALERTCF, I2C_ICR_ALERTCF_Msk //!< Alert clear flag 
//*****************  Bit definition for I2C_PECR register  *******************
.equ I2C_PECR_PEC_Pos, (0) 
.equ I2C_PECR_PEC_Msk, (0xFF << I2C_PECR_PEC_Pos) //!< 0x000000FF 
.equ I2C_PECR_PEC, I2C_PECR_PEC_Msk //!< PEC register 
//*****************  Bit definition for I2C_RXDR register  ********************
.equ I2C_RXDR_RXDATA_Pos, (0) 
.equ I2C_RXDR_RXDATA_Msk, (0xFF << I2C_RXDR_RXDATA_Pos) //!< 0x000000FF 
.equ I2C_RXDR_RXDATA, I2C_RXDR_RXDATA_Msk //!< 8-bit receive data 
//*****************  Bit definition for I2C_TXDR register  ********************
.equ I2C_TXDR_TXDATA_Pos, (0) 
.equ I2C_TXDR_TXDATA_Msk, (0xFF << I2C_TXDR_TXDATA_Pos) //!< 0x000000FF 
.equ I2C_TXDR_TXDATA, I2C_TXDR_TXDATA_Msk //!< 8-bit transmit data 
//****************************************************************************
//
//                           Independent WATCHDOG (IWDG)
//
//****************************************************************************
//******************  Bit definition for IWDG_KR register  *******************
.equ IWDG_KR_KEY_Pos, (0) 
.equ IWDG_KR_KEY_Msk, (0xFFFF << IWDG_KR_KEY_Pos) //!< 0x0000FFFF 
.equ IWDG_KR_KEY, IWDG_KR_KEY_Msk //!< Key value (write only, read 0000h) 
//******************  Bit definition for IWDG_PR register  *******************
.equ IWDG_PR_PR_Pos, (0) 
.equ IWDG_PR_PR_Msk, (0x7 << IWDG_PR_PR_Pos) //!< 0x00000007 
.equ IWDG_PR_PR, IWDG_PR_PR_Msk //!< PR[2:0] (Prescaler divider) 
.equ IWDG_PR_PR_0, (0x1 << IWDG_PR_PR_Pos) //!< 0x00000001 
.equ IWDG_PR_PR_1, (0x2 << IWDG_PR_PR_Pos) //!< 0x00000002 
.equ IWDG_PR_PR_2, (0x4 << IWDG_PR_PR_Pos) //!< 0x00000004 
//******************  Bit definition for IWDG_RLR register  ******************
.equ IWDG_RLR_RL_Pos, (0) 
.equ IWDG_RLR_RL_Msk, (0xFFF << IWDG_RLR_RL_Pos) //!< 0x00000FFF 
.equ IWDG_RLR_RL, IWDG_RLR_RL_Msk //!< Watchdog counter reload value 
//******************  Bit definition for IWDG_SR register  *******************
.equ IWDG_SR_PVU_Pos, (0) 
.equ IWDG_SR_PVU_Msk, (0x1 << IWDG_SR_PVU_Pos) //!< 0x00000001 
.equ IWDG_SR_PVU, IWDG_SR_PVU_Msk //!< Watchdog prescaler value update 
.equ IWDG_SR_RVU_Pos, (1) 
.equ IWDG_SR_RVU_Msk, (0x1 << IWDG_SR_RVU_Pos) //!< 0x00000002 
.equ IWDG_SR_RVU, IWDG_SR_RVU_Msk //!< Watchdog counter reload value update 
.equ IWDG_SR_WVU_Pos, (2) 
.equ IWDG_SR_WVU_Msk, (0x1 << IWDG_SR_WVU_Pos) //!< 0x00000004 
.equ IWDG_SR_WVU, IWDG_SR_WVU_Msk //!< Watchdog counter window value update 
//******************  Bit definition for IWDG_KR register  *******************
.equ IWDG_WINR_WIN_Pos, (0) 
.equ IWDG_WINR_WIN_Msk, (0xFFF << IWDG_WINR_WIN_Pos) //!< 0x00000FFF 
.equ IWDG_WINR_WIN, IWDG_WINR_WIN_Msk //!< Watchdog counter window value 
//****************************************************************************
//
//                             Power Control
//
//****************************************************************************
//*******************  Bit definition for PWR_CR register  *******************
.equ PWR_CR_LPDS_Pos, (0) 
.equ PWR_CR_LPDS_Msk, (0x1 << PWR_CR_LPDS_Pos) //!< 0x00000001 
.equ PWR_CR_LPDS, PWR_CR_LPDS_Msk //!< Low-power Deepsleep 
.equ PWR_CR_PDDS_Pos, (1) 
.equ PWR_CR_PDDS_Msk, (0x1 << PWR_CR_PDDS_Pos) //!< 0x00000002 
.equ PWR_CR_PDDS, PWR_CR_PDDS_Msk //!< Power Down Deepsleep 
.equ PWR_CR_CWUF_Pos, (2) 
.equ PWR_CR_CWUF_Msk, (0x1 << PWR_CR_CWUF_Pos) //!< 0x00000004 
.equ PWR_CR_CWUF, PWR_CR_CWUF_Msk //!< Clear Wakeup Flag 
.equ PWR_CR_CSBF_Pos, (3) 
.equ PWR_CR_CSBF_Msk, (0x1 << PWR_CR_CSBF_Pos) //!< 0x00000008 
.equ PWR_CR_CSBF, PWR_CR_CSBF_Msk //!< Clear Standby Flag 
.equ PWR_CR_PVDE_Pos, (4) 
.equ PWR_CR_PVDE_Msk, (0x1 << PWR_CR_PVDE_Pos) //!< 0x00000010 
.equ PWR_CR_PVDE, PWR_CR_PVDE_Msk //!< Power Voltage Detector Enable 
.equ PWR_CR_PLS_Pos, (5) 
.equ PWR_CR_PLS_Msk, (0x7 << PWR_CR_PLS_Pos) //!< 0x000000E0 
.equ PWR_CR_PLS, PWR_CR_PLS_Msk //!< PLS[2:0] bits (PVD Level Selection) 
.equ PWR_CR_PLS_0, (0x1 << PWR_CR_PLS_Pos) //!< 0x00000020 
.equ PWR_CR_PLS_1, (0x2 << PWR_CR_PLS_Pos) //!< 0x00000040 
.equ PWR_CR_PLS_2, (0x4 << PWR_CR_PLS_Pos) //!< 0x00000080 
//!< PVD level configuration
.equ PWR_CR_PLS_LEV0, (0x00000000) //!< PVD level 0 
.equ PWR_CR_PLS_LEV1, (0x00000020) //!< PVD level 1 
.equ PWR_CR_PLS_LEV2, (0x00000040) //!< PVD level 2 
.equ PWR_CR_PLS_LEV3, (0x00000060) //!< PVD level 3 
.equ PWR_CR_PLS_LEV4, (0x00000080) //!< PVD level 4 
.equ PWR_CR_PLS_LEV5, (0x000000A0) //!< PVD level 5 
.equ PWR_CR_PLS_LEV6, (0x000000C0) //!< PVD level 6 
.equ PWR_CR_PLS_LEV7, (0x000000E0) //!< PVD level 7 
.equ PWR_CR_DBP_Pos, (8) 
.equ PWR_CR_DBP_Msk, (0x1 << PWR_CR_DBP_Pos) //!< 0x00000100 
.equ PWR_CR_DBP, PWR_CR_DBP_Msk //!< Disable Backup Domain write protection 
//******************  Bit definition for PWR_CSR register  *******************
.equ PWR_CSR_WUF_Pos, (0) 
.equ PWR_CSR_WUF_Msk, (0x1 << PWR_CSR_WUF_Pos) //!< 0x00000001 
.equ PWR_CSR_WUF, PWR_CSR_WUF_Msk //!< Wakeup Flag 
.equ PWR_CSR_SBF_Pos, (1) 
.equ PWR_CSR_SBF_Msk, (0x1 << PWR_CSR_SBF_Pos) //!< 0x00000002 
.equ PWR_CSR_SBF, PWR_CSR_SBF_Msk //!< Standby Flag 
.equ PWR_CSR_PVDO_Pos, (2) 
.equ PWR_CSR_PVDO_Msk, (0x1 << PWR_CSR_PVDO_Pos) //!< 0x00000004 
.equ PWR_CSR_PVDO, PWR_CSR_PVDO_Msk //!< PVD Output 
.equ PWR_CSR_VREFINTRDYF_Pos, (3) 
.equ PWR_CSR_VREFINTRDYF_Msk, (0x1 << PWR_CSR_VREFINTRDYF_Pos) //!< 0x00000008 
.equ PWR_CSR_VREFINTRDYF, PWR_CSR_VREFINTRDYF_Msk //!< Internal voltage reference (VREFINT) ready flag 
.equ PWR_CSR_EWUP1_Pos, (8) 
.equ PWR_CSR_EWUP1_Msk, (0x1 << PWR_CSR_EWUP1_Pos) //!< 0x00000100 
.equ PWR_CSR_EWUP1, PWR_CSR_EWUP1_Msk //!< Enable WKUP pin 1 
.equ PWR_CSR_EWUP2_Pos, (9) 
.equ PWR_CSR_EWUP2_Msk, (0x1 << PWR_CSR_EWUP2_Pos) //!< 0x00000200 
.equ PWR_CSR_EWUP2, PWR_CSR_EWUP2_Msk //!< Enable WKUP pin 2 
.equ PWR_CSR_EWUP3_Pos, (10) 
.equ PWR_CSR_EWUP3_Msk, (0x1 << PWR_CSR_EWUP3_Pos) //!< 0x00000400 
.equ PWR_CSR_EWUP3, PWR_CSR_EWUP3_Msk //!< Enable WKUP pin 3 
//****************************************************************************
//
//                         Reset and Clock Control
//
//****************************************************************************
//*******************  Bit definition for RCC_CR register  *******************
.equ RCC_CR_HSION_Pos, (0) 
.equ RCC_CR_HSION_Msk, (0x1 << RCC_CR_HSION_Pos) //!< 0x00000001 
.equ RCC_CR_HSION, RCC_CR_HSION_Msk 
.equ RCC_CR_HSIRDY_Pos, (1) 
.equ RCC_CR_HSIRDY_Msk, (0x1 << RCC_CR_HSIRDY_Pos) //!< 0x00000002 
.equ RCC_CR_HSIRDY, RCC_CR_HSIRDY_Msk 
.equ RCC_CR_HSITRIM_Pos, (3) 
.equ RCC_CR_HSITRIM_Msk, (0x1F << RCC_CR_HSITRIM_Pos) //!< 0x000000F8 
.equ RCC_CR_HSITRIM, RCC_CR_HSITRIM_Msk 
.equ RCC_CR_HSITRIM_0, (0x01 << RCC_CR_HSITRIM_Pos) //!< 0x00000008 
.equ RCC_CR_HSITRIM_1, (0x02 << RCC_CR_HSITRIM_Pos) //!< 0x00000010 
.equ RCC_CR_HSITRIM_2, (0x04 << RCC_CR_HSITRIM_Pos) //!< 0x00000020 
.equ RCC_CR_HSITRIM_3, (0x08 << RCC_CR_HSITRIM_Pos) //!< 0x00000040 
.equ RCC_CR_HSITRIM_4, (0x10 << RCC_CR_HSITRIM_Pos) //!< 0x00000080 
.equ RCC_CR_HSICAL_Pos, (8) 
.equ RCC_CR_HSICAL_Msk, (0xFF << RCC_CR_HSICAL_Pos) //!< 0x0000FF00 
.equ RCC_CR_HSICAL, RCC_CR_HSICAL_Msk 
.equ RCC_CR_HSICAL_0, (0x01 << RCC_CR_HSICAL_Pos) //!< 0x00000100 
.equ RCC_CR_HSICAL_1, (0x02 << RCC_CR_HSICAL_Pos) //!< 0x00000200 
.equ RCC_CR_HSICAL_2, (0x04 << RCC_CR_HSICAL_Pos) //!< 0x00000400 
.equ RCC_CR_HSICAL_3, (0x08 << RCC_CR_HSICAL_Pos) //!< 0x00000800 
.equ RCC_CR_HSICAL_4, (0x10 << RCC_CR_HSICAL_Pos) //!< 0x00001000 
.equ RCC_CR_HSICAL_5, (0x20 << RCC_CR_HSICAL_Pos) //!< 0x00002000 
.equ RCC_CR_HSICAL_6, (0x40 << RCC_CR_HSICAL_Pos) //!< 0x00004000 
.equ RCC_CR_HSICAL_7, (0x80 << RCC_CR_HSICAL_Pos) //!< 0x00008000 
.equ RCC_CR_HSEON_Pos, (16) 
.equ RCC_CR_HSEON_Msk, (0x1 << RCC_CR_HSEON_Pos) //!< 0x00010000 
.equ RCC_CR_HSEON, RCC_CR_HSEON_Msk 
.equ RCC_CR_HSERDY_Pos, (17) 
.equ RCC_CR_HSERDY_Msk, (0x1 << RCC_CR_HSERDY_Pos) //!< 0x00020000 
.equ RCC_CR_HSERDY, RCC_CR_HSERDY_Msk 
.equ RCC_CR_HSEBYP_Pos, (18) 
.equ RCC_CR_HSEBYP_Msk, (0x1 << RCC_CR_HSEBYP_Pos) //!< 0x00040000 
.equ RCC_CR_HSEBYP, RCC_CR_HSEBYP_Msk 
.equ RCC_CR_CSSON_Pos, (19) 
.equ RCC_CR_CSSON_Msk, (0x1 << RCC_CR_CSSON_Pos) //!< 0x00080000 
.equ RCC_CR_CSSON, RCC_CR_CSSON_Msk 
.equ RCC_CR_PLLON_Pos, (24) 
.equ RCC_CR_PLLON_Msk, (0x1 << RCC_CR_PLLON_Pos) //!< 0x01000000 
.equ RCC_CR_PLLON, RCC_CR_PLLON_Msk 
.equ RCC_CR_PLLRDY_Pos, (25) 
.equ RCC_CR_PLLRDY_Msk, (0x1 << RCC_CR_PLLRDY_Pos) //!< 0x02000000 
.equ RCC_CR_PLLRDY, RCC_CR_PLLRDY_Msk 
//*******************  Bit definition for RCC_CFGR register  *****************
//!< SW configuration
.equ RCC_CFGR_SW_Pos, (0) 
.equ RCC_CFGR_SW_Msk, (0x3 << RCC_CFGR_SW_Pos) //!< 0x00000003 
.equ RCC_CFGR_SW, RCC_CFGR_SW_Msk //!< SW[1:0] bits (System clock Switch) 
.equ RCC_CFGR_SW_0, (0x1 << RCC_CFGR_SW_Pos) //!< 0x00000001 
.equ RCC_CFGR_SW_1, (0x2 << RCC_CFGR_SW_Pos) //!< 0x00000002 
.equ RCC_CFGR_SW_HSI, (0x00000000) //!< HSI selected as system clock 
.equ RCC_CFGR_SW_HSE, (0x00000001) //!< HSE selected as system clock 
.equ RCC_CFGR_SW_PLL, (0x00000002) //!< PLL selected as system clock 
//!< SWS configuration
.equ RCC_CFGR_SWS_Pos, (2) 
.equ RCC_CFGR_SWS_Msk, (0x3 << RCC_CFGR_SWS_Pos) //!< 0x0000000C 
.equ RCC_CFGR_SWS, RCC_CFGR_SWS_Msk //!< SWS[1:0] bits (System Clock Switch Status) 
.equ RCC_CFGR_SWS_0, (0x1 << RCC_CFGR_SWS_Pos) //!< 0x00000004 
.equ RCC_CFGR_SWS_1, (0x2 << RCC_CFGR_SWS_Pos) //!< 0x00000008 
.equ RCC_CFGR_SWS_HSI, (0x00000000) //!< HSI oscillator used as system clock 
.equ RCC_CFGR_SWS_HSE, (0x00000004) //!< HSE oscillator used as system clock 
.equ RCC_CFGR_SWS_PLL, (0x00000008) //!< PLL used as system clock 
//!< HPRE configuration
.equ RCC_CFGR_HPRE_Pos, (4) 
.equ RCC_CFGR_HPRE_Msk, (0xF << RCC_CFGR_HPRE_Pos) //!< 0x000000F0 
.equ RCC_CFGR_HPRE, RCC_CFGR_HPRE_Msk //!< HPRE[3:0] bits (AHB prescaler) 
.equ RCC_CFGR_HPRE_0, (0x1 << RCC_CFGR_HPRE_Pos) //!< 0x00000010 
.equ RCC_CFGR_HPRE_1, (0x2 << RCC_CFGR_HPRE_Pos) //!< 0x00000020 
.equ RCC_CFGR_HPRE_2, (0x4 << RCC_CFGR_HPRE_Pos) //!< 0x00000040 
.equ RCC_CFGR_HPRE_3, (0x8 << RCC_CFGR_HPRE_Pos) //!< 0x00000080 
.equ RCC_CFGR_HPRE_DIV1, (0x00000000) //!< SYSCLK not divided 
.equ RCC_CFGR_HPRE_DIV2, (0x00000080) //!< SYSCLK divided by 2 
.equ RCC_CFGR_HPRE_DIV4, (0x00000090) //!< SYSCLK divided by 4 
.equ RCC_CFGR_HPRE_DIV8, (0x000000A0) //!< SYSCLK divided by 8 
.equ RCC_CFGR_HPRE_DIV16, (0x000000B0) //!< SYSCLK divided by 16 
.equ RCC_CFGR_HPRE_DIV64, (0x000000C0) //!< SYSCLK divided by 64 
.equ RCC_CFGR_HPRE_DIV128, (0x000000D0) //!< SYSCLK divided by 128 
.equ RCC_CFGR_HPRE_DIV256, (0x000000E0) //!< SYSCLK divided by 256 
.equ RCC_CFGR_HPRE_DIV512, (0x000000F0) //!< SYSCLK divided by 512 
//!< PPRE1 configuration
.equ RCC_CFGR_PPRE1_Pos, (8) 
.equ RCC_CFGR_PPRE1_Msk, (0x7 << RCC_CFGR_PPRE1_Pos) //!< 0x00000700 
.equ RCC_CFGR_PPRE1, RCC_CFGR_PPRE1_Msk //!< PRE1[2:0] bits (APB1 prescaler) 
.equ RCC_CFGR_PPRE1_0, (0x1 << RCC_CFGR_PPRE1_Pos) //!< 0x00000100 
.equ RCC_CFGR_PPRE1_1, (0x2 << RCC_CFGR_PPRE1_Pos) //!< 0x00000200 
.equ RCC_CFGR_PPRE1_2, (0x4 << RCC_CFGR_PPRE1_Pos) //!< 0x00000400 
.equ RCC_CFGR_PPRE1_DIV1, (0x00000000) //!< HCLK not divided 
.equ RCC_CFGR_PPRE1_DIV2, (0x00000400) //!< HCLK divided by 2 
.equ RCC_CFGR_PPRE1_DIV4, (0x00000500) //!< HCLK divided by 4 
.equ RCC_CFGR_PPRE1_DIV8, (0x00000600) //!< HCLK divided by 8 
.equ RCC_CFGR_PPRE1_DIV16, (0x00000700) //!< HCLK divided by 16 
//!< PPRE2 configuration
.equ RCC_CFGR_PPRE2_Pos, (11) 
.equ RCC_CFGR_PPRE2_Msk, (0x7 << RCC_CFGR_PPRE2_Pos) //!< 0x00003800 
.equ RCC_CFGR_PPRE2, RCC_CFGR_PPRE2_Msk //!< PRE2[2:0] bits (APB2 prescaler) 
.equ RCC_CFGR_PPRE2_0, (0x1 << RCC_CFGR_PPRE2_Pos) //!< 0x00000800 
.equ RCC_CFGR_PPRE2_1, (0x2 << RCC_CFGR_PPRE2_Pos) //!< 0x00001000 
.equ RCC_CFGR_PPRE2_2, (0x4 << RCC_CFGR_PPRE2_Pos) //!< 0x00002000 
.equ RCC_CFGR_PPRE2_DIV1, (0x00000000) //!< HCLK not divided 
.equ RCC_CFGR_PPRE2_DIV2, (0x00002000) //!< HCLK divided by 2 
.equ RCC_CFGR_PPRE2_DIV4, (0x00002800) //!< HCLK divided by 4 
.equ RCC_CFGR_PPRE2_DIV8, (0x00003000) //!< HCLK divided by 8 
.equ RCC_CFGR_PPRE2_DIV16, (0x00003800) //!< HCLK divided by 16 
.equ RCC_CFGR_PLLSRC_Pos, (15) 
.equ RCC_CFGR_PLLSRC_Msk, (0x3 << RCC_CFGR_PLLSRC_Pos) //!< 0x00018000 
.equ RCC_CFGR_PLLSRC, RCC_CFGR_PLLSRC_Msk //!< PLL entry clock source 
.equ RCC_CFGR_PLLSRC_HSI_PREDIV, (0x00008000) //!< HSI/PREDIV clock as PLL entry clock source 
.equ RCC_CFGR_PLLSRC_HSE_PREDIV, (0x00010000) //!< HSE/PREDIV clock selected as PLL entry clock source 
.equ RCC_CFGR_PLLXTPRE_Pos, (17) 
.equ RCC_CFGR_PLLXTPRE_Msk, (0x1 << RCC_CFGR_PLLXTPRE_Pos) //!< 0x00020000 
.equ RCC_CFGR_PLLXTPRE, RCC_CFGR_PLLXTPRE_Msk //!< HSE divider for PLL entry 
.equ RCC_CFGR_PLLXTPRE_HSE_PREDIV_DIV1, (0x00000000) //!< HSE/PREDIV clock not divided for PLL entry 
.equ RCC_CFGR_PLLXTPRE_HSE_PREDIV_DIV2, (0x00020000) //!< HSE/PREDIV clock divided by 2 for PLL entry 
//!< PLLMUL configuration
.equ RCC_CFGR_PLLMUL_Pos, (18) 
.equ RCC_CFGR_PLLMUL_Msk, (0xF << RCC_CFGR_PLLMUL_Pos) //!< 0x003C0000 
.equ RCC_CFGR_PLLMUL, RCC_CFGR_PLLMUL_Msk //!< PLLMUL[3:0] bits (PLL multiplication factor) 
.equ RCC_CFGR_PLLMUL_0, (0x1 << RCC_CFGR_PLLMUL_Pos) //!< 0x00040000 
.equ RCC_CFGR_PLLMUL_1, (0x2 << RCC_CFGR_PLLMUL_Pos) //!< 0x00080000 
.equ RCC_CFGR_PLLMUL_2, (0x4 << RCC_CFGR_PLLMUL_Pos) //!< 0x00100000 
.equ RCC_CFGR_PLLMUL_3, (0x8 << RCC_CFGR_PLLMUL_Pos) //!< 0x00200000 
.equ RCC_CFGR_PLLMUL2, (0x00000000) //!< PLL input clock*2 
.equ RCC_CFGR_PLLMUL3, (0x00040000) //!< PLL input clock*3 
.equ RCC_CFGR_PLLMUL4, (0x00080000) //!< PLL input clock*4 
.equ RCC_CFGR_PLLMUL5, (0x000C0000) //!< PLL input clock*5 
.equ RCC_CFGR_PLLMUL6, (0x00100000) //!< PLL input clock*6 
.equ RCC_CFGR_PLLMUL7, (0x00140000) //!< PLL input clock*7 
.equ RCC_CFGR_PLLMUL8, (0x00180000) //!< PLL input clock*8 
.equ RCC_CFGR_PLLMUL9, (0x001C0000) //!< PLL input clock*9 
.equ RCC_CFGR_PLLMUL10, (0x00200000) //!< PLL input clock10 
.equ RCC_CFGR_PLLMUL11, (0x00240000) //!< PLL input clock*11 
.equ RCC_CFGR_PLLMUL12, (0x00280000) //!< PLL input clock*12 
.equ RCC_CFGR_PLLMUL13, (0x002C0000) //!< PLL input clock*13 
.equ RCC_CFGR_PLLMUL14, (0x00300000) //!< PLL input clock*14 
.equ RCC_CFGR_PLLMUL15, (0x00340000) //!< PLL input clock*15 
.equ RCC_CFGR_PLLMUL16, (0x00380000) //!< PLL input clock*16 
//!< USB configuration
.equ RCC_CFGR_USBPRE_Pos, (22) 
.equ RCC_CFGR_USBPRE_Msk, (0x1 << RCC_CFGR_USBPRE_Pos) //!< 0x00400000 
.equ RCC_CFGR_USBPRE, RCC_CFGR_USBPRE_Msk //!< USB prescaler 
.equ RCC_CFGR_USBPRE_DIV1_5, (0x00000000) //!< USB prescaler is PLL clock divided by 1.5 
.equ RCC_CFGR_USBPRE_DIV1, (0x00400000) //!< USB prescaler is PLL clock divided by 1 
//!< I2S configuration
.equ RCC_CFGR_I2SSRC_Pos, (23) 
.equ RCC_CFGR_I2SSRC_Msk, (0x1 << RCC_CFGR_I2SSRC_Pos) //!< 0x00800000 
.equ RCC_CFGR_I2SSRC, RCC_CFGR_I2SSRC_Msk //!< I2S external clock source selection 
.equ RCC_CFGR_I2SSRC_SYSCLK, (0x00000000) //!< System clock selected as I2S clock source 
.equ RCC_CFGR_I2SSRC_EXT, (0x00800000) //!< External clock selected as I2S clock source 
//!< MCO configuration
.equ RCC_CFGR_MCO_Pos, (24) 
.equ RCC_CFGR_MCO_Msk, (0x7 << RCC_CFGR_MCO_Pos) //!< 0x07000000 
.equ RCC_CFGR_MCO, RCC_CFGR_MCO_Msk //!< MCO[2:0] bits (Microcontroller Clock Output) 
.equ RCC_CFGR_MCO_0, (0x1 << RCC_CFGR_MCO_Pos) //!< 0x01000000 
.equ RCC_CFGR_MCO_1, (0x2 << RCC_CFGR_MCO_Pos) //!< 0x02000000 
.equ RCC_CFGR_MCO_2, (0x4 << RCC_CFGR_MCO_Pos) //!< 0x04000000 
.equ RCC_CFGR_MCO_NOCLOCK, (0x00000000) //!< No clock 
.equ RCC_CFGR_MCO_LSI, (0x02000000) //!< LSI clock selected as MCO source 
.equ RCC_CFGR_MCO_LSE, (0x03000000) //!< LSE clock selected as MCO source 
.equ RCC_CFGR_MCO_SYSCLK, (0x04000000) //!< System clock selected as MCO source 
.equ RCC_CFGR_MCO_HSI, (0x05000000) //!< HSI clock selected as MCO source 
.equ RCC_CFGR_MCO_HSE, (0x06000000) //!< HSE clock selected as MCO source 
.equ RCC_CFGR_MCO_PLL, (0x07000000) //!< PLL clock divided by 2 selected as MCO source 
.equ RCC_CFGR_MCOPRE_Pos, (28) 
.equ RCC_CFGR_MCOPRE_Msk, (0x7 << RCC_CFGR_MCOPRE_Pos) //!< 0x70000000 
.equ RCC_CFGR_MCOPRE, RCC_CFGR_MCOPRE_Msk //!< MCOPRE[3:0] bits (Microcontroller Clock Output Prescaler) 
.equ RCC_CFGR_MCOPRE_0, (0x1 << RCC_CFGR_MCOPRE_Pos) //!< 0x10000000 
.equ RCC_CFGR_MCOPRE_1, (0x2 << RCC_CFGR_MCOPRE_Pos) //!< 0x20000000 
.equ RCC_CFGR_MCOPRE_2, (0x4 << RCC_CFGR_MCOPRE_Pos) //!< 0x40000000 
.equ RCC_CFGR_MCOPRE_DIV1, (0x00000000) //!< MCO is divided by 1 
.equ RCC_CFGR_MCOPRE_DIV2, (0x10000000) //!< MCO is divided by 2 
.equ RCC_CFGR_MCOPRE_DIV4, (0x20000000) //!< MCO is divided by 4 
.equ RCC_CFGR_MCOPRE_DIV8, (0x30000000) //!< MCO is divided by 8 
.equ RCC_CFGR_MCOPRE_DIV16, (0x40000000) //!< MCO is divided by 16 
.equ RCC_CFGR_MCOPRE_DIV32, (0x50000000) //!< MCO is divided by 32 
.equ RCC_CFGR_MCOPRE_DIV64, (0x60000000) //!< MCO is divided by 64 
.equ RCC_CFGR_MCOPRE_DIV128, (0x70000000) //!< MCO is divided by 128 
.equ RCC_CFGR_PLLNODIV_Pos, (31) 
.equ RCC_CFGR_PLLNODIV_Msk, (0x1 << RCC_CFGR_PLLNODIV_Pos) //!< 0x80000000 
.equ RCC_CFGR_PLLNODIV, RCC_CFGR_PLLNODIV_Msk //!< Do not divide PLL to MCO 
// Reference defines
.equ RCC_CFGR_MCOSEL, RCC_CFGR_MCO 
.equ RCC_CFGR_MCOSEL_0, RCC_CFGR_MCO_0 
.equ RCC_CFGR_MCOSEL_1, RCC_CFGR_MCO_1 
.equ RCC_CFGR_MCOSEL_2, RCC_CFGR_MCO_2 
.equ RCC_CFGR_MCOSEL_NOCLOCK, RCC_CFGR_MCO_NOCLOCK 
.equ RCC_CFGR_MCOSEL_LSI, RCC_CFGR_MCO_LSI 
.equ RCC_CFGR_MCOSEL_LSE, RCC_CFGR_MCO_LSE 
.equ RCC_CFGR_MCOSEL_SYSCLK, RCC_CFGR_MCO_SYSCLK 
.equ RCC_CFGR_MCOSEL_HSI, RCC_CFGR_MCO_HSI 
.equ RCC_CFGR_MCOSEL_HSE, RCC_CFGR_MCO_HSE 
.equ RCC_CFGR_MCOSEL_PLL_DIV2, RCC_CFGR_MCO_PLL 
//********************  Bit definition for RCC_CIR register  *******************
.equ RCC_CIR_LSIRDYF_Pos, (0) 
.equ RCC_CIR_LSIRDYF_Msk, (0x1 << RCC_CIR_LSIRDYF_Pos) //!< 0x00000001 
.equ RCC_CIR_LSIRDYF, RCC_CIR_LSIRDYF_Msk //!< LSI Ready Interrupt flag 
.equ RCC_CIR_LSERDYF_Pos, (1) 
.equ RCC_CIR_LSERDYF_Msk, (0x1 << RCC_CIR_LSERDYF_Pos) //!< 0x00000002 
.equ RCC_CIR_LSERDYF, RCC_CIR_LSERDYF_Msk //!< LSE Ready Interrupt flag 
.equ RCC_CIR_HSIRDYF_Pos, (2) 
.equ RCC_CIR_HSIRDYF_Msk, (0x1 << RCC_CIR_HSIRDYF_Pos) //!< 0x00000004 
.equ RCC_CIR_HSIRDYF, RCC_CIR_HSIRDYF_Msk //!< HSI Ready Interrupt flag 
.equ RCC_CIR_HSERDYF_Pos, (3) 
.equ RCC_CIR_HSERDYF_Msk, (0x1 << RCC_CIR_HSERDYF_Pos) //!< 0x00000008 
.equ RCC_CIR_HSERDYF, RCC_CIR_HSERDYF_Msk //!< HSE Ready Interrupt flag 
.equ RCC_CIR_PLLRDYF_Pos, (4) 
.equ RCC_CIR_PLLRDYF_Msk, (0x1 << RCC_CIR_PLLRDYF_Pos) //!< 0x00000010 
.equ RCC_CIR_PLLRDYF, RCC_CIR_PLLRDYF_Msk //!< PLL Ready Interrupt flag 
.equ RCC_CIR_CSSF_Pos, (7) 
.equ RCC_CIR_CSSF_Msk, (0x1 << RCC_CIR_CSSF_Pos) //!< 0x00000080 
.equ RCC_CIR_CSSF, RCC_CIR_CSSF_Msk //!< Clock Security System Interrupt flag 
.equ RCC_CIR_LSIRDYIE_Pos, (8) 
.equ RCC_CIR_LSIRDYIE_Msk, (0x1 << RCC_CIR_LSIRDYIE_Pos) //!< 0x00000100 
.equ RCC_CIR_LSIRDYIE, RCC_CIR_LSIRDYIE_Msk //!< LSI Ready Interrupt Enable 
.equ RCC_CIR_LSERDYIE_Pos, (9) 
.equ RCC_CIR_LSERDYIE_Msk, (0x1 << RCC_CIR_LSERDYIE_Pos) //!< 0x00000200 
.equ RCC_CIR_LSERDYIE, RCC_CIR_LSERDYIE_Msk //!< LSE Ready Interrupt Enable 
.equ RCC_CIR_HSIRDYIE_Pos, (10) 
.equ RCC_CIR_HSIRDYIE_Msk, (0x1 << RCC_CIR_HSIRDYIE_Pos) //!< 0x00000400 
.equ RCC_CIR_HSIRDYIE, RCC_CIR_HSIRDYIE_Msk //!< HSI Ready Interrupt Enable 
.equ RCC_CIR_HSERDYIE_Pos, (11) 
.equ RCC_CIR_HSERDYIE_Msk, (0x1 << RCC_CIR_HSERDYIE_Pos) //!< 0x00000800 
.equ RCC_CIR_HSERDYIE, RCC_CIR_HSERDYIE_Msk //!< HSE Ready Interrupt Enable 
.equ RCC_CIR_PLLRDYIE_Pos, (12) 
.equ RCC_CIR_PLLRDYIE_Msk, (0x1 << RCC_CIR_PLLRDYIE_Pos) //!< 0x00001000 
.equ RCC_CIR_PLLRDYIE, RCC_CIR_PLLRDYIE_Msk //!< PLL Ready Interrupt Enable 
.equ RCC_CIR_LSIRDYC_Pos, (16) 
.equ RCC_CIR_LSIRDYC_Msk, (0x1 << RCC_CIR_LSIRDYC_Pos) //!< 0x00010000 
.equ RCC_CIR_LSIRDYC, RCC_CIR_LSIRDYC_Msk //!< LSI Ready Interrupt Clear 
.equ RCC_CIR_LSERDYC_Pos, (17) 
.equ RCC_CIR_LSERDYC_Msk, (0x1 << RCC_CIR_LSERDYC_Pos) //!< 0x00020000 
.equ RCC_CIR_LSERDYC, RCC_CIR_LSERDYC_Msk //!< LSE Ready Interrupt Clear 
.equ RCC_CIR_HSIRDYC_Pos, (18) 
.equ RCC_CIR_HSIRDYC_Msk, (0x1 << RCC_CIR_HSIRDYC_Pos) //!< 0x00040000 
.equ RCC_CIR_HSIRDYC, RCC_CIR_HSIRDYC_Msk //!< HSI Ready Interrupt Clear 
.equ RCC_CIR_HSERDYC_Pos, (19) 
.equ RCC_CIR_HSERDYC_Msk, (0x1 << RCC_CIR_HSERDYC_Pos) //!< 0x00080000 
.equ RCC_CIR_HSERDYC, RCC_CIR_HSERDYC_Msk //!< HSE Ready Interrupt Clear 
.equ RCC_CIR_PLLRDYC_Pos, (20) 
.equ RCC_CIR_PLLRDYC_Msk, (0x1 << RCC_CIR_PLLRDYC_Pos) //!< 0x00100000 
.equ RCC_CIR_PLLRDYC, RCC_CIR_PLLRDYC_Msk //!< PLL Ready Interrupt Clear 
.equ RCC_CIR_CSSC_Pos, (23) 
.equ RCC_CIR_CSSC_Msk, (0x1 << RCC_CIR_CSSC_Pos) //!< 0x00800000 
.equ RCC_CIR_CSSC, RCC_CIR_CSSC_Msk //!< Clock Security System Interrupt Clear 
//*****************  Bit definition for RCC_APB2RSTR register  ****************
.equ RCC_APB2RSTR_SYSCFGRST_Pos, (0) 
.equ RCC_APB2RSTR_SYSCFGRST_Msk, (0x1 << RCC_APB2RSTR_SYSCFGRST_Pos) //!< 0x00000001 
.equ RCC_APB2RSTR_SYSCFGRST, RCC_APB2RSTR_SYSCFGRST_Msk //!< SYSCFG reset 
.equ RCC_APB2RSTR_TIM1RST_Pos, (11) 
.equ RCC_APB2RSTR_TIM1RST_Msk, (0x1 << RCC_APB2RSTR_TIM1RST_Pos) //!< 0x00000800 
.equ RCC_APB2RSTR_TIM1RST, RCC_APB2RSTR_TIM1RST_Msk //!< TIM1 reset 
.equ RCC_APB2RSTR_SPI1RST_Pos, (12) 
.equ RCC_APB2RSTR_SPI1RST_Msk, (0x1 << RCC_APB2RSTR_SPI1RST_Pos) //!< 0x00001000 
.equ RCC_APB2RSTR_SPI1RST, RCC_APB2RSTR_SPI1RST_Msk //!< SPI1 reset 
.equ RCC_APB2RSTR_TIM8RST_Pos, (13) 
.equ RCC_APB2RSTR_TIM8RST_Msk, (0x1 << RCC_APB2RSTR_TIM8RST_Pos) //!< 0x00002000 
.equ RCC_APB2RSTR_TIM8RST, RCC_APB2RSTR_TIM8RST_Msk //!< TIM8 reset 
.equ RCC_APB2RSTR_USART1RST_Pos, (14) 
.equ RCC_APB2RSTR_USART1RST_Msk, (0x1 << RCC_APB2RSTR_USART1RST_Pos) //!< 0x00004000 
.equ RCC_APB2RSTR_USART1RST, RCC_APB2RSTR_USART1RST_Msk //!< USART1 reset 
.equ RCC_APB2RSTR_SPI4RST_Pos, (15) 
.equ RCC_APB2RSTR_SPI4RST_Msk, (0x1 << RCC_APB2RSTR_SPI4RST_Pos) //!< 0x00008000 
.equ RCC_APB2RSTR_SPI4RST, RCC_APB2RSTR_SPI4RST_Msk //!< SPI4 reset 
.equ RCC_APB2RSTR_TIM15RST_Pos, (16) 
.equ RCC_APB2RSTR_TIM15RST_Msk, (0x1 << RCC_APB2RSTR_TIM15RST_Pos) //!< 0x00010000 
.equ RCC_APB2RSTR_TIM15RST, RCC_APB2RSTR_TIM15RST_Msk //!< TIM15 reset 
.equ RCC_APB2RSTR_TIM16RST_Pos, (17) 
.equ RCC_APB2RSTR_TIM16RST_Msk, (0x1 << RCC_APB2RSTR_TIM16RST_Pos) //!< 0x00020000 
.equ RCC_APB2RSTR_TIM16RST, RCC_APB2RSTR_TIM16RST_Msk //!< TIM16 reset 
.equ RCC_APB2RSTR_TIM17RST_Pos, (18) 
.equ RCC_APB2RSTR_TIM17RST_Msk, (0x1 << RCC_APB2RSTR_TIM17RST_Pos) //!< 0x00040000 
.equ RCC_APB2RSTR_TIM17RST, RCC_APB2RSTR_TIM17RST_Msk //!< TIM17 reset 
.equ RCC_APB2RSTR_TIM20RST_Pos, (20) 
.equ RCC_APB2RSTR_TIM20RST_Msk, (0x1 << RCC_APB2RSTR_TIM20RST_Pos) //!< 0x00100000 
.equ RCC_APB2RSTR_TIM20RST, RCC_APB2RSTR_TIM20RST_Msk //!< TIM20 reset 
//*****************  Bit definition for RCC_APB1RSTR register  *****************
.equ RCC_APB1RSTR_TIM2RST_Pos, (0) 
.equ RCC_APB1RSTR_TIM2RST_Msk, (0x1 << RCC_APB1RSTR_TIM2RST_Pos) //!< 0x00000001 
.equ RCC_APB1RSTR_TIM2RST, RCC_APB1RSTR_TIM2RST_Msk //!< Timer 2 reset 
.equ RCC_APB1RSTR_TIM3RST_Pos, (1) 
.equ RCC_APB1RSTR_TIM3RST_Msk, (0x1 << RCC_APB1RSTR_TIM3RST_Pos) //!< 0x00000002 
.equ RCC_APB1RSTR_TIM3RST, RCC_APB1RSTR_TIM3RST_Msk //!< Timer 3 reset 
.equ RCC_APB1RSTR_TIM4RST_Pos, (2) 
.equ RCC_APB1RSTR_TIM4RST_Msk, (0x1 << RCC_APB1RSTR_TIM4RST_Pos) //!< 0x00000004 
.equ RCC_APB1RSTR_TIM4RST, RCC_APB1RSTR_TIM4RST_Msk //!< Timer 4 reset 
.equ RCC_APB1RSTR_TIM6RST_Pos, (4) 
.equ RCC_APB1RSTR_TIM6RST_Msk, (0x1 << RCC_APB1RSTR_TIM6RST_Pos) //!< 0x00000010 
.equ RCC_APB1RSTR_TIM6RST, RCC_APB1RSTR_TIM6RST_Msk //!< Timer 6 reset 
.equ RCC_APB1RSTR_TIM7RST_Pos, (5) 
.equ RCC_APB1RSTR_TIM7RST_Msk, (0x1 << RCC_APB1RSTR_TIM7RST_Pos) //!< 0x00000020 
.equ RCC_APB1RSTR_TIM7RST, RCC_APB1RSTR_TIM7RST_Msk //!< Timer 7 reset 
.equ RCC_APB1RSTR_WWDGRST_Pos, (11) 
.equ RCC_APB1RSTR_WWDGRST_Msk, (0x1 << RCC_APB1RSTR_WWDGRST_Pos) //!< 0x00000800 
.equ RCC_APB1RSTR_WWDGRST, RCC_APB1RSTR_WWDGRST_Msk //!< Window Watchdog reset 
.equ RCC_APB1RSTR_SPI2RST_Pos, (14) 
.equ RCC_APB1RSTR_SPI2RST_Msk, (0x1 << RCC_APB1RSTR_SPI2RST_Pos) //!< 0x00004000 
.equ RCC_APB1RSTR_SPI2RST, RCC_APB1RSTR_SPI2RST_Msk //!< SPI2 reset 
.equ RCC_APB1RSTR_SPI3RST_Pos, (15) 
.equ RCC_APB1RSTR_SPI3RST_Msk, (0x1 << RCC_APB1RSTR_SPI3RST_Pos) //!< 0x00008000 
.equ RCC_APB1RSTR_SPI3RST, RCC_APB1RSTR_SPI3RST_Msk //!< SPI3 reset 
.equ RCC_APB1RSTR_USART2RST_Pos, (17) 
.equ RCC_APB1RSTR_USART2RST_Msk, (0x1 << RCC_APB1RSTR_USART2RST_Pos) //!< 0x00020000 
.equ RCC_APB1RSTR_USART2RST, RCC_APB1RSTR_USART2RST_Msk //!< USART 2 reset 
.equ RCC_APB1RSTR_USART3RST_Pos, (18) 
.equ RCC_APB1RSTR_USART3RST_Msk, (0x1 << RCC_APB1RSTR_USART3RST_Pos) //!< 0x00040000 
.equ RCC_APB1RSTR_USART3RST, RCC_APB1RSTR_USART3RST_Msk //!< USART 3 reset 
.equ RCC_APB1RSTR_UART4RST_Pos, (19) 
.equ RCC_APB1RSTR_UART4RST_Msk, (0x1 << RCC_APB1RSTR_UART4RST_Pos) //!< 0x00080000 
.equ RCC_APB1RSTR_UART4RST, RCC_APB1RSTR_UART4RST_Msk //!< UART 4 reset 
.equ RCC_APB1RSTR_UART5RST_Pos, (20) 
.equ RCC_APB1RSTR_UART5RST_Msk, (0x1 << RCC_APB1RSTR_UART5RST_Pos) //!< 0x00100000 
.equ RCC_APB1RSTR_UART5RST, RCC_APB1RSTR_UART5RST_Msk //!< UART 5 reset 
.equ RCC_APB1RSTR_I2C1RST_Pos, (21) 
.equ RCC_APB1RSTR_I2C1RST_Msk, (0x1 << RCC_APB1RSTR_I2C1RST_Pos) //!< 0x00200000 
.equ RCC_APB1RSTR_I2C1RST, RCC_APB1RSTR_I2C1RST_Msk //!< I2C 1 reset 
.equ RCC_APB1RSTR_I2C2RST_Pos, (22) 
.equ RCC_APB1RSTR_I2C2RST_Msk, (0x1 << RCC_APB1RSTR_I2C2RST_Pos) //!< 0x00400000 
.equ RCC_APB1RSTR_I2C2RST, RCC_APB1RSTR_I2C2RST_Msk //!< I2C 2 reset 
.equ RCC_APB1RSTR_USBRST_Pos, (23) 
.equ RCC_APB1RSTR_USBRST_Msk, (0x1 << RCC_APB1RSTR_USBRST_Pos) //!< 0x00800000 
.equ RCC_APB1RSTR_USBRST, RCC_APB1RSTR_USBRST_Msk //!< USB reset 
.equ RCC_APB1RSTR_CANRST_Pos, (25) 
.equ RCC_APB1RSTR_CANRST_Msk, (0x1 << RCC_APB1RSTR_CANRST_Pos) //!< 0x02000000 
.equ RCC_APB1RSTR_CANRST, RCC_APB1RSTR_CANRST_Msk //!< CAN reset 
.equ RCC_APB1RSTR_PWRRST_Pos, (28) 
.equ RCC_APB1RSTR_PWRRST_Msk, (0x1 << RCC_APB1RSTR_PWRRST_Pos) //!< 0x10000000 
.equ RCC_APB1RSTR_PWRRST, RCC_APB1RSTR_PWRRST_Msk //!< PWR reset 
.equ RCC_APB1RSTR_DAC1RST_Pos, (29) 
.equ RCC_APB1RSTR_DAC1RST_Msk, (0x1 << RCC_APB1RSTR_DAC1RST_Pos) //!< 0x20000000 
.equ RCC_APB1RSTR_DAC1RST, RCC_APB1RSTR_DAC1RST_Msk //!< DAC 1 reset 
.equ RCC_APB1RSTR_I2C3RST_Pos, (30) 
.equ RCC_APB1RSTR_I2C3RST_Msk, (0x1 << RCC_APB1RSTR_I2C3RST_Pos) //!< 0x40000000 
.equ RCC_APB1RSTR_I2C3RST, RCC_APB1RSTR_I2C3RST_Msk //!< I2C 3 reset 
//*****************  Bit definition for RCC_AHBENR register  *****************
.equ RCC_AHBENR_DMA1EN_Pos, (0) 
.equ RCC_AHBENR_DMA1EN_Msk, (0x1 << RCC_AHBENR_DMA1EN_Pos) //!< 0x00000001 
.equ RCC_AHBENR_DMA1EN, RCC_AHBENR_DMA1EN_Msk //!< DMA1 clock enable 
.equ RCC_AHBENR_DMA2EN_Pos, (1) 
.equ RCC_AHBENR_DMA2EN_Msk, (0x1 << RCC_AHBENR_DMA2EN_Pos) //!< 0x00000002 
.equ RCC_AHBENR_DMA2EN, RCC_AHBENR_DMA2EN_Msk //!< DMA2 clock enable 
.equ RCC_AHBENR_SRAMEN_Pos, (2) 
.equ RCC_AHBENR_SRAMEN_Msk, (0x1 << RCC_AHBENR_SRAMEN_Pos) //!< 0x00000004 
.equ RCC_AHBENR_SRAMEN, RCC_AHBENR_SRAMEN_Msk //!< SRAM interface clock enable 
.equ RCC_AHBENR_FLITFEN_Pos, (4) 
.equ RCC_AHBENR_FLITFEN_Msk, (0x1 << RCC_AHBENR_FLITFEN_Pos) //!< 0x00000010 
.equ RCC_AHBENR_FLITFEN, RCC_AHBENR_FLITFEN_Msk //!< FLITF clock enable 
.equ RCC_AHBENR_FMCEN_Pos, (5) 
.equ RCC_AHBENR_FMCEN_Msk, (0x1 << RCC_AHBENR_FMCEN_Pos) //!< 0x00000020 
.equ RCC_AHBENR_FMCEN, RCC_AHBENR_FMCEN_Msk //!< FMC clock enable 
.equ RCC_AHBENR_CRCEN_Pos, (6) 
.equ RCC_AHBENR_CRCEN_Msk, (0x1 << RCC_AHBENR_CRCEN_Pos) //!< 0x00000040 
.equ RCC_AHBENR_CRCEN, RCC_AHBENR_CRCEN_Msk //!< CRC clock enable 
.equ RCC_AHBENR_GPIOHEN_Pos, (16) 
.equ RCC_AHBENR_GPIOHEN_Msk, (0x1 << RCC_AHBENR_GPIOHEN_Pos) //!< 0x00010000 
.equ RCC_AHBENR_GPIOHEN, RCC_AHBENR_GPIOHEN_Msk //!< GPIOH clock enable 
.equ RCC_AHBENR_GPIOAEN_Pos, (17) 
.equ RCC_AHBENR_GPIOAEN_Msk, (0x1 << RCC_AHBENR_GPIOAEN_Pos) //!< 0x00020000 
.equ RCC_AHBENR_GPIOAEN, RCC_AHBENR_GPIOAEN_Msk //!< GPIOA clock enable 
.equ RCC_AHBENR_GPIOBEN_Pos, (18) 
.equ RCC_AHBENR_GPIOBEN_Msk, (0x1 << RCC_AHBENR_GPIOBEN_Pos) //!< 0x00040000 
.equ RCC_AHBENR_GPIOBEN, RCC_AHBENR_GPIOBEN_Msk //!< GPIOB clock enable 
.equ RCC_AHBENR_GPIOCEN_Pos, (19) 
.equ RCC_AHBENR_GPIOCEN_Msk, (0x1 << RCC_AHBENR_GPIOCEN_Pos) //!< 0x00080000 
.equ RCC_AHBENR_GPIOCEN, RCC_AHBENR_GPIOCEN_Msk //!< GPIOC clock enable 
.equ RCC_AHBENR_GPIODEN_Pos, (20) 
.equ RCC_AHBENR_GPIODEN_Msk, (0x1 << RCC_AHBENR_GPIODEN_Pos) //!< 0x00100000 
.equ RCC_AHBENR_GPIODEN, RCC_AHBENR_GPIODEN_Msk //!< GPIOD clock enable 
.equ RCC_AHBENR_GPIOEEN_Pos, (21) 
.equ RCC_AHBENR_GPIOEEN_Msk, (0x1 << RCC_AHBENR_GPIOEEN_Pos) //!< 0x00200000 
.equ RCC_AHBENR_GPIOEEN, RCC_AHBENR_GPIOEEN_Msk //!< GPIOE clock enable 
.equ RCC_AHBENR_GPIOFEN_Pos, (22) 
.equ RCC_AHBENR_GPIOFEN_Msk, (0x1 << RCC_AHBENR_GPIOFEN_Pos) //!< 0x00400000 
.equ RCC_AHBENR_GPIOFEN, RCC_AHBENR_GPIOFEN_Msk //!< GPIOF clock enable 
.equ RCC_AHBENR_GPIOGEN_Pos, (23) 
.equ RCC_AHBENR_GPIOGEN_Msk, (0x1 << RCC_AHBENR_GPIOGEN_Pos) //!< 0x00800000 
.equ RCC_AHBENR_GPIOGEN, RCC_AHBENR_GPIOGEN_Msk //!< GPIOG clock enable 
.equ RCC_AHBENR_TSCEN_Pos, (24) 
.equ RCC_AHBENR_TSCEN_Msk, (0x1 << RCC_AHBENR_TSCEN_Pos) //!< 0x01000000 
.equ RCC_AHBENR_TSCEN, RCC_AHBENR_TSCEN_Msk //!< TS clock enable 
.equ RCC_AHBENR_ADC12EN_Pos, (28) 
.equ RCC_AHBENR_ADC12EN_Msk, (0x1 << RCC_AHBENR_ADC12EN_Pos) //!< 0x10000000 
.equ RCC_AHBENR_ADC12EN, RCC_AHBENR_ADC12EN_Msk //!< ADC1/ ADC2 clock enable 
.equ RCC_AHBENR_ADC34EN_Pos, (29) 
.equ RCC_AHBENR_ADC34EN_Msk, (0x1 << RCC_AHBENR_ADC34EN_Pos) //!< 0x20000000 
.equ RCC_AHBENR_ADC34EN, RCC_AHBENR_ADC34EN_Msk //!< ADC3/ ADC4 clock enable 
//****************  Bit definition for RCC_APB2ENR register  *****************
.equ RCC_APB2ENR_SYSCFGEN_Pos, (0) 
.equ RCC_APB2ENR_SYSCFGEN_Msk, (0x1 << RCC_APB2ENR_SYSCFGEN_Pos) //!< 0x00000001 
.equ RCC_APB2ENR_SYSCFGEN, RCC_APB2ENR_SYSCFGEN_Msk //!< SYSCFG clock enable 
.equ RCC_APB2ENR_TIM1EN_Pos, (11) 
.equ RCC_APB2ENR_TIM1EN_Msk, (0x1 << RCC_APB2ENR_TIM1EN_Pos) //!< 0x00000800 
.equ RCC_APB2ENR_TIM1EN, RCC_APB2ENR_TIM1EN_Msk //!< TIM1 clock enable 
.equ RCC_APB2ENR_SPI1EN_Pos, (12) 
.equ RCC_APB2ENR_SPI1EN_Msk, (0x1 << RCC_APB2ENR_SPI1EN_Pos) //!< 0x00001000 
.equ RCC_APB2ENR_SPI1EN, RCC_APB2ENR_SPI1EN_Msk //!< SPI1 clock enable 
.equ RCC_APB2ENR_TIM8EN_Pos, (13) 
.equ RCC_APB2ENR_TIM8EN_Msk, (0x1 << RCC_APB2ENR_TIM8EN_Pos) //!< 0x00002000 
.equ RCC_APB2ENR_TIM8EN, RCC_APB2ENR_TIM8EN_Msk //!< TIM8 clock enable 
.equ RCC_APB2ENR_USART1EN_Pos, (14) 
.equ RCC_APB2ENR_USART1EN_Msk, (0x1 << RCC_APB2ENR_USART1EN_Pos) //!< 0x00004000 
.equ RCC_APB2ENR_USART1EN, RCC_APB2ENR_USART1EN_Msk //!< USART1 clock enable 
.equ RCC_APB2ENR_SPI4EN_Pos, (15) 
.equ RCC_APB2ENR_SPI4EN_Msk, (0x1 << RCC_APB2ENR_SPI4EN_Pos) //!< 0x00008000 
.equ RCC_APB2ENR_SPI4EN, RCC_APB2ENR_SPI4EN_Msk //!< SPI4 clock enable 
.equ RCC_APB2ENR_TIM15EN_Pos, (16) 
.equ RCC_APB2ENR_TIM15EN_Msk, (0x1 << RCC_APB2ENR_TIM15EN_Pos) //!< 0x00010000 
.equ RCC_APB2ENR_TIM15EN, RCC_APB2ENR_TIM15EN_Msk //!< TIM15 clock enable 
.equ RCC_APB2ENR_TIM16EN_Pos, (17) 
.equ RCC_APB2ENR_TIM16EN_Msk, (0x1 << RCC_APB2ENR_TIM16EN_Pos) //!< 0x00020000 
.equ RCC_APB2ENR_TIM16EN, RCC_APB2ENR_TIM16EN_Msk //!< TIM16 clock enable 
.equ RCC_APB2ENR_TIM17EN_Pos, (18) 
.equ RCC_APB2ENR_TIM17EN_Msk, (0x1 << RCC_APB2ENR_TIM17EN_Pos) //!< 0x00040000 
.equ RCC_APB2ENR_TIM17EN, RCC_APB2ENR_TIM17EN_Msk //!< TIM17 clock enable 
.equ RCC_APB2ENR_TIM20EN_Pos, (20) 
.equ RCC_APB2ENR_TIM20EN_Msk, (0x1 << RCC_APB2ENR_TIM20EN_Pos) //!< 0x00100000 
.equ RCC_APB2ENR_TIM20EN, RCC_APB2ENR_TIM20EN_Msk //!< TIM20 clock enable 
//*****************  Bit definition for RCC_APB1ENR register  *****************
.equ RCC_APB1ENR_TIM2EN_Pos, (0) 
.equ RCC_APB1ENR_TIM2EN_Msk, (0x1 << RCC_APB1ENR_TIM2EN_Pos) //!< 0x00000001 
.equ RCC_APB1ENR_TIM2EN, RCC_APB1ENR_TIM2EN_Msk //!< Timer 2 clock enable 
.equ RCC_APB1ENR_TIM3EN_Pos, (1) 
.equ RCC_APB1ENR_TIM3EN_Msk, (0x1 << RCC_APB1ENR_TIM3EN_Pos) //!< 0x00000002 
.equ RCC_APB1ENR_TIM3EN, RCC_APB1ENR_TIM3EN_Msk //!< Timer 3 clock enable 
.equ RCC_APB1ENR_TIM4EN_Pos, (2) 
.equ RCC_APB1ENR_TIM4EN_Msk, (0x1 << RCC_APB1ENR_TIM4EN_Pos) //!< 0x00000004 
.equ RCC_APB1ENR_TIM4EN, RCC_APB1ENR_TIM4EN_Msk //!< Timer 4 clock enable 
.equ RCC_APB1ENR_TIM6EN_Pos, (4) 
.equ RCC_APB1ENR_TIM6EN_Msk, (0x1 << RCC_APB1ENR_TIM6EN_Pos) //!< 0x00000010 
.equ RCC_APB1ENR_TIM6EN, RCC_APB1ENR_TIM6EN_Msk //!< Timer 6 clock enable 
.equ RCC_APB1ENR_TIM7EN_Pos, (5) 
.equ RCC_APB1ENR_TIM7EN_Msk, (0x1 << RCC_APB1ENR_TIM7EN_Pos) //!< 0x00000020 
.equ RCC_APB1ENR_TIM7EN, RCC_APB1ENR_TIM7EN_Msk //!< Timer 7 clock enable 
.equ RCC_APB1ENR_WWDGEN_Pos, (11) 
.equ RCC_APB1ENR_WWDGEN_Msk, (0x1 << RCC_APB1ENR_WWDGEN_Pos) //!< 0x00000800 
.equ RCC_APB1ENR_WWDGEN, RCC_APB1ENR_WWDGEN_Msk //!< Window Watchdog clock enable 
.equ RCC_APB1ENR_SPI2EN_Pos, (14) 
.equ RCC_APB1ENR_SPI2EN_Msk, (0x1 << RCC_APB1ENR_SPI2EN_Pos) //!< 0x00004000 
.equ RCC_APB1ENR_SPI2EN, RCC_APB1ENR_SPI2EN_Msk //!< SPI2 clock enable 
.equ RCC_APB1ENR_SPI3EN_Pos, (15) 
.equ RCC_APB1ENR_SPI3EN_Msk, (0x1 << RCC_APB1ENR_SPI3EN_Pos) //!< 0x00008000 
.equ RCC_APB1ENR_SPI3EN, RCC_APB1ENR_SPI3EN_Msk //!< SPI3 clock enable 
.equ RCC_APB1ENR_USART2EN_Pos, (17) 
.equ RCC_APB1ENR_USART2EN_Msk, (0x1 << RCC_APB1ENR_USART2EN_Pos) //!< 0x00020000 
.equ RCC_APB1ENR_USART2EN, RCC_APB1ENR_USART2EN_Msk //!< USART 2 clock enable 
.equ RCC_APB1ENR_USART3EN_Pos, (18) 
.equ RCC_APB1ENR_USART3EN_Msk, (0x1 << RCC_APB1ENR_USART3EN_Pos) //!< 0x00040000 
.equ RCC_APB1ENR_USART3EN, RCC_APB1ENR_USART3EN_Msk //!< USART 3 clock enable 
.equ RCC_APB1ENR_UART4EN_Pos, (19) 
.equ RCC_APB1ENR_UART4EN_Msk, (0x1 << RCC_APB1ENR_UART4EN_Pos) //!< 0x00080000 
.equ RCC_APB1ENR_UART4EN, RCC_APB1ENR_UART4EN_Msk //!< UART 4 clock enable 
.equ RCC_APB1ENR_UART5EN_Pos, (20) 
.equ RCC_APB1ENR_UART5EN_Msk, (0x1 << RCC_APB1ENR_UART5EN_Pos) //!< 0x00100000 
.equ RCC_APB1ENR_UART5EN, RCC_APB1ENR_UART5EN_Msk //!< UART 5 clock enable 
.equ RCC_APB1ENR_I2C1EN_Pos, (21) 
.equ RCC_APB1ENR_I2C1EN_Msk, (0x1 << RCC_APB1ENR_I2C1EN_Pos) //!< 0x00200000 
.equ RCC_APB1ENR_I2C1EN, RCC_APB1ENR_I2C1EN_Msk //!< I2C 1 clock enable 
.equ RCC_APB1ENR_I2C2EN_Pos, (22) 
.equ RCC_APB1ENR_I2C2EN_Msk, (0x1 << RCC_APB1ENR_I2C2EN_Pos) //!< 0x00400000 
.equ RCC_APB1ENR_I2C2EN, RCC_APB1ENR_I2C2EN_Msk //!< I2C 2 clock enable 
.equ RCC_APB1ENR_USBEN_Pos, (23) 
.equ RCC_APB1ENR_USBEN_Msk, (0x1 << RCC_APB1ENR_USBEN_Pos) //!< 0x00800000 
.equ RCC_APB1ENR_USBEN, RCC_APB1ENR_USBEN_Msk //!< USB clock enable 
.equ RCC_APB1ENR_CANEN_Pos, (25) 
.equ RCC_APB1ENR_CANEN_Msk, (0x1 << RCC_APB1ENR_CANEN_Pos) //!< 0x02000000 
.equ RCC_APB1ENR_CANEN, RCC_APB1ENR_CANEN_Msk //!< CAN clock enable 
.equ RCC_APB1ENR_PWREN_Pos, (28) 
.equ RCC_APB1ENR_PWREN_Msk, (0x1 << RCC_APB1ENR_PWREN_Pos) //!< 0x10000000 
.equ RCC_APB1ENR_PWREN, RCC_APB1ENR_PWREN_Msk //!< PWR clock enable 
.equ RCC_APB1ENR_DAC1EN_Pos, (29) 
.equ RCC_APB1ENR_DAC1EN_Msk, (0x1 << RCC_APB1ENR_DAC1EN_Pos) //!< 0x20000000 
.equ RCC_APB1ENR_DAC1EN, RCC_APB1ENR_DAC1EN_Msk //!< DAC 1 clock enable 
.equ RCC_APB1ENR_I2C3EN_Pos, (30) 
.equ RCC_APB1ENR_I2C3EN_Msk, (0x1 << RCC_APB1ENR_I2C3EN_Pos) //!< 0x40000000 
.equ RCC_APB1ENR_I2C3EN, RCC_APB1ENR_I2C3EN_Msk //!< I2C 3 clock enable 
//*******************  Bit definition for RCC_BDCR register  *****************
.equ RCC_BDCR_LSE_Pos, (0) 
.equ RCC_BDCR_LSE_Msk, (0x7 << RCC_BDCR_LSE_Pos) //!< 0x00000007 
.equ RCC_BDCR_LSE, RCC_BDCR_LSE_Msk //!< External Low Speed oscillator [2:0] bits 
.equ RCC_BDCR_LSEON_Pos, (0) 
.equ RCC_BDCR_LSEON_Msk, (0x1 << RCC_BDCR_LSEON_Pos) //!< 0x00000001 
.equ RCC_BDCR_LSEON, RCC_BDCR_LSEON_Msk //!< External Low Speed oscillator enable 
.equ RCC_BDCR_LSERDY_Pos, (1) 
.equ RCC_BDCR_LSERDY_Msk, (0x1 << RCC_BDCR_LSERDY_Pos) //!< 0x00000002 
.equ RCC_BDCR_LSERDY, RCC_BDCR_LSERDY_Msk //!< External Low Speed oscillator Ready 
.equ RCC_BDCR_LSEBYP_Pos, (2) 
.equ RCC_BDCR_LSEBYP_Msk, (0x1 << RCC_BDCR_LSEBYP_Pos) //!< 0x00000004 
.equ RCC_BDCR_LSEBYP, RCC_BDCR_LSEBYP_Msk //!< External Low Speed oscillator Bypass 
.equ RCC_BDCR_LSEDRV_Pos, (3) 
.equ RCC_BDCR_LSEDRV_Msk, (0x3 << RCC_BDCR_LSEDRV_Pos) //!< 0x00000018 
.equ RCC_BDCR_LSEDRV, RCC_BDCR_LSEDRV_Msk //!< LSEDRV[1:0] bits (LSE Osc. drive capability) 
.equ RCC_BDCR_LSEDRV_0, (0x1 << RCC_BDCR_LSEDRV_Pos) //!< 0x00000008 
.equ RCC_BDCR_LSEDRV_1, (0x2 << RCC_BDCR_LSEDRV_Pos) //!< 0x00000010 
.equ RCC_BDCR_RTCSEL_Pos, (8) 
.equ RCC_BDCR_RTCSEL_Msk, (0x3 << RCC_BDCR_RTCSEL_Pos) //!< 0x00000300 
.equ RCC_BDCR_RTCSEL, RCC_BDCR_RTCSEL_Msk //!< RTCSEL[1:0] bits (RTC clock source selection) 
.equ RCC_BDCR_RTCSEL_0, (0x1 << RCC_BDCR_RTCSEL_Pos) //!< 0x00000100 
.equ RCC_BDCR_RTCSEL_1, (0x2 << RCC_BDCR_RTCSEL_Pos) //!< 0x00000200 
//!< RTC configuration
.equ RCC_BDCR_RTCSEL_NOCLOCK, (0x00000000) //!< No clock 
.equ RCC_BDCR_RTCSEL_LSE, (0x00000100) //!< LSE oscillator clock used as RTC clock 
.equ RCC_BDCR_RTCSEL_LSI, (0x00000200) //!< LSI oscillator clock used as RTC clock 
.equ RCC_BDCR_RTCSEL_HSE, (0x00000300) //!< HSE oscillator clock divided by 32 used as RTC clock 
.equ RCC_BDCR_RTCEN_Pos, (15) 
.equ RCC_BDCR_RTCEN_Msk, (0x1 << RCC_BDCR_RTCEN_Pos) //!< 0x00008000 
.equ RCC_BDCR_RTCEN, RCC_BDCR_RTCEN_Msk //!< RTC clock enable 
.equ RCC_BDCR_BDRST_Pos, (16) 
.equ RCC_BDCR_BDRST_Msk, (0x1 << RCC_BDCR_BDRST_Pos) //!< 0x00010000 
.equ RCC_BDCR_BDRST, RCC_BDCR_BDRST_Msk //!< Backup domain software reset 
//*******************  Bit definition for RCC_CSR register  ******************
.equ RCC_CSR_LSION_Pos, (0) 
.equ RCC_CSR_LSION_Msk, (0x1 << RCC_CSR_LSION_Pos) //!< 0x00000001 
.equ RCC_CSR_LSION, RCC_CSR_LSION_Msk //!< Internal Low Speed oscillator enable 
.equ RCC_CSR_LSIRDY_Pos, (1) 
.equ RCC_CSR_LSIRDY_Msk, (0x1 << RCC_CSR_LSIRDY_Pos) //!< 0x00000002 
.equ RCC_CSR_LSIRDY, RCC_CSR_LSIRDY_Msk //!< Internal Low Speed oscillator Ready 
.equ RCC_CSR_V18PWRRSTF_Pos, (23) 
.equ RCC_CSR_V18PWRRSTF_Msk, (0x1 << RCC_CSR_V18PWRRSTF_Pos) //!< 0x00800000 
.equ RCC_CSR_V18PWRRSTF, RCC_CSR_V18PWRRSTF_Msk //!< V1.8 power domain reset flag 
.equ RCC_CSR_RMVF_Pos, (24) 
.equ RCC_CSR_RMVF_Msk, (0x1 << RCC_CSR_RMVF_Pos) //!< 0x01000000 
.equ RCC_CSR_RMVF, RCC_CSR_RMVF_Msk //!< Remove reset flag 
.equ RCC_CSR_OBLRSTF_Pos, (25) 
.equ RCC_CSR_OBLRSTF_Msk, (0x1 << RCC_CSR_OBLRSTF_Pos) //!< 0x02000000 
.equ RCC_CSR_OBLRSTF, RCC_CSR_OBLRSTF_Msk //!< OBL reset flag 
.equ RCC_CSR_PINRSTF_Pos, (26) 
.equ RCC_CSR_PINRSTF_Msk, (0x1 << RCC_CSR_PINRSTF_Pos) //!< 0x04000000 
.equ RCC_CSR_PINRSTF, RCC_CSR_PINRSTF_Msk //!< PIN reset flag 
.equ RCC_CSR_PORRSTF_Pos, (27) 
.equ RCC_CSR_PORRSTF_Msk, (0x1 << RCC_CSR_PORRSTF_Pos) //!< 0x08000000 
.equ RCC_CSR_PORRSTF, RCC_CSR_PORRSTF_Msk //!< POR/PDR reset flag 
.equ RCC_CSR_SFTRSTF_Pos, (28) 
.equ RCC_CSR_SFTRSTF_Msk, (0x1 << RCC_CSR_SFTRSTF_Pos) //!< 0x10000000 
.equ RCC_CSR_SFTRSTF, RCC_CSR_SFTRSTF_Msk //!< Software Reset flag 
.equ RCC_CSR_IWDGRSTF_Pos, (29) 
.equ RCC_CSR_IWDGRSTF_Msk, (0x1 << RCC_CSR_IWDGRSTF_Pos) //!< 0x20000000 
.equ RCC_CSR_IWDGRSTF, RCC_CSR_IWDGRSTF_Msk //!< Independent Watchdog reset flag 
.equ RCC_CSR_WWDGRSTF_Pos, (30) 
.equ RCC_CSR_WWDGRSTF_Msk, (0x1 << RCC_CSR_WWDGRSTF_Pos) //!< 0x40000000 
.equ RCC_CSR_WWDGRSTF, RCC_CSR_WWDGRSTF_Msk //!< Window watchdog reset flag 
.equ RCC_CSR_LPWRRSTF_Pos, (31) 
.equ RCC_CSR_LPWRRSTF_Msk, (0x1 << RCC_CSR_LPWRRSTF_Pos) //!< 0x80000000 
.equ RCC_CSR_LPWRRSTF, RCC_CSR_LPWRRSTF_Msk //!< Low-Power reset flag 
// Legacy defines
.equ RCC_CSR_VREGRSTF, RCC_CSR_V18PWRRSTF 
//******************  Bit definition for RCC_AHBRSTR register  ***************
.equ RCC_AHBRSTR_FMCRST_Pos, (5) 
.equ RCC_AHBRSTR_FMCRST_Msk, (0x1 << RCC_AHBRSTR_FMCRST_Pos) //!< 0x00000020 
.equ RCC_AHBRSTR_FMCRST, RCC_AHBRSTR_FMCRST_Msk //!< FMC reset 
.equ RCC_AHBRSTR_GPIOHRST_Pos, (16) 
.equ RCC_AHBRSTR_GPIOHRST_Msk, (0x1 << RCC_AHBRSTR_GPIOHRST_Pos) //!< 0x00010000 
.equ RCC_AHBRSTR_GPIOHRST, RCC_AHBRSTR_GPIOHRST_Msk //!< GPIOH reset 
.equ RCC_AHBRSTR_GPIOARST_Pos, (17) 
.equ RCC_AHBRSTR_GPIOARST_Msk, (0x1 << RCC_AHBRSTR_GPIOARST_Pos) //!< 0x00020000 
.equ RCC_AHBRSTR_GPIOARST, RCC_AHBRSTR_GPIOARST_Msk //!< GPIOA reset 
.equ RCC_AHBRSTR_GPIOBRST_Pos, (18) 
.equ RCC_AHBRSTR_GPIOBRST_Msk, (0x1 << RCC_AHBRSTR_GPIOBRST_Pos) //!< 0x00040000 
.equ RCC_AHBRSTR_GPIOBRST, RCC_AHBRSTR_GPIOBRST_Msk //!< GPIOB reset 
.equ RCC_AHBRSTR_GPIOCRST_Pos, (19) 
.equ RCC_AHBRSTR_GPIOCRST_Msk, (0x1 << RCC_AHBRSTR_GPIOCRST_Pos) //!< 0x00080000 
.equ RCC_AHBRSTR_GPIOCRST, RCC_AHBRSTR_GPIOCRST_Msk //!< GPIOC reset 
.equ RCC_AHBRSTR_GPIODRST_Pos, (20) 
.equ RCC_AHBRSTR_GPIODRST_Msk, (0x1 << RCC_AHBRSTR_GPIODRST_Pos) //!< 0x00100000 
.equ RCC_AHBRSTR_GPIODRST, RCC_AHBRSTR_GPIODRST_Msk //!< GPIOD reset 
.equ RCC_AHBRSTR_GPIOERST_Pos, (21) 
.equ RCC_AHBRSTR_GPIOERST_Msk, (0x1 << RCC_AHBRSTR_GPIOERST_Pos) //!< 0x00200000 
.equ RCC_AHBRSTR_GPIOERST, RCC_AHBRSTR_GPIOERST_Msk //!< GPIOE reset 
.equ RCC_AHBRSTR_GPIOFRST_Pos, (22) 
.equ RCC_AHBRSTR_GPIOFRST_Msk, (0x1 << RCC_AHBRSTR_GPIOFRST_Pos) //!< 0x00400000 
.equ RCC_AHBRSTR_GPIOFRST, RCC_AHBRSTR_GPIOFRST_Msk //!< GPIOF reset 
.equ RCC_AHBRSTR_GPIOGRST_Pos, (23) 
.equ RCC_AHBRSTR_GPIOGRST_Msk, (0x1 << RCC_AHBRSTR_GPIOGRST_Pos) //!< 0x00800000 
.equ RCC_AHBRSTR_GPIOGRST, RCC_AHBRSTR_GPIOGRST_Msk //!< GPIOG reset 
.equ RCC_AHBRSTR_TSCRST_Pos, (24) 
.equ RCC_AHBRSTR_TSCRST_Msk, (0x1 << RCC_AHBRSTR_TSCRST_Pos) //!< 0x01000000 
.equ RCC_AHBRSTR_TSCRST, RCC_AHBRSTR_TSCRST_Msk //!< TSC reset 
.equ RCC_AHBRSTR_ADC12RST_Pos, (28) 
.equ RCC_AHBRSTR_ADC12RST_Msk, (0x1 << RCC_AHBRSTR_ADC12RST_Pos) //!< 0x10000000 
.equ RCC_AHBRSTR_ADC12RST, RCC_AHBRSTR_ADC12RST_Msk //!< ADC1 & ADC2 reset 
.equ RCC_AHBRSTR_ADC34RST_Pos, (29) 
.equ RCC_AHBRSTR_ADC34RST_Msk, (0x1 << RCC_AHBRSTR_ADC34RST_Pos) //!< 0x20000000 
.equ RCC_AHBRSTR_ADC34RST, RCC_AHBRSTR_ADC34RST_Msk //!< ADC3 & ADC4 reset 
//******************  Bit definition for RCC_CFGR2 register  *****************
//!< PREDIV configuration
.equ RCC_CFGR2_PREDIV_Pos, (0) 
.equ RCC_CFGR2_PREDIV_Msk, (0xF << RCC_CFGR2_PREDIV_Pos) //!< 0x0000000F 
.equ RCC_CFGR2_PREDIV, RCC_CFGR2_PREDIV_Msk //!< PREDIV[3:0] bits 
.equ RCC_CFGR2_PREDIV_0, (0x1 << RCC_CFGR2_PREDIV_Pos) //!< 0x00000001 
.equ RCC_CFGR2_PREDIV_1, (0x2 << RCC_CFGR2_PREDIV_Pos) //!< 0x00000002 
.equ RCC_CFGR2_PREDIV_2, (0x4 << RCC_CFGR2_PREDIV_Pos) //!< 0x00000004 
.equ RCC_CFGR2_PREDIV_3, (0x8 << RCC_CFGR2_PREDIV_Pos) //!< 0x00000008 
.equ RCC_CFGR2_PREDIV_DIV1, (0x00000000) //!< PREDIV input clock not divided 
.equ RCC_CFGR2_PREDIV_DIV2, (0x00000001) //!< PREDIV input clock divided by 2 
.equ RCC_CFGR2_PREDIV_DIV3, (0x00000002) //!< PREDIV input clock divided by 3 
.equ RCC_CFGR2_PREDIV_DIV4, (0x00000003) //!< PREDIV input clock divided by 4 
.equ RCC_CFGR2_PREDIV_DIV5, (0x00000004) //!< PREDIV input clock divided by 5 
.equ RCC_CFGR2_PREDIV_DIV6, (0x00000005) //!< PREDIV input clock divided by 6 
.equ RCC_CFGR2_PREDIV_DIV7, (0x00000006) //!< PREDIV input clock divided by 7 
.equ RCC_CFGR2_PREDIV_DIV8, (0x00000007) //!< PREDIV input clock divided by 8 
.equ RCC_CFGR2_PREDIV_DIV9, (0x00000008) //!< PREDIV input clock divided by 9 
.equ RCC_CFGR2_PREDIV_DIV10, (0x00000009) //!< PREDIV input clock divided by 10 
.equ RCC_CFGR2_PREDIV_DIV11, (0x0000000A) //!< PREDIV input clock divided by 11 
.equ RCC_CFGR2_PREDIV_DIV12, (0x0000000B) //!< PREDIV input clock divided by 12 
.equ RCC_CFGR2_PREDIV_DIV13, (0x0000000C) //!< PREDIV input clock divided by 13 
.equ RCC_CFGR2_PREDIV_DIV14, (0x0000000D) //!< PREDIV input clock divided by 14 
.equ RCC_CFGR2_PREDIV_DIV15, (0x0000000E) //!< PREDIV input clock divided by 15 
.equ RCC_CFGR2_PREDIV_DIV16, (0x0000000F) //!< PREDIV input clock divided by 16 
//!< ADCPRE12 configuration
.equ RCC_CFGR2_ADCPRE12_Pos, (4) 
.equ RCC_CFGR2_ADCPRE12_Msk, (0x1F << RCC_CFGR2_ADCPRE12_Pos) //!< 0x000001F0 
.equ RCC_CFGR2_ADCPRE12, RCC_CFGR2_ADCPRE12_Msk //!< ADCPRE12[8:4] bits 
.equ RCC_CFGR2_ADCPRE12_0, (0x01 << RCC_CFGR2_ADCPRE12_Pos) //!< 0x00000010 
.equ RCC_CFGR2_ADCPRE12_1, (0x02 << RCC_CFGR2_ADCPRE12_Pos) //!< 0x00000020 
.equ RCC_CFGR2_ADCPRE12_2, (0x04 << RCC_CFGR2_ADCPRE12_Pos) //!< 0x00000040 
.equ RCC_CFGR2_ADCPRE12_3, (0x08 << RCC_CFGR2_ADCPRE12_Pos) //!< 0x00000080 
.equ RCC_CFGR2_ADCPRE12_4, (0x10 << RCC_CFGR2_ADCPRE12_Pos) //!< 0x00000100 
.equ RCC_CFGR2_ADCPRE12_NO, (0x00000000) //!< ADC12 clock disabled, ADC12 can use AHB clock 
.equ RCC_CFGR2_ADCPRE12_DIV1, (0x00000100) //!< ADC12 PLL clock divided by 1 
.equ RCC_CFGR2_ADCPRE12_DIV2, (0x00000110) //!< ADC12 PLL clock divided by 2 
.equ RCC_CFGR2_ADCPRE12_DIV4, (0x00000120) //!< ADC12 PLL clock divided by 4 
.equ RCC_CFGR2_ADCPRE12_DIV6, (0x00000130) //!< ADC12 PLL clock divided by 6 
.equ RCC_CFGR2_ADCPRE12_DIV8, (0x00000140) //!< ADC12 PLL clock divided by 8 
.equ RCC_CFGR2_ADCPRE12_DIV10, (0x00000150) //!< ADC12 PLL clock divided by 10 
.equ RCC_CFGR2_ADCPRE12_DIV12, (0x00000160) //!< ADC12 PLL clock divided by 12 
.equ RCC_CFGR2_ADCPRE12_DIV16, (0x00000170) //!< ADC12 PLL clock divided by 16 
.equ RCC_CFGR2_ADCPRE12_DIV32, (0x00000180) //!< ADC12 PLL clock divided by 32 
.equ RCC_CFGR2_ADCPRE12_DIV64, (0x00000190) //!< ADC12 PLL clock divided by 64 
.equ RCC_CFGR2_ADCPRE12_DIV128, (0x000001A0) //!< ADC12 PLL clock divided by 128 
.equ RCC_CFGR2_ADCPRE12_DIV256, (0x000001B0) //!< ADC12 PLL clock divided by 256 
//!< ADCPRE34 configuration
.equ RCC_CFGR2_ADCPRE34_Pos, (9) 
.equ RCC_CFGR2_ADCPRE34_Msk, (0x1F << RCC_CFGR2_ADCPRE34_Pos) //!< 0x00003E00 
.equ RCC_CFGR2_ADCPRE34, RCC_CFGR2_ADCPRE34_Msk //!< ADCPRE34[13:5] bits 
.equ RCC_CFGR2_ADCPRE34_0, (0x01 << RCC_CFGR2_ADCPRE34_Pos) //!< 0x00000200 
.equ RCC_CFGR2_ADCPRE34_1, (0x02 << RCC_CFGR2_ADCPRE34_Pos) //!< 0x00000400 
.equ RCC_CFGR2_ADCPRE34_2, (0x04 << RCC_CFGR2_ADCPRE34_Pos) //!< 0x00000800 
.equ RCC_CFGR2_ADCPRE34_3, (0x08 << RCC_CFGR2_ADCPRE34_Pos) //!< 0x00001000 
.equ RCC_CFGR2_ADCPRE34_4, (0x10 << RCC_CFGR2_ADCPRE34_Pos) //!< 0x00002000 
.equ RCC_CFGR2_ADCPRE34_NO, (0x00000000) //!< ADC34 clock disabled, ADC34 can use AHB clock 
.equ RCC_CFGR2_ADCPRE34_DIV1, (0x00002000) //!< ADC34 PLL clock divided by 1 
.equ RCC_CFGR2_ADCPRE34_DIV2, (0x00002200) //!< ADC34 PLL clock divided by 2 
.equ RCC_CFGR2_ADCPRE34_DIV4, (0x00002400) //!< ADC34 PLL clock divided by 4 
.equ RCC_CFGR2_ADCPRE34_DIV6, (0x00002600) //!< ADC34 PLL clock divided by 6 
.equ RCC_CFGR2_ADCPRE34_DIV8, (0x00002800) //!< ADC34 PLL clock divided by 8 
.equ RCC_CFGR2_ADCPRE34_DIV10, (0x00002A00) //!< ADC34 PLL clock divided by 10 
.equ RCC_CFGR2_ADCPRE34_DIV12, (0x00002C00) //!< ADC34 PLL clock divided by 12 
.equ RCC_CFGR2_ADCPRE34_DIV16, (0x00002E00) //!< ADC34 PLL clock divided by 16 
.equ RCC_CFGR2_ADCPRE34_DIV32, (0x00003000) //!< ADC34 PLL clock divided by 32 
.equ RCC_CFGR2_ADCPRE34_DIV64, (0x00003200) //!< ADC34 PLL clock divided by 64 
.equ RCC_CFGR2_ADCPRE34_DIV128, (0x00003400) //!< ADC34 PLL clock divided by 128 
.equ RCC_CFGR2_ADCPRE34_DIV256, (0x00003600) //!< ADC34 PLL clock divided by 256 
//******************  Bit definition for RCC_CFGR3 register  *****************
.equ RCC_CFGR3_USART1SW_Pos, (0) 
.equ RCC_CFGR3_USART1SW_Msk, (0x3 << RCC_CFGR3_USART1SW_Pos) //!< 0x00000003 
.equ RCC_CFGR3_USART1SW, RCC_CFGR3_USART1SW_Msk //!< USART1SW[1:0] bits 
.equ RCC_CFGR3_USART1SW_0, (0x1 << RCC_CFGR3_USART1SW_Pos) //!< 0x00000001 
.equ RCC_CFGR3_USART1SW_1, (0x2 << RCC_CFGR3_USART1SW_Pos) //!< 0x00000002 
.equ RCC_CFGR3_USART1SW_PCLK2, (0x00000000) //!< PCLK2 clock used as USART1 clock source 
.equ RCC_CFGR3_USART1SW_SYSCLK, (0x00000001) //!< System clock selected as USART1 clock source 
.equ RCC_CFGR3_USART1SW_LSE, (0x00000002) //!< LSE oscillator clock used as USART1 clock source 
.equ RCC_CFGR3_USART1SW_HSI, (0x00000003) //!< HSI oscillator clock used as USART1 clock source 
// Legacy defines
.equ RCC_CFGR3_USART1SW_PCLK, RCC_CFGR3_USART1SW_PCLK2 
.equ RCC_CFGR3_I2CSW_Pos, (4) 
.equ RCC_CFGR3_I2CSW_Msk, (0x7 << RCC_CFGR3_I2CSW_Pos) //!< 0x00000070 
.equ RCC_CFGR3_I2CSW, RCC_CFGR3_I2CSW_Msk //!< I2CSW bits 
.equ RCC_CFGR3_I2C1SW_Pos, (4) 
.equ RCC_CFGR3_I2C1SW_Msk, (0x1 << RCC_CFGR3_I2C1SW_Pos) //!< 0x00000010 
.equ RCC_CFGR3_I2C1SW, RCC_CFGR3_I2C1SW_Msk //!< I2C1SW bits 
.equ RCC_CFGR3_I2C2SW_Pos, (5) 
.equ RCC_CFGR3_I2C2SW_Msk, (0x1 << RCC_CFGR3_I2C2SW_Pos) //!< 0x00000020 
.equ RCC_CFGR3_I2C2SW, RCC_CFGR3_I2C2SW_Msk //!< I2C2SW bits 
.equ RCC_CFGR3_I2C3SW_Pos, (6) 
.equ RCC_CFGR3_I2C3SW_Msk, (0x1 << RCC_CFGR3_I2C3SW_Pos) //!< 0x00000040 
.equ RCC_CFGR3_I2C3SW, RCC_CFGR3_I2C3SW_Msk //!< I2C3SW bits 
.equ RCC_CFGR3_I2C1SW_HSI, (0x00000000) //!< HSI oscillator clock used as I2C1 clock source 
.equ RCC_CFGR3_I2C1SW_SYSCLK_Pos, (4) 
.equ RCC_CFGR3_I2C1SW_SYSCLK_Msk, (0x1 << RCC_CFGR3_I2C1SW_SYSCLK_Pos) //!< 0x00000010 
.equ RCC_CFGR3_I2C1SW_SYSCLK, RCC_CFGR3_I2C1SW_SYSCLK_Msk //!< System clock selected as I2C1 clock source 
.equ RCC_CFGR3_I2C2SW_HSI, (0x00000000) //!< HSI oscillator clock used as I2C2 clock source 
.equ RCC_CFGR3_I2C2SW_SYSCLK_Pos, (5) 
.equ RCC_CFGR3_I2C2SW_SYSCLK_Msk, (0x1 << RCC_CFGR3_I2C2SW_SYSCLK_Pos) //!< 0x00000020 
.equ RCC_CFGR3_I2C2SW_SYSCLK, RCC_CFGR3_I2C2SW_SYSCLK_Msk //!< System clock selected as I2C2 clock source 
.equ RCC_CFGR3_I2C3SW_HSI, (0x00000000) //!< HSI oscillator clock used as I2C3 clock source 
.equ RCC_CFGR3_I2C3SW_SYSCLK_Pos, (6) 
.equ RCC_CFGR3_I2C3SW_SYSCLK_Msk, (0x1 << RCC_CFGR3_I2C3SW_SYSCLK_Pos) //!< 0x00000040 
.equ RCC_CFGR3_I2C3SW_SYSCLK, RCC_CFGR3_I2C3SW_SYSCLK_Msk //!< System clock selected as I2C3 clock source 
.equ RCC_CFGR3_TIMSW_Pos, (8) 
.equ RCC_CFGR3_TIMSW_Msk, (0xAF << RCC_CFGR3_TIMSW_Pos) //!< 0x0000AF00 
.equ RCC_CFGR3_TIMSW, RCC_CFGR3_TIMSW_Msk //!< TIMSW bits 
.equ RCC_CFGR3_TIM1SW_Pos, (8) 
.equ RCC_CFGR3_TIM1SW_Msk, (0x1 << RCC_CFGR3_TIM1SW_Pos) //!< 0x00000100 
.equ RCC_CFGR3_TIM1SW, RCC_CFGR3_TIM1SW_Msk //!< TIM1SW bits 
.equ RCC_CFGR3_TIM8SW_Pos, (9) 
.equ RCC_CFGR3_TIM8SW_Msk, (0x1 << RCC_CFGR3_TIM8SW_Pos) //!< 0x00000200 
.equ RCC_CFGR3_TIM8SW, RCC_CFGR3_TIM8SW_Msk //!< TIM8SW bits 
.equ RCC_CFGR3_TIM15SW_Pos, (10) 
.equ RCC_CFGR3_TIM15SW_Msk, (0x1 << RCC_CFGR3_TIM15SW_Pos) //!< 0x00000400 
.equ RCC_CFGR3_TIM15SW, RCC_CFGR3_TIM15SW_Msk //!< TIM15SW bits 
.equ RCC_CFGR3_TIM16SW_Pos, (11) 
.equ RCC_CFGR3_TIM16SW_Msk, (0x1 << RCC_CFGR3_TIM16SW_Pos) //!< 0x00000800 
.equ RCC_CFGR3_TIM16SW, RCC_CFGR3_TIM16SW_Msk //!< TIM16SW bits 
.equ RCC_CFGR3_TIM17SW_Pos, (13) 
.equ RCC_CFGR3_TIM17SW_Msk, (0x1 << RCC_CFGR3_TIM17SW_Pos) //!< 0x00002000 
.equ RCC_CFGR3_TIM17SW, RCC_CFGR3_TIM17SW_Msk //!< TIM17SW bits 
.equ RCC_CFGR3_TIM20SW_Pos, (15) 
.equ RCC_CFGR3_TIM20SW_Msk, (0x1 << RCC_CFGR3_TIM20SW_Pos) //!< 0x00008000 
.equ RCC_CFGR3_TIM20SW, RCC_CFGR3_TIM20SW_Msk //!< TIM20SW bits 
.equ RCC_CFGR3_TIM2SW_Pos, (24) 
.equ RCC_CFGR3_TIM2SW_Msk, (0x1 << RCC_CFGR3_TIM2SW_Pos) //!< 0x01000000 
.equ RCC_CFGR3_TIM2SW, RCC_CFGR3_TIM2SW_Msk //!< TIM2SW bits 
.equ RCC_CFGR3_TIM34SW_Pos, (25) 
.equ RCC_CFGR3_TIM34SW_Msk, (0x1 << RCC_CFGR3_TIM34SW_Pos) //!< 0x02000000 
.equ RCC_CFGR3_TIM34SW, RCC_CFGR3_TIM34SW_Msk //!< TIM34SW bits 
.equ RCC_CFGR3_TIM1SW_PCLK2, (0x00000000) //!< PCLK2 used as TIM1 clock source 
.equ RCC_CFGR3_TIM1SW_PLL_Pos, (8) 
.equ RCC_CFGR3_TIM1SW_PLL_Msk, (0x1 << RCC_CFGR3_TIM1SW_PLL_Pos) //!< 0x00000100 
.equ RCC_CFGR3_TIM1SW_PLL, RCC_CFGR3_TIM1SW_PLL_Msk //!< PLL clock used as TIM1 clock source 
.equ RCC_CFGR3_TIM8SW_PCLK2, (0x00000000) //!< PCLK2 used as TIM8 clock source 
.equ RCC_CFGR3_TIM8SW_PLL_Pos, (9) 
.equ RCC_CFGR3_TIM8SW_PLL_Msk, (0x1 << RCC_CFGR3_TIM8SW_PLL_Pos) //!< 0x00000200 
.equ RCC_CFGR3_TIM8SW_PLL, RCC_CFGR3_TIM8SW_PLL_Msk //!< PLL clock used as TIM8 clock source 
.equ RCC_CFGR3_TIM15SW_PCLK2, (0x00000000) //!< PCLK2 used as TIM15 clock source 
.equ RCC_CFGR3_TIM15SW_PLL_Pos, (10) 
.equ RCC_CFGR3_TIM15SW_PLL_Msk, (0x1 << RCC_CFGR3_TIM15SW_PLL_Pos) //!< 0x00000400 
.equ RCC_CFGR3_TIM15SW_PLL, RCC_CFGR3_TIM15SW_PLL_Msk //!< PLL clock used as TIM15 clock source 
.equ RCC_CFGR3_TIM16SW_PCLK2, (0x00000000) //!< PCLK2 used as TIM16 clock source 
.equ RCC_CFGR3_TIM16SW_PLL_Pos, (11) 
.equ RCC_CFGR3_TIM16SW_PLL_Msk, (0x1 << RCC_CFGR3_TIM16SW_PLL_Pos) //!< 0x00000800 
.equ RCC_CFGR3_TIM16SW_PLL, RCC_CFGR3_TIM16SW_PLL_Msk //!< PLL clock used as TIM16 clock source 
.equ RCC_CFGR3_TIM17SW_PCLK2, (0x00000000) //!< PCLK2 used as TIM17 clock source 
.equ RCC_CFGR3_TIM17SW_PLL_Pos, (13) 
.equ RCC_CFGR3_TIM17SW_PLL_Msk, (0x1 << RCC_CFGR3_TIM17SW_PLL_Pos) //!< 0x00002000 
.equ RCC_CFGR3_TIM17SW_PLL, RCC_CFGR3_TIM17SW_PLL_Msk //!< PLL clock used as TIM17 clock source 
.equ RCC_CFGR3_TIM20SW_PCLK2, (0x00000000) //!< PCLK2 used as TIM20 clock source 
.equ RCC_CFGR3_TIM20SW_PLL_Pos, (15) 
.equ RCC_CFGR3_TIM20SW_PLL_Msk, (0x1 << RCC_CFGR3_TIM20SW_PLL_Pos) //!< 0x00008000 
.equ RCC_CFGR3_TIM20SW_PLL, RCC_CFGR3_TIM20SW_PLL_Msk //!< PLL clock used as TIM20 clock source 
.equ RCC_CFGR3_USART2SW_Pos, (16) 
.equ RCC_CFGR3_USART2SW_Msk, (0x3 << RCC_CFGR3_USART2SW_Pos) //!< 0x00030000 
.equ RCC_CFGR3_USART2SW, RCC_CFGR3_USART2SW_Msk //!< USART2SW[1:0] bits 
.equ RCC_CFGR3_USART2SW_0, (0x1 << RCC_CFGR3_USART2SW_Pos) //!< 0x00010000 
.equ RCC_CFGR3_USART2SW_1, (0x2 << RCC_CFGR3_USART2SW_Pos) //!< 0x00020000 
.equ RCC_CFGR3_USART2SW_PCLK, (0x00000000) //!< PCLK1 clock used as USART2 clock source 
.equ RCC_CFGR3_USART2SW_SYSCLK, (0x00010000) //!< System clock selected as USART2 clock source 
.equ RCC_CFGR3_USART2SW_LSE, (0x00020000) //!< LSE oscillator clock used as USART2 clock source 
.equ RCC_CFGR3_USART2SW_HSI, (0x00030000) //!< HSI oscillator clock used as USART2 clock source 
.equ RCC_CFGR3_USART3SW_Pos, (18) 
.equ RCC_CFGR3_USART3SW_Msk, (0x3 << RCC_CFGR3_USART3SW_Pos) //!< 0x000C0000 
.equ RCC_CFGR3_USART3SW, RCC_CFGR3_USART3SW_Msk //!< USART3SW[1:0] bits 
.equ RCC_CFGR3_USART3SW_0, (0x1 << RCC_CFGR3_USART3SW_Pos) //!< 0x00040000 
.equ RCC_CFGR3_USART3SW_1, (0x2 << RCC_CFGR3_USART3SW_Pos) //!< 0x00080000 
.equ RCC_CFGR3_USART3SW_PCLK, (0x00000000) //!< PCLK1 clock used as USART3 clock source 
.equ RCC_CFGR3_USART3SW_SYSCLK, (0x00040000) //!< System clock selected as USART3 clock source 
.equ RCC_CFGR3_USART3SW_LSE, (0x00080000) //!< LSE oscillator clock used as USART3 clock source 
.equ RCC_CFGR3_USART3SW_HSI, (0x000C0000) //!< HSI oscillator clock used as USART3 clock source 
.equ RCC_CFGR3_UART4SW_Pos, (20) 
.equ RCC_CFGR3_UART4SW_Msk, (0x3 << RCC_CFGR3_UART4SW_Pos) //!< 0x00300000 
.equ RCC_CFGR3_UART4SW, RCC_CFGR3_UART4SW_Msk //!< UART4SW[1:0] bits 
.equ RCC_CFGR3_UART4SW_0, (0x1 << RCC_CFGR3_UART4SW_Pos) //!< 0x00100000 
.equ RCC_CFGR3_UART4SW_1, (0x2 << RCC_CFGR3_UART4SW_Pos) //!< 0x00200000 
.equ RCC_CFGR3_UART4SW_PCLK, (0x00000000) //!< PCLK1 clock used as UART4 clock source 
.equ RCC_CFGR3_UART4SW_SYSCLK, (0x00100000) //!< System clock selected as UART4 clock source 
.equ RCC_CFGR3_UART4SW_LSE, (0x00200000) //!< LSE oscillator clock used as UART4 clock source 
.equ RCC_CFGR3_UART4SW_HSI, (0x00300000) //!< HSI oscillator clock used as UART4 clock source 
.equ RCC_CFGR3_UART5SW_Pos, (22) 
.equ RCC_CFGR3_UART5SW_Msk, (0x3 << RCC_CFGR3_UART5SW_Pos) //!< 0x00C00000 
.equ RCC_CFGR3_UART5SW, RCC_CFGR3_UART5SW_Msk //!< UART5SW[1:0] bits 
.equ RCC_CFGR3_UART5SW_0, (0x1 << RCC_CFGR3_UART5SW_Pos) //!< 0x00400000 
.equ RCC_CFGR3_UART5SW_1, (0x2 << RCC_CFGR3_UART5SW_Pos) //!< 0x00800000 
.equ RCC_CFGR3_UART5SW_PCLK, (0x00000000) //!< PCLK1 clock used as UART5 clock source 
.equ RCC_CFGR3_UART5SW_SYSCLK, (0x00400000) //!< System clock selected as UART5 clock source 
.equ RCC_CFGR3_UART5SW_LSE, (0x00800000) //!< LSE oscillator clock used as UART5 clock source 
.equ RCC_CFGR3_UART5SW_HSI, (0x00C00000) //!< HSI oscillator clock used as UART5 clock source 
.equ RCC_CFGR3_TIM2SW_PCLK1, (0x00000000) //!< PCLK1 used as TIM2 clock source 
.equ RCC_CFGR3_TIM2SW_PLL_Pos, (24) 
.equ RCC_CFGR3_TIM2SW_PLL_Msk, (0x1 << RCC_CFGR3_TIM2SW_PLL_Pos) //!< 0x01000000 
.equ RCC_CFGR3_TIM2SW_PLL, RCC_CFGR3_TIM2SW_PLL_Msk //!< PLL clock used as TIM2 clock source 
.equ RCC_CFGR3_TIM34SW_PCLK1, (0x00000000) //!< PCLK1 used as TIM3/TIM4 clock source 
.equ RCC_CFGR3_TIM34SW_PLL_Pos, (25) 
.equ RCC_CFGR3_TIM34SW_PLL_Msk, (0x1 << RCC_CFGR3_TIM34SW_PLL_Pos) //!< 0x02000000 
.equ RCC_CFGR3_TIM34SW_PLL, RCC_CFGR3_TIM34SW_PLL_Msk //!< PLL clock used as TIM3/TIM4 clock source 
// Legacy defines
.equ RCC_CFGR3_TIM1SW_HCLK, RCC_CFGR3_TIM1SW_PCLK2 
.equ RCC_CFGR3_TIM8SW_HCLK, RCC_CFGR3_TIM8SW_PCLK2 
.equ RCC_CFGR3_TIM15SW_HCLK, RCC_CFGR3_TIM15SW_PCLK2 
.equ RCC_CFGR3_TIM16SW_HCLK, RCC_CFGR3_TIM16SW_PCLK2 
.equ RCC_CFGR3_TIM17SW_HCLK, RCC_CFGR3_TIM17SW_PCLK2 
.equ RCC_CFGR3_TIM20SW_HCLK, RCC_CFGR3_TIM20SW_PCLK2 
.equ RCC_CFGR3_TIM2SW_HCLK, RCC_CFGR3_TIM2SW_PCLK1 
.equ RCC_CFGR3_TIM34SW_HCLK, RCC_CFGR3_TIM34SW_PCLK1 
//****************************************************************************
//
//                           Real-Time Clock (RTC)
//
//****************************************************************************
//*******************  Bits definition for RTC_TR register  ******************
.equ RTC_TR_PM_Pos, (22) 
.equ RTC_TR_PM_Msk, (0x1 << RTC_TR_PM_Pos) //!< 0x00400000 
.equ RTC_TR_PM, RTC_TR_PM_Msk 
.equ RTC_TR_HT_Pos, (20) 
.equ RTC_TR_HT_Msk, (0x3 << RTC_TR_HT_Pos) //!< 0x00300000 
.equ RTC_TR_HT, RTC_TR_HT_Msk 
.equ RTC_TR_HT_0, (0x1 << RTC_TR_HT_Pos) //!< 0x00100000 
.equ RTC_TR_HT_1, (0x2 << RTC_TR_HT_Pos) //!< 0x00200000 
.equ RTC_TR_HU_Pos, (16) 
.equ RTC_TR_HU_Msk, (0xF << RTC_TR_HU_Pos) //!< 0x000F0000 
.equ RTC_TR_HU, RTC_TR_HU_Msk 
.equ RTC_TR_HU_0, (0x1 << RTC_TR_HU_Pos) //!< 0x00010000 
.equ RTC_TR_HU_1, (0x2 << RTC_TR_HU_Pos) //!< 0x00020000 
.equ RTC_TR_HU_2, (0x4 << RTC_TR_HU_Pos) //!< 0x00040000 
.equ RTC_TR_HU_3, (0x8 << RTC_TR_HU_Pos) //!< 0x00080000 
.equ RTC_TR_MNT_Pos, (12) 
.equ RTC_TR_MNT_Msk, (0x7 << RTC_TR_MNT_Pos) //!< 0x00007000 
.equ RTC_TR_MNT, RTC_TR_MNT_Msk 
.equ RTC_TR_MNT_0, (0x1 << RTC_TR_MNT_Pos) //!< 0x00001000 
.equ RTC_TR_MNT_1, (0x2 << RTC_TR_MNT_Pos) //!< 0x00002000 
.equ RTC_TR_MNT_2, (0x4 << RTC_TR_MNT_Pos) //!< 0x00004000 
.equ RTC_TR_MNU_Pos, (8) 
.equ RTC_TR_MNU_Msk, (0xF << RTC_TR_MNU_Pos) //!< 0x00000F00 
.equ RTC_TR_MNU, RTC_TR_MNU_Msk 
.equ RTC_TR_MNU_0, (0x1 << RTC_TR_MNU_Pos) //!< 0x00000100 
.equ RTC_TR_MNU_1, (0x2 << RTC_TR_MNU_Pos) //!< 0x00000200 
.equ RTC_TR_MNU_2, (0x4 << RTC_TR_MNU_Pos) //!< 0x00000400 
.equ RTC_TR_MNU_3, (0x8 << RTC_TR_MNU_Pos) //!< 0x00000800 
.equ RTC_TR_ST_Pos, (4) 
.equ RTC_TR_ST_Msk, (0x7 << RTC_TR_ST_Pos) //!< 0x00000070 
.equ RTC_TR_ST, RTC_TR_ST_Msk 
.equ RTC_TR_ST_0, (0x1 << RTC_TR_ST_Pos) //!< 0x00000010 
.equ RTC_TR_ST_1, (0x2 << RTC_TR_ST_Pos) //!< 0x00000020 
.equ RTC_TR_ST_2, (0x4 << RTC_TR_ST_Pos) //!< 0x00000040 
.equ RTC_TR_SU_Pos, (0) 
.equ RTC_TR_SU_Msk, (0xF << RTC_TR_SU_Pos) //!< 0x0000000F 
.equ RTC_TR_SU, RTC_TR_SU_Msk 
.equ RTC_TR_SU_0, (0x1 << RTC_TR_SU_Pos) //!< 0x00000001 
.equ RTC_TR_SU_1, (0x2 << RTC_TR_SU_Pos) //!< 0x00000002 
.equ RTC_TR_SU_2, (0x4 << RTC_TR_SU_Pos) //!< 0x00000004 
.equ RTC_TR_SU_3, (0x8 << RTC_TR_SU_Pos) //!< 0x00000008 
//*******************  Bits definition for RTC_DR register  ******************
.equ RTC_DR_YT_Pos, (20) 
.equ RTC_DR_YT_Msk, (0xF << RTC_DR_YT_Pos) //!< 0x00F00000 
.equ RTC_DR_YT, RTC_DR_YT_Msk 
.equ RTC_DR_YT_0, (0x1 << RTC_DR_YT_Pos) //!< 0x00100000 
.equ RTC_DR_YT_1, (0x2 << RTC_DR_YT_Pos) //!< 0x00200000 
.equ RTC_DR_YT_2, (0x4 << RTC_DR_YT_Pos) //!< 0x00400000 
.equ RTC_DR_YT_3, (0x8 << RTC_DR_YT_Pos) //!< 0x00800000 
.equ RTC_DR_YU_Pos, (16) 
.equ RTC_DR_YU_Msk, (0xF << RTC_DR_YU_Pos) //!< 0x000F0000 
.equ RTC_DR_YU, RTC_DR_YU_Msk 
.equ RTC_DR_YU_0, (0x1 << RTC_DR_YU_Pos) //!< 0x00010000 
.equ RTC_DR_YU_1, (0x2 << RTC_DR_YU_Pos) //!< 0x00020000 
.equ RTC_DR_YU_2, (0x4 << RTC_DR_YU_Pos) //!< 0x00040000 
.equ RTC_DR_YU_3, (0x8 << RTC_DR_YU_Pos) //!< 0x00080000 
.equ RTC_DR_WDU_Pos, (13) 
.equ RTC_DR_WDU_Msk, (0x7 << RTC_DR_WDU_Pos) //!< 0x0000E000 
.equ RTC_DR_WDU, RTC_DR_WDU_Msk 
.equ RTC_DR_WDU_0, (0x1 << RTC_DR_WDU_Pos) //!< 0x00002000 
.equ RTC_DR_WDU_1, (0x2 << RTC_DR_WDU_Pos) //!< 0x00004000 
.equ RTC_DR_WDU_2, (0x4 << RTC_DR_WDU_Pos) //!< 0x00008000 
.equ RTC_DR_MT_Pos, (12) 
.equ RTC_DR_MT_Msk, (0x1 << RTC_DR_MT_Pos) //!< 0x00001000 
.equ RTC_DR_MT, RTC_DR_MT_Msk 
.equ RTC_DR_MU_Pos, (8) 
.equ RTC_DR_MU_Msk, (0xF << RTC_DR_MU_Pos) //!< 0x00000F00 
.equ RTC_DR_MU, RTC_DR_MU_Msk 
.equ RTC_DR_MU_0, (0x1 << RTC_DR_MU_Pos) //!< 0x00000100 
.equ RTC_DR_MU_1, (0x2 << RTC_DR_MU_Pos) //!< 0x00000200 
.equ RTC_DR_MU_2, (0x4 << RTC_DR_MU_Pos) //!< 0x00000400 
.equ RTC_DR_MU_3, (0x8 << RTC_DR_MU_Pos) //!< 0x00000800 
.equ RTC_DR_DT_Pos, (4) 
.equ RTC_DR_DT_Msk, (0x3 << RTC_DR_DT_Pos) //!< 0x00000030 
.equ RTC_DR_DT, RTC_DR_DT_Msk 
.equ RTC_DR_DT_0, (0x1 << RTC_DR_DT_Pos) //!< 0x00000010 
.equ RTC_DR_DT_1, (0x2 << RTC_DR_DT_Pos) //!< 0x00000020 
.equ RTC_DR_DU_Pos, (0) 
.equ RTC_DR_DU_Msk, (0xF << RTC_DR_DU_Pos) //!< 0x0000000F 
.equ RTC_DR_DU, RTC_DR_DU_Msk 
.equ RTC_DR_DU_0, (0x1 << RTC_DR_DU_Pos) //!< 0x00000001 
.equ RTC_DR_DU_1, (0x2 << RTC_DR_DU_Pos) //!< 0x00000002 
.equ RTC_DR_DU_2, (0x4 << RTC_DR_DU_Pos) //!< 0x00000004 
.equ RTC_DR_DU_3, (0x8 << RTC_DR_DU_Pos) //!< 0x00000008 
//*******************  Bits definition for RTC_CR register  ******************
.equ RTC_CR_COE_Pos, (23) 
.equ RTC_CR_COE_Msk, (0x1 << RTC_CR_COE_Pos) //!< 0x00800000 
.equ RTC_CR_COE, RTC_CR_COE_Msk 
.equ RTC_CR_OSEL_Pos, (21) 
.equ RTC_CR_OSEL_Msk, (0x3 << RTC_CR_OSEL_Pos) //!< 0x00600000 
.equ RTC_CR_OSEL, RTC_CR_OSEL_Msk 
.equ RTC_CR_OSEL_0, (0x1 << RTC_CR_OSEL_Pos) //!< 0x00200000 
.equ RTC_CR_OSEL_1, (0x2 << RTC_CR_OSEL_Pos) //!< 0x00400000 
.equ RTC_CR_POL_Pos, (20) 
.equ RTC_CR_POL_Msk, (0x1 << RTC_CR_POL_Pos) //!< 0x00100000 
.equ RTC_CR_POL, RTC_CR_POL_Msk 
.equ RTC_CR_COSEL_Pos, (19) 
.equ RTC_CR_COSEL_Msk, (0x1 << RTC_CR_COSEL_Pos) //!< 0x00080000 
.equ RTC_CR_COSEL, RTC_CR_COSEL_Msk 
.equ RTC_CR_BKP_Pos, (18) 
.equ RTC_CR_BKP_Msk, (0x1 << RTC_CR_BKP_Pos) //!< 0x00040000 
.equ RTC_CR_BKP, RTC_CR_BKP_Msk 
.equ RTC_CR_SUB1H_Pos, (17) 
.equ RTC_CR_SUB1H_Msk, (0x1 << RTC_CR_SUB1H_Pos) //!< 0x00020000 
.equ RTC_CR_SUB1H, RTC_CR_SUB1H_Msk 
.equ RTC_CR_ADD1H_Pos, (16) 
.equ RTC_CR_ADD1H_Msk, (0x1 << RTC_CR_ADD1H_Pos) //!< 0x00010000 
.equ RTC_CR_ADD1H, RTC_CR_ADD1H_Msk 
.equ RTC_CR_TSIE_Pos, (15) 
.equ RTC_CR_TSIE_Msk, (0x1 << RTC_CR_TSIE_Pos) //!< 0x00008000 
.equ RTC_CR_TSIE, RTC_CR_TSIE_Msk 
.equ RTC_CR_WUTIE_Pos, (14) 
.equ RTC_CR_WUTIE_Msk, (0x1 << RTC_CR_WUTIE_Pos) //!< 0x00004000 
.equ RTC_CR_WUTIE, RTC_CR_WUTIE_Msk 
.equ RTC_CR_ALRBIE_Pos, (13) 
.equ RTC_CR_ALRBIE_Msk, (0x1 << RTC_CR_ALRBIE_Pos) //!< 0x00002000 
.equ RTC_CR_ALRBIE, RTC_CR_ALRBIE_Msk 
.equ RTC_CR_ALRAIE_Pos, (12) 
.equ RTC_CR_ALRAIE_Msk, (0x1 << RTC_CR_ALRAIE_Pos) //!< 0x00001000 
.equ RTC_CR_ALRAIE, RTC_CR_ALRAIE_Msk 
.equ RTC_CR_TSE_Pos, (11) 
.equ RTC_CR_TSE_Msk, (0x1 << RTC_CR_TSE_Pos) //!< 0x00000800 
.equ RTC_CR_TSE, RTC_CR_TSE_Msk 
.equ RTC_CR_WUTE_Pos, (10) 
.equ RTC_CR_WUTE_Msk, (0x1 << RTC_CR_WUTE_Pos) //!< 0x00000400 
.equ RTC_CR_WUTE, RTC_CR_WUTE_Msk 
.equ RTC_CR_ALRBE_Pos, (9) 
.equ RTC_CR_ALRBE_Msk, (0x1 << RTC_CR_ALRBE_Pos) //!< 0x00000200 
.equ RTC_CR_ALRBE, RTC_CR_ALRBE_Msk 
.equ RTC_CR_ALRAE_Pos, (8) 
.equ RTC_CR_ALRAE_Msk, (0x1 << RTC_CR_ALRAE_Pos) //!< 0x00000100 
.equ RTC_CR_ALRAE, RTC_CR_ALRAE_Msk 
.equ RTC_CR_FMT_Pos, (6) 
.equ RTC_CR_FMT_Msk, (0x1 << RTC_CR_FMT_Pos) //!< 0x00000040 
.equ RTC_CR_FMT, RTC_CR_FMT_Msk 
.equ RTC_CR_BYPSHAD_Pos, (5) 
.equ RTC_CR_BYPSHAD_Msk, (0x1 << RTC_CR_BYPSHAD_Pos) //!< 0x00000020 
.equ RTC_CR_BYPSHAD, RTC_CR_BYPSHAD_Msk 
.equ RTC_CR_REFCKON_Pos, (4) 
.equ RTC_CR_REFCKON_Msk, (0x1 << RTC_CR_REFCKON_Pos) //!< 0x00000010 
.equ RTC_CR_REFCKON, RTC_CR_REFCKON_Msk 
.equ RTC_CR_TSEDGE_Pos, (3) 
.equ RTC_CR_TSEDGE_Msk, (0x1 << RTC_CR_TSEDGE_Pos) //!< 0x00000008 
.equ RTC_CR_TSEDGE, RTC_CR_TSEDGE_Msk 
.equ RTC_CR_WUCKSEL_Pos, (0) 
.equ RTC_CR_WUCKSEL_Msk, (0x7 << RTC_CR_WUCKSEL_Pos) //!< 0x00000007 
.equ RTC_CR_WUCKSEL, RTC_CR_WUCKSEL_Msk 
.equ RTC_CR_WUCKSEL_0, (0x1 << RTC_CR_WUCKSEL_Pos) //!< 0x00000001 
.equ RTC_CR_WUCKSEL_1, (0x2 << RTC_CR_WUCKSEL_Pos) //!< 0x00000002 
.equ RTC_CR_WUCKSEL_2, (0x4 << RTC_CR_WUCKSEL_Pos) //!< 0x00000004 
// Legacy defines
.equ RTC_CR_BCK_Pos, RTC_CR_BKP_Pos 
.equ RTC_CR_BCK_Msk, RTC_CR_BKP_Msk 
.equ RTC_CR_BCK, RTC_CR_BKP 
//*******************  Bits definition for RTC_ISR register  *****************
.equ RTC_ISR_RECALPF_Pos, (16) 
.equ RTC_ISR_RECALPF_Msk, (0x1 << RTC_ISR_RECALPF_Pos) //!< 0x00010000 
.equ RTC_ISR_RECALPF, RTC_ISR_RECALPF_Msk 
.equ RTC_ISR_TAMP3F_Pos, (15) 
.equ RTC_ISR_TAMP3F_Msk, (0x1 << RTC_ISR_TAMP3F_Pos) //!< 0x00008000 
.equ RTC_ISR_TAMP3F, RTC_ISR_TAMP3F_Msk 
.equ RTC_ISR_TAMP2F_Pos, (14) 
.equ RTC_ISR_TAMP2F_Msk, (0x1 << RTC_ISR_TAMP2F_Pos) //!< 0x00004000 
.equ RTC_ISR_TAMP2F, RTC_ISR_TAMP2F_Msk 
.equ RTC_ISR_TAMP1F_Pos, (13) 
.equ RTC_ISR_TAMP1F_Msk, (0x1 << RTC_ISR_TAMP1F_Pos) //!< 0x00002000 
.equ RTC_ISR_TAMP1F, RTC_ISR_TAMP1F_Msk 
.equ RTC_ISR_TSOVF_Pos, (12) 
.equ RTC_ISR_TSOVF_Msk, (0x1 << RTC_ISR_TSOVF_Pos) //!< 0x00001000 
.equ RTC_ISR_TSOVF, RTC_ISR_TSOVF_Msk 
.equ RTC_ISR_TSF_Pos, (11) 
.equ RTC_ISR_TSF_Msk, (0x1 << RTC_ISR_TSF_Pos) //!< 0x00000800 
.equ RTC_ISR_TSF, RTC_ISR_TSF_Msk 
.equ RTC_ISR_WUTF_Pos, (10) 
.equ RTC_ISR_WUTF_Msk, (0x1 << RTC_ISR_WUTF_Pos) //!< 0x00000400 
.equ RTC_ISR_WUTF, RTC_ISR_WUTF_Msk 
.equ RTC_ISR_ALRBF_Pos, (9) 
.equ RTC_ISR_ALRBF_Msk, (0x1 << RTC_ISR_ALRBF_Pos) //!< 0x00000200 
.equ RTC_ISR_ALRBF, RTC_ISR_ALRBF_Msk 
.equ RTC_ISR_ALRAF_Pos, (8) 
.equ RTC_ISR_ALRAF_Msk, (0x1 << RTC_ISR_ALRAF_Pos) //!< 0x00000100 
.equ RTC_ISR_ALRAF, RTC_ISR_ALRAF_Msk 
.equ RTC_ISR_INIT_Pos, (7) 
.equ RTC_ISR_INIT_Msk, (0x1 << RTC_ISR_INIT_Pos) //!< 0x00000080 
.equ RTC_ISR_INIT, RTC_ISR_INIT_Msk 
.equ RTC_ISR_INITF_Pos, (6) 
.equ RTC_ISR_INITF_Msk, (0x1 << RTC_ISR_INITF_Pos) //!< 0x00000040 
.equ RTC_ISR_INITF, RTC_ISR_INITF_Msk 
.equ RTC_ISR_RSF_Pos, (5) 
.equ RTC_ISR_RSF_Msk, (0x1 << RTC_ISR_RSF_Pos) //!< 0x00000020 
.equ RTC_ISR_RSF, RTC_ISR_RSF_Msk 
.equ RTC_ISR_INITS_Pos, (4) 
.equ RTC_ISR_INITS_Msk, (0x1 << RTC_ISR_INITS_Pos) //!< 0x00000010 
.equ RTC_ISR_INITS, RTC_ISR_INITS_Msk 
.equ RTC_ISR_SHPF_Pos, (3) 
.equ RTC_ISR_SHPF_Msk, (0x1 << RTC_ISR_SHPF_Pos) //!< 0x00000008 
.equ RTC_ISR_SHPF, RTC_ISR_SHPF_Msk 
.equ RTC_ISR_WUTWF_Pos, (2) 
.equ RTC_ISR_WUTWF_Msk, (0x1 << RTC_ISR_WUTWF_Pos) //!< 0x00000004 
.equ RTC_ISR_WUTWF, RTC_ISR_WUTWF_Msk 
.equ RTC_ISR_ALRBWF_Pos, (1) 
.equ RTC_ISR_ALRBWF_Msk, (0x1 << RTC_ISR_ALRBWF_Pos) //!< 0x00000002 
.equ RTC_ISR_ALRBWF, RTC_ISR_ALRBWF_Msk 
.equ RTC_ISR_ALRAWF_Pos, (0) 
.equ RTC_ISR_ALRAWF_Msk, (0x1 << RTC_ISR_ALRAWF_Pos) //!< 0x00000001 
.equ RTC_ISR_ALRAWF, RTC_ISR_ALRAWF_Msk 
//*******************  Bits definition for RTC_PRER register  ****************
.equ RTC_PRER_PREDIV_A_Pos, (16) 
.equ RTC_PRER_PREDIV_A_Msk, (0x7F << RTC_PRER_PREDIV_A_Pos) //!< 0x007F0000 
.equ RTC_PRER_PREDIV_A, RTC_PRER_PREDIV_A_Msk 
.equ RTC_PRER_PREDIV_S_Pos, (0) 
.equ RTC_PRER_PREDIV_S_Msk, (0x7FFF << RTC_PRER_PREDIV_S_Pos) //!< 0x00007FFF 
.equ RTC_PRER_PREDIV_S, RTC_PRER_PREDIV_S_Msk 
//*******************  Bits definition for RTC_WUTR register  ****************
.equ RTC_WUTR_WUT_Pos, (0) 
.equ RTC_WUTR_WUT_Msk, (0xFFFF << RTC_WUTR_WUT_Pos) //!< 0x0000FFFF 
.equ RTC_WUTR_WUT, RTC_WUTR_WUT_Msk 
//*******************  Bits definition for RTC_ALRMAR register  **************
.equ RTC_ALRMAR_MSK4_Pos, (31) 
.equ RTC_ALRMAR_MSK4_Msk, (0x1 << RTC_ALRMAR_MSK4_Pos) //!< 0x80000000 
.equ RTC_ALRMAR_MSK4, RTC_ALRMAR_MSK4_Msk 
.equ RTC_ALRMAR_WDSEL_Pos, (30) 
.equ RTC_ALRMAR_WDSEL_Msk, (0x1 << RTC_ALRMAR_WDSEL_Pos) //!< 0x40000000 
.equ RTC_ALRMAR_WDSEL, RTC_ALRMAR_WDSEL_Msk 
.equ RTC_ALRMAR_DT_Pos, (28) 
.equ RTC_ALRMAR_DT_Msk, (0x3 << RTC_ALRMAR_DT_Pos) //!< 0x30000000 
.equ RTC_ALRMAR_DT, RTC_ALRMAR_DT_Msk 
.equ RTC_ALRMAR_DT_0, (0x1 << RTC_ALRMAR_DT_Pos) //!< 0x10000000 
.equ RTC_ALRMAR_DT_1, (0x2 << RTC_ALRMAR_DT_Pos) //!< 0x20000000 
.equ RTC_ALRMAR_DU_Pos, (24) 
.equ RTC_ALRMAR_DU_Msk, (0xF << RTC_ALRMAR_DU_Pos) //!< 0x0F000000 
.equ RTC_ALRMAR_DU, RTC_ALRMAR_DU_Msk 
.equ RTC_ALRMAR_DU_0, (0x1 << RTC_ALRMAR_DU_Pos) //!< 0x01000000 
.equ RTC_ALRMAR_DU_1, (0x2 << RTC_ALRMAR_DU_Pos) //!< 0x02000000 
.equ RTC_ALRMAR_DU_2, (0x4 << RTC_ALRMAR_DU_Pos) //!< 0x04000000 
.equ RTC_ALRMAR_DU_3, (0x8 << RTC_ALRMAR_DU_Pos) //!< 0x08000000 
.equ RTC_ALRMAR_MSK3_Pos, (23) 
.equ RTC_ALRMAR_MSK3_Msk, (0x1 << RTC_ALRMAR_MSK3_Pos) //!< 0x00800000 
.equ RTC_ALRMAR_MSK3, RTC_ALRMAR_MSK3_Msk 
.equ RTC_ALRMAR_PM_Pos, (22) 
.equ RTC_ALRMAR_PM_Msk, (0x1 << RTC_ALRMAR_PM_Pos) //!< 0x00400000 
.equ RTC_ALRMAR_PM, RTC_ALRMAR_PM_Msk 
.equ RTC_ALRMAR_HT_Pos, (20) 
.equ RTC_ALRMAR_HT_Msk, (0x3 << RTC_ALRMAR_HT_Pos) //!< 0x00300000 
.equ RTC_ALRMAR_HT, RTC_ALRMAR_HT_Msk 
.equ RTC_ALRMAR_HT_0, (0x1 << RTC_ALRMAR_HT_Pos) //!< 0x00100000 
.equ RTC_ALRMAR_HT_1, (0x2 << RTC_ALRMAR_HT_Pos) //!< 0x00200000 
.equ RTC_ALRMAR_HU_Pos, (16) 
.equ RTC_ALRMAR_HU_Msk, (0xF << RTC_ALRMAR_HU_Pos) //!< 0x000F0000 
.equ RTC_ALRMAR_HU, RTC_ALRMAR_HU_Msk 
.equ RTC_ALRMAR_HU_0, (0x1 << RTC_ALRMAR_HU_Pos) //!< 0x00010000 
.equ RTC_ALRMAR_HU_1, (0x2 << RTC_ALRMAR_HU_Pos) //!< 0x00020000 
.equ RTC_ALRMAR_HU_2, (0x4 << RTC_ALRMAR_HU_Pos) //!< 0x00040000 
.equ RTC_ALRMAR_HU_3, (0x8 << RTC_ALRMAR_HU_Pos) //!< 0x00080000 
.equ RTC_ALRMAR_MSK2_Pos, (15) 
.equ RTC_ALRMAR_MSK2_Msk, (0x1 << RTC_ALRMAR_MSK2_Pos) //!< 0x00008000 
.equ RTC_ALRMAR_MSK2, RTC_ALRMAR_MSK2_Msk 
.equ RTC_ALRMAR_MNT_Pos, (12) 
.equ RTC_ALRMAR_MNT_Msk, (0x7 << RTC_ALRMAR_MNT_Pos) //!< 0x00007000 
.equ RTC_ALRMAR_MNT, RTC_ALRMAR_MNT_Msk 
.equ RTC_ALRMAR_MNT_0, (0x1 << RTC_ALRMAR_MNT_Pos) //!< 0x00001000 
.equ RTC_ALRMAR_MNT_1, (0x2 << RTC_ALRMAR_MNT_Pos) //!< 0x00002000 
.equ RTC_ALRMAR_MNT_2, (0x4 << RTC_ALRMAR_MNT_Pos) //!< 0x00004000 
.equ RTC_ALRMAR_MNU_Pos, (8) 
.equ RTC_ALRMAR_MNU_Msk, (0xF << RTC_ALRMAR_MNU_Pos) //!< 0x00000F00 
.equ RTC_ALRMAR_MNU, RTC_ALRMAR_MNU_Msk 
.equ RTC_ALRMAR_MNU_0, (0x1 << RTC_ALRMAR_MNU_Pos) //!< 0x00000100 
.equ RTC_ALRMAR_MNU_1, (0x2 << RTC_ALRMAR_MNU_Pos) //!< 0x00000200 
.equ RTC_ALRMAR_MNU_2, (0x4 << RTC_ALRMAR_MNU_Pos) //!< 0x00000400 
.equ RTC_ALRMAR_MNU_3, (0x8 << RTC_ALRMAR_MNU_Pos) //!< 0x00000800 
.equ RTC_ALRMAR_MSK1_Pos, (7) 
.equ RTC_ALRMAR_MSK1_Msk, (0x1 << RTC_ALRMAR_MSK1_Pos) //!< 0x00000080 
.equ RTC_ALRMAR_MSK1, RTC_ALRMAR_MSK1_Msk 
.equ RTC_ALRMAR_ST_Pos, (4) 
.equ RTC_ALRMAR_ST_Msk, (0x7 << RTC_ALRMAR_ST_Pos) //!< 0x00000070 
.equ RTC_ALRMAR_ST, RTC_ALRMAR_ST_Msk 
.equ RTC_ALRMAR_ST_0, (0x1 << RTC_ALRMAR_ST_Pos) //!< 0x00000010 
.equ RTC_ALRMAR_ST_1, (0x2 << RTC_ALRMAR_ST_Pos) //!< 0x00000020 
.equ RTC_ALRMAR_ST_2, (0x4 << RTC_ALRMAR_ST_Pos) //!< 0x00000040 
.equ RTC_ALRMAR_SU_Pos, (0) 
.equ RTC_ALRMAR_SU_Msk, (0xF << RTC_ALRMAR_SU_Pos) //!< 0x0000000F 
.equ RTC_ALRMAR_SU, RTC_ALRMAR_SU_Msk 
.equ RTC_ALRMAR_SU_0, (0x1 << RTC_ALRMAR_SU_Pos) //!< 0x00000001 
.equ RTC_ALRMAR_SU_1, (0x2 << RTC_ALRMAR_SU_Pos) //!< 0x00000002 
.equ RTC_ALRMAR_SU_2, (0x4 << RTC_ALRMAR_SU_Pos) //!< 0x00000004 
.equ RTC_ALRMAR_SU_3, (0x8 << RTC_ALRMAR_SU_Pos) //!< 0x00000008 
//*******************  Bits definition for RTC_ALRMBR register  **************
.equ RTC_ALRMBR_MSK4_Pos, (31) 
.equ RTC_ALRMBR_MSK4_Msk, (0x1 << RTC_ALRMBR_MSK4_Pos) //!< 0x80000000 
.equ RTC_ALRMBR_MSK4, RTC_ALRMBR_MSK4_Msk 
.equ RTC_ALRMBR_WDSEL_Pos, (30) 
.equ RTC_ALRMBR_WDSEL_Msk, (0x1 << RTC_ALRMBR_WDSEL_Pos) //!< 0x40000000 
.equ RTC_ALRMBR_WDSEL, RTC_ALRMBR_WDSEL_Msk 
.equ RTC_ALRMBR_DT_Pos, (28) 
.equ RTC_ALRMBR_DT_Msk, (0x3 << RTC_ALRMBR_DT_Pos) //!< 0x30000000 
.equ RTC_ALRMBR_DT, RTC_ALRMBR_DT_Msk 
.equ RTC_ALRMBR_DT_0, (0x1 << RTC_ALRMBR_DT_Pos) //!< 0x10000000 
.equ RTC_ALRMBR_DT_1, (0x2 << RTC_ALRMBR_DT_Pos) //!< 0x20000000 
.equ RTC_ALRMBR_DU_Pos, (24) 
.equ RTC_ALRMBR_DU_Msk, (0xF << RTC_ALRMBR_DU_Pos) //!< 0x0F000000 
.equ RTC_ALRMBR_DU, RTC_ALRMBR_DU_Msk 
.equ RTC_ALRMBR_DU_0, (0x1 << RTC_ALRMBR_DU_Pos) //!< 0x01000000 
.equ RTC_ALRMBR_DU_1, (0x2 << RTC_ALRMBR_DU_Pos) //!< 0x02000000 
.equ RTC_ALRMBR_DU_2, (0x4 << RTC_ALRMBR_DU_Pos) //!< 0x04000000 
.equ RTC_ALRMBR_DU_3, (0x8 << RTC_ALRMBR_DU_Pos) //!< 0x08000000 
.equ RTC_ALRMBR_MSK3_Pos, (23) 
.equ RTC_ALRMBR_MSK3_Msk, (0x1 << RTC_ALRMBR_MSK3_Pos) //!< 0x00800000 
.equ RTC_ALRMBR_MSK3, RTC_ALRMBR_MSK3_Msk 
.equ RTC_ALRMBR_PM_Pos, (22) 
.equ RTC_ALRMBR_PM_Msk, (0x1 << RTC_ALRMBR_PM_Pos) //!< 0x00400000 
.equ RTC_ALRMBR_PM, RTC_ALRMBR_PM_Msk 
.equ RTC_ALRMBR_HT_Pos, (20) 
.equ RTC_ALRMBR_HT_Msk, (0x3 << RTC_ALRMBR_HT_Pos) //!< 0x00300000 
.equ RTC_ALRMBR_HT, RTC_ALRMBR_HT_Msk 
.equ RTC_ALRMBR_HT_0, (0x1 << RTC_ALRMBR_HT_Pos) //!< 0x00100000 
.equ RTC_ALRMBR_HT_1, (0x2 << RTC_ALRMBR_HT_Pos) //!< 0x00200000 
.equ RTC_ALRMBR_HU_Pos, (16) 
.equ RTC_ALRMBR_HU_Msk, (0xF << RTC_ALRMBR_HU_Pos) //!< 0x000F0000 
.equ RTC_ALRMBR_HU, RTC_ALRMBR_HU_Msk 
.equ RTC_ALRMBR_HU_0, (0x1 << RTC_ALRMBR_HU_Pos) //!< 0x00010000 
.equ RTC_ALRMBR_HU_1, (0x2 << RTC_ALRMBR_HU_Pos) //!< 0x00020000 
.equ RTC_ALRMBR_HU_2, (0x4 << RTC_ALRMBR_HU_Pos) //!< 0x00040000 
.equ RTC_ALRMBR_HU_3, (0x8 << RTC_ALRMBR_HU_Pos) //!< 0x00080000 
.equ RTC_ALRMBR_MSK2_Pos, (15) 
.equ RTC_ALRMBR_MSK2_Msk, (0x1 << RTC_ALRMBR_MSK2_Pos) //!< 0x00008000 
.equ RTC_ALRMBR_MSK2, RTC_ALRMBR_MSK2_Msk 
.equ RTC_ALRMBR_MNT_Pos, (12) 
.equ RTC_ALRMBR_MNT_Msk, (0x7 << RTC_ALRMBR_MNT_Pos) //!< 0x00007000 
.equ RTC_ALRMBR_MNT, RTC_ALRMBR_MNT_Msk 
.equ RTC_ALRMBR_MNT_0, (0x1 << RTC_ALRMBR_MNT_Pos) //!< 0x00001000 
.equ RTC_ALRMBR_MNT_1, (0x2 << RTC_ALRMBR_MNT_Pos) //!< 0x00002000 
.equ RTC_ALRMBR_MNT_2, (0x4 << RTC_ALRMBR_MNT_Pos) //!< 0x00004000 
.equ RTC_ALRMBR_MNU_Pos, (8) 
.equ RTC_ALRMBR_MNU_Msk, (0xF << RTC_ALRMBR_MNU_Pos) //!< 0x00000F00 
.equ RTC_ALRMBR_MNU, RTC_ALRMBR_MNU_Msk 
.equ RTC_ALRMBR_MNU_0, (0x1 << RTC_ALRMBR_MNU_Pos) //!< 0x00000100 
.equ RTC_ALRMBR_MNU_1, (0x2 << RTC_ALRMBR_MNU_Pos) //!< 0x00000200 
.equ RTC_ALRMBR_MNU_2, (0x4 << RTC_ALRMBR_MNU_Pos) //!< 0x00000400 
.equ RTC_ALRMBR_MNU_3, (0x8 << RTC_ALRMBR_MNU_Pos) //!< 0x00000800 
.equ RTC_ALRMBR_MSK1_Pos, (7) 
.equ RTC_ALRMBR_MSK1_Msk, (0x1 << RTC_ALRMBR_MSK1_Pos) //!< 0x00000080 
.equ RTC_ALRMBR_MSK1, RTC_ALRMBR_MSK1_Msk 
.equ RTC_ALRMBR_ST_Pos, (4) 
.equ RTC_ALRMBR_ST_Msk, (0x7 << RTC_ALRMBR_ST_Pos) //!< 0x00000070 
.equ RTC_ALRMBR_ST, RTC_ALRMBR_ST_Msk 
.equ RTC_ALRMBR_ST_0, (0x1 << RTC_ALRMBR_ST_Pos) //!< 0x00000010 
.equ RTC_ALRMBR_ST_1, (0x2 << RTC_ALRMBR_ST_Pos) //!< 0x00000020 
.equ RTC_ALRMBR_ST_2, (0x4 << RTC_ALRMBR_ST_Pos) //!< 0x00000040 
.equ RTC_ALRMBR_SU_Pos, (0) 
.equ RTC_ALRMBR_SU_Msk, (0xF << RTC_ALRMBR_SU_Pos) //!< 0x0000000F 
.equ RTC_ALRMBR_SU, RTC_ALRMBR_SU_Msk 
.equ RTC_ALRMBR_SU_0, (0x1 << RTC_ALRMBR_SU_Pos) //!< 0x00000001 
.equ RTC_ALRMBR_SU_1, (0x2 << RTC_ALRMBR_SU_Pos) //!< 0x00000002 
.equ RTC_ALRMBR_SU_2, (0x4 << RTC_ALRMBR_SU_Pos) //!< 0x00000004 
.equ RTC_ALRMBR_SU_3, (0x8 << RTC_ALRMBR_SU_Pos) //!< 0x00000008 
//*******************  Bits definition for RTC_WPR register  *****************
.equ RTC_WPR_KEY_Pos, (0) 
.equ RTC_WPR_KEY_Msk, (0xFF << RTC_WPR_KEY_Pos) //!< 0x000000FF 
.equ RTC_WPR_KEY, RTC_WPR_KEY_Msk 
//*******************  Bits definition for RTC_SSR register  *****************
.equ RTC_SSR_SS_Pos, (0) 
.equ RTC_SSR_SS_Msk, (0xFFFF << RTC_SSR_SS_Pos) //!< 0x0000FFFF 
.equ RTC_SSR_SS, RTC_SSR_SS_Msk 
//*******************  Bits definition for RTC_SHIFTR register  **************
.equ RTC_SHIFTR_SUBFS_Pos, (0) 
.equ RTC_SHIFTR_SUBFS_Msk, (0x7FFF << RTC_SHIFTR_SUBFS_Pos) //!< 0x00007FFF 
.equ RTC_SHIFTR_SUBFS, RTC_SHIFTR_SUBFS_Msk 
.equ RTC_SHIFTR_ADD1S_Pos, (31) 
.equ RTC_SHIFTR_ADD1S_Msk, (0x1 << RTC_SHIFTR_ADD1S_Pos) //!< 0x80000000 
.equ RTC_SHIFTR_ADD1S, RTC_SHIFTR_ADD1S_Msk 
//*******************  Bits definition for RTC_TSTR register  ****************
.equ RTC_TSTR_PM_Pos, (22) 
.equ RTC_TSTR_PM_Msk, (0x1 << RTC_TSTR_PM_Pos) //!< 0x00400000 
.equ RTC_TSTR_PM, RTC_TSTR_PM_Msk 
.equ RTC_TSTR_HT_Pos, (20) 
.equ RTC_TSTR_HT_Msk, (0x3 << RTC_TSTR_HT_Pos) //!< 0x00300000 
.equ RTC_TSTR_HT, RTC_TSTR_HT_Msk 
.equ RTC_TSTR_HT_0, (0x1 << RTC_TSTR_HT_Pos) //!< 0x00100000 
.equ RTC_TSTR_HT_1, (0x2 << RTC_TSTR_HT_Pos) //!< 0x00200000 
.equ RTC_TSTR_HU_Pos, (16) 
.equ RTC_TSTR_HU_Msk, (0xF << RTC_TSTR_HU_Pos) //!< 0x000F0000 
.equ RTC_TSTR_HU, RTC_TSTR_HU_Msk 
.equ RTC_TSTR_HU_0, (0x1 << RTC_TSTR_HU_Pos) //!< 0x00010000 
.equ RTC_TSTR_HU_1, (0x2 << RTC_TSTR_HU_Pos) //!< 0x00020000 
.equ RTC_TSTR_HU_2, (0x4 << RTC_TSTR_HU_Pos) //!< 0x00040000 
.equ RTC_TSTR_HU_3, (0x8 << RTC_TSTR_HU_Pos) //!< 0x00080000 
.equ RTC_TSTR_MNT_Pos, (12) 
.equ RTC_TSTR_MNT_Msk, (0x7 << RTC_TSTR_MNT_Pos) //!< 0x00007000 
.equ RTC_TSTR_MNT, RTC_TSTR_MNT_Msk 
.equ RTC_TSTR_MNT_0, (0x1 << RTC_TSTR_MNT_Pos) //!< 0x00001000 
.equ RTC_TSTR_MNT_1, (0x2 << RTC_TSTR_MNT_Pos) //!< 0x00002000 
.equ RTC_TSTR_MNT_2, (0x4 << RTC_TSTR_MNT_Pos) //!< 0x00004000 
.equ RTC_TSTR_MNU_Pos, (8) 
.equ RTC_TSTR_MNU_Msk, (0xF << RTC_TSTR_MNU_Pos) //!< 0x00000F00 
.equ RTC_TSTR_MNU, RTC_TSTR_MNU_Msk 
.equ RTC_TSTR_MNU_0, (0x1 << RTC_TSTR_MNU_Pos) //!< 0x00000100 
.equ RTC_TSTR_MNU_1, (0x2 << RTC_TSTR_MNU_Pos) //!< 0x00000200 
.equ RTC_TSTR_MNU_2, (0x4 << RTC_TSTR_MNU_Pos) //!< 0x00000400 
.equ RTC_TSTR_MNU_3, (0x8 << RTC_TSTR_MNU_Pos) //!< 0x00000800 
.equ RTC_TSTR_ST_Pos, (4) 
.equ RTC_TSTR_ST_Msk, (0x7 << RTC_TSTR_ST_Pos) //!< 0x00000070 
.equ RTC_TSTR_ST, RTC_TSTR_ST_Msk 
.equ RTC_TSTR_ST_0, (0x1 << RTC_TSTR_ST_Pos) //!< 0x00000010 
.equ RTC_TSTR_ST_1, (0x2 << RTC_TSTR_ST_Pos) //!< 0x00000020 
.equ RTC_TSTR_ST_2, (0x4 << RTC_TSTR_ST_Pos) //!< 0x00000040 
.equ RTC_TSTR_SU_Pos, (0) 
.equ RTC_TSTR_SU_Msk, (0xF << RTC_TSTR_SU_Pos) //!< 0x0000000F 
.equ RTC_TSTR_SU, RTC_TSTR_SU_Msk 
.equ RTC_TSTR_SU_0, (0x1 << RTC_TSTR_SU_Pos) //!< 0x00000001 
.equ RTC_TSTR_SU_1, (0x2 << RTC_TSTR_SU_Pos) //!< 0x00000002 
.equ RTC_TSTR_SU_2, (0x4 << RTC_TSTR_SU_Pos) //!< 0x00000004 
.equ RTC_TSTR_SU_3, (0x8 << RTC_TSTR_SU_Pos) //!< 0x00000008 
//*******************  Bits definition for RTC_TSDR register  ****************
.equ RTC_TSDR_WDU_Pos, (13) 
.equ RTC_TSDR_WDU_Msk, (0x7 << RTC_TSDR_WDU_Pos) //!< 0x0000E000 
.equ RTC_TSDR_WDU, RTC_TSDR_WDU_Msk 
.equ RTC_TSDR_WDU_0, (0x1 << RTC_TSDR_WDU_Pos) //!< 0x00002000 
.equ RTC_TSDR_WDU_1, (0x2 << RTC_TSDR_WDU_Pos) //!< 0x00004000 
.equ RTC_TSDR_WDU_2, (0x4 << RTC_TSDR_WDU_Pos) //!< 0x00008000 
.equ RTC_TSDR_MT_Pos, (12) 
.equ RTC_TSDR_MT_Msk, (0x1 << RTC_TSDR_MT_Pos) //!< 0x00001000 
.equ RTC_TSDR_MT, RTC_TSDR_MT_Msk 
.equ RTC_TSDR_MU_Pos, (8) 
.equ RTC_TSDR_MU_Msk, (0xF << RTC_TSDR_MU_Pos) //!< 0x00000F00 
.equ RTC_TSDR_MU, RTC_TSDR_MU_Msk 
.equ RTC_TSDR_MU_0, (0x1 << RTC_TSDR_MU_Pos) //!< 0x00000100 
.equ RTC_TSDR_MU_1, (0x2 << RTC_TSDR_MU_Pos) //!< 0x00000200 
.equ RTC_TSDR_MU_2, (0x4 << RTC_TSDR_MU_Pos) //!< 0x00000400 
.equ RTC_TSDR_MU_3, (0x8 << RTC_TSDR_MU_Pos) //!< 0x00000800 
.equ RTC_TSDR_DT_Pos, (4) 
.equ RTC_TSDR_DT_Msk, (0x3 << RTC_TSDR_DT_Pos) //!< 0x00000030 
.equ RTC_TSDR_DT, RTC_TSDR_DT_Msk 
.equ RTC_TSDR_DT_0, (0x1 << RTC_TSDR_DT_Pos) //!< 0x00000010 
.equ RTC_TSDR_DT_1, (0x2 << RTC_TSDR_DT_Pos) //!< 0x00000020 
.equ RTC_TSDR_DU_Pos, (0) 
.equ RTC_TSDR_DU_Msk, (0xF << RTC_TSDR_DU_Pos) //!< 0x0000000F 
.equ RTC_TSDR_DU, RTC_TSDR_DU_Msk 
.equ RTC_TSDR_DU_0, (0x1 << RTC_TSDR_DU_Pos) //!< 0x00000001 
.equ RTC_TSDR_DU_1, (0x2 << RTC_TSDR_DU_Pos) //!< 0x00000002 
.equ RTC_TSDR_DU_2, (0x4 << RTC_TSDR_DU_Pos) //!< 0x00000004 
.equ RTC_TSDR_DU_3, (0x8 << RTC_TSDR_DU_Pos) //!< 0x00000008 
//*******************  Bits definition for RTC_TSSSR register  ***************
.equ RTC_TSSSR_SS_Pos, (0) 
.equ RTC_TSSSR_SS_Msk, (0xFFFF << RTC_TSSSR_SS_Pos) //!< 0x0000FFFF 
.equ RTC_TSSSR_SS, RTC_TSSSR_SS_Msk 
//*******************  Bits definition for RTC_CAL register  ****************
.equ RTC_CALR_CALP_Pos, (15) 
.equ RTC_CALR_CALP_Msk, (0x1 << RTC_CALR_CALP_Pos) //!< 0x00008000 
.equ RTC_CALR_CALP, RTC_CALR_CALP_Msk 
.equ RTC_CALR_CALW8_Pos, (14) 
.equ RTC_CALR_CALW8_Msk, (0x1 << RTC_CALR_CALW8_Pos) //!< 0x00004000 
.equ RTC_CALR_CALW8, RTC_CALR_CALW8_Msk 
.equ RTC_CALR_CALW16_Pos, (13) 
.equ RTC_CALR_CALW16_Msk, (0x1 << RTC_CALR_CALW16_Pos) //!< 0x00002000 
.equ RTC_CALR_CALW16, RTC_CALR_CALW16_Msk 
.equ RTC_CALR_CALM_Pos, (0) 
.equ RTC_CALR_CALM_Msk, (0x1FF << RTC_CALR_CALM_Pos) //!< 0x000001FF 
.equ RTC_CALR_CALM, RTC_CALR_CALM_Msk 
.equ RTC_CALR_CALM_0, (0x001 << RTC_CALR_CALM_Pos) //!< 0x00000001 
.equ RTC_CALR_CALM_1, (0x002 << RTC_CALR_CALM_Pos) //!< 0x00000002 
.equ RTC_CALR_CALM_2, (0x004 << RTC_CALR_CALM_Pos) //!< 0x00000004 
.equ RTC_CALR_CALM_3, (0x008 << RTC_CALR_CALM_Pos) //!< 0x00000008 
.equ RTC_CALR_CALM_4, (0x010 << RTC_CALR_CALM_Pos) //!< 0x00000010 
.equ RTC_CALR_CALM_5, (0x020 << RTC_CALR_CALM_Pos) //!< 0x00000020 
.equ RTC_CALR_CALM_6, (0x040 << RTC_CALR_CALM_Pos) //!< 0x00000040 
.equ RTC_CALR_CALM_7, (0x080 << RTC_CALR_CALM_Pos) //!< 0x00000080 
.equ RTC_CALR_CALM_8, (0x100 << RTC_CALR_CALM_Pos) //!< 0x00000100 
//*******************  Bits definition for RTC_TAFCR register  ***************
.equ RTC_TAFCR_PC15MODE_Pos, (23) 
.equ RTC_TAFCR_PC15MODE_Msk, (0x1 << RTC_TAFCR_PC15MODE_Pos) //!< 0x00800000 
.equ RTC_TAFCR_PC15MODE, RTC_TAFCR_PC15MODE_Msk 
.equ RTC_TAFCR_PC15VALUE_Pos, (22) 
.equ RTC_TAFCR_PC15VALUE_Msk, (0x1 << RTC_TAFCR_PC15VALUE_Pos) //!< 0x00400000 
.equ RTC_TAFCR_PC15VALUE, RTC_TAFCR_PC15VALUE_Msk 
.equ RTC_TAFCR_PC14MODE_Pos, (21) 
.equ RTC_TAFCR_PC14MODE_Msk, (0x1 << RTC_TAFCR_PC14MODE_Pos) //!< 0x00200000 
.equ RTC_TAFCR_PC14MODE, RTC_TAFCR_PC14MODE_Msk 
.equ RTC_TAFCR_PC14VALUE_Pos, (20) 
.equ RTC_TAFCR_PC14VALUE_Msk, (0x1 << RTC_TAFCR_PC14VALUE_Pos) //!< 0x00100000 
.equ RTC_TAFCR_PC14VALUE, RTC_TAFCR_PC14VALUE_Msk 
.equ RTC_TAFCR_PC13MODE_Pos, (19) 
.equ RTC_TAFCR_PC13MODE_Msk, (0x1 << RTC_TAFCR_PC13MODE_Pos) //!< 0x00080000 
.equ RTC_TAFCR_PC13MODE, RTC_TAFCR_PC13MODE_Msk 
.equ RTC_TAFCR_PC13VALUE_Pos, (18) 
.equ RTC_TAFCR_PC13VALUE_Msk, (0x1 << RTC_TAFCR_PC13VALUE_Pos) //!< 0x00040000 
.equ RTC_TAFCR_PC13VALUE, RTC_TAFCR_PC13VALUE_Msk 
.equ RTC_TAFCR_TAMPPUDIS_Pos, (15) 
.equ RTC_TAFCR_TAMPPUDIS_Msk, (0x1 << RTC_TAFCR_TAMPPUDIS_Pos) //!< 0x00008000 
.equ RTC_TAFCR_TAMPPUDIS, RTC_TAFCR_TAMPPUDIS_Msk 
.equ RTC_TAFCR_TAMPPRCH_Pos, (13) 
.equ RTC_TAFCR_TAMPPRCH_Msk, (0x3 << RTC_TAFCR_TAMPPRCH_Pos) //!< 0x00006000 
.equ RTC_TAFCR_TAMPPRCH, RTC_TAFCR_TAMPPRCH_Msk 
.equ RTC_TAFCR_TAMPPRCH_0, (0x1 << RTC_TAFCR_TAMPPRCH_Pos) //!< 0x00002000 
.equ RTC_TAFCR_TAMPPRCH_1, (0x2 << RTC_TAFCR_TAMPPRCH_Pos) //!< 0x00004000 
.equ RTC_TAFCR_TAMPFLT_Pos, (11) 
.equ RTC_TAFCR_TAMPFLT_Msk, (0x3 << RTC_TAFCR_TAMPFLT_Pos) //!< 0x00001800 
.equ RTC_TAFCR_TAMPFLT, RTC_TAFCR_TAMPFLT_Msk 
.equ RTC_TAFCR_TAMPFLT_0, (0x1 << RTC_TAFCR_TAMPFLT_Pos) //!< 0x00000800 
.equ RTC_TAFCR_TAMPFLT_1, (0x2 << RTC_TAFCR_TAMPFLT_Pos) //!< 0x00001000 
.equ RTC_TAFCR_TAMPFREQ_Pos, (8) 
.equ RTC_TAFCR_TAMPFREQ_Msk, (0x7 << RTC_TAFCR_TAMPFREQ_Pos) //!< 0x00000700 
.equ RTC_TAFCR_TAMPFREQ, RTC_TAFCR_TAMPFREQ_Msk 
.equ RTC_TAFCR_TAMPFREQ_0, (0x1 << RTC_TAFCR_TAMPFREQ_Pos) //!< 0x00000100 
.equ RTC_TAFCR_TAMPFREQ_1, (0x2 << RTC_TAFCR_TAMPFREQ_Pos) //!< 0x00000200 
.equ RTC_TAFCR_TAMPFREQ_2, (0x4 << RTC_TAFCR_TAMPFREQ_Pos) //!< 0x00000400 
.equ RTC_TAFCR_TAMPTS_Pos, (7) 
.equ RTC_TAFCR_TAMPTS_Msk, (0x1 << RTC_TAFCR_TAMPTS_Pos) //!< 0x00000080 
.equ RTC_TAFCR_TAMPTS, RTC_TAFCR_TAMPTS_Msk 
.equ RTC_TAFCR_TAMP3TRG_Pos, (6) 
.equ RTC_TAFCR_TAMP3TRG_Msk, (0x1 << RTC_TAFCR_TAMP3TRG_Pos) //!< 0x00000040 
.equ RTC_TAFCR_TAMP3TRG, RTC_TAFCR_TAMP3TRG_Msk 
.equ RTC_TAFCR_TAMP3E_Pos, (5) 
.equ RTC_TAFCR_TAMP3E_Msk, (0x1 << RTC_TAFCR_TAMP3E_Pos) //!< 0x00000020 
.equ RTC_TAFCR_TAMP3E, RTC_TAFCR_TAMP3E_Msk 
.equ RTC_TAFCR_TAMP2TRG_Pos, (4) 
.equ RTC_TAFCR_TAMP2TRG_Msk, (0x1 << RTC_TAFCR_TAMP2TRG_Pos) //!< 0x00000010 
.equ RTC_TAFCR_TAMP2TRG, RTC_TAFCR_TAMP2TRG_Msk 
.equ RTC_TAFCR_TAMP2E_Pos, (3) 
.equ RTC_TAFCR_TAMP2E_Msk, (0x1 << RTC_TAFCR_TAMP2E_Pos) //!< 0x00000008 
.equ RTC_TAFCR_TAMP2E, RTC_TAFCR_TAMP2E_Msk 
.equ RTC_TAFCR_TAMPIE_Pos, (2) 
.equ RTC_TAFCR_TAMPIE_Msk, (0x1 << RTC_TAFCR_TAMPIE_Pos) //!< 0x00000004 
.equ RTC_TAFCR_TAMPIE, RTC_TAFCR_TAMPIE_Msk 
.equ RTC_TAFCR_TAMP1TRG_Pos, (1) 
.equ RTC_TAFCR_TAMP1TRG_Msk, (0x1 << RTC_TAFCR_TAMP1TRG_Pos) //!< 0x00000002 
.equ RTC_TAFCR_TAMP1TRG, RTC_TAFCR_TAMP1TRG_Msk 
.equ RTC_TAFCR_TAMP1E_Pos, (0) 
.equ RTC_TAFCR_TAMP1E_Msk, (0x1 << RTC_TAFCR_TAMP1E_Pos) //!< 0x00000001 
.equ RTC_TAFCR_TAMP1E, RTC_TAFCR_TAMP1E_Msk 
// Reference defines
.equ RTC_TAFCR_ALARMOUTTYPE, RTC_TAFCR_PC13VALUE 
//*******************  Bits definition for RTC_ALRMASSR register  ************
.equ RTC_ALRMASSR_MASKSS_Pos, (24) 
.equ RTC_ALRMASSR_MASKSS_Msk, (0xF << RTC_ALRMASSR_MASKSS_Pos) //!< 0x0F000000 
.equ RTC_ALRMASSR_MASKSS, RTC_ALRMASSR_MASKSS_Msk 
.equ RTC_ALRMASSR_MASKSS_0, (0x1 << RTC_ALRMASSR_MASKSS_Pos) //!< 0x01000000 
.equ RTC_ALRMASSR_MASKSS_1, (0x2 << RTC_ALRMASSR_MASKSS_Pos) //!< 0x02000000 
.equ RTC_ALRMASSR_MASKSS_2, (0x4 << RTC_ALRMASSR_MASKSS_Pos) //!< 0x04000000 
.equ RTC_ALRMASSR_MASKSS_3, (0x8 << RTC_ALRMASSR_MASKSS_Pos) //!< 0x08000000 
.equ RTC_ALRMASSR_SS_Pos, (0) 
.equ RTC_ALRMASSR_SS_Msk, (0x7FFF << RTC_ALRMASSR_SS_Pos) //!< 0x00007FFF 
.equ RTC_ALRMASSR_SS, RTC_ALRMASSR_SS_Msk 
//*******************  Bits definition for RTC_ALRMBSSR register  ************
.equ RTC_ALRMBSSR_MASKSS_Pos, (24) 
.equ RTC_ALRMBSSR_MASKSS_Msk, (0xF << RTC_ALRMBSSR_MASKSS_Pos) //!< 0x0F000000 
.equ RTC_ALRMBSSR_MASKSS, RTC_ALRMBSSR_MASKSS_Msk 
.equ RTC_ALRMBSSR_MASKSS_0, (0x1 << RTC_ALRMBSSR_MASKSS_Pos) //!< 0x01000000 
.equ RTC_ALRMBSSR_MASKSS_1, (0x2 << RTC_ALRMBSSR_MASKSS_Pos) //!< 0x02000000 
.equ RTC_ALRMBSSR_MASKSS_2, (0x4 << RTC_ALRMBSSR_MASKSS_Pos) //!< 0x04000000 
.equ RTC_ALRMBSSR_MASKSS_3, (0x8 << RTC_ALRMBSSR_MASKSS_Pos) //!< 0x08000000 
.equ RTC_ALRMBSSR_SS_Pos, (0) 
.equ RTC_ALRMBSSR_SS_Msk, (0x7FFF << RTC_ALRMBSSR_SS_Pos) //!< 0x00007FFF 
.equ RTC_ALRMBSSR_SS, RTC_ALRMBSSR_SS_Msk 
//*******************  Bits definition for RTC_BKP0R register  ***************
.equ RTC_BKP0R_Pos, (0) 
.equ RTC_BKP0R_Msk, (0xFFFFFFFF << RTC_BKP0R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP1R register  ***************
.equ RTC_BKP1R_Pos, (0) 
.equ RTC_BKP1R_Msk, (0xFFFFFFFF << RTC_BKP1R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP2R register  ***************
.equ RTC_BKP2R_Pos, (0) 
.equ RTC_BKP2R_Msk, (0xFFFFFFFF << RTC_BKP2R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP3R register  ***************
.equ RTC_BKP3R_Pos, (0) 
.equ RTC_BKP3R_Msk, (0xFFFFFFFF << RTC_BKP3R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP4R register  ***************
.equ RTC_BKP4R_Pos, (0) 
.equ RTC_BKP4R_Msk, (0xFFFFFFFF << RTC_BKP4R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP5R register  ***************
.equ RTC_BKP5R_Pos, (0) 
.equ RTC_BKP5R_Msk, (0xFFFFFFFF << RTC_BKP5R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP6R register  ***************
.equ RTC_BKP6R_Pos, (0) 
.equ RTC_BKP6R_Msk, (0xFFFFFFFF << RTC_BKP6R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP7R register  ***************
.equ RTC_BKP7R_Pos, (0) 
.equ RTC_BKP7R_Msk, (0xFFFFFFFF << RTC_BKP7R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP8R register  ***************
.equ RTC_BKP8R_Pos, (0) 
.equ RTC_BKP8R_Msk, (0xFFFFFFFF << RTC_BKP8R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP9R register  ***************
.equ RTC_BKP9R_Pos, (0) 
.equ RTC_BKP9R_Msk, (0xFFFFFFFF << RTC_BKP9R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP10R register  **************
.equ RTC_BKP10R_Pos, (0) 
.equ RTC_BKP10R_Msk, (0xFFFFFFFF << RTC_BKP10R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP11R register  **************
.equ RTC_BKP11R_Pos, (0) 
.equ RTC_BKP11R_Msk, (0xFFFFFFFF << RTC_BKP11R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP12R register  **************
.equ RTC_BKP12R_Pos, (0) 
.equ RTC_BKP12R_Msk, (0xFFFFFFFF << RTC_BKP12R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP13R register  **************
.equ RTC_BKP13R_Pos, (0) 
.equ RTC_BKP13R_Msk, (0xFFFFFFFF << RTC_BKP13R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP14R register  **************
.equ RTC_BKP14R_Pos, (0) 
.equ RTC_BKP14R_Msk, (0xFFFFFFFF << RTC_BKP14R_Pos) //!< 0xFFFFFFFF 
//*******************  Bits definition for RTC_BKP15R register  **************
.equ RTC_BKP15R_Pos, (0) 
.equ RTC_BKP15R_Msk, (0xFFFFFFFF << RTC_BKP15R_Pos) //!< 0xFFFFFFFF 
//******************* Number of backup registers *****************************
.equ RTC_BKP_NUMBER, 16 
//****************************************************************************
//
//                        Serial Peripheral Interface (SPI)
//
//****************************************************************************
//******************  Bit definition for SPI_CR1 register  *******************
.equ SPI_CR1_CPHA_Pos, (0) 
.equ SPI_CR1_CPHA_Msk, (0x1 << SPI_CR1_CPHA_Pos) //!< 0x00000001 
.equ SPI_CR1_CPHA, SPI_CR1_CPHA_Msk //!< Clock Phase 
.equ SPI_CR1_CPOL_Pos, (1) 
.equ SPI_CR1_CPOL_Msk, (0x1 << SPI_CR1_CPOL_Pos) //!< 0x00000002 
.equ SPI_CR1_CPOL, SPI_CR1_CPOL_Msk //!< Clock Polarity 
.equ SPI_CR1_MSTR_Pos, (2) 
.equ SPI_CR1_MSTR_Msk, (0x1 << SPI_CR1_MSTR_Pos) //!< 0x00000004 
.equ SPI_CR1_MSTR, SPI_CR1_MSTR_Msk //!< Master Selection 
.equ SPI_CR1_BR_Pos, (3) 
.equ SPI_CR1_BR_Msk, (0x7 << SPI_CR1_BR_Pos) //!< 0x00000038 
.equ SPI_CR1_BR, SPI_CR1_BR_Msk //!< BR[2:0] bits (Baud Rate Control) 
.equ SPI_CR1_BR_0, (0x1 << SPI_CR1_BR_Pos) //!< 0x00000008 
.equ SPI_CR1_BR_1, (0x2 << SPI_CR1_BR_Pos) //!< 0x00000010 
.equ SPI_CR1_BR_2, (0x4 << SPI_CR1_BR_Pos) //!< 0x00000020 
.equ SPI_CR1_SPE_Pos, (6) 
.equ SPI_CR1_SPE_Msk, (0x1 << SPI_CR1_SPE_Pos) //!< 0x00000040 
.equ SPI_CR1_SPE, SPI_CR1_SPE_Msk //!< SPI Enable 
.equ SPI_CR1_LSBFIRST_Pos, (7) 
.equ SPI_CR1_LSBFIRST_Msk, (0x1 << SPI_CR1_LSBFIRST_Pos) //!< 0x00000080 
.equ SPI_CR1_LSBFIRST, SPI_CR1_LSBFIRST_Msk //!< Frame Format 
.equ SPI_CR1_SSI_Pos, (8) 
.equ SPI_CR1_SSI_Msk, (0x1 << SPI_CR1_SSI_Pos) //!< 0x00000100 
.equ SPI_CR1_SSI, SPI_CR1_SSI_Msk //!< Internal slave select 
.equ SPI_CR1_SSM_Pos, (9) 
.equ SPI_CR1_SSM_Msk, (0x1 << SPI_CR1_SSM_Pos) //!< 0x00000200 
.equ SPI_CR1_SSM, SPI_CR1_SSM_Msk //!< Software slave management 
.equ SPI_CR1_RXONLY_Pos, (10) 
.equ SPI_CR1_RXONLY_Msk, (0x1 << SPI_CR1_RXONLY_Pos) //!< 0x00000400 
.equ SPI_CR1_RXONLY, SPI_CR1_RXONLY_Msk //!< Receive only 
.equ SPI_CR1_CRCL_Pos, (11) 
.equ SPI_CR1_CRCL_Msk, (0x1 << SPI_CR1_CRCL_Pos) //!< 0x00000800 
.equ SPI_CR1_CRCL, SPI_CR1_CRCL_Msk //!< CRC Length 
.equ SPI_CR1_CRCNEXT_Pos, (12) 
.equ SPI_CR1_CRCNEXT_Msk, (0x1 << SPI_CR1_CRCNEXT_Pos) //!< 0x00001000 
.equ SPI_CR1_CRCNEXT, SPI_CR1_CRCNEXT_Msk //!< Transmit CRC next 
.equ SPI_CR1_CRCEN_Pos, (13) 
.equ SPI_CR1_CRCEN_Msk, (0x1 << SPI_CR1_CRCEN_Pos) //!< 0x00002000 
.equ SPI_CR1_CRCEN, SPI_CR1_CRCEN_Msk //!< Hardware CRC calculation enable 
.equ SPI_CR1_BIDIOE_Pos, (14) 
.equ SPI_CR1_BIDIOE_Msk, (0x1 << SPI_CR1_BIDIOE_Pos) //!< 0x00004000 
.equ SPI_CR1_BIDIOE, SPI_CR1_BIDIOE_Msk //!< Output enable in bidirectional mode 
.equ SPI_CR1_BIDIMODE_Pos, (15) 
.equ SPI_CR1_BIDIMODE_Msk, (0x1 << SPI_CR1_BIDIMODE_Pos) //!< 0x00008000 
.equ SPI_CR1_BIDIMODE, SPI_CR1_BIDIMODE_Msk //!< Bidirectional data mode enable 
//******************  Bit definition for SPI_CR2 register  *******************
.equ SPI_CR2_RXDMAEN_Pos, (0) 
.equ SPI_CR2_RXDMAEN_Msk, (0x1 << SPI_CR2_RXDMAEN_Pos) //!< 0x00000001 
.equ SPI_CR2_RXDMAEN, SPI_CR2_RXDMAEN_Msk //!< Rx Buffer DMA Enable 
.equ SPI_CR2_TXDMAEN_Pos, (1) 
.equ SPI_CR2_TXDMAEN_Msk, (0x1 << SPI_CR2_TXDMAEN_Pos) //!< 0x00000002 
.equ SPI_CR2_TXDMAEN, SPI_CR2_TXDMAEN_Msk //!< Tx Buffer DMA Enable 
.equ SPI_CR2_SSOE_Pos, (2) 
.equ SPI_CR2_SSOE_Msk, (0x1 << SPI_CR2_SSOE_Pos) //!< 0x00000004 
.equ SPI_CR2_SSOE, SPI_CR2_SSOE_Msk //!< SS Output Enable 
.equ SPI_CR2_NSSP_Pos, (3) 
.equ SPI_CR2_NSSP_Msk, (0x1 << SPI_CR2_NSSP_Pos) //!< 0x00000008 
.equ SPI_CR2_NSSP, SPI_CR2_NSSP_Msk //!< NSS pulse management Enable 
.equ SPI_CR2_FRF_Pos, (4) 
.equ SPI_CR2_FRF_Msk, (0x1 << SPI_CR2_FRF_Pos) //!< 0x00000010 
.equ SPI_CR2_FRF, SPI_CR2_FRF_Msk //!< Frame Format Enable 
.equ SPI_CR2_ERRIE_Pos, (5) 
.equ SPI_CR2_ERRIE_Msk, (0x1 << SPI_CR2_ERRIE_Pos) //!< 0x00000020 
.equ SPI_CR2_ERRIE, SPI_CR2_ERRIE_Msk //!< Error Interrupt Enable 
.equ SPI_CR2_RXNEIE_Pos, (6) 
.equ SPI_CR2_RXNEIE_Msk, (0x1 << SPI_CR2_RXNEIE_Pos) //!< 0x00000040 
.equ SPI_CR2_RXNEIE, SPI_CR2_RXNEIE_Msk //!< RX buffer Not Empty Interrupt Enable 
.equ SPI_CR2_TXEIE_Pos, (7) 
.equ SPI_CR2_TXEIE_Msk, (0x1 << SPI_CR2_TXEIE_Pos) //!< 0x00000080 
.equ SPI_CR2_TXEIE, SPI_CR2_TXEIE_Msk //!< Tx buffer Empty Interrupt Enable 
.equ SPI_CR2_DS_Pos, (8) 
.equ SPI_CR2_DS_Msk, (0xF << SPI_CR2_DS_Pos) //!< 0x00000F00 
.equ SPI_CR2_DS, SPI_CR2_DS_Msk //!< DS[3:0] Data Size 
.equ SPI_CR2_DS_0, (0x1 << SPI_CR2_DS_Pos) //!< 0x00000100 
.equ SPI_CR2_DS_1, (0x2 << SPI_CR2_DS_Pos) //!< 0x00000200 
.equ SPI_CR2_DS_2, (0x4 << SPI_CR2_DS_Pos) //!< 0x00000400 
.equ SPI_CR2_DS_3, (0x8 << SPI_CR2_DS_Pos) //!< 0x00000800 
.equ SPI_CR2_FRXTH_Pos, (12) 
.equ SPI_CR2_FRXTH_Msk, (0x1 << SPI_CR2_FRXTH_Pos) //!< 0x00001000 
.equ SPI_CR2_FRXTH, SPI_CR2_FRXTH_Msk //!< FIFO reception Threshold 
.equ SPI_CR2_LDMARX_Pos, (13) 
.equ SPI_CR2_LDMARX_Msk, (0x1 << SPI_CR2_LDMARX_Pos) //!< 0x00002000 
.equ SPI_CR2_LDMARX, SPI_CR2_LDMARX_Msk //!< Last DMA transfer for reception 
.equ SPI_CR2_LDMATX_Pos, (14) 
.equ SPI_CR2_LDMATX_Msk, (0x1 << SPI_CR2_LDMATX_Pos) //!< 0x00004000 
.equ SPI_CR2_LDMATX, SPI_CR2_LDMATX_Msk //!< Last DMA transfer for transmission 
//*******************  Bit definition for SPI_SR register  *******************
.equ SPI_SR_RXNE_Pos, (0) 
.equ SPI_SR_RXNE_Msk, (0x1 << SPI_SR_RXNE_Pos) //!< 0x00000001 
.equ SPI_SR_RXNE, SPI_SR_RXNE_Msk //!< Receive buffer Not Empty 
.equ SPI_SR_TXE_Pos, (1) 
.equ SPI_SR_TXE_Msk, (0x1 << SPI_SR_TXE_Pos) //!< 0x00000002 
.equ SPI_SR_TXE, SPI_SR_TXE_Msk //!< Transmit buffer Empty 
.equ SPI_SR_CHSIDE_Pos, (2) 
.equ SPI_SR_CHSIDE_Msk, (0x1 << SPI_SR_CHSIDE_Pos) //!< 0x00000004 
.equ SPI_SR_CHSIDE, SPI_SR_CHSIDE_Msk //!< Channel side 
.equ SPI_SR_UDR_Pos, (3) 
.equ SPI_SR_UDR_Msk, (0x1 << SPI_SR_UDR_Pos) //!< 0x00000008 
.equ SPI_SR_UDR, SPI_SR_UDR_Msk //!< Underrun flag 
.equ SPI_SR_CRCERR_Pos, (4) 
.equ SPI_SR_CRCERR_Msk, (0x1 << SPI_SR_CRCERR_Pos) //!< 0x00000010 
.equ SPI_SR_CRCERR, SPI_SR_CRCERR_Msk //!< CRC Error flag 
.equ SPI_SR_MODF_Pos, (5) 
.equ SPI_SR_MODF_Msk, (0x1 << SPI_SR_MODF_Pos) //!< 0x00000020 
.equ SPI_SR_MODF, SPI_SR_MODF_Msk //!< Mode fault 
.equ SPI_SR_OVR_Pos, (6) 
.equ SPI_SR_OVR_Msk, (0x1 << SPI_SR_OVR_Pos) //!< 0x00000040 
.equ SPI_SR_OVR, SPI_SR_OVR_Msk //!< Overrun flag 
.equ SPI_SR_BSY_Pos, (7) 
.equ SPI_SR_BSY_Msk, (0x1 << SPI_SR_BSY_Pos) //!< 0x00000080 
.equ SPI_SR_BSY, SPI_SR_BSY_Msk //!< Busy flag 
.equ SPI_SR_FRE_Pos, (8) 
.equ SPI_SR_FRE_Msk, (0x1 << SPI_SR_FRE_Pos) //!< 0x00000100 
.equ SPI_SR_FRE, SPI_SR_FRE_Msk //!< TI frame format error 
.equ SPI_SR_FRLVL_Pos, (9) 
.equ SPI_SR_FRLVL_Msk, (0x3 << SPI_SR_FRLVL_Pos) //!< 0x00000600 
.equ SPI_SR_FRLVL, SPI_SR_FRLVL_Msk //!< FIFO Reception Level 
.equ SPI_SR_FRLVL_0, (0x1 << SPI_SR_FRLVL_Pos) //!< 0x00000200 
.equ SPI_SR_FRLVL_1, (0x2 << SPI_SR_FRLVL_Pos) //!< 0x00000400 
.equ SPI_SR_FTLVL_Pos, (11) 
.equ SPI_SR_FTLVL_Msk, (0x3 << SPI_SR_FTLVL_Pos) //!< 0x00001800 
.equ SPI_SR_FTLVL, SPI_SR_FTLVL_Msk //!< FIFO Transmission Level 
.equ SPI_SR_FTLVL_0, (0x1 << SPI_SR_FTLVL_Pos) //!< 0x00000800 
.equ SPI_SR_FTLVL_1, (0x2 << SPI_SR_FTLVL_Pos) //!< 0x00001000 
//*******************  Bit definition for SPI_DR register  *******************
.equ SPI_DR_DR_Pos, (0) 
.equ SPI_DR_DR_Msk, (0xFFFF << SPI_DR_DR_Pos) //!< 0x0000FFFF 
.equ SPI_DR_DR, SPI_DR_DR_Msk //!< Data Register 
//******************  Bit definition for SPI_CRCPR register  *****************
.equ SPI_CRCPR_CRCPOLY_Pos, (0) 
.equ SPI_CRCPR_CRCPOLY_Msk, (0xFFFF << SPI_CRCPR_CRCPOLY_Pos) //!< 0x0000FFFF 
.equ SPI_CRCPR_CRCPOLY, SPI_CRCPR_CRCPOLY_Msk //!< CRC polynomial register 
//*****************  Bit definition for SPI_RXCRCR register  *****************
.equ SPI_RXCRCR_RXCRC_Pos, (0) 
.equ SPI_RXCRCR_RXCRC_Msk, (0xFFFF << SPI_RXCRCR_RXCRC_Pos) //!< 0x0000FFFF 
.equ SPI_RXCRCR_RXCRC, SPI_RXCRCR_RXCRC_Msk //!< Rx CRC Register 
//*****************  Bit definition for SPI_TXCRCR register  *****************
.equ SPI_TXCRCR_TXCRC_Pos, (0) 
.equ SPI_TXCRCR_TXCRC_Msk, (0xFFFF << SPI_TXCRCR_TXCRC_Pos) //!< 0x0000FFFF 
.equ SPI_TXCRCR_TXCRC, SPI_TXCRCR_TXCRC_Msk //!< Tx CRC Register 
//*****************  Bit definition for SPI_I2SCFGR register  ****************
.equ SPI_I2SCFGR_CHLEN_Pos, (0) 
.equ SPI_I2SCFGR_CHLEN_Msk, (0x1 << SPI_I2SCFGR_CHLEN_Pos) //!< 0x00000001 
.equ SPI_I2SCFGR_CHLEN, SPI_I2SCFGR_CHLEN_Msk //!<Channel length (number of bits per audio channel) 
.equ SPI_I2SCFGR_DATLEN_Pos, (1) 
.equ SPI_I2SCFGR_DATLEN_Msk, (0x3 << SPI_I2SCFGR_DATLEN_Pos) //!< 0x00000006 
.equ SPI_I2SCFGR_DATLEN, SPI_I2SCFGR_DATLEN_Msk //!<DATLEN[1:0] bits (Data length to be transferred) 
.equ SPI_I2SCFGR_DATLEN_0, (0x1 << SPI_I2SCFGR_DATLEN_Pos) //!< 0x00000002 
.equ SPI_I2SCFGR_DATLEN_1, (0x2 << SPI_I2SCFGR_DATLEN_Pos) //!< 0x00000004 
.equ SPI_I2SCFGR_CKPOL_Pos, (3) 
.equ SPI_I2SCFGR_CKPOL_Msk, (0x1 << SPI_I2SCFGR_CKPOL_Pos) //!< 0x00000008 
.equ SPI_I2SCFGR_CKPOL, SPI_I2SCFGR_CKPOL_Msk //!<steady state clock polarity 
.equ SPI_I2SCFGR_I2SSTD_Pos, (4) 
.equ SPI_I2SCFGR_I2SSTD_Msk, (0x3 << SPI_I2SCFGR_I2SSTD_Pos) //!< 0x00000030 
.equ SPI_I2SCFGR_I2SSTD, SPI_I2SCFGR_I2SSTD_Msk //!<I2SSTD[1:0] bits (I2S standard selection) 
.equ SPI_I2SCFGR_I2SSTD_0, (0x1 << SPI_I2SCFGR_I2SSTD_Pos) //!< 0x00000010 
.equ SPI_I2SCFGR_I2SSTD_1, (0x2 << SPI_I2SCFGR_I2SSTD_Pos) //!< 0x00000020 
.equ SPI_I2SCFGR_PCMSYNC_Pos, (7) 
.equ SPI_I2SCFGR_PCMSYNC_Msk, (0x1 << SPI_I2SCFGR_PCMSYNC_Pos) //!< 0x00000080 
.equ SPI_I2SCFGR_PCMSYNC, SPI_I2SCFGR_PCMSYNC_Msk //!<PCM frame synchronization 
.equ SPI_I2SCFGR_I2SCFG_Pos, (8) 
.equ SPI_I2SCFGR_I2SCFG_Msk, (0x3 << SPI_I2SCFGR_I2SCFG_Pos) //!< 0x00000300 
.equ SPI_I2SCFGR_I2SCFG, SPI_I2SCFGR_I2SCFG_Msk //!<I2SCFG[1:0] bits (I2S configuration mode) 
.equ SPI_I2SCFGR_I2SCFG_0, (0x1 << SPI_I2SCFGR_I2SCFG_Pos) //!< 0x00000100 
.equ SPI_I2SCFGR_I2SCFG_1, (0x2 << SPI_I2SCFGR_I2SCFG_Pos) //!< 0x00000200 
.equ SPI_I2SCFGR_I2SE_Pos, (10) 
.equ SPI_I2SCFGR_I2SE_Msk, (0x1 << SPI_I2SCFGR_I2SE_Pos) //!< 0x00000400 
.equ SPI_I2SCFGR_I2SE, SPI_I2SCFGR_I2SE_Msk //!<I2S Enable 
.equ SPI_I2SCFGR_I2SMOD_Pos, (11) 
.equ SPI_I2SCFGR_I2SMOD_Msk, (0x1 << SPI_I2SCFGR_I2SMOD_Pos) //!< 0x00000800 
.equ SPI_I2SCFGR_I2SMOD, SPI_I2SCFGR_I2SMOD_Msk //!<I2S mode selection 
//*****************  Bit definition for SPI_I2SPR register  ******************
.equ SPI_I2SPR_I2SDIV_Pos, (0) 
.equ SPI_I2SPR_I2SDIV_Msk, (0xFF << SPI_I2SPR_I2SDIV_Pos) //!< 0x000000FF 
.equ SPI_I2SPR_I2SDIV, SPI_I2SPR_I2SDIV_Msk //!<I2S Linear prescaler 
.equ SPI_I2SPR_ODD_Pos, (8) 
.equ SPI_I2SPR_ODD_Msk, (0x1 << SPI_I2SPR_ODD_Pos) //!< 0x00000100 
.equ SPI_I2SPR_ODD, SPI_I2SPR_ODD_Msk //!<Odd factor for the prescaler 
.equ SPI_I2SPR_MCKOE_Pos, (9) 
.equ SPI_I2SPR_MCKOE_Msk, (0x1 << SPI_I2SPR_MCKOE_Pos) //!< 0x00000200 
.equ SPI_I2SPR_MCKOE, SPI_I2SPR_MCKOE_Msk //!<Master Clock Output Enable 
//****************************************************************************
//
//                        System Configuration(SYSCFG)
//
//****************************************************************************
//****************  Bit definition for SYSCFG_CFGR1 register  ***************
.equ SYSCFG_CFGR1_MEM_MODE_Pos, (0) 
.equ SYSCFG_CFGR1_MEM_MODE_Msk, (0x7 << SYSCFG_CFGR1_MEM_MODE_Pos) //!< 0x00000007 
.equ SYSCFG_CFGR1_MEM_MODE, SYSCFG_CFGR1_MEM_MODE_Msk //!< SYSCFG_Memory Remap Config 
.equ SYSCFG_CFGR1_MEM_MODE_0, (0x00000001) //!< Bit 0 
.equ SYSCFG_CFGR1_MEM_MODE_1, (0x00000002) //!< Bit 1 
.equ SYSCFG_CFGR1_MEM_MODE_2, (0x00000004) //!< Bit 2 
.equ SYSCFG_CFGR1_USB_IT_RMP_Pos, (5) 
.equ SYSCFG_CFGR1_USB_IT_RMP_Msk, (0x1 << SYSCFG_CFGR1_USB_IT_RMP_Pos) //!< 0x00000020 
.equ SYSCFG_CFGR1_USB_IT_RMP, SYSCFG_CFGR1_USB_IT_RMP_Msk //!< USB interrupt remap 
.equ SYSCFG_CFGR1_TIM1_ITR3_RMP_Pos, (6) 
.equ SYSCFG_CFGR1_TIM1_ITR3_RMP_Msk, (0x1 << SYSCFG_CFGR1_TIM1_ITR3_RMP_Pos) //!< 0x00000040 
.equ SYSCFG_CFGR1_TIM1_ITR3_RMP, SYSCFG_CFGR1_TIM1_ITR3_RMP_Msk //!< Timer 1 ITR3 selection 
.equ SYSCFG_CFGR1_DAC1_TRIG1_RMP_Pos, (7) 
.equ SYSCFG_CFGR1_DAC1_TRIG1_RMP_Msk, (0x1 << SYSCFG_CFGR1_DAC1_TRIG1_RMP_Pos) //!< 0x00000080 
.equ SYSCFG_CFGR1_DAC1_TRIG1_RMP, SYSCFG_CFGR1_DAC1_TRIG1_RMP_Msk //!< DAC1 Trigger1 remap 
.equ SYSCFG_CFGR1_DMA_RMP_Pos, (8) 
.equ SYSCFG_CFGR1_DMA_RMP_Msk, (0x79 << SYSCFG_CFGR1_DMA_RMP_Pos) //!< 0x00007900 
.equ SYSCFG_CFGR1_DMA_RMP, SYSCFG_CFGR1_DMA_RMP_Msk //!< DMA remap mask 
.equ SYSCFG_CFGR1_ADC24_DMA_RMP_Pos, (8) 
.equ SYSCFG_CFGR1_ADC24_DMA_RMP_Msk, (0x1 << SYSCFG_CFGR1_ADC24_DMA_RMP_Pos) //!< 0x00000100 
.equ SYSCFG_CFGR1_ADC24_DMA_RMP, SYSCFG_CFGR1_ADC24_DMA_RMP_Msk //!< ADC2 and ADC4 DMA remap 
.equ SYSCFG_CFGR1_TIM16_DMA_RMP_Pos, (11) 
.equ SYSCFG_CFGR1_TIM16_DMA_RMP_Msk, (0x1 << SYSCFG_CFGR1_TIM16_DMA_RMP_Pos) //!< 0x00000800 
.equ SYSCFG_CFGR1_TIM16_DMA_RMP, SYSCFG_CFGR1_TIM16_DMA_RMP_Msk //!< Timer 16 DMA remap 
.equ SYSCFG_CFGR1_TIM17_DMA_RMP_Pos, (12) 
.equ SYSCFG_CFGR1_TIM17_DMA_RMP_Msk, (0x1 << SYSCFG_CFGR1_TIM17_DMA_RMP_Pos) //!< 0x00001000 
.equ SYSCFG_CFGR1_TIM17_DMA_RMP, SYSCFG_CFGR1_TIM17_DMA_RMP_Msk //!< Timer 17 DMA remap 
.equ SYSCFG_CFGR1_TIM6DAC1Ch1_DMA_RMP_Pos, (13) 
.equ SYSCFG_CFGR1_TIM6DAC1Ch1_DMA_RMP_Msk, (0x1 << SYSCFG_CFGR1_TIM6DAC1Ch1_DMA_RMP_Pos) //!< 0x00002000 
.equ SYSCFG_CFGR1_TIM6DAC1Ch1_DMA_RMP, SYSCFG_CFGR1_TIM6DAC1Ch1_DMA_RMP_Msk //!< Timer 6 / DAC1 Ch1 DMA remap 
.equ SYSCFG_CFGR1_TIM7DAC1Ch2_DMA_RMP_Pos, (14) 
.equ SYSCFG_CFGR1_TIM7DAC1Ch2_DMA_RMP_Msk, (0x1 << SYSCFG_CFGR1_TIM7DAC1Ch2_DMA_RMP_Pos) //!< 0x00004000 
.equ SYSCFG_CFGR1_TIM7DAC1Ch2_DMA_RMP, SYSCFG_CFGR1_TIM7DAC1Ch2_DMA_RMP_Msk //!< Timer 7 / DAC1 Ch2 DMA remap 
.equ SYSCFG_CFGR1_I2C_PB6_FMP_Pos, (16) 
.equ SYSCFG_CFGR1_I2C_PB6_FMP_Msk, (0x1 << SYSCFG_CFGR1_I2C_PB6_FMP_Pos) //!< 0x00010000 
.equ SYSCFG_CFGR1_I2C_PB6_FMP, SYSCFG_CFGR1_I2C_PB6_FMP_Msk //!< I2C PB6 Fast mode plus 
.equ SYSCFG_CFGR1_I2C_PB7_FMP_Pos, (17) 
.equ SYSCFG_CFGR1_I2C_PB7_FMP_Msk, (0x1 << SYSCFG_CFGR1_I2C_PB7_FMP_Pos) //!< 0x00020000 
.equ SYSCFG_CFGR1_I2C_PB7_FMP, SYSCFG_CFGR1_I2C_PB7_FMP_Msk //!< I2C PB7 Fast mode plus 
.equ SYSCFG_CFGR1_I2C_PB8_FMP_Pos, (18) 
.equ SYSCFG_CFGR1_I2C_PB8_FMP_Msk, (0x1 << SYSCFG_CFGR1_I2C_PB8_FMP_Pos) //!< 0x00040000 
.equ SYSCFG_CFGR1_I2C_PB8_FMP, SYSCFG_CFGR1_I2C_PB8_FMP_Msk //!< I2C PB8 Fast mode plus 
.equ SYSCFG_CFGR1_I2C_PB9_FMP_Pos, (19) 
.equ SYSCFG_CFGR1_I2C_PB9_FMP_Msk, (0x1 << SYSCFG_CFGR1_I2C_PB9_FMP_Pos) //!< 0x00080000 
.equ SYSCFG_CFGR1_I2C_PB9_FMP, SYSCFG_CFGR1_I2C_PB9_FMP_Msk //!< I2C PB9 Fast mode plus 
.equ SYSCFG_CFGR1_I2C1_FMP_Pos, (20) 
.equ SYSCFG_CFGR1_I2C1_FMP_Msk, (0x1 << SYSCFG_CFGR1_I2C1_FMP_Pos) //!< 0x00100000 
.equ SYSCFG_CFGR1_I2C1_FMP, SYSCFG_CFGR1_I2C1_FMP_Msk //!< I2C1 Fast mode plus 
.equ SYSCFG_CFGR1_I2C2_FMP_Pos, (21) 
.equ SYSCFG_CFGR1_I2C2_FMP_Msk, (0x1 << SYSCFG_CFGR1_I2C2_FMP_Pos) //!< 0x00200000 
.equ SYSCFG_CFGR1_I2C2_FMP, SYSCFG_CFGR1_I2C2_FMP_Msk //!< I2C2 Fast mode plus 
.equ SYSCFG_CFGR1_ENCODER_MODE_Pos, (22) 
.equ SYSCFG_CFGR1_ENCODER_MODE_Msk, (0x3 << SYSCFG_CFGR1_ENCODER_MODE_Pos) //!< 0x00C00000 
.equ SYSCFG_CFGR1_ENCODER_MODE, SYSCFG_CFGR1_ENCODER_MODE_Msk //!< Encoder Mode 
.equ SYSCFG_CFGR1_ENCODER_MODE_0, (0x1 << SYSCFG_CFGR1_ENCODER_MODE_Pos) //!< 0x00400000 
.equ SYSCFG_CFGR1_ENCODER_MODE_1, (0x2 << SYSCFG_CFGR1_ENCODER_MODE_Pos) //!< 0x00800000 
.equ SYSCFG_CFGR1_ENCODER_MODE_TIM2_Pos, (22) 
.equ SYSCFG_CFGR1_ENCODER_MODE_TIM2_Msk, (0x1 << SYSCFG_CFGR1_ENCODER_MODE_TIM2_Pos) //!< 0x00400000 
.equ SYSCFG_CFGR1_ENCODER_MODE_TIM2, SYSCFG_CFGR1_ENCODER_MODE_TIM2_Msk //!< TIM2 IC1 and TIM2 IC2 are connected to TIM15 IC1 and TIM15 IC2 respectively 
.equ SYSCFG_CFGR1_ENCODER_MODE_TIM3_Pos, (23) 
.equ SYSCFG_CFGR1_ENCODER_MODE_TIM3_Msk, (0x1 << SYSCFG_CFGR1_ENCODER_MODE_TIM3_Pos) //!< 0x00800000 
.equ SYSCFG_CFGR1_ENCODER_MODE_TIM3, SYSCFG_CFGR1_ENCODER_MODE_TIM3_Msk //!< TIM3 IC1 and TIM3 IC2 are connected to TIM15 IC1 and TIM15 IC2 respectively 
.equ SYSCFG_CFGR1_I2C3_FMP_Pos, (24) 
.equ SYSCFG_CFGR1_I2C3_FMP_Msk, (0x1 << SYSCFG_CFGR1_I2C3_FMP_Pos) //!< 0x01000000 
.equ SYSCFG_CFGR1_I2C3_FMP, SYSCFG_CFGR1_I2C3_FMP_Msk //!< I2C3 Fast mode plus 
.equ SYSCFG_CFGR1_FPU_IE_Pos, (26) 
.equ SYSCFG_CFGR1_FPU_IE_Msk, (0x3F << SYSCFG_CFGR1_FPU_IE_Pos) //!< 0xFC000000 
.equ SYSCFG_CFGR1_FPU_IE, SYSCFG_CFGR1_FPU_IE_Msk //!< Floating Point Unit Interrupt Enable 
.equ SYSCFG_CFGR1_FPU_IE_0, (0x01 << SYSCFG_CFGR1_FPU_IE_Pos) //!< 0x04000000 
.equ SYSCFG_CFGR1_FPU_IE_1, (0x02 << SYSCFG_CFGR1_FPU_IE_Pos) //!< 0x08000000 
.equ SYSCFG_CFGR1_FPU_IE_2, (0x04 << SYSCFG_CFGR1_FPU_IE_Pos) //!< 0x10000000 
.equ SYSCFG_CFGR1_FPU_IE_3, (0x08 << SYSCFG_CFGR1_FPU_IE_Pos) //!< 0x20000000 
.equ SYSCFG_CFGR1_FPU_IE_4, (0x10 << SYSCFG_CFGR1_FPU_IE_Pos) //!< 0x40000000 
.equ SYSCFG_CFGR1_FPU_IE_5, (0x20 << SYSCFG_CFGR1_FPU_IE_Pos) //!< 0x80000000 
//****************  Bit definition for SYSCFG_RCR register  ******************
.equ SYSCFG_RCR_PAGE0_Pos, (0) 
.equ SYSCFG_RCR_PAGE0_Msk, (0x1 << SYSCFG_RCR_PAGE0_Pos) //!< 0x00000001 
.equ SYSCFG_RCR_PAGE0, SYSCFG_RCR_PAGE0_Msk //!< ICODE SRAM Write protection page 0 
.equ SYSCFG_RCR_PAGE1_Pos, (1) 
.equ SYSCFG_RCR_PAGE1_Msk, (0x1 << SYSCFG_RCR_PAGE1_Pos) //!< 0x00000002 
.equ SYSCFG_RCR_PAGE1, SYSCFG_RCR_PAGE1_Msk //!< ICODE SRAM Write protection page 1 
.equ SYSCFG_RCR_PAGE2_Pos, (2) 
.equ SYSCFG_RCR_PAGE2_Msk, (0x1 << SYSCFG_RCR_PAGE2_Pos) //!< 0x00000004 
.equ SYSCFG_RCR_PAGE2, SYSCFG_RCR_PAGE2_Msk //!< ICODE SRAM Write protection page 2 
.equ SYSCFG_RCR_PAGE3_Pos, (3) 
.equ SYSCFG_RCR_PAGE3_Msk, (0x1 << SYSCFG_RCR_PAGE3_Pos) //!< 0x00000008 
.equ SYSCFG_RCR_PAGE3, SYSCFG_RCR_PAGE3_Msk //!< ICODE SRAM Write protection page 3 
.equ SYSCFG_RCR_PAGE4_Pos, (4) 
.equ SYSCFG_RCR_PAGE4_Msk, (0x1 << SYSCFG_RCR_PAGE4_Pos) //!< 0x00000010 
.equ SYSCFG_RCR_PAGE4, SYSCFG_RCR_PAGE4_Msk //!< ICODE SRAM Write protection page 4 
.equ SYSCFG_RCR_PAGE5_Pos, (5) 
.equ SYSCFG_RCR_PAGE5_Msk, (0x1 << SYSCFG_RCR_PAGE5_Pos) //!< 0x00000020 
.equ SYSCFG_RCR_PAGE5, SYSCFG_RCR_PAGE5_Msk //!< ICODE SRAM Write protection page 5 
.equ SYSCFG_RCR_PAGE6_Pos, (6) 
.equ SYSCFG_RCR_PAGE6_Msk, (0x1 << SYSCFG_RCR_PAGE6_Pos) //!< 0x00000040 
.equ SYSCFG_RCR_PAGE6, SYSCFG_RCR_PAGE6_Msk //!< ICODE SRAM Write protection page 6 
.equ SYSCFG_RCR_PAGE7_Pos, (7) 
.equ SYSCFG_RCR_PAGE7_Msk, (0x1 << SYSCFG_RCR_PAGE7_Pos) //!< 0x00000080 
.equ SYSCFG_RCR_PAGE7, SYSCFG_RCR_PAGE7_Msk //!< ICODE SRAM Write protection page 7 
.equ SYSCFG_RCR_PAGE8_Pos, (8) 
.equ SYSCFG_RCR_PAGE8_Msk, (0x1 << SYSCFG_RCR_PAGE8_Pos) //!< 0x00000100 
.equ SYSCFG_RCR_PAGE8, SYSCFG_RCR_PAGE8_Msk //!< ICODE SRAM Write protection page 8 
.equ SYSCFG_RCR_PAGE9_Pos, (9) 
.equ SYSCFG_RCR_PAGE9_Msk, (0x1 << SYSCFG_RCR_PAGE9_Pos) //!< 0x00000200 
.equ SYSCFG_RCR_PAGE9, SYSCFG_RCR_PAGE9_Msk //!< ICODE SRAM Write protection page 9 
.equ SYSCFG_RCR_PAGE10_Pos, (10) 
.equ SYSCFG_RCR_PAGE10_Msk, (0x1 << SYSCFG_RCR_PAGE10_Pos) //!< 0x00000400 
.equ SYSCFG_RCR_PAGE10, SYSCFG_RCR_PAGE10_Msk //!< ICODE SRAM Write protection page 10 
.equ SYSCFG_RCR_PAGE11_Pos, (11) 
.equ SYSCFG_RCR_PAGE11_Msk, (0x1 << SYSCFG_RCR_PAGE11_Pos) //!< 0x00000800 
.equ SYSCFG_RCR_PAGE11, SYSCFG_RCR_PAGE11_Msk //!< ICODE SRAM Write protection page 11 
.equ SYSCFG_RCR_PAGE12_Pos, (12) 
.equ SYSCFG_RCR_PAGE12_Msk, (0x1 << SYSCFG_RCR_PAGE12_Pos) //!< 0x00001000 
.equ SYSCFG_RCR_PAGE12, SYSCFG_RCR_PAGE12_Msk //!< ICODE SRAM Write protection page 12 
.equ SYSCFG_RCR_PAGE13_Pos, (13) 
.equ SYSCFG_RCR_PAGE13_Msk, (0x1 << SYSCFG_RCR_PAGE13_Pos) //!< 0x00002000 
.equ SYSCFG_RCR_PAGE13, SYSCFG_RCR_PAGE13_Msk //!< ICODE SRAM Write protection page 13 
.equ SYSCFG_RCR_PAGE14_Pos, (14) 
.equ SYSCFG_RCR_PAGE14_Msk, (0x1 << SYSCFG_RCR_PAGE14_Pos) //!< 0x00004000 
.equ SYSCFG_RCR_PAGE14, SYSCFG_RCR_PAGE14_Msk //!< ICODE SRAM Write protection page 14 
.equ SYSCFG_RCR_PAGE15_Pos, (15) 
.equ SYSCFG_RCR_PAGE15_Msk, (0x1 << SYSCFG_RCR_PAGE15_Pos) //!< 0x00008000 
.equ SYSCFG_RCR_PAGE15, SYSCFG_RCR_PAGE15_Msk //!< ICODE SRAM Write protection page 15 
//****************  Bit definition for SYSCFG_EXTICR1 register  **************
.equ SYSCFG_EXTICR1_EXTI0_Pos, (0) 
.equ SYSCFG_EXTICR1_EXTI0_Msk, (0xF << SYSCFG_EXTICR1_EXTI0_Pos) //!< 0x0000000F 
.equ SYSCFG_EXTICR1_EXTI0, SYSCFG_EXTICR1_EXTI0_Msk //!< EXTI 0 configuration 
.equ SYSCFG_EXTICR1_EXTI1_Pos, (4) 
.equ SYSCFG_EXTICR1_EXTI1_Msk, (0xF << SYSCFG_EXTICR1_EXTI1_Pos) //!< 0x000000F0 
.equ SYSCFG_EXTICR1_EXTI1, SYSCFG_EXTICR1_EXTI1_Msk //!< EXTI 1 configuration 
.equ SYSCFG_EXTICR1_EXTI2_Pos, (8) 
.equ SYSCFG_EXTICR1_EXTI2_Msk, (0xF << SYSCFG_EXTICR1_EXTI2_Pos) //!< 0x00000F00 
.equ SYSCFG_EXTICR1_EXTI2, SYSCFG_EXTICR1_EXTI2_Msk //!< EXTI 2 configuration 
.equ SYSCFG_EXTICR1_EXTI3_Pos, (12) 
.equ SYSCFG_EXTICR1_EXTI3_Msk, (0xF << SYSCFG_EXTICR1_EXTI3_Pos) //!< 0x0000F000 
.equ SYSCFG_EXTICR1_EXTI3, SYSCFG_EXTICR1_EXTI3_Msk //!< EXTI 3 configuration 
//!<  EXTI0 configuration
.equ SYSCFG_EXTICR1_EXTI0_PA, (0x00000000) //!< PA[0] pin 
.equ SYSCFG_EXTICR1_EXTI0_PB, (0x00000001) //!< PB[0] pin 
.equ SYSCFG_EXTICR1_EXTI0_PC, (0x00000002) //!< PC[0] pin 
.equ SYSCFG_EXTICR1_EXTI0_PD, (0x00000003) //!< PD[0] pin 
.equ SYSCFG_EXTICR1_EXTI0_PE, (0x00000004) //!< PE[0] pin 
.equ SYSCFG_EXTICR1_EXTI0_PF, (0x00000005) //!< PF[0] pin 
.equ SYSCFG_EXTICR1_EXTI0_PG, (0x00000006) //!< PG[0] pin 
.equ SYSCFG_EXTICR1_EXTI0_PH, (0x00000007) //!< PH[0] pin 
//!<  EXTI1 configuration
.equ SYSCFG_EXTICR1_EXTI1_PA, (0x00000000) //!< PA[1] pin 
.equ SYSCFG_EXTICR1_EXTI1_PB, (0x00000010) //!< PB[1] pin 
.equ SYSCFG_EXTICR1_EXTI1_PC, (0x00000020) //!< PC[1] pin 
.equ SYSCFG_EXTICR1_EXTI1_PD, (0x00000030) //!< PD[1] pin 
.equ SYSCFG_EXTICR1_EXTI1_PE, (0x00000040) //!< PE[1] pin 
.equ SYSCFG_EXTICR1_EXTI1_PF, (0x00000050) //!< PF[1] pin 
.equ SYSCFG_EXTICR1_EXTI1_PG, (0x00000060) //!< PG[1] pin 
.equ SYSCFG_EXTICR1_EXTI1_PH, (0x00000070) //!< PH[1] pin 
//!<  EXTI2 configuration
.equ SYSCFG_EXTICR1_EXTI2_PA, (0x00000000) //!< PA[2] pin 
.equ SYSCFG_EXTICR1_EXTI2_PB, (0x00000100) //!< PB[2] pin 
.equ SYSCFG_EXTICR1_EXTI2_PC, (0x00000200) //!< PC[2] pin 
.equ SYSCFG_EXTICR1_EXTI2_PD, (0x00000300) //!< PD[2] pin 
.equ SYSCFG_EXTICR1_EXTI2_PE, (0x00000400) //!< PE[2] pin 
.equ SYSCFG_EXTICR1_EXTI2_PF, (0x00000500) //!< PF[2] pin 
.equ SYSCFG_EXTICR1_EXTI2_PG, (0x00000600) //!< PG[2] pin 
//!<  EXTI3 configuration
.equ SYSCFG_EXTICR1_EXTI3_PA, (0x00000000) //!< PA[3] pin 
.equ SYSCFG_EXTICR1_EXTI3_PB, (0x00001000) //!< PB[3] pin 
.equ SYSCFG_EXTICR1_EXTI3_PC, (0x00002000) //!< PC[3] pin 
.equ SYSCFG_EXTICR1_EXTI3_PD, (0x00003000) //!< PD[3] pin 
.equ SYSCFG_EXTICR1_EXTI3_PE, (0x00004000) //!< PE[3] pin 
.equ SYSCFG_EXTICR1_EXTI3_PF, (0x00005000) //!< PE[3] pin 
.equ SYSCFG_EXTICR1_EXTI3_PG, (0x00006000) //!< PG[3] pin 
//****************  Bit definition for SYSCFG_EXTICR2 register  **************
.equ SYSCFG_EXTICR2_EXTI4_Pos, (0) 
.equ SYSCFG_EXTICR2_EXTI4_Msk, (0xF << SYSCFG_EXTICR2_EXTI4_Pos) //!< 0x0000000F 
.equ SYSCFG_EXTICR2_EXTI4, SYSCFG_EXTICR2_EXTI4_Msk //!< EXTI 4 configuration 
.equ SYSCFG_EXTICR2_EXTI5_Pos, (4) 
.equ SYSCFG_EXTICR2_EXTI5_Msk, (0xF << SYSCFG_EXTICR2_EXTI5_Pos) //!< 0x000000F0 
.equ SYSCFG_EXTICR2_EXTI5, SYSCFG_EXTICR2_EXTI5_Msk //!< EXTI 5 configuration 
.equ SYSCFG_EXTICR2_EXTI6_Pos, (8) 
.equ SYSCFG_EXTICR2_EXTI6_Msk, (0xF << SYSCFG_EXTICR2_EXTI6_Pos) //!< 0x00000F00 
.equ SYSCFG_EXTICR2_EXTI6, SYSCFG_EXTICR2_EXTI6_Msk //!< EXTI 6 configuration 
.equ SYSCFG_EXTICR2_EXTI7_Pos, (12) 
.equ SYSCFG_EXTICR2_EXTI7_Msk, (0xF << SYSCFG_EXTICR2_EXTI7_Pos) //!< 0x0000F000 
.equ SYSCFG_EXTICR2_EXTI7, SYSCFG_EXTICR2_EXTI7_Msk //!< EXTI 7 configuration 
//!<  EXTI4 configuration
.equ SYSCFG_EXTICR2_EXTI4_PA, (0x00000000) //!< PA[4] pin 
.equ SYSCFG_EXTICR2_EXTI4_PB, (0x00000001) //!< PB[4] pin 
.equ SYSCFG_EXTICR2_EXTI4_PC, (0x00000002) //!< PC[4] pin 
.equ SYSCFG_EXTICR2_EXTI4_PD, (0x00000003) //!< PD[4] pin 
.equ SYSCFG_EXTICR2_EXTI4_PE, (0x00000004) //!< PE[4] pin 
.equ SYSCFG_EXTICR2_EXTI4_PF, (0x00000005) //!< PF[4] pin 
.equ SYSCFG_EXTICR2_EXTI4_PG, (0x00000006) //!< PG[4] pin 
.equ SYSCFG_EXTICR2_EXTI4_PH, (0x00000007) //!< PH[4] pin 
//!<  EXTI5 configuration
.equ SYSCFG_EXTICR2_EXTI5_PA, (0x00000000) //!< PA[5] pin 
.equ SYSCFG_EXTICR2_EXTI5_PB, (0x00000010) //!< PB[5] pin 
.equ SYSCFG_EXTICR2_EXTI5_PC, (0x00000020) //!< PC[5] pin 
.equ SYSCFG_EXTICR2_EXTI5_PD, (0x00000030) //!< PD[5] pin 
.equ SYSCFG_EXTICR2_EXTI5_PE, (0x00000040) //!< PE[5] pin 
.equ SYSCFG_EXTICR2_EXTI5_PF, (0x00000050) //!< PF[5] pin 
.equ SYSCFG_EXTICR2_EXTI5_PG, (0x00000060) //!< PG[5] pin 
//!<  EXTI6 configuration
.equ SYSCFG_EXTICR2_EXTI6_PA, (0x00000000) //!< PA[6] pin 
.equ SYSCFG_EXTICR2_EXTI6_PB, (0x00000100) //!< PB[6] pin 
.equ SYSCFG_EXTICR2_EXTI6_PC, (0x00000200) //!< PC[6] pin 
.equ SYSCFG_EXTICR2_EXTI6_PD, (0x00000300) //!< PD[6] pin 
.equ SYSCFG_EXTICR2_EXTI6_PE, (0x00000400) //!< PE[6] pin 
.equ SYSCFG_EXTICR2_EXTI6_PF, (0x00000500) //!< PF[6] pin 
.equ SYSCFG_EXTICR2_EXTI6_PG, (0x00000600) //!< PG[6] pin 
//!<  EXTI7 configuration
.equ SYSCFG_EXTICR2_EXTI7_PA, (0x00000000) //!< PA[7] pin 
.equ SYSCFG_EXTICR2_EXTI7_PB, (0x00001000) //!< PB[7] pin 
.equ SYSCFG_EXTICR2_EXTI7_PC, (0x00002000) //!< PC[7] pin 
.equ SYSCFG_EXTICR2_EXTI7_PD, (0x00003000) //!< PD[7] pin 
.equ SYSCFG_EXTICR2_EXTI7_PE, (0x00004000) //!< PE[7] pin 
.equ SYSCFG_EXTICR2_EXTI7_PF, (0x00005000) //!< PF[7] pin 
.equ SYSCFG_EXTICR2_EXTI7_PG, (0x00006000) //!< PG[7] pin 
//****************  Bit definition for SYSCFG_EXTICR3 register  **************
.equ SYSCFG_EXTICR3_EXTI8_Pos, (0) 
.equ SYSCFG_EXTICR3_EXTI8_Msk, (0xF << SYSCFG_EXTICR3_EXTI8_Pos) //!< 0x0000000F 
.equ SYSCFG_EXTICR3_EXTI8, SYSCFG_EXTICR3_EXTI8_Msk //!< EXTI 8 configuration 
.equ SYSCFG_EXTICR3_EXTI9_Pos, (4) 
.equ SYSCFG_EXTICR3_EXTI9_Msk, (0xF << SYSCFG_EXTICR3_EXTI9_Pos) //!< 0x000000F0 
.equ SYSCFG_EXTICR3_EXTI9, SYSCFG_EXTICR3_EXTI9_Msk //!< EXTI 9 configuration 
.equ SYSCFG_EXTICR3_EXTI10_Pos, (8) 
.equ SYSCFG_EXTICR3_EXTI10_Msk, (0xF << SYSCFG_EXTICR3_EXTI10_Pos) //!< 0x00000F00 
.equ SYSCFG_EXTICR3_EXTI10, SYSCFG_EXTICR3_EXTI10_Msk //!< EXTI 10 configuration 
.equ SYSCFG_EXTICR3_EXTI11_Pos, (12) 
.equ SYSCFG_EXTICR3_EXTI11_Msk, (0xF << SYSCFG_EXTICR3_EXTI11_Pos) //!< 0x0000F000 
.equ SYSCFG_EXTICR3_EXTI11, SYSCFG_EXTICR3_EXTI11_Msk //!< EXTI 11 configuration 
//!<  EXTI8 configuration
.equ SYSCFG_EXTICR3_EXTI8_PA, (0x00000000) //!< PA[8] pin 
.equ SYSCFG_EXTICR3_EXTI8_PB, (0x00000001) //!< PB[8] pin 
.equ SYSCFG_EXTICR3_EXTI8_PC, (0x00000002) //!< PC[8] pin 
.equ SYSCFG_EXTICR3_EXTI8_PD, (0x00000003) //!< PD[8] pin 
.equ SYSCFG_EXTICR3_EXTI8_PE, (0x00000004) //!< PE[8] pin 
.equ SYSCFG_EXTICR3_EXTI8_PF, (0x00000005) //!< PF[8] pin 
.equ SYSCFG_EXTICR3_EXTI8_PG, (0x00000006) //!< PG[8] pin 
//!<  EXTI9 configuration
.equ SYSCFG_EXTICR3_EXTI9_PA, (0x00000000) //!< PA[9] pin 
.equ SYSCFG_EXTICR3_EXTI9_PB, (0x00000010) //!< PB[9] pin 
.equ SYSCFG_EXTICR3_EXTI9_PC, (0x00000020) //!< PC[9] pin 
.equ SYSCFG_EXTICR3_EXTI9_PD, (0x00000030) //!< PD[9] pin 
.equ SYSCFG_EXTICR3_EXTI9_PE, (0x00000040) //!< PE[9] pin 
.equ SYSCFG_EXTICR3_EXTI9_PF, (0x00000050) //!< PF[9] pin 
.equ SYSCFG_EXTICR3_EXTI9_PG, (0x00000060) //!< PG[9] pin 
//!<  EXTI10 configuration
.equ SYSCFG_EXTICR3_EXTI10_PA, (0x00000000) //!< PA[10] pin 
.equ SYSCFG_EXTICR3_EXTI10_PB, (0x00000100) //!< PB[10] pin 
.equ SYSCFG_EXTICR3_EXTI10_PC, (0x00000200) //!< PC[10] pin 
.equ SYSCFG_EXTICR3_EXTI10_PD, (0x00000300) //!< PD[10] pin 
.equ SYSCFG_EXTICR3_EXTI10_PE, (0x00000400) //!< PE[10] pin 
.equ SYSCFG_EXTICR3_EXTI10_PF, (0x00000500) //!< PF[10] pin 
.equ SYSCFG_EXTICR3_EXTI10_PG, (0x00000600) //!< PG[10] pin 
//!<  EXTI11 configuration
.equ SYSCFG_EXTICR3_EXTI11_PA, (0x00000000) //!< PA[11] pin 
.equ SYSCFG_EXTICR3_EXTI11_PB, (0x00001000) //!< PB[11] pin 
.equ SYSCFG_EXTICR3_EXTI11_PC, (0x00002000) //!< PC[11] pin 
.equ SYSCFG_EXTICR3_EXTI11_PD, (0x00003000) //!< PD[11] pin 
.equ SYSCFG_EXTICR3_EXTI11_PE, (0x00004000) //!< PE[11] pin 
.equ SYSCFG_EXTICR3_EXTI11_PF, (0x00005000) //!< PF[11] pin 
.equ SYSCFG_EXTICR3_EXTI11_PG, (0x00006000) //!< PG[11] pin 
//****************  Bit definition for SYSCFG_EXTICR4 register  ****************
.equ SYSCFG_EXTICR4_EXTI12_Pos, (0) 
.equ SYSCFG_EXTICR4_EXTI12_Msk, (0xF << SYSCFG_EXTICR4_EXTI12_Pos) //!< 0x0000000F 
.equ SYSCFG_EXTICR4_EXTI12, SYSCFG_EXTICR4_EXTI12_Msk //!< EXTI 12 configuration 
.equ SYSCFG_EXTICR4_EXTI13_Pos, (4) 
.equ SYSCFG_EXTICR4_EXTI13_Msk, (0xF << SYSCFG_EXTICR4_EXTI13_Pos) //!< 0x000000F0 
.equ SYSCFG_EXTICR4_EXTI13, SYSCFG_EXTICR4_EXTI13_Msk //!< EXTI 13 configuration 
.equ SYSCFG_EXTICR4_EXTI14_Pos, (8) 
.equ SYSCFG_EXTICR4_EXTI14_Msk, (0xF << SYSCFG_EXTICR4_EXTI14_Pos) //!< 0x00000F00 
.equ SYSCFG_EXTICR4_EXTI14, SYSCFG_EXTICR4_EXTI14_Msk //!< EXTI 14 configuration 
.equ SYSCFG_EXTICR4_EXTI15_Pos, (12) 
.equ SYSCFG_EXTICR4_EXTI15_Msk, (0xF << SYSCFG_EXTICR4_EXTI15_Pos) //!< 0x0000F000 
.equ SYSCFG_EXTICR4_EXTI15, SYSCFG_EXTICR4_EXTI15_Msk //!< EXTI 15 configuration 
//!<  EXTI12 configuration
.equ SYSCFG_EXTICR4_EXTI12_PA, (0x00000000) //!< PA[12] pin 
.equ SYSCFG_EXTICR4_EXTI12_PB, (0x00000001) //!< PB[12] pin 
.equ SYSCFG_EXTICR4_EXTI12_PC, (0x00000002) //!< PC[12] pin 
.equ SYSCFG_EXTICR4_EXTI12_PD, (0x00000003) //!< PD[12] pin 
.equ SYSCFG_EXTICR4_EXTI12_PE, (0x00000004) //!< PE[12] pin 
.equ SYSCFG_EXTICR4_EXTI12_PF, (0x00000005) //!< PF[12] pin 
.equ SYSCFG_EXTICR4_EXTI12_PG, (0x00000006) //!< PG[12] pin 
//!<  EXTI13 configuration
.equ SYSCFG_EXTICR4_EXTI13_PA, (0x00000000) //!< PA[13] pin 
.equ SYSCFG_EXTICR4_EXTI13_PB, (0x00000010) //!< PB[13] pin 
.equ SYSCFG_EXTICR4_EXTI13_PC, (0x00000020) //!< PC[13] pin 
.equ SYSCFG_EXTICR4_EXTI13_PD, (0x00000030) //!< PD[13] pin 
.equ SYSCFG_EXTICR4_EXTI13_PE, (0x00000040) //!< PE[13] pin 
.equ SYSCFG_EXTICR4_EXTI13_PF, (0x00000050) //!< PF[13] pin 
.equ SYSCFG_EXTICR4_EXTI13_PG, (0x00000060) //!< PG[13] pin 
//!<  EXTI14 configuration
.equ SYSCFG_EXTICR4_EXTI14_PA, (0x00000000) //!< PA[14] pin 
.equ SYSCFG_EXTICR4_EXTI14_PB, (0x00000100) //!< PB[14] pin 
.equ SYSCFG_EXTICR4_EXTI14_PC, (0x00000200) //!< PC[14] pin 
.equ SYSCFG_EXTICR4_EXTI14_PD, (0x00000300) //!< PD[14] pin 
.equ SYSCFG_EXTICR4_EXTI14_PE, (0x00000400) //!< PE[14] pin 
.equ SYSCFG_EXTICR4_EXTI14_PF, (0x00000500) //!< PF[14] pin 
.equ SYSCFG_EXTICR4_EXTI14_PG, (0x00000600) //!< PG[14] pin 
//!<  EXTI15 configuration
.equ SYSCFG_EXTICR4_EXTI15_PA, (0x00000000) //!< PA[15] pin 
.equ SYSCFG_EXTICR4_EXTI15_PB, (0x00001000) //!< PB[15] pin 
.equ SYSCFG_EXTICR4_EXTI15_PC, (0x00002000) //!< PC[15] pin 
.equ SYSCFG_EXTICR4_EXTI15_PD, (0x00003000) //!< PD[15] pin 
.equ SYSCFG_EXTICR4_EXTI15_PE, (0x00004000) //!< PE[15] pin 
.equ SYSCFG_EXTICR4_EXTI15_PF, (0x00005000) //!< PF[15] pin 
.equ SYSCFG_EXTICR4_EXTI15_PG, (0x00006000) //!< PG[15] pin 
//****************  Bit definition for SYSCFG_CFGR2 register  ***************
.equ SYSCFG_CFGR2_LOCKUP_LOCK_Pos, (0) 
.equ SYSCFG_CFGR2_LOCKUP_LOCK_Msk, (0x1 << SYSCFG_CFGR2_LOCKUP_LOCK_Pos) //!< 0x00000001 
.equ SYSCFG_CFGR2_LOCKUP_LOCK, SYSCFG_CFGR2_LOCKUP_LOCK_Msk //!< Enables and locks the LOCKUP (Hardfault) output of CortexM4 with Break Input of TIMx 
.equ SYSCFG_CFGR2_SRAM_PARITY_LOCK_Pos, (1) 
.equ SYSCFG_CFGR2_SRAM_PARITY_LOCK_Msk, (0x1 << SYSCFG_CFGR2_SRAM_PARITY_LOCK_Pos) //!< 0x00000002 
.equ SYSCFG_CFGR2_SRAM_PARITY_LOCK, SYSCFG_CFGR2_SRAM_PARITY_LOCK_Msk //!< Enables and locks the SRAM_PARITY error signal with Break Input of TIMx 
.equ SYSCFG_CFGR2_PVD_LOCK_Pos, (2) 
.equ SYSCFG_CFGR2_PVD_LOCK_Msk, (0x1 << SYSCFG_CFGR2_PVD_LOCK_Pos) //!< 0x00000004 
.equ SYSCFG_CFGR2_PVD_LOCK, SYSCFG_CFGR2_PVD_LOCK_Msk //!< Enables and locks the PVD connection with TIMx Break Input, as well as the PVDE and PLS[2:0] in the PWR_CR register 
.equ SYSCFG_CFGR2_BYP_ADDR_PAR_Pos, (4) 
.equ SYSCFG_CFGR2_BYP_ADDR_PAR_Msk, (0x1 << SYSCFG_CFGR2_BYP_ADDR_PAR_Pos) //!< 0x00000010 
.equ SYSCFG_CFGR2_BYP_ADDR_PAR, SYSCFG_CFGR2_BYP_ADDR_PAR_Msk //!< Disables the adddress parity check on RAM 
.equ SYSCFG_CFGR2_SRAM_PE_Pos, (8) 
.equ SYSCFG_CFGR2_SRAM_PE_Msk, (0x1 << SYSCFG_CFGR2_SRAM_PE_Pos) //!< 0x00000100 
.equ SYSCFG_CFGR2_SRAM_PE, SYSCFG_CFGR2_SRAM_PE_Msk //!< SRAM Parity error flag 
//****************  Bit definition for SYSCFG_CFGR4 register  ****************
.equ SYSCFG_CFGR4_ADC12_EXT2_RMP_Pos, (0) 
.equ SYSCFG_CFGR4_ADC12_EXT2_RMP_Msk, (0x1 << SYSCFG_CFGR4_ADC12_EXT2_RMP_Pos) //!< 0x00000001 
.equ SYSCFG_CFGR4_ADC12_EXT2_RMP, SYSCFG_CFGR4_ADC12_EXT2_RMP_Msk //!< ADC12 regular channel EXT2 remap 
.equ SYSCFG_CFGR4_ADC12_EXT3_RMP_Pos, (1) 
.equ SYSCFG_CFGR4_ADC12_EXT3_RMP_Msk, (0x1 << SYSCFG_CFGR4_ADC12_EXT3_RMP_Pos) //!< 0x00000002 
.equ SYSCFG_CFGR4_ADC12_EXT3_RMP, SYSCFG_CFGR4_ADC12_EXT3_RMP_Msk //!< ADC12 regular channel EXT3 remap 
.equ SYSCFG_CFGR4_ADC12_EXT5_RMP_Pos, (2) 
.equ SYSCFG_CFGR4_ADC12_EXT5_RMP_Msk, (0x1 << SYSCFG_CFGR4_ADC12_EXT5_RMP_Pos) //!< 0x00000004 
.equ SYSCFG_CFGR4_ADC12_EXT5_RMP, SYSCFG_CFGR4_ADC12_EXT5_RMP_Msk //!< ADC12 regular channel EXT5 remap 
.equ SYSCFG_CFGR4_ADC12_EXT13_RMP_Pos, (3) 
.equ SYSCFG_CFGR4_ADC12_EXT13_RMP_Msk, (0x1 << SYSCFG_CFGR4_ADC12_EXT13_RMP_Pos) //!< 0x00000008 
.equ SYSCFG_CFGR4_ADC12_EXT13_RMP, SYSCFG_CFGR4_ADC12_EXT13_RMP_Msk //!< ADC12 regular channel EXT13 remap 
.equ SYSCFG_CFGR4_ADC12_EXT15_RMP_Pos, (4) 
.equ SYSCFG_CFGR4_ADC12_EXT15_RMP_Msk, (0x1 << SYSCFG_CFGR4_ADC12_EXT15_RMP_Pos) //!< 0x00000010 
.equ SYSCFG_CFGR4_ADC12_EXT15_RMP, SYSCFG_CFGR4_ADC12_EXT15_RMP_Msk //!< ADC12 regular channel EXT15 remap 
.equ SYSCFG_CFGR4_ADC12_JEXT3_RMP_Pos, (5) 
.equ SYSCFG_CFGR4_ADC12_JEXT3_RMP_Msk, (0x1 << SYSCFG_CFGR4_ADC12_JEXT3_RMP_Pos) //!< 0x00000020 
.equ SYSCFG_CFGR4_ADC12_JEXT3_RMP, SYSCFG_CFGR4_ADC12_JEXT3_RMP_Msk //!< ADC12 injected channel JEXT3 remap 
.equ SYSCFG_CFGR4_ADC12_JEXT6_RMP_Pos, (6) 
.equ SYSCFG_CFGR4_ADC12_JEXT6_RMP_Msk, (0x1 << SYSCFG_CFGR4_ADC12_JEXT6_RMP_Pos) //!< 0x00000040 
.equ SYSCFG_CFGR4_ADC12_JEXT6_RMP, SYSCFG_CFGR4_ADC12_JEXT6_RMP_Msk //!< ADC12 injected channel JEXT6 remap 
.equ SYSCFG_CFGR4_ADC12_JEXT13_RMP_Pos, (7) 
.equ SYSCFG_CFGR4_ADC12_JEXT13_RMP_Msk, (0x1 << SYSCFG_CFGR4_ADC12_JEXT13_RMP_Pos) //!< 0x00000080 
.equ SYSCFG_CFGR4_ADC12_JEXT13_RMP, SYSCFG_CFGR4_ADC12_JEXT13_RMP_Msk //!< ADC12 injected channel JEXT13 remap 
.equ SYSCFG_CFGR4_ADC34_EXT5_RMP_Pos, (8) 
.equ SYSCFG_CFGR4_ADC34_EXT5_RMP_Msk, (0x1 << SYSCFG_CFGR4_ADC34_EXT5_RMP_Pos) //!< 0x00000100 
.equ SYSCFG_CFGR4_ADC34_EXT5_RMP, SYSCFG_CFGR4_ADC34_EXT5_RMP_Msk //!< ADC34 regular channel EXT5 remap 
.equ SYSCFG_CFGR4_ADC34_EXT6_RMP_Pos, (9) 
.equ SYSCFG_CFGR4_ADC34_EXT6_RMP_Msk, (0x1 << SYSCFG_CFGR4_ADC34_EXT6_RMP_Pos) //!< 0x00000200 
.equ SYSCFG_CFGR4_ADC34_EXT6_RMP, SYSCFG_CFGR4_ADC34_EXT6_RMP_Msk //!< ADC34 regular channel EXT6 remap 
.equ SYSCFG_CFGR4_ADC34_EXT15_RMP_Pos, (10) 
.equ SYSCFG_CFGR4_ADC34_EXT15_RMP_Msk, (0x1 << SYSCFG_CFGR4_ADC34_EXT15_RMP_Pos) //!< 0x00000400 
.equ SYSCFG_CFGR4_ADC34_EXT15_RMP, SYSCFG_CFGR4_ADC34_EXT15_RMP_Msk //!< ADC34 regular channel EXT15 remap 
.equ SYSCFG_CFGR4_ADC34_JEXT5_RMP_Pos, (11) 
.equ SYSCFG_CFGR4_ADC34_JEXT5_RMP_Msk, (0x1 << SYSCFG_CFGR4_ADC34_JEXT5_RMP_Pos) //!< 0x00000800 
.equ SYSCFG_CFGR4_ADC34_JEXT5_RMP, SYSCFG_CFGR4_ADC34_JEXT5_RMP_Msk //!< ADC34 injected channel JEXT5 remap 
.equ SYSCFG_CFGR4_ADC34_JEXT11_RMP_Pos, (12) 
.equ SYSCFG_CFGR4_ADC34_JEXT11_RMP_Msk, (0x1 << SYSCFG_CFGR4_ADC34_JEXT11_RMP_Pos) //!< 0x00001000 
.equ SYSCFG_CFGR4_ADC34_JEXT11_RMP, SYSCFG_CFGR4_ADC34_JEXT11_RMP_Msk //!< ADC34 injected channel JEXT11 remap 
.equ SYSCFG_CFGR4_ADC34_JEXT14_RMP_Pos, (13) 
.equ SYSCFG_CFGR4_ADC34_JEXT14_RMP_Msk, (0x1 << SYSCFG_CFGR4_ADC34_JEXT14_RMP_Pos) //!< 0x00002000 
.equ SYSCFG_CFGR4_ADC34_JEXT14_RMP, SYSCFG_CFGR4_ADC34_JEXT14_RMP_Msk //!< ADC34 injected channel JEXT14 remap 
//****************************************************************************
//
//                                    TIM
//
//****************************************************************************
//******************  Bit definition for TIM_CR1 register  *******************
.equ TIM_CR1_CEN_Pos, (0) 
.equ TIM_CR1_CEN_Msk, (0x1 << TIM_CR1_CEN_Pos) //!< 0x00000001 
.equ TIM_CR1_CEN, TIM_CR1_CEN_Msk //!<Counter enable 
.equ TIM_CR1_UDIS_Pos, (1) 
.equ TIM_CR1_UDIS_Msk, (0x1 << TIM_CR1_UDIS_Pos) //!< 0x00000002 
.equ TIM_CR1_UDIS, TIM_CR1_UDIS_Msk //!<Update disable 
.equ TIM_CR1_URS_Pos, (2) 
.equ TIM_CR1_URS_Msk, (0x1 << TIM_CR1_URS_Pos) //!< 0x00000004 
.equ TIM_CR1_URS, TIM_CR1_URS_Msk //!<Update request source 
.equ TIM_CR1_OPM_Pos, (3) 
.equ TIM_CR1_OPM_Msk, (0x1 << TIM_CR1_OPM_Pos) //!< 0x00000008 
.equ TIM_CR1_OPM, TIM_CR1_OPM_Msk //!<One pulse mode 
.equ TIM_CR1_DIR_Pos, (4) 
.equ TIM_CR1_DIR_Msk, (0x1 << TIM_CR1_DIR_Pos) //!< 0x00000010 
.equ TIM_CR1_DIR, TIM_CR1_DIR_Msk //!<Direction 
.equ TIM_CR1_CMS_Pos, (5) 
.equ TIM_CR1_CMS_Msk, (0x3 << TIM_CR1_CMS_Pos) //!< 0x00000060 
.equ TIM_CR1_CMS, TIM_CR1_CMS_Msk //!<CMS[1:0] bits (Center-aligned mode selection) 
.equ TIM_CR1_CMS_0, (0x1 << TIM_CR1_CMS_Pos) //!< 0x00000020 
.equ TIM_CR1_CMS_1, (0x2 << TIM_CR1_CMS_Pos) //!< 0x00000040 
.equ TIM_CR1_ARPE_Pos, (7) 
.equ TIM_CR1_ARPE_Msk, (0x1 << TIM_CR1_ARPE_Pos) //!< 0x00000080 
.equ TIM_CR1_ARPE, TIM_CR1_ARPE_Msk //!<Auto-reload preload enable 
.equ TIM_CR1_CKD_Pos, (8) 
.equ TIM_CR1_CKD_Msk, (0x3 << TIM_CR1_CKD_Pos) //!< 0x00000300 
.equ TIM_CR1_CKD, TIM_CR1_CKD_Msk //!<CKD[1:0] bits (clock division) 
.equ TIM_CR1_CKD_0, (0x1 << TIM_CR1_CKD_Pos) //!< 0x00000100 
.equ TIM_CR1_CKD_1, (0x2 << TIM_CR1_CKD_Pos) //!< 0x00000200 
.equ TIM_CR1_UIFREMAP_Pos, (11) 
.equ TIM_CR1_UIFREMAP_Msk, (0x1 << TIM_CR1_UIFREMAP_Pos) //!< 0x00000800 
.equ TIM_CR1_UIFREMAP, TIM_CR1_UIFREMAP_Msk //!<Update interrupt flag remap 
//******************  Bit definition for TIM_CR2 register  *******************
.equ TIM_CR2_CCPC_Pos, (0) 
.equ TIM_CR2_CCPC_Msk, (0x1 << TIM_CR2_CCPC_Pos) //!< 0x00000001 
.equ TIM_CR2_CCPC, TIM_CR2_CCPC_Msk //!<Capture/Compare Preloaded Control 
.equ TIM_CR2_CCUS_Pos, (2) 
.equ TIM_CR2_CCUS_Msk, (0x1 << TIM_CR2_CCUS_Pos) //!< 0x00000004 
.equ TIM_CR2_CCUS, TIM_CR2_CCUS_Msk //!<Capture/Compare Control Update Selection 
.equ TIM_CR2_CCDS_Pos, (3) 
.equ TIM_CR2_CCDS_Msk, (0x1 << TIM_CR2_CCDS_Pos) //!< 0x00000008 
.equ TIM_CR2_CCDS, TIM_CR2_CCDS_Msk //!<Capture/Compare DMA Selection 
.equ TIM_CR2_MMS_Pos, (4) 
.equ TIM_CR2_MMS_Msk, (0x7 << TIM_CR2_MMS_Pos) //!< 0x00000070 
.equ TIM_CR2_MMS, TIM_CR2_MMS_Msk //!<MMS[2:0] bits (Master Mode Selection) 
.equ TIM_CR2_MMS_0, (0x1 << TIM_CR2_MMS_Pos) //!< 0x00000010 
.equ TIM_CR2_MMS_1, (0x2 << TIM_CR2_MMS_Pos) //!< 0x00000020 
.equ TIM_CR2_MMS_2, (0x4 << TIM_CR2_MMS_Pos) //!< 0x00000040 
.equ TIM_CR2_TI1S_Pos, (7) 
.equ TIM_CR2_TI1S_Msk, (0x1 << TIM_CR2_TI1S_Pos) //!< 0x00000080 
.equ TIM_CR2_TI1S, TIM_CR2_TI1S_Msk //!<TI1 Selection 
.equ TIM_CR2_OIS1_Pos, (8) 
.equ TIM_CR2_OIS1_Msk, (0x1 << TIM_CR2_OIS1_Pos) //!< 0x00000100 
.equ TIM_CR2_OIS1, TIM_CR2_OIS1_Msk //!<Output Idle state 1 (OC1 output) 
.equ TIM_CR2_OIS1N_Pos, (9) 
.equ TIM_CR2_OIS1N_Msk, (0x1 << TIM_CR2_OIS1N_Pos) //!< 0x00000200 
.equ TIM_CR2_OIS1N, TIM_CR2_OIS1N_Msk //!<Output Idle state 1 (OC1N output) 
.equ TIM_CR2_OIS2_Pos, (10) 
.equ TIM_CR2_OIS2_Msk, (0x1 << TIM_CR2_OIS2_Pos) //!< 0x00000400 
.equ TIM_CR2_OIS2, TIM_CR2_OIS2_Msk //!<Output Idle state 2 (OC2 output) 
.equ TIM_CR2_OIS2N_Pos, (11) 
.equ TIM_CR2_OIS2N_Msk, (0x1 << TIM_CR2_OIS2N_Pos) //!< 0x00000800 
.equ TIM_CR2_OIS2N, TIM_CR2_OIS2N_Msk //!<Output Idle state 2 (OC2N output) 
.equ TIM_CR2_OIS3_Pos, (12) 
.equ TIM_CR2_OIS3_Msk, (0x1 << TIM_CR2_OIS3_Pos) //!< 0x00001000 
.equ TIM_CR2_OIS3, TIM_CR2_OIS3_Msk //!<Output Idle state 3 (OC3 output) 
.equ TIM_CR2_OIS3N_Pos, (13) 
.equ TIM_CR2_OIS3N_Msk, (0x1 << TIM_CR2_OIS3N_Pos) //!< 0x00002000 
.equ TIM_CR2_OIS3N, TIM_CR2_OIS3N_Msk //!<Output Idle state 3 (OC3N output) 
.equ TIM_CR2_OIS4_Pos, (14) 
.equ TIM_CR2_OIS4_Msk, (0x1 << TIM_CR2_OIS4_Pos) //!< 0x00004000 
.equ TIM_CR2_OIS4, TIM_CR2_OIS4_Msk //!<Output Idle state 4 (OC4 output) 
.equ TIM_CR2_OIS5_Pos, (16) 
.equ TIM_CR2_OIS5_Msk, (0x1 << TIM_CR2_OIS5_Pos) //!< 0x00010000 
.equ TIM_CR2_OIS5, TIM_CR2_OIS5_Msk //!<Output Idle state 4 (OC4 output) 
.equ TIM_CR2_OIS6_Pos, (18) 
.equ TIM_CR2_OIS6_Msk, (0x1 << TIM_CR2_OIS6_Pos) //!< 0x00040000 
.equ TIM_CR2_OIS6, TIM_CR2_OIS6_Msk //!<Output Idle state 4 (OC4 output) 
.equ TIM_CR2_MMS2_Pos, (20) 
.equ TIM_CR2_MMS2_Msk, (0xF << TIM_CR2_MMS2_Pos) //!< 0x00F00000 
.equ TIM_CR2_MMS2, TIM_CR2_MMS2_Msk //!<MMS[2:0] bits (Master Mode Selection) 
.equ TIM_CR2_MMS2_0, (0x1 << TIM_CR2_MMS2_Pos) //!< 0x00100000 
.equ TIM_CR2_MMS2_1, (0x2 << TIM_CR2_MMS2_Pos) //!< 0x00200000 
.equ TIM_CR2_MMS2_2, (0x4 << TIM_CR2_MMS2_Pos) //!< 0x00400000 
.equ TIM_CR2_MMS2_3, (0x8 << TIM_CR2_MMS2_Pos) //!< 0x00800000 
//******************  Bit definition for TIM_SMCR register  ******************
.equ TIM_SMCR_SMS_Pos, (0) 
.equ TIM_SMCR_SMS_Msk, (0x10007 << TIM_SMCR_SMS_Pos) //!< 0x00010007 
.equ TIM_SMCR_SMS, TIM_SMCR_SMS_Msk //!<SMS[2:0] bits (Slave mode selection) 
.equ TIM_SMCR_SMS_0, (0x00000001) //!<Bit 0 
.equ TIM_SMCR_SMS_1, (0x00000002) //!<Bit 1 
.equ TIM_SMCR_SMS_2, (0x00000004) //!<Bit 2 
.equ TIM_SMCR_SMS_3, (0x00010000) //!<Bit 3 
.equ TIM_SMCR_OCCS_Pos, (3) 
.equ TIM_SMCR_OCCS_Msk, (0x1 << TIM_SMCR_OCCS_Pos) //!< 0x00000008 
.equ TIM_SMCR_OCCS, TIM_SMCR_OCCS_Msk //!< OCREF clear selection 
.equ TIM_SMCR_TS_Pos, (4) 
.equ TIM_SMCR_TS_Msk, (0x7 << TIM_SMCR_TS_Pos) //!< 0x00000070 
.equ TIM_SMCR_TS, TIM_SMCR_TS_Msk //!<TS[2:0] bits (Trigger selection) 
.equ TIM_SMCR_TS_0, (0x1 << TIM_SMCR_TS_Pos) //!< 0x00000010 
.equ TIM_SMCR_TS_1, (0x2 << TIM_SMCR_TS_Pos) //!< 0x00000020 
.equ TIM_SMCR_TS_2, (0x4 << TIM_SMCR_TS_Pos) //!< 0x00000040 
.equ TIM_SMCR_MSM_Pos, (7) 
.equ TIM_SMCR_MSM_Msk, (0x1 << TIM_SMCR_MSM_Pos) //!< 0x00000080 
.equ TIM_SMCR_MSM, TIM_SMCR_MSM_Msk //!<Master/slave mode 
.equ TIM_SMCR_ETF_Pos, (8) 
.equ TIM_SMCR_ETF_Msk, (0xF << TIM_SMCR_ETF_Pos) //!< 0x00000F00 
.equ TIM_SMCR_ETF, TIM_SMCR_ETF_Msk //!<ETF[3:0] bits (External trigger filter) 
.equ TIM_SMCR_ETF_0, (0x1 << TIM_SMCR_ETF_Pos) //!< 0x00000100 
.equ TIM_SMCR_ETF_1, (0x2 << TIM_SMCR_ETF_Pos) //!< 0x00000200 
.equ TIM_SMCR_ETF_2, (0x4 << TIM_SMCR_ETF_Pos) //!< 0x00000400 
.equ TIM_SMCR_ETF_3, (0x8 << TIM_SMCR_ETF_Pos) //!< 0x00000800 
.equ TIM_SMCR_ETPS_Pos, (12) 
.equ TIM_SMCR_ETPS_Msk, (0x3 << TIM_SMCR_ETPS_Pos) //!< 0x00003000 
.equ TIM_SMCR_ETPS, TIM_SMCR_ETPS_Msk //!<ETPS[1:0] bits (External trigger prescaler) 
.equ TIM_SMCR_ETPS_0, (0x1 << TIM_SMCR_ETPS_Pos) //!< 0x00001000 
.equ TIM_SMCR_ETPS_1, (0x2 << TIM_SMCR_ETPS_Pos) //!< 0x00002000 
.equ TIM_SMCR_ECE_Pos, (14) 
.equ TIM_SMCR_ECE_Msk, (0x1 << TIM_SMCR_ECE_Pos) //!< 0x00004000 
.equ TIM_SMCR_ECE, TIM_SMCR_ECE_Msk //!<External clock enable 
.equ TIM_SMCR_ETP_Pos, (15) 
.equ TIM_SMCR_ETP_Msk, (0x1 << TIM_SMCR_ETP_Pos) //!< 0x00008000 
.equ TIM_SMCR_ETP, TIM_SMCR_ETP_Msk //!<External trigger polarity 
//******************  Bit definition for TIM_DIER register  ******************
.equ TIM_DIER_UIE_Pos, (0) 
.equ TIM_DIER_UIE_Msk, (0x1 << TIM_DIER_UIE_Pos) //!< 0x00000001 
.equ TIM_DIER_UIE, TIM_DIER_UIE_Msk //!<Update interrupt enable 
.equ TIM_DIER_CC1IE_Pos, (1) 
.equ TIM_DIER_CC1IE_Msk, (0x1 << TIM_DIER_CC1IE_Pos) //!< 0x00000002 
.equ TIM_DIER_CC1IE, TIM_DIER_CC1IE_Msk //!<Capture/Compare 1 interrupt enable 
.equ TIM_DIER_CC2IE_Pos, (2) 
.equ TIM_DIER_CC2IE_Msk, (0x1 << TIM_DIER_CC2IE_Pos) //!< 0x00000004 
.equ TIM_DIER_CC2IE, TIM_DIER_CC2IE_Msk //!<Capture/Compare 2 interrupt enable 
.equ TIM_DIER_CC3IE_Pos, (3) 
.equ TIM_DIER_CC3IE_Msk, (0x1 << TIM_DIER_CC3IE_Pos) //!< 0x00000008 
.equ TIM_DIER_CC3IE, TIM_DIER_CC3IE_Msk //!<Capture/Compare 3 interrupt enable 
.equ TIM_DIER_CC4IE_Pos, (4) 
.equ TIM_DIER_CC4IE_Msk, (0x1 << TIM_DIER_CC4IE_Pos) //!< 0x00000010 
.equ TIM_DIER_CC4IE, TIM_DIER_CC4IE_Msk //!<Capture/Compare 4 interrupt enable 
.equ TIM_DIER_COMIE_Pos, (5) 
.equ TIM_DIER_COMIE_Msk, (0x1 << TIM_DIER_COMIE_Pos) //!< 0x00000020 
.equ TIM_DIER_COMIE, TIM_DIER_COMIE_Msk //!<COM interrupt enable 
.equ TIM_DIER_TIE_Pos, (6) 
.equ TIM_DIER_TIE_Msk, (0x1 << TIM_DIER_TIE_Pos) //!< 0x00000040 
.equ TIM_DIER_TIE, TIM_DIER_TIE_Msk //!<Trigger interrupt enable 
.equ TIM_DIER_BIE_Pos, (7) 
.equ TIM_DIER_BIE_Msk, (0x1 << TIM_DIER_BIE_Pos) //!< 0x00000080 
.equ TIM_DIER_BIE, TIM_DIER_BIE_Msk //!<Break interrupt enable 
.equ TIM_DIER_UDE_Pos, (8) 
.equ TIM_DIER_UDE_Msk, (0x1 << TIM_DIER_UDE_Pos) //!< 0x00000100 
.equ TIM_DIER_UDE, TIM_DIER_UDE_Msk //!<Update DMA request enable 
.equ TIM_DIER_CC1DE_Pos, (9) 
.equ TIM_DIER_CC1DE_Msk, (0x1 << TIM_DIER_CC1DE_Pos) //!< 0x00000200 
.equ TIM_DIER_CC1DE, TIM_DIER_CC1DE_Msk //!<Capture/Compare 1 DMA request enable 
.equ TIM_DIER_CC2DE_Pos, (10) 
.equ TIM_DIER_CC2DE_Msk, (0x1 << TIM_DIER_CC2DE_Pos) //!< 0x00000400 
.equ TIM_DIER_CC2DE, TIM_DIER_CC2DE_Msk //!<Capture/Compare 2 DMA request enable 
.equ TIM_DIER_CC3DE_Pos, (11) 
.equ TIM_DIER_CC3DE_Msk, (0x1 << TIM_DIER_CC3DE_Pos) //!< 0x00000800 
.equ TIM_DIER_CC3DE, TIM_DIER_CC3DE_Msk //!<Capture/Compare 3 DMA request enable 
.equ TIM_DIER_CC4DE_Pos, (12) 
.equ TIM_DIER_CC4DE_Msk, (0x1 << TIM_DIER_CC4DE_Pos) //!< 0x00001000 
.equ TIM_DIER_CC4DE, TIM_DIER_CC4DE_Msk //!<Capture/Compare 4 DMA request enable 
.equ TIM_DIER_COMDE_Pos, (13) 
.equ TIM_DIER_COMDE_Msk, (0x1 << TIM_DIER_COMDE_Pos) //!< 0x00002000 
.equ TIM_DIER_COMDE, TIM_DIER_COMDE_Msk //!<COM DMA request enable 
.equ TIM_DIER_TDE_Pos, (14) 
.equ TIM_DIER_TDE_Msk, (0x1 << TIM_DIER_TDE_Pos) //!< 0x00004000 
.equ TIM_DIER_TDE, TIM_DIER_TDE_Msk //!<Trigger DMA request enable 
//*******************  Bit definition for TIM_SR register  *******************
.equ TIM_SR_UIF_Pos, (0) 
.equ TIM_SR_UIF_Msk, (0x1 << TIM_SR_UIF_Pos) //!< 0x00000001 
.equ TIM_SR_UIF, TIM_SR_UIF_Msk //!<Update interrupt Flag 
.equ TIM_SR_CC1IF_Pos, (1) 
.equ TIM_SR_CC1IF_Msk, (0x1 << TIM_SR_CC1IF_Pos) //!< 0x00000002 
.equ TIM_SR_CC1IF, TIM_SR_CC1IF_Msk //!<Capture/Compare 1 interrupt Flag 
.equ TIM_SR_CC2IF_Pos, (2) 
.equ TIM_SR_CC2IF_Msk, (0x1 << TIM_SR_CC2IF_Pos) //!< 0x00000004 
.equ TIM_SR_CC2IF, TIM_SR_CC2IF_Msk //!<Capture/Compare 2 interrupt Flag 
.equ TIM_SR_CC3IF_Pos, (3) 
.equ TIM_SR_CC3IF_Msk, (0x1 << TIM_SR_CC3IF_Pos) //!< 0x00000008 
.equ TIM_SR_CC3IF, TIM_SR_CC3IF_Msk //!<Capture/Compare 3 interrupt Flag 
.equ TIM_SR_CC4IF_Pos, (4) 
.equ TIM_SR_CC4IF_Msk, (0x1 << TIM_SR_CC4IF_Pos) //!< 0x00000010 
.equ TIM_SR_CC4IF, TIM_SR_CC4IF_Msk //!<Capture/Compare 4 interrupt Flag 
.equ TIM_SR_COMIF_Pos, (5) 
.equ TIM_SR_COMIF_Msk, (0x1 << TIM_SR_COMIF_Pos) //!< 0x00000020 
.equ TIM_SR_COMIF, TIM_SR_COMIF_Msk //!<COM interrupt Flag 
.equ TIM_SR_TIF_Pos, (6) 
.equ TIM_SR_TIF_Msk, (0x1 << TIM_SR_TIF_Pos) //!< 0x00000040 
.equ TIM_SR_TIF, TIM_SR_TIF_Msk //!<Trigger interrupt Flag 
.equ TIM_SR_BIF_Pos, (7) 
.equ TIM_SR_BIF_Msk, (0x1 << TIM_SR_BIF_Pos) //!< 0x00000080 
.equ TIM_SR_BIF, TIM_SR_BIF_Msk //!<Break interrupt Flag 
.equ TIM_SR_B2IF_Pos, (8) 
.equ TIM_SR_B2IF_Msk, (0x1 << TIM_SR_B2IF_Pos) //!< 0x00000100 
.equ TIM_SR_B2IF, TIM_SR_B2IF_Msk //!<Break2 interrupt Flag 
.equ TIM_SR_CC1OF_Pos, (9) 
.equ TIM_SR_CC1OF_Msk, (0x1 << TIM_SR_CC1OF_Pos) //!< 0x00000200 
.equ TIM_SR_CC1OF, TIM_SR_CC1OF_Msk //!<Capture/Compare 1 Overcapture Flag 
.equ TIM_SR_CC2OF_Pos, (10) 
.equ TIM_SR_CC2OF_Msk, (0x1 << TIM_SR_CC2OF_Pos) //!< 0x00000400 
.equ TIM_SR_CC2OF, TIM_SR_CC2OF_Msk //!<Capture/Compare 2 Overcapture Flag 
.equ TIM_SR_CC3OF_Pos, (11) 
.equ TIM_SR_CC3OF_Msk, (0x1 << TIM_SR_CC3OF_Pos) //!< 0x00000800 
.equ TIM_SR_CC3OF, TIM_SR_CC3OF_Msk //!<Capture/Compare 3 Overcapture Flag 
.equ TIM_SR_CC4OF_Pos, (12) 
.equ TIM_SR_CC4OF_Msk, (0x1 << TIM_SR_CC4OF_Pos) //!< 0x00001000 
.equ TIM_SR_CC4OF, TIM_SR_CC4OF_Msk //!<Capture/Compare 4 Overcapture Flag 
.equ TIM_SR_CC5IF_Pos, (16) 
.equ TIM_SR_CC5IF_Msk, (0x1 << TIM_SR_CC5IF_Pos) //!< 0x00010000 
.equ TIM_SR_CC5IF, TIM_SR_CC5IF_Msk //!<Capture/Compare 5 interrupt Flag 
.equ TIM_SR_CC6IF_Pos, (17) 
.equ TIM_SR_CC6IF_Msk, (0x1 << TIM_SR_CC6IF_Pos) //!< 0x00020000 
.equ TIM_SR_CC6IF, TIM_SR_CC6IF_Msk //!<Capture/Compare 6 interrupt Flag 
//******************  Bit definition for TIM_EGR register  *******************
.equ TIM_EGR_UG_Pos, (0) 
.equ TIM_EGR_UG_Msk, (0x1 << TIM_EGR_UG_Pos) //!< 0x00000001 
.equ TIM_EGR_UG, TIM_EGR_UG_Msk //!<Update Generation 
.equ TIM_EGR_CC1G_Pos, (1) 
.equ TIM_EGR_CC1G_Msk, (0x1 << TIM_EGR_CC1G_Pos) //!< 0x00000002 
.equ TIM_EGR_CC1G, TIM_EGR_CC1G_Msk //!<Capture/Compare 1 Generation 
.equ TIM_EGR_CC2G_Pos, (2) 
.equ TIM_EGR_CC2G_Msk, (0x1 << TIM_EGR_CC2G_Pos) //!< 0x00000004 
.equ TIM_EGR_CC2G, TIM_EGR_CC2G_Msk //!<Capture/Compare 2 Generation 
.equ TIM_EGR_CC3G_Pos, (3) 
.equ TIM_EGR_CC3G_Msk, (0x1 << TIM_EGR_CC3G_Pos) //!< 0x00000008 
.equ TIM_EGR_CC3G, TIM_EGR_CC3G_Msk //!<Capture/Compare 3 Generation 
.equ TIM_EGR_CC4G_Pos, (4) 
.equ TIM_EGR_CC4G_Msk, (0x1 << TIM_EGR_CC4G_Pos) //!< 0x00000010 
.equ TIM_EGR_CC4G, TIM_EGR_CC4G_Msk //!<Capture/Compare 4 Generation 
.equ TIM_EGR_COMG_Pos, (5) 
.equ TIM_EGR_COMG_Msk, (0x1 << TIM_EGR_COMG_Pos) //!< 0x00000020 
.equ TIM_EGR_COMG, TIM_EGR_COMG_Msk //!<Capture/Compare Control Update Generation 
.equ TIM_EGR_TG_Pos, (6) 
.equ TIM_EGR_TG_Msk, (0x1 << TIM_EGR_TG_Pos) //!< 0x00000040 
.equ TIM_EGR_TG, TIM_EGR_TG_Msk //!<Trigger Generation 
.equ TIM_EGR_BG_Pos, (7) 
.equ TIM_EGR_BG_Msk, (0x1 << TIM_EGR_BG_Pos) //!< 0x00000080 
.equ TIM_EGR_BG, TIM_EGR_BG_Msk //!<Break Generation 
.equ TIM_EGR_B2G_Pos, (8) 
.equ TIM_EGR_B2G_Msk, (0x1 << TIM_EGR_B2G_Pos) //!< 0x00000100 
.equ TIM_EGR_B2G, TIM_EGR_B2G_Msk //!<Break Generation 
//*****************  Bit definition for TIM_CCMR1 register  ******************
.equ TIM_CCMR1_CC1S_Pos, (0) 
.equ TIM_CCMR1_CC1S_Msk, (0x3 << TIM_CCMR1_CC1S_Pos) //!< 0x00000003 
.equ TIM_CCMR1_CC1S, TIM_CCMR1_CC1S_Msk //!<CC1S[1:0] bits (Capture/Compare 1 Selection) 
.equ TIM_CCMR1_CC1S_0, (0x1 << TIM_CCMR1_CC1S_Pos) //!< 0x00000001 
.equ TIM_CCMR1_CC1S_1, (0x2 << TIM_CCMR1_CC1S_Pos) //!< 0x00000002 
.equ TIM_CCMR1_OC1FE_Pos, (2) 
.equ TIM_CCMR1_OC1FE_Msk, (0x1 << TIM_CCMR1_OC1FE_Pos) //!< 0x00000004 
.equ TIM_CCMR1_OC1FE, TIM_CCMR1_OC1FE_Msk //!<Output Compare 1 Fast enable 
.equ TIM_CCMR1_OC1PE_Pos, (3) 
.equ TIM_CCMR1_OC1PE_Msk, (0x1 << TIM_CCMR1_OC1PE_Pos) //!< 0x00000008 
.equ TIM_CCMR1_OC1PE, TIM_CCMR1_OC1PE_Msk //!<Output Compare 1 Preload enable 
.equ TIM_CCMR1_OC1M_Pos, (4) 
.equ TIM_CCMR1_OC1M_Msk, (0x1007 << TIM_CCMR1_OC1M_Pos) //!< 0x00010070 
.equ TIM_CCMR1_OC1M, TIM_CCMR1_OC1M_Msk //!<OC1M[2:0] bits (Output Compare 1 Mode) 
.equ TIM_CCMR1_OC1M_0, (0x00000010) //!<Bit 0 
.equ TIM_CCMR1_OC1M_1, (0x00000020) //!<Bit 1 
.equ TIM_CCMR1_OC1M_2, (0x00000040) //!<Bit 2 
.equ TIM_CCMR1_OC1M_3, (0x00010000) //!<Bit 3 
.equ TIM_CCMR1_OC1CE_Pos, (7) 
.equ TIM_CCMR1_OC1CE_Msk, (0x1 << TIM_CCMR1_OC1CE_Pos) //!< 0x00000080 
.equ TIM_CCMR1_OC1CE, TIM_CCMR1_OC1CE_Msk //!<Output Compare 1Clear Enable 
.equ TIM_CCMR1_CC2S_Pos, (8) 
.equ TIM_CCMR1_CC2S_Msk, (0x3 << TIM_CCMR1_CC2S_Pos) //!< 0x00000300 
.equ TIM_CCMR1_CC2S, TIM_CCMR1_CC2S_Msk //!<CC2S[1:0] bits (Capture/Compare 2 Selection) 
.equ TIM_CCMR1_CC2S_0, (0x1 << TIM_CCMR1_CC2S_Pos) //!< 0x00000100 
.equ TIM_CCMR1_CC2S_1, (0x2 << TIM_CCMR1_CC2S_Pos) //!< 0x00000200 
.equ TIM_CCMR1_OC2FE_Pos, (10) 
.equ TIM_CCMR1_OC2FE_Msk, (0x1 << TIM_CCMR1_OC2FE_Pos) //!< 0x00000400 
.equ TIM_CCMR1_OC2FE, TIM_CCMR1_OC2FE_Msk //!<Output Compare 2 Fast enable 
.equ TIM_CCMR1_OC2PE_Pos, (11) 
.equ TIM_CCMR1_OC2PE_Msk, (0x1 << TIM_CCMR1_OC2PE_Pos) //!< 0x00000800 
.equ TIM_CCMR1_OC2PE, TIM_CCMR1_OC2PE_Msk //!<Output Compare 2 Preload enable 
.equ TIM_CCMR1_OC2M_Pos, (12) 
.equ TIM_CCMR1_OC2M_Msk, (0x1007 << TIM_CCMR1_OC2M_Pos) //!< 0x01007000 
.equ TIM_CCMR1_OC2M, TIM_CCMR1_OC2M_Msk //!<OC2M[2:0] bits (Output Compare 2 Mode) 
.equ TIM_CCMR1_OC2M_0, (0x00001000) //!<Bit 0 
.equ TIM_CCMR1_OC2M_1, (0x00002000) //!<Bit 1 
.equ TIM_CCMR1_OC2M_2, (0x00004000) //!<Bit 2 
.equ TIM_CCMR1_OC2M_3, (0x01000000) //!<Bit 3 
.equ TIM_CCMR1_OC2CE_Pos, (15) 
.equ TIM_CCMR1_OC2CE_Msk, (0x1 << TIM_CCMR1_OC2CE_Pos) //!< 0x00008000 
.equ TIM_CCMR1_OC2CE, TIM_CCMR1_OC2CE_Msk //!<Output Compare 2 Clear Enable 
//----------------------------------------------------------------------------
.equ TIM_CCMR1_IC1PSC_Pos, (2) 
.equ TIM_CCMR1_IC1PSC_Msk, (0x3 << TIM_CCMR1_IC1PSC_Pos) //!< 0x0000000C 
.equ TIM_CCMR1_IC1PSC, TIM_CCMR1_IC1PSC_Msk //!<IC1PSC[1:0] bits (Input Capture 1 Prescaler) 
.equ TIM_CCMR1_IC1PSC_0, (0x1 << TIM_CCMR1_IC1PSC_Pos) //!< 0x00000004 
.equ TIM_CCMR1_IC1PSC_1, (0x2 << TIM_CCMR1_IC1PSC_Pos) //!< 0x00000008 
.equ TIM_CCMR1_IC1F_Pos, (4) 
.equ TIM_CCMR1_IC1F_Msk, (0xF << TIM_CCMR1_IC1F_Pos) //!< 0x000000F0 
.equ TIM_CCMR1_IC1F, TIM_CCMR1_IC1F_Msk //!<IC1F[3:0] bits (Input Capture 1 Filter) 
.equ TIM_CCMR1_IC1F_0, (0x1 << TIM_CCMR1_IC1F_Pos) //!< 0x00000010 
.equ TIM_CCMR1_IC1F_1, (0x2 << TIM_CCMR1_IC1F_Pos) //!< 0x00000020 
.equ TIM_CCMR1_IC1F_2, (0x4 << TIM_CCMR1_IC1F_Pos) //!< 0x00000040 
.equ TIM_CCMR1_IC1F_3, (0x8 << TIM_CCMR1_IC1F_Pos) //!< 0x00000080 
.equ TIM_CCMR1_IC2PSC_Pos, (10) 
.equ TIM_CCMR1_IC2PSC_Msk, (0x3 << TIM_CCMR1_IC2PSC_Pos) //!< 0x00000C00 
.equ TIM_CCMR1_IC2PSC, TIM_CCMR1_IC2PSC_Msk //!<IC2PSC[1:0] bits (Input Capture 2 Prescaler) 
.equ TIM_CCMR1_IC2PSC_0, (0x1 << TIM_CCMR1_IC2PSC_Pos) //!< 0x00000400 
.equ TIM_CCMR1_IC2PSC_1, (0x2 << TIM_CCMR1_IC2PSC_Pos) //!< 0x00000800 
.equ TIM_CCMR1_IC2F_Pos, (12) 
.equ TIM_CCMR1_IC2F_Msk, (0xF << TIM_CCMR1_IC2F_Pos) //!< 0x0000F000 
.equ TIM_CCMR1_IC2F, TIM_CCMR1_IC2F_Msk //!<IC2F[3:0] bits (Input Capture 2 Filter) 
.equ TIM_CCMR1_IC2F_0, (0x1 << TIM_CCMR1_IC2F_Pos) //!< 0x00001000 
.equ TIM_CCMR1_IC2F_1, (0x2 << TIM_CCMR1_IC2F_Pos) //!< 0x00002000 
.equ TIM_CCMR1_IC2F_2, (0x4 << TIM_CCMR1_IC2F_Pos) //!< 0x00004000 
.equ TIM_CCMR1_IC2F_3, (0x8 << TIM_CCMR1_IC2F_Pos) //!< 0x00008000 
//*****************  Bit definition for TIM_CCMR2 register  ******************
.equ TIM_CCMR2_CC3S_Pos, (0) 
.equ TIM_CCMR2_CC3S_Msk, (0x3 << TIM_CCMR2_CC3S_Pos) //!< 0x00000003 
.equ TIM_CCMR2_CC3S, TIM_CCMR2_CC3S_Msk //!<CC3S[1:0] bits (Capture/Compare 3 Selection) 
.equ TIM_CCMR2_CC3S_0, (0x1 << TIM_CCMR2_CC3S_Pos) //!< 0x00000001 
.equ TIM_CCMR2_CC3S_1, (0x2 << TIM_CCMR2_CC3S_Pos) //!< 0x00000002 
.equ TIM_CCMR2_OC3FE_Pos, (2) 
.equ TIM_CCMR2_OC3FE_Msk, (0x1 << TIM_CCMR2_OC3FE_Pos) //!< 0x00000004 
.equ TIM_CCMR2_OC3FE, TIM_CCMR2_OC3FE_Msk //!<Output Compare 3 Fast enable 
.equ TIM_CCMR2_OC3PE_Pos, (3) 
.equ TIM_CCMR2_OC3PE_Msk, (0x1 << TIM_CCMR2_OC3PE_Pos) //!< 0x00000008 
.equ TIM_CCMR2_OC3PE, TIM_CCMR2_OC3PE_Msk //!<Output Compare 3 Preload enable 
.equ TIM_CCMR2_OC3M_Pos, (4) 
.equ TIM_CCMR2_OC3M_Msk, (0x1007 << TIM_CCMR2_OC3M_Pos) //!< 0x00010070 
.equ TIM_CCMR2_OC3M, TIM_CCMR2_OC3M_Msk //!<OC3M[2:0] bits (Output Compare 3 Mode) 
.equ TIM_CCMR2_OC3M_0, (0x00000010) //!<Bit 0 
.equ TIM_CCMR2_OC3M_1, (0x00000020) //!<Bit 1 
.equ TIM_CCMR2_OC3M_2, (0x00000040) //!<Bit 2 
.equ TIM_CCMR2_OC3M_3, (0x00010000) //!<Bit 3 
.equ TIM_CCMR2_OC3CE_Pos, (7) 
.equ TIM_CCMR2_OC3CE_Msk, (0x1 << TIM_CCMR2_OC3CE_Pos) //!< 0x00000080 
.equ TIM_CCMR2_OC3CE, TIM_CCMR2_OC3CE_Msk //!<Output Compare 3 Clear Enable 
.equ TIM_CCMR2_CC4S_Pos, (8) 
.equ TIM_CCMR2_CC4S_Msk, (0x3 << TIM_CCMR2_CC4S_Pos) //!< 0x00000300 
.equ TIM_CCMR2_CC4S, TIM_CCMR2_CC4S_Msk //!<CC4S[1:0] bits (Capture/Compare 4 Selection) 
.equ TIM_CCMR2_CC4S_0, (0x1 << TIM_CCMR2_CC4S_Pos) //!< 0x00000100 
.equ TIM_CCMR2_CC4S_1, (0x2 << TIM_CCMR2_CC4S_Pos) //!< 0x00000200 
.equ TIM_CCMR2_OC4FE_Pos, (10) 
.equ TIM_CCMR2_OC4FE_Msk, (0x1 << TIM_CCMR2_OC4FE_Pos) //!< 0x00000400 
.equ TIM_CCMR2_OC4FE, TIM_CCMR2_OC4FE_Msk //!<Output Compare 4 Fast enable 
.equ TIM_CCMR2_OC4PE_Pos, (11) 
.equ TIM_CCMR2_OC4PE_Msk, (0x1 << TIM_CCMR2_OC4PE_Pos) //!< 0x00000800 
.equ TIM_CCMR2_OC4PE, TIM_CCMR2_OC4PE_Msk //!<Output Compare 4 Preload enable 
.equ TIM_CCMR2_OC4M_Pos, (12) 
.equ TIM_CCMR2_OC4M_Msk, (0x1007 << TIM_CCMR2_OC4M_Pos) //!< 0x01007000 
.equ TIM_CCMR2_OC4M, TIM_CCMR2_OC4M_Msk //!<OC4M[2:0] bits (Output Compare 4 Mode) 
.equ TIM_CCMR2_OC4M_0, (0x00001000) //!<Bit 0 
.equ TIM_CCMR2_OC4M_1, (0x00002000) //!<Bit 1 
.equ TIM_CCMR2_OC4M_2, (0x00004000) //!<Bit 2 
.equ TIM_CCMR2_OC4M_3, (0x01000000) //!<Bit 3 
.equ TIM_CCMR2_OC4CE_Pos, (15) 
.equ TIM_CCMR2_OC4CE_Msk, (0x1 << TIM_CCMR2_OC4CE_Pos) //!< 0x00008000 
.equ TIM_CCMR2_OC4CE, TIM_CCMR2_OC4CE_Msk //!<Output Compare 4 Clear Enable 
//----------------------------------------------------------------------------
.equ TIM_CCMR2_IC3PSC_Pos, (2) 
.equ TIM_CCMR2_IC3PSC_Msk, (0x3 << TIM_CCMR2_IC3PSC_Pos) //!< 0x0000000C 
.equ TIM_CCMR2_IC3PSC, TIM_CCMR2_IC3PSC_Msk //!<IC3PSC[1:0] bits (Input Capture 3 Prescaler) 
.equ TIM_CCMR2_IC3PSC_0, (0x1 << TIM_CCMR2_IC3PSC_Pos) //!< 0x00000004 
.equ TIM_CCMR2_IC3PSC_1, (0x2 << TIM_CCMR2_IC3PSC_Pos) //!< 0x00000008 
.equ TIM_CCMR2_IC3F_Pos, (4) 
.equ TIM_CCMR2_IC3F_Msk, (0xF << TIM_CCMR2_IC3F_Pos) //!< 0x000000F0 
.equ TIM_CCMR2_IC3F, TIM_CCMR2_IC3F_Msk //!<IC3F[3:0] bits (Input Capture 3 Filter) 
.equ TIM_CCMR2_IC3F_0, (0x1 << TIM_CCMR2_IC3F_Pos) //!< 0x00000010 
.equ TIM_CCMR2_IC3F_1, (0x2 << TIM_CCMR2_IC3F_Pos) //!< 0x00000020 
.equ TIM_CCMR2_IC3F_2, (0x4 << TIM_CCMR2_IC3F_Pos) //!< 0x00000040 
.equ TIM_CCMR2_IC3F_3, (0x8 << TIM_CCMR2_IC3F_Pos) //!< 0x00000080 
.equ TIM_CCMR2_IC4PSC_Pos, (10) 
.equ TIM_CCMR2_IC4PSC_Msk, (0x3 << TIM_CCMR2_IC4PSC_Pos) //!< 0x00000C00 
.equ TIM_CCMR2_IC4PSC, TIM_CCMR2_IC4PSC_Msk //!<IC4PSC[1:0] bits (Input Capture 4 Prescaler) 
.equ TIM_CCMR2_IC4PSC_0, (0x1 << TIM_CCMR2_IC4PSC_Pos) //!< 0x00000400 
.equ TIM_CCMR2_IC4PSC_1, (0x2 << TIM_CCMR2_IC4PSC_Pos) //!< 0x00000800 
.equ TIM_CCMR2_IC4F_Pos, (12) 
.equ TIM_CCMR2_IC4F_Msk, (0xF << TIM_CCMR2_IC4F_Pos) //!< 0x0000F000 
.equ TIM_CCMR2_IC4F, TIM_CCMR2_IC4F_Msk //!<IC4F[3:0] bits (Input Capture 4 Filter) 
.equ TIM_CCMR2_IC4F_0, (0x1 << TIM_CCMR2_IC4F_Pos) //!< 0x00001000 
.equ TIM_CCMR2_IC4F_1, (0x2 << TIM_CCMR2_IC4F_Pos) //!< 0x00002000 
.equ TIM_CCMR2_IC4F_2, (0x4 << TIM_CCMR2_IC4F_Pos) //!< 0x00004000 
.equ TIM_CCMR2_IC4F_3, (0x8 << TIM_CCMR2_IC4F_Pos) //!< 0x00008000 
//******************  Bit definition for TIM_CCER register  ******************
.equ TIM_CCER_CC1E_Pos, (0) 
.equ TIM_CCER_CC1E_Msk, (0x1 << TIM_CCER_CC1E_Pos) //!< 0x00000001 
.equ TIM_CCER_CC1E, TIM_CCER_CC1E_Msk //!<Capture/Compare 1 output enable 
.equ TIM_CCER_CC1P_Pos, (1) 
.equ TIM_CCER_CC1P_Msk, (0x1 << TIM_CCER_CC1P_Pos) //!< 0x00000002 
.equ TIM_CCER_CC1P, TIM_CCER_CC1P_Msk //!<Capture/Compare 1 output Polarity 
.equ TIM_CCER_CC1NE_Pos, (2) 
.equ TIM_CCER_CC1NE_Msk, (0x1 << TIM_CCER_CC1NE_Pos) //!< 0x00000004 
.equ TIM_CCER_CC1NE, TIM_CCER_CC1NE_Msk //!<Capture/Compare 1 Complementary output enable 
.equ TIM_CCER_CC1NP_Pos, (3) 
.equ TIM_CCER_CC1NP_Msk, (0x1 << TIM_CCER_CC1NP_Pos) //!< 0x00000008 
.equ TIM_CCER_CC1NP, TIM_CCER_CC1NP_Msk //!<Capture/Compare 1 Complementary output Polarity 
.equ TIM_CCER_CC2E_Pos, (4) 
.equ TIM_CCER_CC2E_Msk, (0x1 << TIM_CCER_CC2E_Pos) //!< 0x00000010 
.equ TIM_CCER_CC2E, TIM_CCER_CC2E_Msk //!<Capture/Compare 2 output enable 
.equ TIM_CCER_CC2P_Pos, (5) 
.equ TIM_CCER_CC2P_Msk, (0x1 << TIM_CCER_CC2P_Pos) //!< 0x00000020 
.equ TIM_CCER_CC2P, TIM_CCER_CC2P_Msk //!<Capture/Compare 2 output Polarity 
.equ TIM_CCER_CC2NE_Pos, (6) 
.equ TIM_CCER_CC2NE_Msk, (0x1 << TIM_CCER_CC2NE_Pos) //!< 0x00000040 
.equ TIM_CCER_CC2NE, TIM_CCER_CC2NE_Msk //!<Capture/Compare 2 Complementary output enable 
.equ TIM_CCER_CC2NP_Pos, (7) 
.equ TIM_CCER_CC2NP_Msk, (0x1 << TIM_CCER_CC2NP_Pos) //!< 0x00000080 
.equ TIM_CCER_CC2NP, TIM_CCER_CC2NP_Msk //!<Capture/Compare 2 Complementary output Polarity 
.equ TIM_CCER_CC3E_Pos, (8) 
.equ TIM_CCER_CC3E_Msk, (0x1 << TIM_CCER_CC3E_Pos) //!< 0x00000100 
.equ TIM_CCER_CC3E, TIM_CCER_CC3E_Msk //!<Capture/Compare 3 output enable 
.equ TIM_CCER_CC3P_Pos, (9) 
.equ TIM_CCER_CC3P_Msk, (0x1 << TIM_CCER_CC3P_Pos) //!< 0x00000200 
.equ TIM_CCER_CC3P, TIM_CCER_CC3P_Msk //!<Capture/Compare 3 output Polarity 
.equ TIM_CCER_CC3NE_Pos, (10) 
.equ TIM_CCER_CC3NE_Msk, (0x1 << TIM_CCER_CC3NE_Pos) //!< 0x00000400 
.equ TIM_CCER_CC3NE, TIM_CCER_CC3NE_Msk //!<Capture/Compare 3 Complementary output enable 
.equ TIM_CCER_CC3NP_Pos, (11) 
.equ TIM_CCER_CC3NP_Msk, (0x1 << TIM_CCER_CC3NP_Pos) //!< 0x00000800 
.equ TIM_CCER_CC3NP, TIM_CCER_CC3NP_Msk //!<Capture/Compare 3 Complementary output Polarity 
.equ TIM_CCER_CC4E_Pos, (12) 
.equ TIM_CCER_CC4E_Msk, (0x1 << TIM_CCER_CC4E_Pos) //!< 0x00001000 
.equ TIM_CCER_CC4E, TIM_CCER_CC4E_Msk //!<Capture/Compare 4 output enable 
.equ TIM_CCER_CC4P_Pos, (13) 
.equ TIM_CCER_CC4P_Msk, (0x1 << TIM_CCER_CC4P_Pos) //!< 0x00002000 
.equ TIM_CCER_CC4P, TIM_CCER_CC4P_Msk //!<Capture/Compare 4 output Polarity 
.equ TIM_CCER_CC4NP_Pos, (15) 
.equ TIM_CCER_CC4NP_Msk, (0x1 << TIM_CCER_CC4NP_Pos) //!< 0x00008000 
.equ TIM_CCER_CC4NP, TIM_CCER_CC4NP_Msk //!<Capture/Compare 4 Complementary output Polarity 
.equ TIM_CCER_CC5E_Pos, (16) 
.equ TIM_CCER_CC5E_Msk, (0x1 << TIM_CCER_CC5E_Pos) //!< 0x00010000 
.equ TIM_CCER_CC5E, TIM_CCER_CC5E_Msk //!<Capture/Compare 5 output enable 
.equ TIM_CCER_CC5P_Pos, (17) 
.equ TIM_CCER_CC5P_Msk, (0x1 << TIM_CCER_CC5P_Pos) //!< 0x00020000 
.equ TIM_CCER_CC5P, TIM_CCER_CC5P_Msk //!<Capture/Compare 5 output Polarity 
.equ TIM_CCER_CC6E_Pos, (20) 
.equ TIM_CCER_CC6E_Msk, (0x1 << TIM_CCER_CC6E_Pos) //!< 0x00100000 
.equ TIM_CCER_CC6E, TIM_CCER_CC6E_Msk //!<Capture/Compare 6 output enable 
.equ TIM_CCER_CC6P_Pos, (21) 
.equ TIM_CCER_CC6P_Msk, (0x1 << TIM_CCER_CC6P_Pos) //!< 0x00200000 
.equ TIM_CCER_CC6P, TIM_CCER_CC6P_Msk //!<Capture/Compare 6 output Polarity 
//******************  Bit definition for TIM_CNT register  *******************
.equ TIM_CNT_CNT_Pos, (0) 
.equ TIM_CNT_CNT_Msk, (0xFFFFFFFF << TIM_CNT_CNT_Pos) //!< 0xFFFFFFFF 
.equ TIM_CNT_CNT, TIM_CNT_CNT_Msk //!<Counter Value 
.equ TIM_CNT_UIFCPY_Pos, (31) 
.equ TIM_CNT_UIFCPY_Msk, (0x1 << TIM_CNT_UIFCPY_Pos) //!< 0x80000000 
.equ TIM_CNT_UIFCPY, TIM_CNT_UIFCPY_Msk //!<Update interrupt flag copy 
//******************  Bit definition for TIM_PSC register  *******************
.equ TIM_PSC_PSC_Pos, (0) 
.equ TIM_PSC_PSC_Msk, (0xFFFF << TIM_PSC_PSC_Pos) //!< 0x0000FFFF 
.equ TIM_PSC_PSC, TIM_PSC_PSC_Msk //!<Prescaler Value 
//******************  Bit definition for TIM_ARR register  *******************
.equ TIM_ARR_ARR_Pos, (0) 
.equ TIM_ARR_ARR_Msk, (0xFFFFFFFF << TIM_ARR_ARR_Pos) //!< 0xFFFFFFFF 
.equ TIM_ARR_ARR, TIM_ARR_ARR_Msk //!<actual auto-reload Value 
//******************  Bit definition for TIM_RCR register  *******************
.equ TIM_RCR_REP_Pos, (0) 
.equ TIM_RCR_REP_Msk, (0xFFFF << TIM_RCR_REP_Pos) //!< 0x0000FFFF 
.equ TIM_RCR_REP, TIM_RCR_REP_Msk //!<Repetition Counter Value 
//******************  Bit definition for TIM_CCR1 register  ******************
.equ TIM_CCR1_CCR1_Pos, (0) 
.equ TIM_CCR1_CCR1_Msk, (0xFFFF << TIM_CCR1_CCR1_Pos) //!< 0x0000FFFF 
.equ TIM_CCR1_CCR1, TIM_CCR1_CCR1_Msk //!<Capture/Compare 1 Value 
//******************  Bit definition for TIM_CCR2 register  ******************
.equ TIM_CCR2_CCR2_Pos, (0) 
.equ TIM_CCR2_CCR2_Msk, (0xFFFF << TIM_CCR2_CCR2_Pos) //!< 0x0000FFFF 
.equ TIM_CCR2_CCR2, TIM_CCR2_CCR2_Msk //!<Capture/Compare 2 Value 
//******************  Bit definition for TIM_CCR3 register  ******************
.equ TIM_CCR3_CCR3_Pos, (0) 
.equ TIM_CCR3_CCR3_Msk, (0xFFFF << TIM_CCR3_CCR3_Pos) //!< 0x0000FFFF 
.equ TIM_CCR3_CCR3, TIM_CCR3_CCR3_Msk //!<Capture/Compare 3 Value 
//******************  Bit definition for TIM_CCR4 register  ******************
.equ TIM_CCR4_CCR4_Pos, (0) 
.equ TIM_CCR4_CCR4_Msk, (0xFFFF << TIM_CCR4_CCR4_Pos) //!< 0x0000FFFF 
.equ TIM_CCR4_CCR4, TIM_CCR4_CCR4_Msk //!<Capture/Compare 4 Value 
//******************  Bit definition for TIM_CCR5 register  ******************
.equ TIM_CCR5_CCR5_Pos, (0) 
.equ TIM_CCR5_CCR5_Msk, (0xFFFFFFFF << TIM_CCR5_CCR5_Pos) //!< 0xFFFFFFFF 
.equ TIM_CCR5_CCR5, TIM_CCR5_CCR5_Msk //!<Capture/Compare 5 Value 
.equ TIM_CCR5_GC5C1_Pos, (29) 
.equ TIM_CCR5_GC5C1_Msk, (0x1 << TIM_CCR5_GC5C1_Pos) //!< 0x20000000 
.equ TIM_CCR5_GC5C1, TIM_CCR5_GC5C1_Msk //!<Group Channel 5 and Channel 1 
.equ TIM_CCR5_GC5C2_Pos, (30) 
.equ TIM_CCR5_GC5C2_Msk, (0x1 << TIM_CCR5_GC5C2_Pos) //!< 0x40000000 
.equ TIM_CCR5_GC5C2, TIM_CCR5_GC5C2_Msk //!<Group Channel 5 and Channel 2 
.equ TIM_CCR5_GC5C3_Pos, (31) 
.equ TIM_CCR5_GC5C3_Msk, (0x1 << TIM_CCR5_GC5C3_Pos) //!< 0x80000000 
.equ TIM_CCR5_GC5C3, TIM_CCR5_GC5C3_Msk //!<Group Channel 5 and Channel 3 
//******************  Bit definition for TIM_CCR6 register  ******************
.equ TIM_CCR6_CCR6_Pos, (0) 
.equ TIM_CCR6_CCR6_Msk, (0xFFFF << TIM_CCR6_CCR6_Pos) //!< 0x0000FFFF 
.equ TIM_CCR6_CCR6, TIM_CCR6_CCR6_Msk //!<Capture/Compare 6 Value 
//******************  Bit definition for TIM_BDTR register  ******************
.equ TIM_BDTR_DTG_Pos, (0) 
.equ TIM_BDTR_DTG_Msk, (0xFF << TIM_BDTR_DTG_Pos) //!< 0x000000FF 
.equ TIM_BDTR_DTG, TIM_BDTR_DTG_Msk //!<DTG[0:7] bits (Dead-Time Generator set-up) 
.equ TIM_BDTR_DTG_0, (0x01 << TIM_BDTR_DTG_Pos) //!< 0x00000001 
.equ TIM_BDTR_DTG_1, (0x02 << TIM_BDTR_DTG_Pos) //!< 0x00000002 
.equ TIM_BDTR_DTG_2, (0x04 << TIM_BDTR_DTG_Pos) //!< 0x00000004 
.equ TIM_BDTR_DTG_3, (0x08 << TIM_BDTR_DTG_Pos) //!< 0x00000008 
.equ TIM_BDTR_DTG_4, (0x10 << TIM_BDTR_DTG_Pos) //!< 0x00000010 
.equ TIM_BDTR_DTG_5, (0x20 << TIM_BDTR_DTG_Pos) //!< 0x00000020 
.equ TIM_BDTR_DTG_6, (0x40 << TIM_BDTR_DTG_Pos) //!< 0x00000040 
.equ TIM_BDTR_DTG_7, (0x80 << TIM_BDTR_DTG_Pos) //!< 0x00000080 
.equ TIM_BDTR_LOCK_Pos, (8) 
.equ TIM_BDTR_LOCK_Msk, (0x3 << TIM_BDTR_LOCK_Pos) //!< 0x00000300 
.equ TIM_BDTR_LOCK, TIM_BDTR_LOCK_Msk //!<LOCK[1:0] bits (Lock Configuration) 
.equ TIM_BDTR_LOCK_0, (0x1 << TIM_BDTR_LOCK_Pos) //!< 0x00000100 
.equ TIM_BDTR_LOCK_1, (0x2 << TIM_BDTR_LOCK_Pos) //!< 0x00000200 
.equ TIM_BDTR_OSSI_Pos, (10) 
.equ TIM_BDTR_OSSI_Msk, (0x1 << TIM_BDTR_OSSI_Pos) //!< 0x00000400 
.equ TIM_BDTR_OSSI, TIM_BDTR_OSSI_Msk //!<Off-State Selection for Idle mode 
.equ TIM_BDTR_OSSR_Pos, (11) 
.equ TIM_BDTR_OSSR_Msk, (0x1 << TIM_BDTR_OSSR_Pos) //!< 0x00000800 
.equ TIM_BDTR_OSSR, TIM_BDTR_OSSR_Msk //!<Off-State Selection for Run mode 
.equ TIM_BDTR_BKE_Pos, (12) 
.equ TIM_BDTR_BKE_Msk, (0x1 << TIM_BDTR_BKE_Pos) //!< 0x00001000 
.equ TIM_BDTR_BKE, TIM_BDTR_BKE_Msk //!<Break enable for Break1 
.equ TIM_BDTR_BKP_Pos, (13) 
.equ TIM_BDTR_BKP_Msk, (0x1 << TIM_BDTR_BKP_Pos) //!< 0x00002000 
.equ TIM_BDTR_BKP, TIM_BDTR_BKP_Msk //!<Break Polarity for Break1 
.equ TIM_BDTR_AOE_Pos, (14) 
.equ TIM_BDTR_AOE_Msk, (0x1 << TIM_BDTR_AOE_Pos) //!< 0x00004000 
.equ TIM_BDTR_AOE, TIM_BDTR_AOE_Msk //!<Automatic Output enable 
.equ TIM_BDTR_MOE_Pos, (15) 
.equ TIM_BDTR_MOE_Msk, (0x1 << TIM_BDTR_MOE_Pos) //!< 0x00008000 
.equ TIM_BDTR_MOE, TIM_BDTR_MOE_Msk //!<Main Output enable 
.equ TIM_BDTR_BKF_Pos, (16) 
.equ TIM_BDTR_BKF_Msk, (0xF << TIM_BDTR_BKF_Pos) //!< 0x000F0000 
.equ TIM_BDTR_BKF, TIM_BDTR_BKF_Msk //!<Break Filter for Break1 
.equ TIM_BDTR_BK2F_Pos, (20) 
.equ TIM_BDTR_BK2F_Msk, (0xF << TIM_BDTR_BK2F_Pos) //!< 0x00F00000 
.equ TIM_BDTR_BK2F, TIM_BDTR_BK2F_Msk //!<Break Filter for Break2 
.equ TIM_BDTR_BK2E_Pos, (24) 
.equ TIM_BDTR_BK2E_Msk, (0x1 << TIM_BDTR_BK2E_Pos) //!< 0x01000000 
.equ TIM_BDTR_BK2E, TIM_BDTR_BK2E_Msk //!<Break enable for Break2 
.equ TIM_BDTR_BK2P_Pos, (25) 
.equ TIM_BDTR_BK2P_Msk, (0x1 << TIM_BDTR_BK2P_Pos) //!< 0x02000000 
.equ TIM_BDTR_BK2P, TIM_BDTR_BK2P_Msk //!<Break Polarity for Break2 
//******************  Bit definition for TIM_DCR register  *******************
.equ TIM_DCR_DBA_Pos, (0) 
.equ TIM_DCR_DBA_Msk, (0x1F << TIM_DCR_DBA_Pos) //!< 0x0000001F 
.equ TIM_DCR_DBA, TIM_DCR_DBA_Msk //!<DBA[4:0] bits (DMA Base Address) 
.equ TIM_DCR_DBA_0, (0x01 << TIM_DCR_DBA_Pos) //!< 0x00000001 
.equ TIM_DCR_DBA_1, (0x02 << TIM_DCR_DBA_Pos) //!< 0x00000002 
.equ TIM_DCR_DBA_2, (0x04 << TIM_DCR_DBA_Pos) //!< 0x00000004 
.equ TIM_DCR_DBA_3, (0x08 << TIM_DCR_DBA_Pos) //!< 0x00000008 
.equ TIM_DCR_DBA_4, (0x10 << TIM_DCR_DBA_Pos) //!< 0x00000010 
.equ TIM_DCR_DBL_Pos, (8) 
.equ TIM_DCR_DBL_Msk, (0x1F << TIM_DCR_DBL_Pos) //!< 0x00001F00 
.equ TIM_DCR_DBL, TIM_DCR_DBL_Msk //!<DBL[4:0] bits (DMA Burst Length) 
.equ TIM_DCR_DBL_0, (0x01 << TIM_DCR_DBL_Pos) //!< 0x00000100 
.equ TIM_DCR_DBL_1, (0x02 << TIM_DCR_DBL_Pos) //!< 0x00000200 
.equ TIM_DCR_DBL_2, (0x04 << TIM_DCR_DBL_Pos) //!< 0x00000400 
.equ TIM_DCR_DBL_3, (0x08 << TIM_DCR_DBL_Pos) //!< 0x00000800 
.equ TIM_DCR_DBL_4, (0x10 << TIM_DCR_DBL_Pos) //!< 0x00001000 
//******************  Bit definition for TIM_DMAR register  ******************
.equ TIM_DMAR_DMAB_Pos, (0) 
.equ TIM_DMAR_DMAB_Msk, (0xFFFF << TIM_DMAR_DMAB_Pos) //!< 0x0000FFFF 
.equ TIM_DMAR_DMAB, TIM_DMAR_DMAB_Msk //!<DMA register for burst accesses 
//******************  Bit definition for TIM16_OR register  ********************
.equ TIM16_OR_TI1_RMP_Pos, (0) 
.equ TIM16_OR_TI1_RMP_Msk, (0x3 << TIM16_OR_TI1_RMP_Pos) //!< 0x00000003 
.equ TIM16_OR_TI1_RMP, TIM16_OR_TI1_RMP_Msk //!<TI1_RMP[1:0] bits (TIM16 Input 1 remap) 
.equ TIM16_OR_TI1_RMP_0, (0x1 << TIM16_OR_TI1_RMP_Pos) //!< 0x00000001 
.equ TIM16_OR_TI1_RMP_1, (0x2 << TIM16_OR_TI1_RMP_Pos) //!< 0x00000002 
//******************  Bit definition for TIM1_OR register  ********************
.equ TIM1_OR_ETR_RMP_Pos, (0) 
.equ TIM1_OR_ETR_RMP_Msk, (0xF << TIM1_OR_ETR_RMP_Pos) //!< 0x0000000F 
.equ TIM1_OR_ETR_RMP, TIM1_OR_ETR_RMP_Msk //!<ETR_RMP[3:0] bits (TIM1 ETR remap) 
.equ TIM1_OR_ETR_RMP_0, (0x1 << TIM1_OR_ETR_RMP_Pos) //!< 0x00000001 
.equ TIM1_OR_ETR_RMP_1, (0x2 << TIM1_OR_ETR_RMP_Pos) //!< 0x00000002 
.equ TIM1_OR_ETR_RMP_2, (0x4 << TIM1_OR_ETR_RMP_Pos) //!< 0x00000004 
.equ TIM1_OR_ETR_RMP_3, (0x8 << TIM1_OR_ETR_RMP_Pos) //!< 0x00000008 
//******************  Bit definition for TIM8_OR register  ********************
.equ TIM8_OR_ETR_RMP_Pos, (0) 
.equ TIM8_OR_ETR_RMP_Msk, (0xF << TIM8_OR_ETR_RMP_Pos) //!< 0x0000000F 
.equ TIM8_OR_ETR_RMP, TIM8_OR_ETR_RMP_Msk //!<ETR_RMP[3:0] bits (TIM8 ETR remap) 
.equ TIM8_OR_ETR_RMP_0, (0x1 << TIM8_OR_ETR_RMP_Pos) //!< 0x00000001 
.equ TIM8_OR_ETR_RMP_1, (0x2 << TIM8_OR_ETR_RMP_Pos) //!< 0x00000002 
.equ TIM8_OR_ETR_RMP_2, (0x4 << TIM8_OR_ETR_RMP_Pos) //!< 0x00000004 
.equ TIM8_OR_ETR_RMP_3, (0x8 << TIM8_OR_ETR_RMP_Pos) //!< 0x00000008 
//******************  Bit definition for TIM20_OR register  ******************
.equ TIM20_OR_ETR_RMP_Pos, (0) 
.equ TIM20_OR_ETR_RMP_Msk, (0xF << TIM20_OR_ETR_RMP_Pos) //!< 0x0000000F 
.equ TIM20_OR_ETR_RMP, TIM20_OR_ETR_RMP_Msk //!<ETR_RMP[3:0] bits (TIM20 ETR remap) 
.equ TIM20_OR_ETR_RMP_0, (0x1 << TIM20_OR_ETR_RMP_Pos) //!< 0x00000001 
.equ TIM20_OR_ETR_RMP_1, (0x2 << TIM20_OR_ETR_RMP_Pos) //!< 0x00000002 
.equ TIM20_OR_ETR_RMP_2, (0x4 << TIM20_OR_ETR_RMP_Pos) //!< 0x00000004 
.equ TIM20_OR_ETR_RMP_3, (0x8 << TIM20_OR_ETR_RMP_Pos) //!< 0x00000008 
//*****************  Bit definition for TIM_CCMR3 register  ******************
.equ TIM_CCMR3_OC5FE_Pos, (2) 
.equ TIM_CCMR3_OC5FE_Msk, (0x1 << TIM_CCMR3_OC5FE_Pos) //!< 0x00000004 
.equ TIM_CCMR3_OC5FE, TIM_CCMR3_OC5FE_Msk //!<Output Compare 5 Fast enable 
.equ TIM_CCMR3_OC5PE_Pos, (3) 
.equ TIM_CCMR3_OC5PE_Msk, (0x1 << TIM_CCMR3_OC5PE_Pos) //!< 0x00000008 
.equ TIM_CCMR3_OC5PE, TIM_CCMR3_OC5PE_Msk //!<Output Compare 5 Preload enable 
.equ TIM_CCMR3_OC5M_Pos, (4) 
.equ TIM_CCMR3_OC5M_Msk, (0x1007 << TIM_CCMR3_OC5M_Pos) //!< 0x00010070 
.equ TIM_CCMR3_OC5M, TIM_CCMR3_OC5M_Msk //!<OC5M[2:0] bits (Output Compare 5 Mode) 
.equ TIM_CCMR3_OC5M_0, (0x0001 << TIM_CCMR3_OC5M_Pos) //!< 0x00000010 
.equ TIM_CCMR3_OC5M_1, (0x0002 << TIM_CCMR3_OC5M_Pos) //!< 0x00000020 
.equ TIM_CCMR3_OC5M_2, (0x0004 << TIM_CCMR3_OC5M_Pos) //!< 0x00000040 
.equ TIM_CCMR3_OC5M_3, (0x1000 << TIM_CCMR3_OC5M_Pos) //!< 0x00010000 
.equ TIM_CCMR3_OC5CE_Pos, (7) 
.equ TIM_CCMR3_OC5CE_Msk, (0x1 << TIM_CCMR3_OC5CE_Pos) //!< 0x00000080 
.equ TIM_CCMR3_OC5CE, TIM_CCMR3_OC5CE_Msk //!<Output Compare 5 Clear Enable 
.equ TIM_CCMR3_OC6FE_Pos, (10) 
.equ TIM_CCMR3_OC6FE_Msk, (0x1 << TIM_CCMR3_OC6FE_Pos) //!< 0x00000400 
.equ TIM_CCMR3_OC6FE, TIM_CCMR3_OC6FE_Msk //!<Output Compare 6 Fast enable 
.equ TIM_CCMR3_OC6PE_Pos, (11) 
.equ TIM_CCMR3_OC6PE_Msk, (0x1 << TIM_CCMR3_OC6PE_Pos) //!< 0x00000800 
.equ TIM_CCMR3_OC6PE, TIM_CCMR3_OC6PE_Msk //!<Output Compare 6 Preload enable 
.equ TIM_CCMR3_OC6M_Pos, (12) 
.equ TIM_CCMR3_OC6M_Msk, (0x1007 << TIM_CCMR3_OC6M_Pos) //!< 0x01007000 
.equ TIM_CCMR3_OC6M, TIM_CCMR3_OC6M_Msk //!<OC6M[2:0] bits (Output Compare 6 Mode) 
.equ TIM_CCMR3_OC6M_0, (0x0001 << TIM_CCMR3_OC6M_Pos) //!< 0x00001000 
.equ TIM_CCMR3_OC6M_1, (0x0002 << TIM_CCMR3_OC6M_Pos) //!< 0x00002000 
.equ TIM_CCMR3_OC6M_2, (0x0004 << TIM_CCMR3_OC6M_Pos) //!< 0x00004000 
.equ TIM_CCMR3_OC6M_3, (0x1000 << TIM_CCMR3_OC6M_Pos) //!< 0x01000000 
.equ TIM_CCMR3_OC6CE_Pos, (15) 
.equ TIM_CCMR3_OC6CE_Msk, (0x1 << TIM_CCMR3_OC6CE_Pos) //!< 0x00008000 
.equ TIM_CCMR3_OC6CE, TIM_CCMR3_OC6CE_Msk //!<Output Compare 6 Clear Enable 
//****************************************************************************
//
//                          Touch Sensing Controller (TSC)
//
//****************************************************************************
//******************  Bit definition for TSC_CR register  ********************
.equ TSC_CR_TSCE_Pos, (0) 
.equ TSC_CR_TSCE_Msk, (0x1 << TSC_CR_TSCE_Pos) //!< 0x00000001 
.equ TSC_CR_TSCE, TSC_CR_TSCE_Msk //!<Touch sensing controller enable 
.equ TSC_CR_START_Pos, (1) 
.equ TSC_CR_START_Msk, (0x1 << TSC_CR_START_Pos) //!< 0x00000002 
.equ TSC_CR_START, TSC_CR_START_Msk //!<Start acquisition 
.equ TSC_CR_AM_Pos, (2) 
.equ TSC_CR_AM_Msk, (0x1 << TSC_CR_AM_Pos) //!< 0x00000004 
.equ TSC_CR_AM, TSC_CR_AM_Msk //!<Acquisition mode 
.equ TSC_CR_SYNCPOL_Pos, (3) 
.equ TSC_CR_SYNCPOL_Msk, (0x1 << TSC_CR_SYNCPOL_Pos) //!< 0x00000008 
.equ TSC_CR_SYNCPOL, TSC_CR_SYNCPOL_Msk //!<Synchronization pin polarity 
.equ TSC_CR_IODEF_Pos, (4) 
.equ TSC_CR_IODEF_Msk, (0x1 << TSC_CR_IODEF_Pos) //!< 0x00000010 
.equ TSC_CR_IODEF, TSC_CR_IODEF_Msk //!<IO default mode 
.equ TSC_CR_MCV_Pos, (5) 
.equ TSC_CR_MCV_Msk, (0x7 << TSC_CR_MCV_Pos) //!< 0x000000E0 
.equ TSC_CR_MCV, TSC_CR_MCV_Msk //!<MCV[2:0] bits (Max Count Value) 
.equ TSC_CR_MCV_0, (0x1 << TSC_CR_MCV_Pos) //!< 0x00000020 
.equ TSC_CR_MCV_1, (0x2 << TSC_CR_MCV_Pos) //!< 0x00000040 
.equ TSC_CR_MCV_2, (0x4 << TSC_CR_MCV_Pos) //!< 0x00000080 
.equ TSC_CR_PGPSC_Pos, (12) 
.equ TSC_CR_PGPSC_Msk, (0x7 << TSC_CR_PGPSC_Pos) //!< 0x00007000 
.equ TSC_CR_PGPSC, TSC_CR_PGPSC_Msk //!<PGPSC[2:0] bits (Pulse Generator Prescaler) 
.equ TSC_CR_PGPSC_0, (0x1 << TSC_CR_PGPSC_Pos) //!< 0x00001000 
.equ TSC_CR_PGPSC_1, (0x2 << TSC_CR_PGPSC_Pos) //!< 0x00002000 
.equ TSC_CR_PGPSC_2, (0x4 << TSC_CR_PGPSC_Pos) //!< 0x00004000 
.equ TSC_CR_SSPSC_Pos, (15) 
.equ TSC_CR_SSPSC_Msk, (0x1 << TSC_CR_SSPSC_Pos) //!< 0x00008000 
.equ TSC_CR_SSPSC, TSC_CR_SSPSC_Msk //!<Spread Spectrum Prescaler 
.equ TSC_CR_SSE_Pos, (16) 
.equ TSC_CR_SSE_Msk, (0x1 << TSC_CR_SSE_Pos) //!< 0x00010000 
.equ TSC_CR_SSE, TSC_CR_SSE_Msk //!<Spread Spectrum Enable 
.equ TSC_CR_SSD_Pos, (17) 
.equ TSC_CR_SSD_Msk, (0x7F << TSC_CR_SSD_Pos) //!< 0x00FE0000 
.equ TSC_CR_SSD, TSC_CR_SSD_Msk //!<SSD[6:0] bits (Spread Spectrum Deviation) 
.equ TSC_CR_SSD_0, (0x01 << TSC_CR_SSD_Pos) //!< 0x00020000 
.equ TSC_CR_SSD_1, (0x02 << TSC_CR_SSD_Pos) //!< 0x00040000 
.equ TSC_CR_SSD_2, (0x04 << TSC_CR_SSD_Pos) //!< 0x00080000 
.equ TSC_CR_SSD_3, (0x08 << TSC_CR_SSD_Pos) //!< 0x00100000 
.equ TSC_CR_SSD_4, (0x10 << TSC_CR_SSD_Pos) //!< 0x00200000 
.equ TSC_CR_SSD_5, (0x20 << TSC_CR_SSD_Pos) //!< 0x00400000 
.equ TSC_CR_SSD_6, (0x40 << TSC_CR_SSD_Pos) //!< 0x00800000 
.equ TSC_CR_CTPL_Pos, (24) 
.equ TSC_CR_CTPL_Msk, (0xF << TSC_CR_CTPL_Pos) //!< 0x0F000000 
.equ TSC_CR_CTPL, TSC_CR_CTPL_Msk //!<CTPL[3:0] bits (Charge Transfer pulse low) 
.equ TSC_CR_CTPL_0, (0x1 << TSC_CR_CTPL_Pos) //!< 0x01000000 
.equ TSC_CR_CTPL_1, (0x2 << TSC_CR_CTPL_Pos) //!< 0x02000000 
.equ TSC_CR_CTPL_2, (0x4 << TSC_CR_CTPL_Pos) //!< 0x04000000 
.equ TSC_CR_CTPL_3, (0x8 << TSC_CR_CTPL_Pos) //!< 0x08000000 
.equ TSC_CR_CTPH_Pos, (28) 
.equ TSC_CR_CTPH_Msk, (0xF << TSC_CR_CTPH_Pos) //!< 0xF0000000 
.equ TSC_CR_CTPH, TSC_CR_CTPH_Msk //!<CTPH[3:0] bits (Charge Transfer pulse high) 
.equ TSC_CR_CTPH_0, (0x1 << TSC_CR_CTPH_Pos) //!< 0x10000000 
.equ TSC_CR_CTPH_1, (0x2 << TSC_CR_CTPH_Pos) //!< 0x20000000 
.equ TSC_CR_CTPH_2, (0x4 << TSC_CR_CTPH_Pos) //!< 0x40000000 
.equ TSC_CR_CTPH_3, (0x8 << TSC_CR_CTPH_Pos) //!< 0x80000000 
//******************  Bit definition for TSC_IER register  *******************
.equ TSC_IER_EOAIE_Pos, (0) 
.equ TSC_IER_EOAIE_Msk, (0x1 << TSC_IER_EOAIE_Pos) //!< 0x00000001 
.equ TSC_IER_EOAIE, TSC_IER_EOAIE_Msk //!<End of acquisition interrupt enable 
.equ TSC_IER_MCEIE_Pos, (1) 
.equ TSC_IER_MCEIE_Msk, (0x1 << TSC_IER_MCEIE_Pos) //!< 0x00000002 
.equ TSC_IER_MCEIE, TSC_IER_MCEIE_Msk //!<Max count error interrupt enable 
//******************  Bit definition for TSC_ICR register  *******************
.equ TSC_ICR_EOAIC_Pos, (0) 
.equ TSC_ICR_EOAIC_Msk, (0x1 << TSC_ICR_EOAIC_Pos) //!< 0x00000001 
.equ TSC_ICR_EOAIC, TSC_ICR_EOAIC_Msk //!<End of acquisition interrupt clear 
.equ TSC_ICR_MCEIC_Pos, (1) 
.equ TSC_ICR_MCEIC_Msk, (0x1 << TSC_ICR_MCEIC_Pos) //!< 0x00000002 
.equ TSC_ICR_MCEIC, TSC_ICR_MCEIC_Msk //!<Max count error interrupt clear 
//******************  Bit definition for TSC_ISR register  *******************
.equ TSC_ISR_EOAF_Pos, (0) 
.equ TSC_ISR_EOAF_Msk, (0x1 << TSC_ISR_EOAF_Pos) //!< 0x00000001 
.equ TSC_ISR_EOAF, TSC_ISR_EOAF_Msk //!<End of acquisition flag 
.equ TSC_ISR_MCEF_Pos, (1) 
.equ TSC_ISR_MCEF_Msk, (0x1 << TSC_ISR_MCEF_Pos) //!< 0x00000002 
.equ TSC_ISR_MCEF, TSC_ISR_MCEF_Msk //!<Max count error flag 
//******************  Bit definition for TSC_IOHCR register  *****************
.equ TSC_IOHCR_G1_IO1_Pos, (0) 
.equ TSC_IOHCR_G1_IO1_Msk, (0x1 << TSC_IOHCR_G1_IO1_Pos) //!< 0x00000001 
.equ TSC_IOHCR_G1_IO1, TSC_IOHCR_G1_IO1_Msk //!<GROUP1_IO1 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G1_IO2_Pos, (1) 
.equ TSC_IOHCR_G1_IO2_Msk, (0x1 << TSC_IOHCR_G1_IO2_Pos) //!< 0x00000002 
.equ TSC_IOHCR_G1_IO2, TSC_IOHCR_G1_IO2_Msk //!<GROUP1_IO2 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G1_IO3_Pos, (2) 
.equ TSC_IOHCR_G1_IO3_Msk, (0x1 << TSC_IOHCR_G1_IO3_Pos) //!< 0x00000004 
.equ TSC_IOHCR_G1_IO3, TSC_IOHCR_G1_IO3_Msk //!<GROUP1_IO3 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G1_IO4_Pos, (3) 
.equ TSC_IOHCR_G1_IO4_Msk, (0x1 << TSC_IOHCR_G1_IO4_Pos) //!< 0x00000008 
.equ TSC_IOHCR_G1_IO4, TSC_IOHCR_G1_IO4_Msk //!<GROUP1_IO4 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G2_IO1_Pos, (4) 
.equ TSC_IOHCR_G2_IO1_Msk, (0x1 << TSC_IOHCR_G2_IO1_Pos) //!< 0x00000010 
.equ TSC_IOHCR_G2_IO1, TSC_IOHCR_G2_IO1_Msk //!<GROUP2_IO1 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G2_IO2_Pos, (5) 
.equ TSC_IOHCR_G2_IO2_Msk, (0x1 << TSC_IOHCR_G2_IO2_Pos) //!< 0x00000020 
.equ TSC_IOHCR_G2_IO2, TSC_IOHCR_G2_IO2_Msk //!<GROUP2_IO2 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G2_IO3_Pos, (6) 
.equ TSC_IOHCR_G2_IO3_Msk, (0x1 << TSC_IOHCR_G2_IO3_Pos) //!< 0x00000040 
.equ TSC_IOHCR_G2_IO3, TSC_IOHCR_G2_IO3_Msk //!<GROUP2_IO3 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G2_IO4_Pos, (7) 
.equ TSC_IOHCR_G2_IO4_Msk, (0x1 << TSC_IOHCR_G2_IO4_Pos) //!< 0x00000080 
.equ TSC_IOHCR_G2_IO4, TSC_IOHCR_G2_IO4_Msk //!<GROUP2_IO4 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G3_IO1_Pos, (8) 
.equ TSC_IOHCR_G3_IO1_Msk, (0x1 << TSC_IOHCR_G3_IO1_Pos) //!< 0x00000100 
.equ TSC_IOHCR_G3_IO1, TSC_IOHCR_G3_IO1_Msk //!<GROUP3_IO1 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G3_IO2_Pos, (9) 
.equ TSC_IOHCR_G3_IO2_Msk, (0x1 << TSC_IOHCR_G3_IO2_Pos) //!< 0x00000200 
.equ TSC_IOHCR_G3_IO2, TSC_IOHCR_G3_IO2_Msk //!<GROUP3_IO2 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G3_IO3_Pos, (10) 
.equ TSC_IOHCR_G3_IO3_Msk, (0x1 << TSC_IOHCR_G3_IO3_Pos) //!< 0x00000400 
.equ TSC_IOHCR_G3_IO3, TSC_IOHCR_G3_IO3_Msk //!<GROUP3_IO3 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G3_IO4_Pos, (11) 
.equ TSC_IOHCR_G3_IO4_Msk, (0x1 << TSC_IOHCR_G3_IO4_Pos) //!< 0x00000800 
.equ TSC_IOHCR_G3_IO4, TSC_IOHCR_G3_IO4_Msk //!<GROUP3_IO4 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G4_IO1_Pos, (12) 
.equ TSC_IOHCR_G4_IO1_Msk, (0x1 << TSC_IOHCR_G4_IO1_Pos) //!< 0x00001000 
.equ TSC_IOHCR_G4_IO1, TSC_IOHCR_G4_IO1_Msk //!<GROUP4_IO1 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G4_IO2_Pos, (13) 
.equ TSC_IOHCR_G4_IO2_Msk, (0x1 << TSC_IOHCR_G4_IO2_Pos) //!< 0x00002000 
.equ TSC_IOHCR_G4_IO2, TSC_IOHCR_G4_IO2_Msk //!<GROUP4_IO2 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G4_IO3_Pos, (14) 
.equ TSC_IOHCR_G4_IO3_Msk, (0x1 << TSC_IOHCR_G4_IO3_Pos) //!< 0x00004000 
.equ TSC_IOHCR_G4_IO3, TSC_IOHCR_G4_IO3_Msk //!<GROUP4_IO3 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G4_IO4_Pos, (15) 
.equ TSC_IOHCR_G4_IO4_Msk, (0x1 << TSC_IOHCR_G4_IO4_Pos) //!< 0x00008000 
.equ TSC_IOHCR_G4_IO4, TSC_IOHCR_G4_IO4_Msk //!<GROUP4_IO4 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G5_IO1_Pos, (16) 
.equ TSC_IOHCR_G5_IO1_Msk, (0x1 << TSC_IOHCR_G5_IO1_Pos) //!< 0x00010000 
.equ TSC_IOHCR_G5_IO1, TSC_IOHCR_G5_IO1_Msk //!<GROUP5_IO1 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G5_IO2_Pos, (17) 
.equ TSC_IOHCR_G5_IO2_Msk, (0x1 << TSC_IOHCR_G5_IO2_Pos) //!< 0x00020000 
.equ TSC_IOHCR_G5_IO2, TSC_IOHCR_G5_IO2_Msk //!<GROUP5_IO2 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G5_IO3_Pos, (18) 
.equ TSC_IOHCR_G5_IO3_Msk, (0x1 << TSC_IOHCR_G5_IO3_Pos) //!< 0x00040000 
.equ TSC_IOHCR_G5_IO3, TSC_IOHCR_G5_IO3_Msk //!<GROUP5_IO3 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G5_IO4_Pos, (19) 
.equ TSC_IOHCR_G5_IO4_Msk, (0x1 << TSC_IOHCR_G5_IO4_Pos) //!< 0x00080000 
.equ TSC_IOHCR_G5_IO4, TSC_IOHCR_G5_IO4_Msk //!<GROUP5_IO4 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G6_IO1_Pos, (20) 
.equ TSC_IOHCR_G6_IO1_Msk, (0x1 << TSC_IOHCR_G6_IO1_Pos) //!< 0x00100000 
.equ TSC_IOHCR_G6_IO1, TSC_IOHCR_G6_IO1_Msk //!<GROUP6_IO1 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G6_IO2_Pos, (21) 
.equ TSC_IOHCR_G6_IO2_Msk, (0x1 << TSC_IOHCR_G6_IO2_Pos) //!< 0x00200000 
.equ TSC_IOHCR_G6_IO2, TSC_IOHCR_G6_IO2_Msk //!<GROUP6_IO2 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G6_IO3_Pos, (22) 
.equ TSC_IOHCR_G6_IO3_Msk, (0x1 << TSC_IOHCR_G6_IO3_Pos) //!< 0x00400000 
.equ TSC_IOHCR_G6_IO3, TSC_IOHCR_G6_IO3_Msk //!<GROUP6_IO3 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G6_IO4_Pos, (23) 
.equ TSC_IOHCR_G6_IO4_Msk, (0x1 << TSC_IOHCR_G6_IO4_Pos) //!< 0x00800000 
.equ TSC_IOHCR_G6_IO4, TSC_IOHCR_G6_IO4_Msk //!<GROUP6_IO4 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G7_IO1_Pos, (24) 
.equ TSC_IOHCR_G7_IO1_Msk, (0x1 << TSC_IOHCR_G7_IO1_Pos) //!< 0x01000000 
.equ TSC_IOHCR_G7_IO1, TSC_IOHCR_G7_IO1_Msk //!<GROUP7_IO1 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G7_IO2_Pos, (25) 
.equ TSC_IOHCR_G7_IO2_Msk, (0x1 << TSC_IOHCR_G7_IO2_Pos) //!< 0x02000000 
.equ TSC_IOHCR_G7_IO2, TSC_IOHCR_G7_IO2_Msk //!<GROUP7_IO2 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G7_IO3_Pos, (26) 
.equ TSC_IOHCR_G7_IO3_Msk, (0x1 << TSC_IOHCR_G7_IO3_Pos) //!< 0x04000000 
.equ TSC_IOHCR_G7_IO3, TSC_IOHCR_G7_IO3_Msk //!<GROUP7_IO3 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G7_IO4_Pos, (27) 
.equ TSC_IOHCR_G7_IO4_Msk, (0x1 << TSC_IOHCR_G7_IO4_Pos) //!< 0x08000000 
.equ TSC_IOHCR_G7_IO4, TSC_IOHCR_G7_IO4_Msk //!<GROUP7_IO4 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G8_IO1_Pos, (28) 
.equ TSC_IOHCR_G8_IO1_Msk, (0x1 << TSC_IOHCR_G8_IO1_Pos) //!< 0x10000000 
.equ TSC_IOHCR_G8_IO1, TSC_IOHCR_G8_IO1_Msk //!<GROUP8_IO1 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G8_IO2_Pos, (29) 
.equ TSC_IOHCR_G8_IO2_Msk, (0x1 << TSC_IOHCR_G8_IO2_Pos) //!< 0x20000000 
.equ TSC_IOHCR_G8_IO2, TSC_IOHCR_G8_IO2_Msk //!<GROUP8_IO2 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G8_IO3_Pos, (30) 
.equ TSC_IOHCR_G8_IO3_Msk, (0x1 << TSC_IOHCR_G8_IO3_Pos) //!< 0x40000000 
.equ TSC_IOHCR_G8_IO3, TSC_IOHCR_G8_IO3_Msk //!<GROUP8_IO3 schmitt trigger hysteresis mode 
.equ TSC_IOHCR_G8_IO4_Pos, (31) 
.equ TSC_IOHCR_G8_IO4_Msk, (0x1 << TSC_IOHCR_G8_IO4_Pos) //!< 0x80000000 
.equ TSC_IOHCR_G8_IO4, TSC_IOHCR_G8_IO4_Msk //!<GROUP8_IO4 schmitt trigger hysteresis mode 
//******************  Bit definition for TSC_IOASCR register  ****************
.equ TSC_IOASCR_G1_IO1_Pos, (0) 
.equ TSC_IOASCR_G1_IO1_Msk, (0x1 << TSC_IOASCR_G1_IO1_Pos) //!< 0x00000001 
.equ TSC_IOASCR_G1_IO1, TSC_IOASCR_G1_IO1_Msk //!<GROUP1_IO1 analog switch enable 
.equ TSC_IOASCR_G1_IO2_Pos, (1) 
.equ TSC_IOASCR_G1_IO2_Msk, (0x1 << TSC_IOASCR_G1_IO2_Pos) //!< 0x00000002 
.equ TSC_IOASCR_G1_IO2, TSC_IOASCR_G1_IO2_Msk //!<GROUP1_IO2 analog switch enable 
.equ TSC_IOASCR_G1_IO3_Pos, (2) 
.equ TSC_IOASCR_G1_IO3_Msk, (0x1 << TSC_IOASCR_G1_IO3_Pos) //!< 0x00000004 
.equ TSC_IOASCR_G1_IO3, TSC_IOASCR_G1_IO3_Msk //!<GROUP1_IO3 analog switch enable 
.equ TSC_IOASCR_G1_IO4_Pos, (3) 
.equ TSC_IOASCR_G1_IO4_Msk, (0x1 << TSC_IOASCR_G1_IO4_Pos) //!< 0x00000008 
.equ TSC_IOASCR_G1_IO4, TSC_IOASCR_G1_IO4_Msk //!<GROUP1_IO4 analog switch enable 
.equ TSC_IOASCR_G2_IO1_Pos, (4) 
.equ TSC_IOASCR_G2_IO1_Msk, (0x1 << TSC_IOASCR_G2_IO1_Pos) //!< 0x00000010 
.equ TSC_IOASCR_G2_IO1, TSC_IOASCR_G2_IO1_Msk //!<GROUP2_IO1 analog switch enable 
.equ TSC_IOASCR_G2_IO2_Pos, (5) 
.equ TSC_IOASCR_G2_IO2_Msk, (0x1 << TSC_IOASCR_G2_IO2_Pos) //!< 0x00000020 
.equ TSC_IOASCR_G2_IO2, TSC_IOASCR_G2_IO2_Msk //!<GROUP2_IO2 analog switch enable 
.equ TSC_IOASCR_G2_IO3_Pos, (6) 
.equ TSC_IOASCR_G2_IO3_Msk, (0x1 << TSC_IOASCR_G2_IO3_Pos) //!< 0x00000040 
.equ TSC_IOASCR_G2_IO3, TSC_IOASCR_G2_IO3_Msk //!<GROUP2_IO3 analog switch enable 
.equ TSC_IOASCR_G2_IO4_Pos, (7) 
.equ TSC_IOASCR_G2_IO4_Msk, (0x1 << TSC_IOASCR_G2_IO4_Pos) //!< 0x00000080 
.equ TSC_IOASCR_G2_IO4, TSC_IOASCR_G2_IO4_Msk //!<GROUP2_IO4 analog switch enable 
.equ TSC_IOASCR_G3_IO1_Pos, (8) 
.equ TSC_IOASCR_G3_IO1_Msk, (0x1 << TSC_IOASCR_G3_IO1_Pos) //!< 0x00000100 
.equ TSC_IOASCR_G3_IO1, TSC_IOASCR_G3_IO1_Msk //!<GROUP3_IO1 analog switch enable 
.equ TSC_IOASCR_G3_IO2_Pos, (9) 
.equ TSC_IOASCR_G3_IO2_Msk, (0x1 << TSC_IOASCR_G3_IO2_Pos) //!< 0x00000200 
.equ TSC_IOASCR_G3_IO2, TSC_IOASCR_G3_IO2_Msk //!<GROUP3_IO2 analog switch enable 
.equ TSC_IOASCR_G3_IO3_Pos, (10) 
.equ TSC_IOASCR_G3_IO3_Msk, (0x1 << TSC_IOASCR_G3_IO3_Pos) //!< 0x00000400 
.equ TSC_IOASCR_G3_IO3, TSC_IOASCR_G3_IO3_Msk //!<GROUP3_IO3 analog switch enable 
.equ TSC_IOASCR_G3_IO4_Pos, (11) 
.equ TSC_IOASCR_G3_IO4_Msk, (0x1 << TSC_IOASCR_G3_IO4_Pos) //!< 0x00000800 
.equ TSC_IOASCR_G3_IO4, TSC_IOASCR_G3_IO4_Msk //!<GROUP3_IO4 analog switch enable 
.equ TSC_IOASCR_G4_IO1_Pos, (12) 
.equ TSC_IOASCR_G4_IO1_Msk, (0x1 << TSC_IOASCR_G4_IO1_Pos) //!< 0x00001000 
.equ TSC_IOASCR_G4_IO1, TSC_IOASCR_G4_IO1_Msk //!<GROUP4_IO1 analog switch enable 
.equ TSC_IOASCR_G4_IO2_Pos, (13) 
.equ TSC_IOASCR_G4_IO2_Msk, (0x1 << TSC_IOASCR_G4_IO2_Pos) //!< 0x00002000 
.equ TSC_IOASCR_G4_IO2, TSC_IOASCR_G4_IO2_Msk //!<GROUP4_IO2 analog switch enable 
.equ TSC_IOASCR_G4_IO3_Pos, (14) 
.equ TSC_IOASCR_G4_IO3_Msk, (0x1 << TSC_IOASCR_G4_IO3_Pos) //!< 0x00004000 
.equ TSC_IOASCR_G4_IO3, TSC_IOASCR_G4_IO3_Msk //!<GROUP4_IO3 analog switch enable 
.equ TSC_IOASCR_G4_IO4_Pos, (15) 
.equ TSC_IOASCR_G4_IO4_Msk, (0x1 << TSC_IOASCR_G4_IO4_Pos) //!< 0x00008000 
.equ TSC_IOASCR_G4_IO4, TSC_IOASCR_G4_IO4_Msk //!<GROUP4_IO4 analog switch enable 
.equ TSC_IOASCR_G5_IO1_Pos, (16) 
.equ TSC_IOASCR_G5_IO1_Msk, (0x1 << TSC_IOASCR_G5_IO1_Pos) //!< 0x00010000 
.equ TSC_IOASCR_G5_IO1, TSC_IOASCR_G5_IO1_Msk //!<GROUP5_IO1 analog switch enable 
.equ TSC_IOASCR_G5_IO2_Pos, (17) 
.equ TSC_IOASCR_G5_IO2_Msk, (0x1 << TSC_IOASCR_G5_IO2_Pos) //!< 0x00020000 
.equ TSC_IOASCR_G5_IO2, TSC_IOASCR_G5_IO2_Msk //!<GROUP5_IO2 analog switch enable 
.equ TSC_IOASCR_G5_IO3_Pos, (18) 
.equ TSC_IOASCR_G5_IO3_Msk, (0x1 << TSC_IOASCR_G5_IO3_Pos) //!< 0x00040000 
.equ TSC_IOASCR_G5_IO3, TSC_IOASCR_G5_IO3_Msk //!<GROUP5_IO3 analog switch enable 
.equ TSC_IOASCR_G5_IO4_Pos, (19) 
.equ TSC_IOASCR_G5_IO4_Msk, (0x1 << TSC_IOASCR_G5_IO4_Pos) //!< 0x00080000 
.equ TSC_IOASCR_G5_IO4, TSC_IOASCR_G5_IO4_Msk //!<GROUP5_IO4 analog switch enable 
.equ TSC_IOASCR_G6_IO1_Pos, (20) 
.equ TSC_IOASCR_G6_IO1_Msk, (0x1 << TSC_IOASCR_G6_IO1_Pos) //!< 0x00100000 
.equ TSC_IOASCR_G6_IO1, TSC_IOASCR_G6_IO1_Msk //!<GROUP6_IO1 analog switch enable 
.equ TSC_IOASCR_G6_IO2_Pos, (21) 
.equ TSC_IOASCR_G6_IO2_Msk, (0x1 << TSC_IOASCR_G6_IO2_Pos) //!< 0x00200000 
.equ TSC_IOASCR_G6_IO2, TSC_IOASCR_G6_IO2_Msk //!<GROUP6_IO2 analog switch enable 
.equ TSC_IOASCR_G6_IO3_Pos, (22) 
.equ TSC_IOASCR_G6_IO3_Msk, (0x1 << TSC_IOASCR_G6_IO3_Pos) //!< 0x00400000 
.equ TSC_IOASCR_G6_IO3, TSC_IOASCR_G6_IO3_Msk //!<GROUP6_IO3 analog switch enable 
.equ TSC_IOASCR_G6_IO4_Pos, (23) 
.equ TSC_IOASCR_G6_IO4_Msk, (0x1 << TSC_IOASCR_G6_IO4_Pos) //!< 0x00800000 
.equ TSC_IOASCR_G6_IO4, TSC_IOASCR_G6_IO4_Msk //!<GROUP6_IO4 analog switch enable 
.equ TSC_IOASCR_G7_IO1_Pos, (24) 
.equ TSC_IOASCR_G7_IO1_Msk, (0x1 << TSC_IOASCR_G7_IO1_Pos) //!< 0x01000000 
.equ TSC_IOASCR_G7_IO1, TSC_IOASCR_G7_IO1_Msk //!<GROUP7_IO1 analog switch enable 
.equ TSC_IOASCR_G7_IO2_Pos, (25) 
.equ TSC_IOASCR_G7_IO2_Msk, (0x1 << TSC_IOASCR_G7_IO2_Pos) //!< 0x02000000 
.equ TSC_IOASCR_G7_IO2, TSC_IOASCR_G7_IO2_Msk //!<GROUP7_IO2 analog switch enable 
.equ TSC_IOASCR_G7_IO3_Pos, (26) 
.equ TSC_IOASCR_G7_IO3_Msk, (0x1 << TSC_IOASCR_G7_IO3_Pos) //!< 0x04000000 
.equ TSC_IOASCR_G7_IO3, TSC_IOASCR_G7_IO3_Msk //!<GROUP7_IO3 analog switch enable 
.equ TSC_IOASCR_G7_IO4_Pos, (27) 
.equ TSC_IOASCR_G7_IO4_Msk, (0x1 << TSC_IOASCR_G7_IO4_Pos) //!< 0x08000000 
.equ TSC_IOASCR_G7_IO4, TSC_IOASCR_G7_IO4_Msk //!<GROUP7_IO4 analog switch enable 
.equ TSC_IOASCR_G8_IO1_Pos, (28) 
.equ TSC_IOASCR_G8_IO1_Msk, (0x1 << TSC_IOASCR_G8_IO1_Pos) //!< 0x10000000 
.equ TSC_IOASCR_G8_IO1, TSC_IOASCR_G8_IO1_Msk //!<GROUP8_IO1 analog switch enable 
.equ TSC_IOASCR_G8_IO2_Pos, (29) 
.equ TSC_IOASCR_G8_IO2_Msk, (0x1 << TSC_IOASCR_G8_IO2_Pos) //!< 0x20000000 
.equ TSC_IOASCR_G8_IO2, TSC_IOASCR_G8_IO2_Msk //!<GROUP8_IO2 analog switch enable 
.equ TSC_IOASCR_G8_IO3_Pos, (30) 
.equ TSC_IOASCR_G8_IO3_Msk, (0x1 << TSC_IOASCR_G8_IO3_Pos) //!< 0x40000000 
.equ TSC_IOASCR_G8_IO3, TSC_IOASCR_G8_IO3_Msk //!<GROUP8_IO3 analog switch enable 
.equ TSC_IOASCR_G8_IO4_Pos, (31) 
.equ TSC_IOASCR_G8_IO4_Msk, (0x1 << TSC_IOASCR_G8_IO4_Pos) //!< 0x80000000 
.equ TSC_IOASCR_G8_IO4, TSC_IOASCR_G8_IO4_Msk //!<GROUP8_IO4 analog switch enable 
//******************  Bit definition for TSC_IOSCR register  *****************
.equ TSC_IOSCR_G1_IO1_Pos, (0) 
.equ TSC_IOSCR_G1_IO1_Msk, (0x1 << TSC_IOSCR_G1_IO1_Pos) //!< 0x00000001 
.equ TSC_IOSCR_G1_IO1, TSC_IOSCR_G1_IO1_Msk //!<GROUP1_IO1 sampling mode 
.equ TSC_IOSCR_G1_IO2_Pos, (1) 
.equ TSC_IOSCR_G1_IO2_Msk, (0x1 << TSC_IOSCR_G1_IO2_Pos) //!< 0x00000002 
.equ TSC_IOSCR_G1_IO2, TSC_IOSCR_G1_IO2_Msk //!<GROUP1_IO2 sampling mode 
.equ TSC_IOSCR_G1_IO3_Pos, (2) 
.equ TSC_IOSCR_G1_IO3_Msk, (0x1 << TSC_IOSCR_G1_IO3_Pos) //!< 0x00000004 
.equ TSC_IOSCR_G1_IO3, TSC_IOSCR_G1_IO3_Msk //!<GROUP1_IO3 sampling mode 
.equ TSC_IOSCR_G1_IO4_Pos, (3) 
.equ TSC_IOSCR_G1_IO4_Msk, (0x1 << TSC_IOSCR_G1_IO4_Pos) //!< 0x00000008 
.equ TSC_IOSCR_G1_IO4, TSC_IOSCR_G1_IO4_Msk //!<GROUP1_IO4 sampling mode 
.equ TSC_IOSCR_G2_IO1_Pos, (4) 
.equ TSC_IOSCR_G2_IO1_Msk, (0x1 << TSC_IOSCR_G2_IO1_Pos) //!< 0x00000010 
.equ TSC_IOSCR_G2_IO1, TSC_IOSCR_G2_IO1_Msk //!<GROUP2_IO1 sampling mode 
.equ TSC_IOSCR_G2_IO2_Pos, (5) 
.equ TSC_IOSCR_G2_IO2_Msk, (0x1 << TSC_IOSCR_G2_IO2_Pos) //!< 0x00000020 
.equ TSC_IOSCR_G2_IO2, TSC_IOSCR_G2_IO2_Msk //!<GROUP2_IO2 sampling mode 
.equ TSC_IOSCR_G2_IO3_Pos, (6) 
.equ TSC_IOSCR_G2_IO3_Msk, (0x1 << TSC_IOSCR_G2_IO3_Pos) //!< 0x00000040 
.equ TSC_IOSCR_G2_IO3, TSC_IOSCR_G2_IO3_Msk //!<GROUP2_IO3 sampling mode 
.equ TSC_IOSCR_G2_IO4_Pos, (7) 
.equ TSC_IOSCR_G2_IO4_Msk, (0x1 << TSC_IOSCR_G2_IO4_Pos) //!< 0x00000080 
.equ TSC_IOSCR_G2_IO4, TSC_IOSCR_G2_IO4_Msk //!<GROUP2_IO4 sampling mode 
.equ TSC_IOSCR_G3_IO1_Pos, (8) 
.equ TSC_IOSCR_G3_IO1_Msk, (0x1 << TSC_IOSCR_G3_IO1_Pos) //!< 0x00000100 
.equ TSC_IOSCR_G3_IO1, TSC_IOSCR_G3_IO1_Msk //!<GROUP3_IO1 sampling mode 
.equ TSC_IOSCR_G3_IO2_Pos, (9) 
.equ TSC_IOSCR_G3_IO2_Msk, (0x1 << TSC_IOSCR_G3_IO2_Pos) //!< 0x00000200 
.equ TSC_IOSCR_G3_IO2, TSC_IOSCR_G3_IO2_Msk //!<GROUP3_IO2 sampling mode 
.equ TSC_IOSCR_G3_IO3_Pos, (10) 
.equ TSC_IOSCR_G3_IO3_Msk, (0x1 << TSC_IOSCR_G3_IO3_Pos) //!< 0x00000400 
.equ TSC_IOSCR_G3_IO3, TSC_IOSCR_G3_IO3_Msk //!<GROUP3_IO3 sampling mode 
.equ TSC_IOSCR_G3_IO4_Pos, (11) 
.equ TSC_IOSCR_G3_IO4_Msk, (0x1 << TSC_IOSCR_G3_IO4_Pos) //!< 0x00000800 
.equ TSC_IOSCR_G3_IO4, TSC_IOSCR_G3_IO4_Msk //!<GROUP3_IO4 sampling mode 
.equ TSC_IOSCR_G4_IO1_Pos, (12) 
.equ TSC_IOSCR_G4_IO1_Msk, (0x1 << TSC_IOSCR_G4_IO1_Pos) //!< 0x00001000 
.equ TSC_IOSCR_G4_IO1, TSC_IOSCR_G4_IO1_Msk //!<GROUP4_IO1 sampling mode 
.equ TSC_IOSCR_G4_IO2_Pos, (13) 
.equ TSC_IOSCR_G4_IO2_Msk, (0x1 << TSC_IOSCR_G4_IO2_Pos) //!< 0x00002000 
.equ TSC_IOSCR_G4_IO2, TSC_IOSCR_G4_IO2_Msk //!<GROUP4_IO2 sampling mode 
.equ TSC_IOSCR_G4_IO3_Pos, (14) 
.equ TSC_IOSCR_G4_IO3_Msk, (0x1 << TSC_IOSCR_G4_IO3_Pos) //!< 0x00004000 
.equ TSC_IOSCR_G4_IO3, TSC_IOSCR_G4_IO3_Msk //!<GROUP4_IO3 sampling mode 
.equ TSC_IOSCR_G4_IO4_Pos, (15) 
.equ TSC_IOSCR_G4_IO4_Msk, (0x1 << TSC_IOSCR_G4_IO4_Pos) //!< 0x00008000 
.equ TSC_IOSCR_G4_IO4, TSC_IOSCR_G4_IO4_Msk //!<GROUP4_IO4 sampling mode 
.equ TSC_IOSCR_G5_IO1_Pos, (16) 
.equ TSC_IOSCR_G5_IO1_Msk, (0x1 << TSC_IOSCR_G5_IO1_Pos) //!< 0x00010000 
.equ TSC_IOSCR_G5_IO1, TSC_IOSCR_G5_IO1_Msk //!<GROUP5_IO1 sampling mode 
.equ TSC_IOSCR_G5_IO2_Pos, (17) 
.equ TSC_IOSCR_G5_IO2_Msk, (0x1 << TSC_IOSCR_G5_IO2_Pos) //!< 0x00020000 
.equ TSC_IOSCR_G5_IO2, TSC_IOSCR_G5_IO2_Msk //!<GROUP5_IO2 sampling mode 
.equ TSC_IOSCR_G5_IO3_Pos, (18) 
.equ TSC_IOSCR_G5_IO3_Msk, (0x1 << TSC_IOSCR_G5_IO3_Pos) //!< 0x00040000 
.equ TSC_IOSCR_G5_IO3, TSC_IOSCR_G5_IO3_Msk //!<GROUP5_IO3 sampling mode 
.equ TSC_IOSCR_G5_IO4_Pos, (19) 
.equ TSC_IOSCR_G5_IO4_Msk, (0x1 << TSC_IOSCR_G5_IO4_Pos) //!< 0x00080000 
.equ TSC_IOSCR_G5_IO4, TSC_IOSCR_G5_IO4_Msk //!<GROUP5_IO4 sampling mode 
.equ TSC_IOSCR_G6_IO1_Pos, (20) 
.equ TSC_IOSCR_G6_IO1_Msk, (0x1 << TSC_IOSCR_G6_IO1_Pos) //!< 0x00100000 
.equ TSC_IOSCR_G6_IO1, TSC_IOSCR_G6_IO1_Msk //!<GROUP6_IO1 sampling mode 
.equ TSC_IOSCR_G6_IO2_Pos, (21) 
.equ TSC_IOSCR_G6_IO2_Msk, (0x1 << TSC_IOSCR_G6_IO2_Pos) //!< 0x00200000 
.equ TSC_IOSCR_G6_IO2, TSC_IOSCR_G6_IO2_Msk //!<GROUP6_IO2 sampling mode 
.equ TSC_IOSCR_G6_IO3_Pos, (22) 
.equ TSC_IOSCR_G6_IO3_Msk, (0x1 << TSC_IOSCR_G6_IO3_Pos) //!< 0x00400000 
.equ TSC_IOSCR_G6_IO3, TSC_IOSCR_G6_IO3_Msk //!<GROUP6_IO3 sampling mode 
.equ TSC_IOSCR_G6_IO4_Pos, (23) 
.equ TSC_IOSCR_G6_IO4_Msk, (0x1 << TSC_IOSCR_G6_IO4_Pos) //!< 0x00800000 
.equ TSC_IOSCR_G6_IO4, TSC_IOSCR_G6_IO4_Msk //!<GROUP6_IO4 sampling mode 
.equ TSC_IOSCR_G7_IO1_Pos, (24) 
.equ TSC_IOSCR_G7_IO1_Msk, (0x1 << TSC_IOSCR_G7_IO1_Pos) //!< 0x01000000 
.equ TSC_IOSCR_G7_IO1, TSC_IOSCR_G7_IO1_Msk //!<GROUP7_IO1 sampling mode 
.equ TSC_IOSCR_G7_IO2_Pos, (25) 
.equ TSC_IOSCR_G7_IO2_Msk, (0x1 << TSC_IOSCR_G7_IO2_Pos) //!< 0x02000000 
.equ TSC_IOSCR_G7_IO2, TSC_IOSCR_G7_IO2_Msk //!<GROUP7_IO2 sampling mode 
.equ TSC_IOSCR_G7_IO3_Pos, (26) 
.equ TSC_IOSCR_G7_IO3_Msk, (0x1 << TSC_IOSCR_G7_IO3_Pos) //!< 0x04000000 
.equ TSC_IOSCR_G7_IO3, TSC_IOSCR_G7_IO3_Msk //!<GROUP7_IO3 sampling mode 
.equ TSC_IOSCR_G7_IO4_Pos, (27) 
.equ TSC_IOSCR_G7_IO4_Msk, (0x1 << TSC_IOSCR_G7_IO4_Pos) //!< 0x08000000 
.equ TSC_IOSCR_G7_IO4, TSC_IOSCR_G7_IO4_Msk //!<GROUP7_IO4 sampling mode 
.equ TSC_IOSCR_G8_IO1_Pos, (28) 
.equ TSC_IOSCR_G8_IO1_Msk, (0x1 << TSC_IOSCR_G8_IO1_Pos) //!< 0x10000000 
.equ TSC_IOSCR_G8_IO1, TSC_IOSCR_G8_IO1_Msk //!<GROUP8_IO1 sampling mode 
.equ TSC_IOSCR_G8_IO2_Pos, (29) 
.equ TSC_IOSCR_G8_IO2_Msk, (0x1 << TSC_IOSCR_G8_IO2_Pos) //!< 0x20000000 
.equ TSC_IOSCR_G8_IO2, TSC_IOSCR_G8_IO2_Msk //!<GROUP8_IO2 sampling mode 
.equ TSC_IOSCR_G8_IO3_Pos, (30) 
.equ TSC_IOSCR_G8_IO3_Msk, (0x1 << TSC_IOSCR_G8_IO3_Pos) //!< 0x40000000 
.equ TSC_IOSCR_G8_IO3, TSC_IOSCR_G8_IO3_Msk //!<GROUP8_IO3 sampling mode 
.equ TSC_IOSCR_G8_IO4_Pos, (31) 
.equ TSC_IOSCR_G8_IO4_Msk, (0x1 << TSC_IOSCR_G8_IO4_Pos) //!< 0x80000000 
.equ TSC_IOSCR_G8_IO4, TSC_IOSCR_G8_IO4_Msk //!<GROUP8_IO4 sampling mode 
//******************  Bit definition for TSC_IOCCR register  *****************
.equ TSC_IOCCR_G1_IO1_Pos, (0) 
.equ TSC_IOCCR_G1_IO1_Msk, (0x1 << TSC_IOCCR_G1_IO1_Pos) //!< 0x00000001 
.equ TSC_IOCCR_G1_IO1, TSC_IOCCR_G1_IO1_Msk //!<GROUP1_IO1 channel mode 
.equ TSC_IOCCR_G1_IO2_Pos, (1) 
.equ TSC_IOCCR_G1_IO2_Msk, (0x1 << TSC_IOCCR_G1_IO2_Pos) //!< 0x00000002 
.equ TSC_IOCCR_G1_IO2, TSC_IOCCR_G1_IO2_Msk //!<GROUP1_IO2 channel mode 
.equ TSC_IOCCR_G1_IO3_Pos, (2) 
.equ TSC_IOCCR_G1_IO3_Msk, (0x1 << TSC_IOCCR_G1_IO3_Pos) //!< 0x00000004 
.equ TSC_IOCCR_G1_IO3, TSC_IOCCR_G1_IO3_Msk //!<GROUP1_IO3 channel mode 
.equ TSC_IOCCR_G1_IO4_Pos, (3) 
.equ TSC_IOCCR_G1_IO4_Msk, (0x1 << TSC_IOCCR_G1_IO4_Pos) //!< 0x00000008 
.equ TSC_IOCCR_G1_IO4, TSC_IOCCR_G1_IO4_Msk //!<GROUP1_IO4 channel mode 
.equ TSC_IOCCR_G2_IO1_Pos, (4) 
.equ TSC_IOCCR_G2_IO1_Msk, (0x1 << TSC_IOCCR_G2_IO1_Pos) //!< 0x00000010 
.equ TSC_IOCCR_G2_IO1, TSC_IOCCR_G2_IO1_Msk //!<GROUP2_IO1 channel mode 
.equ TSC_IOCCR_G2_IO2_Pos, (5) 
.equ TSC_IOCCR_G2_IO2_Msk, (0x1 << TSC_IOCCR_G2_IO2_Pos) //!< 0x00000020 
.equ TSC_IOCCR_G2_IO2, TSC_IOCCR_G2_IO2_Msk //!<GROUP2_IO2 channel mode 
.equ TSC_IOCCR_G2_IO3_Pos, (6) 
.equ TSC_IOCCR_G2_IO3_Msk, (0x1 << TSC_IOCCR_G2_IO3_Pos) //!< 0x00000040 
.equ TSC_IOCCR_G2_IO3, TSC_IOCCR_G2_IO3_Msk //!<GROUP2_IO3 channel mode 
.equ TSC_IOCCR_G2_IO4_Pos, (7) 
.equ TSC_IOCCR_G2_IO4_Msk, (0x1 << TSC_IOCCR_G2_IO4_Pos) //!< 0x00000080 
.equ TSC_IOCCR_G2_IO4, TSC_IOCCR_G2_IO4_Msk //!<GROUP2_IO4 channel mode 
.equ TSC_IOCCR_G3_IO1_Pos, (8) 
.equ TSC_IOCCR_G3_IO1_Msk, (0x1 << TSC_IOCCR_G3_IO1_Pos) //!< 0x00000100 
.equ TSC_IOCCR_G3_IO1, TSC_IOCCR_G3_IO1_Msk //!<GROUP3_IO1 channel mode 
.equ TSC_IOCCR_G3_IO2_Pos, (9) 
.equ TSC_IOCCR_G3_IO2_Msk, (0x1 << TSC_IOCCR_G3_IO2_Pos) //!< 0x00000200 
.equ TSC_IOCCR_G3_IO2, TSC_IOCCR_G3_IO2_Msk //!<GROUP3_IO2 channel mode 
.equ TSC_IOCCR_G3_IO3_Pos, (10) 
.equ TSC_IOCCR_G3_IO3_Msk, (0x1 << TSC_IOCCR_G3_IO3_Pos) //!< 0x00000400 
.equ TSC_IOCCR_G3_IO3, TSC_IOCCR_G3_IO3_Msk //!<GROUP3_IO3 channel mode 
.equ TSC_IOCCR_G3_IO4_Pos, (11) 
.equ TSC_IOCCR_G3_IO4_Msk, (0x1 << TSC_IOCCR_G3_IO4_Pos) //!< 0x00000800 
.equ TSC_IOCCR_G3_IO4, TSC_IOCCR_G3_IO4_Msk //!<GROUP3_IO4 channel mode 
.equ TSC_IOCCR_G4_IO1_Pos, (12) 
.equ TSC_IOCCR_G4_IO1_Msk, (0x1 << TSC_IOCCR_G4_IO1_Pos) //!< 0x00001000 
.equ TSC_IOCCR_G4_IO1, TSC_IOCCR_G4_IO1_Msk //!<GROUP4_IO1 channel mode 
.equ TSC_IOCCR_G4_IO2_Pos, (13) 
.equ TSC_IOCCR_G4_IO2_Msk, (0x1 << TSC_IOCCR_G4_IO2_Pos) //!< 0x00002000 
.equ TSC_IOCCR_G4_IO2, TSC_IOCCR_G4_IO2_Msk //!<GROUP4_IO2 channel mode 
.equ TSC_IOCCR_G4_IO3_Pos, (14) 
.equ TSC_IOCCR_G4_IO3_Msk, (0x1 << TSC_IOCCR_G4_IO3_Pos) //!< 0x00004000 
.equ TSC_IOCCR_G4_IO3, TSC_IOCCR_G4_IO3_Msk //!<GROUP4_IO3 channel mode 
.equ TSC_IOCCR_G4_IO4_Pos, (15) 
.equ TSC_IOCCR_G4_IO4_Msk, (0x1 << TSC_IOCCR_G4_IO4_Pos) //!< 0x00008000 
.equ TSC_IOCCR_G4_IO4, TSC_IOCCR_G4_IO4_Msk //!<GROUP4_IO4 channel mode 
.equ TSC_IOCCR_G5_IO1_Pos, (16) 
.equ TSC_IOCCR_G5_IO1_Msk, (0x1 << TSC_IOCCR_G5_IO1_Pos) //!< 0x00010000 
.equ TSC_IOCCR_G5_IO1, TSC_IOCCR_G5_IO1_Msk //!<GROUP5_IO1 channel mode 
.equ TSC_IOCCR_G5_IO2_Pos, (17) 
.equ TSC_IOCCR_G5_IO2_Msk, (0x1 << TSC_IOCCR_G5_IO2_Pos) //!< 0x00020000 
.equ TSC_IOCCR_G5_IO2, TSC_IOCCR_G5_IO2_Msk //!<GROUP5_IO2 channel mode 
.equ TSC_IOCCR_G5_IO3_Pos, (18) 
.equ TSC_IOCCR_G5_IO3_Msk, (0x1 << TSC_IOCCR_G5_IO3_Pos) //!< 0x00040000 
.equ TSC_IOCCR_G5_IO3, TSC_IOCCR_G5_IO3_Msk //!<GROUP5_IO3 channel mode 
.equ TSC_IOCCR_G5_IO4_Pos, (19) 
.equ TSC_IOCCR_G5_IO4_Msk, (0x1 << TSC_IOCCR_G5_IO4_Pos) //!< 0x00080000 
.equ TSC_IOCCR_G5_IO4, TSC_IOCCR_G5_IO4_Msk //!<GROUP5_IO4 channel mode 
.equ TSC_IOCCR_G6_IO1_Pos, (20) 
.equ TSC_IOCCR_G6_IO1_Msk, (0x1 << TSC_IOCCR_G6_IO1_Pos) //!< 0x00100000 
.equ TSC_IOCCR_G6_IO1, TSC_IOCCR_G6_IO1_Msk //!<GROUP6_IO1 channel mode 
.equ TSC_IOCCR_G6_IO2_Pos, (21) 
.equ TSC_IOCCR_G6_IO2_Msk, (0x1 << TSC_IOCCR_G6_IO2_Pos) //!< 0x00200000 
.equ TSC_IOCCR_G6_IO2, TSC_IOCCR_G6_IO2_Msk //!<GROUP6_IO2 channel mode 
.equ TSC_IOCCR_G6_IO3_Pos, (22) 
.equ TSC_IOCCR_G6_IO3_Msk, (0x1 << TSC_IOCCR_G6_IO3_Pos) //!< 0x00400000 
.equ TSC_IOCCR_G6_IO3, TSC_IOCCR_G6_IO3_Msk //!<GROUP6_IO3 channel mode 
.equ TSC_IOCCR_G6_IO4_Pos, (23) 
.equ TSC_IOCCR_G6_IO4_Msk, (0x1 << TSC_IOCCR_G6_IO4_Pos) //!< 0x00800000 
.equ TSC_IOCCR_G6_IO4, TSC_IOCCR_G6_IO4_Msk //!<GROUP6_IO4 channel mode 
.equ TSC_IOCCR_G7_IO1_Pos, (24) 
.equ TSC_IOCCR_G7_IO1_Msk, (0x1 << TSC_IOCCR_G7_IO1_Pos) //!< 0x01000000 
.equ TSC_IOCCR_G7_IO1, TSC_IOCCR_G7_IO1_Msk //!<GROUP7_IO1 channel mode 
.equ TSC_IOCCR_G7_IO2_Pos, (25) 
.equ TSC_IOCCR_G7_IO2_Msk, (0x1 << TSC_IOCCR_G7_IO2_Pos) //!< 0x02000000 
.equ TSC_IOCCR_G7_IO2, TSC_IOCCR_G7_IO2_Msk //!<GROUP7_IO2 channel mode 
.equ TSC_IOCCR_G7_IO3_Pos, (26) 
.equ TSC_IOCCR_G7_IO3_Msk, (0x1 << TSC_IOCCR_G7_IO3_Pos) //!< 0x04000000 
.equ TSC_IOCCR_G7_IO3, TSC_IOCCR_G7_IO3_Msk //!<GROUP7_IO3 channel mode 
.equ TSC_IOCCR_G7_IO4_Pos, (27) 
.equ TSC_IOCCR_G7_IO4_Msk, (0x1 << TSC_IOCCR_G7_IO4_Pos) //!< 0x08000000 
.equ TSC_IOCCR_G7_IO4, TSC_IOCCR_G7_IO4_Msk //!<GROUP7_IO4 channel mode 
.equ TSC_IOCCR_G8_IO1_Pos, (28) 
.equ TSC_IOCCR_G8_IO1_Msk, (0x1 << TSC_IOCCR_G8_IO1_Pos) //!< 0x10000000 
.equ TSC_IOCCR_G8_IO1, TSC_IOCCR_G8_IO1_Msk //!<GROUP8_IO1 channel mode 
.equ TSC_IOCCR_G8_IO2_Pos, (29) 
.equ TSC_IOCCR_G8_IO2_Msk, (0x1 << TSC_IOCCR_G8_IO2_Pos) //!< 0x20000000 
.equ TSC_IOCCR_G8_IO2, TSC_IOCCR_G8_IO2_Msk //!<GROUP8_IO2 channel mode 
.equ TSC_IOCCR_G8_IO3_Pos, (30) 
.equ TSC_IOCCR_G8_IO3_Msk, (0x1 << TSC_IOCCR_G8_IO3_Pos) //!< 0x40000000 
.equ TSC_IOCCR_G8_IO3, TSC_IOCCR_G8_IO3_Msk //!<GROUP8_IO3 channel mode 
.equ TSC_IOCCR_G8_IO4_Pos, (31) 
.equ TSC_IOCCR_G8_IO4_Msk, (0x1 << TSC_IOCCR_G8_IO4_Pos) //!< 0x80000000 
.equ TSC_IOCCR_G8_IO4, TSC_IOCCR_G8_IO4_Msk //!<GROUP8_IO4 channel mode 
//******************  Bit definition for TSC_IOGCSR register  ****************
.equ TSC_IOGCSR_G1E_Pos, (0) 
.equ TSC_IOGCSR_G1E_Msk, (0x1 << TSC_IOGCSR_G1E_Pos) //!< 0x00000001 
.equ TSC_IOGCSR_G1E, TSC_IOGCSR_G1E_Msk //!<Analog IO GROUP1 enable 
.equ TSC_IOGCSR_G2E_Pos, (1) 
.equ TSC_IOGCSR_G2E_Msk, (0x1 << TSC_IOGCSR_G2E_Pos) //!< 0x00000002 
.equ TSC_IOGCSR_G2E, TSC_IOGCSR_G2E_Msk //!<Analog IO GROUP2 enable 
.equ TSC_IOGCSR_G3E_Pos, (2) 
.equ TSC_IOGCSR_G3E_Msk, (0x1 << TSC_IOGCSR_G3E_Pos) //!< 0x00000004 
.equ TSC_IOGCSR_G3E, TSC_IOGCSR_G3E_Msk //!<Analog IO GROUP3 enable 
.equ TSC_IOGCSR_G4E_Pos, (3) 
.equ TSC_IOGCSR_G4E_Msk, (0x1 << TSC_IOGCSR_G4E_Pos) //!< 0x00000008 
.equ TSC_IOGCSR_G4E, TSC_IOGCSR_G4E_Msk //!<Analog IO GROUP4 enable 
.equ TSC_IOGCSR_G5E_Pos, (4) 
.equ TSC_IOGCSR_G5E_Msk, (0x1 << TSC_IOGCSR_G5E_Pos) //!< 0x00000010 
.equ TSC_IOGCSR_G5E, TSC_IOGCSR_G5E_Msk //!<Analog IO GROUP5 enable 
.equ TSC_IOGCSR_G6E_Pos, (5) 
.equ TSC_IOGCSR_G6E_Msk, (0x1 << TSC_IOGCSR_G6E_Pos) //!< 0x00000020 
.equ TSC_IOGCSR_G6E, TSC_IOGCSR_G6E_Msk //!<Analog IO GROUP6 enable 
.equ TSC_IOGCSR_G7E_Pos, (6) 
.equ TSC_IOGCSR_G7E_Msk, (0x1 << TSC_IOGCSR_G7E_Pos) //!< 0x00000040 
.equ TSC_IOGCSR_G7E, TSC_IOGCSR_G7E_Msk //!<Analog IO GROUP7 enable 
.equ TSC_IOGCSR_G8E_Pos, (7) 
.equ TSC_IOGCSR_G8E_Msk, (0x1 << TSC_IOGCSR_G8E_Pos) //!< 0x00000080 
.equ TSC_IOGCSR_G8E, TSC_IOGCSR_G8E_Msk //!<Analog IO GROUP8 enable 
.equ TSC_IOGCSR_G1S_Pos, (16) 
.equ TSC_IOGCSR_G1S_Msk, (0x1 << TSC_IOGCSR_G1S_Pos) //!< 0x00010000 
.equ TSC_IOGCSR_G1S, TSC_IOGCSR_G1S_Msk //!<Analog IO GROUP1 status 
.equ TSC_IOGCSR_G2S_Pos, (17) 
.equ TSC_IOGCSR_G2S_Msk, (0x1 << TSC_IOGCSR_G2S_Pos) //!< 0x00020000 
.equ TSC_IOGCSR_G2S, TSC_IOGCSR_G2S_Msk //!<Analog IO GROUP2 status 
.equ TSC_IOGCSR_G3S_Pos, (18) 
.equ TSC_IOGCSR_G3S_Msk, (0x1 << TSC_IOGCSR_G3S_Pos) //!< 0x00040000 
.equ TSC_IOGCSR_G3S, TSC_IOGCSR_G3S_Msk //!<Analog IO GROUP3 status 
.equ TSC_IOGCSR_G4S_Pos, (19) 
.equ TSC_IOGCSR_G4S_Msk, (0x1 << TSC_IOGCSR_G4S_Pos) //!< 0x00080000 
.equ TSC_IOGCSR_G4S, TSC_IOGCSR_G4S_Msk //!<Analog IO GROUP4 status 
.equ TSC_IOGCSR_G5S_Pos, (20) 
.equ TSC_IOGCSR_G5S_Msk, (0x1 << TSC_IOGCSR_G5S_Pos) //!< 0x00100000 
.equ TSC_IOGCSR_G5S, TSC_IOGCSR_G5S_Msk //!<Analog IO GROUP5 status 
.equ TSC_IOGCSR_G6S_Pos, (21) 
.equ TSC_IOGCSR_G6S_Msk, (0x1 << TSC_IOGCSR_G6S_Pos) //!< 0x00200000 
.equ TSC_IOGCSR_G6S, TSC_IOGCSR_G6S_Msk //!<Analog IO GROUP6 status 
.equ TSC_IOGCSR_G7S_Pos, (22) 
.equ TSC_IOGCSR_G7S_Msk, (0x1 << TSC_IOGCSR_G7S_Pos) //!< 0x00400000 
.equ TSC_IOGCSR_G7S, TSC_IOGCSR_G7S_Msk //!<Analog IO GROUP7 status 
.equ TSC_IOGCSR_G8S_Pos, (23) 
.equ TSC_IOGCSR_G8S_Msk, (0x1 << TSC_IOGCSR_G8S_Pos) //!< 0x00800000 
.equ TSC_IOGCSR_G8S, TSC_IOGCSR_G8S_Msk //!<Analog IO GROUP8 status 
//******************  Bit definition for TSC_IOGXCR register  ****************
.equ TSC_IOGXCR_CNT_Pos, (0) 
.equ TSC_IOGXCR_CNT_Msk, (0x3FFF << TSC_IOGXCR_CNT_Pos) //!< 0x00003FFF 
.equ TSC_IOGXCR_CNT, TSC_IOGXCR_CNT_Msk //!<CNT[13:0] bits (Counter value) 
//****************************************************************************
//
//      Universal Synchronous Asynchronous Receiver Transmitter (USART)
//
//****************************************************************************
//*****************  Bit definition for USART_CR1 register  ******************
.equ USART_CR1_UE_Pos, (0) 
.equ USART_CR1_UE_Msk, (0x1 << USART_CR1_UE_Pos) //!< 0x00000001 
.equ USART_CR1_UE, USART_CR1_UE_Msk //!< USART Enable 
.equ USART_CR1_UESM_Pos, (1) 
.equ USART_CR1_UESM_Msk, (0x1 << USART_CR1_UESM_Pos) //!< 0x00000002 
.equ USART_CR1_UESM, USART_CR1_UESM_Msk //!< USART Enable in STOP Mode 
.equ USART_CR1_RE_Pos, (2) 
.equ USART_CR1_RE_Msk, (0x1 << USART_CR1_RE_Pos) //!< 0x00000004 
.equ USART_CR1_RE, USART_CR1_RE_Msk //!< Receiver Enable 
.equ USART_CR1_TE_Pos, (3) 
.equ USART_CR1_TE_Msk, (0x1 << USART_CR1_TE_Pos) //!< 0x00000008 
.equ USART_CR1_TE, USART_CR1_TE_Msk //!< Transmitter Enable 
.equ USART_CR1_IDLEIE_Pos, (4) 
.equ USART_CR1_IDLEIE_Msk, (0x1 << USART_CR1_IDLEIE_Pos) //!< 0x00000010 
.equ USART_CR1_IDLEIE, USART_CR1_IDLEIE_Msk //!< IDLE Interrupt Enable 
.equ USART_CR1_RXNEIE_Pos, (5) 
.equ USART_CR1_RXNEIE_Msk, (0x1 << USART_CR1_RXNEIE_Pos) //!< 0x00000020 
.equ USART_CR1_RXNEIE, USART_CR1_RXNEIE_Msk //!< RXNE Interrupt Enable 
.equ USART_CR1_TCIE_Pos, (6) 
.equ USART_CR1_TCIE_Msk, (0x1 << USART_CR1_TCIE_Pos) //!< 0x00000040 
.equ USART_CR1_TCIE, USART_CR1_TCIE_Msk //!< Transmission Complete Interrupt Enable 
.equ USART_CR1_TXEIE_Pos, (7) 
.equ USART_CR1_TXEIE_Msk, (0x1 << USART_CR1_TXEIE_Pos) //!< 0x00000080 
.equ USART_CR1_TXEIE, USART_CR1_TXEIE_Msk //!< TXE Interrupt Enable 
.equ USART_CR1_PEIE_Pos, (8) 
.equ USART_CR1_PEIE_Msk, (0x1 << USART_CR1_PEIE_Pos) //!< 0x00000100 
.equ USART_CR1_PEIE, USART_CR1_PEIE_Msk //!< PE Interrupt Enable 
.equ USART_CR1_PS_Pos, (9) 
.equ USART_CR1_PS_Msk, (0x1 << USART_CR1_PS_Pos) //!< 0x00000200 
.equ USART_CR1_PS, USART_CR1_PS_Msk //!< Parity Selection 
.equ USART_CR1_PCE_Pos, (10) 
.equ USART_CR1_PCE_Msk, (0x1 << USART_CR1_PCE_Pos) //!< 0x00000400 
.equ USART_CR1_PCE, USART_CR1_PCE_Msk //!< Parity Control Enable 
.equ USART_CR1_WAKE_Pos, (11) 
.equ USART_CR1_WAKE_Msk, (0x1 << USART_CR1_WAKE_Pos) //!< 0x00000800 
.equ USART_CR1_WAKE, USART_CR1_WAKE_Msk //!< Receiver Wakeup method 
.equ USART_CR1_M0_Pos, (12) 
.equ USART_CR1_M0_Msk, (0x1 << USART_CR1_M0_Pos) //!< 0x00001000 
.equ USART_CR1_M0, USART_CR1_M0_Msk //!< Word length bit 0 
.equ USART_CR1_MME_Pos, (13) 
.equ USART_CR1_MME_Msk, (0x1 << USART_CR1_MME_Pos) //!< 0x00002000 
.equ USART_CR1_MME, USART_CR1_MME_Msk //!< Mute Mode Enable 
.equ USART_CR1_CMIE_Pos, (14) 
.equ USART_CR1_CMIE_Msk, (0x1 << USART_CR1_CMIE_Pos) //!< 0x00004000 
.equ USART_CR1_CMIE, USART_CR1_CMIE_Msk //!< Character match interrupt enable 
.equ USART_CR1_OVER8_Pos, (15) 
.equ USART_CR1_OVER8_Msk, (0x1 << USART_CR1_OVER8_Pos) //!< 0x00008000 
.equ USART_CR1_OVER8, USART_CR1_OVER8_Msk //!< Oversampling by 8-bit or 16-bit mode 
.equ USART_CR1_DEDT_Pos, (16) 
.equ USART_CR1_DEDT_Msk, (0x1F << USART_CR1_DEDT_Pos) //!< 0x001F0000 
.equ USART_CR1_DEDT, USART_CR1_DEDT_Msk //!< DEDT[4:0] bits (Driver Enable Deassertion Time) 
.equ USART_CR1_DEDT_0, (0x01 << USART_CR1_DEDT_Pos) //!< 0x00010000 
.equ USART_CR1_DEDT_1, (0x02 << USART_CR1_DEDT_Pos) //!< 0x00020000 
.equ USART_CR1_DEDT_2, (0x04 << USART_CR1_DEDT_Pos) //!< 0x00040000 
.equ USART_CR1_DEDT_3, (0x08 << USART_CR1_DEDT_Pos) //!< 0x00080000 
.equ USART_CR1_DEDT_4, (0x10 << USART_CR1_DEDT_Pos) //!< 0x00100000 
.equ USART_CR1_DEAT_Pos, (21) 
.equ USART_CR1_DEAT_Msk, (0x1F << USART_CR1_DEAT_Pos) //!< 0x03E00000 
.equ USART_CR1_DEAT, USART_CR1_DEAT_Msk //!< DEAT[4:0] bits (Driver Enable Assertion Time) 
.equ USART_CR1_DEAT_0, (0x01 << USART_CR1_DEAT_Pos) //!< 0x00200000 
.equ USART_CR1_DEAT_1, (0x02 << USART_CR1_DEAT_Pos) //!< 0x00400000 
.equ USART_CR1_DEAT_2, (0x04 << USART_CR1_DEAT_Pos) //!< 0x00800000 
.equ USART_CR1_DEAT_3, (0x08 << USART_CR1_DEAT_Pos) //!< 0x01000000 
.equ USART_CR1_DEAT_4, (0x10 << USART_CR1_DEAT_Pos) //!< 0x02000000 
.equ USART_CR1_RTOIE_Pos, (26) 
.equ USART_CR1_RTOIE_Msk, (0x1 << USART_CR1_RTOIE_Pos) //!< 0x04000000 
.equ USART_CR1_RTOIE, USART_CR1_RTOIE_Msk //!< Receive Time Out interrupt enable 
.equ USART_CR1_EOBIE_Pos, (27) 
.equ USART_CR1_EOBIE_Msk, (0x1 << USART_CR1_EOBIE_Pos) //!< 0x08000000 
.equ USART_CR1_EOBIE, USART_CR1_EOBIE_Msk //!< End of Block interrupt enable 
.equ USART_CR1_M1_Pos, (28) 
.equ USART_CR1_M1_Msk, (0x1 << USART_CR1_M1_Pos) //!< 0x10000000 
.equ USART_CR1_M1, USART_CR1_M1_Msk //!< Word length bit 1 
.equ USART_CR1_M_Pos, (12) 
.equ USART_CR1_M_Msk, (0x10001 << USART_CR1_M_Pos) //!< 0x10001000 
.equ USART_CR1_M, USART_CR1_M_Msk //!< [M1:M0] Word length 
//*****************  Bit definition for USART_CR2 register  ******************
.equ USART_CR2_ADDM7_Pos, (4) 
.equ USART_CR2_ADDM7_Msk, (0x1 << USART_CR2_ADDM7_Pos) //!< 0x00000010 
.equ USART_CR2_ADDM7, USART_CR2_ADDM7_Msk //!< 7-bit or 4-bit Address Detection 
.equ USART_CR2_LBDL_Pos, (5) 
.equ USART_CR2_LBDL_Msk, (0x1 << USART_CR2_LBDL_Pos) //!< 0x00000020 
.equ USART_CR2_LBDL, USART_CR2_LBDL_Msk //!< LIN Break Detection Length 
.equ USART_CR2_LBDIE_Pos, (6) 
.equ USART_CR2_LBDIE_Msk, (0x1 << USART_CR2_LBDIE_Pos) //!< 0x00000040 
.equ USART_CR2_LBDIE, USART_CR2_LBDIE_Msk //!< LIN Break Detection Interrupt Enable 
.equ USART_CR2_LBCL_Pos, (8) 
.equ USART_CR2_LBCL_Msk, (0x1 << USART_CR2_LBCL_Pos) //!< 0x00000100 
.equ USART_CR2_LBCL, USART_CR2_LBCL_Msk //!< Last Bit Clock pulse 
.equ USART_CR2_CPHA_Pos, (9) 
.equ USART_CR2_CPHA_Msk, (0x1 << USART_CR2_CPHA_Pos) //!< 0x00000200 
.equ USART_CR2_CPHA, USART_CR2_CPHA_Msk //!< Clock Phase 
.equ USART_CR2_CPOL_Pos, (10) 
.equ USART_CR2_CPOL_Msk, (0x1 << USART_CR2_CPOL_Pos) //!< 0x00000400 
.equ USART_CR2_CPOL, USART_CR2_CPOL_Msk //!< Clock Polarity 
.equ USART_CR2_CLKEN_Pos, (11) 
.equ USART_CR2_CLKEN_Msk, (0x1 << USART_CR2_CLKEN_Pos) //!< 0x00000800 
.equ USART_CR2_CLKEN, USART_CR2_CLKEN_Msk //!< Clock Enable 
.equ USART_CR2_STOP_Pos, (12) 
.equ USART_CR2_STOP_Msk, (0x3 << USART_CR2_STOP_Pos) //!< 0x00003000 
.equ USART_CR2_STOP, USART_CR2_STOP_Msk //!< STOP[1:0] bits (STOP bits) 
.equ USART_CR2_STOP_0, (0x1 << USART_CR2_STOP_Pos) //!< 0x00001000 
.equ USART_CR2_STOP_1, (0x2 << USART_CR2_STOP_Pos) //!< 0x00002000 
.equ USART_CR2_LINEN_Pos, (14) 
.equ USART_CR2_LINEN_Msk, (0x1 << USART_CR2_LINEN_Pos) //!< 0x00004000 
.equ USART_CR2_LINEN, USART_CR2_LINEN_Msk //!< LIN mode enable 
.equ USART_CR2_SWAP_Pos, (15) 
.equ USART_CR2_SWAP_Msk, (0x1 << USART_CR2_SWAP_Pos) //!< 0x00008000 
.equ USART_CR2_SWAP, USART_CR2_SWAP_Msk //!< SWAP TX/RX pins 
.equ USART_CR2_RXINV_Pos, (16) 
.equ USART_CR2_RXINV_Msk, (0x1 << USART_CR2_RXINV_Pos) //!< 0x00010000 
.equ USART_CR2_RXINV, USART_CR2_RXINV_Msk //!< RX pin active level inversion 
.equ USART_CR2_TXINV_Pos, (17) 
.equ USART_CR2_TXINV_Msk, (0x1 << USART_CR2_TXINV_Pos) //!< 0x00020000 
.equ USART_CR2_TXINV, USART_CR2_TXINV_Msk //!< TX pin active level inversion 
.equ USART_CR2_DATAINV_Pos, (18) 
.equ USART_CR2_DATAINV_Msk, (0x1 << USART_CR2_DATAINV_Pos) //!< 0x00040000 
.equ USART_CR2_DATAINV, USART_CR2_DATAINV_Msk //!< Binary data inversion 
.equ USART_CR2_MSBFIRST_Pos, (19) 
.equ USART_CR2_MSBFIRST_Msk, (0x1 << USART_CR2_MSBFIRST_Pos) //!< 0x00080000 
.equ USART_CR2_MSBFIRST, USART_CR2_MSBFIRST_Msk //!< Most Significant Bit First 
.equ USART_CR2_ABREN_Pos, (20) 
.equ USART_CR2_ABREN_Msk, (0x1 << USART_CR2_ABREN_Pos) //!< 0x00100000 
.equ USART_CR2_ABREN, USART_CR2_ABREN_Msk //!< Auto Baud-Rate Enable 
.equ USART_CR2_ABRMODE_Pos, (21) 
.equ USART_CR2_ABRMODE_Msk, (0x3 << USART_CR2_ABRMODE_Pos) //!< 0x00600000 
.equ USART_CR2_ABRMODE, USART_CR2_ABRMODE_Msk //!< ABRMOD[1:0] bits (Auto Baud-Rate Mode) 
.equ USART_CR2_ABRMODE_0, (0x1 << USART_CR2_ABRMODE_Pos) //!< 0x00200000 
.equ USART_CR2_ABRMODE_1, (0x2 << USART_CR2_ABRMODE_Pos) //!< 0x00400000 
.equ USART_CR2_RTOEN_Pos, (23) 
.equ USART_CR2_RTOEN_Msk, (0x1 << USART_CR2_RTOEN_Pos) //!< 0x00800000 
.equ USART_CR2_RTOEN, USART_CR2_RTOEN_Msk //!< Receiver Time-Out enable 
.equ USART_CR2_ADD_Pos, (24) 
.equ USART_CR2_ADD_Msk, (0xFF << USART_CR2_ADD_Pos) //!< 0xFF000000 
.equ USART_CR2_ADD, USART_CR2_ADD_Msk //!< Address of the USART node 
//*****************  Bit definition for USART_CR3 register  ******************
.equ USART_CR3_EIE_Pos, (0) 
.equ USART_CR3_EIE_Msk, (0x1 << USART_CR3_EIE_Pos) //!< 0x00000001 
.equ USART_CR3_EIE, USART_CR3_EIE_Msk //!< Error Interrupt Enable 
.equ USART_CR3_IREN_Pos, (1) 
.equ USART_CR3_IREN_Msk, (0x1 << USART_CR3_IREN_Pos) //!< 0x00000002 
.equ USART_CR3_IREN, USART_CR3_IREN_Msk //!< IrDA mode Enable 
.equ USART_CR3_IRLP_Pos, (2) 
.equ USART_CR3_IRLP_Msk, (0x1 << USART_CR3_IRLP_Pos) //!< 0x00000004 
.equ USART_CR3_IRLP, USART_CR3_IRLP_Msk //!< IrDA Low-Power 
.equ USART_CR3_HDSEL_Pos, (3) 
.equ USART_CR3_HDSEL_Msk, (0x1 << USART_CR3_HDSEL_Pos) //!< 0x00000008 
.equ USART_CR3_HDSEL, USART_CR3_HDSEL_Msk //!< Half-Duplex Selection 
.equ USART_CR3_NACK_Pos, (4) 
.equ USART_CR3_NACK_Msk, (0x1 << USART_CR3_NACK_Pos) //!< 0x00000010 
.equ USART_CR3_NACK, USART_CR3_NACK_Msk //!< SmartCard NACK enable 
.equ USART_CR3_SCEN_Pos, (5) 
.equ USART_CR3_SCEN_Msk, (0x1 << USART_CR3_SCEN_Pos) //!< 0x00000020 
.equ USART_CR3_SCEN, USART_CR3_SCEN_Msk //!< SmartCard mode enable 
.equ USART_CR3_DMAR_Pos, (6) 
.equ USART_CR3_DMAR_Msk, (0x1 << USART_CR3_DMAR_Pos) //!< 0x00000040 
.equ USART_CR3_DMAR, USART_CR3_DMAR_Msk //!< DMA Enable Receiver 
.equ USART_CR3_DMAT_Pos, (7) 
.equ USART_CR3_DMAT_Msk, (0x1 << USART_CR3_DMAT_Pos) //!< 0x00000080 
.equ USART_CR3_DMAT, USART_CR3_DMAT_Msk //!< DMA Enable Transmitter 
.equ USART_CR3_RTSE_Pos, (8) 
.equ USART_CR3_RTSE_Msk, (0x1 << USART_CR3_RTSE_Pos) //!< 0x00000100 
.equ USART_CR3_RTSE, USART_CR3_RTSE_Msk //!< RTS Enable 
.equ USART_CR3_CTSE_Pos, (9) 
.equ USART_CR3_CTSE_Msk, (0x1 << USART_CR3_CTSE_Pos) //!< 0x00000200 
.equ USART_CR3_CTSE, USART_CR3_CTSE_Msk //!< CTS Enable 
.equ USART_CR3_CTSIE_Pos, (10) 
.equ USART_CR3_CTSIE_Msk, (0x1 << USART_CR3_CTSIE_Pos) //!< 0x00000400 
.equ USART_CR3_CTSIE, USART_CR3_CTSIE_Msk //!< CTS Interrupt Enable 
.equ USART_CR3_ONEBIT_Pos, (11) 
.equ USART_CR3_ONEBIT_Msk, (0x1 << USART_CR3_ONEBIT_Pos) //!< 0x00000800 
.equ USART_CR3_ONEBIT, USART_CR3_ONEBIT_Msk //!< One sample bit method enable 
.equ USART_CR3_OVRDIS_Pos, (12) 
.equ USART_CR3_OVRDIS_Msk, (0x1 << USART_CR3_OVRDIS_Pos) //!< 0x00001000 
.equ USART_CR3_OVRDIS, USART_CR3_OVRDIS_Msk //!< Overrun Disable 
.equ USART_CR3_DDRE_Pos, (13) 
.equ USART_CR3_DDRE_Msk, (0x1 << USART_CR3_DDRE_Pos) //!< 0x00002000 
.equ USART_CR3_DDRE, USART_CR3_DDRE_Msk //!< DMA Disable on Reception Error 
.equ USART_CR3_DEM_Pos, (14) 
.equ USART_CR3_DEM_Msk, (0x1 << USART_CR3_DEM_Pos) //!< 0x00004000 
.equ USART_CR3_DEM, USART_CR3_DEM_Msk //!< Driver Enable Mode 
.equ USART_CR3_DEP_Pos, (15) 
.equ USART_CR3_DEP_Msk, (0x1 << USART_CR3_DEP_Pos) //!< 0x00008000 
.equ USART_CR3_DEP, USART_CR3_DEP_Msk //!< Driver Enable Polarity Selection 
.equ USART_CR3_SCARCNT_Pos, (17) 
.equ USART_CR3_SCARCNT_Msk, (0x7 << USART_CR3_SCARCNT_Pos) //!< 0x000E0000 
.equ USART_CR3_SCARCNT, USART_CR3_SCARCNT_Msk //!< SCARCNT[2:0] bits (SmartCard Auto-Retry Count) 
.equ USART_CR3_SCARCNT_0, (0x1 << USART_CR3_SCARCNT_Pos) //!< 0x00020000 
.equ USART_CR3_SCARCNT_1, (0x2 << USART_CR3_SCARCNT_Pos) //!< 0x00040000 
.equ USART_CR3_SCARCNT_2, (0x4 << USART_CR3_SCARCNT_Pos) //!< 0x00080000 
.equ USART_CR3_WUS_Pos, (20) 
.equ USART_CR3_WUS_Msk, (0x3 << USART_CR3_WUS_Pos) //!< 0x00300000 
.equ USART_CR3_WUS, USART_CR3_WUS_Msk //!< WUS[1:0] bits (Wake UP Interrupt Flag Selection) 
.equ USART_CR3_WUS_0, (0x1 << USART_CR3_WUS_Pos) //!< 0x00100000 
.equ USART_CR3_WUS_1, (0x2 << USART_CR3_WUS_Pos) //!< 0x00200000 
.equ USART_CR3_WUFIE_Pos, (22) 
.equ USART_CR3_WUFIE_Msk, (0x1 << USART_CR3_WUFIE_Pos) //!< 0x00400000 
.equ USART_CR3_WUFIE, USART_CR3_WUFIE_Msk //!< Wake Up Interrupt Enable 
//*****************  Bit definition for USART_BRR register  ******************
.equ USART_BRR_DIV_FRACTION_Pos, (0) 
.equ USART_BRR_DIV_FRACTION_Msk, (0xF << USART_BRR_DIV_FRACTION_Pos) //!< 0x0000000F 
.equ USART_BRR_DIV_FRACTION, USART_BRR_DIV_FRACTION_Msk //!< Fraction of USARTDIV 
.equ USART_BRR_DIV_MANTISSA_Pos, (4) 
.equ USART_BRR_DIV_MANTISSA_Msk, (0xFFF << USART_BRR_DIV_MANTISSA_Pos) //!< 0x0000FFF0 
.equ USART_BRR_DIV_MANTISSA, USART_BRR_DIV_MANTISSA_Msk //!< Mantissa of USARTDIV 
//*****************  Bit definition for USART_GTPR register  *****************
.equ USART_GTPR_PSC_Pos, (0) 
.equ USART_GTPR_PSC_Msk, (0xFF << USART_GTPR_PSC_Pos) //!< 0x000000FF 
.equ USART_GTPR_PSC, USART_GTPR_PSC_Msk //!< PSC[7:0] bits (Prescaler value) 
.equ USART_GTPR_GT_Pos, (8) 
.equ USART_GTPR_GT_Msk, (0xFF << USART_GTPR_GT_Pos) //!< 0x0000FF00 
.equ USART_GTPR_GT, USART_GTPR_GT_Msk //!< GT[7:0] bits (Guard time value) 
//******************  Bit definition for USART_RTOR register  ****************
.equ USART_RTOR_RTO_Pos, (0) 
.equ USART_RTOR_RTO_Msk, (0xFFFFFF << USART_RTOR_RTO_Pos) //!< 0x00FFFFFF 
.equ USART_RTOR_RTO, USART_RTOR_RTO_Msk //!< Receiver Time Out Value 
.equ USART_RTOR_BLEN_Pos, (24) 
.equ USART_RTOR_BLEN_Msk, (0xFF << USART_RTOR_BLEN_Pos) //!< 0xFF000000 
.equ USART_RTOR_BLEN, USART_RTOR_BLEN_Msk //!< Block Length 
//******************  Bit definition for USART_RQR register  *****************
.equ USART_RQR_ABRRQ_Pos, (0) 
.equ USART_RQR_ABRRQ_Msk, (0x1 << USART_RQR_ABRRQ_Pos) //!< 0x00000001 
.equ USART_RQR_ABRRQ, USART_RQR_ABRRQ_Msk //!< Auto-Baud Rate Request 
.equ USART_RQR_SBKRQ_Pos, (1) 
.equ USART_RQR_SBKRQ_Msk, (0x1 << USART_RQR_SBKRQ_Pos) //!< 0x00000002 
.equ USART_RQR_SBKRQ, USART_RQR_SBKRQ_Msk //!< Send Break Request 
.equ USART_RQR_MMRQ_Pos, (2) 
.equ USART_RQR_MMRQ_Msk, (0x1 << USART_RQR_MMRQ_Pos) //!< 0x00000004 
.equ USART_RQR_MMRQ, USART_RQR_MMRQ_Msk //!< Mute Mode Request 
.equ USART_RQR_RXFRQ_Pos, (3) 
.equ USART_RQR_RXFRQ_Msk, (0x1 << USART_RQR_RXFRQ_Pos) //!< 0x00000008 
.equ USART_RQR_RXFRQ, USART_RQR_RXFRQ_Msk //!< Receive Data flush Request 
.equ USART_RQR_TXFRQ_Pos, (4) 
.equ USART_RQR_TXFRQ_Msk, (0x1 << USART_RQR_TXFRQ_Pos) //!< 0x00000010 
.equ USART_RQR_TXFRQ, USART_RQR_TXFRQ_Msk //!< Transmit data flush Request 
//******************  Bit definition for USART_ISR register  *****************
.equ USART_ISR_PE_Pos, (0) 
.equ USART_ISR_PE_Msk, (0x1 << USART_ISR_PE_Pos) //!< 0x00000001 
.equ USART_ISR_PE, USART_ISR_PE_Msk //!< Parity Error 
.equ USART_ISR_FE_Pos, (1) 
.equ USART_ISR_FE_Msk, (0x1 << USART_ISR_FE_Pos) //!< 0x00000002 
.equ USART_ISR_FE, USART_ISR_FE_Msk //!< Framing Error 
.equ USART_ISR_NE_Pos, (2) 
.equ USART_ISR_NE_Msk, (0x1 << USART_ISR_NE_Pos) //!< 0x00000004 
.equ USART_ISR_NE, USART_ISR_NE_Msk //!< Noise detected Flag 
.equ USART_ISR_ORE_Pos, (3) 
.equ USART_ISR_ORE_Msk, (0x1 << USART_ISR_ORE_Pos) //!< 0x00000008 
.equ USART_ISR_ORE, USART_ISR_ORE_Msk //!< OverRun Error 
.equ USART_ISR_IDLE_Pos, (4) 
.equ USART_ISR_IDLE_Msk, (0x1 << USART_ISR_IDLE_Pos) //!< 0x00000010 
.equ USART_ISR_IDLE, USART_ISR_IDLE_Msk //!< IDLE line detected 
.equ USART_ISR_RXNE_Pos, (5) 
.equ USART_ISR_RXNE_Msk, (0x1 << USART_ISR_RXNE_Pos) //!< 0x00000020 
.equ USART_ISR_RXNE, USART_ISR_RXNE_Msk //!< Read Data Register Not Empty 
.equ USART_ISR_TC_Pos, (6) 
.equ USART_ISR_TC_Msk, (0x1 << USART_ISR_TC_Pos) //!< 0x00000040 
.equ USART_ISR_TC, USART_ISR_TC_Msk //!< Transmission Complete 
.equ USART_ISR_TXE_Pos, (7) 
.equ USART_ISR_TXE_Msk, (0x1 << USART_ISR_TXE_Pos) //!< 0x00000080 
.equ USART_ISR_TXE, USART_ISR_TXE_Msk //!< Transmit Data Register Empty 
.equ USART_ISR_LBDF_Pos, (8) 
.equ USART_ISR_LBDF_Msk, (0x1 << USART_ISR_LBDF_Pos) //!< 0x00000100 
.equ USART_ISR_LBDF, USART_ISR_LBDF_Msk //!< LIN Break Detection Flag 
.equ USART_ISR_CTSIF_Pos, (9) 
.equ USART_ISR_CTSIF_Msk, (0x1 << USART_ISR_CTSIF_Pos) //!< 0x00000200 
.equ USART_ISR_CTSIF, USART_ISR_CTSIF_Msk //!< CTS interrupt flag 
.equ USART_ISR_CTS_Pos, (10) 
.equ USART_ISR_CTS_Msk, (0x1 << USART_ISR_CTS_Pos) //!< 0x00000400 
.equ USART_ISR_CTS, USART_ISR_CTS_Msk //!< CTS flag 
.equ USART_ISR_RTOF_Pos, (11) 
.equ USART_ISR_RTOF_Msk, (0x1 << USART_ISR_RTOF_Pos) //!< 0x00000800 
.equ USART_ISR_RTOF, USART_ISR_RTOF_Msk //!< Receiver Time Out 
.equ USART_ISR_EOBF_Pos, (12) 
.equ USART_ISR_EOBF_Msk, (0x1 << USART_ISR_EOBF_Pos) //!< 0x00001000 
.equ USART_ISR_EOBF, USART_ISR_EOBF_Msk //!< End Of Block Flag 
.equ USART_ISR_ABRE_Pos, (14) 
.equ USART_ISR_ABRE_Msk, (0x1 << USART_ISR_ABRE_Pos) //!< 0x00004000 
.equ USART_ISR_ABRE, USART_ISR_ABRE_Msk //!< Auto-Baud Rate Error 
.equ USART_ISR_ABRF_Pos, (15) 
.equ USART_ISR_ABRF_Msk, (0x1 << USART_ISR_ABRF_Pos) //!< 0x00008000 
.equ USART_ISR_ABRF, USART_ISR_ABRF_Msk //!< Auto-Baud Rate Flag 
.equ USART_ISR_BUSY_Pos, (16) 
.equ USART_ISR_BUSY_Msk, (0x1 << USART_ISR_BUSY_Pos) //!< 0x00010000 
.equ USART_ISR_BUSY, USART_ISR_BUSY_Msk //!< Busy Flag 
.equ USART_ISR_CMF_Pos, (17) 
.equ USART_ISR_CMF_Msk, (0x1 << USART_ISR_CMF_Pos) //!< 0x00020000 
.equ USART_ISR_CMF, USART_ISR_CMF_Msk //!< Character Match Flag 
.equ USART_ISR_SBKF_Pos, (18) 
.equ USART_ISR_SBKF_Msk, (0x1 << USART_ISR_SBKF_Pos) //!< 0x00040000 
.equ USART_ISR_SBKF, USART_ISR_SBKF_Msk //!< Send Break Flag 
.equ USART_ISR_RWU_Pos, (19) 
.equ USART_ISR_RWU_Msk, (0x1 << USART_ISR_RWU_Pos) //!< 0x00080000 
.equ USART_ISR_RWU, USART_ISR_RWU_Msk //!< Receive Wake Up from mute mode Flag 
.equ USART_ISR_WUF_Pos, (20) 
.equ USART_ISR_WUF_Msk, (0x1 << USART_ISR_WUF_Pos) //!< 0x00100000 
.equ USART_ISR_WUF, USART_ISR_WUF_Msk //!< Wake Up from stop mode Flag 
.equ USART_ISR_TEACK_Pos, (21) 
.equ USART_ISR_TEACK_Msk, (0x1 << USART_ISR_TEACK_Pos) //!< 0x00200000 
.equ USART_ISR_TEACK, USART_ISR_TEACK_Msk //!< Transmit Enable Acknowledge Flag 
.equ USART_ISR_REACK_Pos, (22) 
.equ USART_ISR_REACK_Msk, (0x1 << USART_ISR_REACK_Pos) //!< 0x00400000 
.equ USART_ISR_REACK, USART_ISR_REACK_Msk //!< Receive Enable Acknowledge Flag 
//******************  Bit definition for USART_ICR register  *****************
.equ USART_ICR_PECF_Pos, (0) 
.equ USART_ICR_PECF_Msk, (0x1 << USART_ICR_PECF_Pos) //!< 0x00000001 
.equ USART_ICR_PECF, USART_ICR_PECF_Msk //!< Parity Error Clear Flag 
.equ USART_ICR_FECF_Pos, (1) 
.equ USART_ICR_FECF_Msk, (0x1 << USART_ICR_FECF_Pos) //!< 0x00000002 
.equ USART_ICR_FECF, USART_ICR_FECF_Msk //!< Framing Error Clear Flag 
.equ USART_ICR_NCF_Pos, (2) 
.equ USART_ICR_NCF_Msk, (0x1 << USART_ICR_NCF_Pos) //!< 0x00000004 
.equ USART_ICR_NCF, USART_ICR_NCF_Msk //!< Noise detected Clear Flag 
.equ USART_ICR_ORECF_Pos, (3) 
.equ USART_ICR_ORECF_Msk, (0x1 << USART_ICR_ORECF_Pos) //!< 0x00000008 
.equ USART_ICR_ORECF, USART_ICR_ORECF_Msk //!< OverRun Error Clear Flag 
.equ USART_ICR_IDLECF_Pos, (4) 
.equ USART_ICR_IDLECF_Msk, (0x1 << USART_ICR_IDLECF_Pos) //!< 0x00000010 
.equ USART_ICR_IDLECF, USART_ICR_IDLECF_Msk //!< IDLE line detected Clear Flag 
.equ USART_ICR_TCCF_Pos, (6) 
.equ USART_ICR_TCCF_Msk, (0x1 << USART_ICR_TCCF_Pos) //!< 0x00000040 
.equ USART_ICR_TCCF, USART_ICR_TCCF_Msk //!< Transmission Complete Clear Flag 
.equ USART_ICR_LBDCF_Pos, (8) 
.equ USART_ICR_LBDCF_Msk, (0x1 << USART_ICR_LBDCF_Pos) //!< 0x00000100 
.equ USART_ICR_LBDCF, USART_ICR_LBDCF_Msk //!< LIN Break Detection Clear Flag 
.equ USART_ICR_CTSCF_Pos, (9) 
.equ USART_ICR_CTSCF_Msk, (0x1 << USART_ICR_CTSCF_Pos) //!< 0x00000200 
.equ USART_ICR_CTSCF, USART_ICR_CTSCF_Msk //!< CTS Interrupt Clear Flag 
.equ USART_ICR_RTOCF_Pos, (11) 
.equ USART_ICR_RTOCF_Msk, (0x1 << USART_ICR_RTOCF_Pos) //!< 0x00000800 
.equ USART_ICR_RTOCF, USART_ICR_RTOCF_Msk //!< Receiver Time Out Clear Flag 
.equ USART_ICR_EOBCF_Pos, (12) 
.equ USART_ICR_EOBCF_Msk, (0x1 << USART_ICR_EOBCF_Pos) //!< 0x00001000 
.equ USART_ICR_EOBCF, USART_ICR_EOBCF_Msk //!< End Of Block Clear Flag 
.equ USART_ICR_CMCF_Pos, (17) 
.equ USART_ICR_CMCF_Msk, (0x1 << USART_ICR_CMCF_Pos) //!< 0x00020000 
.equ USART_ICR_CMCF, USART_ICR_CMCF_Msk //!< Character Match Clear Flag 
.equ USART_ICR_WUCF_Pos, (20) 
.equ USART_ICR_WUCF_Msk, (0x1 << USART_ICR_WUCF_Pos) //!< 0x00100000 
.equ USART_ICR_WUCF, USART_ICR_WUCF_Msk //!< Wake Up from stop mode Clear Flag 
//******************  Bit definition for USART_RDR register  *****************
.equ USART_RDR_RDR_Pos, (0) 
.equ USART_RDR_RDR_Msk, (0x1FF << USART_RDR_RDR_Pos) //!< 0x000001FF 
.equ USART_RDR_RDR, USART_RDR_RDR_Msk //!< RDR[8:0] bits (Receive Data value) 
//******************  Bit definition for USART_TDR register  *****************
.equ USART_TDR_TDR_Pos, (0) 
.equ USART_TDR_TDR_Msk, (0x1FF << USART_TDR_TDR_Pos) //!< 0x000001FF 
.equ USART_TDR_TDR, USART_TDR_TDR_Msk //!< TDR[8:0] bits (Transmit Data value) 
//****************************************************************************
//
//                         USB Device General registers
//
//****************************************************************************
//***************************  ISTR interrupt events  ************************
.equ USB_ISTR_CTR, (0x8000) //!< Correct TRansfer (clear-only bit) 
.equ USB_ISTR_PMAOVR, (0x4000) //!< DMA OVeR/underrun (clear-only bit) 
.equ USB_ISTR_ERR, (0x2000) //!< ERRor (clear-only bit) 
.equ USB_ISTR_WKUP, (0x1000) //!< WaKe UP (clear-only bit) 
.equ USB_ISTR_SUSP, (0x0800) //!< SUSPend (clear-only bit) 
.equ USB_ISTR_RESET, (0x0400) //!< RESET (clear-only bit) 
.equ USB_ISTR_SOF, (0x0200) //!< Start Of Frame (clear-only bit) 
.equ USB_ISTR_ESOF, (0x0100) //!< Expected Start Of Frame (clear-only bit) 
.equ USB_ISTR_L1REQ, (0x0080) //!< LPM L1 state request 
.equ USB_ISTR_DIR, (0x0010) //!< DIRection of transaction (read-only bit) 
.equ USB_ISTR_EP_ID, (0x000F) //!< EndPoint IDentifier (read-only bit) 
// Legacy defines
.equ USB_ISTR_PMAOVRM, USB_ISTR_PMAOVR 
.equ USB_CLR_CTR, (~USB_ISTR_CTR) //!< clear Correct TRansfer bit 
.equ USB_CLR_PMAOVR, (~USB_ISTR_PMAOVR) //!< clear DMA OVeR/underrun bit 
.equ USB_CLR_ERR, (~USB_ISTR_ERR) //!< clear ERRor bit 
.equ USB_CLR_WKUP, (~USB_ISTR_WKUP) //!< clear WaKe UP bit 
.equ USB_CLR_SUSP, (~USB_ISTR_SUSP) //!< clear SUSPend bit 
.equ USB_CLR_RESET, (~USB_ISTR_RESET) //!< clear RESET bit 
.equ USB_CLR_SOF, (~USB_ISTR_SOF) //!< clear Start Of Frame bit 
.equ USB_CLR_ESOF, (~USB_ISTR_ESOF) //!< clear Expected Start Of Frame bit 
.equ USB_CLR_L1REQ, (~USB_ISTR_L1REQ) //!< clear LPM L1 bit 
// Legacy defines
.equ USB_CLR_PMAOVRM, USB_CLR_PMAOVR 
//************************  CNTR control register bits definitions  **********
.equ USB_CNTR_CTRM, (0x8000) //!< Correct TRansfer Mask 
.equ USB_CNTR_PMAOVR, (0x4000) //!< DMA OVeR/underrun Mask 
.equ USB_CNTR_ERRM, (0x2000) //!< ERRor Mask 
.equ USB_CNTR_WKUPM, (0x1000) //!< WaKe UP Mask 
.equ USB_CNTR_SUSPM, (0x0800) //!< SUSPend Mask 
.equ USB_CNTR_RESETM, (0x0400) //!< RESET Mask 
.equ USB_CNTR_SOFM, (0x0200) //!< Start Of Frame Mask 
.equ USB_CNTR_ESOFM, (0x0100) //!< Expected Start Of Frame Mask 
.equ USB_CNTR_L1REQM, (0x0080) //!< LPM L1 state request interrupt mask 
.equ USB_CNTR_L1RESUME, (0x0020) //!< LPM L1 Resume request 
.equ USB_CNTR_RESUME, (0x0010) //!< RESUME request 
.equ USB_CNTR_FSUSP, (0x0008) //!< Force SUSPend 
.equ USB_CNTR_LPMODE, (0x0004) //!< Low-power MODE 
.equ USB_CNTR_PDWN, (0x0002) //!< Power DoWN 
.equ USB_CNTR_FRES, (0x0001) //!< Force USB RESet 
// Legacy defines
.equ USB_CNTR_PMAOVRM, USB_CNTR_PMAOVR 
.equ USB_CNTR_LP_MODE, USB_CNTR_LPMODE 
//**************************  LPM register bits definitions  *****************
.equ USB_LPMCSR_BESL, (0x00F0) //!< BESL value received with last ACKed LPM Token 
.equ USB_LPMCSR_REMWAKE, (0x0008) //!< bRemoteWake value received with last ACKed LPM Token 
.equ USB_LPMCSR_LPMACK, (0x0002) //!< LPM Token acknowledge enable 
.equ USB_LPMCSR_LMPEN, (0x0001) //!< LPM support enable 
//*******************  FNR Frame Number Register bit definitions   ***********
.equ USB_FNR_RXDP, (0x8000) //!< status of D+ data line 
.equ USB_FNR_RXDM, (0x4000) //!< status of D- data line 
.equ USB_FNR_LCK, (0x2000) //!< LoCKed 
.equ USB_FNR_LSOF, (0x1800) //!< Lost SOF 
.equ USB_FNR_FN, (0x07FF) //!< Frame Number 
//*******************  DADDR Device ADDRess bit definitions    ***************
.equ USB_DADDR_EF, (0x80) //!< USB device address Enable Function 
.equ USB_DADDR_ADD, (0x7F) //!< USB device address 
//*****************************  Endpoint register    ************************
.equ USB_EP_CTR_RX, (0x8000) //!< EndPoint Correct TRansfer RX 
.equ USB_EP_DTOG_RX, (0x4000) //!< EndPoint Data TOGGLE RX 
.equ USB_EPRX_STAT, (0x3000) //!< EndPoint RX STATus bit field 
.equ USB_EP_SETUP, (0x0800) //!< EndPoint SETUP 
.equ USB_EP_T_FIELD, (0x0600) //!< EndPoint TYPE 
.equ USB_EP_KIND, (0x0100) //!< EndPoint KIND 
.equ USB_EP_CTR_TX, (0x0080) //!< EndPoint Correct TRansfer TX 
.equ USB_EP_DTOG_TX, (0x0040) //!< EndPoint Data TOGGLE TX 
.equ USB_EPTX_STAT, (0x0030) //!< EndPoint TX STATus bit field 
.equ USB_EPADDR_FIELD, (0x000F) //!< EndPoint ADDRess FIELD 
// EndPoint REGister MASK (no toggle fields)
.equ USB_EPREG_MASK, (USB_EP_CTR_RX | USB_EP_SETUP | USB_EP_T_FIELD | USB_EP_KIND | USB_EP_CTR_TX | USB_EPADDR_FIELD) 
.equ USB_EP_TYPE_MASK, (0x0600) //!< EndPoint TYPE Mask 
.equ USB_EP_BULK, (0x0000) //!< EndPoint BULK 
.equ USB_EP_CONTROL, (0x0200) //!< EndPoint CONTROL 
.equ USB_EP_ISOCHRONOUS, (0x0400) //!< EndPoint ISOCHRONOUS 
.equ USB_EP_INTERRUPT, (0x0600) //!< EndPoint INTERRUPT 
.equ USB_EP_T_MASK, ( (! USB_EP_T_FIELD) & USB_EPREG_MASK) 
.equ USB_EPKIND_MASK, ( (! USB_EP_KIND) & USB_EPREG_MASK) //!< EP_KIND EndPoint KIND 
.equ USB_EP_TX_DIS, (0x0000) //!< EndPoint TX DISabled 
.equ USB_EP_TX_STALL, (0x0010) //!< EndPoint TX STALLed 
.equ USB_EP_TX_NAK, (0x0020) //!< EndPoint TX NAKed 
.equ USB_EP_TX_VALID, (0x0030) //!< EndPoint TX VALID 
.equ USB_EPTX_DTOG1, (0x0010) //!< EndPoint TX Data TOGgle bit1 
.equ USB_EPTX_DTOG2, (0x0020) //!< EndPoint TX Data TOGgle bit2 
.equ USB_EPTX_DTOGMASK, (USB_EPTX_STAT | USB_EPREG_MASK) 
.equ USB_EP_RX_DIS, (0x0000) //!< EndPoint RX DISabled 
.equ USB_EP_RX_STALL, (0x1000) //!< EndPoint RX STALLed 
.equ USB_EP_RX_NAK, (0x2000) //!< EndPoint RX NAKed 
.equ USB_EP_RX_VALID, (0x3000) //!< EndPoint RX VALID 
.equ USB_EPRX_DTOG1, (0x1000) //!< EndPoint RX Data TOGgle bit1 
.equ USB_EPRX_DTOG2, (0x2000) //!< EndPoint RX Data TOGgle bit1 
.equ USB_EPRX_DTOGMASK, (USB_EPRX_STAT | USB_EPREG_MASK) 
//****************************************************************************
//
//                            Window WATCHDOG
//
//****************************************************************************
//******************  Bit definition for WWDG_CR register  *******************
.equ WWDG_CR_T_Pos, (0) 
.equ WWDG_CR_T_Msk, (0x7F << WWDG_CR_T_Pos) //!< 0x0000007F 
.equ WWDG_CR_T, WWDG_CR_T_Msk //!< T[6:0] bits (7-Bit counter (MSB to LSB)) 
.equ WWDG_CR_T_0, (0x01 << WWDG_CR_T_Pos) //!< 0x00000001 
.equ WWDG_CR_T_1, (0x02 << WWDG_CR_T_Pos) //!< 0x00000002 
.equ WWDG_CR_T_2, (0x04 << WWDG_CR_T_Pos) //!< 0x00000004 
.equ WWDG_CR_T_3, (0x08 << WWDG_CR_T_Pos) //!< 0x00000008 
.equ WWDG_CR_T_4, (0x10 << WWDG_CR_T_Pos) //!< 0x00000010 
.equ WWDG_CR_T_5, (0x20 << WWDG_CR_T_Pos) //!< 0x00000020 
.equ WWDG_CR_T_6, (0x40 << WWDG_CR_T_Pos) //!< 0x00000040 
// Legacy defines
.equ WWDG_CR_T0, WWDG_CR_T_0 
.equ WWDG_CR_T1, WWDG_CR_T_1 
.equ WWDG_CR_T2, WWDG_CR_T_2 
.equ WWDG_CR_T3, WWDG_CR_T_3 
.equ WWDG_CR_T4, WWDG_CR_T_4 
.equ WWDG_CR_T5, WWDG_CR_T_5 
.equ WWDG_CR_T6, WWDG_CR_T_6 
.equ WWDG_CR_WDGA_Pos, (7) 
.equ WWDG_CR_WDGA_Msk, (0x1 << WWDG_CR_WDGA_Pos) //!< 0x00000080 
.equ WWDG_CR_WDGA, WWDG_CR_WDGA_Msk //!<Activation bit 
//******************  Bit definition for WWDG_CFR register  ******************
.equ WWDG_CFR_W_Pos, (0) 
.equ WWDG_CFR_W_Msk, (0x7F << WWDG_CFR_W_Pos) //!< 0x0000007F 
.equ WWDG_CFR_W, WWDG_CFR_W_Msk //!< W[6:0] bits (7-bit window value) 
.equ WWDG_CFR_W_0, (0x01 << WWDG_CFR_W_Pos) //!< 0x00000001 
.equ WWDG_CFR_W_1, (0x02 << WWDG_CFR_W_Pos) //!< 0x00000002 
.equ WWDG_CFR_W_2, (0x04 << WWDG_CFR_W_Pos) //!< 0x00000004 
.equ WWDG_CFR_W_3, (0x08 << WWDG_CFR_W_Pos) //!< 0x00000008 
.equ WWDG_CFR_W_4, (0x10 << WWDG_CFR_W_Pos) //!< 0x00000010 
.equ WWDG_CFR_W_5, (0x20 << WWDG_CFR_W_Pos) //!< 0x00000020 
.equ WWDG_CFR_W_6, (0x40 << WWDG_CFR_W_Pos) //!< 0x00000040 
// Legacy defines
.equ WWDG_CFR_W0, WWDG_CFR_W_0 
.equ WWDG_CFR_W1, WWDG_CFR_W_1 
.equ WWDG_CFR_W2, WWDG_CFR_W_2 
.equ WWDG_CFR_W3, WWDG_CFR_W_3 
.equ WWDG_CFR_W4, WWDG_CFR_W_4 
.equ WWDG_CFR_W5, WWDG_CFR_W_5 
.equ WWDG_CFR_W6, WWDG_CFR_W_6 
.equ WWDG_CFR_WDGTB_Pos, (7) 
.equ WWDG_CFR_WDGTB_Msk, (0x3 << WWDG_CFR_WDGTB_Pos) //!< 0x00000180 
.equ WWDG_CFR_WDGTB, WWDG_CFR_WDGTB_Msk //!< WDGTB[1:0] bits (Timer Base) 
.equ WWDG_CFR_WDGTB_0, (0x1 << WWDG_CFR_WDGTB_Pos) //!< 0x00000080 
.equ WWDG_CFR_WDGTB_1, (0x2 << WWDG_CFR_WDGTB_Pos) //!< 0x00000100 
// Legacy defines
.equ WWDG_CFR_WDGTB0, WWDG_CFR_WDGTB_0 
.equ WWDG_CFR_WDGTB1, WWDG_CFR_WDGTB_1 
.equ WWDG_CFR_EWI_Pos, (9) 
.equ WWDG_CFR_EWI_Msk, (0x1 << WWDG_CFR_EWI_Pos) //!< 0x00000200 
.equ WWDG_CFR_EWI, WWDG_CFR_EWI_Msk //!<Early Wakeup Interrupt 
//******************  Bit definition for WWDG_SR register  *******************
.equ WWDG_SR_EWIF_Pos, (0) 
.equ WWDG_SR_EWIF_Msk, (0x1 << WWDG_SR_EWIF_Pos) //!< 0x00000001 
.equ WWDG_SR_EWIF, WWDG_SR_EWIF_Msk //!<Early Wakeup Interrupt Flag 
//****************************************************************************
//  For a painless codes migration between the STM32F3xx device product
//  lines, the aliases defined below are put in place to overcome the
//  differences in the interrupt handlers and IRQn definitions.
//  No need to update developed interrupt code when moving across
//  product lines within the same STM32F3 Family
//****************************************************************************
// Aliases for __IRQn
//ADC1_IRQn        .equ ADC1_2_IRQn
//SDADC1_IRQn      .equ ADC4_IRQn
//COMP1_2_IRQn     .equ COMP1_2_3_IRQn
//COMP2_IRQn       .equ COMP1_2_3_IRQn
//COMP_IRQn        .equ COMP1_2_3_IRQn
//COMP4_6_IRQn     .equ COMP4_5_6_IRQn
//HRTIM1_FLT_IRQn  .equ I2C3_ER_IRQn
//HRTIM1_TIME_IRQn .equ I2C3_EV_IRQn
//TIM15_IRQn       .equ TIM1_BRK_TIM15_IRQn
//TIM18_DAC2_IRQn  .equ TIM1_CC_IRQn
//TIM17_IRQn       .equ TIM1_TRG_COM_TIM17_IRQn
//TIM16_IRQn       .equ TIM1_UP_TIM16_IRQn
//TIM19_IRQn       .equ TIM20_UP_IRQn
//TIM6_DAC1_IRQn   .equ TIM6_DAC_IRQn
//TIM7_DAC2_IRQn   .equ TIM7_IRQn
//TIM12_IRQn       .equ TIM8_BRK_IRQn
//TIM14_IRQn       .equ TIM8_TRG_COM_IRQn
//TIM13_IRQn       .equ TIM8_UP_IRQn
//CEC_IRQn         .equ USBWakeUp_IRQn
//USBWakeUp_IRQn   .equ USBWakeUp_RMP_IRQn
//CAN_TX_IRQn      .equ USB_HP_CAN_TX_IRQn
//CAN_RX0_IRQn     .equ USB_LP_CAN_RX0_IRQn
// Aliases for __IRQHandler
//ADC1_IRQHandler        .equ ADC1_2_IRQHandler
//SDADC1_IRQHandler      .equ ADC4_IRQHandler
//COMP1_2_IRQHandler     .equ COMP1_2_3_IRQHandler
//COMP2_IRQHandler       .equ COMP1_2_3_IRQHandler
//COMP_IRQHandler        .equ COMP1_2_3_IRQHandler
//COMP4_6_IRQHandler     .equ COMP4_5_6_IRQHandler
//HRTIM1_FLT_IRQHandler  .equ I2C3_ER_IRQHandler
//HRTIM1_TIME_IRQHandler .equ I2C3_EV_IRQHandler
//TIM15_IRQHandler       .equ TIM1_BRK_TIM15_IRQHandler
//TIM18_DAC2_IRQHandler  .equ TIM1_CC_IRQHandler
//TIM17_IRQHandler       .equ TIM1_TRG_COM_TIM17_IRQHandler
//TIM16_IRQHandler       .equ TIM1_UP_TIM16_IRQHandler
//TIM19_IRQHandler       .equ TIM20_UP_IRQHandler
//TIM6_DAC1_IRQHandler   .equ TIM6_DAC_IRQHandler
//TIM7_DAC2_IRQHandler   .equ TIM7_IRQHandler
//TIM12_IRQHandler       .equ TIM8_BRK_IRQHandler
//TIM14_IRQHandler       .equ TIM8_TRG_COM_IRQHandler
//TIM13_IRQHandler       .equ TIM8_UP_IRQHandler
//CEC_IRQHandler         .equ USBWakeUp_IRQHandler
//USBWakeUp_IRQHandler   .equ USBWakeUp_RMP_IRQHandler
//CAN_TX_IRQHandler      .equ USB_HP_CAN_TX_IRQHandler
//CAN_RX0_IRQHandler     .equ USB_LP_CAN_RX0_IRQHandler
//*********************** (C) COPYRIGHT STMicroelectronics *****END OF FILE***
