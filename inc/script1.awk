# zmena poradi EQU 
/^;/ {print; next }                  # Ignore lines starting with a semicolon
$2 != "EQU" { next }                 # Ignore lines where the second word is not "EQU"
{
    temp = $1
    $1 = $2
    $2 = temp ","                    # Append a comma after swapping word1 and word2
    for (i = 1; i <= NF; i++) {
        printf $i " "                # Reconstruct the line with the changes
    }
    printf "\n"
}

