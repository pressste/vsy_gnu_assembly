#!/bin/sh

tmp_file=$(mktemp)
src_file="stm32f303xe.s"
out_file="stm32defs.s"

# spust skript1 s awk
awk -f script1.awk $src_file > $tmp_file
# nahrad veskerej bordel
awk '{gsub(/;/, "//"); gsub(/:SHL:/, "<<"); gsub(/EQU/, ".equ"); gsub(/:NOT:/, "!"); gsub(/:AND:/, "&"); gsub(/:OR:/, "|"); gsub("\r", ""); print}' $tmp_file > $out_file
